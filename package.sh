mkdir new_engine_install/
#cp -v -r new_engine/* new_engine_install/
cd new_engine/
find . \( -not \( -wholename "*/.svn/*" \) -and -not \( -name "*~" \)  \) -exec cp -v --parent {} ../new_engine_install/ \;
cd ../new_engine_install/
#find . \( -not \( -name .svn \) -and -not \( -name "*~" \) \) -exec rm -rf {} \; -print
#find . -name "*~" -exec rm -rf {} \; -print
sed -i 's/IICHANTRA_INSTALL OFF/IICHANTRA_INSTALL ON/' CMakeLists.txt
echo ""
echo "sudo chown -R root:root *"
sudo chown -R root:root *
echo ""
echo "sudo chmod -R 755 *"
sudo chmod -R 755 *
#echo ""
#echo "sudo chmod +x bin/iichantra"
#sudo chmod +x bin/iichantra
echo ""
echo "sudo cmake . ."
sudo cmake . .
echo ""
echo "sudo make package"
sudo make -j8 package
#echo ""
#echo "sudo dpkg -i iichantra.deb"
#sudo dpkg -i iichantra.deb
