#ifndef _SND_H_
#define _SND_H_

#include "../resource_mgr.h"
#include "../resource.h"
#include "bass.h"

typedef HMUSIC BASSHANDLE;
enum AudioType { atSound, atMusic };

class Sound : public Resource
{
public:
	Sound(string name, unsigned char* buffer, size_t buffer_size) : Resource(name, buffer, buffer_size)
	{
		_buffer = NULL;
		_buffer_size = 0;
		bassHandle = 0;
		type = atSound;
	}

	virtual ~Sound();

	bool Play(bool restart) const;
	bool Pause() const;
	bool Stop() const;

	// TODO: ��������� BASS_ChannelSlideAttribute, ����� ������ ������ ���������.

	float GetVolume() const;
	bool SetVolume(float value);
	uint32_t GetLevel();
	double GetLength();
	float* GetFFT();
	bool SetPan( float dx );
	void SetSpeed(float speed);
	float GetSpeed();
	
	bool SetLooped(bool loop);

	virtual bool Load();

private:
	char* _buffer;
	size_t _buffer_size;
	BASSHANDLE bassHandle;
	AudioType type;
};

class SoundMgr : public ResourceMgr<Sound>
{
public:
	SoundMgr(const char* path, const char* extension, vector<string>* archive_list, vector<uint32_t>* crc_list);
	~SoundMgr();
	bool Initialize();
	void Destroy();

	bool PlaySnd(const string& name, bool looped, float volume = 1.0f);
	bool PlaySnd(const string& name, bool restart, Vector2 origin, float volume = 1.0f);
	bool PauseSnd(const string& name);
	bool StopSnd(const string& name);

	void StopAll();
	bool PauseAll();
	bool ResumeAll();

	float GetVolume();
	bool SetVolume(float vol);
	float GetSoundVolume();
	float GetMusicVolume();
	void SetSoundVolume(float value);
	void SetMusicVolume(float value);
	float GetSndVolume(string name);
	bool SetSndVolume(string name, float vol);
	bool SetMusVolume(float vol);
	float GetMusicSpeed();
	void SetMusicSpeed(float value);

	bool PlayBackMusic(string name, float volume);
	char* GetBackMusic();
	bool ResumeBackMusic();
	bool PauseBackMusic();
	bool StopBackMusic();
	uint32_t GetBackMusicLevel();
	double GetBackMusicLength();
	double GetSndLength(string name);
	float* GetBackMusicFFT();

	const string& GetCurrentBackMusic() const;
private:
	void SetCurrentBackMusic(string newMusic);



	string currBackMusic;
	int err;
	bool initialized;
};

#endif 
