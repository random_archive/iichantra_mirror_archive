#include "StdAfx.h"

#include "resource_mgr.h"

#include "render/texture.h"
#include "render/font.h"
#include "game/proto.h"
#include "script/script.h"
#include "sound/snd.h"

#include "game/crc32.h"

ResourceMgr<Proto> * protoMgr = NULL;
ResourceMgr<Texture> * textureMgr = NULL;
ResourceMgr<TexFrames> * texFramesMgr = NULL;
ResourceMgr<TexFont> * fontMgr = NULL;
ResourceMgr<Script> * scriptMgr = NULL;
SoundMgr * soundMgr = NULL;
vector<string> archive_list;
vector<uint32_t> crc32_list;
unsigned char* input_buffer;
size_t input_buffer_size;
void InitResourceManagers()
{
	protoMgr = new ResourceMgr<Proto>(DEFAULT_PROTO_PATH, "lua", &archive_list, &crc32_list);
	textureMgr = new ResourceMgr<Texture>(DEFAULT_TEXTURES_PATH, "png", &archive_list, &crc32_list);
	texFramesMgr = new ResourceMgr<TexFrames>(DEFAULT_TEXTURES_PATH, "lua", &archive_list, &crc32_list);
	fontMgr = new ResourceMgr<TexFont>(DEFAULT_FONTS_PATH, NULL, &archive_list, &crc32_list);
	scriptMgr = new ResourceMgr<Script>("", NULL, &archive_list, &crc32_list);
	soundMgr = new SoundMgr(DEFAULT_SOUNDS_PATH, NULL, &archive_list, &crc32_list);
	input_buffer = new unsigned char[ 10000 ];
	input_buffer_size = 10000;
}

void DestroyResourceManagers()
{
	DELETESINGLE(protoMgr);
	DELETESINGLE(textureMgr);
	DELETESINGLE(texFramesMgr);
	DELETESINGLE(fontMgr);
	DELETESINGLE(soundMgr);
	DELETESINGLE(scriptMgr);
	DELETEARRAY(input_buffer);
}

void UnloadResourceManagersArchive( string* arch )
{
	/*
			���������! ���� ���� ��� �������� ���������� ������, �� �������� ������������� ����������� ������ ����
		� ���� ������� ������, ����� ����� ������ ������ ��-�� ������������� ����������� ���������� �������������.
		��������, ���� texFramesMgr ����� ���� ����� textureMgr, �� ��� �������� �������� ������ ��� �� ����� ��
		���������� ������������� ����� ��������������� � ��� �� ��������, �� �������������� "������" �������������
		� ��������� unmanaged, ��-�� ���� �� �������� �������.
	*/
	protoMgr->UnloadArchive( arch );
	texFramesMgr->UnloadArchive( arch );
	textureMgr->UnloadArchive( arch );
	fontMgr->UnloadArchive( arch );
	scriptMgr->UnloadArchive( arch );
	soundMgr->UnloadArchive( arch );
}

void LoadResourceManagersArchive()
{
	protoMgr->ArchiveRegistered();
	textureMgr->ArchiveRegistered();
	texFramesMgr->ArchiveRegistered();
	fontMgr->ArchiveRegistered();
	scriptMgr->ArchiveRegistered();
	soundMgr->ArchiveRegistered();
}

//void DoLoad(const char* const name)
//{
//	char* n1 = GetNameFromPath(name);
//	protoMgr->GetByName(n1);
//	DELETEARRAY(n1);
//}
//
//
//void TestLoading()
//{
//	char full_file_name[MAX_PATH];
//	FindAllFiles(path_protos, full_file_name, DoLoad);
//}

void RegisterResourceArchive( const char* name )
{
	for ( vector<string>::iterator iter = archive_list.begin(); iter != archive_list.end(); ++iter )
	{
		if ( strcmp( name, iter->c_str() ) == 0 )
		{
			Log( DEFAULT_LOG_NAME, logLevelWarning, "Trying to register archive '%s' that is already registered", name );
			return;
		}
	}
	archive_list.push_back( string( name ) );
	char full_archive_name[MAX_PATH];
	strcpy(full_archive_name, path_app);
	strcat(full_archive_name, name );
	crc32_list.push_back( crc32f( full_archive_name ) );
	Script* sc = scriptMgr->GetFromArchive( "_on_register.lua", "", archive_list.size()-1 );
	if ( sc )
	{
		sc->Execute();
		DELETESINGLE( sc );
	}
	LoadResourceManagersArchive();
}

void UnregisterResourceArchive( const char* name )
{
	size_t num = 0;
	for ( vector<string>::iterator iter = archive_list.begin(); iter != archive_list.end(); ++iter )
	{
		if ( strcmp( name, iter->c_str() ) == 0 )
		{
			Script* sc = scriptMgr->GetFromArchive( "_on_unregister.lua", "", num );
			if ( sc )
			{
				sc->Execute();
				DELETESINGLE( sc );
			}
			UnloadResourceManagersArchive( &(*iter) );
			archive_list.erase( iter );
			crc32_list.erase( crc32_list.begin()+num );
			return;
		}
		num++;
	}
	Log( DEFAULT_LOG_NAME, logLevelWarning, "Trying to unregister archive '%s' that is not registered", name );
}
