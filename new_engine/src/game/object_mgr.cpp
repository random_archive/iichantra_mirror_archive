#include "StdAfx.h"

#include "camera.h"
#include "game.h"
#include "net.h"

#include "object_mgr.h"
#include "objects/object_physic.h"
#include "objects/object_ray.h"
#include "objects/object_waypoint.h"

#include "phys/phys_collisionsolver.h"
#ifdef SELECTIVE_RENDERING
# include "phys/sap/OPC_ArraySAP.h"
# define SELECTIVE_RENDERING_SORTING 
# ifdef SELECTIVE_RENDERING_SORTING
#  include "phys/sap/IceRevisitedRadix.h"
# endif
#endif


#include "../input_mgr.h"
#include "editor.h"


//////////////////////////////////////////////////////////////////////////

extern InputMgr inpmgr;
extern bool isNewGameCreated;
extern bool netgame;
extern game::GameStates game_state;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

extern lua_State *lua;
extern UINT internal_time;

extern float CAMERA_GAME_LEFT;
extern float CAMERA_GAME_RIGHT;
extern float CAMERA_GAME_TOP;
extern float CAMERA_GAME_BOTTOM;

extern GameObject* attached_object;
extern GameObject* Waypoints;

#ifdef SELECTIVE_RENDERING
extern ASAP_AABB GetASAP_AABB( CAABB& aabb );
#endif

//////////////////////////////////////////////////////////////////////////

#ifdef DEBUG_LOG_INFO
const char* tNames[] = {"objNone", "objPlayer", "objSprite", "objBullet", "objSpatialTrigger", "objEffect", "objEnemy", "objItem", "objEnvironment", "objRay", "objParticleSystem"};
#endif // DEBUG_LOG_INFO

std::map<int,GameObject*>* ObjTree = NULL;		//��������� �� ������ ��������
std::map<int,GameObject*>* AddTree = NULL;		//��������� �� ������ ������ ��� ����������� ��������

std::map<int,ObjRay*>* RayTree = NULL;		// ��������� ������-��������� �����

std::map<int,GameObject*>* ActiveObjTree = NULL;
std::map<int,GameObject*>* PassiveObjTree = NULL;

std::map<int,GameObject*>* tempObjTree = NULL;

vector<GameObject*> removedObjects;

vector<GameObject*> screenPhys1;
vector<GameObject*> screenPhys2;

vector<GameObject*>* oldOnScreenPhys = &screenPhys2;
vector<GameObject*>* newOnScreenPhys = &screenPhys1;

UINT last_id = 1;

#ifdef SELECTIVE_RENDERING
Opcode::ArraySAP graphSap(Opcode::ArraySAP::NONSIMMETRICAL);   // SAP ��� ������ �������� �� ������

std::vector<GameObject const*> objsForRender;
std::vector<UINT> inObjsForRender;
const UINT INOFR_INV_ID = 0xffffffff;

void cleanObjsForRender();
#endif

//////////////////////////////////////////////////////////////////////////

// ����������� ������ �������. ��������� ���������� ������� � SAP.
void AddObject(GameObject* obj, UINT id/* = 0*/)
{
	if (!ObjTree) ObjTree = new std::map<int,GameObject*>;
	if (!AddTree) AddTree = new std::map<int,GameObject*>;
	if (!RayTree) RayTree = new std::map<int,ObjRay*>;
	if (!ActiveObjTree) ActiveObjTree = new std::map<int,GameObject*>;
	if (!PassiveObjTree) PassiveObjTree = new std::map<int,GameObject*>;

	if ( netgame ) 
	{
		if ( id > 0 ) 
		{
			if ( GetGameObject( id ) != NULL )
				return;
			obj->id = id;
		}
		else
		{
			obj->id = Net::GetNextID();
			if ( obj->id == 0 )
				return;
		}
	}
	else 
		obj->id = ++last_id;

	(*AddTree)[obj->id] = obj;

	if (obj->IsPhysic())
	{
		if (obj->aabb.W <= 0) obj->aabb.W = 1;
		if (obj->aabb.H <= 0) obj->aabb.H = 1;
		((ObjPhysic*)obj)->sap_handle = AddSAPObject((ObjPhysic*)obj, (UINT)obj->id, obj->aabb);
#ifdef DEBUG_SAP
		const char* protoName = obj->sprite ? obj->sprite->proto_name : NULL;
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Adding to sap %s(%s) 0x%p id: %d, sap: %d, p:%f,%f", 
			tNames[obj->type], protoName, obj, obj->id, ((ObjPhysic*)obj)->sap_handle, obj->aabb.p.x, obj->aabb.p.y);
#endif // DEBUG_SAP
	}

}

void RemoveObject(GameObject* obj)
{
	// ��������� ������ � ������ � ��������
	removedObjects.push_back(obj);
	if (obj->IsPhysic())
	{
#ifdef DEBUG_SAP
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Removing from sap %s 0x%p id: %d, sap: %d", tNames[obj->type], obj, obj->id, ((ObjPhysic*)obj)->sap_handle);
#endif // DEBUG_SAP 
		// ������� ������ �� ASAP
		RemoveSAPObject( ((ObjPhysic*)obj)->sap_handle );
	}

	// ������� �� ������ ����� ��� ��, ��� ���������� ������� ��������� �� SAP
	if (obj->type == objRay) RayTree->erase(obj->id);
}


//////////////////////////////////////////////////////////////////////////

// �������� ���� ����� �������� � �������� ��������� ��������.
void BatchAdd()
{
	if ( AddTree == NULL || AddTree->empty() )
		return;

	bool update_sap = false;
	bool update_graph = false;
	UNUSED_ARG( update_graph );
    for(std::map<int,GameObject*>::iterator AddIter = AddTree->begin(); AddIter != AddTree->end(); AddIter++)
	{
        GameObject* obj = AddIter->second;
		if ( obj->IsPhysic() ) update_sap = true;
        (*ObjTree)[obj->id] = obj;
		if (obj->type == objRay) (*RayTree)[obj->id] = (ObjRay*)obj;
		// ��� �������� �������� ������ ��������� ��������.
		if ((!obj->IsPhysic() 
#ifdef MAP_EDITOR
			|| ( editor_state == editor::EDITOR_OFF && !((ObjPhysic*)obj)->IsDynamic() )
#else
			|| !((ObjPhysic*)obj)->IsDynamic()
#endif //MAP_EDITOR
			) && 
			(!obj->sprite || (obj->sprite && obj->sprite->animsCount == 0) || obj->sprite->IsAnimDone()) &&
			!obj->ParticleSystem && obj->type != objSpawner)
		{
			//Next line, if removed, causes first update cycle to be processed without passive physic objects.
			//More often than not it causes falling through platforms when large maps are first loaded.
			//Think carefully before removing that.
			if (obj->IsPhysic() && static_cast<ObjPhysic*>(obj)->sap_handle > 0) AddSAPObject( static_cast<ObjPhysic*>(obj), obj->id, obj->aabb );

			(*PassiveObjTree)[obj->id] = obj;
		}
		else
		{
			(*ActiveObjTree)[obj->id] = obj;
		}

#ifdef SELECTIVE_RENDERING

		CAABB tmp_aabb;
		if (obj->GetDrawAABB(tmp_aabb))
		{
			obj->graph_handle = graphSap.AddObject(obj, obj->id, GetASAP_AABB( tmp_aabb ), 2, 1);
			update_graph = true;
		}


		//if (obj->sprite && obj->sprite->getRenderMethod() == rsmStandart && IsNear(obj->sprite->getAngle(), 0.0f, 0.0001f))
		//if (obj->sprite)
		//{
		//	CAABB tmp_aabb = obj->sprite->getRenderAABB(obj->aabb);
		//	obj->graph_handle = graphSap.AddObject(obj, obj->id, GetASAP_AABB( tmp_aabb ));
		//}
		//else 
		//{
			//(*PassiveGraphObjTree)[obj->id] = obj;

			//if (!obj->sprite)
			//	Log(DEFAULT_LOG_NAME, logLevelInfo, "PASSIVE no sprite");
			//else if (obj->sprite->getRenderMethod() != rsmStandart)
			//	Log(DEFAULT_LOG_NAME, logLevelInfo, "PASSIVE not rsmStandart %d", (int)obj->sprite->getRenderMethod());
			//else if (!IsNear(obj->sprite->getAngle(), 0.0f, 0.0001f))
			//	Log(DEFAULT_LOG_NAME, logLevelInfo, "PASSIVE angle %f", obj->sprite->getAngle());
			//else
			//	Log(DEFAULT_LOG_NAME, logLevelInfo, "PASSIVE WTF");
		//}
#endif
	}
	AddTree->clear();

	if (update_sap)
		UpdateSAPState();

#ifdef SELECTIVE_RENDERING
	if (update_graph)
		graphSap.DumpPairs(0,0,0,0);
#endif
}


// ����������� �������
void RemObj(GameObject* obj)
{
	if (obj)
	{
		if (GetCameraAttachedObject() == obj)
		{
			// �� ���� ������� ������������� ������. ������� ���.
			CameraAttachToObject(NULL);
		}
		if (Waypoints == obj)
			Waypoints = NULL;
		if ( obj->childrenConnection )
		{
			if ( obj->childrenConnection->children.size() > 0 )
			{
				for ( vector<GameObject*>::iterator iter = obj->childrenConnection->children.begin(); iter != obj->childrenConnection->children.end(); ++iter )
					(*iter)->ParentEvent( ObjectEventInfo( eventDead ) );
			}
			obj->childrenConnection->parentDead();
		}
		if ( obj->parentConnection )
			obj->parentConnection->childDead( obj->id );
#ifdef MAP_EDITOR
		if ( editor_state != editor::EDITOR_OFF )
		{
			if ( obj->incoming_links ) obj->incoming_links->parentDead();
			if ( obj->link ) obj->link->childDead( obj->id );
		}
#endif // MAP_EDITOR
		
#ifdef SELECTIVE_RENDERING
		if (obj->graph_handle != GameObject::INVALID_GRAPH_HANDLE)
		{
			graphSap.RemoveObject(obj->graph_handle);
			//obj->graph_handle = GameObject::INVALID_GRAPH_HANDLE;
		}

		if (inObjsForRender.size() > obj->id && inObjsForRender[obj->id] != INOFR_INV_ID)
		{
			if (objsForRender.size() - 1 != inObjsForRender[obj->id])
			{
				GameObject const * last = objsForRender.back();
				inObjsForRender[last->id] = inObjsForRender[obj->id];
				objsForRender[inObjsForRender[last->id]] = last;
			}

			objsForRender.pop_back();
			inObjsForRender[obj->id] = INOFR_INV_ID;
		}

#endif

		DELETESINGLE(obj);
	}
}

// ������� ��� �������� ������� �� ��������� ���������� �������� � �������� �� �����������.
void BatchRemove()
{
	if (!removedObjects.size())
		return;

	GameObject* obj = NULL;

	bool update_sap = false;

	for(vector<GameObject*>::iterator it = removedObjects.begin();
		it != removedObjects.end();
		it++)
	{
		obj = *it;
		ObjTree->erase(obj->id);
		ActiveObjTree->erase(obj->id);
		PassiveObjTree->erase(obj->id);
		if (obj->IsPhysic()) update_sap = true;	

		RemObj(obj);
	}

	if (update_sap)
		UpdateSAPState();

	removedObjects.clear();
}

//////////////////////////////////////////////////////////////////////////

// 
void PrepareRemoveAllObjects()
{
	screenPhys1.clear();
	screenPhys2.clear();
	tempObjTree = ObjTree;
	ObjTree = NULL;
	DELETESINGLE(RayTree);
	DELETESINGLE(ActiveObjTree);
	DELETESINGLE(PassiveObjTree);
	
	DefaultEnvDelete();
	EmptyWPStack();

	// ������� �������, ������� ���� �������, �� ��� �� ���� ��������� � �������� ���������.
	if ( AddTree != NULL && !(AddTree->empty()) )
	{
        for(std::map<int,GameObject*>::iterator AddIter = AddTree->begin(); AddIter != AddTree->end(); AddIter++)
        {
            GameObject* obj = AddIter->second;
			if (obj->IsPhysic()) RemoveSAPObject(static_cast<ObjPhysic*>(obj)->sap_handle);
			RemObj(obj);
		}
		DumpPairs();
	}
	DELETESINGLE(AddTree);

	// ���� �������� ����� � RemoveAllObjects
	if (removedObjects.size() > 0)
		removedObjects.clear();

	PrepareRemoveArraySAP();

#ifdef SELECTIVE_RENDERING
	if (tempObjTree)
	{
# ifdef DEBUG_SAP
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Cleaning graphSap");
# endif // DEBUG_SAP
		for(std::map<int,GameObject*>::iterator it = tempObjTree->begin(); it != tempObjTree->end(); ++it)
		{
			GameObject* obj = it->second;
			if (obj->graph_handle != GameObject::INVALID_GRAPH_HANDLE)
			{
				graphSap.RemoveObject(obj->graph_handle);
				obj->graph_handle = GameObject::INVALID_GRAPH_HANDLE;
			}
		}
		graphSap.DumpPairs(0,0,0,0);
	}

	cleanObjsForRender();
#endif // SELECTIVE_RENDERING
}

// ���������� ��� �������.
void RemoveAllObjects()
{
	game::GameStates temp_game_state = game_state;
	game_state = game::GAME_DESTROYING;

	if ( tempObjTree )
	{
		for(std::map<int,GameObject*>::iterator ObjIter = tempObjTree->begin(); ObjIter != tempObjTree->end(); ObjIter++)
		{
			GameObject* obj = ObjIter->second;
			if (obj->IsPhysic()) RemoveTempSAPObject(((ObjPhysic*)obj)->sap_handle);
			RemObj(obj);
		}
		delete tempObjTree;
		tempObjTree = NULL;
		DumpTempPairs();
	}

	Waypoints = NULL;
	ClearUnusedMaterials();

	RemoveArraySAP();
	game_state = temp_game_state;
}

//////////////////////////////////////////////////////////////////////////

GameObject* GetGameObject(UINT id)
{
    if ( AddTree != NULL )
    {
        std::map<int,GameObject*>::iterator iter = AddTree->find(id);
        if ( iter != AddTree->end() && iter->second != NULL )
            return iter->second;
    }

    if ( ObjTree != NULL )
    {
        std::map<int,GameObject*>::iterator iter = ObjTree->find(id);
        if ( iter != ObjTree->end() && iter->second != NULL )
            return iter->second;
    }

    return NULL;
}

void GroupObjects( int objGroup, int objToAdd )
{
	GameObject * group = GetGameObject( objGroup );
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		group->grouped = true;
	}
#endif //MAP_EDITOR
	GameObject * obj = GetGameObject( objToAdd );
	if ( !group || !obj || !group->IsPhysic() || !obj->IsPhysic() )
		return;

//	if (obj->IsPhysic())
	{
		//Vector2 old_corner = Vector2( group->aabb.GetMin(0)+group->sprite->realX,
		//							  group->aabb.GetMin(1)+group->sprite->realY );
		group->aabb = CAABB( min( group->aabb.GetMin(0), obj->aabb.GetMin(0) ),
							 	  min( group->aabb.GetMin(1), obj->aabb.GetMin(1) ),
								  max( group->aabb.GetMax(0), obj->aabb.GetMax(0) ),
								  max( group->aabb.GetMax(1), obj->aabb.GetMax(1) ));
		ObjPhysic* op = (ObjPhysic*)group;
		op->rectangle[0] = Vector2(-op->aabb.W, -op->aabb.H);
		op->rectangle[1] = Vector2( op->aabb.W, -op->aabb.H);
		op->rectangle[2] = Vector2( op->aabb.W,  op->aabb.H);
		op->rectangle[3] = Vector2(-op->aabb.W,  op->aabb.H);
		op->rectangle.CalcBox();
		//DELETESINGLE( group->aabb );
		RemoveSAPObject( ((ObjPhysic*)obj)->sap_handle );
		UpdateSAPState();
		UpdateSAPObject( ((ObjPhysic*)group)->sap_handle, group->aabb );
		//group->sprite->realX = old_corner.x - group->aabb.GetMin(0);
		//group->sprite->realY = old_corner.y - group->aabb.GetMin(1);
	}
	//DELETESINGLE( obj->aabb );
	obj->ClearPhysic();
	obj->SetDead();
	if ( group->sprite ) group->sprite->setRenderMethod(rsmRepeatXY);
}

//////////////////////////////////////////////////////////////////////////

void ProcessAllActiveObjects()
{
#ifdef DEBUG_SAP
	static UINT turn_number = 0;
	Log(DEFAULT_LOG_NAME, logLevelInfo, "=================== Turn begin #%d ====================", turn_number);
	turn_number++;
#endif // DEBUG_SAP
	
	BatchAdd();		// ����� �������� �� ��������� ����

	if(ActiveObjTree != NULL)
	{
		CAABB temp_old_aabb;
        for(std::map<int,GameObject*>::iterator ObjIter = ActiveObjTree->begin(); ObjIter != ActiveObjTree->end(); ObjIter++)
		{
			GameObject* obj = ObjIter->second;

			temp_old_aabb = obj->aabb;

#ifdef MAP_EDITOR
			if ( editor_state == editor::EDITOR_OFF )
#endif //MAP_EDITOR
			{
				if (obj->scriptProcess >= 0)
				{
					//lua_pushinteger(lua, obj->id);
					obj->pushUData(lua);
					if (SCRIPT::ExecChunkFromReg(obj->scriptProcess, 1) == -1)
					{
						// � ������� ��������� �����-�� ������. ����� ����������� ����������� � ��������� ���.
						SCRIPT::ReleaseProc(&obj->scriptProcess);
					}
				}
				/*else
				{*/
					obj->Process();
				//}
				if ( obj->net_update )
				{
					float c_dist = (obj->compensation_p - obj->aabb.p).Length();
					if ( c_dist > 100 || c_dist < 2 )
					{
						obj->aabb.p = obj->compensation_p;
						obj->net_update = false;
					}
					else
						obj->aabb.p += (obj->compensation_p - obj->aabb.p) * 0.1f;
				}

				if (obj->sprite && !obj->IsSleep())
				{
					obj->ProcessSprite();
				}

				if (isNewGameCreated)
					return;

				if (obj->ParticleSystem)
				{
					ObjEnvironment* env = static_cast<ObjDynamic*>(obj)->env;
					obj->ParticleSystem->Update(env);
					if ( obj->ParticleSystem->dead && obj->ParticleSystem->ParticlesActive == 0)
						obj->SetDead();
#ifdef SELECTIVE_RENDERING
					else
						selectForRenederUpdate(obj);
#endif
				}


				if (obj->type == objPlayer/* || obj->type == objEnemy*/)
				{
					ObjCharacter* ch = dynamic_cast<ObjCharacter*>(obj);
					if (NULL != ch)
					{
						ch->ProcessShooting();
					}
				}
			}
			
			if (obj->IsPhysic())
			{
				((ObjPhysic*)obj)->PhysProcess();
			}
			
			if (obj->IsDead())
			{
				RemoveObject(obj);
			}
#ifdef SELECTIVE_RENDERING
			else
			{
				if (temp_old_aabb != obj->aabb)
				{
					selectForRenederUpdate(obj);
				}
			}
#endif

		} // for

		// ��������� ��� ������������
#ifdef DEBUG_SAP
		Log(DEFAULT_LOG_NAME, logLevelInfo, "ProcessCollisions");
#endif // DEBUG_SAP
		ProcessCollisions();
	}

	BatchRemove();
	inpmgr.ProlongedArrayFlush(); // <- ��������� (�������) �������, ����� ������������.
	//BatchAdd();		// ����� �������� �� ��������� ����
}

void ProcessAllPassiveObjects()
{
	if ( newOnScreenPhys == &screenPhys1 )
	{
		newOnScreenPhys = &screenPhys2;
		oldOnScreenPhys = &screenPhys1;
	}
	else
	{
		newOnScreenPhys = &screenPhys1;
		oldOnScreenPhys = &screenPhys2;
	}
	
	Vector2 v = Vector2(320, 960);
	if ( attached_object && attached_object->IsPhysic() && ((ObjPhysic*)attached_object)->IsDynamic() )
	{
		ObjDynamic* od = (ObjDynamic*)attached_object;
		v.x += abs( od->vel.x );
		v.y += abs( od->vel.y );
	}
	
	newOnScreenPhys->clear();
	if(PassiveObjTree != NULL)
	{
		for(std::map<int,GameObject*>::iterator ObjIter = PassiveObjTree->begin(); ObjIter != PassiveObjTree->end(); ObjIter++)
		{
			GameObject* obj = ObjIter->second;
			
			if (obj->IsDead())
			{
				RemoveObject(obj);
			}
			else
			{
				if ( obj->IsPhysic() )
				{
					ObjPhysic* op = reinterpret_cast<ObjPhysic*>(obj);
					if ( op->update_needed )
					{
						UpdateObject(op->sap_handle, op->aabb);
						op->update_needed = false;
					}
					if ( !((ObjPhysic*)obj)->IsDynamic() && obj->aabb.p.x - obj->aabb.W < CAMERA_GAME_RIGHT + v.x && obj->aabb.p.x + obj->aabb.W > CAMERA_GAME_LEFT - v.x &&
					obj->aabb.p.y + obj->aabb.H > CAMERA_GAME_TOP - v.y && obj->aabb.p.y - obj->aabb.H < CAMERA_GAME_BOTTOM + v.y )
					{
						newOnScreenPhys->push_back( obj );
					}
				}
			}
		}
	}
	
	BatchRemove();
}

//////////////////////////////////////////////////////////////////////////

void DrawAllObjects()
{
#ifndef SELECTIVE_RENDERING

	if (ObjTree == NULL)
		return;

	for(std::map<int,GameObject*>::iterator ObjIter = ObjTree->begin(); ObjIter != ObjTree->end(); ObjIter++)
	{
        GameObject* obj = ObjIter->second;
		obj->Draw();
		if ( obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic() )
			((ObjDynamic*)obj)->DropShadow();
	}

#else

	//if (ActiveObjTree)
	//{
	//	for(std::map<int,GameObject*>::iterator ObjIter = ActiveObjTree->begin(); ObjIter != ActiveObjTree->end(); ObjIter++)
	//	{
	//		GameObject* obj = ObjIter->second;
	//		obj->Draw();
	//		if ( obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic() )
	//			((ObjDynamic*)obj)->DropShadow();
	//	}
	//}

	//if (PassiveGraphObjTree)
	//{
	//	for(std::map<int,GameObject*>::iterator ObjIter = PassiveGraphObjTree->begin(); ObjIter != PassiveGraphObjTree->end(); ObjIter++)
	//	{
	//		GameObject* obj = ObjIter->second;
	//		obj->Draw();
	//		if ( obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic() )
	//			((ObjDynamic*)obj)->DropShadow();
	//	}
	//}


	extern float CAMERA_DRAW_LEFT;
	extern float CAMERA_DRAW_RIGHT;
	extern float CAMERA_DRAW_TOP;
	extern float CAMERA_DRAW_BOTTOM;

	//Without temporary CAABBB variable in clang 2.8 you get:
	//candidate function not viable: no known conversion from 'CAABB' to 'CAABB &' for 1st argument
	CAABB tmp_aabb = CAABB(
		CAMERA_DRAW_LEFT, CAMERA_DRAW_TOP - SHADOW_DISTANCE, 
		CAMERA_DRAW_RIGHT, CAMERA_DRAW_BOTTOM );
	ASAP_AABB cam = GetASAP_AABB( tmp_aabb );

	static bool first_time = true;
	static udword cam_handle = 0;

	if (first_time)
	{
		cam_handle = graphSap.AddObject(&cam_handle, 0, cam, 1, 2);
		graphSap.DumpPairs(NULL, NULL, NULL, NULL);
		first_time = false;
	}

	for(vector<GameObject const*>::const_iterator it = objsForRender.begin(); 
		it != objsForRender.end();
		++it)
	{
		//assert((*it)->graph_handle != GameObject::INVALID_GRAPH_HANDLE);
		if ((*it)->graph_handle == GameObject::INVALID_GRAPH_HANDLE) continue;
		if ((*it)->GetDrawAABB(tmp_aabb))
		{
			graphSap.UpdateObject((*it)->graph_handle, GetASAP_AABB( tmp_aabb ));
		}
	}
	cleanObjsForRender();

	{
		graphSap.UpdateObject(cam_handle, cam);

		Opcode::ASAP_Pair* pairs;
		UINT pairsCount = graphSap.DumpPairs(NULL, NULL, NULL, &pairs);

#ifdef SELECTIVE_RENDERING_SORTING
		IceCore::RadixSort r;
		static std::vector<UINT> temp_order;
		temp_order.resize(pairsCount);
		for (UINT i = 0; i < pairsCount; ++i)
			temp_order[i] = pairs[i].id1;
		
		const udword* sorted = NULL;
		if (pairsCount > 0)
			sorted = r.Sort(&temp_order.front(), pairsCount, false).GetRanks();

		for (UINT i = 0; i < pairsCount; ++i)
		{
			Opcode::ASAP_Pair& pi = pairs[sorted[i]];

#else // !SELECTIVE_RENDERING_SORTING
		for (UINT i = 0; i < pairsCount; ++i)
		{
			Opcode::ASAP_Pair& pi = pairs[i];
#endif // SELECTIVE_RENDERING_SORTING
			assert(pi.id0 == 0);
			assert(pi.id1 != 0);
			
			// ���� �� ���������� �������� � ����� ������ ������, ��� ��� ����� ����������� ���
			GameObject* obj = (GameObject*)( (&cam_handle == pi.object0) ? pi.object1 : pi.object0);

			assert(obj);
			assert(obj->id == pi.id1);

			obj->Draw();
			if ( obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic() )
				((ObjDynamic*)obj)->DropShadow();
		}
	}


#if defined(DEBUG_DRAW_SPRITES_BORDERS_SEL_RD)

	Opcode::ASAP_Box const* boxes = graphSap.GetBoxes();
	udword nb_boxes = graphSap.GetNbUsedBoxes();
	Opcode::ASAP_EndPoint const* x_ax = graphSap.GetEndPoints(0);
	Opcode::ASAP_EndPoint const* y_ax = graphSap.GetEndPoints(1);

	for (udword i = 0; i < nb_boxes; ++i)
	{
		Opcode::ASAP_Box const& box = boxes[i];
		
		float l = x_ax[box.mMin[0]].mValue;
		float r = x_ax[box.mMax[0]].mValue;
		float t = y_ax[box.mMin[1]].mValue;
		float b = y_ax[box.mMax[1]].mValue;

		RenderBox(l, t, 1.f, r-l, b-t, RGBAf(DEBUG_SPRITES_BORDERS_SEL_RD_COLOR));
	}

#endif // DEBUG_DRAW_SPRITES_BORDERS_SEL_RD
#endif // SELECTIVE_RENDERING
}

#ifdef SELECTIVE_RENDERING

void selectForRenederUpdate(GameObject const* obj)
{
	assert(obj);
	UINT const & id = obj->id;

	// This object is not properly created yet, it has no ID
	if (id == 0)
		return;

	if (inObjsForRender.size() <= id ||
		inObjsForRender[id] == INOFR_INV_ID)
	{
		objsForRender.push_back(obj);

		if (inObjsForRender.size() <= id)
			inObjsForRender.resize(IceCore::NextPowerOfTwo(id+1), INOFR_INV_ID);

		inObjsForRender[id] = objsForRender.size() - 1;
	}
}

void cleanObjsForRender()
{
	if (!objsForRender.empty())
	{
		objsForRender.clear();
		std::fill(inObjsForRender.begin(), inObjsForRender.end(), INOFR_INV_ID);
	}
}
#endif
