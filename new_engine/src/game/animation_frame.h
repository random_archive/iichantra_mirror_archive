#ifndef __ANIMATION_FRAME_H_
#define  __ANIMATION_FRAME_H_


enum AnimationFrameCommand
{
	afcNone, afcRealX, afcRealY, afcRealW, afcRealH,
	afcInitW, afcInitH, afcInitWH,
	afcShoot, afcShootX, afcShootY, afcShootDir, afcShootBeh,
	afcLoop, afcMenuLoop, afcReloadTime,
	afcSetDamageMult, afcStartDying,
	afcJumpIfCloseToCamera, afcJumpIfCloseToCameraLeft, afcJumpIfCloseToCameraRight, afcJumpIfCloseToCameraUp, afcJumpIfCloseToCameraDown,
	afcJumpIfObjectExists,
	afcSetAnim, afcSetAnimIfWeaponNotReady, afcSetAnimIfGunDirection, afcSetWeaponAngle, afcSetTrajectory, afcCallAnim,
	afcPushString, afcPushInt, afcPushRandomInt,
	afcJump, afcJumpIfYSpeedGreater, afcJumpIfXSpeedGreater, afcJumpIfWeaponReady, afcJumpRandom,
	afcJumpIfOnPlane, afcJumpIfIntEquals, afcLocalJumpIfIntEquals, afcLocalJumpIfNextIntEquals, afcJumpCheckFOV,
	afcJumpIfPlayerId,
	afcJumpIfXLess, afcJumpIfXGreater, afcJumpIfYLess, afcJumpIfYGreater,
	afcRecover, afcRemoveRecoveryState, afcCreateRecoveryState, afcMirror, afcWait,
	afcCreateObject, afcCreateEnemy, afcDestroyObject, afcCreateParticles, afcCreateItem, afcCreateEffect, afcSummonObject,
	afcMountPointSet, afcSetSolid, afcSetSolidTo,
	afcSetHealth, afcSetGravity, afcSetWaypoint,
	afcWaitForTarget, afcWaitForEnemy, afcMoveToTarget, afcMoveToTargetX,	afcMoveToTargetY, afcJumpIfTargetClose, afcFaceTarget, afcClearTarget, afcAdjustVelToTarget,
	afcFlyToWaypoint, afcTeleportToWaypoint, afcSetNearestWaypoint,
	afcJumpIfTargetY, afcJumpIfTargetX, afcJumpIfTargetCloseByX, afcJumpIfTargetCloseByY, afcJumpIfStackIsNotEmpty,
	afcJumpIfWaypointClose,
	afcAdjustAim,
	afcCreateEnemyBullet, afcAimedShot, afcAngledShot, afcCreateEnemyRay, afcRandomAngledSpeed, afcCustomBullet,
	afcPlaySound,
	afcJumpIfSquashCondition, afcBounceObject, afcSetAnimOnTargetPos, afcPushObject,
	afcGiveHealth, afcGiveAmmo, afcDamage, afcSetInvincible, afcReduceHealth, afcGiveWeapon,
	afcSetShielding, afcSetLifetime,
	afcDealDamage, afcDrop,
	afcAdjustX, afcAdjustY, afcSetRelativePos, afcAdjustHomingAcc,
	afcSetAccY, afcSetVelY, afcSetAccX, afcAdjustAccY, afcSetVelX, afcSetRelativeVelX, afcSetRelativeAccX, afcStop, afcSetMaxVelX, afcSetMaxVelY, afcSetZ,
	afcMapVarFieldAdd, afcEnemyClean, afcSetOwnVelX, afcSetOwnVelY,
	afcCallFunction, afcCallFunctionWithStackParameter, afcSetProcessor,
	afcSetTouchable, afcSetInvisible, afcSetBulletCollidable,
	afcRandomOverlayColor, afcControlledOverlayColor, afcSetColor,
	afcPop, afcStopMorphing, afcSetShadow, afcReplaceWithRandomTile,
	afcEnvSound, afcEnvSprite, afcMaterialSound, afcMaterialSprite,
	afcBreakpoint
};

class AnimationFrame
{
public:
	UINT duration;
	UINT num;
	UINT polygon;
	
	int param;
	char* txt_param;

	AnimationFrameCommand command;
	bool is_param_function;

	AnimationFrame();
	AnimationFrame(const AnimationFrame& src);
	AnimationFrame& operator=(const AnimationFrame& src);

	~AnimationFrame();

private:

	void ReleaseResources();
};



#endif // __ANIMATION_FRAME_H_
