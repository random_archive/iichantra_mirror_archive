#ifndef __CAMERA_H_
#define __CAMERA_H_

class GameObject;

// ����� �������, �� ������� ����� �������������� ������
enum CameraFocusObjectPoint { 
	CamFocusCenter,					// �����
	CamFocusBottomCenter,			// �������� ������� ����
	CamFocusTopCenter,				// �������� �������� ����
	CamFocusLeftCenter,				// �������� ������ ����
	CamFocusRightCenter,			// �������� ������� ����
	CamFocusLeftTopCorner,			// ����� ������� ����
	CamFocusLeftBottomCorner,		// ����� ������ ����
	CamFocusRightTopCorner,			// ������ ������� ����
	CamFocusRightBottomCorner		// ������ ������ ����
};

void CameraMoveToPos(float x, float y, bool use_bounds = true);

void CameraUpdatePosition();
void CameraUpdateFocus();

void SetCameraAttachedObjectOffset(float x, float y);
void CameraAttachToAxis(bool x, bool y);
void CameraAttachToObject(GameObject* obj);
void CameraUseBounds(bool b);
void CameraSetBounds(float left, float right, float top, float bottom);
GameObject* GetCameraAttachedObject();
void GetCameraAttachedFocusShift(float& x, float& y);
void CameraSetLag( float lag );

void CameraSetScale(float x, float y);
void CameraSetAngle(float angle);
bool _CameraUsesTransformations();
void _CameraCalcTransformedBounds();
void _CameraApplyScale();
void _CameraApplyAngle();
void StoreCamera();
void RestoreCamera();
void RestoreCameraEdges();

#endif // __CAMERA_H_
