#include "StdAfx.h"
#include "animation.h"
#include "animation_frame.h"
#include "../script/script.h"
#include "../misc.h"


AnimationFrame::AnimationFrame() :
	duration(0),
	num(0),
	polygon(0),
	param(0),
	txt_param(NULL),
	command(afcNone),
	is_param_function(false)
{
}

AnimationFrame::AnimationFrame(const AnimationFrame& src) : 
	duration(src.duration), 
	num(src.num), 
	polygon(src.polygon),
	param(src.param),
	txt_param(StrDupl(src.txt_param)),
	command(src.command),
	is_param_function(src.is_param_function)
{
	if (is_param_function)
	{
		SCRIPT::ReserveProc(static_cast<LuaRegRef>(this->param));
	}
}

AnimationFrame& AnimationFrame::operator=(const AnimationFrame& src)
{
	ReleaseResources();

	duration          = src.duration;
	num               = src.num;
	polygon           = src.polygon;
	param             = src.param;
	txt_param         = StrDupl(src.txt_param);
	command           = src.command;
	is_param_function = src.is_param_function;

	if (is_param_function)
	{
		SCRIPT::ReserveProc(static_cast<LuaRegRef>(this->param));
	}

	return *this;
}

AnimationFrame::~AnimationFrame()
{
	ReleaseResources();
}

void AnimationFrame::ReleaseResources()
{
	if (is_param_function)
	{
		SCRIPT::ReleaseProc(static_cast<LuaRegRef*>(&this->param));
	}
	DELETEARRAY(txt_param);
}



Animation::Animation(const Animation& src)
	: name(StrDupl(src.name)), frameCount(src.frameCount),  
	current(MAX_ANIMFRAMES_COUNT)
{
	frames = new AnimationFrame[frameCount];
	for (size_t i = 0; i < frameCount; i++)
	{
		frames[i] = src.frames[i];
	}
}

Animation& Animation::operator=(const Animation& src)
{
	current = src.current;
	frameCount = src.frameCount;
	DELETEARRAY(name);
	name = StrDupl(src.name);
	if ( frames )
		DELETEARRAY(frames);
	frames = new AnimationFrame[frameCount];
	for (size_t i = 0; i < frameCount; i++)
	{
		frames[i] = src.frames[i];
	}
	return *this;
}
