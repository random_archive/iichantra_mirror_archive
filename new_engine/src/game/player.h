#ifndef __PLAYER_H_
#define  __PLAYER_H_

#include "objects/object_player.h"
#include "../config.h"
#include "../input_mgr.h"

// �����, �������� ������ �� ������� ������ � ����������� ���
class Player
{
public:
	
	std::vector<std::pair<ObjPlayer*,std::string> >		character_list;	// ������ ��������� ���������� � �� �����������
	ObjPlayer* current;		// ������� �����
	int current_num;		// ������� �����
	
	Input* input;
	
	Vector2 revivePoint;	// �����, � ������� ����� ����� �������

	bool change_blocked;	// ���������� ������������ ����� ��������

	LuaRegRef onChangePlayerProcessor;	    // ���-���������� ����� ������
	LuaRegRef onPlayerDeathProcessor;		// ���-���������� ������ ������

	Player()
	{
		current = NULL;
		current_num = 0;
		input = NULL;

		change_blocked = false;
		onChangePlayerProcessor = LUA_NOREF;
		onPlayerDeathProcessor = LUA_NOREF;

	}

	~Player();

	void Update();
	void Changer( bool forced, bool immediate );

	void BlockChange();
	void UnblockChange();

	void OnDying(ObjPlayer* plr);

	ObjPlayer* Create(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust = true);

	ObjPlayer* Revive(size_t num, const char* start_anim);
	
	ObjPlayer* GetByNum(size_t num);
	
};


#endif // __PLAYER_H_
