#include "StdAfx.h"
#include "net.h"
#include "game.h"
//#include "objects/object_remote_player.h"
#include "net.h"
#include "highscores.h"
#include "math.h"
#include "object_mgr.h"
#include "objects/object_player.h"
#include "objects/object_bullet.h"
#include "player.h"
#include "../scene.h"
#include "../script/script.h"
#include "../render/font.h"
#include <vector>
#ifdef __APPLE__
// TODO: SDL_net ���������� ��� framework. ���������, ��� ��� ����� � �������� ����������.
	#include "SDL_net.h"
#else
	#include "SDL/SDL_net.h"
#endif

extern bool netgame;
extern vector<Player*> players;
extern UINT current_time;
extern InputMgr inpmgr;
extern UINT last_id;
extern config cfg;

namespace Net
{
	struct NetPacket
	{
		NetPacketPriority priority;
		IPaddress address;
		Uint8* data;
		uint32_t last_sent;
		uint32_t id;
		uint32_t len;
		
		NetPacket( UDPpacket* packet, NetPacketPriority priority ) : priority( priority ), address( packet->address ), last_sent( 0 ), len( packet->len )
		{ 
			data = new Uint8[ packet->len+1 ];
			memcpy( data, packet->data, packet->len );
			id = SDLNet_Read32( packet->data+4 ); 
		}
		
		~NetPacket()
		{
			if ( data ) DELETEARRAY( data );
		}
	};
	
	struct NetPlayer
	{
		IPaddress address;
		Player* player;
		uint32_t last_packet_id;
		map<uint32_t, NetPacket*> outbound_packets;
		
		bool send_data;
		uint32_t ack_last;
		uint16_t ack_mask;
		
		uint32_t last_packet_sent;
		uint32_t last_packet_received;
		uint32_t packet_send_delay;
		
		uint32_t ping;
		uint32_t last_ping_sent;
		uint32_t last_ping_recieved;
		map<uint32_t, uint32_t> pings;	
		uint32_t last_flow_change;
		
		char* name;
		RGBAf color;
		vector<LocalEvent*> unreported_events;
		
		NetPlayer( IPaddress addr, Player* pl ) : player(pl), last_packet_id(0), send_data(false), ack_last(0), ack_mask(0),
		last_packet_sent(0), last_packet_received(0), packet_send_delay( 1000 / 30 ), ping(0),
		last_ping_sent(0), last_ping_recieved(0), last_flow_change(0), name(NULL), color( 1, 1, 1, 1 )
		{ 
			address.host = addr.host;
			address.port = addr.port;
		}
		
		~NetPlayer()
		{
			if ( name ) DELETEARRAY( name );
			for ( map<uint32_t, NetPacket*>::iterator iter = outbound_packets.begin(); iter != outbound_packets.end(); iter++ )
			{
				DELETESINGLE( iter->second );
			}
		}
	};
	
	class IDBatch
	{
	public:
		uint32_t getNext();
		bool dead;
		IDBatch( uint32_t start_id, uint32_t length ) : dead(false), id(start_id), start(start_id), end(start_id+length-1), half_full(false)
		{ }
		
	protected:
		
		uint32_t id;
		uint32_t start;
		uint32_t end;
		bool half_full;
	};
	
	bool server;
	
	uint32_t last_packet_id;
	uint32_t packet_send_delay = 1000 / 30;
	
	UDPsocket udp_socket;
	UDPpacket* packet;
	UDPpacket* answer_packet;
	IPaddress srvadd;
	
	vector<NetPlayer> clntadd;
	vector<IDBatch> allocated_ids;
	size_t local_player;
	
	uint32_t next_unused_id;
	uint8_t next_unused_player;
	
	bool new_ids_needed = false;
	
	//Queues a packet to be sent to a remote player, possibly many times
	void QueuePacket( NetPlayer* pl, UDPpacket* p, NetPacketPriority priority )
	{
		pl->outbound_packets[SDLNet_Read32( p->data+4 )] = new NetPacket( p, priority );
	}
	
	//Send a packet to a remote player
	void SendPacket( UDPpacket* p )
	{
		SDLNet_UDP_Send( udp_socket, -1, p );
	}


//-------------------------------------------------------------------------------------
// 			WRITING PACKET DATA
//-------------------------------------------------------------------------------------

	void WriteString( UDPpacket* p, char* str, bool specify_length = false )
	{
		int sz = strlen( str );
		if ( specify_length )
		{
			SDLNet_Write32( sz, p->data+p->len );
			p->len += 4;
		}
		memcpy( p->data+p->len, str, sz );
		p->data[p->len + sz + 1] = '\0';
		p->len += sz + 1;
	}
	
	void WriteUINT( UDPpacket* p, UINT u )
	{
		SDLNet_Write32( u, p->data+p->len );
		p->len += 4;
	}
	
	void WriteInt( UDPpacket* p, int i )
	{
		SDLNet_Write32( i, p->data+p->len );
		p->len += 4;
	}
	
	void WriteFloat( UDPpacket* p, float f )
	{
		uint32_t tmp;
		memcpy( &tmp, &f, min( sizeof( float ), sizeof( uint32_t ) ));
		SDLNet_Write32( tmp, p->data+p->len );
		p->len += 4;
	}
	
	void WriteVector2( UDPpacket* p, Vector2 v )
	{
		WriteFloat( p, v.x );
		WriteFloat( p, v.y );
	}
	
	void WriteByte( UDPpacket* p, unsigned char b )
	{
		*(p->data+(p->len++)) = b;
	}
	
	void WriteShort( UDPpacket* p, uint16_t s )
	{
		SDLNet_Write16( s, p->data+p->len );
		p->len += 2;
	}
	
	void SerializeDynObject( ObjDynamic* obj, UDPpacket* p )
	{
		WriteVector2( p, obj->vel );
		WriteVector2( p, obj->acc );
		WriteVector2( p, obj->own_vel );
		WriteFloat( p, obj->max_x_vel );
		WriteFloat( p, obj->max_y_vel );
	}
	
	void SerializePhysObject( ObjPhysic* obj, UDPpacket* p )
	{
		WriteByte(	p, obj->physFlags );
		WriteByte(	p, obj->solid_to );
		if ( obj->IsDynamic() ) SerializeDynObject( static_cast<ObjDynamic*>(obj), p );
	}
	
	void SerializeInput( Input* input, UDPpacket* p )
	{
		uint32_t input_int = 0;
		uint32_t mask = 1;
		for ( int i = 0; i < cakLastKey; i++ )
		{
			if ( input->held[i] ) input_int |= mask;
			mask = mask << 1;
		}
		WriteUINT( p, input_int );
		input_int = 0;
		mask = 1;
		for ( int i = 0; i < cakLastKey; i++ )
		{
			if ( input->released[i] ) input_int |= mask;
			mask = mask << 1;
		}
		WriteUINT( p, input_int );
	}
	
	//Writes object state into a given net packet. Probably should be inside the object itself,
	//but requires SDLNet_Write32, so I just follow the precedent - PushObject in script API.
	void SerializeObject( GameObject* obj, UDPpacket* p )
	{
		WriteUINT(	p, obj->id		);
		WriteInt(	p, obj->type	);
		
		WriteVector2( p, obj->aabb.p );
		WriteFloat( p, obj->aabb.W	);
		WriteFloat( p, obj->aabb.H	);
		
		WriteByte(	p, obj->objFlags );
		
		if ( obj->sprite )
		{
			WriteUINT( p, obj->sprite->cur_anim_num );
			WriteUINT( p, obj->sprite->getCurrentFrame() );
			
			WriteInt( p, obj->sprite->getRealX() );
			WriteInt( p, obj->sprite->getRealY() );
			WriteUINT( p, internal_time-obj->sprite->prevAnimTick );
			if ( obj->sprite->cur_anim_num < obj->sprite->animsCount )
				WriteUINT( p, obj->sprite->anims[ obj->sprite->cur_anim_num ].current );
			WriteFloat( p, obj->sprite->getAngle() );
			WriteByte( p, obj->sprite->flags );
		}
		
		if ( obj->IsPhysic() )
			SerializePhysObject( static_cast<ObjPhysic*>(obj), p );
	}
	
	uint32_t WritePacketHeader( NetPlayer* player, NetEventType ptype, NetPacketPriority priority, UDPpacket* p )
	{
		p->len = 0;
		WriteInt( p, priority );			//+0
		WriteUINT( p, last_packet_id++ );	//+4
		WriteUINT( p, player->ack_last );	//+8
		WriteShort( p, player->ack_mask );	//+12
		WriteInt( p, ptype );				//+14
		return last_packet_id - 1;
	}

//-------------------------------------------------------------------------------------
// 			READING PACKET DATA
//-------------------------------------------------------------------------------------

	char* ReadString( UDPpacket* p, uint32_t& iter )
	{
		int size = SDLNet_Read32( p->data+iter );
		iter += 4;
		char* ret = new char[size + 1];
		memcpy( ret, p->data+iter, size );
		*(ret+size) = '\0';
		iter += size+1;
		return ret;
	}
	
	UINT ReadUINT( UDPpacket* p, uint32_t& iter )
	{
		UINT ret = SDLNet_Read32( p->data+iter );
		iter += 4;
		return ret;
	}
	
	int ReadInt( UDPpacket* p, uint32_t& iter )
	{
		int ret = SDLNet_Read32( p->data+iter );
		iter += 4;
		return ret;
	}
	
	float ReadFloat( UDPpacket* p, uint32_t& iter )
	{
		uint32_t val = SDLNet_Read32( p->data+iter );
		float ret = 0.0f;
		memcpy( &ret, &val, sizeof( float ) );
		iter += 4;
		return ret;
	}
	
	Vector2 ReadVector2( UDPpacket* p, uint32_t& iter )
	{
		Vector2 ret = Vector2();
		ret.x = ReadFloat( p, iter );
		ret.y = ReadFloat( p, iter );		
		return ret;
	}
	
	unsigned char ReadByte( UDPpacket* p, uint32_t& iter )
	{
		return *(p->data+(iter++));
	}
	
	uint16_t ReadShort( UDPpacket* p, uint32_t& iter )
	{
		uint16_t ret = SDLNet_Read16( p->data+iter );
		iter += 2;
		return ret;
	}
	
	void DeserializeDynObject( ObjDynamic* obj, UDPpacket* p, uint32_t& iter )
	{
		obj->vel = ReadVector2( p, iter );
		obj->acc = ReadVector2( p, iter );
		obj->own_vel = ReadVector2( p, iter );
		obj->max_x_vel = ReadFloat( p, iter );
		obj->max_y_vel = ReadFloat( p, iter );
	}
	
	void DeserializePhysObject( ObjPhysic* obj, UDPpacket* p, uint32_t& iter )
	{
		obj->physFlags = ReadByte( p, iter );
		obj->solid_to = ReadByte( p, iter );
		if ( obj->IsDynamic() ) DeserializeDynObject( static_cast<ObjDynamic*>(obj), p, iter );
	}
	
	void DeserializeObject( UDPpacket* p, uint32_t& iter, bool compensate_prediction = true )
	{
		UINT oid = ReadUINT( p, iter );
		GameObject* obj = GetGameObject( oid );
		if ( !obj ) return;
		
		obj->type = static_cast<ObjectType>(ReadInt( p, iter ));
		if ( compensate_prediction )
		{
			obj->net_update = true;
			obj->compensation_p = ReadVector2( p, iter );
		}
		else
		{
			obj->aabb.p = ReadVector2( p, iter );
		}
		obj->aabb.W	= ReadFloat( p, iter );
		obj->aabb.H	= ReadFloat( p, iter );
		
		obj->objFlags = ReadByte( p, iter );
		
		if ( obj->sprite )
		{
			obj->sprite->cur_anim_num = ReadUINT( p, iter );
			obj->sprite->setCurrentFrame(ReadUINT( p, iter ));
			obj->sprite->setRealX(static_cast<short>(ReadInt( p, iter )));
			obj->sprite->setRealY(static_cast<short>(ReadInt( p, iter )));
			obj->sprite->prevAnimTick = internal_time - ReadUINT( p, iter );
			if ( obj->sprite->cur_anim_num < obj->sprite->animsCount )
				obj->sprite->anims[ obj->sprite->cur_anim_num ].current = static_cast<USHORT>(ReadUINT( p, iter ));
			obj->sprite->setAngle(ReadFloat( p, iter ));
			obj->sprite->flags = ReadByte( p, iter );
		}
		
		if ( obj->IsPhysic() )
			DeserializePhysObject( static_cast<ObjPhysic*>(obj), p, iter );
	}
	
	void DeserializeInput( Input* input, UDPpacket* p, uint32_t& iter )
	{
		uint32_t input_int = ReadUINT( p, iter );
		uint32_t mask = 1;
		for ( int i = 0; i < cakLastKey; i++ )
		{
			input->held[i] = (input_int & mask) != 0;
			mask = mask << 1;
		}
		input_int = ReadUINT( p, iter );
		mask = 1;
		for ( int i = 0; i < cakLastKey; i++ )
		{
			input->released[i] = (input_int & mask) != 0;
			mask = mask << 1;
		}
	}
	
	void ReadPacketHeader( UDPpacket* p, uint32_t& iter, NetPacketPriority& priority, uint32_t& id, uint32_t& ack_last, uint16_t& ack_mask, NetEventType& etype )
	{
		priority = static_cast<NetPacketPriority>(ReadInt(p, iter));
		id = ReadUINT(p, iter);
		ack_last = ReadUINT(p, iter);
		ack_mask = ReadShort(p, iter);
		etype = static_cast<NetEventType>(ReadInt(p, iter));
	}
	
	bool ProcessPacketHeader( UDPpacket* p, NetPlayer* pl, uint32_t& iter, uint32_t& last_id, NetEventType& event_type )
	{
		NetPacketPriority priority;
		uint32_t id;
		uint32_t r_ack_last;
		uint16_t r_ack_mask;
		ReadPacketHeader( p, iter, priority, id, r_ack_last, r_ack_mask, event_type );
		//ids wrap around
		if ( id < last_id && priority == nppNormal && !( numeric_limits<uint32_t>::max() - id < 100 && id < 100 ) )
			return true;
		for ( uint16_t i = 0; i < min(16u, r_ack_last); i++ )
		{
			if ( ((r_ack_mask & 1) != 0) && pl->outbound_packets[ r_ack_last ] )
			{
				DELETESINGLE( pl->outbound_packets[ r_ack_last ] );
				pl->outbound_packets.erase( r_ack_last );
			}
			r_ack_last--;
			r_ack_mask = r_ack_mask >> 1;
		}
		if ( priority > nppNormal )
		{
			pl->ack_mask = (pl->ack_mask << abs(static_cast<long>(id - pl->ack_last))) | 1;
			pl->ack_last = id;
		}
		last_id = id;
		
		if ( event_type == pingPacket )
		{
			pl->last_ping_recieved = current_time;
			answer_packet->address.host = pl->address.host;
			answer_packet->address.port = pl->address.port;
			WritePacketHeader( pl, pongPacket, nppNormal, answer_packet );
			WriteUINT( answer_packet, id );
			//QueuePacket( pl, answer_packet, nppNormal );
			SendPacket( answer_packet );
			
			return true;
		}
		if ( event_type == pongPacket )
		{
			uint32_t p_id = ReadUINT( p, iter);
			uint32_t time = pl->pings[p_id];
			if ( time == 0 )
				return true;
			pl->ping += ( current_time - time - pl->ping ) / 10;
			pl->pings.erase( p_id );
			return true;
		}
		
		return false;
	}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

	bool Init()
	{
		if ( SDLNet_Init() == -1 )
		{
			Log(DEFAULT_NET_LOG_NAME, logLevelError, "SDL_net initialization failed: %s", SDLNet_GetError());
			return false;
		}
		
		if ( (packet = SDLNet_AllocPacket(512)) == NULL || (answer_packet = SDLNet_AllocPacket(512)) == NULL )
		{
			Log(DEFAULT_NET_LOG_NAME, logLevelError, "SDLNet_AllocPacket failed: %s", SDLNet_GetError());
			return false;
		}
		
		local_player = 0;
		
		return true;
	}
	
	bool StartServer( uint16_t port )
	{
		if ( (udp_socket = SDLNet_UDP_Open(port)) == NULL )
		{
			Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::StartServer: SDLNet_UDP_Open failed: %s\n", SDLNet_GetError());
			return false;
		}
		clntadd.clear();
		last_packet_id = 0;
		next_unused_player = 1;
		next_unused_id = last_id + 1;
		RequestNewIDs();
		netgame = true;
		server = true;
		
		clntadd.push_back( NetPlayer( srvadd, players[0] ) );
		for ( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
		{
			(*iter)->input->clear();
			inpmgr.UnregisterInput( (*iter)->input );
		}
		inpmgr.RegisterInput( players[0]->input, 0 );
		clntadd[0].name = StrDupl( cfg.nick );
		clntadd[0].color = RGBAf( cfg.color_r, cfg.color_g, cfg.color_b );
		clntadd[0].send_data = false;
		
		return true;
	}
	
	bool Connect( const char* host, uint16_t port )
	{
		if ( (udp_socket = SDLNet_UDP_Open(0)) == NULL )
		{
			Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect: SDLNet_UDP_Open failed: %s\n", SDLNet_GetError());
			return false;
		}
		if (SDLNet_ResolveHost(&srvadd, host, port) == -1)
		{
			Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect: SDLNet_ResolveHost(%s, %d) failed: %s\n", host, port, SDLNet_GetError());
			return false;
		}
		packet->address.host = srvadd.host;
		packet->address.port = srvadd.port;
		packet->len = 0;
		WriteString( packet, const_cast<char*>(NET_MSG_HELLO) );
		SDLNet_UDP_Send( udp_socket, -1, packet );
		
		uint32_t connection_time = SDL_GetTicks();
		uint32_t answer = 0;
		while ( SDL_GetTicks() - connection_time < 3000 && (answer = SDLNet_UDP_Recv( udp_socket, packet )) == 0 )
		{ }
		
		if ( answer )
		{
			last_packet_id = 0;
			if ( strcmp( reinterpret_cast<char*>(packet->data), NET_MSG_ACK ) >= 0 ) //Why are they not equal?
			{
				connection_time = SDL_GetTicks();
				answer = 0;
				while ( SDL_GetTicks() - connection_time < 3000 && (answer = SDLNet_UDP_Recv( udp_socket, packet )) == 0 )
				{ }
				
				if ( answer )
				{
					uint32_t p_iter = 0;
					NetEventType event;
					NetPlayer srv_player = NetPlayer( packet->address, players[0] );
					srv_player.send_data = true;
					clntadd.clear();
					clntadd.push_back( srv_player );
					ProcessPacketHeader( packet, &clntadd[0], p_iter, clntadd[0].last_packet_id, event );
					if ( event == playerInfo )
					{
						int pl = ReadInt( packet, p_iter );
						if ( pl < 0 )
						{
							Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect rejected by server: server is full." );
							return false;
						}
						else
						{
							for ( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
							{
								(*iter)->input->clear();
								inpmgr.UnregisterInput((*iter)->input );
							}
							Log(DEFAULT_NET_LOG_NAME, logLevelWarning, "Connected as player %i.", pl+1 );
							local_player = pl;
							while ( clntadd.size() <= static_cast<uint32_t>(pl) ) clntadd.push_back( NetPlayer(srvadd, players[0]) );
							clntadd[pl].player = players[pl];
							players[pl]->current->local = true;
							clntadd[pl].name = StrDupl( cfg.nick );
							clntadd[pl].color = RGBAf( cfg.color_r, cfg.color_g, cfg.color_b );
							clntadd[pl].send_data = false;
							inpmgr.RegisterInput( players[pl]->input, 0 );
							WritePacketHeader( &clntadd[0], playerInfo, nppImportant, packet );
							WriteByte( packet, static_cast<unsigned char>(pl) );
							WriteString( packet, cfg.nick, true );
							WriteFloat( packet, cfg.color_r );
							WriteFloat( packet, cfg.color_g );
							WriteFloat( packet, cfg.color_b );
							QueuePacket( &clntadd[0], packet, nppImportant );
						}
					}
					else
					{
						Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect: Server returned an unexpected message (%i instead of %i) instead of player info. Are you sure the game is running there?", event, playerInfo );
						return false;	
					}
				}
				else
				{
					Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect: Server decided not to give us any info about open player slots (this doesn't mean it was full)." );
					return false;	
				}
				
				return (netgame = true);
			}
			
			Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect: Server returned an unexpected message. Are you sure the game is running there?" );		
			return false;
		}
		
		Log(DEFAULT_NET_LOG_NAME, logLevelError, "Net::Connect: Server never called back. It probably doesn't like us that much." );
		
		return false;
	}
	
	char* GetError()
	{
		return SDLNet_GetError();
	}
	
	GameObject* RemoteObjectCreation( UDPpacket* pck, uint32_t& iter )
	{
		GameObject* ret = NULL;
		ObjectType ot = static_cast<ObjectType>(ReadInt(pck, iter) );
		switch ( ot )
		{
			case objBullet:
				{
					char* proto = ReadString( pck, iter );
					Vector2 coord = ReadVector2( pck, iter );
					GameObject* shooter = GetGameObject( ReadUINT( pck, iter ) );
					WeaponDirection wd = static_cast<WeaponDirection>( ReadInt( pck, iter ) );
					UINT id = ReadUINT( pck, iter );
					if ( shooter )
						ret = CreateBullet( proto, coord, static_cast<ObjCharacter*>(shooter), wd, id );
					DELETEARRAY( proto ); 
					break;
				}
			default:
				{
					break;
				}
		}
		return ret;
	}
	
	void ProcessPlayerPacket( NetPlayer& pl, UDPpacket* pck )
	{
		uint32_t iter = 0;
		NetEventType type;
		if ( ProcessPacketHeader( pck, &pl, iter, pl.last_packet_id, type ) ) return; 
		switch( type )
		{
			case inputUpdated:
				{
					DeserializeInput( pl.player->input, pck, iter );
					return;
				}
			case playerUpdated:
				{
					DeserializeInput( pl.player->input, pck, iter );
					DeserializeObject( pck, iter );
					return;
				}
			case objectCreated:
				{
					GameObject* obj = RemoteObjectCreation( pck, iter );
					if ( obj )
					{
						LocalEvent* l_event = new LocalEvent( eventBroadcast, NULL, NULL );
						l_event->packet = new uint8_t[ pck->len ];
						memcpy( l_event->packet, pck->data, pck->len );
						l_event->uint_param = pck->len;
						l_event->origin_player = &pl;
						AddEvent( l_event );
					}
					return;
				}
			case idBatchRequest:
				{
					WritePacketHeader( &pl, idBatchConfirmed, nppImportant, answer_packet );
					WriteUINT( answer_packet, next_unused_id );
					WriteUINT( answer_packet, NET_ID_BATCH_SIZE );
					next_unused_id += NET_ID_BATCH_SIZE;
					QueuePacket( &pl, answer_packet, nppImportant );
					return;
				}
			case objectUpdated:
				{
					DeserializeObject( pck, iter );
					return;
				}
			case playerInfo:
				{
					ReadByte( pck, iter );
					pl.name = ReadString( pck, iter );
					float c_r = ReadFloat( pck, iter );
					float c_g = ReadFloat( pck, iter );
					float c_b = ReadFloat( pck, iter );
					pl.color = RGBAf( c_r, c_g, c_b );
					
					LocalEvent* l_event = new LocalEvent( eventBroadcast, NULL, NULL );
					l_event->packet = new uint8_t[ pck->len ];
					memcpy( l_event->packet, pck->data, pck->len );
					l_event->uint_param = pck->len;
					l_event->origin_player = &pl;
					AddEvent( l_event );
					
					return;
				}
			case chatMessage:
				{
					ReadByte(pck, iter);
					char* message = ReadString(pck, iter);
					SCRIPT::OnChatMessage( pl.name, message );
					DELETEARRAY( message );
					
					LocalEvent* l_event = new LocalEvent( eventBroadcast, NULL, NULL );
					l_event->packet = new uint8_t[ pck->len ];
					memcpy( l_event->packet, pck->data, pck->len );
					l_event->uint_param = pck->len;
					l_event->origin_player = &pl;
					AddEvent( l_event );
					
					return;
				}
			default:
				{
					Log(DEFAULT_NET_LOG_NAME, logLevelWarning, "Recieved an unknown packet type from one of the clients." );
					return;
				}
		}
	}
	
	void ProcessServerPacket( UDPpacket* pck )
	{
		uint32_t iter = 0;
		NetEventType type;
		if ( ProcessPacketHeader( pck, &clntadd[0], iter, clntadd[0].last_packet_id, type ) ) return;
		switch( type )
		{
			case inputUpdated:
				{
					//DeserializeInput( players[3]->input, pck, iter ); //TODO: ???
					return;
				}
			case objectUpdated:
				{
					DeserializeObject( pck, iter );
					return;
				}
			case objectCreated:
				{
					RemoteObjectCreation( pck, iter );
					return;
				}
			case playerUpdated:
				{
					unsigned char pl_num = ReadByte( pck, iter );
					DeserializeInput( players[pl_num]->input, pck, iter );
					DeserializeObject( pck, iter );
					return;
				}
			case playerInfo:
				{
					unsigned char pl_num = ReadByte( pck, iter );
					while ( clntadd.size() <= pl_num ) clntadd.push_back( NetPlayer( clntadd[0].address, players[0] ) );
					clntadd[pl_num].player = players[pl_num];
					clntadd[pl_num].name = ReadString( pck, iter );
					float c_r = ReadFloat( pck, iter );
					float c_g = ReadFloat( pck, iter );
					float c_b = ReadFloat( pck, iter );
					clntadd[pl_num].color = RGBAf( c_r, c_g, c_b );
					return;
				}
			case idBatchConfirmed:
				{
					uint32_t start = ReadUINT(pck, iter);
					uint32_t size = ReadUINT(pck, iter);
					allocated_ids.push_back( IDBatch( start, size ) );
					return;
				}
			case chatMessage:
				{
					uint8_t pl = ReadByte(pck, iter);
					char* message = ReadString(pck, iter);
					SCRIPT::OnChatMessage( clntadd[pl].name, message );
					DELETEARRAY( message );
					return;
				}
			default:
				{
					Log(DEFAULT_NET_LOG_NAME, logLevelWarning, "Recieved an unknown packet type from the server." );
					return;
				}
		}
	}
	
	void SendWorldUpdate( NetPlayer& pl )
	{
		if ( !pl.send_data )
			return;
		unsigned char pl_num = 0;
		packet->address.host = pl.address.host;
		packet->address.port = pl.address.port;
		for( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
		{
			//Sending updates for any active player that is not this client
			if ( pl.player != (*iter) && (*iter)->current )
			{
				WritePacketHeader( &pl, playerUpdated, nppNormal, packet );
				WriteByte( packet, pl_num );
				SerializeInput( (*iter)->input, packet );
				SerializeObject( (*iter)->current, packet );
				QueuePacket( &pl, packet, nppNormal );
			}
			pl_num++;
		}
	}
	
	void SendPlayerUpdate()
	{
		WritePacketHeader( &clntadd[0], playerUpdated, nppNormal, packet );
		SerializeInput( players[local_player]->input, packet );
		SerializeObject( players[local_player]->current, packet );
		QueuePacket( &clntadd[0], packet, nppNormal );
	}
	
	void ReportEvent( UDPpacket* p, NetPacketPriority priority, NetPlayer* origin = NULL )
	{
		if ( server )
		{
			for ( vector<NetPlayer>::iterator iter = clntadd.begin(); iter != clntadd.end(); iter++ )
			{
				if ( iter->send_data && (!origin || &(*iter) != origin) )
				{
					uint32_t len = p->len;
					uint32_t p_iter = 0;
					NetPacketPriority t_priority;
					uint32_t t_ack_last;
					uint16_t t_ack_mask;
					uint32_t t_id;
					NetEventType etype;
					ReadPacketHeader( p, p_iter, t_priority, t_id, t_ack_last, t_ack_mask, etype );
					WritePacketHeader( &(*iter), etype, priority, p );
					p->len = len;
					QueuePacket( &(*iter), p, priority );
				}
			}
		}
		else
		{
			QueuePacket( &clntadd[0], p, priority );
		}
	}
	
	uint32_t FlowControlDelay( uint32_t ping )
	{
		if ( ping <= 250 )
			return 1000 / 30;
		if ( ping <= 500 )
			return 1000 / 15;
		return 1000 / 10;
	}
	
	void Update()
	{
		if ( !netgame ) return;
		
		//If we are the server then we should mark anyone except ourserlf as ready to receive data
		for ( vector<NetPlayer>::iterator iter = clntadd.begin(); iter != clntadd.end(); iter++ )
		{
			if ( iter->player != players[0] )
				iter->send_data = true;
		}
		
		
		//RECEIVING
		
		if ( server )
		{
			while ( SDLNet_UDP_Recv( udp_socket, packet ) )
			{
				bool existing_client = false;
				for ( vector<NetPlayer>::iterator iter = clntadd.begin(); iter != clntadd.end(); iter++ ) 
				{
					if ( iter->address.host == packet->address.host && iter->address.port == packet->address.port )
					{
						ProcessPlayerPacket( *iter, packet );
						existing_client = true;
						break;
					}
				}
				if ( !existing_client )
				{
					Log(DEFAULT_NET_LOG_NAME, logLevelWarning, "Client connected." );
					packet->len = 0;
					WriteString( packet, const_cast<char*>(NET_MSG_ACK) );
					SDLNet_UDP_Send( udp_socket, -1, packet );
					if ( next_unused_player >= players.size() )
					{
						packet->len = 0;
						WritePacketHeader( &clntadd[0], playerInfo, nppNormal, packet );
						WriteInt( packet, -1 );
						QueuePacket( &clntadd[0], packet, nppNormal );
					}
					else
					{
						NetPlayer new_player = NetPlayer( packet->address, players[next_unused_player] ); //TODO: find an open slot
						inpmgr.UnregisterInput( players[next_unused_player]->input );
						players[next_unused_player]->current->local = true;
						clntadd.push_back( new_player );
						packet->len = 0;
						WritePacketHeader( &(*clntadd.rbegin()), playerInfo, nppNormal, packet );
						WriteInt( packet, next_unused_player++ );
						//QueuePacket( &(*clntadd.rbegin()), packet, nppImportant );
						SendPacket( packet );
						WritePacketHeader( &(*clntadd.rbegin()), idBatchConfirmed, nppNormal, packet );
						WriteUINT( packet, next_unused_id );
						WriteUINT( packet, NET_ID_BATCH_SIZE );
						next_unused_id += NET_ID_BATCH_SIZE;
						
						//QueuePacket( &(*clntadd.rbegin()), packet, nppImportant );
						SendPacket( packet );
						for ( int i = 0; i < next_unused_player-1; i++ )
						{
							WritePacketHeader( &(*clntadd.rbegin()), playerInfo, nppImportant, packet );
							WriteByte( packet, static_cast<unsigned char>(i) );
							WriteString( packet, players[i]->current->name, true );
							WriteFloat( packet, players[i]->current->color.r );
							WriteFloat( packet, players[i]->current->color.g );
							WriteFloat( packet, players[i]->current->color.b );
							QueuePacket( &(*clntadd.rbegin()), packet, nppImportant );
						}
					}
				}
			}
			
		}
		else
		{
			while ( SDLNet_UDP_Recv( udp_socket, packet ) )
			{
				ProcessServerPacket( packet );
			}
		}
		
		//SENDING AND PROCESSING NETPLAYERS
		
		for ( vector<NetPlayer>::iterator npl = clntadd.begin(); npl != clntadd.end(); npl++ )
		{
			if ( npl->send_data == true )
			{
				//Basic flow control
				if ( current_time - npl->last_ping_sent > 500 )
				{
					packet->address.host = npl->address.host;
					packet->address.port = npl->address.port;
					npl->last_ping_sent = current_time;
					uint32_t ping_id = WritePacketHeader( &(*npl), pingPacket, nppNormal, packet );
					npl->pings[ping_id] = current_time;
					SendPacket( packet );
				}
				if ( npl->last_ping_recieved > 0 && current_time - npl->last_ping_recieved > 5000 )
				{
					Log(DEFAULT_NET_LOG_NAME, logLevelError, "Ping timeout for %s!", npl->name);
					npl->send_data = false;
				}
				uint32_t delay = FlowControlDelay( npl->ping );
				if ( current_time - npl->last_flow_change > 100 )
				{
					npl->last_flow_change = current_time;
					npl->packet_send_delay = delay;
				}
			}
			if ( npl->player->current && npl->player->current->name == NULL )
			{
				npl->player->current->name = StrDupl( npl->name );
				npl->player->current->color = RGBAf( npl->color.r, npl->color.g, npl->color.b );
			}
			if ( npl->send_data && current_time - npl->last_packet_sent > npl->packet_send_delay )
			{
				npl->last_packet_sent = current_time;
				
				packet->address.host = npl->address.host;
				packet->address.port = npl->address.port;
				
				for ( vector<LocalEvent*>::iterator e_iter = npl->unreported_events.begin(); e_iter != npl->unreported_events.end(); e_iter++ )
				{
					switch ( (*e_iter)->type )
					{
						case objectCreated:
							{
								WritePacketHeader( &(*npl), objectCreated, nppImportant, packet );
								WriteInt( packet, (*e_iter)->obj->type );
								switch( (*e_iter)->obj->type )
								{
									case objBullet:
										{
											WriteString( packet, (*e_iter)->proto, true );
											WriteVector2( packet, (*e_iter)->coord );
											WriteUINT( packet, (*e_iter)->obj2->id );
											WriteInt( packet, (*e_iter)->int_param );
											WriteUINT( packet, (*e_iter)->obj->id );
											break;
										}
									default:
										{
											break;
										}
								}
								DELETESINGLE( *e_iter );
								QueuePacket( &(*npl), packet, nppImportant ); 
								break;
							}
						case objectUpdated:
							{
								GameObject* obj;
								if ( (obj = GetGameObject( (*e_iter)->uint_param )) != NULL )
								{
									WritePacketHeader( &(*npl), objectUpdated, nppNormal, packet );
									SerializeObject( obj, packet );
									DELETESINGLE( *e_iter );
									QueuePacket( &(*npl), packet, nppNormal ); 
								}
								break;
							}
						case eventBroadcast:
							{
								if ( &(*npl) != (*e_iter)->origin_player )
								{
									memcpy( packet->data, (*e_iter)->packet, (*e_iter)->uint_param );
									uint32_t p_iter = 0;
									NetPacketPriority t_priority;
									uint32_t t_ack_last;
									uint16_t t_ack_mask;
									uint32_t t_id;
									NetEventType etype;
									ReadPacketHeader( packet, p_iter, t_priority, t_id, t_ack_last, t_ack_mask, etype );
									WritePacketHeader( &(*npl), etype, t_priority, packet );
									packet->len = (*e_iter)->uint_param;
									QueuePacket( &(*npl), packet, nppImportant );
								} 
								DELETESINGLE( *e_iter );
								break;
							}
						default:
							break;
					}
				}
				npl->unreported_events.clear();
				
				if ( server )
				{
					SendWorldUpdate( *npl );
				}
				else
				{
					SendPlayerUpdate();
				}
				
				bool erased;
				for ( map<uint32_t, NetPacket*>::iterator p_iter = npl->outbound_packets.begin(); p_iter != npl->outbound_packets.end(); )
				{
					erased = false;
					if ( p_iter->second && current_time - p_iter->second->last_sent >= NET_PACKET_RETRY_TIME )
					{
						p_iter->second->last_sent = current_time;
						packet->address.host = p_iter->second->address.host;
						packet->address.port = p_iter->second->address.port;
						memcpy( packet->data, p_iter->second->data, p_iter->second->len );
						SendPacket( packet );
						if ( p_iter->second->priority == nppNormal )
						{
							DELETESINGLE( p_iter->second );
							npl->outbound_packets.erase( p_iter++ );
							erased = true;
						}
					}
					if (!erased) ++p_iter;
				}
				
			}
			
		}
		
	}
	
	//Stop the game
	bool Stop()
	{
		clntadd.clear();
		allocated_ids.clear();
		netgame = false;
		return true;
	}
	
	//Completely destroy the network
	bool Close()
	{
		Stop();
		
		SDLNet_FreePacket( packet );
		SDLNet_FreePacket( answer_packet );
		packet = NULL;
		answer_packet = NULL;
		SDLNet_Quit();
		Highscores::Free();
		return true;
	}
	
	void RequestNewIDs()
	{
		if ( !netgame || server )
		{
			//Get some delicious ids immideately
			allocated_ids.push_back( IDBatch(next_unused_id, NET_ID_BATCH_SIZE ) );
			next_unused_id += NET_ID_BATCH_SIZE;
		} else
		{
			WritePacketHeader( &clntadd[0], idBatchRequest, nppImportant, packet );
			QueuePacket( &clntadd[0], packet, nppImportant );
		}
	}
	
	bool IsServer()
	{
		return netgame && server;
	}
	
	void AddEvent( LocalEvent* event )
	{
		if ( server )
		{
			for ( vector<NetPlayer>::iterator iter = clntadd.begin(); iter != clntadd.end(); iter++ )
			{
				iter->unreported_events.push_back( new LocalEvent( event ) );
			}
		}
		else clntadd[0].unreported_events.push_back( new LocalEvent( event ) );
		DELETESINGLE( event );
	}
	
	uint32_t GetNextID()
	{
		uint32_t id = 0;
		vector<IDBatch>::iterator iter = allocated_ids.begin();
		if ( iter != allocated_ids.end() && iter->dead )
		{
			allocated_ids.erase( iter );
			iter = allocated_ids.begin();
		}
		while ( iter != allocated_ids.end() && ( (id = iter->getNext()) == 0 ) )
			iter++;
		if ( new_ids_needed )
		{
			new_ids_needed = false;
			RequestNewIDs();
		}
		return id;
	}
	
	uint32_t IDBatch::getNext()
	{
		if (dead) return 0;
		if ( !half_full && ((half_full = (id > (start + end)/2)) == true ) )
			new_ids_needed = true;
			//RequestNewIDs();
		id++;
		dead = (id >= end);
		
		return id;
	}
	
	void ChatMessage( char* message )
	{
		if ( !netgame )
			return;
		
		if ( server )
		{
			SCRIPT::OnChatMessage( cfg.nick, message );
			WritePacketHeader( &clntadd[0], chatMessage, nppImportant, packet );
			WriteByte( packet, 0 );
			WriteString( packet, message, true );
			LocalEvent* l_event = new LocalEvent( eventBroadcast, NULL, NULL );
			l_event->packet = new uint8_t[ packet->len ];
			memcpy( l_event->packet, packet->data, packet->len );
			l_event->uint_param = packet->len;
			l_event->origin_player = &clntadd[0];
			AddEvent( l_event );
		}
		else
		{
			SCRIPT::OnChatMessage( players[local_player]->current->name, message );
			WritePacketHeader( &clntadd[0], chatMessage, nppImportant, packet );
			WriteByte( packet, static_cast<unsigned char>(local_player) );
			WriteString( packet, message, true );
			QueuePacket( &clntadd[0], packet, nppImportant );
		}
	}
	
} // Net
