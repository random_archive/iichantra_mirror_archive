#ifndef _OBJECT_PARTICLE_SYSTEM_H_
#define _OBJECT_PARTICLE_SYSTEM_H_

class GameObject;
class Vector2;
GameObject* CreateParticleSystem( const char* proto_name, Vector2 coord, UINT Emitter, int mode );

#endif // _OBJECT_PARTICLE_SYSTEM_H_