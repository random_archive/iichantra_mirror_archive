#include "StdAfx.h"

#include "object.h"
#include "object_dynamic.h"
#include "object_character.h"
#include "object_player.h"
#include "object_sprite.h"
#include "object_bullet.h"
#include "object_waypoint.h"
#include "object_item.h"
#include "object_enemy.h"
#include "object_effect.h"
#include "object_environment.h"
#include "object_particle_system.h"
#include "object_ray.h"

#include "../camera.h"
#include "../game.h"
#include "../editor.h"
#include "../object_mgr.h"
#include "../player.h"
#include "../net.h"

#include "../phys/phys_collisionsolver.h"
#include "../phys/phys_misc.h"

#include "../../config.h"
#include "../../input_mgr.h"
#include "../../misc.h"
#include "../../resource_mgr.h"

#include "../../render/renderer.h"

#include "../../script/api.h"
#include "../../script/udata.hpp"

#include "../../sound/snd.h"

//////////////////////////////////////////////////////////////////////////

extern float CAMERA_X;
extern float CAMERA_Y;

extern config cfg;
extern InputMgr inpmgr;
extern GameObject* Waypoints;
extern lua_State *lua;
extern Player* playerControl;
extern SoundMgr* soundMgr;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

#ifdef DEBUG_PRINT
extern string DBG_PRINT_CUR_PLR_ANIM;
#endif // DEBUG_PRINT

#ifdef MAP_EDITOR
extern bool editor_ShowBorders;
#endif // MAP_EDITOR

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef SELECTIVE_RENDERING
const UINT GameObject::INVALID_GRAPH_HANDLE = 0xffffffff;
#endif //SELECTIVE_RENDERING

//////////////////////////////////////////////////////////////////////////

void GameObject::Resize( float w, float h )
{
	aabb.W = w * 0.5f;
	aabb.H = h * 0.5f;
}

bool GameObject::ApplyProto(const Proto* proto)
{
	if (!proto)
		return false;

	sprite = new Sprite(proto);
	touch_detection = (TouchDetectionType)proto->touch_detection;
	return true;
}

GameObject::~GameObject()
{
	DELETESINGLE(sprite);
	DELETESINGLE(ParticleSystem);

	DELETEARRAY(mem_anim);
#ifdef MAP_EDITOR
	DELETEARRAY(proto_name);
#endif // MAP_EDITOR

	if (this->scriptProcess)
		SCRIPT::ReleaseProc(&this->scriptProcess);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool GameObject::SetAnimation(string anim_name, bool restart)
{

	bool ret = false;

	if (this->sprite)
	{
		if (!restart && this->sprite->cur_anim == anim_name)
			return false;

		if (this->sprite->SetAnimation(anim_name))
		{
			this->ProcessSprite();
			ret = true;
		}
		else if ( this->sprite->animsCount == 0 )
		{
			//If we have no animations at all it's okay to change size
			this->aabb.H = this->sprite->frameHeight * 0.5f;
			this->aabb.W = this->sprite->frameWidth * 0.5f;
			if ( this->IsPhysic() )
			{
				ObjPhysic* op = (ObjPhysic*)this;
				op->rectangle[0] = Vector2(-this->aabb.W, -this->aabb.H);
				op->rectangle[1] = Vector2( this->aabb.W, -this->aabb.H);
				op->rectangle[2] = Vector2( this->aabb.W,  this->aabb.H);
				op->rectangle[3] = Vector2(-this->aabb.W,  this->aabb.H);
				op->rectangle.CalcBox();
			}
		}

	}

#ifdef DEBUG_PRINT
	if (this->type == objPlayer)
	{
		DBG_PRINT_CUR_PLR_ANIM = this->sprite->cur_anim;
	}
#endif // DEBUG_PRINT

	return ret;
}

bool GameObject::SetAnimationIfPossible(string anim_name, bool restart)
{
	if ( this->sprite->GetAnimation(anim_name) != NULL )
		return SetAnimation( anim_name, restart );
	else return false;
}

//////////////////////////////////////////////////////////////////////////

void GameObject::Draw()
{
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		ObjPhysic* op = static_cast<ObjPhysic*>(this);
		if ( op->IsPhysic() && op->geometry != &op->rectangle )
		{
			RenderPolygon( op->geometry, aabb.p, 0.8f, op->editor_color, id );
		}
		else if ( (this->sprite && !this->sprite->IsVisible()) || this->grouped )
		{
			coord2f_t coord = coord2f_t(2*this->aabb.W, 2*this->aabb.H);
			RenderFrameRotated(this->aabb.Left(), this->aabb.Top(), 0.8f, &coord, NULL, NULL, ((ObjPhysic*)this)->editor_color, 0.0f, aabb.p, bmSrcA_OneMinusSrcA);
		}
		if ( this->type == objWaypoint && ((ObjWaypoint*)this)->unique_map_id > 0 )
		{
			static IFont* font = FontByName(DEFAULT_FONT);
			if (font)
			{
				font->p = this->aabb.p;
				font->p.x -= this->aabb.W;
				font->p.y += this->aabb.H;
				font->z = this->sprite->z;
				font->tClr = RGBAf(DEBUG_PHYSOBJ_ID_FONT_COLOR);
				font->Print("%d", ((ObjWaypoint*)this)->unique_map_id);
			}
		}
		if ( link && link->getParent() )
		{
			GameObject* target = link->getParent();
			if ( target->sprite->IsVisible() )
				RenderLine( this->aabb.p.x, this->aabb.p.y, target->aabb.p.x, target->aabb.p.y, target->sprite->z, RGBAf(DEBUG_WAYPOINT_COLOR) );
		}
	}
#endif // MAP_EDITOR
	
	if (this->sprite && this->type != objParticleSystem)
	{
		this->sprite->Draw(this->aabb);
		if ( this->sprite->outline_color.a > 0.0f )
		{
			CAABB outline_aabb = this->aabb;
			RGBAf oldcolor = this->sprite->color;
			float oldz = this->sprite->z;
			this->sprite->z = this->sprite->z - 0.001f;
			this->sprite->color.r = this->sprite->outline_color.r;
			this->sprite->color.g = this->sprite->outline_color.g;
			this->sprite->color.b = this->sprite->outline_color.b;
			outline_aabb.p.x = this->aabb.p.x + 1.0f;
			this->sprite->Draw(outline_aabb);
			outline_aabb.p.x = this->aabb.p.x - 1.0f;
			this->sprite->Draw(outline_aabb);
			outline_aabb.p.x = this->aabb.p.x;
			outline_aabb.p.y = this->aabb.p.y + 1.0f;
			this->sprite->Draw(outline_aabb);
			outline_aabb.p.y = this->aabb.p.y - 1.0f;
			this->sprite->Draw(outline_aabb);
			this->sprite->color = oldcolor;
			this->sprite->z = oldz;
		}
	}
	if (this->ParticleSystem)
		ParticleSystem->Render();

#ifdef DEBUG_DRAW_WAYPOINTS
	if ( cfg.debug && this->type == objWaypoint)
	{
		RenderLine( aabb.Left(), aabb.Top(), aabb.Right(), aabb.Bottom(), 1.0, RGBAf(DEBUG_WAYPOINT_COLOR) );
		RenderLine( aabb.Left(), aabb.Bottom(), aabb.Right(), aabb.Top(), 1.0, RGBAf(DEBUG_WAYPOINT_COLOR) );
	}

	if (cfg.debug && this->type == objEnemy)
	{
		ObjEnemy* oe = static_cast<ObjEnemy*>(this);
		GameObject* ow = NULL;
		if (oe->current_waypoint && ( (ow = GetGameObject(oe->current_waypoint)) != NULL) )
		{
			RenderLine( this->aabb.p.x, this->aabb.p.y, ow->aabb.p.x, ow->aabb.p.y, 1.0, RGBAf(DEBUG_WAYPOINT_COLOR) );
		}
	}
#endif //DEBUG_DRAW_WAYPOINTS

#ifdef DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY
#ifdef __MINGW32__
    RGBAf tmp = RGBAf(DEBUG_PHYSOBJ_BORDER_COLOR);
	if (this->IsPhysic() && cfg.debug)
		RenderBox(aabb.p.x - aabb.W, aabb.p.y - aabb.H, this->sprite->z,
		2 * aabb.W, 2* aabb.H,
        tmp);
#else
	if ( true
		#ifndef MAP_EDITOR
		&& cfg.debug
		#else
		&& (editor_ShowBorders || this->show_border)
		#endif // MAP_EDITOR
		)
	{
		//ObjPhysic* op = (ObjPhysic*)this;
		RenderBox(aabb.p.x - aabb.W, aabb.p.y - aabb.H, 
			1,
			2 * aabb.W, 2* aabb.H,
			#ifdef MAP_EDITOR
			this->border_color
			#else
			type == objBullet ? RGBAf(1.0f, 0.0f, 0.0f, 1.0f) : RGBAf(DEBUG_PHYSOBJ_BORDER_COLOR)
			#endif // MAP_EDITOR
			);
		
		if ( this->IsPhysic() )
		{
			ObjPhysic* op = (ObjPhysic*)this;
			if ( op->geometry && op->geometry != &op->rectangle )
			{
				UINT vc = op->geometry->GetVertexCount();
				for ( UINT i = 0; i < vc - 1; i++ )
					RenderLine( aabb.p.x + (*op->geometry)[i].x, aabb.p.y + (*op->geometry)[i].y, 
								aabb.p.x + (*op->geometry)[i+1].x, aabb.p.y + (*op->geometry)[i+1].y,
								1, RGBAf(DEBUG_POLYGON_COLOR) );
				RenderLine( aabb.p.x + (*op->geometry)[vc-1].x, aabb.p.y + (*op->geometry)[vc-1].y, 
							aabb.p.x + (*op->geometry)[0].x, aabb.p.y + (*op->geometry)[0].y,
							1, RGBAf(DEBUG_POLYGON_COLOR) );
			}
			if (((ObjPhysic*)this)->IsDynamic())
			{
				ObjDynamic* dyn = ((ObjDynamic*)this);
				#ifdef DEBUG_DRAW_SPEED_VECTORS
				Vector2 vv = 10 * (dyn->vel + 0.5f * dyn->acc) + this->aabb.p;
				RenderLine(aabb.p.x, aabb.p.y, 
					vv.x, vv.y, 1.0f, RGBAf(DEBUG_DRAW_SPEED_VEC_COLOR));
				
				vv = 10 * dyn->env->gravity_bonus + this->aabb.p;
				RenderLine(vv.x, vv.y, this->aabb.p.x, this->aabb.p.y, 1.0f, RGBAf(DEBUG_DRAW_GRAV_BONUS_COLOR));
				#endif // DEBUG_DRAW_SPEED_VECTORS
				
				#ifdef DEBUG_DRAW_SUSPECTED_PLAIN
				if (dyn->GetSuspectedPlane())
				{
					RenderLine(aabb.p.x, aabb.p.y, dyn->GetSuspectedPlane()->aabb.p.x, dyn->GetSuspectedPlane()->aabb.p.y, 1.0f, RGBAf(DEBUG_DRAW_SUSPECTED_PLAIN_COLOR));
				}
				#endif // DEBUG_DRAW_SUSPECTED_PLAIN
			}
		}
	}
#endif
#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY
	
#ifdef DEBUG_DRAW_PHYSOBJ_ID
	if (this->IsPhysic() && cfg.debug)
	{
		static IFont* font = FontByName(DEFAULT_FONT);
		if (font)
		{
			font->p = this->aabb.p;
			font->z = 1.0f;
			font->tClr = RGBAf(DEBUG_PHYSOBJ_ID_FONT_COLOR);
			font->outline = RGBAf(DEBUG_PHYSOBJ_ID_FONT_OUTLINE_COLOR);
			font->Print("%d", this->id);
		}
	}
#endif // DEBUG_DRAW_PHYSOBJ_ID
}

#ifdef SELECTIVE_RENDERING
bool GameObject::GetDrawAABB(CAABB& res) const
{
	bool changed = false;
	if (sprite)
	{
		res = sprite->getRenderAABB(aabb);
		changed = true;
	}
	if (ParticleSystem)
	{
		if (changed)
		{
			res = CAABB(std::min(res.Left(), ParticleSystem->min_lt.x), std::min(res.Top(), ParticleSystem->min_lt.y), 
				std::max(res.Right(), ParticleSystem->max_rb.x), std::max(res.Bottom(), ParticleSystem->max_rb.y));		
		}
		else
		{
			res = CAABB(ParticleSystem->min_lt.x, ParticleSystem->min_lt.y, 
				ParticleSystem->max_rb.x, ParticleSystem->max_rb.y);
		}
		changed = true;
	}
	return changed;
}
#endif


void GameObject::ProcessSprite()
{
	class StackBalancer
	{
	public:
		int top;
		StackBalancer() : top(lua_gettop(lua)) {}
		~StackBalancer() { lua_settop(lua, top); }
	};
	StackBalancer stackBalancer;

	Sprite* s= this->sprite;

	// ���� ������������ �� ������ ObjCharacter, �� ����� ���� �������� �������������� ��������.
	ObjCharacter* ch = (this->type == objPlayer || this->type == objEnemy) ? static_cast<ObjCharacter*>(this) : NULL;
	ParametersStack* ps = this->sprite->stack;
	int param;
	const char* txt_param;

	if (s->animNames)
	{
		Animation* a = s->GetAnimation(this->sprite->cur_anim_num);
		if (a)
		{
#ifdef SELECTIVE_RENDERING
			const CAABB tmp_old_aabb(aabb);
#endif
			while (!s->ChangeFrame(a))
			{
				const AnimationFrame& af = a->frames[a->current];

				if (af.is_param_function)
				{
					this->pushUData(lua);
					int ret_val = SCRIPT::ExecChunkFromReg(af.param, 1);
					if ( ret_val == -1)
					{
						Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
							"An error occurred while executing a local function. obj id %d, proto_name '%s', anim '%s', frame %d: %s",
							this->id, this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current, lua_tostring(lua, -1) );
					}
					
					const int top = lua_gettop(lua);
					const int p1 = top - ret_val + 1;
					const int p2 = top - ret_val + 2;
					const int p3 = top - ret_val + 3;

					param     = (ret_val > 0 && lua_isnumber(lua, p1)) ? lua_tointeger(lua, p1) : 0;
					txt_param = (ret_val > 1 && lua_isstring(lua, p2)) ? lua_tostring(lua, p2) : NULL;
					if ( ret_val > 2 && lua_isnumber(lua, p3) ) s->setCurrentFrame(lua_tointeger(lua, p3));
				}
				else
				{
					param = af.param;
					txt_param = af.txt_param;
				}

				if ( af.polygon && IsPhysicType() )
				{
					ObjPhysic* op = (ObjPhysic*)this;
					if ( af.polygon && af.polygon < sprite->polygons->size()+1 )
					{
						op->geometry = (CPolygon*)((sprite->polygons->at( af.polygon-1 )));
						CBox box = op->geometry->GetBox();
						op->aabb.W = box.Width()/2.0f;
						op->aabb.H = box.Height()/2.0f;
					}
					else op->geometry = &op->rectangle;
				}

				// Big ugly motherfucker
				switch (af.command)
				{
				case afcRandomOverlayColor:
					if ( this->sprite->overlayCount )
					{
						if (!this->sprite->ocolor)
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning, "ProcessSprite(), afcRandomOverlayColor ocolor==NULL, proto %s, anim %s, frame %d",
								this->sprite->proto_name, a->name, a->current);
							break;
						}
						if (this->sprite->overlayCount <= (UINT)param || param < 0)
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning, "ProcessSprite(), afcControlledOverlayColor invalid param, proto %s, anim %s, frame %d",
								this->sprite->proto_name, a->name, a->current);
							break;
						}
						this->sprite->ocolor[param] = RGBAf( (rand() % 101)/100.0f, (rand() % 101)/100.0f, (rand() % 101)/100.0f, 1.0f);
					}
					break;
				case afcSetColor:
					if ( this->sprite )
					{
						if ( !ps->CheckParamTypes(3, stInt, stInt, stInt) )
							break;
						float b = ps->PopInt()/255.0f;
						float g = ps->PopInt()/255.0f;
						float r = ps->PopInt()/255.0f;
						this->sprite->color = RGBAf(r, g, b, 1.0f);
					}
					break;
				case afcSetWaypoint:
					if ( this->type == objEnemy )
					{
						ObjWaypoint* wp = GetWaypoint( param );
						if ( wp )
							((ObjEnemy*)this)->current_waypoint = wp->id;
					}
					break;
				case afcSetDamageMult:
					{
						((ObjCharacter*)this)->damage_mult = param / 1000.0f;

					}
					break;
				case afcSetZ:
					{
						this->sprite->z = param / 1000.0f;
					}
					break;
				case afcStartDying:
					{
						if ( this->type == objBullet )
						{
							if (param != -1) 
							{
								if (((ObjBullet*)this)->multiple_targets)
									((ObjBullet*)this)->activity = oatUndead;
								else
									((ObjBullet*)this)->activity = oatDying;
							}
							else ((ObjBullet*)this)->activity = oatIdling;
						}
						if ( this->type == objEnemy )
						{
							((ObjEnemy*)this)->activity = oatDying;
							((ObjEnemy*)this)->health = 0;
						}
						if ( GetCameraAttachedObject() == this )
						{
							CameraAttachToObject( NULL );
						}
					}
					break;
				case afcControlledOverlayColor:
					if ( this->sprite->overlayCount )
					{
						if (!this->sprite->ocolor)
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning, "ProcessSprite(), afcControlledOverlayColor ocolor==NULL, proto %s, anim %s, frame %d",
								this->sprite->proto_name, a->name, a->current);
							break;
						}
						if (this->sprite->overlayCount <= (UINT)param || param < 0)
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning, "ProcessSprite(), afcControlledOverlayColor invalid param, proto %s, anim %s, frame %d",
								this->sprite->proto_name, a->name, a->current);
							break;
						}
						if ( !ps->CheckParamTypes(6, stInt, stInt, stInt, stInt, stInt, stInt) )
							break;
						int b2 = ps->PopInt();
						int b1 = ps->PopInt();
						int g2 = ps->PopInt();
						int g1 = ps->PopInt();
						int r2 = ps->PopInt();
						int r1 = ps->PopInt();
						float r = 0.0f;
						if ( r1 != r2 )
							r = (r1 + rand() %(r2 - r1))/255.0f;
						else
							r = r1/255.0f;
						float g = 0.0f;
						if ( g1 != g2 )
							g = (g1 + rand() %(g2 - g1))/255.0f;
						else
							g = g1/255.0f;
						float b = 0.0f;
						if ( b1 != b2 )
							b = (b1 + rand() %(b2 - b1))/255.0f;
						else
							b = b1/255.0f;
						this->sprite->ocolor[param] = RGBAf( r, g, b, 1.0);
					}
					break;
				case afcRealX:
					s->setRealX(static_cast<short>(param));
					break;
				case afcBreakpoint:
					{
						string cmt = txt_param ? txt_param : "(empty)";
						Log(DEFAULT_LOG_NAME, logLevelWarning, "Breakpoint triggered at %s with comment %s.", this->sprite->cur_anim.c_str(), cmt.c_str());
					}
					break;
				case afcRealY:
					s->setRealY(static_cast<short>(param));
					break;
				case afcRealW:
					s->setRealWidth(static_cast<USHORT>(param));
					this->aabb.W = s->getRealWidth() * 0.5f;
					if ( IsPhysicType() )
					{
						ObjPhysic* op = (ObjPhysic*)this;
						op->rectangle[0].x = -this->aabb.W;
						op->rectangle[1].x =  this->aabb.W;
						op->rectangle[2].x =  this->aabb.W;
						op->rectangle[3].x = -this->aabb.W;
						op->rectangle.CalcBox();
					}
					if ( this->IsPhysic() && !((ObjPhysic*)this)->IsDynamic()
#ifdef MAP_EDITOR
						&& editor_state == editor::EDITOR_OFF
#endif
						)
							UpdateSAPObject(((ObjPhysic*)this)->sap_handle, this->aabb);
					break;
				case afcRealH:
					s->setRealHeight(static_cast<USHORT>(param));
					this->aabb.p.y += this->aabb.H - s->getRealHeight() * 0.5f;
					this->aabb.H = s->getRealHeight() * 0.5f;
					if ( IsPhysicType() )
					{
						ObjPhysic* op = (ObjPhysic*)this;
						op->rectangle[0].y = -this->aabb.H;
						op->rectangle[1].y = -this->aabb.H;
						op->rectangle[2].y =  this->aabb.H;
						op->rectangle[3].y =  this->aabb.H;
						op->rectangle.CalcBox();
					}
					if ( this->IsPhysic() && !((ObjPhysic*)this)->IsDynamic() 
#ifdef MAP_EDITOR
						&& editor_state == editor::EDITOR_OFF
#endif
						)
							UpdateSAPObject(((ObjPhysic*)this)->sap_handle, this->aabb);
					break;
				case afcInitW:
					{
						int mode = this->sprite->stack->PopInt();
						s->setRealWidth(static_cast<USHORT>(param));
						if ( mode & 1 ) this->aabb.p.x -= this->aabb.W - s->getRealWidth() * 0.5f;
						else this->aabb.p.x += this->aabb.W - s->getRealWidth() * 0.5f;
						this->aabb.W = s->getRealWidth() * 0.5f;
						if ( IsPhysicType() )
						{
							ObjPhysic* op = (ObjPhysic*)this;
							op->rectangle[0].x = -this->aabb.W;
							op->rectangle[1].x =  this->aabb.W;
							op->rectangle[2].x =  this->aabb.W;
							op->rectangle[3].x = -this->aabb.W;
							op->rectangle.CalcBox();
						}
						if ( this->IsPhysic() && !((ObjPhysic*)this)->IsDynamic() ) UpdateSAPObject(((ObjPhysic*)this)->sap_handle, this->aabb);
					}
					break;
				case afcInitH:
					{
						if ( !ps->CheckParamTypes(1, stIntOrNone) )
							break;
						s->setRealHeight(static_cast<USHORT>(param));
						int mode = ps->GetInt();
						if ( mode == -1 )
							this->aabb.p.y -= this->aabb.H - s->getRealHeight() * 0.5f;
						else if ( mode == 1 )
							this->aabb.p.y += this->aabb.H - s->getRealHeight() * 0.5f;
						this->aabb.H = s->getRealHeight() * 0.5f;
						if ( IsPhysicType() )
						{
							ObjPhysic* op = (ObjPhysic*)this;
							op->rectangle[0].y = -this->aabb.H;
							op->rectangle[1].y = -this->aabb.H;
							op->rectangle[2].y =  this->aabb.H;
							op->rectangle[3].y =  this->aabb.H;
							op->rectangle.CalcBox();
						}
						if ( this->IsPhysic() && !((ObjPhysic*)this)->IsDynamic() 
#ifdef MAP_EDITOR
							&& editor_state == editor::EDITOR_OFF
#endif //MAP_EDITOR
							) UpdateSAPObject(((ObjPhysic*)this)->sap_handle, this->aabb);
						break;
					}
				case afcInitWH:
					{
						if ( !ps->CheckParamTypes(1, stIntOrNone) )
							break;
						s->setRealWidth(static_cast<USHORT>(param));
						s->setRealHeight(static_cast<USHORT>(param));
						int mode = ps->GetInt();
						if ( mode == -1 )
						{
							this->aabb.p.x -= this->aabb.W - s->getRealWidth() * 0.5f;
							this->aabb.p.y -= this->aabb.H - s->getRealHeight() * 0.5f;
						}
						else if ( mode == 1 )
						{
							this->aabb.p.x += this->aabb.W - s->getRealWidth() * 0.5f;
							this->aabb.p.y += this->aabb.H - s->getRealHeight() * 0.5f;
						}
						this->aabb.W = s->getRealWidth() * 0.5f;
						this->aabb.H = s->getRealHeight() * 0.5f;
						if ( IsPhysicType() )
						{
							ObjPhysic* op = (ObjPhysic*)this;
							op->rectangle[0].x = -this->aabb.W;
							op->rectangle[1].x =  this->aabb.W;
							op->rectangle[2].x =  this->aabb.W;
							op->rectangle[3].x = -this->aabb.W;
							op->rectangle[0].y = -this->aabb.H;
							op->rectangle[1].y = -this->aabb.H;
							op->rectangle[2].y =  this->aabb.H;
							op->rectangle[3].y =  this->aabb.H;

							op->rectangle.CalcBox();
						}
						if ( this->IsPhysic() && !((ObjPhysic*)this)->IsDynamic() 
#ifdef MAP_EDITOR
							&& editor_state == editor::EDITOR_OFF
#endif //MAP_EDITOR
							) UpdateSAPObject(((ObjPhysic*)this)->sap_handle, this->aabb);
						break;
					}
				case afcReloadTime:
					{
						if ( type != objPlayer ) break;
						ObjPlayer* op = static_cast<ObjPlayer*>(this);
						UINT time = op->alt_fire ? ( op->alt_weapon ? op->alt_weapon->reload_time : 0 ) : ( op->cur_weapon ? op->cur_weapon->reload_time : 0 );
						a->frames[a->current].duration = param == 0 ? 0 : time / param;
					}
					break;
				case afcShootDir:
					if (NULL != ch)
					{
						ch->gunDirection = (CharacterGunDirection)param;
					}
					break;
				case afcShootBeh:
					if (NULL != ch)
					{
						ch->shootingBeh = (CharacterShootingBeh)param;
					}
					break;
				case afcShoot:
					if (NULL != ch)
					{
						ch->shootingBeh = csbOnAnimCommand;
					}
					break;
				case afcSetAnimIfGunDirection:
					{
						if (!txt_param)
							break;
						ObjCharacter* oc = (ObjCharacter*)this;
						if ( param == 1 && oc->gunDirection == cgdUp )
						{
							this->SetAnimation(txt_param, true);
							return;
						}
						if ( param == -1 && oc->gunDirection == cgdDown )
						{
							this->SetAnimation(txt_param, true);
							return;
						}
						if (param == 0 && oc->gunDirection == cgdNone )
						{
							this->SetAnimation(txt_param, true);
							return;
						}
					}
					break;
				case afcCallAnim:
					{
						if ( txt_param )
						{
							sprite->SaveState();
							this->SetAnimation(txt_param, true);
							return;
						}
						break;
					}
				case afcSetAnim:
					if (!txt_param)
					{
						Log(DEFAULT_LOG_NAME, logLevelError, "SetAnim with no animation name at %s.", this->sprite->cur_anim.c_str());
						break;
					}
					if ( param <= 0 || param >= 256 || rand()%256 > param )
					{
						this->SetAnimation(txt_param, true);
						return;
					}
					break;
				case afcSetAnimIfWeaponNotReady:
					if (!txt_param)
					{
						Log(DEFAULT_LOG_NAME, logLevelError, "SetAnimIfWeaponNotReady with no animation name at %s.", this->sprite->cur_anim.c_str());
						break;
					}
					if ( this->type == objPlayer && ((ObjPlayer*)this)->cur_weapon && !((ObjPlayer*)this)->cur_weapon->IsReady() && ((ObjPlayer*)this)->HasAmmo())
						this->SetAnimation(txt_param, true);
					return;
				case afcPushInt:
					this->sprite->stack->Push( param );
					break;
				case afcPushString:
					this->sprite->stack->Push( txt_param );
					break;
				case afcPushRandomInt:
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int hi = ps->PopInt();
						int lo = ps->PopInt();
						if (hi <= lo) this->sprite->stack->Push( lo );
						else this->sprite->stack->Push( lo + rand()%(hi - lo) );
					}
					break;
				case afcJump:
					this->sprite->JumpFrame( param );
					break;
				case afcRecover:
					{
						sprite->RestoreState();
						a = sprite->GetAnimation(this->sprite->cur_anim_num);
						break;
					}
				case afcRemoveRecoveryState:
					{
						sprite->PopState();
						break;
					}
				case afcCreateRecoveryState:
					{
						sprite->SaveState();
						break;
					}
				case afcJumpRandom:
					{
						if ( !ps->CheckParamTypes(1,stInt) )
							break;
						int var = ps->PopInt();
						if (  (rand() % 256) > var )

							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfXLess:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int x = ps->PopInt();
						if ( this->aabb.p.x <= x )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfXGreater:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int x = ps->PopInt();
						if ( this->aabb.p.x >= x )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfYLess:
				{
					if ( !ps->CheckParamTypes(1, stInt) )
						break;
					int y = ps->PopInt();
					if ( this->aabb.p.y <= y )
						this->sprite->JumpFrame( param );
				}
				break;
				case afcJumpIfYGreater:
				{
					if ( !ps->CheckParamTypes(1, stInt) )
						break;
					int y = ps->PopInt();
					if ( this->aabb.p.y >= y )
						this->sprite->JumpFrame( param );
				}
				break;
				case afcJumpIfYSpeedGreater:
					if ( this->IsPhysic() && ( (ObjPhysic*)this)->IsDynamic() )
					{
						if ( !ps->CheckParamTypes(1, stIntOrNone) )
							break;
						ObjDynamic* dno = (ObjDynamic*)this;
						int sparam = ps->PopInt();
						if ( dno->vel.y > sparam )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcSetInvisible:
					//if ( true )
					{
						if ( !this->sprite ) break;
						if ( ( param ) == 0 ) this->sprite->SetVisible();
						else this->sprite->ClearVisible();
					}
					break;
				case afcSetShielding:
					if ( this->type == objBullet )
					{
						if ( ( param ) == 1 ) ((ObjBullet*)this)->SetShielding();
						else ((ObjBullet*)this)->ClearShielding();
					}
					break;
				case afcSetLifetime:
					{
						if (!this->IsPhysicType() || !((ObjPhysic*)this)->IsDynamic())
						{
							Log(DEFAULT_LOG_NAME, logLevelError, "afcSetLifetime: wrong usage, object must be dynamic");
							break;
						}
						ObjDynamic* od = (ObjDynamic*)this;
						od->lifetime = (float)param;
					}
					break;
				case afcSetBulletCollidable:
					if ( this->IsPhysic() )
					{
						if ( !this->sprite ) break;
						if ( ( param ) == 1 ) ((ObjPhysic *)this)->SetBulletCollidable();
						else ((ObjPhysic *)this)->ClearBulletCollidable();
					}
					break;
				case afcSetSolid:
					if ( this->IsPhysic() )
					{
						if ( !this->sprite ) break;
						if ( ( param ) == 1 ) ((ObjPhysic *)this)->SetSolid();
						else ((ObjPhysic *)this)->ClearSolid();
					}
					break;
				case afcSetSolidTo:
					if ( this->IsPhysic() )
					{
						(reinterpret_cast<ObjPhysic*>(this))->solid_to = static_cast<BYTE>( param );
					}
					break;
				case afcJumpIfCloseToCamera:
					//if ( true )
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( fabs(this->aabb.p.x - CAMERA_X) < x && fabs(this->aabb.p.y - CAMERA_Y ) <= y)
							this->sprite->JumpFrame( param );

					}
					break;
				case afcJumpCheckFOV:	//TODO: ��������� ����������� �������.
					if ( this->type == objEnemy )
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = oe->GetTarget();
						if (!otarget) break;
						int angle = ps->PopInt();
						if ( atan2( otarget->aabb.p.y - oe->aabb.p.y, otarget->aabb.p.x - oe->aabb.p.x ) <= angle
							&& oe->aabb.p.x > otarget->aabb.p.x )
								this->sprite->JumpFrame( param );

					}
					break;
				case afcJumpIfObjectExists:
					//if ( true)
					{
						if ( !ps->CheckParamTypes(1, stIntOrNone) )
						{
							break;
						}
						int oid = ps->GetInt();
						if ( oid == 0 ) break;
						GameObject* go = GetGameObject(oid);
						if ( go && !go->IsDead() )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfCloseToCameraRight:
					//if ( true )
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( CAMERA_X > this->aabb.p.x - x && fabs(this->aabb.p.y - CAMERA_Y ) <= y)
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfCloseToCameraLeft:
					//if ( true )
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( CAMERA_X > this->aabb.p.x + x && fabs(this->aabb.p.y - CAMERA_Y ) <= y)
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfCloseToCameraUp:
					//if ( true )
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( CAMERA_Y < this->aabb.p.y - y && fabs(this->aabb.p.x - CAMERA_X ) <= x)
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfCloseToCameraDown:
					//if ( true )
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( CAMERA_Y > this->aabb.p.y + y && fabs(this->aabb.p.x - CAMERA_X ) <= x)
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfSquashCondition: //TODO: �������� �� JumpIfYLess + JumpIfObjectYSpeedGreater?
					if ( this->type == objEnemy )
					{
						ObjDynamic* dno = (ObjDynamic*)this;
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						GameObject* go = GetGameObject(oid);
						if ( !go->IsPhysic() || !(((ObjPhysic*)go)->IsDynamic()) || go->type == objItem || go->type == objBullet ) break;
						ObjDynamic * od = (ObjDynamic*)go;
						if ( od->vel.y > 0 && od->aabb.p.y + od->aabb.H < dno->aabb.p.y )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcBounceObject:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						GameObject* go = GetGameObject(oid);
						if ( !go->IsPhysic() || !(((ObjPhysic*)go)->IsDynamic()) ) break;
						go->SetAnimation("jump", true);

						ObjDynamic* od = (ObjDynamic *)go;

						if ( go->type == objPlayer && inpmgr.IsPressed(cakJump) )
							od->vel.y = min(-10.0f, od->vel.y * -1.25f);
						else
							od->vel.y = min(-10.0f, od->vel.y * -0.75f);
					}
					break;
				case afcPushObject:
					{
						if ( !ps->CheckParamTypes(3, stInt, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						int oid = ps->GetInt();
						GameObject* go = GetGameObject(oid);
						if ( !go->IsPhysic() || !(((ObjPhysic*)go)->IsDynamic()) ) break;
						ObjDynamic* od = (ObjDynamic *)go;
						od->vel.x += x;
						od->vel.y += y;
					}
					break;
				case afcJumpIfPlayerId:
					if ( playerControl && playerControl->current )
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						UINT oid = (UINT)ps->GetInt();
						GameObject* player = GetGameObject( oid );
						if ( player && player->type == objPlayer && !player->IsSleep() )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcJumpIfXSpeedGreater:
					if ( this->IsPhysic() && ( (ObjPhysic*)this)->IsDynamic() )
					{
						ObjDynamic* dno = (ObjDynamic*)this;
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int sparam = ps->PopInt();
						if ( dno->vel.x > sparam || dno->vel.x < -sparam )
							this->sprite->JumpFrame( param );
					}
					break;
				case afcSetHealth:
					if ( this->type == objEnemy )
					{
						((ObjEnemy*)this)->health = static_cast<short>(param);
						((ObjEnemy*)this)->health_max = static_cast<short>(param * 2); //TODO: ������ �� ���������?
					}
					if ( this->type == objEffect )
					{
						((ObjEffect*)this)->health = param;
					}
					break;
				case afcSetGravity:
					//if ( this->IsPhysic() && ((ObjPhysic*)this)->IsDynamic() )
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						((ObjDynamic*)this)->gravity = Vector2( x/1000.0f, y/1000.0f );
					}
					break;
				case afcDamage:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						ObjCharacter* oc = (ObjCharacter*)GetGameObject( oid );
						if (oc)
							oc->ReduceHealth(param);
						//������� ������ � ��������� ��������.
						if (GetCameraAttachedObject() == oc )
						{
							// �� ���� ������� ������������� ������. ������� ���.
							CameraAttachToObject(NULL);
						}
					}
					break;
				case afcDealDamage:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						ObjCharacter* oc = (ObjCharacter*)GetGameObject( oid );
						if (oc)
							oc->ReceiveDamage(param, 0);
					}
					break;
				case afcDrop:
					{
						if ( this->IsPhysic() && ((ObjPhysic*)this)->IsDynamic() )
						{
							ObjDynamic* od = (ObjDynamic*)this;
							if ( param == 1  )
							{
								od->SetDropping();
								od->ClearOnPlane();
							}
							else
								od->ClearDropping();
						}
					}
					break;
				case afcReduceHealth:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int damage = ps->PopInt();
						if ( this->type == objPlayer || this->type == objEnemy || this->type == objItem )
						{
							if ( damage > 0 )
							{
								if (((ObjCharacter*)this)->ReduceHealth( (UINT)damage )) return;
							}
							else
							{
								((ObjCharacter*)this)->ReceiveHealing( (UINT)(-damage) );
							}
						}
						break;
					}
				case afcSetInvincible:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						ObjCharacter* oc = (ObjCharacter*)GetGameObject( oid );
						if (oc)
						{
							if ( param == 1 )
								oc->is_invincible = true;
							else
								oc->is_invincible = false;
						}
					}
					break;
				case afcSetTouchable:
					{
						if (this->type == objItem || this->type == objEnemy)
						{
							if (param == 1) ((ObjPhysic*)this)->SetTouchable();
							else ((ObjPhysic*)this)->ClearTouchable();
						}
					}
					break;
				case afcGiveHealth:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						ObjCharacter* oc = (ObjCharacter*)GetGameObject(oid);
						if (oc)
							oc->ReceiveHealing(param);
					}
					break;
				case afcGiveWeapon:
					{
						if ( !ps->CheckParamTypes(1, stInt) || !txt_param )
							break;
						int oid = ps->GetInt();
						GameObject* go = GetGameObject(oid);
						if ( go->type != objPlayer ) break;
						ObjPlayer* op = (ObjPlayer*)go;
						if (op)
						{
							lua_getglobal(lua, "WeaponSystem");
							if ( lua_isfunction(lua, -1) )
							{
								lua_pushinteger(lua, op->id);
								lua_pushstring(lua, txt_param);
								lua_call(lua, 2, 1);
								if ( lua_isstring(lua, -1) )
									op->GiveWeapon(lua_tostring(lua, -1), false);
								else

								{
									DELETESINGLE(op->alt_weapon);
									op->cur_weapon = op->weapon;
								}
							}
							else
								op->GiveWeapon(txt_param, false);
						}
					}
					break;
				case afcReplaceWithRandomTile:
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) || !txt_param )
							break;
						this->SetDead();
						int to = ps->PopInt();
						int from = ps->PopInt();
						GameObject* obj1 = CreateSprite( txt_param, this->aabb.p-Vector2( this->aabb.W, this->aabb.H ), false, NULL);
						int tilenum = Random_Int( from, to );
						if (obj1 && obj1->sprite)
						{
							obj1->sprite->SetCurrentFrame(tilenum);
							obj1->sprite->animsCount = 0;
						}
					}
					break;
				case afcGiveAmmo:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int oid = ps->GetInt();
						GameObject* obj = GetGameObject( oid );
						if (obj)
						{
							if ( obj->type != objPlayer )
								break;
							( (ObjPlayer*)obj )->RecieveAmmo(param);
						}
					}
					break;
				case afcSetAccY:
					{
						/*if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;*/ //������-�� �� �������� ��� �����. ���������.
						((ObjDynamic*)this)->acc.y = param/1000.0f;
					}
					break;
				case afcSetVelY:
					{
						/*if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;*/
						((ObjDynamic*)this)->vel.y = param/1000.0f;
					}
					break;
				case afcSetAccX:
					{
						/*if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;*/
						((ObjDynamic*)this)->acc.x = param/1000.0f;
					}
					break;
				case afcAdjustAccY:
					{
						/*if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;*/
						((ObjDynamic*)this)->acc.y += param/1000.0f;
					}
					break;
				case afcSetOwnVelX:
					{
						((ObjDynamic*)this)->own_vel.x = param/1000.0f;
					}
					break;
				case afcSetOwnVelY:
					{
						((ObjDynamic*)this)->own_vel.y = param/1000.0f;
					}
					break;
				case afcSetVelX:
					{
						/*if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;*/
						((ObjDynamic*)this)->vel.x = param/1000.0f;
					}
					break;
				case afcSetRelativeVelX:
					{
						if ( this->type == objEffect )
						{
							ObjEffect* oe = (ObjEffect*)this;
							ObjDynamic* origin = (ObjDynamic*)oe->parentConnection->getParent();
							if ( origin && origin->GetFacing() )
								((ObjDynamic*)this)->vel.x = -param/1000.0f;
							else
								((ObjDynamic*)this)->vel.x = param/1000.0f;
						}
						else
						{
							float speed = param/1000.0f;
							ObjDynamic* od = (ObjDynamic*)this;
							od->vel.x = ( od->GetFacing() ? -speed : speed  );
						}
					}
					break;
				case afcSetRelativeAccX:
					{
						if ( this->type == objEffect )
						{
							ObjEffect* oe = (ObjEffect*)this;
							ObjDynamic* origin = (ObjDynamic*)oe->parentConnection->getParent();
							if ( origin && origin->GetFacing() )
								((ObjDynamic*)this)->acc.x = -param/1000.0f;
							else
								((ObjDynamic*)this)->acc.x = param/1000.0f;
						}
						else
						{
							float speed = param/1000.0f;
							ObjDynamic* od = (ObjDynamic*)this;
							od->acc.x = ( od->GetFacing() ? -speed : speed  );
						}
					}
					break;
				case afcSetRelativePos:
					{
						if ( this->type != objEffect )
							break;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						ObjEffect* oe = (ObjEffect*)this;
						int x = ps->PopInt();
						int y = ps->PopInt();
						ObjDynamic* origin = (ObjDynamic*)oe->parentConnection->getParent();
						if (origin)
						{
							if ( origin->GetFacing() ) 
							{
								Vector2 mp = origin->sprite->mp[ oe->origin_point ];
								mp.x *= -1;
								oe->aabb.p = origin->aabb.p +  Vector2((float)-x, (float)y) + mp;
							}
							else oe->aabb.p = origin->aabb.p +  Vector2((float)x, (float)y) + origin->sprite->mp[ oe->origin_point ];
						}
					}
					break;
				case afcSetMaxVelX:
					{
						if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;

						((ObjDynamic*)this)->max_x_vel = param/1000.0f;
					}
					break;
				case afcSetMaxVelY:
					{
						if ( !(this->IsPhysic() || this->type == objBullet ) || !((ObjPhysic*)this)->IsDynamic() )
							break;
						((ObjDynamic*)this)->max_y_vel = param/1000.0f;
					}
					break;
				case afcAdjustX:
					{
						if ( this->GetFacing() )
							this->aabb.p.x -= param;
						else
							this->aabb.p.x += param;
					}
					break;
				case afcAdjustY:
					{
						this->aabb.p.y += param;
					}
					break;
				case afcStop:
					{
						if ( !this->IsPhysic() || !((ObjPhysic*)this)->IsDynamic() )
							break;
						((ObjDynamic*)this)->acc = Vector2(0,0);
						((ObjDynamic*)this)->vel = Vector2(0,0);
						((ObjDynamic*)this)->own_vel = Vector2(0,0);
					}
					break;
				case afcClearTarget:
					{
						if (this->type != objEnemy)
							break;
						ObjEnemy* oe = (ObjEnemy*)this;
						if ( oe->target )
							oe->target->childDead(id);
						oe->target = NULL;
						break;
					}
				case afcWaitForTarget:
					{
						//assert(this->type == objEnemy);
						if (this->type != objEnemy)
							break;

						ObjEnemy* oe = (ObjEnemy*)this;
						GameObject* tg = oe->GetTarget();
						if ( tg && (tg->aabb.p - aabb.p).Length() < param )
						{
							this->SetAnimation(txt_param, true);
							return;
						}
						ObjCharacter* otarget = (ObjCharacter*)oe->GetNearestCharacter( oe->faction_hates );
						if ( (otarget) && (oe->aabb.p - otarget->aabb.p).Length() < param &&
							 (!oe->GetTarget() || oe->GetTarget() != otarget))
						{
							oe->UpdateTarget( otarget );
							this->SetAnimation(txt_param, true);
							return;
						}
						else
						{
							 otarget = (ObjCharacter*)oe->GetNearestCharacter( oe->faction_follows );
							 if ( (otarget) && (oe->aabb.p - otarget->aabb.p).Length() < param &&
								(!oe->GetTarget() || oe->GetTarget() != otarget))
							 {
								oe->UpdateTarget( otarget );
							 	this->SetAnimation("follow", true);
							 	return;
							 }
						}
					break;
					}
				case afcWaitForEnemy:
					{
						if (this->type != objEnemy)
							break;
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = (ObjCharacter*)oe->GetNearestCharacter( oe->faction_hates );
						if ( (otarget) && (oe->aabb.p - otarget->aabb.p).Length() < param &&
							(!oe->GetTarget() || otarget != oe->GetTarget() ) )
						{
							oe->UpdateTarget(otarget);
							this->SetAnimation(txt_param, true);
							return;
						}
						break;
					}
				case afcMapVarFieldAdd:
					{
						scriptApi::MapVarAdd( lua, txt_param, param );
					}
					break;
				case afcSetAnimOnTargetPos:
					{
						if ( !ps->CheckParamTypes(4, stInt, stInt, stInt, stInt) )
							break;
						ObjDynamic* target = NULL;
						if ( this->type == objEffect )
						{
							ObjDynamic* origin = (ObjDynamic*)this->parentConnection->getParent();
							if (!origin) break;
							target = ( (ObjEnemy*)origin )->GetTarget();
						}
						else if ( this->type == objEnemy ) target = ((ObjEnemy*)this)->GetTarget();
						if (!target && playerControl) target = playerControl->current;
						if (target)
						{
							int dist = ps->PopInt();
							int anim3 = ps->PopInt();
							int anim2 = ps->PopInt();
							int anim1 = ps->PopInt();
							float self = this->aabb.p.y;
							float targ = target->aabb.p.y;
							if ( param & 1 )
							{
								self = this->aabb.p.x;
								targ = target->aabb.p.x;
							}
							if ( targ - self > dist )
							{
								if ( (param & 2) && this->GetFacing() )
								{
									this->sprite->JumpFrame( anim1 );
									break;
								}
								else
								{
									this->sprite->JumpFrame( anim3 );
									break;
								}
							}
							else if ( self - targ > dist )
							{
								if ( (param & 2) && this->GetFacing() )
								{
									this->sprite->JumpFrame( anim3 );
									break;
								}
								else
								{
									this->sprite->JumpFrame( anim1 );
									break;
								}
							}
							else this->sprite->JumpFrame( anim2 );
						}
					}
					break;
				case afcJumpIfTargetClose:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int distance = ps->PopInt();
						ObjEnemy* oe = NULL;
						if ( this->type == objEnemy ) oe = (ObjEnemy*)this;
						if ( this->type == objEffect ) 
						{
							ObjDynamic* origin = (ObjDynamic*)this->parentConnection->getParent();
							if (!origin) break;
							oe = (ObjEnemy*)origin;
						}
						if ( !oe ) break;
						ObjCharacter* otarget = oe->GetTarget();
						if ( otarget == NULL ) oe->UpdateTarget( static_cast<ObjCharacter*>(oe->GetNearestCharacter( oe->faction_hates )) );
						if ( (otarget) && !(otarget->IsDead()) && (oe->aabb.p - otarget->aabb.p).Length() < distance )
							this->sprite->JumpFrame( param );
					break;
					}
				case afcJumpIfTargetCloseByX:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int distance = ps->PopInt();
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = oe->GetTarget();
						if ( (otarget) && !(otarget->IsDead()) && abs(oe->aabb.p.x - otarget->aabb.p.x) < distance )
							this->sprite->JumpFrame( param );
					break;
					}
				case afcJumpIfTargetCloseByY:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int distance = ps->PopInt();
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = oe->GetTarget();
						if ( otarget && abs(oe->aabb.p.y - otarget->aabb.p.y) < distance )
							this->sprite->JumpFrame( param );
					break;
					}
				case afcJumpIfOnPlane:
					{
						if (this->IsPhysic() && ((ObjPhysic*)this)->IsDynamic() && ((ObjDynamic*)this)->IsOnPlane())
							this->sprite->JumpFrame( param );
						break;
					}
				case afcStopMorphing:
					{
						if ( this->type == objPlayer )
							((ObjPlayer*)this)->activity = oatIdling;
					}
					break;
				case afcJumpIfIntEquals:
					{
						if ( !ps->CheckParamTypes(1, stIntOrNone) || !txt_param )
							break;
						int eq = ps->GetInt();
						if ( eq == param )
						{
							SetAnimation(txt_param, false);
							return;
						}
						break;
					}
				case afcLocalJumpIfIntEquals:
					{
						if ( !ps->CheckParamTypes(2, stInt, stIntOrNone) )
							break;
						int jmp = ps->PopInt();
						int eq = ps->GetInt();
						if ( eq == param  )
							this->sprite->JumpFrame(jmp);
						break;
					}
				case afcLocalJumpIfNextIntEquals:
					{
						if ( !ps->CheckParamTypes(3, stInt, stIntOrNone, stIntOrNone) )
							break;
						int jmp = ps->PopInt();
						int eq = ps->GetInt(1);
						if ( eq == param  )
							this->sprite->JumpFrame(jmp);
						break;
					}
				case afcWait:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						this->sprite->offset = -ps->PopInt();
						return;
					}
				case afcPop:
					{
						StackElement* sd = this->sprite->stack->Pop();
						DELETESINGLE(sd);
						break;
					}
				case afcJumpIfTargetY:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int shift = ps->PopInt();
						ObjCharacter* target = NULL;
						ObjEnemy* oe = NULL;
						if ( this->type == objEnemy ) oe = (ObjEnemy*)this;
						if ( this->type == objEffect )
						{
							ObjDynamic* origin = (ObjDynamic*)this->parentConnection->getParent();
							oe = (ObjEnemy*)origin;
						}
						if ( oe ) target = oe->GetTarget();
						if ( !target && playerControl ) target = playerControl->current;

						if ( target && !target->IsDead())
						{
							if ( shift >= 0 && target->aabb.p.y + shift > this->aabb.p.y )
								this->sprite->JumpFrame( param );
							else if ( shift< 0 && target->aabb.p.y - shift < this->aabb.p.y )
								this->sprite->JumpFrame( param );
						}
						break;
					}
				case afcJumpIfTargetX:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int shift = ps->PopInt();
						ObjCharacter* target = NULL;
						ObjEnemy* oe = NULL;
						if ( this->type == objEnemy ) oe = (ObjEnemy*)this;
						if ( this->type == objEffect )
						{
							ObjDynamic* origin = (ObjDynamic*)this->parentConnection->getParent();
							oe = (ObjEnemy*)origin;
						}
						if ( oe ) target = oe->GetTarget();
						if ( !target && playerControl ) target = playerControl->current;
						
						if ( target && !target->IsDead())
						{
							if ( shift >= 0 && target->aabb.p.x + shift > this->aabb.p.x )
								this->sprite->JumpFrame( param );

							else if ( shift < 0 && target->aabb.p.x - shift < this->aabb.p.x )
								this->sprite->JumpFrame( param );

						}
						break;
					}
				case afcSetNearestWaypoint:
					{
						if ( this->type == objEnemy && Waypoints )
						{
							ObjWaypoint* wp = NULL;
							ObjWaypoint* target = NULL;
							float dist = 0.0f;
							for ( vector<GameObject*>::iterator iter = Waypoints->childrenConnection->children.begin(); 
										iter != Waypoints->childrenConnection->children.end(); ++iter)
							{
								wp = (ObjWaypoint*)(*iter);
								if ( target == NULL || (wp->aabb.p - aabb.p).Length() < dist )
								{
									target = wp;
									dist = (wp->aabb.p - aabb.p).Length();
								}
							}
							((ObjEnemy*)this)->current_waypoint = target->id;
						}
					}
					break;
				case afcFlyToWaypoint:
					if ( this->type == objEnemy )
					{
						ObjEnemy* oe = (ObjEnemy*)this;
						ps->CheckParamTypes(1, stIntOrNone);
						oe->waypoint_mode = ps->PopInt();
						
						if ( oe->waypoint_mode & 1 )
						{
							ps->CheckParamTypes(1, stIntOrNone);
							oe->waypoint_global = ps->PopInt(); 
						}
			
						if ( oe->waypoint_mode & 2 ) //������������ �������� ������
						{
							ps->CheckParamTypes(1, stInt);
							oe->waypoint_speed_limit = ps->PopInt() / 1000.0f;
						}

						else oe->waypoint_speed_limit = 0;

						oe->waypoint_start = Vector2(this->aabb.p.x, this->aabb.p.y);
						oe->waypoint_speed = param / 1000.0f;

						oe->movement = omtMovingToWaypoint;
					}
					break;
				case afcTeleportToWaypoint:
					if ( this->type == objEnemy )
					{
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjWaypoint* wp = NULL;
						wp = param == -1 ? (ObjWaypoint*)GetGameObject(oe->current_waypoint) : GetWaypoint( param );

						if (wp)
						{
							this->aabb.p = wp->aabb.p;
							oe->vel.x = oe->vel.y = 0;
							oe->acc.x = oe->acc.y = 0;
						}
					}
					break;
				case afcAdjustHomingAcc:
					if ( this->type == objBullet )
					{
						ps->CheckParamTypes(1, stIntOrNone);

						float speed_limit = ps->PopInt()/1000.0f;
						float speed = param/1000.0f;
						ObjBullet* ob = (ObjBullet*)this;
						ObjCharacter* target = NULL;
						GameObject* shooter = this->parentConnection->getParent();
						if ( shooter && shooter->type == objEnemy )
							target = ((ObjEnemy*)shooter)->GetTarget();
						if ( !target && playerControl ) target = playerControl->current;
						if ( !target ) break;
						float angle = atan2( target->aabb.p.y - ob->aabb.p.y, target->aabb.p.x - ob->aabb.p.x );
						ob->acc = speed * Vector2( cos(angle), sin(angle) );
						if ( abs(ob->acc.x) >= speed_limit ) ob->acc.x = 0;
						if ( abs(ob->acc.x) >= speed_limit ) ob->acc.y = 0;
					}
					break;
				case afcMoveToTargetX:
					if ( this->type == objEnemy )
					{
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = oe->GetTarget();
						float speed = param / 100.0f;
						if ( otarget )
						{
							if ( otarget->aabb.p.x > oe->aabb.p.x )
							{
								oe->vel.x = speed;
								oe->SetFacing( false );
							}
							else if ( otarget->aabb.p.x < oe->aabb.p.x )
							{
								oe->vel.x = -speed;
								oe->SetFacing( true );
							}
						}
						else this->SetAnimation( "idle", false );
					}
					break;
				case afcMoveToTargetY:
					if ( this->type == objEnemy )
					{
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = oe->GetTarget();
						float speed = param / 100.0f;
						if ( otarget )
						{
							if ( otarget->aabb.p.y > oe->aabb.p.y )
								oe->vel.y = speed;
							else if ( otarget->aabb.p.y < oe->aabb.p.y )
								oe->vel.y = -speed;
						}
					}
					break;
				case afcMoveToTarget:
					if ( this->type == objEnemy )
					{
						float speed = param / 100.0f;
						if ( ((ObjEnemy*)this)->target )
						{
							ObjEnemy* oe = (ObjEnemy*)this;
							ObjCharacter* otarget = oe->GetTarget();
							float angle = atan2( otarget->aabb.p.y - this->aabb.p.y, otarget->aabb.p.x - this->aabb.p.x );
							oe->vel = speed * Vector2( cos(angle), sin(angle) );
						}
					}
					break;
				case afcAdjustVelToTarget:
					{
						if ( !ps->CheckParamTypes(1, stIntOrNone) )
							break;
						float max_speed = static_cast<float>(ps->PopInt()) / 100.0f;
						float speed = param / 100.0f;
						if ( ((ObjEnemy*)this)->target )
						{
							ObjEnemy* oe = reinterpret_cast<ObjEnemy*>(this);
							ObjCharacter* otarget = oe->GetTarget();
							float angle = atan2( otarget->aabb.p.y - this->aabb.p.y, otarget->aabb.p.x - this->aabb.p.x );
							Vector2 adj = speed*Vector2( cos(angle), sin(angle) );
							if ( adj.x < 0.0f )
							{
								SetFacing( true );
							}
							else if ( adj.x > 0.0f )
							{
								SetFacing( false );
							}
							if ( max_speed == 0 || abs(oe->vel.x + adj.x) <= max_speed  )
								oe->vel.x += adj.x;
							if ( max_speed == 0 || abs(oe->vel.y + adj.y) <= max_speed )
								oe->vel.y += adj.y;
						}
					}
					break;
				case afcMountPointSet:
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( this->sprite->mp )
						{
							if ( param >= this->sprite->mpCount )
							{
								Log(DEFAULT_LOG_NAME, logLevelError, "Attemp to set non-existent mount point %i in anim %s.", this->sprite->mpCount, this->sprite->cur_anim.c_str());
								break;
							}
							if ( this->childrenConnection ) this->childrenConnection->Event( ObjectEventInfo( eventMPChanged, Vector2((float)x,(float)y)-this->sprite->mp[ param ], param ) );
							this->sprite->mp[ param ] = Vector2( (float)x, (float)y );
						}
						break;
					}
				case afcCreateParticles:
					{
						if ( !ps->CheckParamTypes(3, stIntOrNone, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						int mode = ps->PopInt();
						if ( this->GetFacing() ) x *= -1;
						//CAABB* emitter = ( param & 2 ? new CAABB(this->aabb) : NULL );
						UINT emitter = param & 2 ? this->id : 0 ;
						Vector2 coord = param & 8 ? Vector2((float)x,(float)y): this->aabb.p + Vector2((float)x, (float)y) ;
						GameObject* ops = ::CreateParticleSystem(txt_param, coord, emitter, mode);
						if (param & 1)
							ps->Push( ops->id );
					}
				break;
				case afcSetShadow:
					{
						if ( !this->IsPhysic() || !((ObjPhysic*)this)->IsDynamic() )
							break;
						((ObjDynamic*)this)->drops_shadow = 1 == param;
					}
				break;
				case afcCreateEnemyBullet:
					if ( this->type == objEnemy || this->type == objSprite || this->type == objEffect || this->type == objBullet )
					{
						ObjBullet* bullet = NULL;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( this->GetFacing() ) x *= -1;
						WeaponDirection wd = wdDownLeft;

						if ( param == 1 )
							if ( this->GetFacing() )
								 wd = wdDownLeft;
							else
								 wd = wdDownRight;
						else
						if ( param == 2 )
							if ( this->GetFacing() )
								wd = wdUpLeft;
							else
								wd = wdUpRight;
						else
							if ( this->GetFacing() )
								wd = wdLeft;
							else
								wd = wdRight;
						ObjCharacter* shooter = NULL;
						switch( type )
						{
							case objEnemy:
							{
								shooter = static_cast<ObjCharacter*>(this);
								break;
							}
							case objEffect:
							{
								if ( param == 3 ) shooter = static_cast<ObjCharacter*>(parentConnection->getParent());
								break;
							}
							default:
							{
								break;
							}
						}

						bullet = ::CreateBullet( txt_param, Vector2( this->aabb.p.x + x, this->aabb.p.y + y ),
												 shooter, wd );
						if ( this->type == objBullet )
							bullet->player_num = static_cast<ObjBullet*>(this)->player_num;
						if ( param == 4 && bullet) //TODO: ...
						{
							if ( y == 0 ) y = 1;
							bullet->t_value = x * (2.0f/y)*Const::Math::PI;
						}
						if (bullet && this->GetFacing()) bullet->SetFacing( true );
					}
					else if(this->type == objBullet)
					{
						ObjBullet* bullet = ::CreateBullet( txt_param, Vector2(this->aabb.p.x, this->aabb.p.y), (ObjCharacter*)this->parentConnection->getParent(), wdRight);
						bullet->player_num = static_cast<ObjBullet*>(this)->player_num;
					}
					break;
				case afcCreateEnemyRay:
					if ( this->type == objEnemy || this->type == objSprite || this->type == objEffect )
					{
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( this->GetFacing() ) x *= -1;
						int wd = 0;
						if ( this->GetFacing() )
								wd = param;
						else
								wd = 180 - param;
						ObjRay* ray = CreateRay(txt_param, this->aabb.p + Vector2((float)x,(float)y), (ObjCharacter*)this, static_cast<float>(wd));
						if (ray && this->GetFacing()) ray->SetFacing( true );
					}
					else if(this->type == objBullet)
					{
						CreateRay( txt_param, Vector2(this->aabb.p.x, this->aabb.p.y), (ObjCharacter*)this->parentConnection->getParent(), wdRight);
					}
					break;
				case afcEnemyClean:
					{
						if ( fabs(this->aabb.p.x+this->aabb.W-CAMERA_X) >= 320+param
							/*|| fabs(this->aabb.p.y+this->aabb.H-CAMERA_Y) >= 240+param*/  )
							this->SetDead();
					}
					break;
				case afcAdjustAim:
					{
						ObjEnemy* oen = NULL;
						if ( this->type == objEnemy ) oen = (ObjEnemy*)this;
						else if ( this->type == objEffect )
						{
							ObjDynamic* origin = (ObjDynamic*)this->parentConnection->getParent();
							oen = (ObjEnemy*)origin;
						}
						if (!oen) break;
						ObjCharacter* target = oen->GetTarget();
						switch ( param )
						{
							case 1:		//���� ����� ���� �� ���������.
								{
									if ( !ps->CheckParamTypes(2, stInt, stInt) )
										break;
									int y = ps->PopInt();
									int x = ps->PopInt();
									if ( target )
										oen->aim = target->aabb.p + Vector2((float)x, (float)y);
									else
										oen->aim = Vector2((float)x, (float)y);
								}
								break;
							case 2:		//�������� ������������ ������������ ������.
								{
									if ( !ps->CheckParamTypes(2, stInt, stInt) )
										break;
									int y = ps->PopInt();
									int x = ps->PopInt();
									oen->aim = oen->aabb.p + Vector2((float)x, (float)y);
								}
								break;
							case 3:		//���������� ����������
								{
									if ( !ps->CheckParamTypes(2, stInt, stInt) )
										break;
									int y = ps->PopInt();
									int x = ps->PopInt();
									oen->aim = Vector2((float)x, (float)y);
								}
								break;
							case 4:		//����� �� ������
								{
									if (playerControl && playerControl->current) oen->aim = playerControl->current->aabb.p;
								}
								break;
							case 5:		//�� ����������� ������ �� ��� ���� ��� �����
								{
									if ( !ps->CheckParamTypes(1, stInt) )
										break;
									int y = ps->PopInt();
									if ( target ) oen->aim = target->aabb.p + Vector2( 0, target->aabb.H - y );
									else if (playerControl && playerControl->current) oen->aim = playerControl->current->aabb.p+Vector2(0, playerControl->current->aabb.H-y);
								}
								break;
							default:	//� ���������� ������ ������ ���� ���������� ������� ����
								{
									if ( target )
										oen->aim = target->aabb.p;
									else
										oen->aim = Vector2(0,0);
								}
						}
					}

					break;
				case afcAimedShot:
					if ( (this->type == objEnemy || this->type == objEffect ) )
					{
						ObjBullet* bullet = NULL;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( this->GetFacing() ) x *= -1;
						Vector2 aim_target;
						if (this->type == objEnemy)
						{
							aim_target = ((ObjEnemy*)this)->aim;
						}
						else
						{
							assert(this->type == objEffect);
							ObjEnemy* oe = (ObjEnemy*)this->parentConnection->getParent();
							if (oe)
								aim_target = oe->aim;
						}						
						bullet = ::CreateBullet( txt_param, Vector2( this->aabb.p.x + x, this->aabb.p.y + y ),
							(ObjCharacter*)this, aim_target, param );
						if (bullet && this->GetFacing() ) bullet->SetFacing( true );
						//if ( txt_param && 1 ) bullet->max_y_vel += 0.8; //<- WTF?
					}
					else if(this->type == objBullet)
					{
						/*ObjBullet* bul = NULL;

						bul = */::CreateBullet( txt_param, Vector2(this->aabb.p.x, this->aabb.p.y), (ObjCharacter*)this->parentConnection->getParent(), wdRight);

					}
					break;
				case afcAngledShot:
					//if ( true )
					{
						ObjBullet* bullet = NULL;
						if ( !ps->CheckParamTypes(5, stInt, stInt, stInt, stInt, stInt) )
							break;
						int ani = ps->PopInt();
						int angle = ps->PopInt();
						int y = ps->PopInt();
						int x = ps->PopInt();
						int mode = ps->PopInt();
						ObjDynamic* shooter = playerControl ? playerControl->current : NULL;
						if ( mode & 2 )
							shooter = (ObjDynamic*)this;
						if ( this->GetFacing() )
						{
							x *= -1;
							angle *= -1;
						}
						bullet = ::CreateAngledBullet( txt_param, Vector2( this->aabb.p.x + x, this->aabb.p.y + y ),
							shooter, this->GetFacing(), angle, ani );
						if ( mode & 1 )
						{
							bullet->max_x_vel += fabs(shooter->vel.x);
							bullet->vel.x += fabs(shooter->vel.x);
						}
					}
					break;
				case afcRandomAngledSpeed:
					//if ( true )
					{
						ObjDynamic* od = (ObjDynamic*)this;
						if ( !ps->CheckParamTypes(4, stInt, stInt, stInt, stInt) )
							break;
						int max_angle = ps->PopInt();
						int min_angle = ps->PopInt();
						float max_speed = ps->PopInt()/1000.0f;
						float min_speed = ps->PopInt()/1000.0f;
						float speed = Random_Float(min_speed, max_speed);
						float angle = ((float)Random_Int(min_angle, max_angle))*Const::Math::PI/180;
						od->vel = speed * Vector2( cos(angle), sin(angle) );
						if ( (param & 1) )
						{
							od->max_x_vel = max( od->max_x_vel, abs(od->vel.x) );
							od->max_y_vel = max( od->max_y_vel, abs(od->vel.y) );
						}
					}
					break;
				case afcFaceTarget:
					if ( this->type == objEnemy )

					{
						ObjEnemy* oe = (ObjEnemy*)this;
						ObjCharacter* otarget = oe->GetTarget();
						if ( otarget )
						{
							if ( oe->aabb.p.x > otarget->aabb.p.x )
								oe->SetFacing( true );
							if ( oe->aabb.p.x < otarget->aabb.p.x )
								oe->SetFacing( false );
						}
					}
					break;
				case afcCreateEffect:
					{
						ObjEffect* of = NULL;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						//� ������� ������ �������� �� x ��������� � ����������� ������� ���������.
						if ( (param & 4 ) && this->GetFacing() )
							x *= -1;
						
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						int mpoint = ps->PopInt();
						
						if ( this->type == objBullet )
							of = ::CreateEffect( txt_param,
							false, (ObjDynamic*)this->parentConnection->getParent(), (USHORT)mpoint, Vector2( (float)x, (float)y ) );
						else if ( this->type == objRay )
							of = ::CreateEffect( txt_param,
								false, (ObjDynamic*)this->parentConnection->getParent(), (USHORT)mpoint, Vector2( (float)x, (float)y ) );
						else
							of = ::CreateEffect( txt_param,
												  false, (ObjDynamic*)this, (USHORT)mpoint, Vector2( (float)x, (float)y ) );
						if (of)
						{
							//���� ������ ����� �������, �� ������� IsMirrored
							if ( (param & 1 ) && this->GetFacing() )
								of->SetFacing( true );
							//���� ������ ����� �������, �� �������� �������������� ������� ����� ������� � �����.
							if ( param & 2 )
							{
								of->aabb.p -= Vector2( ((float)(of->sprite->frameWidth))/2, ((float)(of->sprite->frameHeight))/2 );
								//������ �� ������� ��������, ���� ����� ��������
								//if ( this->type == objBullet && ( ((ObjPhysic*)obj)->IsDynamic() ) ) //� ���� ��� ��� � ���� � ����������� ������ �������� - ������� ��������.
								//	((ObjDynamic*)obj)->vel = ((ObjBullet*)this)->shooter->vel;

							}
						}
						else
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning,
								"Error creating object %s in anim %s:%d",
								txt_param, this->sprite->cur_anim.c_str(), a->current);
						}

					}
					break;
				case afcCreateObject:
					{
						GameObject* obj = NULL;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						//� ������� ������ �������� �� x ��������� � ����������� ������� ���������.
						if ( (param & 4 ) && this->GetFacing() )
							x *= -1;
						obj = ::CreateSprite( txt_param, Vector2(  this->aabb.p.x+x, this->aabb.p.y+y ), false, NULL);

						if (obj)
						{
							//���� ������ ����� �������, �� ������� IsMirrored
							if ( (param & 1 ) && this->GetFacing() )
								obj->SetFacing( true );
							//���� ������ ����� �������, �� �������� �������������� ������� ����� ������� � �����.
							if ( param & 2 )
							{
								obj->aabb.p -= Vector2( ((float)(obj->sprite->frameWidth)/2), ((float)(obj->sprite->frameHeight))/2 );
								//������ �� ������� ��������, ���� ����� ��������
								//if ( this->type == objBullet && ( ((ObjPhysic*)obj)->IsDynamic() ) ) //� ���� ��� ��� � ���� � ����������� ������ �������� - ������� ��������.
								//	((ObjDynamic*)obj)->vel = ((ObjBullet*)this)->shooter->vel;

							}
						}
						else
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning,
								"Error creating object %s in anim %s:%d",
								txt_param, this->sprite->cur_anim.c_str(), a->current);
						}

						break;
					}
				case afcCreateEnemy:
					{
						ObjEnemy* obj = NULL;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						if ( (param & 1) && this->GetFacing() )
							x *= -1;
						obj = ::CreateEnemy( txt_param, Vector2( aabb.GetMin(0)+x, aabb.GetMin(1)+y ), "init");
						if (obj)
						{
							if ( (param & 2) && this->GetFacing() )
								obj->SetFacing( true );
							this->sprite->stack->Push(obj->id);
							if ( (param & 4) )
							{
								ObjectConnection::addChild( this, obj );
								obj->attached = true;
							}
							if ( (param & 8) && this->sprite && obj->sprite )	//Inherit sprite and overlay colors
							{
								obj->sprite->color = this->sprite->color;
								for ( int i = 0; (UINT)i < min( this->sprite->overlayCount, obj->sprite->overlayCount ); i++ )
								{
									obj->sprite->ocolor[i] = this->sprite->ocolor[i];
								}
							}
						}
						break;
					}
				case afcSummonObject:
					{
						ObjDynamic* obj = NULL;
						if ( !ps->CheckParamTypes(3, stInt, stInt, stInt) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						UINT oid = ps->PopInt();
						if ( (param & 1) && this->GetFacing() )
							x *= -1;
						obj = (ObjDynamic*)GetGameObject( oid );
						if (obj)
						{
							obj->aabb.p = Vector2( aabb.GetMin(0)+x, aabb.GetMin(1)+y );
							if ( (param & 2) && this->GetFacing() )
								obj->SetFacing( true );
							this->sprite->stack->Push(obj->id);
						}
						break;
					}
				case afcJumpIfStackIsNotEmpty:
					{
						if ( !ps->isEmpty() )
						{
							this->sprite->JumpFrame( param );
						}
					}
					break;
				case afcJumpIfWaypointClose:
					{
						if ( !ps->CheckParamTypes(2, stInt, stInt) )
							break;
						int num = ps->PopInt();
						int dist = ps->PopInt();
						ObjWaypoint* wp = GetWaypoint( num );
						if ( wp && ( wp->aabb.p - aabb.p ).Length() < dist )
						{
							this->sprite->JumpFrame( param );
						}
					}
					break;
				case afcMirror:
					{
						this->SetFacing( !this->GetFacing() );
					}
					break;
				case afcCreateItem: //��������� ����� ���������
					{
						ObjItem* obj = NULL;
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int y = ps->PopInt();
						int x = ps->PopInt();
						//� ������� ������ �������� �� x ��������� � ����������� ������� ���������.
						if ( (param & 4 ) && this->GetFacing() )
							x *= -1;
						//����� ������ ������ ������.
						obj = ::CreateItem( txt_param, Vector2(  this->aabb.p.x+x, this->aabb.p.y+y ), NULL);

						if (obj)
						{
							//���� ������ ����� �������, �� ������� IsMirrored
							if ( (param & 1 ) && this->GetFacing() )
								obj->SetFacing( true );
							//���� ������ ����� �������, �� �������� �������������� ������� ����� ������� � �����.
							if ( param & 2 )
							{
								obj->aabb.p -= Vector2( ((float)(obj->sprite->frameWidth))/2, ((float)(obj->sprite->frameHeight))/2 );
							}
						}
						else
						{
							Log(DEFAULT_LOG_NAME, logLevelWarning,
								"Error creating object %s in anim %s:%d",
								txt_param, this->sprite->cur_anim.c_str(), a->current);
						}
						break;
					}
				case afcDestroyObject:
					{
						if ( !(param & 2) )
						{
							this->SetDead();
							return;
						}
						if ( param & 1 && this->childrenConnection )
						{
							for ( vector<GameObject*>::iterator iter = childrenConnection->children.begin(); iter != childrenConnection->children.end(); ++iter)
								(*iter)->SetDead();
						}
						break;
					}
				case afcSetWeaponAngle:
					if ( ch )
					{
						ch->target_weapon_angle = ch->GetFacing() ? (param * Const::Math::PI) / 180.0f : -(param * Const::Math::PI) / 180.0f;
					}
					break;
				case afcSetTrajectory:
					if ( IsPhysic() && reinterpret_cast<ObjPhysic*>(this)->IsDynamic() )
					{
						reinterpret_cast<ObjDynamic*>(this)->trajectory = static_cast<TrajectoryType>(param);
					}
					break;
				case afcJumpIfWeaponReady:
					if ( this->type == objPlayer ) //������ � ������ ���� ���������� ������
					{
						ObjPlayer* op = (ObjPlayer*)this;
						if (op->cur_weapon && op->cur_weapon->IsReady() && op->ammo > 0 )
						{
							this->sprite->JumpFrame( param );
						}
					}
					break;
				case afcPlaySound:
					//assert(snd);
					if (soundMgr)
					{
						Vector2 origin = this->aabb.p;

						if ( param & 1 ) origin = Vector2( CAMERA_X, CAMERA_Y );
						float volume = 1.0f;
						if ( param & 2 ) 
						{
							if ( !ps->CheckParamTypes(1, stIntOrNone) )
								break;
							volume = static_cast<float>(ps->PopInt() / 255.0f);
						}
						soundMgr->PlaySnd(string(txt_param), !((param & 4) != 0), origin );
					}
					break;
				case afcMaterialSound:
					{
						const char * sound_name = this->GetSound(param);
						if (!sound_name) break;
						if (soundMgr)
							soundMgr->PlaySnd(string(sound_name), true, this->aabb.p);
						break;
					}
				case afcEnvSound:
					{
						//assert(snd);
						char * sound_name = ((ObjDynamic*)this)->env->GetSound(param);
						if (!sound_name) break;
						if (soundMgr)
							soundMgr->PlaySnd(string(sound_name), true, this->aabb.p);
					}
					break;
				case afcMaterialSprite:
					{
						const char * sprite_proto = this->GetSprite(static_cast<size_t>(param));
						if (!sprite_proto) break;
						GameObject* obj = NULL;
						if ( !ps->CheckParamTypes(3, stInt, stIntOrNone, stIntOrNone) )
							break;
						int sparam = ps->PopInt();
						int y = ps->PopInt();
						int x = ps->PopInt();
						//� ������� ������ �������� �� x ��������� � ����������� ������� ���������.
						if ( (sparam & 4 ) && this->GetFacing() )
							x *= -1;
						obj = ::CreateSprite( sprite_proto, Vector2(  this->aabb.p.x+x, this->aabb.p.y+y ), false, NULL, !(sparam & 8));
						if ( !obj ) break;
						//���� ������ ����� �������, �� ������� IsMirrored
						if ( (sparam & 1 ) && this->GetFacing() )
							obj->SetFacing( true );
						if ( sparam & 2 )
							obj->aabb.p -= Vector2( ((float)(obj->sprite->frameWidth))/2, ((float)(obj->sprite->frameHeight))/2 );
						break;
					}
				case afcEnvSprite:
					{
						char * sprite_proto = ((ObjDynamic*)this)->env->GetSprite(param);
						if (!sprite_proto) break;
						GameObject* obj = NULL;
						if ( !ps->CheckParamTypes(3, stInt, stIntOrNone, stIntOrNone) )
							break;
						int sparam = ps->PopInt();
						int y = ps->PopInt();
						int x = ps->PopInt();
						//� ������� ������ �������� �� x ��������� � ����������� ������� ���������.
						if ( (sparam & 4 ) && this->GetFacing() )
							x *= -1;
						obj = ::CreateSprite( sprite_proto, Vector2(  this->aabb.p.x+x, this->aabb.p.y+y ), false, NULL);

						if (obj)
						{
							//���� ������ ����� �������, �� ������� IsMirrored
							if ( (sparam & 1 ) && this->GetFacing() )
								obj->SetFacing( true );
							//���� ������ ����� �������, �� �������� �������������� ������� ����� ������� � �����.
							if ( sparam & 2 )
							{
								obj->aabb.p -= Vector2( ((float)(obj->sprite->frameWidth))/2, ((float)(obj->sprite->frameHeight))/2 );
								//������ �� ������� ��������, ���� ����� ��������
								//if ( this->type == objBullet && ( ((ObjPhysic*)obj)->IsDynamic() ) ) //� ���� ��� ��� � ���� � ����������� ������ �������� - ������� ��������.
								//	((ObjDynamic*)obj)->vel = ((ObjBullet*)this)->shooter->vel;

							}
						}
						else

						{
							Log(DEFAULT_LOG_NAME, logLevelWarning,
								"Error creating object %s in anim %s:%d",
								sprite_proto, this->sprite->cur_anim.c_str(), a->current);

						}

						break;
					}
				case afcCallFunction:
					{
						if (!txt_param)
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, 
								"Function name is not specified. proto_name %s, anim %s, frame%d", 
								this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current);
							break;
						}
						lua_getglobal(lua, txt_param);
						if (!lua_isfunction(lua, -1))
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, 
								"The type of '%s' is %s, not a function. proto_name %s, anim %s, frame %d", 
								txt_param, lua_typename(lua, lua_type(lua, -1)), this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current);
							lua_pop(lua, 1);
							break;
						}

						lua_pushinteger(lua, this->id);
						lua_pushinteger(lua, param);
						if (lua_pcall(lua, 2, 0, 0) != 0 )
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
								"An error occurred while executing function %s(%d,%d), proto_name %s, anim %s, frame %d: %s",
								txt_param, this->id, param, this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current, lua_tostring(lua, -1) );
							lua_pop(lua, 1);
							break;
						}

						break;
					}
				case afcSetProcessor:
					{
						SCRIPT::ReleaseProc(&scriptProcess);

						if (!txt_param)
						{
							if ( param < 0 )							
							{
								//Everything is quite fine, we are just removing an object processor
								break;
							}

							SCRIPT::GetFromRegistry(lua, param);
							if ( lua_isfunction(lua, -1) )
							{
								this->scriptProcess = param;
								SCRIPT::ReserveProc(scriptProcess);
							}

							break;
						}

						lua_getglobal(lua, txt_param);
						if (!lua_isfunction(lua, -1))
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, 
								"The type of '%s' is %s, not a function. proto_name %s, anim %s, frame %d", 
								txt_param, lua_typename(lua, lua_type(lua, -1)), this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current);
							lua_pop(lua, 1);
							break;
						}

						SCRIPT::RegProc(lua, &this->scriptProcess, -1);

						break;
					}
				case afcCustomBullet:
					{
						if ( !ps->CheckParamTypes(2, stIntOrNone, stIntOrNone) )
							break;
						int dy = ps->PopInt();
						int dx = ps->PopInt();
						lua_getglobal(lua, "custom_weapon");
						if (!lua_isfunction(lua, -1))
						{
							break;
						}
						ObjCharacter* oc = NULL;
						if ( this->parentConnection && this->parentConnection->getParent() )
							oc = (ObjCharacter*)(this->parentConnection->getParent());

						lua_pushinteger(lua, this->id);
						lua_pushinteger(lua, oc->id);
						lua_pushinteger(lua, param);
						lua_pushnumber(lua, (oc->weapon_angle * 180.0f)/ Const::Math::PI );
						lua_pushnumber(lua, aabb.p.x + dx);
						lua_pushnumber(lua, aabb.p.y + dy);
						if (lua_pcall(lua, 6, 0, 0) != 0)
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
								"Error executing function %s(%d,%d), proto_name %s, anim %s, frame %d: %s",
								txt_param, this->id, param, this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current, lua_tostring(lua, -1) );
							lua_pop(lua, 1);
							break;
						}

						break;
					}
				case afcCallFunctionWithStackParameter:
					{
						if ( !ps->CheckParamTypes(1, stInt) )
							break;
						if (!txt_param)
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, 
								"Function name is not specified. proto_name %s, anim %s, frame %d", 
								this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current);
							break;
						}
						lua_getglobal(lua, txt_param);
						if (!lua_isfunction(lua, -1))
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, 
								"The type of '%s' is %s, not a function. proto_name %s, anim %s, frame %d", 
								txt_param, lua_typename(lua, lua_type(lua, -1)), this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current);
							lua_pop(lua, 1);
							break;
						}

						lua_pushinteger(lua, this->id);
						lua_pushinteger(lua, ps->PopInt());
						if (lua_pcall(lua, 2, 0, 0) != 0 )
						{
							Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
								"Error executing function %s(%d,%d), proto_name %s, anim %s, frame %d: %s",
								txt_param, this->id, param, this->sprite->proto_name, this->sprite->cur_anim.c_str(), a->current, lua_tostring(lua, -1) );
							lua_pop(lua, 1);
							break;
						}

						break;
					}
				case afcNone:
				default:
					break;
				} //switch (af.command)
			} // while (!s->ChangeFrame(a))

#ifdef SELECTIVE_RENDERING
			if (sprite->renderChanged || tmp_old_aabb != aabb)
			{
				selectForRenederUpdate(this);
				sprite->renderChanged = false;
			}
#endif

		} // if (a)
	} // if (s->animNames)
}

void GameObject::ObjectConnection::parentDead()
{
	this->orphan = true;
	if ( this->children.size() == 0 )
	{
		ObjectConnection::removeConnection( this );
	}
}

GameObject* GameObject::ObjectConnection::getParent()
{
	return this->orphan ? NULL : this->parent;
}


void GameObject::ObjectConnection::childDead( UINT id )

{
	for ( vector<GameObject*>::iterator iter = children.begin(); iter != children.end(); ++iter)
	{
		if ( (*iter)->id == id )
		{
			children.erase( iter );
			break;
		}
	}

	if ( this->children.size() == 0 && this->orphan )
	{
		GameObject::ObjectConnection::removeConnection( this );
	}
}

void GameObject::ObjectConnection::_addChild( GameObject* child )
{
	this->children.push_back( child );
}

void GameObject::ObjectConnection::addChild( GameObject* parent, GameObject* child )
{
	if ( !parent->childrenConnection )
		parent->childrenConnection = new ObjectConnection( parent );
	parent->childrenConnection->_addChild( child );
	child->parentConnection = parent->childrenConnection;
}

void GameObject::ObjectConnection::Event( ObjectEventInfo event_info )
{
	if ( this->children.size() == 0 )
		return;
	
	for ( vector<GameObject*>::iterator iter = children.begin(); iter != children.end(); ++iter)
		(*iter)->ParentEvent( event_info );
}

