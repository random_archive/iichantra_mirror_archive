#ifndef __OBJECT_RIBBON_H_
#define __OBJECT_RIBBON_H_

#include "object.h"

class ObjRibbon: public GameObject
{
public:
	float bl, br, bt, bb;
	bool ubl, ubr, ubt, ubb;		// bounds usage
	
	bool repeat_x, repeat_y;		// направления повторения
	Vector2 k;
	
#ifdef MAP_EDITOR
	bool from_proto;
#endif //MAP_EDITOR
	
	float getBL() const { return bl; }
	float getBR() const { return br; }
	float getBT() const { return bt; }
	float getBB() const { return bb; }

	float getUBL() const { return ubl; }
	float getUBR() const { return ubr; }
	float getUBT() const { return ubt; }
	float getUBB() const { return ubb; }

	enum ORBoundSide { orBoundLeft, orBoundRight, orBoundTop, orBoundBottom };

	float getBound( ORBoundSide side );

	void setBounds( float x1, float y1, float x2, float y2 );
	void setBoundsUsage( bool x1, bool y1, bool x2, bool y2 );
	void setRepitition( bool x, bool y );

#ifdef SELECTIVE_RENDERING
	virtual bool GetDrawAABB(CAABB& res) const;
#endif
	virtual void Draw();

	ObjRibbon() :
		bl(0.0f), br(1.0f), 
		bt(0.0f), bb(1.0f),
		ubl(false), ubr(false), 
		ubt(false), ubb(false),
		repeat_x(true), 
		repeat_y(true),
		k(1.0f, 1.0f)
#ifdef MAP_EDITOR
		, from_proto(false)
#endif //MAP_EDITOR
	{
		type = objRibbon;
	}
};

ObjRibbon* CreateRibbon( const char* texture, Vector2 k, Vector2 coord, float z, bool from_proto );
ObjRibbon* CreateDummyRibbon();
#endif //__OBJECT_RIBBON_H_