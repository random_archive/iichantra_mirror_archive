#include "StdAfx.h"

#include "object_sprite.h"
#include "object_effect.h"

#include "../net.h"
#include "../object_mgr.h"
#include "../sprite.h"

#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;
extern bool netgame;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

GameObject* CreateColorBox(const CAABB& aabb, float z, const RGBAf& color, UINT id)
{
	if ( netgame && !Net::IsServer() && id )
		return NULL;

	GameObject* obj = new GameObject;
	obj->aabb = aabb;

	obj->sprite = new Sprite();
	obj->sprite->color = color;
	if (color.a < 1.0f)
	{
		obj->sprite->blendingMode = bmSrcA_OneMinusSrcA;
	}
	obj->sprite->render_without_texture = true;
	obj->sprite->SetVisible();
	obj->sprite->z = z;
	obj->sprite->frameWidth = (USHORT)aabb.W * 2;
	obj->sprite->frameHeight = (USHORT)aabb.H * 2;
	obj->type = objSprite;
	AddObject(obj, id);
	if ( obj->id == 0 )
	{
		if ( obj->parentConnection ) obj->parentConnection->childDead( 0 );
		DELETESINGLE( obj );
		return NULL;
	}
	return obj;
}

GameObject* CreateSprite(const Proto* proto, Vector2 coord, bool fixed, const char* start_anim, bool resize, CAABB size, bool corner_adjust, UINT id)
{
	if (!proto)
		return NULL;

	GameObject* object = NULL;
	if (proto->physic)
	{
		object = new ObjPhysic;
	}
	else if (proto->effect)
	{
		object = new ObjEffect;
		object->type = objEffect;
	}
	else
	{
		object = new GameObject;
		object->objFlags = 0;
	}

	if (!object->ApplyProto(proto))
	{
		DELETESINGLE(object);
		return NULL;
	}

	if (object->type != objEffect) object->type = objSprite;  //������ �����-��. ���� ��������, ��� ��������� �� �������.

	if (fixed) object->sprite->SetFixed();
	object->aabb.p = coord;

	// TODO: ������ ���. ��� �������� � ���������� � ���������. �� ����� ������ �� ��������.
	// � ���� ������ ������� � SAP ��� �� ��������, ������� � SetAnimation ������ ��� ���������
	// ��� ��������� � SAP.
	object->ClearPhysic();
	object->SetAnimation(start_anim ? start_anim : "idle", true);
	if (!corner_adjust) object->aabb.p = coord;

#ifdef COORD_LEFT_UP_CORNER
	if ( corner_adjust )
	{
		if (object->sprite->tex)
		{
			object->aabb.p.x += object->sprite->tex->frames->frame->size.x * 0.5f;
			object->aabb.p.y += object->sprite->tex->frames->frame->size.y * 0.5f;
		}
		else
		{
			object->aabb.p.x += object->sprite->frameWidth * 0.5f;
			object->aabb.p.y += object->sprite->frameHeight * 0.5f;
		}
	}
#endif // COORD_LEFT_UP_CORNER
	if (resize) 
	{
		object->aabb = size;
#ifdef MAP_EDITOR
		object->grouped = true;
#endif //MAP_EDITOR
	}

	if (proto->physic) 
	{
		object->SetPhysic();
		ObjPhysic* op = (ObjPhysic*)object;
		op->rectangle[0] = Vector2(-op->aabb.W, -op->aabb.H);
		op->rectangle[1] = Vector2( op->aabb.W, -op->aabb.H);
		op->rectangle[2] = Vector2( op->aabb.W,  op->aabb.H);
		op->rectangle[3] = Vector2(-op->aabb.W,  op->aabb.H);
		op->rectangle.CalcBox();
	}

	AddObject(object, id);
	if (object->id == 0)
	{
		DELETESINGLE(object);
		return NULL;
	}

	return object;
}

GameObject* CreateSprite(const char* proto_name, Vector2 coord, bool fixed, const char* start_anim, CAABB size, bool corner_adjust, UINT id)
{
	if (!proto_name)
		return NULL;
	GameObject* obj = CreateSprite(protoMgr->GetByName(proto_name, "sprites/"), coord, fixed, start_anim, true, size, corner_adjust, id);
	obj->sprite->setRenderMethod(rsmRepeatXY);
	//obj->aabb = size;
#ifdef MAP_EDITOR
	obj->proto_name = new char[ strlen(proto_name) + 1 ];
	strcpy( obj->proto_name, proto_name );
	obj->creation_shift = obj->aabb.p - coord;
#endif //MAP_EDITOR
	return obj;
}

GameObject* CreateSprite(const char* proto_name, Vector2 coord, bool fixed, const char* start_anim, bool corner_adjust, UINT id)
{
	if (!proto_name)
		return NULL;

	GameObject* go = CreateSprite(protoMgr->GetByName(proto_name, "sprites/"), coord, fixed, start_anim, false, CAABB(), corner_adjust, id);
#ifdef MAP_EDITOR
	go->proto_name = new char[ strlen(proto_name) + 1 ];
	strcpy( go->proto_name, proto_name );
	go->creation_shift = go->aabb.p - coord;
#endif //MAP_EDITOR
	return go;
}


GameObject* CreateDummySprite(bool physic/* = false*/)
{
	GameObject* obj = new GameObject;
	
	if (obj && physic)
	{
		assert(obj->aabb.W > 0 && obj->aabb.H > 0);
		obj->SetPhysic();
	}

	AddObject(obj);

	return obj;
}