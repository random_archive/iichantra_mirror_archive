#ifndef __OBJECT_PLAYER_H_
#define __OBJECT_PLAYER_H_

#include "object_character.h"

//#define GOD_MODE

class Player;
class ObjEffect;
struct Input;

class ObjPlayer : public ObjCharacter
{
public:
	Player* player;
	bool controlEnabled;            // ����� �� ����� ���������	
	Weapon* alt_weapon;             // �������������� ������
	Weapon* charging_weapon;
	UINT ammo;
	UINT weapon_charge_time;
	UINT last_weapon_charge;
	int camera_offset;              //��������� �������� �����-����
	USHORT additional_jumps;
	USHORT walljumps;
	USHORT jump_reserve;            //���������� �������, ��������� �� �����������
	USHORT walljump_reserve;
	bool jump_complete;             //����� ����� ������
	bool charge_shot_complete;
	float recovery_time;
	float old_weapon_angle;
	float air_control;
	float walljump_vel;
	UINT recovery;
	ObjEffect* charge_effect;

	Input* input;

	UINT last_on_plane;

	char* name;
	RGBAf color;
	
	ObjPlayer() :
		player(NULL),
		controlEnabled(true),
		alt_weapon(NULL),
		charging_weapon(NULL),
		ammo(100),
		weapon_charge_time(0),
		last_weapon_charge(0),
		camera_offset(0),
		additional_jumps(0),
		walljumps(0),
		jump_reserve(0),
		walljump_reserve(0),
		jump_complete(true),
		charge_shot_complete(true),
		recovery_time(0.0f),
		old_weapon_angle(0.0f),
		air_control(0.3f),
		walljump_vel(0.0f),
		recovery(50),
		charge_effect(NULL),
		input(NULL),
		last_on_plane(current_time),
		name(NULL),
		color(0, 0, 0, 1)
	{
#ifdef GOD_MODE
		is_invincible = true;
#endif // GOD_MODE
		this->type = objPlayer;
		this->gravity = Vector2(0.0f, 0.8f);
		this->health = 50;
	}

	~ObjPlayer();

	virtual bool ApplyProto(const Proto* proto);

	virtual void Process();
	void ChangeWeapon();
	void SetAmmo( int amount );
	void RecieveAmmo( int amount );
	bool HasAmmo();
	bool WeaponReady( bool alt_weapon = false );
	void SpendAmmo();
	void GiveWeapon(const char* weapon_name, bool primary);
	void ReceiveDamage( UINT damage, UINT damage_type, uint8_t player_num = 0 );
	void SetChargeEffect( ObjEffect* effect );
	bool CanShoot();
	virtual void Draw();
protected:
	virtual CUData* createUData();
};

ObjPlayer* CreatePlayer(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust = true);
ObjPlayer* CreateDummyPlayer();

#endif // __OBJECT_PLAYER_H_
