#include "StdAfx.h"
#include "object_spawner.h"
#include "object_enemy.h"
#include "object_particle_system.h"
#include "../camera.h"
#include "../object_mgr.h"
#include "../editor.h"

extern float CAMERA_GAME_LEFT;
extern float CAMERA_GAME_RIGHT;
extern float CAMERA_GAME_TOP;
extern float CAMERA_GAME_BOTTOM;
extern float CAMERA_X;
extern float CAMERA_Y;

#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

ObjSpawner::ObjSpawner( const char* enemyType, UINT maximumEnemies, UINT enemySpawnDelay, directionType direction, UINT size, UINT respawn_dist, UINT respawnDelay, int spawnLimit /*= UNLIMITED_SPAWN*/ ) : 
	enemyType(StrDupl(enemyType)), 
	spawnLimit(spawnLimit),
	maximumEnemies(maximumEnemies), 
	enemySpawnDelay(enemySpawnDelay), 
	respawnDelay(respawnDelay),
	last_time_on_screen(0),
	size(size), 
	respawn_dist(respawn_dist), 
	direction(direction),
	charges(maximumEnemies),
	cooldown(0),
	directionBeforeScript(dirNone)
{
	this->type = objSpawner;
	this->childrenConnection = new ObjectConnection( this );
}

ObjSpawner::~ObjSpawner()
{
	DELETESINGLE( this->enemyType );
#ifdef MAP_EDITOR
	DELETESINGLE( this->sprite );
#endif // MAP_EDITOR
}

__INLINE bool ObjSpawner::onScreen( float size )
{
	if ( aabb.p.x - size < CAMERA_GAME_RIGHT && aabb.p.x + size > CAMERA_GAME_LEFT &&
		 aabb.p.y + size > CAMERA_GAME_TOP   && aabb.p.y - size < CAMERA_GAME_BOTTOM )
		 return true;
	return false;
}

__INLINE void ObjSpawner::SpawnEnemy()
{
	ObjEnemy* oe = CreateEnemy( enemyType, aabb.p, NULL, false);
	if (!oe)
		return;

	if ( direction == dirLeft || direction == dirRight )
		oe->SetFacing( direction != dirLeft );
	Vector2 coord = this->aabb.p;

	/*switch ( direction )
	{
	case dirDown:
		{
			coord.y = CAMERA_GAME_BOTTOM + oe->aabb.H;
			break;
		}
	case dirUp:
		{
			coord.y = CAMERA_GAME_TOP - oe->aabb.H;
			break;
		}
	case dirLeft:
		{
			coord.x = CAMERA_GAME_LEFT - oe->aabb.W;
			break;
		}
	case dirRight:
		{
			coord.x = CAMERA_GAME_RIGHT + oe->aabb.W;
			break;
		}
	default: break;
	}*/

	oe->aabb.p = coord;
	ObjectConnection::addChild( this, oe );
}

bool ObjSpawner::ScriptLaunch()
{
	return ChangeDirection(dirScript);
}

bool ObjSpawner::ChangeDirection(directionType dir)
{
	if (dir == direction)
		return false;

	if (dir == dirScript)
	{
		directionBeforeScript = direction;
		charges = maximumEnemies;
		cooldown = 0;
	}

	direction = dir;

	return true;
}

void ObjSpawner::Process()
{
	/*#ifdef MAP_EDITOR
		spriteObj->aabb.p = this->aabb.p;
		RenderBox( aabb.Left(), aabb.Top(), 1, static_cast<float>(2*size), static_cast<float>(2*size), RGBAf(1,1,1,1));
	#endif // MAP_EDITOR*/

	// ���������� ������� ������ �� ������
	if (this->IsSleep())
		return;

	// ���� ���������� ����������� dirScript, �� ������� ������������, �� ������� �������� �� ���������� ��� ��������� ������ � ��������� �� ������.
	// ����� ������ ����� ���������, �� ���������� ����������� �� ��, ������� ���� ����������� �� ����, ��� ��������� dirScript.

	if ( charges > 0 && cooldown == 0 && (spawnLimit > 0 || spawnLimit == UNLIMITED_SPAWN) && 
		(direction == dirScript || (onScreen((float)size) && this->childrenConnection->children.size() < maximumEnemies )))
	{
		if ( direction == dirHorizontal  && abs(aabb.p.y - CAMERA_Y) > size )
			return;
		if ( direction == dirVertical  && abs(aabb.p.x - CAMERA_X) > size )
			return;
		if ( direction == dirLeft && CAMERA_GAME_LEFT < aabb.p.x + size/2 ) return;
		if ( direction == dirRight && CAMERA_GAME_RIGHT > aabb.p.x - size/2 ) return;
		if ( direction == dirUp && CAMERA_GAME_TOP < aabb.p.y + size/2 ) return;
		if ( direction == dirDown && CAMERA_GAME_BOTTOM > aabb.p.y - size/2 ) return;
		charges--;
		cooldown = enemySpawnDelay;

		if (spawnLimit != UNLIMITED_SPAWN)
			--spawnLimit;
		SpawnEnemy();
	}

	if ( cooldown > 0 ) 
		cooldown--;

	// ����� �������� ��������.
	if (spawnLimit == 0)
		this->SetDead();

	if ( childrenConnection->children.size() > 0 || onScreen( (float)respawn_dist ) )
		last_time_on_screen = current_time;

	bool respawn_time = current_time - last_time_on_screen > respawnDelay;

	switch ( direction )
	{
		case dirDown:
			{
				if (respawn_time && CAMERA_GAME_BOTTOM - respawn_dist < aabb.p.y) charges = maximumEnemies;
				break;
			}
		case dirUp:
			{
				if (respawn_time && CAMERA_GAME_TOP + respawn_dist > aabb.p.y) charges = maximumEnemies;
				break;
			}
		case dirLeft:
			{
				if (respawn_time && CAMERA_GAME_LEFT + respawn_dist > aabb.p.x) charges = maximumEnemies;
				break;
			}
		case dirRight:
			{
				if (respawn_time && CAMERA_GAME_RIGHT - respawn_dist < aabb.p.x ) charges = maximumEnemies;
				break;
			}
		case dirScript:
			{
				if (charges == 0)
				{
					direction = directionBeforeScript;
				}
				break;
			}
		case dirHorizontal:
		case dirVertical:
		case dirAny:
			{
				if ( respawn_time )
				{
					charges = maximumEnemies - childrenConnection->children.size();
				}
			}
		default: break;
	}
}

ObjSpawner* CreateSpawner(Vector2 coord, const char* enemy_type, UINT max_enemies, UINT delay, directionType direction, UINT size, UINT respawnDelay )
{
	return CreateSpawner( coord, enemy_type, max_enemies, delay, direction, size, size, respawnDelay, static_cast<UINT>(ObjSpawner::UNLIMITED_SPAWN) );
}

ObjSpawner* CreateSpawner(Vector2 coord, const char* enemy_type, UINT max_enemies, UINT delay, directionType direction, UINT size, UINT respawn_dist, UINT respawnDelay, UINT spawnLimit)
{
	ObjSpawner* spawner = new ObjSpawner(enemy_type, max_enemies, delay, direction, size, respawn_dist, respawnDelay, spawnLimit);
	spawner->aabb.p = coord;

#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		spawner->sprite = new Sprite( "editor_misc" );
		if (spawner->sprite->tex && spawner->sprite->tex->frames->framesCount >= 3)
		{
			spawner->sprite->currentFrame = 3;
			spawner->sprite->frameWidth = (USHORT)spawner->sprite->tex->frames->frame[2].size.x;
			spawner->sprite->frameHeight = (USHORT)spawner->sprite->tex->frames->frame[2].size.y;
		}
		spawner->sprite->z = 1;
		spawner->aabb.W = 32;
		spawner->aabb.H = 32;
		spawner->sprite->renderMethod = rsmStretch;
	}
#endif // MAP_EDITOR
	spawner->ClearPhysic();
	
	AddObject(spawner);

#ifdef MAP_EDITOR
	spawner->proto_name = new char[ strlen(enemy_type) + 1 ];
	strcpy( spawner->proto_name, enemy_type );
#endif // MAP_EDITOR
	
	return spawner;
}

ObjSpawner* CreateDummySpawner()
{
	ObjSpawner* obj = new ObjSpawner(NULL, 0, 0, dirNone, 1, 0, 1);
	AddObject(obj);
	return obj;
}
