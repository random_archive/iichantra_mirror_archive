#ifndef __OBJECT_SPANWER_H_
#define __OBJECT_SPAWNER_H_

#include "object.h"

enum directionType { dirNone, dirLeft, dirRight, dirUp, dirDown, dirAny, dirHorizontal, dirVertical, dirScript };

class ObjSpawner : public GameObject
{
public:
	enum { UNLIMITED_SPAWN = -1 };

	char* enemyType;

	/// ����������� ����������� ������������� ������. UNLIMITED_SPAWN = ����������
	int spawnLimit;
	/// ����������� �� ��������� ������������ ������������ ������������� ������
	UINT maximumEnemies;
	/// �������� ����� ��������� ������
	UINT enemySpawnDelay;
	/// �������� ����� "������������"
	UINT respawnDelay;
	/// ������ �������, ����� � ��������� ��� ��������� �� ������
	UINT last_time_on_screen;
	/// 
	UINT size;
	///
	UINT respawn_dist;
	/// �����������, � �������� ������ ������������ �����, ����� ������� ��������
	directionType direction;

	/// ������� "�������" - ��������� ������, ������� ��� ����� �������
	UINT charges;

	ObjSpawner( const char* enemyType, UINT maximumEnemies, UINT enemySpawnDelay, directionType direction, UINT size, UINT respawn_dist, UINT respawnDelay, int spawnLimit = UNLIMITED_SPAWN );
	~ObjSpawner();

	void Process();

	bool ChangeDirection(directionType dir);
	bool ScriptLaunch();

private:

	/// ���������� �������, ����������� ����� �� �������� ���������� �����
	UINT cooldown;

	directionType directionBeforeScript;

	bool onScreen(float size);
	void SpawnEnemy();

};

ObjSpawner* CreateSpawner(Vector2 coord, const char* enemy_type, UINT max_enemies, UINT delay, directionType direction, UINT size, UINT respawnDelay );
ObjSpawner* CreateSpawner(Vector2 coord, const char* enemy_type, UINT max_enemies, UINT delay, directionType direction, UINT size, UINT respawn_dist, UINT respawnDelay, UINT spawnLimit );
ObjSpawner* CreateDummySpawner();

#endif //__OBJECT_SPAWNER_H
