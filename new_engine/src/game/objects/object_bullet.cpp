#include "StdAfx.h"

#include "object_character.h"
#include "object_bullet.h"
#include "object_enemy.h"
#include "object_effect.h"
#include "object_player.h"

#include "../net.h"
#include "../object_mgr.h"

#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;

//////////////////////////////////////////////////////////////////////////

//#define SHOOTER_SPEED_ADDS_TO_BULLETS

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool ObjBullet::ApplyProto(const Proto* proto)
{
	if (!ObjDynamic::ApplyProto(proto))
		return false;

	// ��� ���������������� ���������, ��������� �����
	max_x_vel = fabs(acc.x);
	max_y_vel = fabs(acc.y);
	vel = acc;	// ������� ��������. ������ ���������� ������������� ���������, � �� ��������?

	damage = proto->bullet_damage;
	damage_type = proto->damage_type;
	push_force = proto->push_force;
	multiple_targets = proto->multiple_targets != 0;
	hurts_same_type = (proto->hurts_same_type & 1) != 0;
	hurts_same_faction = (proto->hurts_same_type & 2) != 0;

	return true;

}

// �������� ���� �� ���������. ������� �������� ����.
bool LoadBulletFromProto(const Proto* proto, ObjBullet* bullet, WeaponDirection wd)
{
	if (!proto || !bullet)
		return false;

	switch (wd)
	{
	case wdLeft:
	case wdRight:
		bullet->acc.x = proto->bullet_vel;
		bullet->acc.y = 0;
		break;
	case wdUpLeft:
	case wdUpRight:
		bullet->acc.x = proto->bullet_vel * ((float)M_SQRT2 * 0.5f);	// cos 45
		bullet->acc.y = -proto->bullet_vel * ((float)M_SQRT2 * 0.5f);	// sin 45
		break;
	case wdDownLeft:
	case wdDownRight:
		bullet->acc.x = proto->bullet_vel * ((float)M_SQRT2 * 0.5f);	// cos 45
		bullet->acc.y = proto->bullet_vel * ((float)M_SQRT2 * 0.5f);	// sin 45
		break;
	}

	switch (wd)
	{
	case wdLeft:
	case wdUpLeft:
	case wdDownLeft:
		bullet->SetFacing( true );
		break;
	default: break;
	}

	if (bullet->GetFacing())
		bullet->acc.x = -bullet->acc.x;

	if (!bullet->ApplyProto(proto))
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////////

extern bool netgame;

ObjBullet* CreateBullet(const Proto* proto, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd, UINT id)
{
	if (!proto)
		return NULL;

	if ( netgame )
	{
		//Do nothing if we are not the server and shooter is not managed locally
		if ( !Net::IsServer() && id == 0 && (!shooter || !shooter->local) )
			return NULL;

		//If we are the server, leave locally managed objects to shoot client-side and report it
		if ( Net::IsServer() && id == 0 && shooter && shooter->local )
			return NULL;
	}

	ObjBullet* bullet = new ObjBullet(shooter);



	if (!LoadBulletFromProto(proto, bullet, wd))
	{
		DELETESINGLE(bullet);
		return NULL;
	}

	// TODO: ������ ���. ��� �������� � ���������� � ���������. �� ����� ������ �� ��������.
	// � ���� ������ ������� � SAP ��� �� ��������, ������� � SetAnimation ������ ��� ���������
	// ��� ��������� � SAP.
	bullet->ClearPhysic();

	bullet->aabb.p = coord;
	bullet->trajectory = (TrajectoryType)proto->trajectory;
	bullet->t_param1 = proto->trajectory_param1;
	bullet->t_param2 = proto->trajectory_param2;

	switch (wd)
	{
	case wdLeft:
	case wdRight:
		bullet->SetAnimation("straight", true);
		break;
	case wdUpLeft:
	case wdUpRight:
		bullet->SetAnimation("diagup", true);
		break;
	case wdDownLeft:
	case wdDownRight:
		bullet->SetAnimation("diagdown", true);
		break;
	}

	bullet->SetPhysic();

	AddObject(bullet, id);
	if ( bullet->id == 0 ) 
	{
		if ( bullet->parentConnection ) bullet->parentConnection->childDead( 0 );
		DELETESINGLE( bullet );
		return NULL;
	}

	if ( bullet && bullet->id > 0 && id == 0 && netgame )
	{
		if (!Net::IsServer()) bullet->local = true;
		Net::LocalEvent* event = new Net::LocalEvent( Net::objectCreated, bullet, StrDupl(proto->name.c_str()) );
		event->int_param = wd;
		event->coord = coord;
		event->obj2 = shooter;
		Net::AddEvent( event );
	}
	
	return bullet;
}

ObjBullet* CreateBullet(const Proto* proto, Vector2 coord, ObjCharacter* shooter, Vector2 aim, int angle, UINT id)
{
	// TODO: ���� ���� ������ �� ������������, �� ����� ��� � ��������� �� ����.
	if (!proto)
		return NULL;

	ObjBullet* bullet = CreateBullet(proto, coord, shooter, wdLeft, id);
		
	bullet->ClearPhysic();
	bullet->SetAnimation("straight", true);
	if( bullet->aabb.W != 0 && bullet->aabb.H != 0 )
	{
		bullet->rectangle[0].x = -bullet->aabb.W;
		bullet->rectangle[1].x =  bullet->aabb.W;
		bullet->rectangle[2].x =  bullet->aabb.W;
		bullet->rectangle[3].x = -bullet->aabb.W;
		bullet->rectangle[0].y = -bullet->aabb.H;
		bullet->rectangle[1].y = -bullet->aabb.H;
		bullet->rectangle[2].y =  bullet->aabb.H;
		bullet->rectangle[3].y =  bullet->aabb.H;
		bullet->rectangle.CalcBox();
		bullet->SetPhysic();
	}

	float ang = atan2( aim.y - coord.y, aim.x - coord.x );
	ang += (angle * Const::Math::PI)/180;
	bullet->vel = Vector2(bullet->max_x_vel*cos(ang), bullet->max_x_vel*sin(ang) );
	//bullet->SetFacing( aim.x < coord.x );
	//if ( aim.x < coord.x && bullet->vel.x > 0) bullet->vel.x *= -1;
	bullet->acc = bullet->vel;
	bullet->max_y_vel = fabs(bullet->vel.y);
	bullet->max_x_vel = fabs(bullet->vel.x);

	bullet->SetBulletCollidable();
//	bullet->SetFacing( bullet->acc.x < 0 );

	return bullet;
}

ObjBullet* CreateBullet(const char* proto_name, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd, UINT id)
{
	if (!proto_name)
		return NULL;

	return CreateBullet(protoMgr->GetByName(proto_name, "projectiles/"), coord, shooter, wd, id);
}

ObjBullet* CreateAngledBullet(const char* proto_name, Vector2 coord, GameObject* shooter, bool mirror, int angle, int dir, UINT id)
{
	if (!proto_name)
		return NULL;
	
	return CreateAngledBullet(protoMgr->GetByName(proto_name, "projectiles/"), coord, shooter, mirror, (float)Const::Math::PI*angle/180.0f, dir, id);
}

ObjBullet* CreateAngledBullet(const Proto* proto, Vector2 coord, GameObject* shooter, bool mirror, float angle, int dir, UINT id)
{
	if (!proto)
		return NULL;

	ObjBullet * bul = CreateBullet(proto, coord, (ObjCharacter*)shooter, wdRight, id);
	if (!bul) return NULL;
	float ang = angle;
	bul->own_vel = Vector2( bul->vel.x*cos(ang), bul->vel.x*sin(ang) );
	if ( mirror )
	{
		bul->sprite->SetMirrored();
		bul->own_vel *= -1;
	}
	if ( dir > 4 || dir < -4 )
	{
		if ( mirror ) bul->sprite->ClearMirrored();
		else bul->sprite->SetMirrored();
		if ( dir == 9 || dir == -9 ) dir = 0;
		else if ( dir < 0 ) dir += 5;
		else dir -= 5;
	}
	bul->vel = Vector2(0,0);
	bul->acc = Vector2(0,0);
	bul->max_x_vel = proto->bullet_vel;
	bul->max_y_vel = proto->bullet_vel;
	//��� ������ ���� ��������-������, �� ��� ����� ������� ������, ��� ��� ���� ����� ����.
	if ( dir == 4 )
		bul->SetAnimation("straightup", false);
	if ( dir == -4 )
		bul->SetAnimation("straightdown", false);
	if ( dir == 3 )
		bul->SetAnimation("highup", false);
	if ( dir == -3 )
		bul->SetAnimation("highdown", false);
	if ( dir == 2 )
		bul->SetAnimation("diagup", false);
	if ( dir == -2 )
		bul->SetAnimation("diagdown", false);
	if ( dir == 1 )
		bul->SetAnimation("slightlyup", false);
	if ( dir == -1 )
		bul->SetAnimation("slightlydown", false);

	if( bul->aabb.W != 0 && bul->aabb.H != 0 )
	{
		bul->rectangle[0].x = -bul->aabb.W;
		bul->rectangle[1].x =  bul->aabb.W;
		bul->rectangle[2].x =  bul->aabb.W;
		bul->rectangle[3].x = -bul->aabb.W;
		bul->rectangle[0].y = -bul->aabb.H;
		bul->rectangle[1].y = -bul->aabb.H;
		bul->rectangle[2].y =  bul->aabb.H;
		bul->rectangle[3].y =  bul->aabb.H;
		bul->rectangle.CalcBox();
		bul->SetPhysic();
	}

	return bul;
}

ObjBullet* CreateBullet(const char* proto, Vector2 coord, ObjCharacter* shooter, Vector2 aim, int angle, UINT id)
{
	if (!proto)
		return NULL;
	return CreateBullet(protoMgr->GetByName(proto, "projectiles/"), coord, shooter, aim, angle, id);
}

ObjBullet* CreateDummyBullet()
{
	ObjBullet* obj = new ObjBullet(NULL);
	AddObject(obj);
	return obj;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void ObjBullet::Process()
{
	if (activity == oatDying)
	{
		this->SetAnimation("die", false);
		if ( this->sprite->IsAnimDone() ) this->SetDead();
	}
	if (activity == oatUndead)
		activity = oatDying;
	//if ( this->vel.x < 0 && this->sprite ) this->sprite->SetMirrored();
	if ( netgame )
	{
		if ( !IsDead() && ( (local && !Net::IsServer()) || (!local && Net::IsServer()) ) )
		{
			Net::LocalEvent* event = new Net::LocalEvent( Net::objectUpdated, NULL, NULL );
			event->uint_param = id;
			Net::AddEvent( event );
		}
	}
}

void ObjBullet::PhysProcess()
{
	aabb.p += own_vel;
	switch ( trajectory )
		{
			case pttOrbit:
				{
					GameObject* shooter = (ObjDynamic*)this->parentConnection->getParent();
					if ( shooter )
					{
						Vector2 center_point = shooter->aabb.p;
						t_value += t_param1;
						Vector2 tr = center_point + t_param2 * Vector2( cos( t_value ), sin( t_value) );
						if ( (tr - aabb.p).x >= 0 ) this->SetFacing( false ); 
						else this->SetFacing( true );
						vel = tr - aabb.p;
					}
					break;
				}
			default:
				{
					break;
				}
		}
	ObjDynamic::PhysProcess();
}

void ObjBullet::Bounce()
{
	SetAnimationIfPossible( "bounce", true );
}

// ��������� ���� �� ���-�� (����������� �� � ���������� ��������)
// Returns true if we should completely ignore the target afterwards
bool ObjBullet::Hit(ObjPhysic* obj)
{
	if (this->IsDead() || (activity == oatDying /*&& !multiple_targets*/) )
		return true;

	GameObject* shooter = this->parentConnection ? this->parentConnection->getParent() : NULL;
	if ( shooter && shooter == obj )
		return true;

	if (this->shooter_type == obj->type && !this->hurts_same_type)
		return true;

	if (obj->type == objBullet && static_cast<ObjBullet*>(obj)->shooter_type == objPlayer)
		return true;

	if (!this->hurts_same_faction && shooter && (shooter->type == objEnemy || shooter->type == objPlayer) && !((ObjCharacter*)shooter)->IsEnemy(obj))
		return true;

	//���� ������� �� ������������ � ����������-�����������. ������� � ������ - �����������, �� ��� � ������� ������������.
	if (obj->IsOneSide())
		return false;

	if (!obj->IsBulletCollidable() )
		return false;
	
	if ( obj->type == objBullet && this->parentConnection && obj->parentConnection && this->parentConnection->getParent() == obj->parentConnection->getParent() )
		return true;

	DELETEARRAY(this->mem_anim);
	this->mem_anim = StrDupl(this->sprite->cur_anim.c_str());
	this->mem_frame = this->sprite->GetAnimation(this->sprite->cur_anim_num)->current;
	this->mem_tick = internal_time - this->sprite->prevAnimTick;

	if (obj->IsDynamic())
	{
		//�� ����������� ������������ �������, � ����� ������� � ����������� (�������������) ������.
		if ( obj->type != objEffect && !(obj->type == objEnemy && ((ObjEnemy*)obj)->attached) && ((ObjDynamic*)obj)->mass >= 0)
		{
			//��� ����� �������, ����� ������ ����������� �� ������, � ������� �������� �� ��������.
			//�������� ����� ��������� multiple_targets
			float ang = 0; 
			if ( this->vel.x != 0 || this->vel.y != 0 )
				ang = atan2( this->vel.y, this->vel.x );
			else
				ang = atan2( obj->aabb.p.y - this->aabb.p.y, obj->aabb.p.x - this->aabb.p.x );
			Vector2 push = push_force*Vector2( cos(ang), sin(ang) );
			ObjDynamic* od = (ObjDynamic*)obj;
			if ( od->mass == 0 )
				od->acc += push;
			else
			{
				od->vel += push * (1/od->mass);
				od->aabb.p += push * (1/od->mass);
#ifdef SELECTIVE_RENDERING
				selectForRenederUpdate(od);
#endif
			}
		}
		switch (obj->type)
		{
		case objItem:
			{
				//����� �������� ��������, �� ���� - ����� ����������
				obj->SetAnimation("die", false);
			}
			break;
		case objPlayer:
			{
				if (this->damage > 0)
				{
					ObjPlayer* op = static_cast<ObjPlayer*>(obj);
					op->last_hit_from = aabb.p;
					op->ReceiveDamage(this->damage, this->damage_type, this->player_num);
				}
				break;
			}
		case objEnemy:
			{
				ObjEnemy* oe = static_cast<ObjEnemy*>(obj);
				oe->last_hit_from = aabb.p;
				oe->ReceiveDamage(this->damage, this->damage_type, this->player_num);
				break;
			}
		case objEffect:
			{
				static_cast<ObjEffect*>(obj)->ReceiveDamage(this->damage);
				break;
			}
		case objBullet:
			{
				//��������� �� ��������� ������������ ��������� ���������� ��� ���� ����� ���������� ��������� ���� ����������, � �� ��������
//				printf( "'tis a bullet\n" );
				if (obj->parentConnection && obj->parentConnection == this->parentConnection) break;
				ObjBullet* ob = (ObjBullet*)obj;
				if ( !ob->IsShielding() ) //������ ���� ������ ������� �� �����������.
				{
					if (!ob->multiple_targets) ob->activity = oatDying;
					else ob->activity = oatUndead;
				}
				if ( this->IsShielding() ) return false; //���� ��������� ��������, �� �� �������� �������� ���������.
				break;
			}
		default: break;
		}

		if (!multiple_targets) this->activity = oatDying;
		else if (this->activity != oatDying) this->activity = oatUndead;
	}
	else
	{
		// �������� ����� - ������������ ������ ������� ���, ��� �� ������
		// �� ����������� ��������, ��-�� ���� ��� �� ���������
		//if (activity != oatDying && activity != oatUndead)
		if (activity != oatDying)
		{
			this->sprite->SaveState();
			this->SetAnimation("miss", false);
		}
	}
	return false;
}

bool ObjBullet::Miss()
{
	if (!multiple_targets) this->activity = oatDying;
	else if (this->activity != oatDying) this->activity = oatUndead;
	if (activity != oatDying)
	{
		this->sprite->SaveState();
		this->SetAnimation("miss", false);
	}
	return false;
}
