#include "StdAfx.h"

#include "object_player.h"
#include "object_effect.h"

#include "../object_mgr.h"
#include "../player.h"

#include "../../input_mgr.h"
#include "../../resource_mgr.h"
#include "../../render/font.h"


//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;
extern bool netgame;

//////////////////////////////////////////////////////////////////////////

bool ObjPlayer::CanShoot()
{
	return controlEnabled && 
			movement != omtDodging && 
			movement != omtRolling &&
			movement != omtLying &&
			activity != oatMorphing;
}

bool ObjPlayer::ApplyProto(const Proto* proto)
{
	if (!ObjCharacter::ApplyProto(proto))
		return false;

	weapon = CreateWeapon(proto->main_weapon);
	alt_weapon = CreateWeapon(proto->alt_weapon);

	cur_weapon = weapon;
	
	air_control = proto->air_control;
	recovery = proto->recovery;

	aiming_speed = proto->aiming_speed;

	additional_jumps = static_cast<USHORT>(proto->additional_jumps);
	walljumps = static_cast<USHORT>(proto->walljumps);
	walljump_vel = proto->walljump_vel;

	if (proto->health > 0)
		health = (short)proto->health;
	if ( proto->max_health > proto->health )
		health_max = static_cast<short>(proto->max_health);
	else 
		health_max = health * 2;
	ammo = proto->ammo;

	return true;
}

ObjPlayer* CreatePlayer(const Proto* proto, Vector2 coord, const char* start_anim, bool corner_adjust)
{
	if (!proto)
		return NULL;

	ObjPlayer* player = new ObjPlayer;
	if (!player->ApplyProto(proto))
	{
		DELETESINGLE(player);
		return NULL;
	}

	player->LoadFactionInfo( proto );
	player->aabb.p = coord;


	// TODO: ������ ���. ��� �������� � ���������� � ���������. �� ����� ������ �� ��������.
	// � ���� ������ ������� � SAP ��� �� ��������, ������� � SetAnimation ������ ��� ���������
	// ��� ��������� � SAP.
	player->ClearPhysic();
	player->SetAnimation(start_anim ? start_anim : "idle", true);
	if (!corner_adjust) player->aabb.p = coord;
	player->SetPhysic();

#ifdef COORD_LEFT_UP_CORNER
	if ( corner_adjust )
	{
		player->aabb.p.x += player->sprite->frameWidth * 0.5f;
		player->aabb.p.y += player->sprite->frameHeight * 0.5f;
	}
#endif // COORD_LEFT_UP_CORNER

#ifdef MAP_EDITOR
	player->creation_shift = player->aabb.p - coord;
#endif //MAP_EDITOR

	AddObject(player);
	

	player->InitFaction();

	return player;
}

ObjPlayer* CreateDummyPlayer()
{
	ObjPlayer* obj = new ObjPlayer();
	AddObject(obj);
	return obj;
}

void ObjPlayer::SetAmmo( int amount )
{
	this->ammo = amount;
}

void ObjPlayer::RecieveAmmo( int amount )
{
	this->ammo += amount;
}

ObjPlayer* CreatePlayer(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust)
{
	if (!proto_name)
		return NULL;

	ObjPlayer* op = CreatePlayer(protoMgr->GetByName( proto_name, "characters/" ), coord, start_anim, corner_adjust);
#ifdef MAP_EDITOR
	op->proto_name = new char[ strlen(proto_name) + 1 ];
	strcpy( op->proto_name, proto_name );
#endif //MAP_EDITOR
	return op;
}

void ObjPlayer::ReceiveDamage( UINT damage, UINT damage_type, uint8_t player_num )
{
#ifdef GOD_MODE
	return;
#endif //GOD_MODE
	if ( !player || !player->current->controlEnabled ) return; //������ �������� ����, ����� �� �� ����� ��������. ��� ����� ���� ReduceHealth.
	ObjCharacter::ReceiveDamage( damage, damage_type, player_num );
	if ( !this->is_invincible )
	{
		this->is_invincible = true;
		this->recovery_time = static_cast<float>(this->recovery);
	}
}

//////////////////////////////////////////////////////////////////////////

ObjPlayer::~ObjPlayer()
{
	if( player ) player->OnDying(this);

	if ( name ) DELETEARRAY( name );
	DELETESINGLE(alt_weapon);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

extern config cfg;

//#define KEYDOWN(vk_code)((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)




extern InputMgr inpmgr;

#define KEYHELD(key) (input != NULL ? input->held[key] : false)



#define HELD_FIRE KEYHELD(cakFire)
#define HELD_ALT_FIRE KEYHELD(cakAltFire)
#define HELD_LEFT KEYHELD(cakLeft)
#define HELD_RIGHT KEYHELD(cakRight)
#define HELD_JUMP KEYHELD(cakJump)
#define HELD_SIT KEYHELD(cakSit)
#define HELD_DOWN KEYHELD(cakDown)
#define HELD_UP KEYHELD(cakUp)



#define RELEASED_CHANGE_WEAPON	(input != NULL ? input->released[cakChangeWeapon] : false)
#define RELEASED_USE			(input != NULL ? input->released[cakPlayerUse] : false)


#define PLAYER_STILL_WALKING_LEFT (this->movement == omtWalking && !this->movementDirectionX)
#define PLAYER_STILL_WALKING_RIGHT (this->movement == omtWalking && this->movementDirectionX)
#define PLAYER_STILL_AIMING_UP (this->movement == omtAimingUp || this->movement == omtWalkingAimingUp)

extern UINT current_time;

void ObjPlayer::Process()
{
	if (IsSleep())
		return;

	ObjCharacter::Process();

#ifdef PLAYER_CAN_DODGE
	if ( movement == omtDodging && (sprite->IsAnimDone() || sprite->cur_anim != "dodge") )
	{
		movement = omtIdling;
	}
#endif //PLAYER_CAN_DODGE

	//���� ���������� ��� - ������ �� ������� � ���� �������� �� ��������
	//������ ��� ������������ ���� �� �����.
	bool new_jump = false;
	if ( player && player->current->controlEnabled )
	{
		if ( HELD_LEFT & !HELD_RIGHT )
		{
			SetFacing( true );
		}
		else if ( !HELD_LEFT & HELD_RIGHT )
		{
			SetFacing( false );
		}
		if ( (HELD_DOWN && camera_offset < 100)||
		 (!HELD_DOWN && !HELD_UP && camera_offset < 0) )
			camera_offset+=10;
		if ( (HELD_UP && camera_offset > -100) ||
		 (!HELD_DOWN && !HELD_UP && camera_offset > 0) )
			camera_offset-=10;
#ifndef GOD_MODE
		if (this->is_invincible)
		{
				/*UINT off = abs(((int)current_time % 10)-5);
				s->color = RGBAf(0.8-off*0.05, 0.8-off*0.05, 1.0-off*0.1, 1.0);*/
				if ( current_time % 100 > 50 )
					this->sprite->color = RGBAf(1.0f, 1.0f, 1.0f, 0.0f);
				else
					this->sprite->color = RGBAf(0.9f, 0.9f, 1.0f, 1.0f);
		}
		else
		{
			this->sprite->color = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
		}
#endif //!GOD_MODE
	}
	else
	{
		if ( this->activity != oatMorphing )
			this->activity = oatIdling;
		this->sprite->color = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
		if ( !IsNear(vel.x, 0.0f, 0.01f) )
			movementDirectionX = this->vel.x > 0;
		if ( this->IsOnPlane() )
		{
			if ( abs(vel.x) > 0.01 )
				this->movement = omtWalking;
			else
				this->movement = omtIdling;
		}
		else
		{
			this->movement = omtJumping;
			new_jump = true;
		}
		// ������������� ������ �� �����������
		if ( (abs(vel.x) > 0.01f) || this->movement == omtSitting ) //�����, �� ����� �����
		{
			if (movementDirectionX )
				SetFacing( false );
			else
				SetFacing( true );
		}
	}

	if ( this->recovery_time > 0 )
	{
		this->recovery_time--;
		if ( this->recovery_time <= 0 ) this->is_invincible = false;
	}

	Sprite* s = this->sprite;
	assert(s);

	if ( activity == oatMorphing )
		return;

	ObjectMovementType last_movement = this->movement;
	bool last_direction = (this->sprite->IsMirrored() == false);
	CharacterGunDirection oldGunDirection = gunDirection;

	gunDirection = cgdNone;

	if (this->health <= 0)
	{
		activity = oatDying;
		controlEnabled = false;
		//player_change_blocked = true;
	}
	if (activity == oatDying)
	{
		//if (!player_change_blocked) player_change_blocked = true;
		movement = omtIdling;
		this->SetAnimation("die", false);
		if (s->IsAnimDone())
		{
			this->SetDead();
		}
		return;
	}

	// ��������� ������� �� �������
	// TODO: �������� ����� � �������� �� ����������� ����������/����� ������
	float dvx = 0.0f;	// ��������� ��������, ���������� � ����� � ����������� �� ���� ��������

	if ( !new_jump && movement == omtJumping && IsOnPlane() )
	{
		movement = omtIdling;
	}
	if (this->controlEnabled)
	{
		if ( charge_shot_complete && activity == oatShooting )
		{
			charge_shot_complete = false;
			activity = oatIdling;
		}
		if ( !charging_weapon ) //Can start a new charge
		{
			if ((HELD_FIRE && this->cur_weapon) ^ (HELD_ALT_FIRE && this->alt_weapon)) //Starting a charge
			{
				Weapon* wp = (HELD_ALT_FIRE && this->alt_weapon) ? alt_weapon : cur_weapon;
				if ( WeaponReady(HELD_ALT_FIRE && this->alt_weapon) )
				{
					if ( wp->charge_proto )
					{
						weapon_charge_time = 1;
						wp->Charge(this);
						charging_weapon = wp;
					}
					else
					{
						//Fire!
						alt_fire = ( wp == alt_weapon );
						this->activity = oatShooting;
					}
				}
			}
			else
			{
				this->activity = oatIdling;
			}
		}
		else
		{
			if ( (HELD_FIRE && weapon != charging_weapon) || (HELD_ALT_FIRE && alt_weapon != charging_weapon ) )
			{
				//Interrupt, because character cannot charge both weapons at the same time
				weapon_charge_time = 0;
				SetChargeEffect( NULL );
				charging_weapon = NULL;
			}
			else if ( (HELD_FIRE && weapon == charging_weapon) || (HELD_ALT_FIRE && alt_weapon == charging_weapon ) )
			{
				//Keep charging
				if ( CanShoot() )
				{
					weapon_charge_time += internal_time - last_weapon_charge;
					if ( charging_weapon->charge_hold > 0 && weapon_charge_time >= charging_weapon->charge_hold )
					{
						weapon_charge_time = charging_weapon->charge_hold;
						alt_fire = ( charging_weapon == alt_weapon );
						this->activity = oatShooting;
						charging_weapon = NULL;
					}
				}
			}
			else
			{
				//Fire!
				if ( CanShoot() )
				{
					alt_fire = ( charging_weapon == alt_weapon );
					this->activity = oatShooting;
					charging_weapon = NULL;
					charge_shot_complete = true;
				}
				else
				{
					weapon_charge_time = 0;
					SetChargeEffect( NULL );
					charging_weapon = NULL;
				}
			}
		}
		last_weapon_charge = internal_time;

		if (HELD_SIT && movement != omtJumping && movement != omtDropSitting)
		{
			if ( HELD_LEFT ^ HELD_RIGHT ) 
			{
				if ( (movement == omtRolling) || (movement != omtLying && abs(vel.x) > 1) )
				{
					if ( sprite->GetAnimation("roll") != NULL )	//Our character can roll or slide
						movement = omtRolling;
					else

						movement = omtSitting;			//otherwise just sit
				}
				else
				{
					if ( sprite->GetAnimation("lie") != NULL )	// Our character can lay down
						movement = omtLying;
					else						//otherwise just sit
						movement = omtSitting;
				}
			}
			else movement = omtSitting;
		}

		if (movement == omtLying && !HELD_SIT)
		{
			movement = omtIdling;
		}

		if (movement == omtRolling && sprite->IsAnimDone())
		{
			movement = omtLying;
		}

		if (HELD_UP && !HELD_DOWN && weapon != NULL)
		{
			gunDirection = cgdUp;
		}

		if (HELD_DOWN && !HELD_UP && weapon != NULL)
		{
			gunDirection = cgdDown;
		}

		if ( !HELD_JUMP )
			jump_complete = true;

		if ( !HELD_JUMP || !HELD_SIT ) 
		{
			drop_from = NULL;
			ClearDropping();
		}
		if (HELD_JUMP && HELD_SIT )
		{
			if ( movement != omtDropSitting && drop_from == NULL && !(suspected_plane && GetSuspectedPlane()->IsOneSide() && GetSuspectedPlane()->IsForced()) )
			{
				this->SetDropping();
			}
		}
		else if (HELD_JUMP && movement != omtLanding && jump_reserve > 0 && jump_complete )
		{
			jump_reserve--;
			movement = omtJumping;
			this->vel.y = -this->jump_vel*this->env->jump_vel_multiplier/*/10.0*/;
			this->ClearOnPlane();
			this->on_plane_reserve = -1;
			jump_complete = false;
		}
		else if ( jump_complete && walljump_reserve > 0 && HELD_JUMP && movement == omtJumping && ((vel.x >= 0 && IsPushedLeft()) || (vel.x <= 0 && IsPushedRight())) )
		{
			SetFacing( !GetFacing() );
			movement = omtJumping;
			movementDirectionX = !movementDirectionX;
			this->vel.y = -this->jump_vel*this->env->jump_vel_multiplier;
			this->vel.x *= -1.0f;
			this->vel.x -= this->GetFacing() ? this->walljump_vel*this->env->jump_vel_multiplier : -this->walljump_vel*this->env->jump_vel_multiplier;
			walljump_reserve--;
			jump_complete = false;
			//this->SetAnimation("walljump", false );
		}
		ClearPushes();

		//[������ ������, �����, ���� ��������� ������� ��� ��]
		/*if ( !IsOnPlane() )
		{
			if ( HELD_JUMP && this->jump_reserve > 0 )
			{
				this->vel.y -= jump_vel/10.0;
				jump_reserve--;
			}
			if ( !HELD_JUMP ) jump_reserve = 0;
		}
		else jump_reserve = 10;*/
		//[/������ ������]
		//����������� ����������, ���� ����� �����, �� ������ �������. ����� ���� ��������.
		if ( !IsOnPlane() && this->vel.y < 0 && !HELD_JUMP ) this->vel.y += this->gravity.y * 0.5f /** time*/;

		if (!HELD_SIT && (movement == omtSitting || movement == omtDropSitting))
			movement = omtIdling;

#ifdef PLAYER_CAN_DODGE
		if (RELEASED_USE && IsOnPlane() && (sprite->GetAnimation("dodge") != NULL) )
		{
			movement = omtDodging;
			SetAnimation( "dodge", false );
		}
#endif //PLAYER_CAN_DODGE

		if ( movement == omtRolling )
			dvx = vel.x * 0.3f;
		else
		{
			if (HELD_RIGHT && !HELD_LEFT && movement != omtDodging && movement != omtSitting && movement != omtLying && movement != omtRolling )
			{
				dvx += walk_acc * (IsOnPlane() ? 0.5f : this->air_control); //�������� � ������ ������ ���� ����� ����������, �� �����.
				movementDirectionX = true;
			}
			if (HELD_LEFT && !HELD_RIGHT && movement != omtDodging && movement != omtSitting && movement != omtLying && movement != omtRolling )
			{
				dvx -= walk_acc * (IsOnPlane() ? 0.5f : this->air_control);
				movementDirectionX = false;
			}
		}

		/*if (RELEASED_CHANGE_WEAPON)
		{
			this->ChangeWeapon();
			if (activity == oatShooting)
				activity = oatIdling;
		}*/

		if (RELEASED_USE)
		{
			if (this->env)
			{
				this->env->OnUse(this);
			}
		}
	}


	// ��������� �������� ��� ����������� ���� ������������
	if (movement != omtSitting && movement != omtJumping &&
		movement != omtLanding && movement != omtDropSitting &&
		movement != omtDodging && movement != omtLying &&
		movement != omtRolling &&
		!IsNear(vel.x, 0, 0.1f))
	{
		movement = omtWalking;
	}

	if(this->controlEnabled &&
		(((!HELD_LEFT && PLAYER_STILL_WALKING_LEFT) ||
		(!HELD_RIGHT && PLAYER_STILL_WALKING_RIGHT) ||
		(HELD_LEFT && HELD_RIGHT)) && movement != omtJumping && movement != omtSitting ))
	{
		movement = omtIdling;
	}

	if (!IsOnPlane() && movement != omtJumping)
	{
		if ( jump_reserve > 0 ) jump_reserve--;
		movement = omtJumping;
	}

	// ������ ��������
	if (this->movement == omtWalking)
	{
		if (gunDirection == cgdUp && (this->sprite->GetAnimation("walkgunaimup") != NULL))
		{
			this->SetAnimation("walkgunaimup", false);
		}
		else if (gunDirection == cgdDown && (this->sprite->GetAnimation("walkgunaimdown") != NULL))
		{
			this->SetAnimation("walkgunaimdown", false);
		}
		else
		{
			this->SetAnimation("walk", false);
		}
	}
	else if (this->movement == omtJumping)
	{
		if (gunDirection == cgdDown)
		{
			if ( HELD_LEFT || HELD_RIGHT )
			{
				if (oldGunDirection != gunDirection && (this->sprite->GetAnimation("jumpgunliftaimdownforward") != NULL))
					this->SetAnimation("jumpgunliftaimdownforward", false);

				if (last_movement != omtJumping && (this->sprite->GetAnimation("jumpgunaimdownforward") != NULL))
					this->SetAnimation("jumpgunaimdownforward", false);
			}
			else
			{
				this->SetAnimationIfPossible("jumpgunaimdown", false);
			}
		}
		else if (gunDirection == cgdUp)
		{
			if ( HELD_LEFT || HELD_RIGHT )
			{
				if (oldGunDirection != gunDirection && (this->sprite->GetAnimation("jumpgunliftaimup") != NULL))
					this->SetAnimation("jumpgunliftaimup", false);

				if (last_movement != omtJumping && (this->sprite->GetAnimation("jumpgunaimupforward") != NULL))
					this->SetAnimation("jumpgunaimupforward", false);
			}
			else
			{
				this->SetAnimationIfPossible("jumpgunaimup", false);
			}
		}
		else
		{
/*
			if (last_movement != omtJumping )
			{
				this->SetAnimation("jump", false);
			}
			else if ( this->sprite->IsAnimDone() )
				this->SetAnimation("fly", false);
			if ( this->sprite->cur_anim == "jump" && IsOnPlane() )
				this->sprite->SetCurrentFrame(0);
*/
			if ( last_movement != omtJumping || activity != oatShooting )
			{
				if ( vel.y <= 0 )
					this->SetAnimation("jump", false );
				else
					this->SetAnimation("fly", false );
			}
		}
	}
	/*else if (this->movement == omtLanding)
	{
		this->SetAnimation("land", false);
		if (s->IsAnimDone())
		{
			// �������� ����������� - �� ������������
			movement = omtIdling;
		}

	}*/
	else if ( movement == omtLying )
	{
		SetAnimation( "lie", false );
	}
	else if ( movement == omtRolling )
	{
		SetAnimation( "roll", false );
	}
	else if ((this->movement == omtSitting || this->movement == omtDropSitting ) && this->activity != oatShooting)
	{
		if (gunDirection == cgdUp && this->sprite->GetAnimation("sitaimup") != NULL )
		{
			if ( this->sprite->cur_anim != "situpshoot" && this->sprite->cur_anim != "sitaimup" )
				this->SetAnimation("sitaimup", false);
		} else
		if (gunDirection == cgdDown && this->sprite->GetAnimation("sitaimdown") != NULL )
		{
			if ( this->sprite->cur_anim != "sitdownshoot" && this->sprite->cur_anim != "sitaimdown" )
				this->SetAnimation("sitaimdown", false);
		} else if (this->sprite->cur_anim != "sitshoot") this->SetAnimation("sit", false);
	}

	if (this->movement == omtIdling /*&& !HELD_FIRE*//*this->activity != oatShooting*/)
	{
		if (gunDirection == cgdUp)
		{
			if (oldGunDirection != gunDirection)
				this->SetAnimationIfPossible("gunliftaimup", false);

			if (last_movement != omtIdling && this->sprite->cur_anim != "gunaimupshoot")
				this->SetAnimationIfPossible("gunaimup", false);
		}
		else if (gunDirection == cgdDown)
		{
			if (oldGunDirection != gunDirection)
				this->SetAnimationIfPossible("gunliftaimdown", false);

			if (last_movement != omtIdling && this->sprite->cur_anim != "gunaimdownshoot")
				this->SetAnimationIfPossible("gunaimdown", false);
		}
		else if (this->sprite->cur_anim != "land" 
		      && this->sprite->cur_anim != "stop"
		      && this->sprite->cur_anim != "pain"
		      && this->sprite->cur_anim != "shoot")
		{
			if (gunDirection == cgdNone && oldGunDirection == cgdUp)
			{
				this->SetAnimationIfPossible("gunloweraimup", false);
			}
			else if (gunDirection == cgdNone && oldGunDirection == cgdDown)
			{
				this->SetAnimationIfPossible("gunloweraimdown", false);
			}
			else if ( sprite->cur_anim != "gunloweraimup" && sprite->cur_anim != "gunloweraimdown" ) 
			{
				this->SetAnimation("idle", false);
			}
		}
	}

	//���������� ��������
	if ( this->IsOnPlane() && movement != omtJumping )
	{
		jump_reserve = additional_jumps + 1;
		walljump_reserve = walljumps;
	}
	if ( !new_jump && last_movement == omtJumping && this->IsOnPlane() && movement != omtJumping && vel.Length() < 2 )
	{
		this->SetAnimation("land", false);
	}
	if ( last_movement == omtWalking && (this->movement == omtIdling || (movementDirectionX ^ last_direction)) )
		this->SetAnimation("stop", false);


	if (cur_weapon && this->activity == oatShooting && cur_weapon->ClipReloaded(HELD_FIRE||HELD_ALT_FIRE) ) cur_weapon->clip = cur_weapon->shots_per_clip;
	if (cur_weapon && this->activity == oatShooting && this->cur_weapon->IsReady())
	{
		if ( !(this->sprite->cur_anim == "gunliftaimup" || this->sprite->cur_anim == "gunliftaimdown" || this->sprite->cur_anim == "gunloweraimup" || this->sprite->cur_anim == "gunloweraimdown" ) )
		{
			if (movement == omtSitting || movement == omtDropSitting)
			{
				if (this->gunDirection == cgdDown && (this->sprite->GetAnimation("sitdownshoot") != NULL))
				{
					this->SetAnimation("sitdownshoot", true);
				}
				else if (this->gunDirection == cgdUp && (this->sprite->GetAnimation("situpshoot") != NULL))
				{
					this->SetAnimation("situpshoot", true);
				}
				else
				{
					this->SetAnimation("sitshoot", true);
				}
			}
			//else if (movement == omtWalking)
			//{

			//}
			else if (movement == omtJumping)
			{
				if (this->gunDirection == cgdDown)
				{
					if ( HELD_LEFT || HELD_RIGHT )
						this->SetAnimationIfPossible("jumpgunaimdownforwardshoot", true);
					else
						this->SetAnimationIfPossible("jumpgunaimdownshoot", true);
				}
				else if (this->gunDirection == cgdUp)
				{
					if ( HELD_LEFT || HELD_RIGHT )
						this->SetAnimationIfPossible("jumpgunaimupforwardshoot", true);
					else
						this->SetAnimationIfPossible("jumpgunaimupshoot", true);
				}
				else
				{
					this->SetAnimationIfPossible("jumpshoot", true);
				}
			}
			else if (movement == omtIdling)
			{
				if (this->gunDirection == cgdUp && sprite->cur_anim != "gunliftaimup" )
				{
					this->SetAnimationIfPossible("gunaimupshoot", true);
				}
				else if (this->gunDirection == cgdDown && sprite->cur_anim != "gunliftaimdown" )
				{
					this->SetAnimationIfPossible("gunaimdownshoot", true);
				}
				else if (sprite->cur_anim != "gunloweraimup" && sprite->cur_anim != "gunloweraimdown" )
				{
					this->SetAnimation("shoot", true);
				}
			}
		}
	}


	if (movement != omtSitting && movement != omtLanding && movement != omtDropSitting)
	{
		vel.x += dvx*this->env->walk_vel_multiplier;
	}

}

//���������, ����� �� ����������
bool ObjPlayer::HasAmmo()
{
	//if ( this != _PLAYER ) return false;
	return (cur_weapon && ( this->cur_weapon->is_infinite || (this->ammo >= this->cur_weapon->bullets_count) ) );
}

bool ObjPlayer::WeaponReady( bool alt_weapon )
{
	Weapon* w = alt_weapon ? this->alt_weapon : this->weapon;
	return w && w->IsReady() && ( w->is_infinite || ammo >= w->bullets_count );
}

//������ ������� �� �������.
void ObjPlayer::SpendAmmo()
{
	if ( /*this != _PLAYER ||*/ !this->cur_weapon->IsReady() ) return;
	//this->ammo -= this->cur_weapon->bullets_count;
	//if ( this->ammo < 0 ) this->ammo = 0;
	if (this->ammo > this->cur_weapon->bullets_count)
		this->ammo -= this->cur_weapon->bullets_count;
	else
		this->ammo = 0;
}

// ����������� �������� � �������������� ������
void ObjPlayer::ChangeWeapon()
{
	if (alt_weapon && cur_weapon == this->weapon)
		cur_weapon = alt_weapon;
	else
		cur_weapon = weapon;
}


void ObjPlayer::GiveWeapon(const char* weapon_name, bool primary)
{
	if (!weapon_name)
	{
		if ( primary ) this->weapon = NULL;
		else this->alt_weapon = NULL;
		return;
	}
	SetChargeEffect( NULL );

	if ( !primary )
	{
		if (this->alt_weapon) DELETESINGLE(this->alt_weapon);
		this->alt_weapon = CreateWeapon(weapon_name);
		if (this->cur_weapon != this->weapon) this->cur_weapon = this->alt_weapon;
	}
	else
	{
		if (this->weapon) DELETESINGLE(this->weapon);
		this->weapon = CreateWeapon(weapon_name);
		if (this->cur_weapon != this->alt_weapon) this->cur_weapon = this->weapon;
	}
}

void ObjPlayer::Draw()
{
	ObjCharacter::Draw();
	if ( netgame && this->name )
	{
		static IFont* font = FontByName(TEXT_FONT);
		if (font)
		{
			font->p = this->aabb.p;
			font->p.x -= font->GetStringWidth( this->name )/2.0f;
			font->p.y -= this->aabb.H + 15;
			font->z = this->sprite->z;
			font->tClr = this->color;
			font->Print(this->name);
		}
	}
}

void ObjPlayer::SetChargeEffect( ObjEffect* effect )
{
	if ( charge_effect ) charge_effect->SetDead();
	charge_effect = effect;
}
