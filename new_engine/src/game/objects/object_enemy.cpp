#include "StdAfx.h"

#include "object_enemy.h"
#include "object_spawner.h"
#include "object_dynamic.h"
#include "object_bullet.h"
#include "object_waypoint.h"

#include "../object_mgr.h"

#include "../../misc.h"
#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;
extern map<int, GameObject*> factions;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool ObjEnemy::ApplyProto(const Proto* proto)
{
	if (!ObjCharacter::ApplyProto(proto))
		return false;

	LoadFactionInfo(proto);

	offscreen_behavior = (CharacterOffscreenBeh)proto->offscreen_behavior;
	offscreen_distance = proto->offscreen_distance;

	return true;
}

ObjEnemy* CreateEnemy(const char* protoname, Vector2 coord, const char* start_anim, bool corner_adjust, GameObject* parent)
{

	const Proto* proto_ = protoMgr->GetByName(protoname, "enemies/");
	if (!proto_)
		return NULL;

	ObjEnemy* enemy = new ObjEnemy( parent );
	if (!enemy->ApplyProto(proto_))
	{
		DELETESINGLE(enemy);
		return NULL;
	}
	AddObject(enemy);
	
	enemy->aabb.p = coord;


	// TODO: ������ ���. ��� �������� � ���������� � ���������. �� ����� ������ �� ��������.
	// � ���� ������ ������� � SAP ��� �� ��������, ������� � SetAnimation ������ ��� ���������
	// ��� ��������� � SAP.
	enemy->ClearPhysic();
	enemy->SetAnimation(start_anim ? start_anim : "init", true);
	if (!corner_adjust) enemy->aabb.p = coord;
	enemy->SetPhysic();
	ObjPhysic* op = (ObjPhysic*)enemy;
	op->rectangle[0] = Vector2(-op->aabb.W, -op->aabb.H);
	op->rectangle[1] = Vector2( op->aabb.W, -op->aabb.H);
	op->rectangle[2] = Vector2( op->aabb.W,  op->aabb.H);
	op->rectangle[3] = Vector2(-op->aabb.W,  op->aabb.H);
	op->rectangle.CalcBox();

#ifdef COORD_LEFT_UP_CORNER
	if ( corner_adjust )
	{
		enemy->aabb.p.x += enemy->sprite->frameWidth * 0.5f;
		enemy->aabb.p.y += enemy->sprite->frameHeight * 0.5f;
	}
#endif // COORD_LEFT_UP_CORNER
	
#ifdef MAP_EDITOR
	enemy->proto_name = new char[ strlen(protoname) + 1 ];
	strcpy( enemy->proto_name, protoname );
	enemy->creation_shift = enemy->aabb.p - coord;
#endif //MAP_EDITOR

	enemy->InitFaction();

	return enemy;
}

ObjEnemy* CreateDummyEnemy()
{
	ObjEnemy* obj = new ObjEnemy();
	AddObject(obj);
	return obj;
}

extern float CAMERA_GAME_LEFT;
extern float CAMERA_GAME_RIGHT;
extern float CAMERA_GAME_BOTTOM;
extern float CAMERA_GAME_TOP;

void ObjEnemy::Process()
{
	if ( this->offscreen_behavior != cobNone && (
		 this->aabb.GetMax(0) + this->offscreen_distance < CAMERA_GAME_LEFT || 
		 this->aabb.GetMin(0) - this->offscreen_distance > CAMERA_GAME_RIGHT ||
		 this->aabb.GetMax(1) + this->offscreen_distance < CAMERA_GAME_TOP ||
		 this->aabb.GetMin(1) - this->offscreen_distance > CAMERA_GAME_BOTTOM ) )
	{
		switch ( offscreen_behavior )
		{
			case cobSleep:
				{
					this->SetSleep();
					return;
				}
			case cobDie:
				{
					this->SetDead();
/*					if ( this->parentConnection && this->parentConnection->getParent()->type == objSpawner && this->activity != oatDying )
					{
						((ObjSpawner*)this->parentConnection->getParent())->charges++;
					}
					break;*/
				}
			case cobAnim:
				{
					this->SetAnimation("offscreen", false);
					break;
				}
			default: break;
		}
	}
	else if ( offscreen_behavior == cobSleep && this->IsSleep() )
		this->ClearSleep();

	if (IsSleep())
		return;

	if ( target )
	{
		GameObject* otarget = target->getParent();
		if ( !otarget || ((ObjCharacter*)otarget)->activity == oatDying || !otarget->sprite->IsVisible() || otarget->IsSleep() )
		{
			target->childDead( id );
			target = NULL;
			if ( this->activity != oatDying )
			{
				if ( this->sprite->GetAnimation("target_dead") )
					SetAnimation("target_dead", true);
				else
					SetAnimation("idle", true);
			}
		}
	}

	if ( this->movement == omtMovingToWaypoint && this->current_waypoint != 0 && this->activity != oatDying && this->waypoint_speed > 0 )
	{
		ObjWaypoint* wp = NULL;

		if ( waypoint_mode & 1 ) //���� ����� � ���������� �������
			wp = GetWaypoint( waypoint_global );
		else
			wp = (ObjWaypoint*)GetGameObject( current_waypoint );

		if (!wp)
		{
			vel = Vector2(0, 0);
			acc = Vector2(0, 0);
			Log(DEFAULT_LOG_NAME, logLevelWarning, 	"Waypoint id=%d doesn't exists (enemy id=%d)", current_waypoint, this->id);
			movement = omtIdling;
			current_waypoint = 0;
			return;
		}

		float accuracy_x = wp->aabb.W;
		float accuracy_y = wp->aabb.H;
		if ( waypoint_mode & 64 ) //���������� ���������.
		{
				accuracy_x += aabb.W;
				accuracy_y += aabb.H;
		}
		float speed_limit = waypoint_speed_limit;
		if (   abs(aabb.p.x - wp->aabb.p.x) < accuracy_x
			&& abs(aabb.p.y - wp->aabb.p.y) < accuracy_y )
		{
			current_waypoint = wp->pointReached( this );
			waypoint_start = Vector2(this->aabb.p.x, this->aabb.p.y);
		}
		else
		{
			float speed = waypoint_speed; 
			if ( waypoint_mode & 16 && (wp->aabb.p - aabb.p).Length() < vel.Length() + acc.Length()/2 )
				speed = -speed;
			if ( waypoint_mode & 128 ) //��� ����� �����������, ��� ����� �� � ����.
			{
				{
					float path = abs( (wp->aabb.p - waypoint_start).Length() );
					float dist = abs( (wp->aabb.p - aabb.p).Length() );
					if ( dist < path/4 )
					{
						float k = 4*dist/path;
						if ( waypoint_mode & 8 ) //���� ��������, �� �� ����� ��������� ������ �� ����.
							k = max( k, 0.25f );
						speed *= k;
					}
					else if ( dist > 3*path/4 && dist < path )
					{
						float k = 4*( path/4 - ( dist - 3*path/4 ))/path;
						if ( waypoint_mode & 8 ) //���� ��������, �� �� ����� ��������� ������ �� ����.
							k = max( k, 0.25f );
						speed *= k;
					}
				}
			}
			float angle = atan2( wp->aabb.p.y - aabb.p.y, wp->aabb.p.x - aabb.p.x );
			Vector2 speed_bonus = Vector2();
			speed_bonus.x = cos(angle) * speed;
			speed_bonus.y = sin(angle) * speed;
			if ( waypoint_mode & 32 ) //�� ��������� ����.
			{
				Vector2 approx_point = aabb.p + vel + 0.5f*speed_bonus;
				if ( waypoint_mode & 8 )
				{
					approx_point = aabb.p + speed_bonus + 0.5f*acc;
					if ( (approx_point.x + aabb.W > wp->aabb.Right() && aabb.p.x < wp->aabb.Right()) ||
						 (approx_point.x - aabb.W < wp->aabb.Left() && aabb.p.x > wp->aabb.Left()) )
						 speed_bonus.x = wp->aabb.p.x - aabb.p.x - 0.5f*acc.x;
					if ( (approx_point.y + aabb.H > wp->aabb.Bottom() && aabb.p.y < wp->aabb.Bottom()) ||
						 (approx_point.y - aabb.H < wp->aabb.Top() && aabb.p.y > wp->aabb.Top()) )
						 speed_bonus.y = wp->aabb.p.y - aabb.p.y - 0.5f*acc.y;
				}
				else
				{
					if ( (approx_point.x + aabb.W > wp->aabb.Right() && aabb.p.x < wp->aabb.Right()) ||
						 (approx_point.x - aabb.W < wp->aabb.Left() && aabb.p.x > wp->aabb.Left()) )
						 speed_bonus.x = 2*(wp->aabb.p.x - aabb.p.x - vel.x);
					if ( (approx_point.y + aabb.H > wp->aabb.Bottom() && aabb.p.y < wp->aabb.Bottom()) ||
						 (approx_point.y - aabb.H < wp->aabb.Top() && aabb.p.y > wp->aabb.Top()) )
						 speed_bonus.y = 2*(wp->aabb.p.y - aabb.p.y - vel.y);
				}
			}
			if ( abs( wp->aabb.p.x - aabb.p.x ) < wp->aabb.W && speed_bonus.y != 0 ) speed_bonus.x = 0;
			if ( abs( wp->aabb.p.y - aabb.p.y ) < wp->aabb.H && speed_bonus.x != 0 ) speed_bonus.y = 0;
			if ( waypoint_speed_limit == 0 )
				speed_limit = max( abs(vel.x), abs(vel.y) );
			//oe->waypoint_speed = speed_bonus;
			if ( (speed_bonus.x > 0 && vel.x > speed_limit) || (speed_bonus.x < 0 && vel.x < -speed_limit) )
				speed_bonus.x = 0;
			if ( (speed_bonus.y > 0 && vel.y > speed_limit) || (speed_bonus.y < 0 && vel.y < -speed_limit) )
				speed_bonus.y = 0;
			if ( waypoint_mode & 8 )	//��������, � �� ���������
			{
				if ( gravity.y != 0 )
					vel.x = speed_bonus.x;
				else
					vel = speed_bonus;
			}
			else
			{
				if ( gravity.y != 0 )
					acc.x = speed_bonus.x;
				else
					acc = speed_bonus;
			}
		}
	}





	Sprite* s = this->sprite;
	assert(s);

	ObjectMovementType old_movement = this->movement;

	if ( activity == oatDying )
	{
		movement = omtIdling;
		if ( health > 0 ) 
		{
			if ( target) target->children.push_back( this );
			movement = old_movement;
			activity = oatIdling;
			this->SetAnimation( "respawn", false );
		}
		else 
		{
			if (s->IsAnimDone())
			{
				this->SetDead();
			}
			return;
		}
	}

	if ( !IsOnPlane() && gravity.y != 0 )
	{
		if ( movement != omtMovingToWaypoint ) movement = omtJumping;
	}
	else if ( movement != omtMovingToWaypoint )
	{
		movement = omtWalking;
	}

	if ( IsPushedLeft() || IsPushedRight() )
	{
		ClearPushes();
		this->SetAnimationIfPossible("stuck", false);
	}
	else if ( ( gravity.y <= 0.0f && IsPushedUp() ) || ( gravity.y >= 0.0f && IsPushedDown() ) )
	{
		ClearPushes();
		this->SetAnimationIfPossible("stuck_vertical", false);
	}

	// ������ ��������
	if ( this->gravity.y != 0.0f && this->activity != oatDying )
	{
		if ( this->movement == omtJumping && old_movement != omtJumping  )
		{
			if ( this->activity != oatDying && this->sprite->cur_anim != "touch" ) 
				this->SetAnimationIfPossible("jump", true);
		}
		if ( this->movement != omtJumping && old_movement == omtJumping )
		{
			if ( this->activity != oatDying && this->sprite->cur_anim != "touch" )
				this->SetAnimationIfPossible("land", true);
		}
		if ( IsOnPlane() )
		{
			ObjPhysic* susp_plane = GetSuspectedPlane();
			float vx = vel.x + acc.x + own_vel.x + gravity.x;
			if ( susp_plane && ((vx < 0 && aabb.Left() <= susp_plane->aabb.Left()) || (vx > 0 && aabb.Right() >= susp_plane->aabb.Right() )) )
			{
				if ( sprite && sprite->GetAnimation("edge") != NULL )
				{
					sprite->SaveState();
					SetAnimation("edge", true);
				}
			}
		}
	}

//	if ( vel.x + own_vel.x > 0 )
//		this->SetFacing( false );
//	else if ( vel.x + own_vel.x < 0 )
//		this->SetFacing( true );

}

extern UINT internal_time;

bool ObjEnemy::ReduceHealth( UINT damage )
{
	bool ret = ObjCharacter::ReduceHealth( damage );
	//if ( ret && target )
	//	target->childDead(id);
	return ret;
}

void ObjEnemy::Touch(ObjPhysic *obj)
{
	if ( !this->IsTouchable() ) return;
	if ( obj->type == objBullet && obj->parentConnection->getParent() && obj->parentConnection->getParent() == this) return;
	Animation* a = this->sprite->GetAnimation("touch");
	if (a)
	{
		this->ClearTouchable();
		this->sprite->SaveState();
		this->sprite->stack->Push(obj->id);
		this->SetAnimation("touch", false);
	}
}

void ObjEnemy::ParentEvent( ObjectEventInfo info )
{
	if ( !attached ) return;
	if ( !parentConnection->getParent() ) return;
	if ( activity == oatDying ) return;
	vel.x = vel.y = 0;
	switch ( info.type )
	{
		case eventDead:
			{
				this->SetAnimation( "die", false );
				break;
			}
		case eventMoved:
		case eventCollisionPushed:
			{
				aabb.p = parentConnection->getParent()->aabb.p;
				break;
			}
		case eventFacingChange:
			{
				this->vel.x *= -1;
				this->acc.x *= -1;
				this->SetFacing( !this->GetFacing() );
				break;
			}
		default: break;
	}
}

void ObjEnemy::UpdateTarget(ObjCharacter* target)
{
	if (!target) return;
	if ( this->target )
		this->target->childDead(id);
	target->targeted_by->children.push_back( this );
	this->target = target->targeted_by;
}

ObjCharacter* ObjEnemy::GetTarget()
{
	ObjCharacter* ret = NULL;
	if ( target ) ret = (ObjCharacter*)target->getParent();
	return ret;
}
