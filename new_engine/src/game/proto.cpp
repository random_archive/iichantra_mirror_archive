#include "StdAfx.h"

#include "proto.h"
#include "editor.h"
#include "../misc.h"
#include "../script/script.h"

#include "../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern lua_State* lua;
extern ResourceMgr<Proto> * protoMgr;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

//////////////////////////////////////////////////////////////////////////

Proto::Proto(string name, unsigned char* buffer, size_t buffer_size) : 
	Resource(name, buffer, buffer_size),

	// sprite
	texture(NULL),
	overlayCount(0),
	overlayUsage(NULL),
	ocolor(NULL),
	z(0.0f),
	color(1.0f, 1.0f, 1.0f, 1.0f),
	outline_color(0.0f, 0.0f, 0.0f, 0.0f),
	blendingMode(bmLast),
	renderMethod(rsmStandart),
	frame_widht(0),
	frame_height(0),
	animations(NULL),
	animationsCount(0),
	animNames(),
	mpCount(0),
	polygons(),
	isometric_depth_x(0.0f),
	isometric_depth_y(0.0f),

	// physic
	ghost_to(0),
	phys_solid(0),
	phys_one_sided(0),
	phys_ghostlike(0),
	phys_bullet_collidable(0),
	physic(0),
	slopeType(0),
	touch_detection(0),
	material_name(NULL),
#ifdef MAP_EDITOR
	editor_color( 0.0f, 0.5f, 0.5f, 0.5f ),
#endif //MAP_EDITOR

	// dynamic
	shadow_width(0.25f),
	drops_shadow(0),
	mass(1.0f),
	facing(0),
	phys_walk_acc(0.0f),
	phys_jump_vel(0.0f),
	phys_max_x_vel(0.0f),
	phys_max_y_vel(0.0f),
	bounce(0.0f),
	gravity_x(0.0f),
	gravity_y(0.0f),

	// env
	walk_vel_multiplier(1.0f),
	jump_vel_multiplier(1.0f),
	bounce_bonus(0.0f),
	friction(1.0f),
	sounds(),
	sprites(),
	gravity_bonus(),
	env_material(0),
	env_onenter(NULL),
	env_onleave(NULL),
	env_onstay(NULL),
	env_onuse(NULL),
	env_func_onenter(LUA_NOREF),
	env_func_onleave(LUA_NOREF),
	env_func_onstay(LUA_NOREF),
	env_func_onuse(LUA_NOREF),
	stop_global_wind(0),
	wind(),

	// bullet
	bullet_vel(0),
	hurts_same_type(0),
	push_force(0.0f),
	bullet_damage(0),
	multiple_targets(0),
	damage_type(0),

	// ray
	ray_first_frame_shift(0),
	time_to_live(0),
	end_effect(NULL),
	hit_effect(NULL),
	next_shift_y(0),

	// chracter
	faction_id(0),
	faction_hates(),
	faction_follows(),
	aiming_speed(0.05f),

	// player
	air_control(0.3f),
	walljump_vel(0.0f),
	recovery(50),
	additional_jumps(0),
	walljumps(0),
	health(0),
	max_health(0),
	ammo(100),
	main_weapon(NULL),
	alt_weapon(NULL),

	// enemy
	offscreen_distance(0.0f),
	offscreen_behavior(0),

	// effect
	effect(0),

	// weapon
	recoil(0.0f),
	charge_effect(NULL),
	flash(NULL),
	reload_time(0),
	bullets_count(0),
	shots_per_clip(0),
	clip_reload_time(0),
	is_ray_weapon(0),
	charge_hold(0),
	fire_script(LUA_NOREF),

	// particle
	start_color(),
	end_color(),
	var_color(),
	max_particles(0),
	particle_life_min(0.0f),
	particle_life_max(0.0f),
	start_size(0.0f),
	size_variability(0.0f),
	end_size(0.0f),
	particle_life(0.0f),
	particle_life_var(0.0f),
	system_life(0.0f),
	emission(0.0f),
	min_speed(0.0f),
	max_speed(0.0f),
	min_angle(0.0f),
	max_angle(0.0f),
	min_trace(0.0f),
	max_trace(0.0f),
	trace_width(1.0f),
	min_param(0.0f),
	max_param(0.0f),
	trajectory(0),
	trajectory_param1(0.0f),
	trajectory_param2(0.0f),
	affected_by_wind(0),
	max_bounce(-1),
	particle_anim(-1),
	particle_anim_param(0),
	particle_max_abs_speed(-1.0f)
{

}

Proto::~Proto()
{
	// sprite
	DELETEARRAY(texture);
	DELETEARRAY(overlayUsage);
	DELETEARRAY(ocolor);
	DELETEARRAY(animations);
	for (vector<CPolygon*>::iterator it = polygons.begin(); 
		it != polygons.end();
		++it)
	{
		DELETESINGLE(*it);
	}

	// physic

	// dynamic

	// env
	for (vector<char*>::iterator it = sounds.begin(); 
		it != sounds.end();
		++it)
	{
		DELETEARRAY(*it);
	}
	for(vector<char*>::iterator it = sprites.begin(); 
		it != sprites.end();
		it++)
	{
		DELETEARRAY(*it);
	}
	DELETEARRAY(env_onenter);
	DELETEARRAY(env_onleave);
	DELETEARRAY(env_onstay);
	DELETEARRAY(env_onuse);
	SCRIPT::ReleaseProc(&env_func_onenter);
	SCRIPT::ReleaseProc(&env_func_onleave);
	SCRIPT::ReleaseProc(&env_func_onstay);
	SCRIPT::ReleaseProc(&env_func_onuse);

	// bullet

	// ray
	DELETEARRAY(end_effect);
	DELETEARRAY(hit_effect);

	// character

	// player
	DELETEARRAY(main_weapon);
	DELETEARRAY(alt_weapon);

	// enemy

	// effect

	// weapon
	DELETEARRAY(charge_effect);
	DELETEARRAY(flash);
	SCRIPT::ReleaseProc(&fire_script);

	// particle

}


/// Loads prototype from string lua-script
bool Proto::Load()
{
	if( buffer == NULL)
		return false;

	STACK_CHECK_INIT(lua);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading proto: %s", this->name.c_str());

	// ���� �������� ����� �������� � ���������� ���������, ����� �� �������� ���-���� ����������.
	// ��������� ��������� ��� �����: http://community.livejournal.com/ru_lua/402.html
	lua_newtable(lua);				// ����: env
	lua_newtable(lua);				// ����: env meta
	lua_getglobal(lua, "_G");			// ����: env meta _G
	lua_setfield(lua, -2, "__index");	// ����: env meta
	lua_setmetatable(lua, -2);		// ����: env

	if( luaL_loadbuffer(lua, (const char*)buffer, buffer_size, this->name.c_str()) )
	{
		// ����: env err
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "While trying to load proto '%s': %s", name.c_str(), lua_tostring(lua, -1));
		lua_pop(lua, 2);	// ����:
		STACK_CHECK(lua);
		return false;
	}
	// ����: env loadfile
	lua_pushvalue(lua, -2);			// ����: env loadfile env
	lua_setfenv(lua, -2);				// ����: env loadfile

	if(lua_pcall(lua, 0, 0, 0))
	{
		// �����-�� ������ ���������� �����
		const char* err = lua_tostring(lua, -1);	// ����: env err
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, err );
		Log(DEFAULT_LOG_NAME, logLevelError, "Unable to load proto %s", name.c_str() );
		lua_pop(lua, 2);	// ����: 

		STACK_CHECK(lua);

		return false;
	}
	else
	{
		LoadTable(lua);


	} // if lua_pcall

	lua_pop(lua, 1);						// ����: 

	STACK_CHECK(lua);

	return true;
}





void Proto::Copy(const Proto& src) 
{
	//sprite
	DELETEARRAY(texture); texture = StrDupl(src.texture);
	overlayCount = src.overlayCount;
	DELETEARRAY(overlayUsage);
	if (src.overlayUsage)
	{
		overlayUsage = new UINT[src.overlayCount];
		std::copy(src.overlayUsage, src.overlayUsage + src.overlayCount, overlayUsage);
	}
	DELETEARRAY(ocolor);
	if (src.ocolor)
	{
		ocolor = new RGBAf[src.overlayCount];
		std::copy(src.ocolor, src.ocolor + src.overlayCount, ocolor);
	}
	z = src.z;
	color = src.color;
	outline_color = src.outline_color;
	blendingMode = src.blendingMode;
	renderMethod = src.renderMethod;
	frame_widht = src.frame_widht;
	frame_height = src.frame_height;
	animations = new Animation[src.animationsCount];
	std::copy(src.animations, src.animations + src.animationsCount, animations);
	animationsCount = src.animationsCount;
	animNames = src.animNames;
	mpCount = src.mpCount;
	polygons.clear(); std::copy(src.polygons.begin(), src.polygons.end(), polygons.begin());
	isometric_depth_x = src.isometric_depth_x;
	isometric_depth_y = src.isometric_depth_y;

	// physic
	ghost_to = src.ghost_to;
	phys_solid = src.phys_solid;
	phys_one_sided = src.phys_one_sided;
	phys_ghostlike = src.phys_ghostlike;
	phys_bullet_collidable = src.phys_bullet_collidable;
	physic = src.physic;
	slopeType = src.slopeType;
	touch_detection = src.touch_detection;
	material_name = src.material_name;
#ifdef MAP_EDITOR 
	editor_color = src.editor_color;
#endif //MAP_EDITOR

	// dynamic
	shadow_width = src.shadow_width;
	drops_shadow = src.drops_shadow;
	mass = src.mass;
	facing = src.facing;
	phys_walk_acc = src.phys_walk_acc;
	phys_jump_vel = src.phys_jump_vel;
	phys_max_x_vel = src.phys_max_x_vel;
	phys_max_y_vel = src.phys_max_y_vel;
	bounce = src.bounce;
	gravity_x = src.gravity_x;
	gravity_y = src.gravity_y;

	// env
	walk_vel_multiplier = src.walk_vel_multiplier;
	jump_vel_multiplier = src.jump_vel_multiplier;
	bounce_bonus = src.bounce_bonus;
	friction = src.friction;
	sounds.clear();
	for (vector<char*>::const_iterator it = src.sounds.begin(); it != src.sounds.end(); it++)
		sounds.push_back(StrDupl(*it));
	sprites.clear();
	for (vector<char*>::const_iterator it = src.sprites.begin(); it != src.sprites.end(); it++)
		sprites.push_back(StrDupl(*it));
	gravity_bonus = src.gravity_bonus;
	env_material = src.env_material;
	DELETEARRAY(env_onenter); env_onenter = StrDupl(src.env_onenter);
	DELETEARRAY(env_onleave); env_onleave = StrDupl(src.env_onleave);
	DELETEARRAY(env_onstay);  env_onstay  = StrDupl(src.env_onstay);
	DELETEARRAY(env_onuse);   env_onuse   = StrDupl(src.env_onuse);
	env_func_onenter = SCRIPT::ReserveAndReturnProc(src.env_func_onenter);
	env_func_onleave = SCRIPT::ReserveAndReturnProc(src.env_func_onleave);
	env_func_onstay  = SCRIPT::ReserveAndReturnProc(src.env_func_onstay);
	env_func_onuse   = SCRIPT::ReserveAndReturnProc(src.env_func_onuse);
	stop_global_wind = src.stop_global_wind;
	wind = src.wind;

	// bullet
	bullet_vel = src.bullet_vel;
	hurts_same_type = src.hurts_same_type;
	push_force = src.push_force;
	bullet_damage = src.bullet_damage;
	multiple_targets = src.multiple_targets;
	damage_type = src.damage_type;

	// ray
	ray_first_frame_shift = src.ray_first_frame_shift;
	time_to_live = src.time_to_live;
	DELETEARRAY(end_effect); end_effect = StrDupl(src.end_effect);
	DELETEARRAY(hit_effect); hit_effect = StrDupl(src.hit_effect);
	next_shift_y = src.next_shift_y;

	// character
	faction_id = src.faction_id;
	faction_hates.clear();   std::copy(src.faction_hates.begin(), src.faction_hates.end(), std::back_inserter(faction_hates));
	faction_follows.clear(); std::copy(src.faction_follows.begin(), src.faction_follows.end(), std::back_inserter(faction_follows));
	aiming_speed = src.aiming_speed;

	// player
	air_control = src.air_control;
	walljump_vel = src.walljump_vel;
	recovery = src.recovery;
	additional_jumps = src.additional_jumps;
	walljumps = src.walljumps;
	health = src.health;
	max_health = src.max_health;
	ammo = src.ammo;
	DELETEARRAY(main_weapon); main_weapon = StrDupl(src.main_weapon);
	DELETEARRAY(alt_weapon);  alt_weapon  = StrDupl(src.alt_weapon);

	// enemy
	offscreen_behavior = src.offscreen_behavior;
	offscreen_distance = src.offscreen_distance;

	// effect
	effect = src.effect;

	// weapon
	recoil = src.recoil;
	DELETEARRAY(charge_effect); charge_effect = StrDupl(src.charge_effect);
	DELETEARRAY(flash); flash = StrDupl(src.flash);
	reload_time = src.reload_time;
	bullets_count = src.bullets_count;
	shots_per_clip = src.shots_per_clip;
	clip_reload_time = src.clip_reload_time;
	is_ray_weapon = src.is_ray_weapon;
	charge_hold = src.charge_hold;
	fire_script = SCRIPT::ReserveAndReturnProc(src.fire_script);;

	// particle
	start_color = src.start_color;
	end_color = src.end_color;
	var_color = src.var_color;
	max_particles = src.max_particles;
	particle_life_min = src.particle_life_min;
	particle_life_max = src.particle_life_max;
	start_size = src.start_size;
	size_variability = src.size_variability;
	end_size = src.end_size;
	particle_life = src.particle_life;
	particle_life_var = src.particle_life_var;
	system_life = src.system_life;
	emission = src.emission;
	min_speed = src.min_speed;
	max_speed = src.max_speed;
	min_angle = src.min_angle;
	max_angle = src.max_angle;
	min_trace = src.min_trace;
	max_trace = src.max_trace;
	trace_width = src.trace_width;
	min_param = src.min_param;
	max_param = src.max_param;
	trajectory = src.trajectory;
	trajectory_param1 = src.trajectory_param1;
	trajectory_param2 = src.trajectory_param2;
	affected_by_wind = src.affected_by_wind;
	max_bounce = src.max_bounce;
	particle_anim = src.particle_anim;
	particle_anim_param = src.particle_anim_param;
	particle_max_abs_speed = src.particle_max_abs_speed;
}


//////////////////////////////////////////////////////////////////////////
// Prototype loading

void Proto::LoadTable(lua_State* L)
{
	STACK_CHECK_INIT(L);

	assert(lua_istable(L, -1));

	char* temp_str = NULL;
	SCRIPT::GetFieldByName(L, "parent", temp_str);
	if (temp_str)
	{
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Attempting to get parent proto %s", temp_str);
		const Proto* parent = protoMgr->GetByName(temp_str);
		if (parent)
		{
			this->Copy(*parent);
		}
		else
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "Unable to get parent proto %s", temp_str);
		}
	}
	DELETEARRAY(temp_str);
	
	// ����: env

	LoadSpriteFields(L);
	LoadPhysicFields(L);
	LoadDynamicFields(L);
	LoadEnvironmentFields(L);
	LoadBulletFields(L);
	LoadRayFields(L);
	LoadCharacterFields(L);
	LoadPlayerFields(L);
	LoadEnemyFields(L);
	LoadEffectFields(L);
	LoadWeaponFields(L);
	LoadParticleFields(L);

	STACK_CHECK(L);
}

__INLINE void GET_STR(lua_State* L, const char* name, char*& field)
{
	char* temp_str = NULL;
	SCRIPT::GetFieldByName(L, name, temp_str);
	if (temp_str)
	{
		DELETEARRAY(field);
		field = temp_str;
	}
}

void Proto::LoadSpriteFields( lua_State* L )
{
	GET_STR(L, "texture", texture);
	LoadSpriteOverlay(L);
	SCRIPT::GetFieldByName(L, "z", this->z);
	SCRIPT::GetFieldByName(L, "color", this->color);
	SCRIPT::GetFieldByName(L, "outline_color", this->outline_color);
	SCRIPT::GetFieldByName(L, "blendingMode", this->blendingMode);
	SCRIPT::GetFieldByName(L, "renderMethod", this->renderMethod);
	SCRIPT::GetFieldByName(L, "frame_width", this->frame_widht);
	SCRIPT::GetFieldByName(L, "frame_height", this->frame_height);
	LoadSpriteAnims(L);
	SCRIPT::GetFieldByName(L, "mp_count", this->mpCount);
	LoadSpritePolygons(L);
	SCRIPT::GetFieldByName(L, "isometric_depth_x", this->isometric_depth_x);
	SCRIPT::GetFieldByName(L, "isometric_depth_y", this->isometric_depth_y);
}
void Proto::LoadSpriteOverlay( lua_State* L )
{
	// overlay
	// ����: env
	lua_getfield(L, -1, "overlay");	// ����: env overlayCount
	if (lua_istable(L, -1))
	{
		this->overlayCount = (UINT)lua_objlen(L, -1);
		if (overlayCount)
		{
			this->overlayUsage = new UINT[this->overlayCount];
			UINT i = 0;
			for(lua_pushnil(L); lua_next(L, -2); i++, lua_pop(L, 1))
			{
				this->overlayUsage[i] = (UINT)lua_tointeger(L, -1);
			}

			i = 0;
			this->ocolor = new RGBAf[this->overlayCount];

			lua_getfield(L, -2, "ocolor");
			if (lua_istable(L, -1))
			{
				int top = lua_gettop(L);
				for(lua_pushnil(L); i < this->overlayCount && lua_next(L, -2); i++, lua_pop(L, 1))
				{
					SCRIPT::GetColorFromTable(L, -1, this->ocolor[i]);
				}
				// ���� settop ����� �� ������, ���� �� ������� �� ���� ocolor - lua_next ����� �� ������� ��������� ������� �� �����.
				lua_settop(L, top);
			}
			lua_pop(L, 1);						// ����: env

			if (i < this->overlayCount)
			{
				Log(DEFAULT_LOG_NAME, logLevelWarning, "Proto %s specifies less overlay colors than it has overlays. Using standard ones.", this->name.c_str());
				for (; i < this->overlayCount; i++)
					this->ocolor[i] = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
			}
		}
		else
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "In proto %s: table overlayCount is empty", this->name.c_str());
		}
	}
	lua_pop(L, 1);						// ����: env
}
void Proto::LoadSpriteAnims( lua_State* L )
{
	STACK_CHECK_INIT(L);
	// ����: env
	lua_getfield(L, -1, "animations");	// ����: env animations
	if (lua_istable(L, -1))
	{
		const size_t count = lua_objlen(L, -1);
		if (count == 0)
		{
			lua_pop(L, 1);						// ����: env
			return;
		}

		Animation* a = new Animation[count];
		char* name_buf = NULL;
		size_t nbFrames = 0;
		AnimationFrame* af = NULL;
		map<string, size_t> names;

		// ���� �� ������� ��������
		size_t i = 0;
		// ���� � ������� ���� ���-��, �������� �� ������, ����� ���������� ������ ����� �� �����������
		//for (UINT i = 1; i <= count; i++)
		// ����: env animations nil
		for(lua_pushnil(L); lua_next(L, -2); lua_pop(L, 1), i++)
		{
			// ����: env animations key animations[key]
			if (lua_istable(L, -1))
			{
				// �������� ������� ��������� ��������, ������ ��
#if defined(DEBUG) 
				// Explicitly setting NULL to prevent assertion fail in getFromLua<char*>
				// There will be no memleak.
				name_buf = NULL;
#endif
				// name_buf ����� ��������� � ��������, ���� ��� ����� ��. ������ ����� ����������� ��� � ���� �������.
				SCRIPT::GetFieldByName(L, "name", name_buf);

				lua_getfield(L, -1, "frames");	// ����: env animations key animations[key] frames
				if (lua_istable(L, -1))
				{
					nbFrames = lua_objlen(L, -1);
					if (nbFrames > 0 && nbFrames < MAX_ANIMFRAMES_COUNT)
					{
						af = new AnimationFrame[nbFrames];

						size_t j = 0;
						// ����: env animations key animations[key] frames nil
						for(lua_pushnil(L); lua_next(L, -2); lua_pop(L, 1), j++)
						{
							// ����: env animations key animations[key] frames key frame
							assert(j < nbFrames);
							if (lua_istable(L, -1))
							{
								SCRIPT::GetFieldByName(L, "num", af[j].num);
								SCRIPT::GetFieldByName(L, "dur", af[j].duration);
								SCRIPT::GetFieldByName(L, "poly", af[j].polygon);
								SCRIPT::GetFieldByName(L, "com", af[j].command);		// TODO: ��, �������� ������
								//I don't like this check one bit. I think we should just always check if it is a function or not.
								//Then we check if function actually exists in animation command itself.
								//With this one we efficiently have two separate places defining said commands.
								//But I'll roll with it for the time being.
								//if (af[j].command == afcCallLocalFunction || af[j].command == afcSetProcessor)

								lua_getfield(L, -1, "param"); // st: ... frames key frame param
								if (lua_isfunction(L, -1))
								{
									SCRIPT::RegProc(L, static_cast<LuaRegRef*>(&(af[j].param)), -1);
									af[j].is_param_function = true;

									// st: ... frames key frame
								}
								else
								{
									lua_pop(L, 1); // st: ... frames key frame
									SCRIPT::GetFieldByName(L, "param", af[j].param);
								}
								
								SCRIPT::GetFieldByName(L, "txt", af[j].txt_param);
							}
							else
							{
								Log(DEFAULT_LOG_NAME, logLevelWarning, "In proto '%s', anim '%s' frame %d - %s: Frame loading canceled (not a table)", 
									this->name.c_str(), name_buf, j, lua_typename(L, lua_type(L, -1)) );

								DELETEARRAY(af);
								nbFrames = 0;
							}
							//lua_pop(L, 1);	// ����: env animations key animations[key] frames key
						}
					}
					else
					{						
						Log(DEFAULT_LOG_NAME, logLevelError, "There are %d frames in animation '%s' of proto '%s'. Skipping.", nbFrames, name_buf, this->name.c_str());
						nbFrames = 0;
						DELETEARRAY(name_buf);
					}

					// ����: env animations key animations[key] frames	

				} // if (lua_istable(L, -1))	frames
				else
				{						
					Log(DEFAULT_LOG_NAME, logLevelError, "Anim '%s' in proto '%s' have no frames table. Skipping.", name_buf, this->name.c_str());
					nbFrames = 0;
					DELETEARRAY(name_buf);
				}

				if (nbFrames > 0 && nbFrames < MAX_ANIMFRAMES_COUNT)
				{
					//this->animations.push_back(ap);
					a[i].frames = af;
					a[i].frameCount = (USHORT)nbFrames;

					a[i].name = name_buf;

					if (name_buf)
					{
						// TODO: �������� �� ����������
						names[string(name_buf)] = i;
					}
				}

				// ����: env animations key animations[key] frames
				lua_pop(L, 1);	// ����: env animations key animations[key]
			}
			else
			{
				Log(DEFAULT_LOG_NAME, logLevelWarning, "In proto '%s' animations[%d] is %s", 
					this->name.c_str(), i, lua_typename(L, lua_type(L, -1)) );
			}

		} // for 

		if (this->animations)
		{
			bool* copy_anim = (bool*)malloc(count);
			memset(copy_anim, true, count);
			size_t nbCopy = count;

			map<string, size_t>::iterator fit;
			assert(names.size() == count);
			// ��� �������� �������� ���������� �� ��, ��� ��������� � �������.
			for(map<string, size_t>::iterator it = names.begin(); 
				it != names.end(); )
			{
				fit = this->animNames.find(it->first);
				if (fit != this->animNames.end())
				{
					// ��� ���������� ����������� ����� �������� � ������, ��� ��� ����� �������� ������.
					this->animations[fit->second] = a[it->second];
					copy_anim[it->second] = false;
					nbCopy--;

					names.erase(it++);
					continue;
				}

				it++;
			}

			if (nbCopy > 0)
			{
				// � ������� ��������� ������ ����� ��������
				// ��� ��������� ������ ������� ������� � � ���� ���������� � ������, � ������ �����.
				size_t new_size =  this->animationsCount + nbCopy;
				Animation* new_arr = new Animation[new_size];
				size_t i = 0;
				// �������� � ����� ������ ������ � �����, ����������� ������.
				for (; i < this->animationsCount; i++)
				{
					new_arr[i] = this->animations[i];
				}

				// �������� ������ �����.
				for(size_t j = 0; j < count && nbCopy > 0; j++)
				{
					if (copy_anim[j])
					{
						new_arr[i] = a[j];
						names[a[j].name] = i;
						i++;
						nbCopy--;
					}
				}

				this->animationsCount = new_size;
				// ��������� � ������ ������ ������ �����.
				this->animNames.insert(names.begin(), names.end());
				DELETEARRAY(this->animations);
				this->animations = new_arr;
			}
			DELETEARRAY(a);
			free(copy_anim);
		}
		else
		{
			this->animations = a;
			this->animationsCount = count;
			this->animNames = names;
		}



		// ����: env animations

	} // if (lua_istable(L, -1))   animations	

	lua_pop(L, 1);						// ����: env
	STACK_CHECK(L);
}
void Proto::LoadSpritePolygons( lua_State* L )
{
	lua_getfield( L, -1, "polygons" );
	if (lua_istable(L, -1))
	{
		for	( int i = 1; i <= (int)lua_objlen(L, -1); i++ )			//Polygons
		{
			lua_rawgeti( L, -1, i );
			CPolygon* poly = SCRIPT::GetPolygonField(L, -1);
			if ( poly )	polygons.push_back( poly );
			lua_pop( L, 1 );
		}
	}
	lua_pop(L, 1);
}
void Proto::LoadPhysicFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "ghost_to", this->ghost_to);
	SCRIPT::GetFieldByName(L, "phys_solid", this->phys_solid);
	SCRIPT::GetFieldByName(L, "phys_one_sided", this->phys_one_sided);
	SCRIPT::GetFieldByName(L, "phys_ghostlike", this->phys_ghostlike);
	SCRIPT::GetFieldByName(L, "phys_bullet_collidable", this->phys_bullet_collidable);
	SCRIPT::GetFieldByName(L, "physic", this->physic);
	SCRIPT::GetFieldByName(L, "phys_slope", this->slopeType);
	SCRIPT::GetFieldByName(L, "touch_detection", this->touch_detection);
	GET_STR(L, "material_name", material_name);
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		SCRIPT::GetFieldByName(L, "editor_color", this->editor_color );
	}
#endif //MAP_EDITOR
}
void Proto::LoadDynamicFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "shadow_width", this->shadow_width);
	SCRIPT::GetFieldByName(L, "drops_shadow", this->drops_shadow);
	SCRIPT::GetFieldByName(L, "mass", this->mass);
	SCRIPT::GetFieldByName(L, "facing", this->facing);
	SCRIPT::GetFieldByName(L, "phys_walk_acc", this->phys_walk_acc);
	SCRIPT::GetFieldByName(L, "phys_jump_vel", this->phys_jump_vel);
	SCRIPT::GetFieldByName(L, "phys_max_x_vel", this->phys_max_x_vel);
	SCRIPT::GetFieldByName(L, "phys_max_y_vel", this->phys_max_y_vel);
	SCRIPT::GetFieldByName(L, "bounce", this->bounce);
	SCRIPT::GetFieldByName(L, "gravity_x", this->gravity_x);
	SCRIPT::GetFieldByName(L, "gravity_y", this->gravity_y);
}
void Proto::LoadEnvironmentFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "walk_vel_multiplier", this->walk_vel_multiplier);
	SCRIPT::GetFieldByName(L, "jump_vel_multiplier", this->jump_vel_multiplier);
	SCRIPT::GetFieldByName(L, "bounce_bonus", this->bounce_bonus);
	SCRIPT::GetFieldByName(L, "friction", this->friction);
	LoadEnvironmentSounds(L);
	LoadEnvironmentSprites(L);
	SCRIPT::GetFieldByName(L, "gravity_bonus_x", this->gravity_bonus.x);
	SCRIPT::GetFieldByName(L, "gravity_bonus_y", this->gravity_bonus.y);
	SCRIPT::GetFieldByName(L, "material", this->env_material);
	GET_STR(L, "script_on_enter", env_onenter);
	GET_STR(L, "script_on_leave", env_onleave);
	GET_STR(L, "script_on_stay", env_onstay);
	GET_STR(L, "script_on_use", env_onuse);
	SCRIPT::GetFunctionFieldByName(L, "script_on_enter", &this->env_func_onenter );
	SCRIPT::GetFunctionFieldByName(L, "script_on_leave", &this->env_func_onleave );
	SCRIPT::GetFunctionFieldByName(L, "script_on_stay", &this->env_func_onstay );
	SCRIPT::GetFunctionFieldByName(L, "script_on_use", &this->env_func_onuse );
	SCRIPT::GetFieldByName(L, "stop_global_wind", this->stop_global_wind);
	SCRIPT::GetFieldByName(L, "wind_x", this->wind.x);
	SCRIPT::GetFieldByName(L, "wind_y", this->wind.y);
}
void Proto::LoadEnvironmentSounds( lua_State* L )
{
	lua_getfield(L, -1, "sounds");		// ����: env sounds
	if (lua_istable(L, -1))
	{
		lua_pushnil(L);
		while (lua_next(L, -2) != 0) 
		{
			const char* str = lua_tostring(L, -1);
			char* val = new char[strlen(str) + 1];
			strcpy(val, str);
			this->sounds.push_back(val);
			lua_pop(L, 1);
		}
	}
	lua_pop(L, 1);						// ����: env
}
void Proto::LoadEnvironmentSprites( lua_State* L )
{
	// ����: env
	lua_getfield(L, -1, "sprites");		// ����: env sprites
	if (lua_istable(L, -1))
	{
		lua_pushnil(L);
		while (lua_next(L, -2) != 0) 
		{
			const char* str = lua_tostring(L, -1);
			char* val = new char[strlen(str) + 1];
			strcpy(val, str);
			this->sprites.push_back(val);
			lua_pop(L, 1);
		}
	}
	lua_pop(L, 1);						// ����: env
}
void Proto::LoadBulletFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "bullet_vel", this->bullet_vel);
	SCRIPT::GetFieldByName(L, "hurts_same_type", this->hurts_same_type);
	SCRIPT::GetFieldByName(L, "push_force", this->push_force);
	SCRIPT::GetFieldByName(L, "bullet_damage", this->bullet_damage);
	SCRIPT::GetFieldByName(L, "multiple_targets", this->multiple_targets);
	SCRIPT::GetFieldByName(L, "damage_type", this->damage_type);
}
void Proto::LoadRayFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "ray_first_frame_shift", this->ray_first_frame_shift);
	SCRIPT::GetFieldByName(L, "time_to_live", this->time_to_live);
	GET_STR(L, "end_effect", end_effect);
	GET_STR(L, "hit_effect", hit_effect);
	SCRIPT::GetFieldByName(L, "next_shift_y", this->next_shift_y);
}
void Proto::LoadCharacterFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "faction_id", this->faction_id);
	SCRIPT::GetIntVectorFieldByName( L, "faction_hates", this->faction_hates, false );
	SCRIPT::GetIntVectorFieldByName( L, "faction_follows", this->faction_follows, false );
	SCRIPT::GetFieldByName(L, "aiming_speed", this->aiming_speed);
}
void Proto::LoadPlayerFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "air_control", this->air_control);
	SCRIPT::GetFieldByName(L, "walljump_vel", this->walljump_vel);	
	SCRIPT::GetFieldByName(L, "recovery", this->recovery);
	SCRIPT::GetFieldByName(L, "additional_jumps", this->additional_jumps);
	SCRIPT::GetFieldByName(L, "walljumps", this->walljumps);
	SCRIPT::GetFieldByName(L, "health", this->health);
	SCRIPT::GetFieldByName(L, "max_health", this->max_health);
	SCRIPT::GetFieldByName(L, "ammo", this->ammo);
	GET_STR(L, "main_weapon", main_weapon);
	GET_STR(L, "alt_weapon", alt_weapon);
}
void Proto::LoadEnemyFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "offscreen_distance", this->offscreen_distance);
	SCRIPT::GetFieldByName(L, "offscreen_behavior", this->offscreen_behavior);
}
void Proto::LoadEffectFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "effect", this->effect);
}
void Proto::LoadWeaponFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "recoil", this->recoil);
	GET_STR(L, "flash", flash);
	GET_STR(L, "charge_effect", charge_effect);
	SCRIPT::GetFieldByName(L, "reload_time", this->reload_time);
	SCRIPT::GetFieldByName(L, "bullets_per_shot", this->bullets_count);
	SCRIPT::GetFieldByName(L, "shots_per_clip", this->shots_per_clip);
	SCRIPT::GetFieldByName(L, "clip_reload_time", this->clip_reload_time);
	SCRIPT::GetFieldByName(L, "is_ray_weapon", this->is_ray_weapon);
	SCRIPT::GetFieldByName(L, "charge_hold", this->charge_hold);
	SCRIPT::GetFunctionFieldByName(L, "fire_script", &this->fire_script );
}
void Proto::LoadParticleFields( lua_State* L )
{
	SCRIPT::GetFieldByName(L, "start_color", this->start_color);
	SCRIPT::GetFieldByName(L, "end_color", this->end_color);
	SCRIPT::GetFieldByName(L, "var_color", this->var_color);
	SCRIPT::GetFieldByName(L, "max_particles", this->max_particles);
	SCRIPT::GetFieldByName(L, "particle_life_min", this->particle_life_min);
	SCRIPT::GetFieldByName(L, "particle_life_max", this->particle_life_max);
	SCRIPT::GetFieldByName(L, "start_size", this->start_size);
	SCRIPT::GetFieldByName(L, "size_variability", this->size_variability);
	SCRIPT::GetFieldByName(L, "end_size", this->end_size);
	SCRIPT::GetFieldByName(L, "particle_life", this->particle_life);
	SCRIPT::GetFieldByName(L, "particle_life_var", this->particle_life_var);
	SCRIPT::GetFieldByName(L, "system_life", this->system_life);
	SCRIPT::GetFieldByName(L, "emission", this->emission);
	SCRIPT::GetFieldByName(L, "particle_min_speed", this->min_speed);
	SCRIPT::GetFieldByName(L, "particle_max_speed", this->max_speed);
	SCRIPT::GetFieldByName(L, "particle_min_angle", this->min_angle);
	SCRIPT::GetFieldByName(L, "particle_max_angle", this->max_angle);
	SCRIPT::GetFieldByName(L, "particle_min_trace", this->min_trace);
	SCRIPT::GetFieldByName(L, "particle_max_trace", this->max_trace);
	SCRIPT::GetFieldByName(L, "particle_trace_width", this->trace_width);
	SCRIPT::GetFieldByName(L, "particle_min_param", this->min_param);
	SCRIPT::GetFieldByName(L, "particle_max_param", this->max_param);
	SCRIPT::GetFieldByName(L, "trajectory_type", this->trajectory);
	SCRIPT::GetFieldByName(L, "trajectory_param1", this->trajectory_param1);
	SCRIPT::GetFieldByName(L, "trajectory_param2", this->trajectory_param2);
	SCRIPT::GetFieldByName(L, "affected_by_wind", this->affected_by_wind);
	SCRIPT::GetFieldByName(L, "max_bounce", this->max_bounce);
	SCRIPT::GetFieldByName(L, "particle_anim", this->particle_anim);
	SCRIPT::GetFieldByName(L, "particle_anim_param", this->particle_anim_param);
	SCRIPT::GetFieldByName(L, "particle_max_abs_speed", this->particle_max_abs_speed);
}
