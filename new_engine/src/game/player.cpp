#include "StdAfx.h"

#include "player.h"
#include "camera.h"
#include "../config.h"
#include "../script/api.h"

#include "../input_mgr.h"

#include "../resource_mgr.h"

#include "../game/editor.h"

//////////////////////////////////////////////////////////////////////////


extern ResourceMgr<Proto> * protoMgr;

extern InputMgr inpmgr;

extern  Player* playerControl;

extern config cfg;

extern lua_State* lua;

extern game::GameStates game_state;

#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ���������� ��� �������� ������. ��������� ������ �� ������ ������.
// ��� ������ ��������� �������� CreatePlayer �� object_player.cpp
ObjPlayer* Player::Create(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust)
{
	ObjPlayer* plr = CreatePlayer(proto_name, coord, start_anim, corner_adjust);
	if (plr)
	{
		if (character_list.empty())
		{
			current = plr;
			current_num = 0;
			revivePoint = coord;		// �� ���������, ���� �������� � ����� �������� ������� ������
		}
		else
		{
#ifdef MAP_EDITOR
			if ( editor_state != editor::EDITOR_OFF )
			{
				plr->sprite->color = RGBAf( 0.5f, 0.5f, 0.6f, 0.5f );
			}
			else
#endif // MAP_EDITOR
			{
				// ������ ����� ���� ��� �� �������� ��� �������� ���� � ������ �� ������
				plr->sprite->ClearVisible();
				plr->SetSleep();
				plr->ClearBulletCollidable();
			}
		}
		
		plr->player = this;
		character_list.push_back(std::pair<ObjPlayer*,std::string>(plr,proto_name));
	}
	
	return plr;
}

// �������� ������ ����� num, ������� �������� �������� start_anim
ObjPlayer* Player::Revive(size_t num, const char* start_anim)
{
	if (num <= 0 || num > character_list.size())
		assert(false);		// ���� �������� ������

	const char* pname = character_list[num-1].second.c_str();

	if (!pname)
		return NULL;		// ����������, ���� ��������

	ObjPlayer* plr = CreatePlayer(pname, revivePoint, start_anim, false);

	//ObjPlayer* another_plr = NULL;
	Proto* aproto = NULL;

	character_list[num-1].first = plr;
	
	// ��������������� ������ ���������� �������� ����
	for(size_t i = 0; i < character_list.size(); i++)
		if (i != num-1)
		{
			aproto = protoMgr->GetByName(character_list[i].second);
			if (aproto)
			{
				if (character_list[i].first->health < static_cast<short>(aproto->health))
					character_list[i].first->health = static_cast<short>(aproto->health);
			}
		}

	if (!current)
	{
		current = plr;
		current_num = num-1;
	}
	current->recovery_time = 100;
	current->is_invincible = true;

	plr->player = this;
	return plr;
}

ObjPlayer* Player::GetByNum(size_t num)
{
	if (num > 0 && num <= character_list.size())
		return character_list[num-1].first;

	return NULL;
}

Player::~Player()
{
	inpmgr.UnregisterInput(input);
	if (input) DELETESINGLE(input);
	SCRIPT::ReleaseProc(&onChangePlayerProcessor);
	SCRIPT::ReleaseProc(&onPlayerDeathProcessor);
}

// ���������� �� ������ ����. ��������� ����� ������� ���������� �����
void Player::Update()
{
	if (character_list.empty())
		return;

	Changer( false, false );

	if ( current && current->input != input )
		current->input = input;
}


// ��������� ����� �������
void Player::Changer( bool forced, bool immediate )
{
	if (!current || character_list.size() < 2)
		return;		// ������ ������

	if ( !forced )
	{
		if (change_blocked)
			return;		// ������ ������

		if (current->activity == oatDying || !current->IsOnPlane())
			return;		// ����� ������� ��� �� ����� �� ������ �����������, ������ ������

	}
	if ( !immediate && (forced || inpmgr.IsProlongedReleased(cakChangePlayer)) )
	{
		current->vel = Vector2(0,0);
		current->activity = oatMorphing;
		current->SetAnimation("morph_out", false);
	}
	if ( (current->activity == oatMorphing && current->sprite->IsAnimDone()) || immediate )
	{
		ObjPlayer* old = current;
		if (current == character_list[character_list.size()-1].first)
		{
			current = character_list[0].first;
			current_num = 0;
		}
		else
		{
			current_num++;
			current = character_list[current_num].first;
		}

		old->SetAnimation("idle", true);

		current->aabb.p = old->aabb.p;
		current->old_aabb = old->old_aabb;
		current->vel = old->vel;
		current->acc = old->acc;

		current->controlEnabled = old->controlEnabled;
		current->gravity = old->gravity;
		current->movement = old->movement;
		if ( old->sprite->IsMirrored() ) current->sprite->SetMirrored();
		else current->sprite->ClearMirrored();
		if (!immediate) 
		{
			current->SetAnimation("morph_in", false);
			current->activity = oatMorphing;
		}
		if (current->aabb.H != old->aabb.H)
			current->aabb.p.y = old->aabb.Bottom() - current->aabb.H;

		old->IsOnPlane() ? current->SetOnPlane() : current->ClearOnPlane();
		if (old->suspected_plane && old->suspected_plane->getParent())
		{
			ObjPhysic* sp = static_cast<ObjPhysic*>(old->suspected_plane->getParent());
			sp->PutOn(current);
			current->suspected_plane = sp->foundation_for;
		}
		else
		{
			current->suspected_plane = NULL;
		}
		current->drop_from = old->drop_from;

		old->IsBulletCollidable() ?
			current->SetBulletCollidable() :
			current->ClearBulletCollidable();

		old->ClearBulletCollidable();

		current->movementDirectionX = old->movementDirectionX;
		/*old->sprite->IsMirrored() ?
			_PLAYER->sprite->SetMirrored() :
			_PLAYER->sprite->ClearMirrored();*/

		current->sprite->SetVisible();
		old->sprite->ClearVisible();

		current->ClearSleep();
		old->SetSleep();

		if (GetCameraAttachedObject() == old)
			CameraAttachToObject(current);

		if (onChangePlayerProcessor >= 0)
		{
			lua_pushinteger(lua, old->id);
			lua_pushinteger(lua, current->id);
			if (SCRIPT::ExecChunkFromReg(onChangePlayerProcessor, 2) < 0)
			{
				// � ������� ��������� �����-�� ������. ����� ����������� ����������� � ��������� ���.
				SCRIPT::RemoveFromRegistry(onChangePlayerProcessor);
			}
		}
	}
}

// ����������� ����� �������
void Player::BlockChange()
{
	change_blocked = true;
}

// �������������� ����� �������
void Player::UnblockChange()
{
	change_blocked = false;
}

extern std::vector<Player*> players;
// ���������� �� ����������� ������� ������
void Player::OnDying(ObjPlayer* plr)
{
	if (game_state == game::GAME_DESTROYING)
		return;			// ��� ����������� ���� ��� �� ���� ����������� �� ����������� �������
	
	if ( plr->IsSleep() )
	{
		for( size_t i = 0; i < character_list.size(); i++ )
		{
			if (character_list[i].first == plr)
				character_list[i].first = NULL;
		}
		return;			// ���� ��������� ���������� �������� - �� ���� ��� � ������. ��� �� ������.
	}
	
	if ( plr == current )
		current = NULL;
	
	int num = current_num + 1;
	
	character_list[current_num].first = NULL;
	
	int player_num = -1;
	for ( size_t i = 0; i < players.size(); i++ )
	{
		if ( players[i] == this )
		{
			player_num = i + 1;
			break;
		}
	}
	
	if (onPlayerDeathProcessor >= 0)
	{
		PushObject(lua, plr);				// �������� � ������� ������� � ����������� �� ������� ������������� ������
		lua_pushinteger(lua, player_num);	// ����� ������������� ������
		lua_pushinteger(lua, num);			// ����� ������������� ��������� ������
		playerControl = this;
		
		if (SCRIPT::ExecChunkFromReg(onPlayerDeathProcessor, 3) < 0)
		{
			// � ������� ��������� �����-�� ������. ����� ������������ ����������� � ��������� ���.
			SCRIPT::ReleaseProc(&onPlayerDeathProcessor);
		}
	}
}
