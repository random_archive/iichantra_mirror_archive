#ifndef __PHYSCOLLISIONSOLVER_H_
#define __PHYSCOLLISIONSOLVER_H_

class GameObject;
class ObjPhysic;
class ObjDynamic;

class CAABB;
class CPolygon;
class CRay;
class Vector2;

void InitArraySAP();
void PrepareRemoveArraySAP();
void RemoveArraySAP();

void ProcessCollisions();

void SolveCollision(ObjPhysic* obj1, ObjPhysic* obj2);
void SolveResizeCollision(ObjPhysic* objD, ObjPhysic* objS);
bool SolveVSSlope(const CAABB& aabb, ObjPhysic* slope, Vector2 shift );
float GetSlopeTop(ObjPhysic* slope, float x );
float GetSlopeX(ObjPhysic* slope, float y );

//ASAP_AABB GetASAP_AABB( CAABB& aabb );
UINT AddSAPObject(ObjPhysic* obj, UINT id, CAABB& aabb);
void RemoveSAPObject(UINT sap_handle);
void RemoveTempSAPObject(UINT sap_handle);
void UpdateSAPState();
void UpdateSAPObject(UINT sap_handle, CAABB& aabb);
void UpdateObject(UINT sap_handle, CAABB& aabb);
void DumpPairs();
void DumpTempPairs();

//bool PolygonOver( CPolygon* p1, Vector2 pos1, CPolygon* p2, Vector2 pos2 );
bool RestsOn( ObjDynamic* objD, ObjPhysic* objS );
ObjPhysic* GetShadowPlane( ObjDynamic* od );
bool GetIntersection(CRay const& ray, CPolygon& poly, Vector2& polypos, Vector2& res, Vector2& norm);
#endif // __PHYSCOLLISIONSOLVER_H_
