#ifndef __EDITOR_H_
#define __EDITOR_H_

#ifdef MAP_EDITOR

namespace editor
{
	enum EditorStates
	{
		EDITOR_OFF,
		EDITOR_MAP_EDITOR,
		EDITOR_PROTO_EDITOR,
		EDITOR_TEXTURE_EDITOR,
		EDITOR_SCRIPT_EDITOR,
		EDITOR_CONTROL_EDITOR,
	};
	
	void SetEditorState(EditorStates state);
}

void ProcessEditor();
void EditorGetObjects(CAABB area);
void AddObjectInArea(UINT id);
void EditorRegAreaSelectProc(lua_State* L);
int EditorCopyObject(lua_State* L);
int EditorDumpMap(lua_State* L);
int EditorResize(lua_State* L);

#endif // MAP_EDITOR

#endif //__EDITOR_H_