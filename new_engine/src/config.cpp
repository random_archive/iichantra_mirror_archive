#include "StdAfx.h"
#include <SDL/SDL_events.h>

#include "config.h"
#include "script/script.h"

#include "scene.h"

extern lua_State* lua;
extern config cfg;
extern char path_config[MAX_PATH];
extern char path_app[MAX_PATH];

// ��� �������� ����� � ������� � �����. ������� ��� ��, ��� � � ConfigActionKeys.
const char* configKeysNames[] = 
{	
	"left", "right", "down", "up", "jump", "sit", "fire", "alt_fire",
	"change_weapon", "change_player", 
	"gui_nav_accept", "gui_nav_decline",
	"gui_nav_prev", "gui_nav_next",
	"gui_nav_menu",
	"gui_nav_screenshot",
	"player_use", "weapon_slot1", "weapon_slot2", "weapon_slot3", "weapon_slot4"
};

const char* GetConfigKeyName(ConfigActionKeys key)
{
	return configKeysNames[key];
}


// ��� �������� ������ ��� ����������� ��������� ����������. ������� ��� ��, ��� � � ConfigActionKeys.
const char* standardKeys[KEY_SETS_NUMBER][cakLastKey] = 
{
	{
		"left", "right", "down", "up",
		"x", "z", "c", "f",
		"lshift", "a", 
		"enter", "x",
		"up", "down",
		"esc",
		"f11",
		"d", "1", "2", "3", "4"
	},
	{
		"j1_hat_left", "j1_hat_right", "j1_hat_down", "j1_hat_up",
		"j1_button3", "j1_button6", "j1_button1", "j1_button2",
		"j1_button2", "",
		"j1_button1", "j1_button3",
		"j1_hat_up", "j1_hat_down",
		"j1_button10",
		"f11",
		"j1_button4", "j1_axis1-", "j1_axis0-", "j1_axis0+", "j1_axis1+"
	},
	{
		"", "", "", "",
		"", "", "", "",
		"", "",
		"", "",
		"", "",
		"",
		"",
		"", "", "", "", ""
	},
	{
		"", "", "", "",
		"", "", "", "",
		"", "",
		"", "",
		"", "",
		"",
		"",
		"", "", "", "", ""
	}
};


void CheckConfigKeySanity( size_t s, ConfigActionKeys key )
{
	if (cfg.cfg_keys[s][key] == 0)
	{
		UINT val = GetNumByVKey(standardKeys[s][key]);
		cfg.cfg_keys[s][key] = val;
		
		STACK_CHECK_INIT(lua);
		
		lua_getglobal(lua, "CONFIG");		// ����: config
		if (lua_istable(lua, -1))
		{
			lua_getfield(lua, -1, "key_conf");		// ����: config key_conf
			if (lua_istable(lua, -1))
			{
				lua_rawgeti(lua, -1, static_cast<int>(s+1));			// ����: config key_conf key_conf[s]
				if (lua_istable(lua, -1))
				{
					lua_pushinteger(lua, val);
					lua_setfield(lua, -2, configKeysNames[key]);
				}
				lua_pop(lua, 1);
			}
			lua_pop(lua, 1);
		}
		lua_pop(lua, 1);
		
		STACK_CHECK(lua);
	}
}

void LoadKeysConfig() 
{
	STACK_CHECK_INIT(lua);
	lua_getfield(lua, -1, "key_conf");		// ����: config
	if (lua_istable(lua, -1))
	{
		size_t i = 0;
		lua_pushnil(lua);
		while (lua_next(lua, -2) != 0 && i < KEY_SETS_NUMBER) 
		{
			luaL_argcheck(lua, lua_istable(lua, -1), -1, "Tbale 'key_conf' must contain tables");
			
			for (size_t j = 0; j < cakLastKey; j++)
			{
				SCRIPT::GetFieldByName(lua, configKeysNames[j], cfg.cfg_keys[i][j] );
			}
			
			i++;
			
			lua_pop(lua, 1);
		}
	}
	lua_pop(lua, 1);						// ����: config
	
	STACK_CHECK(lua);
}

bool LoadDefaultConfig()
{
	ChangeDir(path_config);
	if(luaL_loadfile(lua, DEFAULT_CONFIG_NAME))
	{
		const char* err = lua_tostring(lua, -1);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "%s", err);
		ChangeDir(path_app);
		return false;
	}
	ChangeDir(path_app);
	if (lua_pcall(lua, 0, LUA_MULTRET, 0))
	{
		const char* err = lua_tostring(lua, -1);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "In ExecChunk(UINT args): %s", err);
		return false;
	}
	return true;
}

bool LoadConfig()
{
	bool res = false;
	
	lua_getglobal(lua, "CONFIG");		// ����: config
	
	if (lua_istable(lua, -1))
	{
		bool videomode_changed = false;
		bool backcolor_changed = false;
		float tmpf = 0;
		UINT tmpu = 0;
		BOOL tmpb = 0;
		
		tmpu = cfg.scr_width;       SCRIPT::GetFieldByName(lua, "scr_width", cfg.scr_width );               videomode_changed = ( (tmpu != cfg.scr_width) || videomode_changed);
		tmpu = cfg.scr_height;      SCRIPT::GetFieldByName(lua, "scr_height", cfg.scr_height );             videomode_changed = ( (tmpu != cfg.scr_height) || videomode_changed);
		tmpu = cfg.scr_bpp;         SCRIPT::GetFieldByName(lua, "bpp", cfg.scr_bpp );                       videomode_changed = ( (tmpu != cfg.scr_bpp) || videomode_changed);
		tmpf = cfg.aspect_ratio;    SCRIPT::GetFieldByName(lua, "aspect_ratio", cfg.aspect_ratio );   videomode_changed = ( (tmpf != cfg.aspect_ratio) || videomode_changed);
		
		tmpf = cfg.near_z;	SCRIPT::GetFieldByName(lua, "near_z", cfg.near_z);	videomode_changed = ( (tmpf != cfg.near_z) || videomode_changed);
		tmpf = cfg.far_z;	SCRIPT::GetFieldByName(lua, "far_z", cfg.far_z);		videomode_changed = ( (tmpf != cfg.far_z) || videomode_changed);
		
		tmpu = cfg.window_width;    SCRIPT::GetFieldByName(lua, "window_width", cfg.window_width );     videomode_changed = ( (tmpu != cfg.window_width) || videomode_changed);
		tmpu = cfg.window_height;   SCRIPT::GetFieldByName(lua, "window_height", cfg.window_height );   videomode_changed = ( (tmpu != cfg.window_height) || videomode_changed);
		
		tmpu = cfg.full_width;      SCRIPT::GetFieldByName(lua, "full_width", cfg.full_width );     videomode_changed = ( (tmpu != cfg.full_width) || videomode_changed);
		tmpu = cfg.full_height;     SCRIPT::GetFieldByName(lua, "full_height", cfg.full_height );   videomode_changed = ( (tmpu != cfg.full_height) || videomode_changed);
		
		tmpb = cfg.fullscreen;  SCRIPT::GetFieldByName(lua, "fullscreen", cfg.fullscreen );  videomode_changed = ( (tmpb != cfg.fullscreen) || videomode_changed);
		tmpb = cfg.vert_sync;   SCRIPT::GetFieldByName(lua, "vert_sync", cfg.vert_sync );    videomode_changed = ( (tmpb != cfg.vert_sync) || videomode_changed);
		
		SCRIPT::GetFieldByName(lua, "debug", cfg.debug );
		SCRIPT::GetFieldByName(lua, "show_fps", cfg.show_fps );
		
		tmpf = cfg.backcolor_r; SCRIPT::GetFieldByName(lua, "backcolor_r", cfg.backcolor_r); backcolor_changed = ( (tmpf != cfg.backcolor_r) || backcolor_changed);
		tmpf = cfg.backcolor_g; SCRIPT::GetFieldByName(lua, "backcolor_g", cfg.backcolor_g); backcolor_changed = ( (tmpf != cfg.backcolor_g) || backcolor_changed);
		tmpf = cfg.backcolor_b; SCRIPT::GetFieldByName(lua, "backcolor_b", cfg.backcolor_b); backcolor_changed = ( (tmpf != cfg.backcolor_b) || backcolor_changed);
		
		SCRIPT::GetFieldByName(lua, "color_r", cfg.color_r );
		SCRIPT::GetFieldByName(lua, "color_g", cfg.color_g );
		SCRIPT::GetFieldByName(lua, "color_b", cfg.color_b );
		DELETEARRAY(cfg.nick); 
		SCRIPT::GetFieldByName(lua, "nick", cfg.nick );
		
		SCRIPT::GetFieldByName(lua, "volume", cfg.volume);
		SCRIPT::GetFieldByName(lua, "volume_music", cfg.volume_music);
		SCRIPT::GetFieldByName(lua, "volume_sound", cfg.volume_sound);
		
#ifdef GAMETICK_FROM_CONFIG
		SCRIPT::GetFieldByName(lua, "gametick", cfg.gametick );
#endif // GAMETICK_FROM_CONFIG
		
		LoadKeysConfig();
		
		SCRIPT::GetFieldByName(lua, "gui_nav_mode", cfg.gui_nav_mode );
		SCRIPT::GetFieldByName(lua, "gui_nav_cycled", cfg.gui_nav_cycled );
		
		SCRIPT::GetFieldByName(lua, "shadows", cfg.shadows );
		
		cfg.joystick_sensivity = 16383;
		SCRIPT::GetFieldByName(lua, "joystick_sensitivity", cfg.joystick_sensivity );
		
		SCRIPT::GetFieldByName(lua, "log_level", cfg.log_level );
		
		DELETEARRAY(cfg.language);
		SCRIPT::GetFieldByName(lua, "language", cfg.language );
		
		SCRIPT::GetFieldByName(lua, "weather", cfg.weather );
		
		//Checking for config keys sanity
		CheckConfigKeySanity(0, cakGuiNavAccept);
		CheckConfigKeySanity(0, cakGuiNavDecline);
		CheckConfigKeySanity(0, cakGuiNavNext);
		CheckConfigKeySanity(0, cakGuiNavPrev);
		
		scene::ApplyConfig(videomode_changed, backcolor_changed);
		res = true;
		
	}
	else
		luaL_error(lua, "Table CONFIG is not exist");
	
	lua_pop(lua, 1); // ����:
	
	return res;
}

void PrintControls(FILE* f)
{
	fputs("\tkey_conf = {", f);
	
	char ch[MAX_VKEY_NAME_LEN] = "";
	
	for (size_t i = 0; i < KEY_SETS_NUMBER; i++)
	{
		if (i > 0)
			fputs(",", f);
		fputs("\n\t\t{\n", f);
		
		for (size_t j = 0; j < cakLastKey; j++)
		{
			fprintf(f, "\t\t\t%s = ", configKeysNames[j]);
			if (GetVKeyByNum(cfg.cfg_keys[i][j], ch)) 
				fprintf(f, "keys[\"%s\"];\n", ch); 
			else 
				fprintf(f, "%s;\n", ch);
		}
		fprintf(f, "\t\t}");
	}
	fputs("\n\t};\n", f);
}

bool SaveConfig( const char * filename )
{
	bool res = false;
	char path_to_cfg[MAX_PATH];
	char config_file[MAX_PATH];
	sprintf(path_to_cfg, "%s%s", path_config, (NULL == filename ? DEFAULT_CONFIG_NAME : filename));
	sprintf(config_file, "%s%s", DEFAULT_CONFIG_PATH, (NULL == filename ? DEFAULT_CONFIG_NAME : filename) );
	
	FILE* f;
	f = fopen(path_to_cfg, "w");
	
	if(f)
	{
		fprintf(f, "CONFIG = \n");
		fprintf(f, "{\n");
		fprintf(f, "\twindow_width = %d;\n", cfg.window_width);
		fprintf(f, "\twindow_height = %d;\n\n", cfg.window_height);
		fprintf(f, "\tfull_width = %d;\n", cfg.full_width);
		fprintf(f, "\tfull_height = %d;\n\n", cfg.full_height);
		fprintf(f, "\tscr_width = %d;\n", cfg.scr_width);
		fprintf(f, "\tscr_height = %d;\n", cfg.scr_height);
		fprintf(f, "\taspect_ratio = %f;\n", cfg.aspect_ratio);
		fprintf(f, "\tnear_z = %f;\n", cfg.near_z);
		fprintf(f, "\tfar_z = %f;\n", cfg.far_z);
		fprintf(f, "\tbpp = %d;\n", cfg.scr_bpp);
		fprintf(f, "\tfullscreen = %d;\n", cfg.fullscreen);
		fprintf(f, "\tvert_sync = %d;\n", cfg.vert_sync);
		fprintf(f, "\tdebug = %d;\n", cfg.debug);
		fprintf(f, "\tshow_fps = %d;\n\n", cfg.show_fps);
		fprintf(f, "\tlog_level = constants.%s;\n", LogLevelGetName(cfg.log_level));
		fprintf(f, "\t\n");
#ifdef GAMETICK_FROM_CONFIG
		fprintf(f, "\tgametick = %d;\n\n", cfg.gametick);
		fprintf(f, "\t\n");
#endif // GAMETICK_FROM_CONFIG
		fprintf(f, "\tbackcolor_r = %f;\n", cfg.backcolor_r);
		fprintf(f, "\tbackcolor_g = %f;\n", cfg.backcolor_g);
		fprintf(f, "\tbackcolor_b = %f;\n", cfg.backcolor_b);
		fprintf(f, "\t\n");
		fprintf(f, "\tnick = \"%s\";\n", cfg.nick);
		fprintf(f, "\tcolor_r = %f;\n", cfg.color_r);
		fprintf(f, "\tcolor_g = %f;\n", cfg.color_g);
		fprintf(f, "\tcolor_b = %f;\n", cfg.color_b);
		fprintf(f, "\t\n");
		fprintf(f, "\tvolume = %f;\n", cfg.volume);
		fprintf(f, "\tvolume_music = %f;\n", cfg.volume_music);
		fprintf(f, "\tvolume_sound = %f;\n", cfg.volume_sound);
		fprintf(f, "\t\n");
		fprintf(f, "\t-- controls\n\n");
		PrintControls(f);
		
		fprintf(f, "\tjoystick_sensitivity = %i;\n", cfg.joystick_sensivity);
		
		fprintf(f, "\t\n");
		fprintf(f, "\t-- gui settings\n\n");
		fprintf(f, "\tgui_nav_mode = %d;\n", cfg.gui_nav_mode);
		fprintf(f, "\tgui_nav_cycled = %d;\n", cfg.gui_nav_cycled);
		
		fprintf(f, "\t\n");
		fprintf(f, "\t-- game\n\n");
		fprintf(f, "\tlanguage = \"%s\";\n", cfg.language);
		fprintf(f, "\tshadows = %i;\n", cfg.shadows);
		fprintf(f, "\tweather = %i;\n", cfg.weather);
		
		fprintf(f, "}");
		fprintf(f, "\n");
		fprintf(f, "LoadConfig();\n");
		fclose(f);
		Log(DEFAULT_LOG_NAME, logLevelInfo, "%s succesfully saved", path_to_cfg);
		res = true;
		
		//SCRIPT::ExecFile(config_file); Why?
	}
	else
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Failed to save %s", path_to_cfg);
	
	return res;
}

bool RecreateConfig()
{
	Log(DEFAULT_LOG_NAME, logLevelWarning, "Recreating config...");
	
	cfg.window_width = 640;
	cfg.window_height = 480;
	cfg.full_width = 0;
	cfg.full_height = 0;
	cfg.scr_width = 640;
	cfg.scr_height = 480;
	cfg.aspect_ratio = 4.0f/3.0f;
	cfg.near_z = -1.1f;
	cfg.far_z = 1.1f;
	cfg.scr_bpp = 32;
	cfg.fullscreen = 0;
	cfg.vert_sync = 1;
	cfg.debug = 0;
	cfg.show_fps = 0;
#ifdef GAMETICK_FROM_CONFIG
	cfg.gametick = 10;
#endif // GAMETICK_FROM_CONFIG
	cfg.backcolor_r = 0.0f;
	cfg.backcolor_g = 0.0f;
	cfg.backcolor_b = 0.0f;
	cfg.volume = 1.0f;
	cfg.volume_music = 1.0f;
	cfg.volume_sound = 1.0f;
	
	for (size_t s = 0; s < KEY_SETS_NUMBER; s++)
		for (size_t i = 0; i < cakLastKey; i++)
			cfg.cfg_keys[s][i] = GetNumByVKey(standardKeys[s][i]);
	
	cfg.gui_nav_mode = 1;
	cfg.gui_nav_cycled = 1;
	cfg.language = StrDupl("russian");
	cfg.joystick_sensivity = 16383;
	cfg.log_level = logLevelError;
	cfg.shadows = 1;
	cfg.weather = 1;
	
	cfg.nick = StrDupl("Anonymous");
	cfg.color_r = 1.0f;
	cfg.color_g = 1.0f;
	cfg.color_b = 1.0f;
	
	return SaveConfig();
}

//////////////////////////////////////////////////////////////////////////

map<UINT, string> num_VKey_array;
map<string, UINT> VKey_num_array;

#define MAPVK(vk, name) assert((strlen(name) + 1) < MAX_VKEY_NAME_LEN);	str = string(name); num_VKey_array[vk] = str; VKey_num_array[str] = vk; 

void InitVKeysArrays()
{
	string str;
	MAPVK(SDLK_BACKSPACE, "backspace");
	MAPVK(SDLK_TAB, "tab");
	MAPVK(SDLK_RETURN, "enter");
	MAPVK(SDLK_LSHIFT, "lshift");
	MAPVK(SDLK_LCTRL, "lctrl");
	MAPVK(SDLK_LALT, "lalt");
	MAPVK(SDLK_RSHIFT, "rshift");
	MAPVK(SDLK_RCTRL, "rctrl");
	MAPVK(SDLK_RALT, "ralt");
	MAPVK(SDLK_LSUPER, "lwin");
	MAPVK(SDLK_RSUPER, "rwin");
	MAPVK(SDLK_MENU, "menu");
	MAPVK(SDLK_PAUSE, "pause");
	MAPVK(SDLK_CAPSLOCK, "capslock");
	MAPVK(SDLK_SCROLLOCK, "scrollock");
	MAPVK(SDLK_NUMLOCK, "numlock");
	MAPVK(SDLK_ESCAPE, "esc");
	MAPVK(SDLK_SPACE, "space");
	MAPVK(SDLK_PAGEUP, "pgup");
	MAPVK(SDLK_PAGEDOWN, "pgdown");
	MAPVK(SDLK_END, "end");
	MAPVK(SDLK_HOME, "home");
	
	MAPVK(SDLK_LEFT, "left");
	MAPVK(SDLK_UP, "up");
	MAPVK(SDLK_RIGHT, "right");
	MAPVK(SDLK_DOWN, "down");
	
	MAPVK(SDLK_PRINT, "printscreen");
	MAPVK(SDLK_INSERT, "insert");
	MAPVK(SDLK_DELETE, "delete");
	
	MAPVK(SDLK_0, "0");
	MAPVK(SDLK_1, "1");
	MAPVK(SDLK_2, "2");
	MAPVK(SDLK_3, "3");
	MAPVK(SDLK_4, "4");
	MAPVK(SDLK_5, "5");
	MAPVK(SDLK_6, "6");
	MAPVK(SDLK_7, "7");
	MAPVK(SDLK_8, "8");
	MAPVK(SDLK_9, "9");
	
	MAPVK(SDLK_a, "a");
	MAPVK(SDLK_b, "b");
	MAPVK(SDLK_c, "c");
	MAPVK(SDLK_d, "d");
	MAPVK(SDLK_e, "e");
	MAPVK(SDLK_f, "f");
	MAPVK(SDLK_g, "g");
	MAPVK(SDLK_h, "h");
	MAPVK(SDLK_i, "i");
	MAPVK(SDLK_j, "j");
	MAPVK(SDLK_k, "k");
	MAPVK(SDLK_l, "l");
	MAPVK(SDLK_m, "m");
	MAPVK(SDLK_n, "n");
	MAPVK(SDLK_o, "o");
	MAPVK(SDLK_p, "p");
	MAPVK(SDLK_q, "q");
	MAPVK(SDLK_r, "r");
	MAPVK(SDLK_s, "s");
	MAPVK(SDLK_t, "t");
	MAPVK(SDLK_u, "u");
	MAPVK(SDLK_v, "v");
	MAPVK(SDLK_w, "w");
	MAPVK(SDLK_x, "x");
	MAPVK(SDLK_y, "y");
	MAPVK(SDLK_z, "z");
	
	MAPVK(SDLK_KP0, "num0");
	MAPVK(SDLK_KP1, "num1");
	MAPVK(SDLK_KP2, "num2");
	MAPVK(SDLK_KP3, "num3");
	MAPVK(SDLK_KP4, "num4");
	MAPVK(SDLK_KP5, "num5");
	MAPVK(SDLK_KP6, "num6");
	MAPVK(SDLK_KP7, "num7");
	MAPVK(SDLK_KP8, "num8");
	MAPVK(SDLK_KP9, "num9");
	MAPVK(SDLK_KP_MULTIPLY, "multiply");
	MAPVK(SDLK_KP_PLUS, "add");
	MAPVK(SDLK_KP_ENTER, "numenter");
	MAPVK(SDLK_KP_MINUS, "substract");
	MAPVK(SDLK_KP_PERIOD, "decimal");
	MAPVK(SDLK_KP_DIVIDE, "divide");
	
	MAPVK(SDLK_F1, "f1");
	MAPVK(SDLK_F2, "f2");
	MAPVK(SDLK_F3, "f3");
	MAPVK(SDLK_F4, "f4");
	MAPVK(SDLK_F5, "f5");
	MAPVK(SDLK_F6, "f6");
	MAPVK(SDLK_F7, "f7");
	MAPVK(SDLK_F8, "f8");
	MAPVK(SDLK_F9, "f9");
	MAPVK(SDLK_F10, "f10");
	MAPVK(SDLK_F11, "f11");
	MAPVK(SDLK_F12, "f12");
	
	MAPVK(SDLK_SEMICOLON, "semicolon");
	MAPVK(SDLK_PLUS, "plus");
	MAPVK(SDLK_COMMA, "comma");
	MAPVK(SDLK_MINUS, "minus");
	MAPVK(SDLK_PERIOD, "period");
	MAPVK(SDLK_SLASH, "slash");
	MAPVK(SDLK_EQUALS, "equals");
	MAPVK(SDLK_BACKQUOTE, "tilde");
	MAPVK(SDLK_LEFTBRACKET, "lbracket");
	MAPVK(SDLK_BACKSLASH, "backslash");
	MAPVK(SDLK_RIGHTBRACKET, "rbracket");
	MAPVK(SDLK_QUOTE, "quote");
	
	size_t add = 1;
	char buffer[MAX_VKEY_NAME_LEN];
	for ( int i = 0; i < MAX_JOYSTICKS; i++ )
	{
		for ( int j = 0; j < MAX_JOYSTICK_AXES; j++ )
		{
			sprintf(buffer, "j%i_axis%i+", i+1, j );
			MAPVK(SDLK_LAST+add, buffer );
			add++;
			sprintf(buffer, "j%i_axis%i-", i+1, j );
			MAPVK(SDLK_LAST+add, buffer );
			add++;
		}
		for ( int j = 0; j < MAX_JOYSTICK_BUTTONS; j++ )
		{
			sprintf(buffer, "j%i_button%i", i+1, j+1 );
			MAPVK(SDLK_LAST+add, buffer );
			add++;
		}
		sprintf(buffer, "j%i_hat_up", i+1 );
		MAPVK(SDLK_LAST+add, buffer);
		add++;
		sprintf(buffer, "j%i_hat_down", i+1 );
		MAPVK(SDLK_LAST+add, buffer);
		add++;
		sprintf(buffer, "j%i_hat_left", i+1 );
		MAPVK(SDLK_LAST+add, buffer);
		add++;
		sprintf(buffer, "j%i_hat_right", i+1 );
		MAPVK(SDLK_LAST+add, buffer);
		add++;
	}
}

void ClearVKeysArrays()
{
	num_VKey_array.clear();
	VKey_num_array.clear();;
}


bool GetVKeyByNum(UINT num, char* vk)
{
	map<UINT, string>::iterator it = num_VKey_array.find(num);
	if (it != num_VKey_array.end())
	{
		strcpy(vk, it->second.c_str());
		return true;
	}
	else
	{
		sprintf(vk, "%i", num);
		return false;
	}
}


UINT GetNumByVKey(string s)
{
	map<string, UINT>::iterator it = VKey_num_array.find(s);
	if (it != VKey_num_array.end())
	{
		return it->second;
	}
	
	return 0;
}

