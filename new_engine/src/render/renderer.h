#ifndef __RENDERER_H_
#define __RENDERER_H_



#include "render_types.h"
#include "texture.h"
#include "../game/phys/phys_misc.h"
#include "../misc.h"
#include "../game/phys/2de_RGBA.h"
#include "../game/phys/2de_Geometry.h"

struct render_data
{
	GLuint			texture_id;			// ������������� �������� OGL
	vertex3f_t*		vertices;			// ������ ������
	coord2f_t*		coords;				// ������ ��������� �������
	RGBAf*			colors;				// ������ ������
	size_t			vert_allocated_size;		// ���������� ������ ��� ������ ������
	size_t			coord_allocated_size;		// ���������� ������ ��� ������ �������� ������
	size_t			colors_allocated_size;		// ���������� ������ ��� ������ ������
	size_t			count;				// ���������� ���������
};

void r_ZeroRenderData(void);				// �������� ���������
void r_ClearRenderData(void);				// �������� ��������� (���������� ������)
void r_RenderAll(void);						// ���������� ���


void RenderFrameSimple(float x, float y, float z, float x1, float y1, float x2, float y2, const Texture* tex, BOOL mirrorX, const RGBAf& color, float scale = 1.0f, BlendingMode bm = bmLast, RenderArrayTypes rat = ratSprites);
void RenderFrameStandard(float x, float y, float z, const coord2f_t* frame_size, const coord2f_t* tex_coords, const Texture* tex, const RGBAf& color, BlendingMode bm = bmLast);
void RenderFrameRotated(float x, float y, float z, const coord2f_t* frame_size, const coord2f_t* tex_coords, const Texture* tex, const RGBAf& color, float angle, Vector2 stationary_point, BlendingMode bm = bmLast);
void RenderFrameArray(Vector2* coord, float* z, coord2f_t* frame_size, coord2f_t** tex_coords, const Texture* tex, RGBAf* color, size_t size, BlendingMode bm = bmLast);
void RenderFramePart(float x, float y, float z, const coord2f_t* full_frame_size, const coord2f_t* part_frame_size, const coord2f_t* tex_coords, bool mirrorX, const Texture* tex, const RGBAf& color, float angle = 0.0f, Vector2 stationary_point = Vector2(), BlendingMode bm = bmLast);
void RenderFramePartLT(float x, float y, float z, const coord2f_t* full_frame_size, const coord2f_t* part_frame_size, const coord2f_t* tex_coords, bool mirrorX, const Texture* tex, const RGBAf& color, float angle = 0.0f, BlendingMode bm = bmLast);
void RenderFrameCyclic(const Vector2& coord, float z, const Vector2& edge, const Vector2& bs, const FrameInfo* frame, const Texture* tex, const RGBAf& color, bool mirrorX, bool repeat_x, bool repeat_y, BlendingMode bm = bmLast);
void RenderFrame(float x, float y, float z, const CAABB& area, const FrameInfo* frame, const Texture* tex, const RGBAf& color, bool mirrorX, RenderSpriteMethod method, float angle, BlendingMode bm = bmLast);

void RenderBox(const vertex3f_t* source, float z, const RGBAf& color, BlendingMode bm = bmLast);
void RenderBox(float x, float y, float z, float w, float h, const RGBAf& color, BlendingMode bm = bmLast);
void RenderLine(float x1, float y1, float x2, float y2, float z, const RGBAf& color, BlendingMode bm = bmLast);
void RenderEllipse(float x, float y, float w, float h, float z, const RGBAf& color, UINT id, UINT segments, BlendingMode bm = bmLast);
void RenderPolygon(CPolygon* polygon, Vector2 pos, float z, const RGBAf& color, UINT id, BlendingMode bm = bmLast);
void RenderTriangle( Vector2 v1, Vector2 v2, Vector2 v3, float z, const RGBAf& color, BlendingMode bm = bmLast);

#endif // __RENDERER_H_
