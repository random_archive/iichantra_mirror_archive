#ifndef __TEXTURE_H_
#define __TEXTURE_H_

#include "render_types.h"

#include "../resource.h"

#include <SDL/SDL_opengl.h>

struct FrameInfo{
	coord2f_t size;
	coord2f_t offset;
	coord2f_t* coord;
	coord2f_t* coordMir;

	FrameInfo()
	{ 
		//size = new coord2f_t;
		//offset = new coord2f_t;
		coord = new coord2f_t[4];
		coordMir = new coord2f_t[4];
	}

	~FrameInfo()
	{
		//DELETESINGLE(size);
		//DELETESINGLE(offset);
		DELETEARRAY(coord);
		DELETEARRAY(coordMir);
	}
};

class Texture;
struct lua_State;

class TexFrames : public Resource
{
public:
	Texture* texture;
	UINT framesCount;
	FrameInfo* frame;
	UINT overlayCount;
	FrameInfo* overlay;
	UINT dist; //Fonts stuff

	virtual bool Load();
	bool LoadFrames();
	bool LoadFramesFromTable(lua_State* L);

	void MakeDummyFrame(float width, float height);

	int DumpTable(lua_State* L);

	TexFrames(string name, unsigned char* buffer, size_t buffer_size) : Resource(name, buffer, buffer_size), dist(0)
	{
		_buffer = NULL;
		_buffer_size = 0;
		texture = NULL;
		framesCount = 0;
		overlayCount = 0;
		frame = NULL;
		overlay = NULL;
	}

	~TexFrames()
	{
		if( _buffer ) DELETEARRAY( _buffer );
		DELETEARRAY(frame);
		DELETEARRAY(overlay);
	}
	
private:
	char* _buffer;
	size_t _buffer_size;
};

class Texture : public Resource
{
public:
	unsigned char* _buffer;
	size_t _buffer_size;
	GLuint tex;
	UINT width;
	UINT height;

	TexFrames* frames;
	
	bool isTransparent;

	virtual bool Load();
	bool LoadWithTexFrames(TexFrames* frames);
	
	virtual bool Recover();

	Texture(string name, unsigned char* buffer, size_t buffer_size) : 
		Resource(name, buffer, buffer_size),
		_buffer(NULL),
		_buffer_size(0),
		tex(0),
		width(0),
		height(0),
		frames(NULL),
		isTransparent(false)
	{

	}

	~Texture()
	{
		glDeleteTextures(1, &tex);
		if ( frames ) 
			Resource::Release( frames );
		DELETEARRAY(_buffer);
	}

private:
	bool LoadImage();
};


#endif
