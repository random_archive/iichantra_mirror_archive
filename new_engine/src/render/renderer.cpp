#include "StdAfx.h"

#include "texture.h"
#include "renderer.h"


#include "../misc.h"

const size_t BLOCK_INIT_SIZE	= 1024;		// bytes
const size_t BLOCK_SIZE			= 512;		// bytes

#ifdef DEBUG_PRINT
extern int DBG_PRINT_RENDERED;
#endif // DEBUG_PRINT

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

struct render_array 
{
	render_data* data;	// ������ ������� ���������
	size_t size;	// ������ �������
};

// ������ � ����������� ��� glBlendFunc ��� ������� �� �������.
const GLenum blendingOptions[bmLast][2] = {
	{ GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA },   // bmNone - ����� ���� ��� ������, ������ ��� ������� ����� ��������
	{ GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA },   // bmSrcA_OneMinusSrcA
	{ GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA },   // bmBright - �������� � ������
	{ GL_SRC_ALPHA, GL_ONE },                   // bmSrcA_One
	{ GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA }    // bmPostEffect - �� ����, bmSrcA_OneMinusSrcA, �� �������� ����� �����.
};

// ������ �������� ������� ��������� ��� ������ ����� ��������� � ������� ���������
render_array render_arrays[ratLast][bmLast];

// ���������� ����� ������� ������� ������� ��������� � ����������� �� ������ ���������. 
// ���� bm != bmLast, ���������� ��������� ����� ���������.
__INLINE render_array& select_array(RenderArrayTypes rat, const RGBAf& color, BlendingMode bm/* = bmLast*/)
{
	if (bm != bmLast)
		return render_arrays[rat][bm];
	else if (0.0f < color.a && color.a < 1.0f)
		return render_arrays[rat][bmSrcA_OneMinusSrcA];
	else
		return render_arrays[rat][bmNone];
}

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
int DBG_BM_COUNT[ratLast][bmLast];
#endif // DEBUG_PRINT_BM_RENDERED_COUNT

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// ����� id �������� ��� ���� � ������ ���������?
render_data* _id_exists(const render_array& arr, GLuint id)
{
	if(arr.size == 0)
		return NULL;

	// TODO: ������������ ��������������� ������ � �������� �����

	for(size_t i = 0; i < arr.size; i++)
	{
		if(arr.data[i].texture_id == id)
			return &arr.data[i];
	}

	return NULL;
}


// ���������� ������ ��-��� �������� � render_data
void _free_render_data(render_data* p_data)
{
	assert(p_data != NULL);

	if(p_data->coords)
		free(p_data->coords);

	if(p_data->vertices)
		free(p_data->vertices);

	if(p_data->colors)
		free(p_data->colors);

	p_data->vert_allocated_size = 0;
	p_data->coord_allocated_size = 0;
	p_data->colors_allocated_size = 0;
	p_data->count = 0;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ���������� ��������� ������ ��� ���������� ������� ��� r_GetMem
template<typename T>
void r_ReallocArray(size_t add_size, size_t count, size_t& allocated_size, T*& arr)
{
	size_t new_size = count*sizeof(T) + add_size;
	// ���� ��� �� ������� ���������� ������ ��� ����������
	if(new_size > allocated_size)
	{
		// ������� ����� ����������� � ���������� �������
		size_t d_size = new_size - allocated_size;

		// ��������� ������������ ������� �� BLOCK_SIZE ����
		// ����������� ����������� ���������� ������ (��� ������� 1)
		size_t blocks_to_add = (d_size / BLOCK_SIZE) + 1;

		// ����� ����� ��������� ������
		allocated_size += (blocks_to_add * BLOCK_SIZE);

		arr = (T*)realloc(arr, allocated_size);
	}
	// TODO: � ��� ���� realloc �� ���������?!

}



// �������� ������ ��� ����� ������ ��������� ��� ��� ��������
// ���������� ��������� �� ������, � ������� ����� ��������� ������ ������� � 
// ������� count. 
render_data* r_GetMem(render_array& arr, GLuint id, size_t vert_size, size_t coord_size, size_t color_size)
{
	// ������������ ������
	render_data* x_data = _id_exists(arr, id);

	if(x_data == NULL)
	{
		// ����� ������
		arr.data = (render_data*)realloc(arr.data, (arr.size + 1)*sizeof(render_data));
		// TODO: � ��� ���� realloc �� ���������?!
		// ������ ���������, ������� ����� ������� �������� �������. ����� ��������� ������ ����� ����� ��������.
		// � ����� ������ ��������� � ������� ������������. � ����� � � ���� ����.
		memset(&((arr.data)[arr.size]), 0, sizeof(render_data));
		x_data = &((arr.data)[arr.size]);
		x_data->texture_id = id;
		arr.size += 1;

		// TODO: �������������. ���������� �� ���������� ��������
	}

	{
		// ��������� ��������� ������
		r_ReallocArray(vert_size, x_data->count, x_data->vert_allocated_size, x_data->vertices);
		r_ReallocArray(color_size, x_data->count, x_data->colors_allocated_size, x_data->colors);
		if (id)
			r_ReallocArray(coord_size, x_data->count, x_data->coord_allocated_size, x_data->coords);
	}

	return x_data;
}


// �������� ���������, �� ���������� ������
void r_ZeroRenderData(void)
{
	for (size_t rat = ratFirst; rat < ratLast; ++rat)
	{
		for (size_t bm = bmFirst; bm < bmLast; ++bm)
		{
			render_array &arr = render_arrays[rat][bm];
			for (size_t i = 0; i < arr.size; i++)
			{
				arr.data[i].count = 0;
			}
		}
	}
}

// �������� ��������� (���������� ������)
void r_ClearRenderData(void)
{
	for (size_t rat = ratFirst; rat < ratLast; ++rat)
	{
		for (size_t bm = bmFirst; bm < bmLast; ++bm)
		{
			render_array &arr = render_arrays[rat][bm];
			for (size_t i = 0; i < arr.size; i++)
			{
				_free_render_data(&arr.data[i]);
			}

			free(arr.data);
			arr.data = NULL;
			arr.size = 0;
		}
	}
}

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
int DBG_TEMP_RENDER_COUNT;
#endif

void RenderSprites(const render_array& arr)
{
	if(!arr.data || !arr.size)
		return;

	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	// TODO: ������������ ������������� ������. ����� ������ ��� �������� ����� ����������� � ����� ������.
	// ��� ����� ����� ���������� �� ������� �� �����.
	const render_data* null_tex = NULL;

	for(size_t i = 0; i < arr.size; i++)
	{
		const render_data& rdata = arr.data[i];

		if (rdata.count == 0)
			continue;

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
		DBG_TEMP_RENDER_COUNT += static_cast<int>(rdata.count / 4);
#endif

		if (rdata.texture_id)
		{
			glBindTexture(GL_TEXTURE_2D, rdata.texture_id);
			glTexCoordPointer(2, GL_FLOAT, 0, rdata.coords);
			glVertexPointer(3, GL_FLOAT, 0, rdata.vertices);
			glColorPointer(4, GL_FLOAT, 0, rdata.colors);
			glDrawArrays(GL_QUADS, 0, static_cast<GLsizei>(rdata.count));
		}
		else
		{
			null_tex = &rdata;
		}
	}

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	if (null_tex)
	{
		// ���� �������� ���, �� ����� ��������� ������ ����������� �������������
		
		glDisable(GL_TEXTURE_2D);
		glVertexPointer(3, GL_FLOAT, 0, null_tex->vertices);
		glColorPointer(4, GL_FLOAT, 0, null_tex->colors);
		glDrawArrays(GL_QUADS, 0, static_cast<GLsizei>(null_tex->count));
	}

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void RenderLines(const render_array& arr)
{
	if(!arr.data || !arr.size)
		return;

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glDisable(GL_TEXTURE_2D);

	for(size_t i = 0; i < arr.size; i++)
	{
		const render_data& rdata = arr.data[i];

		if (rdata.count == 0)
			continue;

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
		DBG_TEMP_RENDER_COUNT += static_cast<int>(rdata.count / 2);
#endif

		glVertexPointer(3, GL_FLOAT, 0, rdata.vertices);
		glColorPointer(4, GL_FLOAT, 0, rdata.colors);
		glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(rdata.count));
	}

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void RenderFilledShapes(const render_array& arr)
{
	if(!arr.data || !arr.size)
		return;

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glDisable(GL_TEXTURE_2D);

	for(size_t i = 0; i < arr.size; i++)
	{
		const render_data& rdata = arr.data[i];

		if (rdata.count == 0)
			continue;

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
		++DBG_TEMP_RENDER_COUNT;
#endif
		//glBindTexture(GL_TEXTURE_2D, rdata.texture_id);





		glVertexPointer(3, GL_FLOAT, 0, rdata.vertices);
		glColorPointer(4, GL_FLOAT, 0, rdata.colors);
		glDrawArrays(GL_TRIANGLE_FAN, 0, static_cast<GLsizei>(rdata.count));
	}

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void RenderFilledTriangles(const render_array& arr)
{
	if(!arr.data || !arr.size)
		return;

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glDisable(GL_TEXTURE_2D);

	for(size_t i = 0; i < arr.size; i++)
	{
		const render_data& rdata = arr.data[i];

		if (rdata.count == 0)
			continue;

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
		DBG_TEMP_RENDER_COUNT += static_cast<int>(rdata.count / 2);
#endif

		glVertexPointer(3, GL_FLOAT, 0, rdata.vertices);
		glColorPointer(4, GL_FLOAT, 0, rdata.colors);
		glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(rdata.count));
	}

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

// ���������� ���
void r_RenderAll(void)
{
	for (size_t bm = bmFirst; bm < bmLast; bm++)
	{
		if (bm == bmNone)
		{
			glDisable(GL_BLEND);
		}
		else
		{
			glEnable(GL_BLEND);
			glBlendFunc(blendingOptions[bm][0], blendingOptions[bm][1]);

			// ��������� Z-buffer ��� ��������� � bmSrcA_One
#ifndef BLENDING_HACK_Z
			if (bm == bmSrcA_One)
			{
				glDisable(GL_DEPTH_TEST);
			}
#endif
			if (bm == bmBright)
			{
				glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD );
			}
		}

		for (size_t rat = ratFirst; rat < ratLast; rat++)
		{
#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
			DBG_TEMP_RENDER_COUNT = 0;
#endif

			const render_array& arr = render_arrays[rat][bm];
			// TODO: �������� ����� ������������ ������ �������, � �� switch? ��� ����� ����� ������������ ����������.
			switch(rat)
			{
			case ratSprites:   RenderSprites(arr);         break;
			case ratLines:     RenderLines(arr);           break;
			case ratFilled:    RenderFilledShapes(arr);    break;
			case ratTriangles: RenderFilledTriangles(arr); break;
			case ratFonts:     RenderSprites(arr);         break;

			default: assert(false);
			}

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT
			DBG_BM_COUNT[rat][bm] = DBG_TEMP_RENDER_COUNT;
#endif
		}
		
		// �������� Z-buffer �������
#ifndef BLENDING_HACK_Z
		if (bm == bmSrcA_One)
		{
			glEnable(GL_DEPTH_TEST);
		}
#endif
		if (bm == bmBright)
		{
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
		}
	}
	
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

// ���������� ��������� �����.
// x, y, z - ������� ���������� ��������������
// x1, y1, x2, y2 - ���������� �� �������� � �������� (��� �� ������ ������ � ������ �������������� ��� �������� 1)
// tex - ��������
// mirrorX - ���� ���������������
// color - ����
// scale - �������
// bm - ����� ���������
// rat - ������ ���������
void RenderFrameSimple(float x, float y, float z, float x1, float y1, float x2, float y2, const Texture* tex, BOOL mirrorX, const RGBAf& color, float scale/* = 1.0f */, BlendingMode bm/* = bmLast*/, RenderArrayTypes rat/* = ratSprites*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	// �������������� - �� 4 ����������.
	size_t count = 4;

	render_data* rd = r_GetMem(select_array(rat, color, bm), tex ? tex->tex : 0,
		count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 4;

	rd->vertices[count].z = rd->vertices[count + 1].z = rd->vertices[count + 2].z = rd->vertices[count + 3].z = z;

	float dx = x2-x1;
	float dy = y2-y1;

	rd->vertices[count+0].x = x;
	rd->vertices[count+0].y = y;

	rd->vertices[count+1].x = x + dx * scale;
	rd->vertices[count+1].y = y;

	rd->vertices[count+2].x = x + dx * scale;
	rd->vertices[count+2].y = y + dy * scale;

	rd->vertices[count+3].x = x;
	rd->vertices[count+3].y = y + dy * scale;

	rd->colors[count+0] = rd->colors[count+1] = rd->colors[count+2] = rd->colors[count+3] = color;

	if(tex)
	{
		assert(rd->coord_allocated_size && rd->coords && rd->texture_id == tex->tex);

		float CWidth = (float)tex->width;
		float CHeight = (float)tex->height;
		float cx1 = x1 / CWidth;
		float cx2 = x2 / CWidth;
		float cy1 = 1 - y1 / CHeight;
		float cy2 = 1 - y2 / CHeight;

		if(mirrorX)
		{
			rd->coords[count+0].x = cx2;
			rd->coords[count+0].y = cy1;

			rd->coords[count+1].x = cx1;
			rd->coords[count+1].y = cy1;

			rd->coords[count+2].x = cx1;
			rd->coords[count+2].y = cy2;

			rd->coords[count+3].x = cx2;
			rd->coords[count+3].y = cy2;
		}
		else
		{
			rd->coords[count+0].x = cx1;
			rd->coords[count+0].y = cy1;

			rd->coords[count+1].x = cx2;
			rd->coords[count+1].y = cy1;

			rd->coords[count+2].x = cx2;
			rd->coords[count+2].y = cy2;

			rd->coords[count+3].x = cx1;
			rd->coords[count+3].y = cy2;
		}
	}
	else
	{
		// ������, ���� �������� �� ���������.
		assert(!rd->coords && !rd->coord_allocated_size);
	}

#ifdef DEBUG_DRAW_SPRITES_BORDERS
	RenderBox(x, y, z, x2-x1, y2-y1, RGBAf(DEBUG_SPRITES_BORDERS_COLOR));
#endif // DEBUG_DRAW_SPRITES_BORDERS

}

// ���������� ��������� �����. ���������� ������� ����������� ���������� ����������.
// ������ ���� ����� ������� �������� ���������� ������� ������ � �������������� ��� ����������� ��������� �������� � ����, �� ��������� ��������������� � ��������.
// x, y, z - ������� ���������� ��������������
// frame_size - ��������� ������� �����
// tex_coords - ��������� �� ���������������� ���������� ���������� (������ ���� ������������)
// tex - ��������
// color - ����
// bm - ����� ���������
void RenderFrameStandard(float x, float y, float z, const coord2f_t* frame_size, const coord2f_t* tex_coords, const Texture* tex, const RGBAf& color, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	// �������������� - �� 4 ����������.
	size_t count = 4;

	render_data* rd = r_GetMem(select_array(ratSprites, color, bm), tex ? tex->tex : 0,
		count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 4;

	rd->vertices[count].z = rd->vertices[count + 1].z = rd->vertices[count + 2].z = rd->vertices[count + 3].z = z;

	rd->vertices[count].x = x;
	rd->vertices[count].y = y;

	rd->vertices[count+1].x = x + frame_size->x;
	rd->vertices[count+1].y = y;

	rd->vertices[count+2].x = x + frame_size->x;
	rd->vertices[count+2].y = y + frame_size->y;

	rd->vertices[count+3].x = x;
	rd->vertices[count+3].y = y + frame_size->y;

	rd->colors[count] = rd->colors[count+1] = rd->colors[count + 2] = rd->colors[count + 3] = color;

	if(tex)
	{
		assert(rd->coord_allocated_size && rd->coords && rd->texture_id == tex->tex);
		memcpy(rd->coords+count, tex_coords, 4 * sizeof(coord2f_t));
	}
	else
	{
		// ������, ���� �������� �� ���������.
		assert(!rd->coords && !rd->coord_allocated_size);
	}

#ifdef DEBUG_DRAW_SPRITES_BORDERS
	RenderBox(rd->vertices + count, 1.0f, RGBAf(DEBUG_SPRITES_BORDERS_COLOR));
#endif // DEBUG_DRAW_SPRITES_BORDERS
}

// ���������� ��������� �����, �������� �������� ���� ��������.
// x, y, z - ������� ���������� ��������������
// frame_size - ��������� ������� �����
// tex_coords - ��������� �� ���������������� ���������� ���������� (������ ���� ������������)
// tex - ��������
// color - ����
// angle - ���� ��������
// stationary-point - ����� ��������
// bm - ����� ���������
void RenderFrameRotated(float x, float y, float z, const coord2f_t* frame_size, const coord2f_t* tex_coords, const Texture* tex, const RGBAf& color, float angle, Vector2 stationary_point, BlendingMode bm/* = bmLast*/)
{
	RenderFramePart( x, y, z, frame_size, frame_size, tex_coords, false, tex, color, angle, stationary_point, bm);
}


// ���������� ��������� ��������� �������� ����� ��������.
// coord, z - ������� ���������
// frame_size - ������ ������������ �������� ��������������
// tex_coords - ������ ���������� �� ���������������� ���������� ���������� (������ ���� ������������)
// tex - ��������
// color - ������ ������
// size - ��������� ��������� � ��������
// bm - ����� ���������
void RenderFrameArray(Vector2* coord, float* z, coord2f_t* frame_size, coord2f_t** tex_coords, const Texture* tex, RGBAf* color, size_t size, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED += static_cast<int>(size);
#endif // DEBUG_PRINT
	assert(size > 0);
	// �������������� - �� 4 ����������.
	size_t count = 4 * size;

	// ����� �� ������������� ����� (� ������ �����), ���� ���������� ������ �������� ����� ������������ � ����������.
	// TODO: ��������, ����� ���������� ������ ���������, ������ ��� ������ ���� ������ ������ �� ����������.

	//render_data* rd = r_GetMem(select_array(ratSprites, *color), tex ? tex->tex : 0,
	//	count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));
	render_data* rd = r_GetMem(render_arrays[ratSprites][bm == bmLast ? bmSrcA_OneMinusSrcA : bm], tex ? tex->tex : 0,
		count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));

	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	size_t i = rd->count;
	//count = rd->count;
	rd->count += count;

	for (size_t j = 0; j < size; i += 4, j++)
	{

		rd->vertices[i].z = rd->vertices[i + 1].z = rd->vertices[i + 2].z = rd->vertices[i + 3].z = z[j];

		rd->vertices[i].x = coord[j].x - frame_size[j].x;
		rd->vertices[i].y = coord[j].y - frame_size[j].y;

		rd->vertices[i+1].x = coord[j].x + frame_size[j].x;
		rd->vertices[i+1].y = coord[j].y - frame_size[j].y;

		rd->vertices[i+2].x = coord[j].x + frame_size[j].x;
		rd->vertices[i+2].y = coord[j].y + frame_size[j].y;

		rd->vertices[i+3].x = coord[j].x - frame_size[j].x;
		rd->vertices[i+3].y = coord[j].y + frame_size[j].y;

		rd->colors[i] = rd->colors[i+1] = rd->colors[i + 2] = rd->colors[i + 3] = color[j];

		if(tex)
		{
			assert(rd->coord_allocated_size && rd->coords && rd->texture_id == tex->tex);

			memcpy(rd->coords+i, tex_coords[j], 4 * sizeof(coord2f_t));
		}
		else
		{
			// ������, ���� �������� �� ���������.
			assert(!rd->coords && !rd->coord_allocated_size);
		}
	}

}


// ��� ������ RebderSpritePart* ������������ ���������� ��������
__INLINE void _RenderFramePartTexSet( const Texture* tex, render_data* rd, bool mirrorX, const coord2f_t* tex_coords, const coord2f_t* part_frame_size, const coord2f_t* full_frame_size, size_t count)
{
	if(tex)
	{
		assert(rd->coord_allocated_size && rd->coords && rd->texture_id == tex->tex);

		float cx1;
		float cx2;
		float cy1;
		float cy2;
		if (mirrorX)
		{
			cx1 = tex_coords[1].x; 
			cx2 = tex_coords[0].x;
		}
		else
		{
			cx1 = tex_coords[0].x; 
			cx2 = tex_coords[1].x;
		}
		cy1 = tex_coords[0].y;
		cy2 = tex_coords[3].y;

		cx2 = cx1 + part_frame_size->x * (cx2 - cx1) / full_frame_size->x;
		cy2 = cy1 + part_frame_size->y * (cy2 - cy1) / full_frame_size->y;

		if(mirrorX)
		{
			rd->coords[count+0].x = cx2;
			rd->coords[count+0].y = cy1;

			rd->coords[count+1].x = cx1;
			rd->coords[count+1].y = cy1;

			rd->coords[count+2].x = cx1;
			rd->coords[count+2].y = cy2;

			rd->coords[count+3].x = cx2;
			rd->coords[count+3].y = cy2;
		}
		else
		{
			rd->coords[count+0].x = cx1;
			rd->coords[count+0].y = cy1;

			rd->coords[count+1].x = cx2;
			rd->coords[count+1].y = cy1;

			rd->coords[count+2].x = cx2;
			rd->coords[count+2].y = cy2;

			rd->coords[count+3].x = cx1;
			rd->coords[count+3].y = cy2;
		}
	}
	else
	{
		// ������, ���� �������� �� ���������.
		assert(!rd->coords && !rd->coord_allocated_size);
	}
}

// ���������� ��������� ����� ����� �������. 
// �� ���� �������� ��������� ������ �������� ���� ������������� �������.
// x, y, z - ������� ���������� ��������������
// full_frame_size - ��������� �� ������� ������� �����
// part_frame_size  ��������� �� ������� ����� �����
// tex_coords - ��������� �� ���������������� ���������� ���������� (������ ���� ������������)
// mirrorX - ���� ���������������
// tex - ��������
// color - ����
// angle - ���� ��������
// stationary-point - ����� ��������
// bm - ����� ���������
void RenderFramePart(float x, float y, float z, const coord2f_t* full_frame_size, const coord2f_t* part_frame_size, const coord2f_t* tex_coords, bool mirrorX, const Texture* tex, const RGBAf& color, float angle/* = 0.0f*/, Vector2 stationary_point/* = Vector2()*/, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT
	// �������������� - �� 4 ����������.
	size_t count = 4;

	render_data* rd = r_GetMem(select_array(ratSprites, color, bm), tex ? tex->tex : 0,
		count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 4;

	if ( angle )
	{
		float kx = cos(angle);
		float ky = sin(angle);
		float rx1 = x - stationary_point.x;
		float ry1 = y - stationary_point.y;
		float rx2 = x + part_frame_size->x - stationary_point.x;
		float ry2 = y + part_frame_size->y - stationary_point.y;

		rd->vertices[count+0].x = stationary_point.x + rx1*kx - ry1*ky;
		rd->vertices[count+0].y = stationary_point.y + rx1*ky + ry1*kx;
		rd->vertices[count+1].x = stationary_point.x + rx2*kx - ry1*ky;
		rd->vertices[count+1].y = stationary_point.y + rx2*ky + ry1*kx;
		rd->vertices[count+2].x = stationary_point.x + rx2*kx - ry2*ky;
		rd->vertices[count+2].y = stationary_point.y + rx2*ky + ry2*kx;
		rd->vertices[count+3].x = stationary_point.x + rx1*kx - ry2*ky;
		rd->vertices[count+3].y = stationary_point.y + rx1*ky + ry2*kx;
	}
	else
	{
		rd->vertices[count+0].x = x;
		rd->vertices[count+0].y = y;
		rd->vertices[count+1].x = x + part_frame_size->x;
		rd->vertices[count+1].y = y;
		rd->vertices[count+2].x = x + part_frame_size->x;
		rd->vertices[count+2].y = y + part_frame_size->y;
		rd->vertices[count+3].x = x;
		rd->vertices[count+3].y = y + part_frame_size->y;
	}

	RGBAf clr(color);
	if (clr.a < 0)
	{
		clr.a = -clr.a;
	}

	for (size_t i = count; i < count+4; i++)
	{
		rd->vertices[i].z = z;
		memcpy(rd->colors + i, &clr, sizeof(RGBAf));
	}
	
	_RenderFramePartTexSet(tex, rd, mirrorX, tex_coords, part_frame_size, full_frame_size, count);

	
#ifdef DEBUG_DRAW_SPRITES_BORDERS
	RenderBox(rd->vertices + count, 1.0f, RGBAf(DEBUG_SPRITES_BORDERS_COLOR));
#endif // DEBUG_DRAW_SPRITES_BORDERS
}

// ���������� ��������� ����� ����� �������. 
// ������ ��������� ������������ ������ �������� ����.
// �� ���� �������� ��������� ������ �������� ���� ������������� �������.
// x, y, z - ������� ���������� ��������������
// full_frame_size - ��������� �� ������� ������� �����
// part_frame_size  ��������� �� ������� ����� �����
// tex_coords - ��������� �� ���������������� ���������� ���������� (������ ���� ������������)
// mirrorX - ���� ���������������
// tex - ��������
// color - ����
// angle - ���� ��������
// bm - ����� ���������
void RenderFramePartLT(float x, float y, float z, const coord2f_t* full_frame_size, const coord2f_t* part_frame_size, const coord2f_t* tex_coords, bool mirrorX, const Texture* tex, const RGBAf& color, float angle/* = 0.0f*/, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT
	// �������������� - �� 4 ����������.
	size_t count = 4;

	render_data* rd = r_GetMem(select_array(ratSprites, color, bm), tex ? tex->tex : 0,
		count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 4;

	if ( angle )
	{
		float kx = cos(angle);
		float ky = sin(angle);
		
		// ������� �������� m(w, 0) � n(0, h)
		float mx = part_frame_size->x*kx;
		float my = part_frame_size->x*ky;
		float nx = -part_frame_size->y*ky;
		float ny = part_frame_size->y*kx;

		rd->vertices[count+0].x = x;
		rd->vertices[count+0].y = y;
		rd->vertices[count+1].x = x + mx;
		rd->vertices[count+1].y = y + my;
		rd->vertices[count+2].x = x + mx + nx;
		rd->vertices[count+2].y = y + my + ny;
		rd->vertices[count+3].x = x + nx;
		rd->vertices[count+3].y = y + ny;
	}
	else
	{
		rd->vertices[count+0].x = x;
		rd->vertices[count+0].y = y;
		rd->vertices[count+1].x = x + part_frame_size->x;
		rd->vertices[count+1].y = y;
		rd->vertices[count+2].x = x + part_frame_size->x;
		rd->vertices[count+2].y = y + part_frame_size->y;
		rd->vertices[count+3].x = x;
		rd->vertices[count+3].y = y + part_frame_size->y;
	}

	for (size_t i = count; i < count+4; i++)
	{
		rd->vertices[i].z = z;
		memcpy(rd->colors + i, &color, sizeof(RGBAf));
	}

	_RenderFramePartTexSet(tex, rd, mirrorX, tex_coords, part_frame_size, full_frame_size, count);

#ifdef DEBUG_DRAW_SPRITES_BORDERS
	RenderBox(rd->vertices + count, 1.0f, RGBAf(DEBUG_SPRITES_BORDERS_COLOR));
#endif // DEBUG_DRAW_SPRITES_BORDERS
}

// ���������� ���������� ��������� �������, �������� �� ��������� �������.
// coord, z - ������� ���������� ������ �������
// edge - ������ ������ ������� �������
// bs - �������������� ����� ������ ������.
// frame - �������� ����� (������� � ���������� ����������)
// tex - ��������
// color - ����
// mirrorX - ���� ���������������
// repeat_x, repeat_y - ����� ������� �� ����
// bm - ����� ���������
void RenderFrameCyclic(const Vector2& coord, float z, const Vector2& edge, const Vector2& bs, 
	const FrameInfo* frame, const Texture* tex, const RGBAf& color, bool mirrorX,
	bool repeat_x, bool repeat_y, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	if (!tex)
		return;

	// �������������� - �� 4 ����������.
	size_t count = 4, old_count, new_max_count;


	float x_count = 1, y_count = 1;
	if (repeat_x)
		x_count = (edge.x - coord.x + bs.x) / frame->size.x;
	if (repeat_y)
		y_count = (edge.y - coord.y + bs.y) / frame->size.y;

	assert(x_count >= 0);
	assert(y_count >= 0);

	size_t sx_count = (size_t)ceil(x_count);
	size_t sy_count = (size_t)ceil(y_count);
	count *= sx_count * sy_count;

	if (count <= 0)
		return;

	render_data* rd = r_GetMem(select_array(ratSprites, color, bm), tex ? tex->tex : 0,
		count*sizeof(vertex3f_t), tex ? count*sizeof(coord2f_t) : 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	old_count = rd->count;
	rd->count += count;
	new_max_count = rd->count;
	count = old_count;

	float CWidth = (float)tex->width;
	float CHeight = (float)tex->height;

	float x1, x2, y1, y2;
	float rx, ry, rw, rh;
	bool first_x = true;
	bool first_y = true;
	ry = coord.y;
	for ( size_t i = 0; i < sy_count; i++, ry += frame->size.y )
	{
		first_x = true;
		
		rx = coord.x;
		for ( size_t j = 0; j < sx_count; j++, rx += frame->size.x )
		{
			x1 = frame->coord->x * CWidth;
			y1 = (1 - frame->coord->y) * CHeight;
			if ( first_x )
			{
				x1 += bs.x;
				x2 = x1 + min( frame->size.x - bs.x, edge.x - rx );
			}
			else
				x2 = x1 + min( frame->size.x, edge.x - rx );
			if ( first_y )
			{
				y1 += bs.y;
				y2 = y1 + min( frame->size.y - bs.y, edge.y - ry );
			}
			else
				y2 = y1 + min( frame->size.y, edge.y - ry );

			rw = x2 - x1;
			rh = y2 - y1;

			rd->vertices[count].x = rx;
			rd->vertices[count].y = ry;

			rd->vertices[count+1].x = rx + rw;
			rd->vertices[count+1].y = ry;

			rd->vertices[count+2].x = rx + rw;
			rd->vertices[count+2].y = ry + rh;

			rd->vertices[count+3].x = rx;
			rd->vertices[count+3].y = ry + rh;

			rd->vertices[count].z = rd->vertices[count + 1].z = rd->vertices[count + 2].z = rd->vertices[count + 3].z = z;


			rd->colors[count] = rd->colors[count+1] = rd->colors[count + 2] = rd->colors[count + 3] = color;

			
			float cx1 = x1 / CWidth;
			float cx2 = x2 / CWidth;
			float cy1 = 1 - y1 / CHeight;
			float cy2 = 1 - y2 / CHeight;

			if(mirrorX)
			{
				rd->coords[count+0].x = cx2;
				rd->coords[count+0].y = cy1;

				rd->coords[count+1].x = cx1;
				rd->coords[count+1].y = cy1;

				rd->coords[count+2].x = cx1;
				rd->coords[count+2].y = cy2;

				rd->coords[count+3].x = cx2;
				rd->coords[count+3].y = cy2;
			}
			else
			{
				rd->coords[count+0].x = cx1;
				rd->coords[count+0].y = cy1;

				rd->coords[count+1].x = cx2;
				rd->coords[count+1].y = cy1;

				rd->coords[count+2].x = cx2;
				rd->coords[count+2].y = cy2;

				rd->coords[count+3].x = cx1;
				rd->coords[count+3].y = cy2;
			}

			count += 4;

			// � ������ ������� ��� ���������� ������� 1 ������ ���� � ������� �� ���������� ������. ��������.
			// TODO: Smells like a �������. �� assert �� ������ ���� ����� ���� ��� �� ��� �����, � ��� ������� ����� 0 �� ����.
			if (count == new_max_count)
			{
				repeat_x = false;
				repeat_y = false;
			}

			if ( first_x )
			{
				rx -= bs.x; // �� ���� ��������� �������� ������.
				first_x = false;
			}
			if ( !repeat_x ) break;
		}
		if ( first_y )
		{
			ry -= bs.y; // �� ���� ��������� �������� ������.
			first_y = false;
		}
		if ( !repeat_y ) break;
	}

	assert(count == new_max_count);

}


// ���������� ��������� ����� � �������� ������� � ����� ��������. ���������� ���� �� ����������� ���� ������� � ����������� �� ������.
// x, y, z - ������� ���������� ������ �������
// area - ������� ���������
// frame - �������� ����� (������� � ���������� ����������)
// tex - ��������
// color - ����
// mirrorX - ���� ��������������
// method - ����� ���������
//   method = rsmStandart: area �� ������������
//   method = rsmStretch:  area - �������, �� ������� �����������
//   method = rsmCrop:     area - �������, �� ������� ��������
//   method = rsmRepeat*:  area - �������, �� ������� ����������
// angle - ���� ��������
// bm - ����� ���������
void RenderFrame(float x, float y, float z, const CAABB& area, 
	const FrameInfo* frame, const Texture* tex, const RGBAf& color, bool mirrorX,
	RenderSpriteMethod method, float angle, BlendingMode bm/* = bmLast*/)
{
	bool repeat_x = false;
	bool repeat_y = false;
	switch(method)
	{
	case rsmStretch:
		{
			coord2f_t size;
			size.x = area.W * 2;
			size.y = area.H * 2;
			RenderFrameRotated(x, y, z, &size, mirrorX ? frame->coordMir : frame->coord, tex, color, angle, area.p, bm);
		}
		break;
	case rsmCrop:
		{
			coord2f_t crop;
			crop.x = min(frame->size.x, area.W*2);
			crop.y = min(frame->size.y, area.H*2);
			//RenderSprite(x, y, z, &crop, mirrorX ? frame->coord : frame->coord, tex, color);
			RenderFramePart(x, y, z, &frame->size, &crop, mirrorX ? frame->coordMir : frame->coord, mirrorX, tex, color, angle, area.p, bm);
		}
		break;
	case rsmRepeatX:
		repeat_x = true;
		goto repeat;
		break;
	case rsmRepeatY:
		repeat_y = true;
		goto repeat;
		break;
	case rsmRepeatXY:
		repeat_x = true;
		repeat_y = true;
		goto repeat;
		break;
	case rsmStandart:
	default:
		RenderFrameStandard(x, y, z, &frame->size, mirrorX ? frame->coordMir : frame->coord, tex, color, bm);
		break;
	}

	return;

repeat:

	Vector2 coord = Vector2(x, y);
	Vector2 bs = Vector2( max(area.Left() - coord.x - frame->size.x, 0.0f), max(area.Top() - coord.y - frame->size.y, 0.0f) );
	if ( repeat_x ) coord.x = area.Left();
	if ( repeat_y ) coord.y = area.Top();
	if ( !repeat_x && coord.x < area.Left() )
	{
		bs.x += area.Left() - coord.x;
		coord.x = area.Left();
	}

	Vector2 edge = Vector2( area.Right(), area.Bottom() );

	RenderFrameCyclic(coord, z, edge, bs, frame, tex, color, mirrorX, repeat_x, repeat_y, bm);

	return;
}

// ����� ������������� �� ��� ������� �����������
void RenderBox(const vertex3f_t* source, float z, const RGBAf& color, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	// �������������� - �� 8 ����������.
	size_t count = 8;

	render_data* rd = r_GetMem(select_array(ratLines, color, bm), 0,
		count*sizeof(vertex3f_t), 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 8;

	assert(!rd->coords && !rd->coord_allocated_size && !rd->texture_id);

	rd->vertices[count+0].z = rd->vertices[count+1].z = rd->vertices[count+2].z = rd->vertices[count+3].z =
		rd->vertices[count+4].z = rd->vertices[count+5].z = rd->vertices[count+6].z = rd->vertices[count+7].z = z;
	rd->colors[count+0] = rd->colors[count+1] = rd->colors[count+2] = rd->colors[count+3] =
		rd->colors[count+4] = rd->colors[count+5] = rd->colors[count+6] = rd->colors[count+7] = color;

	// 1 - - 2
	//glVertex2i(_x, y);
	rd->vertices[count+0].x = source[0].x;
	rd->vertices[count+0].y = source[0].y;
	//glVertex2i(x + w, y);
	rd->vertices[count+1].x = source[1].x;
	rd->vertices[count+1].y = source[1].y;

	// 2 - - 3
	//glVertex2i(x + w, y);
	rd->vertices[count+2].x = source[1].x;
	rd->vertices[count+2].y = source[1].y;
	//glVertex2i(x + w, y + h);
	rd->vertices[count+3].x = source[2].x;
	rd->vertices[count+3].y = source[2].y;

	// 3 - - 4
	//glVertex2i(x + w, y + h);
	rd->vertices[count+4].x = source[2].x;
	rd->vertices[count+4].y = source[2].y;
	//glVertex2i(x, y + h);
	rd->vertices[count+5].x = source[3].x;
	rd->vertices[count+5].y = source[3].y;

	// 4 - - 1
	//glVertex2i(_x, y + h);
	rd->vertices[count+6].x = source[3].x;
	rd->vertices[count+6].y = source[3].y;
	//glVertex2i(x, y);
	rd->vertices[count+7].x = source[0].x;
	rd->vertices[count+7].y = source[0].y;
}

void RenderBox(float x, float y, float z, float w, float h, const RGBAf& color, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	// �������������� - �� 8 ����������.
	size_t count = 8;

	render_data* rd = r_GetMem(select_array(ratLines, color, bm), 0,
		count*sizeof(vertex3f_t), 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 8;

	assert(!rd->coords && !rd->coord_allocated_size && !rd->texture_id);


	rd->vertices[count+0].z = rd->vertices[count+1].z = rd->vertices[count+2].z = rd->vertices[count+3].z =
		rd->vertices[count+4].z = rd->vertices[count+5].z = rd->vertices[count+6].z = rd->vertices[count+7].z = z;

	rd->colors[count+0] = rd->colors[count+1] = rd->colors[count+2] = rd->colors[count+3] =
		rd->colors[count+4] = rd->colors[count+5] = rd->colors[count+6] = rd->colors[count+7] = color;

	// 1 - - 2
	//glVertex2i(_x, y);
	rd->vertices[count+0].x = x;
	rd->vertices[count+0].y = y;
	//glVertex2i(x + w, y);
	rd->vertices[count+1].x = x + w;
	rd->vertices[count+1].y = y;

	// 2 - - 3
	//glVertex2i(x + w, y);
	rd->vertices[count+2].x = x + w;
	rd->vertices[count+2].y = y;
	//glVertex2i(x + w, y + h);
	rd->vertices[count+3].x = x + w;
	rd->vertices[count+3].y = y + h;

	// 3 - - 4
	//glVertex2i(x + w, y + h);
	rd->vertices[count+4].x = x + w;
	rd->vertices[count+4].y = y + h;
	//glVertex2i(x, y + h);
	rd->vertices[count+5].x = x;
	rd->vertices[count+5].y = y + h;

	// 4 - - 1
	//glVertex2i(_x, y + h);
	rd->vertices[count+6].x = x;
	rd->vertices[count+6].y = y + h;
	//glVertex2i(x, y);
	rd->vertices[count+7].x = x;
	rd->vertices[count+7].y = y;

}

void RenderLine(float x1, float y1, float x2, float y2, float z, const RGBAf& color, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	// ����� - 2 ����������.
	size_t count = 2;

	render_data* rd = r_GetMem(select_array(ratLines, color, bm), 0,
		count*sizeof(vertex3f_t), 0, count*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	count = rd->count;
	rd->count += 2;

	assert(!rd->coords && !rd->coord_allocated_size && !rd->texture_id);

	rd->vertices[count+0].z = rd->vertices[count+1].z = z;

	rd->colors[count+0] = rd->colors[count+1] = color;

	// 1 - - 2
	//glVertex2i(_x, y);
	rd->vertices[count+0].x = x1;
	rd->vertices[count+0].y = y1;
	//glVertex2i(x + w, y);
	rd->vertices[count+1].x = x2;
	rd->vertices[count+1].y = y2;

}


void RenderEllipse(float x, float y, float w, float h, float z, const RGBAf& color, UINT id, UINT segments, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	render_data* rd = r_GetMem(select_array(ratFilled, color, bm), id,
		segments*sizeof(vertex3f_t), 0, segments*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);

	// ��� id �� ��� ��������, � ����� � ������ �������� ���� �� ���������.
	rd->texture_id = id;


	rd->coords = NULL;
	rd->coord_allocated_size = 0;

	size_t count = rd->count;
	for (GLfloat i = 0; i < 2*M_PI && count < rd->count + segments; i+=(2*(GLfloat)M_PI / segments), count++)
	{
		rd->vertices[count].x = x + cos(i) * w;
		rd->vertices[count].y = y + sin(i) * h;
		rd->vertices[count].z = z;
		rd->colors[count] = color;
	}
	rd->count = count;

}

void RenderTriangle( Vector2 v1, Vector2 v2, Vector2 v3, float z, const RGBAf& color, BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT

	render_data* rd = r_GetMem(select_array(ratTriangles, color, bm), 0,
		3*sizeof(vertex3f_t), 0, 3*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);


	rd->coords = NULL;
	rd->coord_allocated_size = 0;
	
	rd->vertices[rd->count+0].x = v1.x;
	rd->vertices[rd->count+0].y = v1.y;
	rd->vertices[rd->count+1].x = v2.x;
	rd->vertices[rd->count+1].y = v2.y;
	rd->vertices[rd->count+2].x = v3.x;
	rd->vertices[rd->count+2].y = v3.y;

	rd->vertices[rd->count+0].z = rd->vertices[rd->count+1].z = rd->vertices[rd->count+2].z = z;
	rd->colors[rd->count+0] = rd->colors[rd->count+1] = rd->colors[rd->count+2] = color;
	
	rd->count += 3;
}

void RenderPolygon( CPolygon* polygon, Vector2 pos, float z, const RGBAf& color, UINT id , BlendingMode bm/* = bmLast*/)
{
#ifdef DEBUG_PRINT
	DBG_PRINT_RENDERED++;
#endif // DEBUG_PRINT
	
	UINT vertices = polygon->GetVertexCount();
	
	render_data* rd = r_GetMem(select_array(ratFilled, color, bm), id,
		vertices*sizeof(vertex3f_t), 0, vertices*sizeof(RGBAf));
	assert(rd && rd->colors && rd->vertices && rd->vert_allocated_size && rd->colors_allocated_size);
	
	rd->texture_id = id;
	rd->coords = NULL;
	rd->coord_allocated_size = 0;
	
	for (UINT i = 0; i < vertices; i++ )
	{
		rd->vertices[rd->count+i].x = pos.x + (*polygon)[i].x;
		rd->vertices[rd->count+i].y = pos.y + (*polygon)[i].y;
		rd->vertices[rd->count+i].z = z;
		rd->colors[rd->count+i] = color;
	}
	rd->count += vertices;
}
