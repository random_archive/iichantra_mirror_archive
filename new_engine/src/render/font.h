#ifndef __FONT_H_
#define __FONT_H_

#include "renderer.h"
#include "../misc.h"
#include "../resource.h"

// ������� ����� ������
class IFont
{
public:
	Vector2 p;				// ��������� ��������� ������
	float z;
	float scale;
	RGBAf tClr;				// ����.
	RGBAf outline;

	IFont() : z(1.0f), scale(1.0f)
	{
	}

	virtual ~IFont() { DELETEARRAY(buffer); buf_len = 0; }
	virtual void Print(const char *text, ...) = 0;
	//virtual void PrintMultiline(const char *text, CAABB &area);
	size_t PrintMultiline(const char *text, CAABB &area, int cursor = -1, int scroll = -1, bool fake = false);

	virtual UINT GetStringWidth(const char *text) = 0;
	virtual UINT GetStringWidth(const char *text, size_t t1, size_t t2) = 0;
	virtual UINT GetStringHeight(const char *text) = 0;
	virtual UINT GetStringHeight(const char *text, size_t t1, size_t t2) = 0;

	virtual UINT GetWidth(char c) = 0;
	// TODO: ������ �����, � ���������� ������� ���������� ����� ���������� ������� �� ����� �������� ��� ���� ������.
	// �� ���� ����� � ���.
	virtual UINT GetWidthAndDist(char c) = 0;
	virtual UINT GetHeight(char c) = 0;
protected:
	virtual void Draw(const UCHAR *text) = 0;
	virtual void Draw(const UCHAR *text, int cursor) = 0;

	static UCHAR *buffer;		// ������-����� ��� ������������ ������ �� ������. ��������� ������ � �������, ������ �� ���� �������������.
	static size_t buf_len;

	// ������ ������ ������� �, ��� �������������, ������������ ������.
	static void SetBufLen(size_t len)
	{
		if (len <= buf_len) return;
		DELETEARRAY(buffer);
		// ������ ���������� �������, �������� �� �������� 2.
		// ����� �������, �� ����� ������ �������, �� �� ����� ������ ��� �� +1 ��������.
		// � �� ���� �� ����� ����� �������� ����� ������� ������, ����� ������������, ��� � ������ ������ ��������� ����� �� �������� ������.
		buf_len = nextPowerOfTwo(len);
		buffer = new UCHAR[buf_len];
		buffer[0] = 0;
	}
};

//////////////////////////////////////////////////////////////////////////

struct CRectf
{
public:
	Vector2 Min;
	Vector2 Max;
};


#define TEXFONT_DEFAULT_DISTANCE	1		// ���������� ����� ���������
#define FONT_STRING_ADD_LENGTH	256			// � Print ���������� ������ ������ �� ��� �������� ��� ����, ����� �������� ���������

class TexFont : public IFont, public Resource
{
public:
	TexFont(std::string _name, unsigned char* _buffer, size_t _buffer_size);
	~TexFont();

	//RGBAf				tClr;					//	����.
	byte				dist;					//	���������� ����� ���������
	Vector2				wh;						//	������ � ������� � ������� ����-��. ��� ��� ������. x - w, y - h
	bool				doUseGlobalCoordSystem;	//	������������ �� ��� ������ ���������� ������� ��������
	char* _buffer;
	size_t _buffer_size;
	
	bool LoadFrom();
	virtual bool Load();
	virtual bool Recover();
	bool		SaveToFile(char* file_name);

	void		Print(const char *text, ...);
	//void		PrintMultiline(const char *text, CAABB &area);
	//void		PrintMultiline(const char *text, CAABB &area, int cursor);

	virtual UINT GetStringWidth(const char *text);
	virtual UINT GetStringWidth(const char *text, size_t t1, size_t t2);
	virtual UINT GetStringHeight(const char *text);
	virtual UINT GetStringHeight(const char *text, size_t t1, size_t t2);

	virtual UINT GetWidth(char c);
	virtual UINT GetWidthAndDist(char c);
	virtual UINT GetHeight(char c);
protected:
	CRectf		bbox[256];		// �������� ���� ������� ��� ������� �������
	byte		width[256];		// ������ ������� �������
	byte		height[256];	// ������ ������� �������
	char		*FontImageName;	// ��� ����� ��������
	const Texture* tex;

	virtual void Draw(const UCHAR *text);
	virtual void Draw(const UCHAR *text, int cursor);
};

bool LoadTextureFont(const char* in_name, const char* out_name);
void RecoverFonts();
//void FreeFont(WinFont* font);
void FreeFont(const char* font_name);
void FreeFonts(void);
TexFont* FontByName(const char* font_name);

namespace ControlSeqParser
{
	enum CSeqType {ENone, EColor, ENewLine};

	CSeqType CheckControlSeq(const char* text, size_t& shift);
	void GetColor(const char* text, RGBAf& color);
}

#endif // __FONT_H_
