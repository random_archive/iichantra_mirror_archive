#ifndef __SCENE_H_
#define __SCENE_H_

namespace scene
{
	void MainLoop();
	void UpdateScene();
	void ClearScene();
	void DrawScene();
	void Flip();
	bool InitWindow(bool fullscreen);
	bool InitSDL();
	void KillWindow();
	void InitScene(int argc, char *argv[]);
	void FreeScene();
	void ResizeScene();
	void ApplyConfig(bool video_mode_changed, bool backcolor_changed);
	void ProcessInput();
	void EnableScreenshots(bool state);
	bool SaveScreenshot();

	void UpdateWindowCaption(const char* caption_part);

	void setVSync(int interval);
}



#endif // __SCENE_H_

