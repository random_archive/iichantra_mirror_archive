#ifndef __RESOURCE_MGR_H_
#define __RESOURCE_MGR_H_

#include <typeinfo>
#include "misc.h"
#include "defines.h"
#include "script/script.h"
#include <zzip/lib.h>
#include <fstream>
#include <sstream>

extern char path_app[MAX_PATH];
extern unsigned char* input_buffer;
extern size_t input_buffer_size;

// ��������� ����� ��������� �������� ������������� ����.
// ������ ������ �������������� ����������, ���������� � class Resource.
template < typename T >
class ResourceMgr
{
public:
//

	// ������ �������������� �������
	vector<string>* localArchive;
	vector<uint32_t>* localCrc;

	// �������������� ����� �����. ���� ��������, �� ����� ��� ������� ������
	// ��������� ������������� ���� � �����.
	bool autoSearchFile;

	// ������������ ��������. ���� ��������, �� ������ ����� �������� �� ������� ����������.
	bool autoLoadResource;

	// ������������ �������� �� ������. ���� ��������, �� ������ ����� �������� �� ������� ����������.
	bool autoLoadArchiveResource;

	// �������� ���� �������, ������� ������������� c ������� typeid(T).name() �
	// ������������ � ���������� �� �������.
	// ��-�� ������������� typeid(T).name(), ������ ������ ����� �� ��� ������ ��
	// ������ �������� ������. ��� ��� ������� ���, ��������� �� ������ VC4.
	const char* typeName;

	// ���������, ���, ���� true, �� ��� ���������� ������� ���������� ������� ������� Load.
	// ����� ������������ ��� ���������� �������� ������� �� ��������� � ������������ ������ 
	// ����������� ��������. ������������ � �������������.
	bool doLoad;

	// path - ���� � �����, ���������� ������ ��� ���� �������� ������� ����.
	// extension - ���������� ������ ���� �������� ������ ����. �������������
	// �������������� � �����, ��� ����������� ����� �����.
	ResourceMgr(const char *path, const char *extension, vector<string>* archive_list, vector<uint32_t>* crc_list)
	{
		assert(path);
		//assert(extension);
		this->path = StrDupl(path);
		this->file_extension = StrDupl(extension);
		this->localArchive = archive_list;
		this->localCrc = crc_list;

		autoSearchFile = false;
		autoLoadResource = true;
		autoLoadArchiveResource = true;

		doLoad = true;

		typeName = typeid(T).name();
	}

	virtual ~ResourceMgr()
	{
		UnloadAll();
		DELETEARRAY(path);
		DELETEARRAY(file_extension);
	}

	// ������� ��������� ��� ������� ������� ����.
	void UnloadAll()
	{
		if (resMap.size() == 0)
			return;

		Log(DEFAULT_LOG_NAME, logLevelInfo, "Unloading all %s", typeName);
		for (ResMapIter it = resMap.begin(); it != resMap.end(); it++)
		{
			DELETESINGLE(it->second);
		}
		resMap.clear();
	}

	void UnloadAllUnUsed()
	{
		if (resMap.size() == 0)
			return;

		Log(DEFAULT_LOG_NAME, logLevelInfo, "Unloading all unused %s", typeName);
		// TODO: ������������
		ResMapIter it = resMap.begin();
		while(it != resMap.end())
		{
			if (it->second->IsUnUsed())
			{
				DELETESINGLE(it->second);
				resMap.erase(it++);
			}
			else
			{
				Log(DEFAULT_LOG_NAME, logLevelInfo, "Unable to unload %s name %s, it is still used", typeName, it->second->name.c_str());
				++it;
			}
		}
	}



	T* TryLoadUnpacked(const std::string& name, const string& possible_prefix, T*& default_resource)
	{
		T* res = NULL;
		if ( possible_prefix.length() > 0 )
		{
			res = Load(name, possible_prefix);
			if (!res ) res = Load(name);
		}
		else
			res = Load(name);
		if (res)
		{
			// ���������, ������ ������ � map
			if ( default_resource ) 
			{
				if ( default_resource->IsUnUsed() ) 
				{
					DELETESINGLE( default_resource );
				}
				else
				{
					default_resource->unmanaged = true;
				}
			}
			//resMap[name] = res;
			res->priority = unpackedResource;
			return res;
		}
		return NULL;
	}

	T* TryLoadPacked(const string& name, const string& possible_prefix, int max_priority, ResourcePriority& m_priority, T*& default_resource)
	{
		if (autoLoadArchiveResource)
		{
			

			T* res = LoadArchive(name, possible_prefix, max_priority, m_priority);
			if (!res) res = LoadArchive(name, "", max_priority, m_priority);
			if (res)
			{
				// ���������, ������ ������ � map
				if ( default_resource ) 
				{
					if ( default_resource->IsUnUsed() ) 
					{
						DELETESINGLE( default_resource );
					}
					else
					{
						default_resource->unmanaged = true;
					}
				}
				//resMap[name] = res;
				return res;
			}
		}
		return NULL;
	}

	T* GetByName(const string& name, const string& possible_prefix = "", int max_priority = maximumResourcePriority, int min_priority = minimumResourcePriority )
	{
		T* default_resource = NULL;
		ResourcePriority m_priority = static_cast<ResourcePriority>(min_priority);
		if (!name.empty())
		{
			T* res = NULL;
			// ����� � map
			ResMapConstIter it = resMap.find(name);
			if (it != resMap.end() && !it->second->unmanaged )
			{
				if ( it->second->priority == defaultVerifiedResource )
				{
					return it->second;
				}
				default_resource = it->second;
				m_priority = it->second->priority;
			}

			for ( int i = max_priority; i > m_priority; i-- )
			{
				switch ( i )
				{
					case archiveResource:
					{
						res = TryLoadPacked(name, possible_prefix, max_priority, m_priority, default_resource);
						break;
					}
					case unpackedResource:
					{
						res = TryLoadUnpacked(name, possible_prefix, default_resource);
						break;
					}
					default:
					{
						break;
					}
				}
				if ( res ) break;
			}
			if (res)
			{
				resMap[name] = res;
				return res;
			}
			if (default_resource)
			{
				default_resource->priority = defaultVerifiedResource;
				return default_resource;
			}
		}

		Log(DEFAULT_LOG_NAME, logLevelError, "An attempt to get %s by the name of '%s' was unsuccessful", typeName, name.c_str());
		return NULL;
	}

	T* GetFromArchive(const string& name, const string& possible_prefix = "", size_t archive_num = 0 )
	{
		UNUSED_ARG( possible_prefix );
		if (!name.empty())
		{
			return LoadFromSpecificArchive( name, "", archive_num );
		}
		Log(DEFAULT_LOG_NAME, logLevelError, "An attempt to get %s by the name of '%s' was unsuccessful", typeName, name.c_str());
		return NULL;
	}

	T* GetByNameWithHint(const string& name, const string name_hint, const string& possible_prefix = "", int max_priority = maximumResourcePriority, int min_priority = minimumResourcePriority )
	{
		if (name_hint.empty())
		{
			return GetByName(name, possible_prefix, max_priority, min_priority);
		}
		else
		{
			T* default_resource = NULL;
			ResourcePriority m_priority = (ResourcePriority)min_priority;
			T* res = NULL;
			if (max_priority >= unpackedResource && autoLoadResource)
			{
				// ������� ���������
				res = TryLoadUnpacked(name, possible_prefix, default_resource);

			}

			if (!res)
				res = TryLoadPacked(name, possible_prefix, max_priority, m_priority, default_resource);

			if (res)
			{
				std::string hint = name_hint;
				FormNameWithHint(hint);
				if (res->name != hint)
				{
					Log(DEFAULT_LOG_NAME, logLevelInfo, "Resource %s is now renamed to %s by GetByName", res->name.c_str(), hint.c_str());
					res->name = hint;
				}

				resMap[hint] = res;
				return res;
			}
		}
		return NULL;
	}

	T* GetByName(const char* name, const char* possible_prefix = "")
	{
		//assert(name);
#ifdef DEBUG_LUA_CONSTANTS
		SCRIPT::DbgSetFile( name );
#endif
		return name ? GetByName((string)name, (string)possible_prefix) : NULL;
	}

	// ��������� ������ � �������� ������.
	void Unload(const string& name)
	{
		if (!name.empty() && resMap.size())
		{
			ResMapIter it = resMap.find(name);
			if (it != resMap.end())
			{
				if (it->second->IsUnUsed())
				{
					DELETESINGLE(it->second);
					resMap.erase(it);
				}
			}
		}
	}

	void PreloadArchived(const char* name, const char* possible_prefix)
	{
		GetByName( string(name), string(possible_prefix) );
	}

	// �������� ��� ���� ������� ������� Recover() - ��������������/������������ �������.
	void RecoverAll()
	{
		if (resMap.size() == 0)
			return;

		for (ResMapConstIter it = resMap.begin(); it != resMap.end(); it++)
		{
			it->second->Recover();
		}
	}

	void UnloadArchive( string* arch )
	{
		if (resMap.size() == 0)
			return;
		for (ResMapIter it = resMap.begin(); it != resMap.end(); )
		{
			if ( it->second->archive && it->second->archive == arch )
			{
				/*	������ ��� ���� ������� ��� �� ��������� ������ � ������� �����������, �� ����� � �����, ��� ��� Recover 
				������ ������ ������� ��� ��������� � �������� �� ���� ����������. ����� ��� ����� ������� Recover ������ ������� 
				��� ������ ��� ���.*/
				T* old_res = it->second;
				old_res->unmanaged = true;
				if ( old_res->usageCounter == 0 )
				{
					old_res->unmanaged = false;
					DELETESINGLE( old_res );
				}
				resMap.erase( it++ );
			}
			else ++it;
		}
	};

	void ArchiveRegistered()
	{
		for (ResMapConstIter it = resMap.begin(); it != resMap.end(); it++)
		{
			if ( it->second->priority == defaultVerifiedResource )
				it->second->priority = defaultResource;
		}
	}

	bool IsExist(const std::string& name) const
	{
		ResMapConstIter cit = resMap.find(name);
		return cit != resMap.end();
	}

	bool IsExist(const char* name) const
	{
		ResMapConstIter cit = resMap.find(name);
		return cit != resMap.end();
	}

	std::string LoadResource(T* res, std::string name_hint)
	{
		assert(res);

		FormNameWithHint(name_hint);
		
		if (res->name != name_hint)
		{
			Log(DEFAULT_LOG_NAME, logLevelInfo, "Resource %s is now renamed to %s by LoadResource", res->name.c_str(), name_hint.c_str());
			res->name = name_hint;
		}

		resMap[name_hint] = res;
		return name_hint;
	}

	void FormNameWithHint(std::string& name_hint)
	{
		static UINT unique_id = 0;
		while (name_hint.empty() || IsExist(name_hint))
		{
			std::stringstream ss;
			ss << name_hint << ++unique_id;
			name_hint = ss.str();
		}
	}
private:
	// ������ ��� � ���� � ����� � ��������� ��������.
	T* Load(const string& name, const string& path = "")
	{
		string file_name = name;
		if (this->file_extension)
			file_name += string(".") + this->file_extension;

		char full_file_name[MAX_PATH];

		if (autoSearchFile)
		{
			char full_path[MAX_PATH];
			strcpy(full_path,path_app);
			strcat(full_path,this->path);
			if( !FindFile(file_name.c_str(), full_path, full_file_name) )
			{
				Log(DEFAULT_LOG_NAME, logLevelInfo, "File '%s' is not found in default location '%s'", file_name.c_str(), path.c_str() );
				return NULL;
			}
		}
		else
		{
			strcpy(full_file_name, path_app);
			strcat(full_file_name, this->path);
			strcat(full_file_name, path.c_str());
			strcat(full_file_name, file_name.c_str());
		}

		FILE *fin;
		fin = fopen(full_file_name, "rb");
		if ( fin == NULL )
		{
			Log(DEFAULT_LOG_NAME, logLevelInfo, "File '%s' is not found", file_name.c_str());
			return NULL;
		}
		long file_size;

		fseek(fin, 0, SEEK_END);
		file_size = ftell(fin);
		fseek(fin, 0, SEEK_SET);

		if ( (size_t)file_size + 1 > input_buffer_size )
		{
			input_buffer_size = file_size + 1;
			DELETEARRAY( input_buffer );
			input_buffer = new unsigned char[ input_buffer_size + 1 ];
		}
		
		if ( fread(input_buffer,1,file_size,fin) != (size_t)file_size )
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "An error occurred while reading file '%s'", file_name.c_str());
			return NULL;
		}
		fclose(fin);
		
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading %s", file_name.c_str());
		T* t = new T(name, input_buffer, file_size);
		if (doLoad && !t->Load())
		{
			DELETESINGLE(t);
			return NULL;
		}

		return t;
	}

	// �������� �� ������.
	T* LoadArchive(const char* full_archive_name, const char* file_name, const char* full_file_name, const string& name)
	{
		UNUSED_ARG(full_file_name);
		if( !FileExists(full_archive_name) )
		{
			return NULL;
		}
		zzip_error_t error;

		ZZIP_DIR * zipdir = zzip_dir_open(full_archive_name,&error);
		if (!zipdir)
		{
			const zzip_char_t* strerror = zzip_strerror(error);
			Log(DEFAULT_LOG_NAME, logLevelInfo, "An attempt to get %s by the name of '%s' from archive '%s' was unsuccessful: %s.", typeName, file_name,full_archive_name, strerror);
			return NULL;
		}
		ZZIP_FILE * zipfile = zzip_file_open(zipdir,file_name,0);
		if (!zipfile)
		{
			Log(DEFAULT_LOG_NAME, logLevelInfo, "%s was not found in archive %s", file_name, full_archive_name);
			zzip_dir_close(zipdir);
			return NULL;
		}
		UINT file_size = 0;
		ZZIP_STAT stat;
		if ( zzip_file_stat( zipfile, &stat ) == -1 )
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "Got error while requesting the information about file '%s' from archive '%s': %s", file_name, full_archive_name, zzip_strerror_of(zipdir));
			zzip_file_close( zipfile );
			zzip_dir_close(zipdir);
			return NULL;
		}
		file_size = (UINT)stat.st_size;
		if ( file_size + 1 > input_buffer_size )
		{
			input_buffer_size = file_size + 1;
			DELETEARRAY( input_buffer );
			input_buffer = new unsigned char[ input_buffer_size + 1 ];
		}
		size_t buffer_size = zzip_file_read(zipfile, input_buffer, file_size);
		if ( buffer_size < file_size )
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "An error occurred while reading form archive '%s' file '%s': %s", full_archive_name, file_name, zzip_strerror_of(zipdir));
			zzip_file_close( zipfile );
			zzip_dir_close(zipdir);
			return NULL;
		}
		zzip_file_close(zipfile);
		zzip_dir_close(zipdir);
		
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading %s", file_name);

		T* t = new T(name,input_buffer,file_size);
		if (doLoad && !t->Load())
		{
			DELETESINGLE(t);
			t = NULL;
		}

		return t;
	}

	// ������ ��� � ���� � ����� � ��������� �������� �� ������.
	T* LoadArchive(const string& name, const string& path = "", int max_priority = maximumResourcePriority, int min_priority = minimumResourcePriority)
	{
		string file_name = name;
		size_t found = file_name.find_last_of("/\\");
		const string local_path = file_name.substr(0,found+1);
		file_name = file_name.substr(found+1);
		if (this->file_extension)
			file_name += string(".") + this->file_extension;

		char local_file_name[MAX_PATH];
		strcpy(local_file_name, this->path);
		strcat(local_file_name, path.c_str());
		strcat(local_file_name, local_path.c_str());
		strcat(local_file_name, file_name.c_str());

		char full_archive_name[MAX_PATH];
		if ( max_priority >= archiveResource && min_priority < archiveResource )
		{
			size_t num = 0;
			for ( vector<string>::reverse_iterator iter = localArchive->rbegin(); iter != localArchive->rend(); iter++ )
			{
				strcpy(full_archive_name,path_app);
				strcat(full_archive_name, iter->c_str() );
/*				uint32_t crc = crc32f( full_archive_name );
				if ( crc != localCrc->at( num ) )
				{
					Highscores::LockScore();
				}*/
				T* t = LoadArchive(full_archive_name,local_file_name,local_file_name,name);
				if (t)
				{
					t->priority = archiveResource;
					t->archive = &(*iter);
					return t;
				}
				num++;
			}
		}

		if ( min_priority < defaultResource )
		{
			strcpy(full_archive_name,path_app);
			strcat(full_archive_name,DEFAULT_MAIN_ARCHIVE_NAME);
			T* t = LoadArchive(full_archive_name,local_file_name,local_file_name,name);
			if ( t ) t->priority = defaultVerifiedResource;
			return t;
		}

		return NULL;
	}

	T* LoadFromSpecificArchive(const string& name, const string& path = "", size_t archive_num = 0)
	{
		if ( archive_num >= localArchive->size() || archive_num < 0 ) return NULL;
		string file_name = name;
		size_t found = file_name.find_last_of("/\\");
		const string local_path = file_name.substr(0,found+1);
		file_name = file_name.substr(found+1);
		if (this->file_extension)
			file_name += string(".") + this->file_extension;

		char local_file_name[MAX_PATH];
		strcpy(local_file_name, this->path);
		strcat(local_file_name, path.c_str());
		strcat(local_file_name, local_path.c_str());
		strcat(local_file_name, file_name.c_str());

		char full_archive_name[MAX_PATH];
		strcpy(full_archive_name,path_app);
		strcat(full_archive_name, localArchive->at(archive_num).c_str() );
		T* t = LoadArchive(full_archive_name,local_file_name,local_file_name,name);
		if (t)
		{
			return t;
		}

		return NULL;
	}

	char* path;
	char* file_extension;

protected:

	typedef typename std::map<string, T*> ResMap;
	typedef typename ResMap::iterator ResMapIter;
	typedef typename ResMap::const_iterator ResMapConstIter;

	ResMap resMap;
};


void InitResourceManagers();
void DestroyResourceManagers();
void RegisterResourceArchive( const char* name );
void UnregisterResourceArchive( const char* name );
//void TestLoading();
#endif // __RESOURCE_MGR_H_
