#ifndef _TEXT_TYPER_H__
#define _TEXT_TYPER_H__

#include "../render/font.h"
#include "../script/timerevent.h"


// ���� ����� �������������� �������� ������ ���, ��� ��� ��������� �� ����� ����������.
// ������� �� �� ����������.
// ������������ ������� ������ �� timerevent.
// � �������� ������ ����������� ����������� '\0' �� ������� 0. ����� �� ���������������
// ���������� ������ ����� ����� period. ����� ����� ������� ������ ���������� max_pos. 
class TextTyper : ITimerEventPerformer
{
	UINT period;					// ������ ������ ��������	
	UINT timerEventId;				// �� ������� �������
	char* buffer;					// ��������� ������

	size_t cur_pos;					// ������� ��������� ����������� ������� � �� ������� �����
	size_t max_pos;					// ����� ������ - ��� ������ ����� ������ �����������

	bool ended;						// �������� �����

	char* sound;					// ���� ��������� �������

	void Init()
	{
		timerEventId = 0;
		buffer = NULL;
		cur_pos = 0;
		max_pos = 0;
		ended = false;
		onTyperEnded = LUA_NOREF;
		pausable = false;
	}

	void PrepareText(const char* text)
	{
		if (text)
		{
			max_pos = strlen(text);
			buffer = new char[max_pos+2];
			buffer[0] = 0;
			memcpy(buffer+1, text, max_pos+1);
		}
		else
		{
			Log(DEFAULT_LOG_NAME, logLevelWarning, "Cretation of TextTyper without text");
		}
	}

public:
	LuaRegRef onTyperEnded;			// ������ �� ���������� ������� ����, ��� ������ �������� �����
	bool pausable;					// ������� ����, ��� ������ ������������������ �� ����� �����.

	TextTyper(const char* text, UINT _period, bool _pausable = false)
		: period(_period), sound(NULL), pausable(_pausable)
	{
		Init();

		PrepareText(text);
	}

	TextTyper(const char* text, const TextTyper& old_typer)
	{
		Init();
		PrepareText(text);
		period = old_typer.GetPeriod();
		pausable = old_typer.pausable;
		sound = StrDupl(old_typer.sound);

		if (old_typer.onTyperEnded >= 0)
		{
			extern lua_State* lua;
			STACK_CHECK_INIT(lua);
			SCRIPT::GetFromRegistry(lua, old_typer.onTyperEnded);
			SCRIPT::RegProc(lua, &this->onTyperEnded, -1);
			STACK_CHECK(lua);
		}

		if (old_typer.IsStarted()) this->Start();
	}

	~TextTyper()
	{
		DELETEARRAY(buffer);
		if ( sound )
			DELETEARRAY(sound);

		SCRIPT::ReleaseProc(&onTyperEnded);

		if (timerEventId)
		{
			DeleteTimerEventById(timerEventId);
		}		
}

	bool Start();
	void Stop();

	virtual void OnTimer(InternalTimerEvent& ev);
	virtual void OnTimetEventDestroy(const InternalTimerEvent& ev);

	void SetSound( const char* sound );
	
	UINT GetPeriod() const { return period; }
	void SetPeriod (UINT val) 
{ 
		period = val;
		if (timerEventId)
			Start();
	}

	bool IsStarted() const { return timerEventId > 0; }
	
	const char* GetBuffer() const { return buffer; }

};


#endif // _TEXT_TYPER_H__
