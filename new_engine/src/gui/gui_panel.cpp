#include "StdAfx.h"
#include "gui_panel.h"
#include "../game/sprite.h"

void GuiPanel::RemoveSprites()
{
	for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
		DELETESINGLE( *iter );
	sprites.clear();
}

void GuiPanel::SetSpritesFixed( bool state )
{
	if ( state ) for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
		(*iter)->SetFixed();
	else for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
		(*iter)->ClearFixed();
}

bool GuiPanel::SetSprite(const char* tex, UINT frame)
{
	//We can't use fixed frame for panels.
	RemoveSprites();

	UNUSED_ARG(tex);
	UNUSED_ARG(frame);

	return true;
}

bool GuiPanel::SetSprite(const Proto* proto, const char* start_anim)
{
	if (!proto)
		return false;

	UNUSED_ARG(start_anim);

	RemoveSprites();

	Sprite* spr = NULL;
	for ( int i = FIRST_PART; i <= LAST_PART; i++ )
	{
		spr = new Sprite(proto);
		spr->SetFixed();
		sprites.push_back( spr );
	}

	z = spr->z;

	sprites[     TOP_LEFT ]->SetAnimation(     "topleft" );
	sprites[          TOP ]->SetAnimation(         "top" );
	sprites[    TOP_RIGHT ]->SetAnimation(    "topright" );
	sprites[         LEFT ]->SetAnimation(        "left" );
	sprites[       MIDDLE ]->SetAnimation(      "center" );
	sprites[        RIGHT ]->SetAnimation(       "right" );
	sprites[  BOTTOM_LEFT ]->SetAnimation(  "bottomleft" );
	sprites[       BOTTOM ]->SetAnimation(      "bottom" );
	sprites[ BOTTOM_RIGHT ]->SetAnimation( "bottomright" );

	sprites[     TOP_LEFT ]->setRenderMethod(rsmStandart);
	sprites[          TOP ]->setRenderMethod(rsmRepeatX);
	sprites[    TOP_RIGHT ]->setRenderMethod(rsmStandart);
	sprites[         LEFT ]->setRenderMethod(rsmRepeatY);
	sprites[       MIDDLE ]->setRenderMethod(rsmRepeatXY);
	sprites[        RIGHT ]->setRenderMethod(rsmRepeatY);
	sprites[  BOTTOM_LEFT ]->setRenderMethod(rsmStandart);
	sprites[       BOTTOM ]->setRenderMethod(rsmRepeatX);
	sprites[ BOTTOM_RIGHT ]->setRenderMethod(rsmStandart);
	
	return true;
}

bool GuiPanel::SetSpriteRenderMethod(RenderSpriteMethod rsm)
{
	//Nope, can't do that.
	UNUSED_ARG( rsm );

	return true;
}

bool GuiPanel::SetSpriteBlendingMode(BlendingMode bm)
{
	if (sprites.size() == 0)
		return false;

	for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
		(*iter)->blendingMode = bm;

	return true;
}

bool GuiPanel::SetColorBox(const RGBAf& color)
{
	bool is_visible = (this->sprites[TOP_LEFT] && this->sprites[TOP_LEFT]->IsVisible());
	RemoveSprites();
	Sprite* spr;
	for ( int i = FIRST_PART; i <= LAST_PART; i++ )
	{
		spr = new Sprite();
		spr->SetFixed();
		spr->color = color;
		spr->render_without_texture = true;
		spr->z = z;
		spr->blendingMode =  bmSrcA_OneMinusSrcA;
		if (is_visible) spr->SetVisible();
		sprites.push_back( spr );
	}

	return true;
}

void GuiPanel::Draw()
{
	if ( sprites.size() > 0 )
	{
		if ( IsVisible() )
		{
			for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
				(*iter)->SetVisible();
		}
		else
		{
			for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
				(*iter)->ClearVisible();
		}
		float x1 = aabb.Left();
		float x2 = aabb.Right();
		float y1 = aabb.Top();
		float y2 = aabb.Bottom();
		sprites[     TOP_LEFT ]->z = z;
		sprites[          TOP ]->z = z;
		sprites[    TOP_RIGHT ]->z = z;
		sprites[         LEFT ]->z = z;
		sprites[       MIDDLE ]->z = z;
		sprites[        RIGHT ]->z = z;
		sprites[  BOTTOM_LEFT ]->z = z;
		sprites[       BOTTOM ]->z = z;
		sprites[ BOTTOM_RIGHT ]->z = z;
		sprites[     TOP_LEFT ]->Draw( CAABB( x1   , y1   , x1+16, y1+16 ) );
		sprites[          TOP ]->Draw( CAABB( x1+16, y1   , x2-16, y1+16 ) );
		sprites[    TOP_RIGHT ]->Draw( CAABB( x2-16, y1   , x2  ,  y1+16 ) );
		sprites[         LEFT ]->Draw( CAABB( x1   , y1+16, x1+16, y2-16 ) );
		sprites[       MIDDLE ]->Draw( CAABB( x1+16, y1+16, x2-16, y2-16 ) );
		sprites[        RIGHT ]->Draw( CAABB( x2-16, y1+16, x2   , y2-16 ) );
		sprites[  BOTTOM_LEFT ]->Draw( CAABB( x1,    y2-16, x1+16, y2    ) );
		sprites[       BOTTOM ]->Draw( CAABB( x1+16, y2-16, x2-16, y2    ) );
		sprites[ BOTTOM_RIGHT ]->Draw( CAABB( x2-16, y2-16, x2   , y2    ) );
	}

	DrawBorder();
}

void GuiPanel::Process()
{
	for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
	{
		sprite = *iter;
		ProcessSprite();
	}
	sprite = NULL;
}
void GuiPanel::SetSpriteColor( RGBAf color )
{
	for ( vector<Sprite*>::iterator iter = sprites.begin(); iter != sprites.end(); iter++ )
	{
		(*iter)->color = color;
	}
}

GuiPanel::GuiPanel()
{
	focusable = false;
}

GuiPanel::~GuiPanel()
{ 
	RemoveSprites();
}
