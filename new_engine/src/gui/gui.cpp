#include "StdAfx.h"
#include "gui.h"

#include "../config.h"

#include "../input_mgr.h"

#include "../misc.h"

extern InputMgr inpmgr;
extern config cfg;
extern lua_State* lua;

Gui::Gui()
{
	focusedWidget = NULL;
	nav_mode = (GuiNavigationMode)cfg.gui_nav_mode;
	nav_cycled = cfg.gui_nav_cycled != 0;
	onKeyDownGlobal = LUA_NOREF;
	onKeyReleaseGlobal = LUA_NOREF;
	onMouseKeyDownGlobal = LUA_NOREF;
	onMouseKeyReleaseGlobal = LUA_NOREF;
	focus_locked = false;
	nextId = 1;
}


Gui::~Gui()
{
	DestroyAllWidgets();

	SCRIPT::ReleaseProc(&this->onKeyDownGlobal);
	SCRIPT::ReleaseProc(&this->onKeyReleaseGlobal);
	SCRIPT::ReleaseProc(&this->onMouseKeyDownGlobal);
	SCRIPT::ReleaseProc(&this->onMouseKeyReleaseGlobal);
}

// ���������� Gui, ����� ������������ ��������, ��������� ������� �����
void Gui::Process()
{
	BatchCreate();

	// �������� ����� ���������� ���������� ������������
	if (this->onKeyDownGlobal >= 0 || this->onKeyReleaseGlobal >= 0)
	{
		int proc = LUA_NOREF;
		int top = lua_gettop(lua);
		bool do_return = false;
		for(InputMgr::EventsIterator it = inpmgr.events.begin();
			it != inpmgr.events.end();
			it++)
		{
			InputEvent* ie = *it;
			if (ie->type == InputKBoard)
			{
				if (ie->state == InputStatePressed && this->onKeyDownGlobal >= 0)
				{
					proc = onKeyDownGlobal;
				}
				else if (ie->state == InputStateReleased && this->onKeyReleaseGlobal >= 0)
				{
					proc = onKeyReleaseGlobal;
				}
				else
					continue;

				lua_pushinteger(lua, ie->kb.key);
			}
			else if (ie->type == InputMouse)
			{
				if (ie->state == InputStatePressed && this->onMouseKeyDownGlobal >= 0)
				{
					proc = onMouseKeyDownGlobal;
				}
				else if (ie->state == InputStateReleased && this->onMouseKeyReleaseGlobal >= 0)
				{
					proc = onMouseKeyReleaseGlobal;
				}
				else
					continue;

				lua_pushinteger(lua, ie->mouse.key);
			}
			else
				continue;

			SCRIPT::ExecChunkFromReg(proc, 1);

			if (lua_gettop(lua) > top && lua_isboolean(lua, -1) && lua_toboolean(lua, -1))
				do_return = true;

			lua_settop(lua, top);
				
			if (do_return)
				return;
		}
	}

	if (widgets.size() == 0)
		return;

	WidgetIter fit;
	WidgetConstRevIter rit;

	for(fit = widgets.begin(); fit != widgets.end(); fit++)
	{
		fit->second->Process();
	}

#ifdef GUI_UNSETFOCUS_ON_MOUSECLICK
	for (BYTE i = 0; i < InputMgr::mousebtns_count; i++)
		if ( this->focusedWidget && inpmgr.mouse[i] && 
			!focusedWidget->aabb.PointInCAABB((float)inpmgr.mouseX, (float)inpmgr.mouseY) && focus_locked )
					{
							UnlockFocus();
							this->focusedWidget->OnUnFocus();
							SetFocus((GuiWidget*)NULL);
					}
#endif //GUI_UNSETFOCUS_ON_MOUSECLICK

	// ������������ ������ ����� ��������� � ����������
	if (nav_mode != gnm_None && widgets.size() > 1)
	{
		int direct = 0;		// ����������� ������������

		if (inpmgr.IsPressed(cakGuiNavPrev) || inpmgr.IsRepeated(cakGuiNavPrev))
			direct = -1;	// �����
		else if (inpmgr.IsPressed(cakGuiNavNext) || inpmgr.IsRepeated(cakGuiNavNext))
			direct = 1;		// ������

		if (direct != 0)
		{
			//fit = find(widgets.begin(), widgets.end(), focusedWidget);
			if (focusedWidget)
			{
				//assert(GetWidget(focusedWidget->id));
				fit = widgets.find(focusedWidget->id);
			}
			else
			{
				fit = widgets.end();
			}

			WidgetIter last = widgets.end();	// ��������� � ������ ������
			last--;

			size_t i = 0;

			// ����� ���������� �������, �������� ����� ������� ��������
			do {
				if (fit == widgets.begin())		// � ������
				{
					if (direct == 1) fit++;
					else if (direct == -1 && nav_cycled) fit = last;
				}
				else if (fit == widgets.end())	// ��� ���������������
				{
					fit = widgets.begin();
				}
				else if (fit == last)			// � �����
				{
					if (direct == -1) fit--;
					else if (direct == 1 && nav_cycled) fit = widgets.begin();
				}
				else							// ����������
				{
					if (direct == 1) fit++;
					else fit--;
				}

				if (fit == widgets.end())
					break;
				if (fit == last && !nav_cycled)
					break;
				if (++i == widgets.size())
					break;

			} while (fit->second->staticWidget || !fit->second->visible || !fit->second->CanGainFocus() );

			if (fit != widgets.end() && !fit->second->staticWidget && fit->second->visible)
				SetFocus(fit->second);
		}
	}

	// �������� ����� ���������� �������, �� ������ �����
	if (focusedWidget && focusedWidget->fully_added  && focusedWidget->IsVisible())
	{
		for(InputMgr::EventsIterator it = inpmgr.events.begin();
			it != inpmgr.events.end();
			it++)
		{
			InputEvent* ie = *it;
			if (ie->type == InputKBoard)
			{
				if (ie->state == InputStatePressed)
				{
					focusedWidget->OnKeyDown((USHORT)ie->kb.key);
					//�������� ������ ��� ���������� � cp1251 �������� (�� ����� ����� ������ �� �����)
					if ( ie->kb.symbol >= 31/* && ie->kb.symbol <= 255 */) 
						focusedWidget->OnKeyInput(ie->kb.symbol);
				}
				else
				{
					focusedWidget->OnKeyPress((USHORT)ie->kb.key);
				}
			}
		}


		if (inpmgr.IsReleased(cakGuiNavAccept))
			focusedWidget->OnNavAccept();

		if (inpmgr.IsReleased(cakGuiNavDecline))
			focusedWidget->OnNavDecline();
	}

	// ��������� �������� �����
	if (inpmgr.mouse_moved)
	{
		for(fit = widgets.begin(); fit != widgets.end(); fit++)
		{
			GuiWidget* w = fit->second;
			//if (w->static) continue;
			if (!w->IsVisible()) continue;
			if (!w->CanGainFocus()) continue;
			//if (!w->enabled) continue;

			if (w->aabb.PointInCAABB((float)inpmgr.mouseX, (float)inpmgr.mouseY))
			{
				if (w->lastMousePos == MouseIsOut)
				{
					// ������ ��� ������
					w->OnMouseEnter();
				}
#ifdef GUI_SETFOCUS_ON_MOUSEMOVE
				if (w->CanGainFocus()) this->SetFocus(w);
#endif // GUI_SETFOCUS_ON_MOUSEMOVE
				w->lastMousePos = MouseIsIn;
			}
			else
			{
				if (w->lastMousePos == MouseIsIn)
				{
					// � �� ����� ����
					w->OnMouseLeave();

#ifdef GUI_UNSETFOCUS_ON_MOUSELEAVE
					if (w == this->focusedWidget)
						SetFocus((GuiWidget*)NULL);
#endif // GUI_UNSETFOCUS_ON_MOUSELEAVE

					for (BYTE i = 0; i < InputMgr::mousebtns_count; i++)
						w->lastmouseButtonState[i] = MouseButtonUnpresed;
				}
				w->lastMousePos = MouseIsOut;
			}
		}
	}

	// ��������� ������� ������ �����
#ifdef GUI_SETFOCUS_ON_MOUSECLICK
	for(rit = widgets.rbegin(); rit != widgets.rend(); rit++)
	{
		GuiWidget* w = rit->second;
		if (!w->IsVisible()) continue;
		if (w->lastMousePos == MouseIsIn)
		{
			for (BYTE i = 0; i < InputMgr::mousebtns_count; i++)
			{
				if (inpmgr.mouse[i] && w->lastmouseButtonState[i] == MouseButtonUnpresed)
				{
					w->lastmouseButtonState[i] = MouseButtonPressed;
				}
				else if (!inpmgr.mouse[i] && w->lastmouseButtonState[i] == MouseButtonPressed)
				{
					// mouse up
					w->OnMouseClick(i);

					this->SetFocus(w);

					w->lastmouseButtonState[i] = MouseButtonUnpresed;
				}
			}
#else	//Only send events to the widget in focus
	if (this->focusedWidget && this->focusedWidget->fully_added && 
		this->focusedWidget->IsVisible() && 
		this->focusedWidget->lastMousePos == MouseIsIn)
	{
		GuiWidget* focus = this->focusedWidget;
		for (BYTE i = 0; i < InputMgr::mousebtns_count; i++)
			{
				if (inpmgr.mouse[i] && focus->lastmouseButtonState[i] == MouseButtonUnpresed)
				{
					focus->lastmouseButtonState[i] = MouseButtonPressed;
				}
				else if (!inpmgr.mouse[i] && focus->lastmouseButtonState[i] == MouseButtonPressed)
				{
					// mouse up
					focus->lastmouseButtonState[i] = MouseButtonUnpresed;
					focus->OnMouseClick(i);
				}
			}
	}
#endif // GUI_SETFOCUS_ON_MOUSECLICK

	BatchDestroy();
}

void Gui::Draw()
{
	//_main->Draw();
	for(WidgetIter it = widgets.begin(); it != widgets.end(); it++)
	{
		GuiWidget* w = it->second;

		if (w->IsVisible())
			w->Draw();
	}
}


UINT Gui::CreateWidget(WidgetTypes wt, const char* name, float x, float y, float w, float h, GuiWidget* parent )
{
	GuiWidget* wi = NULL;

	switch (wt)
	{
	case wt_Widget: wi = new GuiWidget(); break;
	case wt_Button: wi = new GuiButton(); break;
	case wt_Label: wi = new GuiLabel(); break;
	case wt_Picture: wi = new GuiPicture(); break;
	case wt_Textfield: wi = new GuiTextfield(); break;
	case wt_Panel: wi = new GuiPanel(); break;
	default:
		Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Attempting to create widget of unknown type. Nothing is created.");
		return 0;
	}

	wi->name = StrDupl(name);
	wi->aabb = CAABB(x, y, x+w, y+h);
	if ( parent ) 
	{
		parent->AddChild( wi );
		wi->parent_visible = parent->IsVisible();
	}

	createdWidgets[wi->id = nextId] = wi;

	Log(DEFAULT_GUI_LOG_NAME, logLevelInfo, "Widget %s created, id = %d, addr %p", wi->name, wi->id, wi);
	return nextId++;
}


void Gui::DestroyWidget(UINT id)
{
	GuiWidget* wi = GetWidget(id);
	if (wi && !wi->dead)
	{
		wi->dead = true;
		destroyedWidgets.push_back(wi);
		for ( vector<GuiWidget*>::iterator iter = wi->children.begin(); iter != wi->children.end(); iter++ )
		{
			GuiWidget* chwi = (*iter);
			chwi->parent = NULL;
			DestroyWidget( chwi->id );
		}
		wi->children.clear();

		if ( wi->parent )
		{
			for ( vector<GuiWidget*>::iterator iter = wi->parent->children.begin(); iter != wi->parent->children.end(); iter++ )
			{
				if ( (*iter)->id == id )
				{
					wi->parent->children.erase( iter );
					break;
				}
			}
		}
	}
}

void Gui::DestroyAllWidgets()
{
	WidgetIter it;
	for(it = widgets.begin(); it != widgets.end(); it++)
	{
		assert(!it->second->dead);
		DELETESINGLE(it->second);
	}
	widgets.clear();

	for(it = createdWidgets.begin(); it != createdWidgets.end(); it++)
	{
		// ������������� ����� ����� ���� �������, ���� ��� �� �������� ����� ��� �� ����, ��� �� ������ ����� BatchCreate
		if (!it->second->dead)
			DELETESINGLE(it->second);
	}
	createdWidgets.clear();

	for(list<GuiWidget*>::iterator it = destroyedWidgets.begin(); it != destroyedWidgets.end(); it++)
	{
		DELETESINGLE((*it));
	}
	destroyedWidgets.clear();

	focusedWidget = NULL;

	nextId = 1;
}

void Gui::BatchCreate()
{
	if (!createdWidgets.empty())
	{
		for (WidgetIter it = createdWidgets.begin(); it != createdWidgets.end(); it++)
			it->second->fully_added = true;		

		widgets.insert(createdWidgets.begin(), createdWidgets.end());
		createdWidgets.clear();
	}
}

void Gui::BatchDestroy()
{
	if (!destroyedWidgets.empty())
	{
		for(list<GuiWidget*>::iterator it = destroyedWidgets.begin();
			it != destroyedWidgets.end(); it++)
		{
			GuiWidget* w = (*it);

			if (widgets.erase(w->id) == 0)
				createdWidgets.erase(w->id);

			if (w == focusedWidget)
				focusedWidget = NULL;

			DELETESINGLE(w);
		}
		destroyedWidgets.clear();
	}
}

void Gui::LockFocus()
{
	this->focus_locked = true;
}

void Gui::UnlockFocus()
{
	this->focus_locked = false;
}

bool Gui::GetFocusLock()
{
	return this->focus_locked;
}

// ������ �������� ������
void Gui::SetFocus( GuiWidget* wi )
{
	if (focus_locked)
		return;

	if (wi && wi == focusedWidget)
	{
		wi->OnRepeatedFocus();
		return;
	}

	if (focusedWidget)
	{
		focusedWidget->active = false;
	}

	if (wi && !wi->staticWidget && wi->IsVisible())
	{
		if ( focusedWidget )
				focusedWidget->OnUnFocus();
		focusedWidget = wi;
		focusedWidget->active = true;
		focusedWidget->OnFocus();
	}
	else if (!wi)
		focusedWidget = NULL;
}

void Gui::SetFocus(UINT id)
{
	GuiWidget* widget = GetWidget(id);
	if ( widget && id )
		SetFocus(widget);
	else
		focusedWidget = NULL;
}

bool Gui::SetWidgetVisibility(UINT id, bool val)
{
	GuiWidget* widget = GetWidget(id);
	if (widget)
	{
		widget->SetVisibility(val);

		if (focusedWidget && !focusedWidget->IsVisible())
			SetFocus(static_cast<GuiWidget*>(NULL));
		return true;
	}

	return false;
}

GuiWidget* Gui::GetWidget(UINT id)
{
	WidgetIter it = widgets.find(id);
	if (it == widgets.end())
	{
		it = createdWidgets.find(id);
		return it == createdWidgets.end() ? NULL : it->second;
	}
	
	return it->second;
}
