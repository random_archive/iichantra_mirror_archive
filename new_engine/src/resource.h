#ifndef __RESOURCE_H_
#define __RESOURCE_H_

#ifndef PREFER_PACKED
enum ResourcePriority { 
			minimumResourcePriority, 
			defaultResource, 
			defaultVerifiedResource, 
			archiveResource, 
			unpackedResource, 
			maximumResourcePriority
                      };
#else
enum ResourcePriority { 
			minimumResourcePriority, 
			defaultResource, 
			defaultVerifiedResource, 
			unpackedResource,
			archiveResource, 
			maximumResourcePriority
                      };
#endif

class Resource
{
public:
	string name;
	const unsigned char* buffer;
	size_t buffer_size;

	string* archive;
	ResourcePriority priority;
	bool unmanaged;	//������� �� ������� ������ ��-�� �������������. �� �������� �����.

	mutable int usageCounter;

	Resource(string _name, unsigned char* _buffer, size_t _buffer_size)
		: name(_name), buffer(_buffer), buffer_size(_buffer_size)
	{
		usageCounter = 0;
		archive = NULL;
		priority = defaultVerifiedResource;
		unmanaged = false;
	}

	virtual ~Resource()
	{
		//assert(usageCounter == 0);
	}

	virtual bool Load() = 0;
	//virtual bool Unload() = 0;
	virtual bool Recover() { return false; }

	void ReserveUsage()	const	{ usageCounter++; }			// ��� const - ��� ����. �� ����� ���� ��� �����-�������. ������ ��� �����.
	void ReleaseUsage()	const	{ assert(usageCounter > 0); usageCounter--;}
	bool IsUnUsed()		const	{ return usageCounter == 0; }

	static void Release( const Resource* res ) 
	{
		res->ReleaseUsage(); 
		if ( res->unmanaged && res->IsUnUsed() )
			DELETESINGLE( res );
	}
};


#endif // __RESOURCE_H_
