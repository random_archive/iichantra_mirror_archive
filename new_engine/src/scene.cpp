#include "StdAfx.h"

#include <SDL/SDL.h>
#include <IL/il.h>

#include "config.h"
#include "scene.h"

#include "main.h"
#include "misc.h"

#include "script/script.h"
#include "script/timerevent.h"

#include "game/game.h"
#include "game/camera.h"
#include "game/editor.h"

#include "render/font.h"
#include "render/texture.h"
#include "render/image.h"
#include "render/renderer.h"

#include "gui/gui.h"

#include "sound/snd.h"

#include "input_mgr.h"
#include "resource_mgr.h"

#include "game/net.h"


//////////////////////////////////////////////////////////////////////////

#define FPSTICK 1000

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ������� ������� � FPS
UINT fps = 0;			// ���������� ����������� � ������� ������� ������
UINT frames = 0;		// ���������� ����������� � ������ ���� ������� ������
UINT startup_time = 0;	// ��������� ����� (������� ����� ������� ����������)
UINT last_time = 0;
UINT current_time = 0;	// ������� �����

UINT internal_time = 0;	// ���������� ����� ������. ��������� ������ ����� ���� �������.

UINT last_fps_tick = 0;		// ����� ����������� ���� ���
UINT last_timerevent_tick = 0;	// ����� ����������� ���� ������� �������
UINT last_game_tick = 0;		// ����� ����������� ���� ����
//UINT last_keyb_tick = 0;		// ����� ���������� ������� �� ������� �������
//UINT last_gui_tick = 0;		// ����� ����������� ���� ��������� gui

UINT viewport_x = 0;
UINT viewport_y = 0;
UINT viewport_w = 0;
UINT viewport_h = 0;

bool screenshots_enabled = true;

InputMgr inpmgr;

ProgrammStates current_state = PROGRAMM_LOADING;				// ������� ��������� ����

config cfg;

Gui* gui;

extern SoundMgr * soundMgr;

extern game::GameStates game_state;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

bool netgame = false;	// ���� ������� ����
extern char path_screenshots[MAX_PATH];

extern ResourceMgr<Texture> * textureMgr;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ������� FPS
__INLINE void CountFPS()
{
	if(internal_time - last_fps_tick >= FPSTICK)
	{
		fps = frames;
		frames = 0;
		last_fps_tick = internal_time;
	}
}

// ����� FPS �� �����
// TODO: ����� �������� ���������, �������� ����� ������ �� ����.
__INLINE void PrintFPS()
{
	extern float CAMERA_OFF_X;
	extern float CAMERA_OFF_Y;

	static IFont* font = FontByName(DEFAULT_FONT);
	if (!font)
	{
		return;
	}

	font->p = Vector2(5.0f - CAMERA_OFF_X, 0.0f - CAMERA_OFF_Y);
	font->z = 1.0f;
	font->tClr = RGBAf(0.0f, 1.0f, 0.0f, 1.0f);
	font->outline = RGBAf(0.0f, 0.0f, 0.0f, 1.0f);
	font->Print("FPS: %u\0", fps);
}

#ifdef DEBUG_PRINT

#include <sstream>

int DBG_PRINT_RENDERED = 0;
string DBG_PRINT_CUR_PLR_ANIM;

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT

extern int DBG_BM_COUNT[ratLast][bmLast];

#endif // DEBUG_PRINT_BM_RENDERED_COUNT

//float DBG_PRINT_VAL4 = 0;
//float DBG_PRINT_VAL5 = 0;
//float DBG_PRINT_VAL6 = 0;
//float DBG_PRINT_VAL7 = 0;
//float DBG_PRINT_VAL8 = 0;
//float DBG_PRINT_VAL9 = 0;
// 

extern float CAMERA_GAME_LEFT;
extern float CAMERA_GAME_RIGHT;
extern float CAMERA_GAME_TOP;
extern float CAMERA_GAME_BOTTOM;

extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_RIGHT;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_DRAW_BOTTOM;


__INLINE void DbgPrint()
{
	static IFont* font = FontByName(DEFAULT_FONT);
	if (!font)
	{
		return;
	}
	font->tClr = RGBAf(0.0f, 1.0f, 0.0f, 1.0f);
	font->z = 1.0f;

	extern float CAMERA_OFF_X;
	extern float CAMERA_OFF_Y;
	extern float CAMERA_X;
	extern float CAMERA_Y;
	extern lua_State* lua;

	stringstream ss;
	ss << "CAMX: " << CAMERA_X << "/nCAMY: " << CAMERA_Y << "/n";
	ss << "Rendered: " << DBG_PRINT_RENDERED << "/n";

#ifdef DEBUG_PRINT_BM_RENDERED_COUNT

	for (size_t rat = ratFirst; rat < ratLast; ++rat)
	{
		for (size_t bm = bmNone; bm < bmLast; ++bm)
		{
			ss << DBG_BM_COUNT[rat][bm] << " ";
			DBG_BM_COUNT[rat][bm] = 0;
		}
		ss << "/n";
	}

#endif // DEBUG_PRINT_BM_RENDERED_COUNT

	ss << "GAME: L" << CAMERA_GAME_LEFT << " R:" << CAMERA_GAME_RIGHT << " T:" << CAMERA_GAME_TOP << " B:" << CAMERA_GAME_BOTTOM << "/n";
	ss << "DRAW: L" << CAMERA_DRAW_LEFT << " R:" << CAMERA_DRAW_RIGHT << " T:" << CAMERA_DRAW_TOP << " B:" << CAMERA_DRAW_BOTTOM << "/n";

	ss << "STACK TOP: " << lua_gettop(lua) << "/n";

	font->p = Vector2(5.0f - CAMERA_OFF_X, 10.0f - CAMERA_OFF_Y);
	CAABB aabb(0, 0, static_cast<float>(cfg.scr_width), static_cast<float>(cfg.scr_height));
	font->PrintMultiline(ss.str().c_str(), aabb);

	DBG_PRINT_RENDERED = 0;
}

#endif // DEBUG_PRINT

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// ������� ����

inline void UpdateInternalTimer()
{
	current_time = (SDL_GetTicks() - startup_time);
	internal_time += current_time - last_time;
	last_time = current_time;
}

// �������� �������
bool ProcessEvents()
{
	SDL_Event event;
	while(SDL_PollEvent(&event)) // TODO: Look here!!!!!! http://osdl.sourceforge.net/main/documentation/rendering/SDL-inputs.html
	{

		switch(event.type)
		{
		case SDL_KEYDOWN:
			{
				SDL_keysym keysym = event.key.keysym;
#ifdef CLOSE_ON_ALT_F4
				// ������� �� alt+F4. �������� ����� ���������, ��� ������ �������-������������
				// �� ������, �� ���� Windows ����� ����� �� ������.
				if (keysym.sym == SDLK_F4 && (keysym.mod & KMOD_ALT))
				{
					current_state = PROGRAMM_EXITING;
					return false;
					break;
				}
#endif // CLOSE_ON_ALT_F4

				inpmgr.AddEvent((SDL_KeyboardEvent&)event);
				break;
			}
		case SDL_KEYUP:
			{
				inpmgr.AddEvent((SDL_KeyboardEvent&)event);

// 				char TempChar = TranslateKeyFromUnicodeToChar(event);
// 				SDL_keysym keysym = event.key.keysym;
// 				for(int i=0;i<KeyInputFuncCount;i++)
// 					(KeyFuncCallers[i]->*KeyInputFunctions[i])(KEY_RELEASED, keysym.sym, keysym.mod, TempChar);
// 				keys[keysym.sym] = 0;
				break;
			}
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			{
				inpmgr.AddEvent((SDL_MouseButtonEvent&)event);
				break;
			}
		case SDL_JOYAXISMOTION:
			{
				inpmgr.AddEvent((SDL_JoyAxisEvent&)event);
				break;
			}
		case SDL_JOYBUTTONDOWN:
		case SDL_JOYBUTTONUP:
			{
				inpmgr.AddEvent((SDL_JoyButtonEvent&)event);
				break;
			}
		case SDL_JOYHATMOTION:
			{
				inpmgr.AddEvent((SDL_JoyHatEvent&)event);
				break;
			}
		case SDL_MOUSEMOTION:
			{
				inpmgr.AddEvent((SDL_MouseMotionEvent&)event);
				break;
			}
		case SDL_ACTIVEEVENT:
			{
				if (current_state != PROGRAMM_EXITING)
				{
					if (event.active.state != SDL_APPMOUSEFOCUS)
					{
						if (event.active.gain == 0)
							current_state = PROGRAMM_INACTIVE;
						else
							current_state = PROGRAMM_RUNNING;
						// 					current_state = PROGRAMM_RUNNING;
						// 		case WA_INACTIVE: current_state = PROGRAMM_INACTIVE; break;

					}
				}
				break;
			}
		// SDL_VIDEORESIZE �������� ��� ������� ����� SDL_RESIZABLE.
		case SDL_VIDEORESIZE:
			{
#ifdef MAP_EDITOR
				if ( editor_state != editor::EDITOR_OFF )
				{
					cfg.scr_width = (UINT) (cfg.scr_width * event.resize.w / cfg.window_width);
					cfg.scr_height = (UINT) (cfg.scr_height * event.resize.h / cfg.window_height);
					cfg.window_width = event.resize.w;
					cfg.window_height = event.resize.h;
					//scene::ResizeScene(event.resize.w, event.resize.h);
					scene::ApplyConfig(true, false);
#ifndef NO_WRITE_SCREEN_CFG_ON_RESIZE_EDITOR
					SaveConfig();
#endif // NO_WRITE_SCREEN_CFG_ON_RESIZE_EDITOR
				}
#endif // MAP_EDITOR
				break;
			}
		case SDL_QUIT:
			{
				current_state = PROGRAMM_EXITING;
				return false;
				break;
			}
		default:
			{
				break;
			}
		}
	}
	return true;
}

// ������� ����
void scene::MainLoop()
{
	bool inactive = false;

	while(current_state != PROGRAMM_EXITING)
	{
		if (current_state == PROGRAMM_RUNNING)
			inpmgr.PreProcess();


		if (!ProcessEvents())
			break;

		if (current_state == PROGRAMM_INACTIVE && !netgame)


		{
			// ���������� ���������
			inactive = true;
			if (soundMgr)
				soundMgr->PauseAll();

			UINT t = SDL_GetTicks();

			SDL_WaitEvent(NULL);

			// ���������� �����, ������� ������� � ���������� ���������
			t = SDL_GetTicks() - t;
			// ��� ��� ������� ��� ����� ������������ �� �������.
			last_time += t;
		}
		else
		{
			if (inactive)
			{
				// �������������� ����� ��������� ������������
				// � ���������, ������ ������ �������� ���� ��� ����� WaitMessage,
				// �.�. ���� ���������� ���������� ������������ �������� ��������.
				// ����� ����, ��������, �������� "�������" ���� ��� ���������� ����������.
				inactive = false;
				if (soundMgr)
					soundMgr->ResumeAll();
			}
#ifdef STOP_RENDER_ON_INACTIVE_NETGAME
			if (current_state != PROGRAMM_INACTIVE)
#endif // STOP_RENDER_ON_INACTIVE_NETGAME
			{
				scene::ClearScene();
			}

			scene::UpdateScene();

#ifdef STOP_RENDER_ON_INACTIVE_NETGAME
			if (current_state != PROGRAMM_INACTIVE)
#endif // STOP_RENDER_ON_INACTIVE_NETGAME
			{
				CountFPS();
				scene::DrawScene();
				scene::Flip();

				if (inpmgr.IsPressed(cakScreenshot) && screenshots_enabled)
				{
					SaveScreenshot();
				}
			}
		}

	}
	inactive = true;
}

// ���������� �����, ��������� ��� ������ � �.�.
void scene::UpdateScene()
{
	UpdateInternalTimer();


	if (current_state == PROGRAMM_RUNNING)
		inpmgr.Process();


	if (internal_time - last_timerevent_tick >= TIMEREVENTTICK)
	{
		ProcessTimerEvents();
		last_timerevent_tick = internal_time;
	}

	if (internal_time - last_game_tick >= GAMETICK)
	{
		game::UpdateGame(internal_time - last_game_tick);
		last_game_tick = internal_time;
	}

#ifdef MAP_EDITOR
	ProcessEditor();
#endif // MAP_EDITOR

	if (gui/* && internal_time - last_gui_tick >= GUITICK*/)
	{
		gui->Process();
		//last_gui_tick = internal_time;
	}


}

// ������� �����
void scene::ClearScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	r_ZeroRenderData();

#ifdef _DEBUG
	//RENDERED_TEXTURES = 0;
#endif
}


// ��������� �����
void scene::DrawScene()
{
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	bool transform = _CameraUsesTransformations();

	if (transform)
	{
		_CameraCalcTransformedBounds();
	}

	game::DrawGame();

	// �������� ������� ����. ��������� glScale � _CameraApplyScale() � �������� ���.
	// �������� scale � �������� ���.

	
	if (transform)
	{
		StoreCamera();
		_CameraApplyScale();
		_CameraApplyAngle();

#ifdef DEBUG_PRINT
		RenderBox(CAMERA_GAME_LEFT, CAMERA_GAME_TOP, 1.0f, CAMERA_GAME_RIGHT - CAMERA_GAME_LEFT, CAMERA_GAME_BOTTOM - CAMERA_GAME_TOP, RGBAf(1, 1, 1, 1));
#endif
	}

	// ������ ���� ���������� ��������� ������. Scale, �� ����, �� ���� ��������� �� � ������ � ��������.
	//game::DrawGame();

	if (transform)
	{
		r_RenderAll();
		r_ZeroRenderData();
		RestoreCamera();
	}

	if (gui)
	{
		gui->Draw();
	}

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	if(cfg.show_fps)
	{
		PrintFPS();
	}

#ifdef DEBUG_PRINT
	DbgPrint();

#endif // DEBUG_PRINT

	r_RenderAll();
	
	frames++;
}

// ���� :) (����, ��� ��������, ��� ��� ����� ������)
void scene::Flip()
{
	glFlush();
	SDL_GL_SwapBuffers();
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// �������������
void scene::InitScene(int argc, char *argv[])
{
	//startup_time = GetTickCount();
	InitSDL();
	InitResourceManagers();

	if ( !Net::Init() )
	{
		current_state = PROGRAMM_EXITING;
		return;
	}

	startup_time = SDL_GetTicks();
	ilInit();

	gui = new Gui();

	// �������� ���������� ���������� � ���������� ������� ������������.
	if(SCRIPT::InitScript(argc, argv) || SCRIPT::ExecFile(DEFAULT_INIT_SCRIPT) < 0)
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "No init: aborting game");
		current_state = PROGRAMM_EXITING;
		return;
	}
	else
	{
		current_state = PROGRAMM_RUNNING;
	}

	//TestLoading();

	if (game_state != game::GAME_RUNNING)
	{
		CameraMoveToPos(cfg.scr_width * 0.5f, cfg.scr_height * 0.5f);
		CameraUpdatePosition();
	}


}

// ��������� ������ � �������� ��������� ����
void scene::ApplyConfig(bool video_mode_changed, bool backcolor_changed)
{
	if (video_mode_changed || current_state == PROGRAMM_LOADING)
	{
		// ��������� ���������� ��� ��� ������ �������� ��������.
		// ������ ��� ����� ��������� backcolor, ������������� ���������� �� ����.
		scene::ResizeScene();
		inpmgr.SetScreenToWindowCoeff((float)cfg.scr_width / (float)viewport_w,
			(float)cfg.scr_height / (float)viewport_h);
	}
	else if (backcolor_changed)
	{
		glClearColor(cfg.backcolor_r, cfg.backcolor_g, cfg.backcolor_b, 1.0f);
	}

	if (soundMgr)
	{
		soundMgr->SetVolume(cfg.volume);
		soundMgr->SetMusicVolume(cfg.volume_music);
		soundMgr->SetSoundVolume(cfg.volume_sound);
	}

	if (gui)
	{
		gui->nav_mode = (GuiNavigationMode)cfg.gui_nav_mode;
		gui->nav_cycled = cfg.gui_nav_cycled != 0;
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// ���������������

// ����������� ��� ���������� � �����
void scene::FreeScene()
{
	DeleteAllEvents();
	game::FreeGame(true);
	Net::Close();

	DELETESINGLE(gui);

	r_ClearRenderData();
	FreeFonts();

	DestroyResourceManagers();

	SCRIPT::FreeScript();
	scene::KillWindow();

	SDL_Quit();
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// ���� ����

// ��������� OpenGL
static void InitOGLSettings()
{
	scene::setVSync(cfg.vert_sync);

	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glClearDepth( 1.0f );              // ��������� ������� ������ �������
	glEnable( GL_DEPTH_TEST );            // ��������� ���� �������
	glDepthFunc( GL_LEQUAL );            // ��� ����� �������

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.0f);
	glReadBuffer(GL_BACK);
	glClearColor(cfg.backcolor_r, cfg.backcolor_g, cfg.backcolor_b, 1.0f);
	glEnable(GL_BLEND);
}

// ������������� SDL
bool scene::InitSDL()
{
	if( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_JOYSTICK) < 0 )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Video initialization failed: %s\n", SDL_GetError());
		Log(DEFAULT_LOG_NAME, logLevelError, "Last ERROR was critical. Now exiting...");
		return false;
	}

	if ( (inpmgr.joysticks_num = SDL_NumJoysticks()) > 0 )
	{
		for ( int i = 0; i < min( inpmgr.joysticks_num, MAX_JOYSTICKS ); i++ )
		{
			inpmgr.joystick[i] = SDL_JoystickOpen( i );
			Log(DEFAULT_LOG_NAME, logLevelInfo, "Opening joysick or gamepad %i", i);
			if ( inpmgr.joystick[i] == NULL )
			{
				Log(DEFAULT_LOG_NAME, logLevelInfo, "Failed to open joystick or pad!");
			}
		}
		if ( inpmgr.joysticks_num > MAX_JOYSTICKS )
		{
			Log(DEFAULT_LOG_NAME, logLevelWarning, "More than %i are not supported, sorry.", MAX_JOYSTICKS);
			inpmgr.joysticks_num = MAX_JOYSTICKS;
		}
	}

	SDL_EnableUNICODE(1);
	return true;
}

SDL_Surface* screen;
char* custom_caption_part = NULL;

void scene::UpdateWindowCaption( const char* caption_part )
{
	if ( custom_caption_part ) DELETEARRAY( custom_caption_part );
	custom_caption_part = StrDupl( caption_part );
	char * caption = new char[strlen(GAMENAME) + strlen(VERSION) + strlen(caption_part) + 5 ];
	caption[0] = 0;
	strcat( caption, GAMENAME );
	strcat( caption, " " );
	strcat( caption, VERSION );
	strcat( caption, " (" );
	strcat( caption, caption_part );
	strcat( caption, ")" );

	SDL_WM_SetCaption(caption, NULL);
}

// �������� ����
bool scene::InitWindow(bool fullscreen)
{
	size_t custom = custom_caption_part ? strlen(custom_caption_part) + 3 : 0;
	char * caption = new char[strlen(GAMENAME) + strlen(VERSION) + custom + 2];
	caption[0] = 0;
	strcat( caption, GAMENAME );
	strcat( caption, " " );
	strcat( caption, VERSION );
	if ( custom_caption_part )
	{
		strcat( caption, " (" );
		strcat( caption, custom_caption_part );
		strcat( caption, ")" );
	}

	SDL_WM_SetCaption(caption, NULL);
	DELETEARRAY( caption );
	const SDL_VideoInfo* info = NULL;

	info = SDL_GetVideoInfo();

	if (info == NULL)
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Aaaah! SDL WARNING: %s", SDL_GetError());
		Log(DEFAULT_LOG_NAME, logLevelError, "Last ERROR was critical. Now exiting...");
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,		8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,		8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,		8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,		16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,	1);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,		8);

	int width = cfg.window_width > 0 ? cfg.window_width : DEFAULT_WINDOW_WIDTH;
	int height = cfg.window_height > 0 ? cfg.window_height : DEFAULT_WINDOW_HEIGHT;

	int flags = SDL_OPENGL;
#ifdef MAP_EDITOR
	flags |= SDL_RESIZABLE;
#endif
	if(fullscreen == true)
	{
		flags |= SDL_FULLSCREEN;
		width = cfg.full_width;
		height = cfg.full_height;
	}

	// ��������, ����� ����� ���������� ������ ������� ������. ���� ����� � ��� �� �����.
	SDL_putenv((char*)"SDL_VIDEO_CENTERED=center");
	screen = SDL_SetVideoMode(width, height, cfg.scr_bpp, flags);
	if (screen == NULL)
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Setting video mode failed: %s\n", SDL_GetError());
		//Log(DEFAULT_LOG_NAME, logLevelInfo, "Last error was critical. Now exiting...");
		return false;
	}

// 	SDL_Event resizeEvent;
// 	resizeEvent.type=SDL_VIDEORESIZE;
// 	resizeEvent.resize.w = DEFAULT_WINDOW_WIDTH;
// 	resizeEvent.resize.h = DEFAULT_WINDOW_HEIGHT;
// 	SDL_PushEvent(&resizeEvent);
	
	InitOGLSettings();

	return true;
}

// ���������� ����
void scene::KillWindow()
{
// 	if(fullscreenflag)
// 	{
// 		ChangeDisplaySettings(NULL, 0);
// 		ShowCursor(true);
// 	}
//
// 	if(hRC)
// 	{
// 		if(!wglMakeCurrent(NULL, NULL))
// 		{
// 			Log(DEFAULT_LOG_NAME, LOG_WARNING_EV, "Failed to release current context");
// 			MessageBox(NULL, "Failed to release current context", "wglMakeCurrent() Error", MB_OK | MB_ICONEXCLAMATION);
// 		}
//
// 		if(!wglDeleteContext(hRC))
// 		{
// 			Log(DEFAULT_LOG_NAME, LOG_WARNING_EV, "Failed to delete rendering context");
// 			MessageBox(NULL, "Failed to delete rendering context", "wglDeleteContext() Error", MB_OK | MB_ICONEXCLAMATION);
// 		}
//
// 		hRC = NULL;
// 	}
//
// 	if(hDC && ReleaseDC(game_window, hDC) != 1)
// 	{
// 		Log(DEFAULT_LOG_NAME, LOG_WARNING_EV, "Failed to release device context");
// 		MessageBox(NULL, "Failed to release device context", "ReleaseDC() Error", MB_OK | MB_ICONEXCLAMATION);
// 		hDC = NULL;
// 	}
//
// 	if(game_window && DestroyWindow(game_window) == 0)
// 	{
// 		Log(DEFAULT_LOG_NAME, LOG_WARNING_EV, "Failed to destroy game window");
// 		MessageBox(NULL, "Failed to destroy game window", "DestroyWindow() Error", MB_OK | MB_ICONEXCLAMATION);
// 		game_window = NULL;
// 	}
//
// 	if(!UnregisterClass(window_class.lpszClassName, hInstance))
// 	{
// 		Log(DEFAULT_LOG_NAME, LOG_WARNING_EV, "Failed to unregister game window class");
// 		MessageBox(NULL, "Failed to unregister game window class", "UnregisterClass() Error", MB_OK | MB_ICONEXCLAMATION);
// 	}
}

#ifdef WIN32
#include <SDL_syswm.h>
#endif // WIN32


// �������� ������ ����. �������� ��������� OpenGL.
void scene::ResizeScene()
{
#ifdef WIN32
	// Windows-only code for OpenGL context recovering.
	// See http://bytehazard.com/code/sdlres.html

	SDL_SysWMinfo info;
	SDL_VERSION(&info.version);
	
	bool recover = false;
	static bool first_time = true;

	if (first_time)
		goto on_error1;

	// get window handle from SDL
	recover = SDL_GetWMInfo(&info) > 0;
	if (!recover)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "SDL_GetWMInfo #1 failed");
		goto on_error1;
	}

	// get device context handle
	HDC tempDC = GetDC(info.window);

	// create temporary context
	HGLRC tempRC = wglCreateContext(tempDC);
	recover = tempRC != NULL;
	if (!recover)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "wglCreateContext failed");
		goto on_error1;
	}

	SetLastError(ERROR_SUCCESS);
	// share resources to temporary context
	recover = wglShareLists(info.hglrc, tempRC) == TRUE;
	if (!recover)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "wglShareLists #1 failed");
		goto on_error1;
	}

on_error1:;

#endif // WIN32

	if (!scene::InitWindow(cfg.fullscreen != 0))
	{
		current_state = PROGRAMM_EXITING;
		return;
	}

#ifdef WIN32

	if (first_time)
	{
		first_time = false;
		goto on_firts_time;
	}

	if (!recover)
	{
		goto on_error2;
	}

	// previously used structure may possibly be invalid, to be sure we get it again
	SDL_VERSION(&info.version);
	recover = SDL_GetWMInfo(&info) == 1;
	if (!recover)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "SDL_GetWMInfo #2 failed");
		goto on_error2;
	}

	// share resources to new SDL-created context
	recover = wglShareLists(tempRC, info.hglrc) == TRUE;
	if (!recover)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "wglShareLists #2 failed");
		goto on_error2;
	}

	// we no longer need our temporary context
	if (!wglDeleteContext(tempRC))
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "wglDeleteContext failed");
	}

on_error2:;
	if (!recover)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "OpenGL context recover failed. See previous warnings");
	}

on_firts_time:;

#endif // WIN32


#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		viewport_w = cfg.window_width;
		viewport_h = cfg.window_height;
		viewport_x = 0;
		viewport_y = 0;
	}
	else
#endif
	{
		if (cfg.fullscreen != 0)
		{
			const SDL_VideoInfo* info = SDL_GetVideoInfo();
			viewport_w = static_cast<UINT>(min((UINT)info->current_w, (UINT)floor(cfg.aspect_ratio * info->current_h + 0.5f)));
			viewport_h = static_cast<UINT>(floor(viewport_w / cfg.aspect_ratio + 0.5f));
			viewport_x = (info->current_w - viewport_w) / 2;
			viewport_y = (info->current_h - viewport_h) / 2;
		}
		else
		{
			viewport_w = min(cfg.window_width, (UINT)floor(cfg.aspect_ratio * cfg.window_height + 0.5f));
			viewport_h = static_cast<UINT>(floor(viewport_w / cfg.aspect_ratio + 0.5f));
			viewport_x = (cfg.window_width - viewport_w) / 2;
			viewport_y = (cfg.window_height - viewport_h) / 2;
		}
	}
	
	glViewport(viewport_x, viewport_y, viewport_w, viewport_h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, cfg.scr_width, cfg.scr_height, 0.0, cfg.near_z, cfg.far_z);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


// �� ��� � ��� ����������� ��� ������ �� ��������... �����-�� �������� �������������.. ���� ������������ � ������������..
#if defined(_WIN32) || defined(__linux)        // <platforms, that support swap interval control>


#if defined(_WIN32)
#define SWAP_INTERVAL_PROC PFNWGLSWAPINTERVALFARPROC 
#define GL_GET_PROC_ADDRESS_ARG_TYPE (const char*)
#define SWAP_INTERVAL_EXTENSION_NAME "WGL_EXT_swap_control"
#define SWAP_INTERVAL_PROC_NAME_STR "wglSwapIntervalEXT"
#define SWAP_INTERVAL_PROC_NAME wglSwapIntervalEXT
#define GET_PROC_ADDRESS_FUNC wglGetProcAddress
#elif defined(__linux)
#include <GL/glx.h>
#define SWAP_INTERVAL_PROC PFNGLXSWAPINTERVALSGIPROC
#define GL_GET_PROC_ADDRESS_ARG_TYPE (const GLubyte*)
#define SWAP_INTERVAL_EXTENSION_NAME "GLX_SGI_swap_control"
#define SWAP_INTERVAL_PROC_NAME_STR "glXSwapIntervalSGI"
#define SWAP_INTERVAL_PROC_NAME glXSwapIntervalSGI
#define GET_PROC_ADDRESS_FUNC glXGetProcAddress
#endif


#define GET_PROC_ADDRESS(x,y) ((x(GL_GET_PROC_ADDRESS_ARG_TYPE(y))))

typedef int (APIENTRY *SWAP_INTERVAL_PROC)(int);
SWAP_INTERVAL_PROC SWAP_INTERVAL_PROC_NAME = 0;


#endif                        // </platforms, that support swap interval control>


// ��, ������� ����� ��� ����, �� ��� ���������������� ��� ������ ������ �� ����� ������..
// ��� ����� �� �������� ������ � ������ �����..


void scene::setVSync(int interval)
{
#if defined(_WIN32) || defined(__linux)        // <platforms, that support swap interval control>
#if defined(_WIN32)
	const char *extensions = (const char *) glGetString(GL_EXTENSIONS);
#elif defined(__linux)
	const char *extensions = (const char *) glXGetClientString(glXGetCurrentDisplay(), GLX_EXTENSIONS);
#endif


	if (strstr(extensions, SWAP_INTERVAL_EXTENSION_NAME) != 0)
	{
		SWAP_INTERVAL_PROC_NAME = (SWAP_INTERVAL_PROC) GET_PROC_ADDRESS(GET_PROC_ADDRESS_FUNC, SWAP_INTERVAL_PROC_NAME_STR);


		if (SWAP_INTERVAL_PROC_NAME)
			SWAP_INTERVAL_PROC_NAME(interval > 0 ? 1 : 0);
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, SWAP_INTERVAL_EXTENSION_NAME " is not supported on your computer");
	}
#endif                        // </platforms, that support swap interval control>
}


//////////////////////////////////////////////////////////////////////////



bool scene::SaveScreenshot()
{
	char file_name[MAX_PATH];
	static UINT counter = 0;

	// ����� �� ��� ����� � ������ ��� ������ ����� ���������� ���������, ���� �� ��� �����
	do
	{
	counter++;
#ifdef BMP_SCREENSHOTS
	sprintf(file_name, "%sscreenshot%05d.bmp", path_screenshots, counter);
#else
	sprintf(file_name, "%sscreenshot%05d.png", path_screenshots, counter);
#endif // BMP_SCREENSHOTS
	}
	while(FileExists(file_name));

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Saving screenshot %s", file_name);

	CGLImageData ImageData;
	return ImageData.SaveScreenshot(file_name);
}

void scene::EnableScreenshots( bool state )
{
	screenshots_enabled = state;
}
