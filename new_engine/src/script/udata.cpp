// This file contains the implementation details of type-to-lua binding system.
// All type specific declarations are in userdata_binding.hpp

#include "StdAfx.h"

#include <typeinfo>

#include "typelist.hpp"
#include "CUData.hpp"
#include "lua_pusher.hpp"

#include "udata.hpp"


// The block 1 declares the typelist.
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 1
#include "userdata_binding.hpp"

// Array of references to userdata's metatables for each registerd class
// Those metatables are used to check types and to declare all methods of userdata
LuaRegRef udata_refs[TL::Length<ClassesList>::value] = { };
#define UD_TYPE_ID(T)  TL::IndexOf<ClassesList, T>::value
#define UD_META_REF(T) udata_refs[UD_TYPE_ID(T)]


// Function template for userdata creation.
// This function must be called from implementations of CUDataUser::createUData
template <typename T>
CUData* CreateUData(T& user)
{
	extern lua_State* lua;

	CUData* ud = static_cast<CUData*>(lua_newuserdata(lua, sizeof(CUData)));
	if (!ud)
		return NULL;
	// st: ud

	CUDataUser* p = static_cast<CUDataUser*>(&user);

	ud = new(ud) CUData(p);

	SCRIPT::GetFromRegistry(lua, UD_META_REF(T));   // st: ud meta
	lua_setmetatable(lua, -2);                      // st: ud

	ud->setRegRef(SCRIPT::AddToRegistry());       // st: 
	return ud;
}

// This macro is used to explicitly declare CreateUData for TYPE and 
// to implement CUDataUser::createUData for this TYPE.
// This also helps to not forget the createUData declaration in TYPE.
#define UDATA_CREATOR_FUNC_DECL(TYPE)                                  \
	template CUData* CreateUData<TYPE>(TYPE& user);                    \
	CUData* TYPE::createUData()                                        \
	{                                                                  \
		return CreateUData(*this);                                     \
	}

// The block 2 uses UDATA_CREATOR_FUNC_DECL macro to declare needed functions.
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 2 
#include "userdata_binding.hpp"

// Used for check userdata's metatable.
// Will throw lua error if throw_error == true (default). Otherwise, will return NULL.
template <typename T>
CUData* check_userdata(lua_State* L, int ud, bool throw_error /*= true*/)
{
	CUData *p = reinterpret_cast<CUData*>(lua_touserdata(L, ud));
	if (p != NULL) {  /* value is a userdata? */
		if (lua_getmetatable(L, ud)) {  /* does it have a metatable? */
			SCRIPT::GetFromRegistry(L, UD_META_REF(T));  /* get correct metatable */
			if (lua_rawequal(L, -1, -2)) {  /* does it have the correct mt? */
				lua_pop(L, 2);  /* remove both metatables */
				return p;
			}
		}
	}
	if (throw_error)
		luaL_argerror(L, ud, "got wrong userdata");  /* else error */
	// luaL_argerror will not return.
	return NULL;
}

// Special standard methamethods. It is wise to use them for all methatables (at least, on_gc).
// Invoked on gc event. Used to make the user object forget the CUData existance.
template <typename T>
int on_gc(lua_State* L)
{
	CUData* ud = check_userdata<T>(L, 1);
	ud->onGC();
	return 0;
}

template <typename T>
int on_tostring(lua_State* L)
{
	CUData* ud = check_userdata<T>(L, 1);
	lua_pushfstring(L, "userdata: %p, %s", ud, typeid(T).name());
	return 1;
}

template <typename T>
int object_present(lua_State* L)
{
	CUData* ud = check_userdata<T>(L, 1);
	if (!ud->getUser()) lua_pushboolean(L, false);
	else lua_pushboolean(L, true);
	return 1;
}

//////////////////////////////////////////////////////////////////////////
// Macros for declaration of getter method
#define GETTER_METHOD_DECL(FIELD)                                    \
	template <typename T>                                            \
	int getter_##FIELD(lua_State* L)                                 \
	{                                                                \
		CUData* ud = check_userdata<T>(L, 1);                        \
		if (!ud->getUser())                                          \
			luaL_error(L, "Object destroyed");                       \
		return pushToLua(L, static_cast<T*>(ud->getUser())->FIELD);  \
	}

// Macros for getting the pointer to getter method
#define GETTER_METHOD(ID, FIELD)                          \
	&getter_##FIELD<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETTER_METHOD_ENTRY(ID, FIELD)    \
	{ #FIELD, GETTER_METHOD(ID, FIELD) }
//////////////////////////////////////////////////////////////////////////

// Macros for declaration of getter method with specified NAME
#define GETTER_NAMED_METHOD_DECL(NAME, FIELD)                        \
	template <typename T>                                            \
	int getter_##NAME(lua_State* L)                                  \
	{                                                                \
		CUData* ud = check_userdata<T>(L, 1);                        \
		if (!ud->getUser())                                          \
			luaL_error(L, "Object destroyed");                       \
		return pushToLua(L, static_cast<T*>(ud->getUser())->FIELD);  \
	}

// Macros for declaration of getter method with specified NAME
// If the CHECK fail, then getter will throw lua error.
#define GETTER_NAMED_CHECKED_METHOD_DECL(NAME, CHECK, FIELD)     \
	template <typename T>                                        \
	int getter_##NAME(lua_State* L)                              \
	{                                                            \
		CUData* ud = check_userdata<T>(L, 1);                    \
		T* obj = static_cast<T*>(ud->getUser());                 \
		if (!obj)                                                \
			luaL_error(L, "Object destroyed");                   \
		if (!(CHECK))                                            \
			luaL_error(L, "Check not passed");                   \
		return pushToLua(L, obj->FIELD);                         \
	}

// Macros for declaration of getter method with specified NAME
// If the CHECK fail, then getter will return nil.
#define GETTER_NAMED_CHECKED_NIL_METHOD_DECL(NAME, CHECK, FIELD) \
	template <typename T>                                        \
	int getter_##NAME(lua_State* L)                              \
	{                                                            \
		CUData* ud = check_userdata<T>(L, 1);                    \
		T* obj = static_cast<T*>(ud->getUser());                 \
		if (!obj)                                                \
			luaL_error(L, "Object destroyed");                   \
		if (!(CHECK))                                            \
		{                                                        \
			lua_pushnil(L);                                      \
			return 1;                                            \
		}                                                        \
		return pushToLua(L, obj->FIELD);                         \
	}


// Macros for getting the pointer to getter method with specified NAME
#define GETTER_NAMED_METHOD(ID, NAME)                         \
	&getter_##NAME<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETTER_NAMED_METHOD_ENTRY(ID, NAME)    \
	{ #NAME, GETTER_NAMED_METHOD(ID, NAME) }
//////////////////////////////////////////////////////////////////////////

// Macros for declaration of getset method
#define GETSET_METHOD_DECL(FIELD)                                                               \
	template <typename T>                                                                       \
	int getset_##FIELD(lua_State* L)                                                            \
	{                                                                                           \
		CUData* ud = check_userdata<T>(L, 1);                                                   \
		if (!ud->getUser())                                                                     \
			luaL_error(L, "Object destroyed");                                                  \
		T* obj = static_cast<T*>(ud->getUser());                                                \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                   \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");                 \
		if (typecheck)                                                                          \
			getFromLua(L, 2, obj->FIELD);                                                       \
		return pushToLua(L, obj->FIELD);                                                        \
	}

// Macros for getting the pointer to getset method
#define GETSET_METHOD(ID, FIELD)                          \
	&getset_##FIELD<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETSET_METHOD_ENTRY(ID, FIELD)    \
	{ #FIELD, GETSET_METHOD(ID, FIELD) }
//////////////////////////////////////////////////////////////////////////

// Macros for declaration of getset method with specified NAME
#define GETSET_NAMED_METHOD_DECL(NAME, FIELD)                                                    \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                    \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");   \
		if (typecheck)                                                                           \
			getFromLua(L, 2, obj->FIELD);                                                        \
		return pushToLua(L, obj->FIELD);                                                         \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will throw lua error.
// TYPECHECK is function like lua_isnumber.
#define GETSET_NAMED_CHECKED_METHOD_DECL(NAME, CHECK, FIELD)                                     \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
			luaL_error(L, "Check not passed");                                                   \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                    \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");                  \
		if (typecheck)                                                                           \
			getFromLua(L, 2, obj->FIELD);                                                        \
		return pushToLua(L, obj->FIELD);                                                         \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will return nil.
#define GETSET_NAMED_CHECKED_NIL_METHOD_DECL(NAME, CHECK, FIELD)                                 \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
		{                                                                                        \
			lua_pushnil(L);                                                                      \
			return 1;                                                                            \
		}                                                                                        \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                                   \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");   \
		if (typecheck)                                                                           \
			getFromLua(L, 2, obj->FIELD);                                                        \
		return pushToLua(L, obj->FIELD);                                                         \
	}

// Macros for declaration of getset method fro flag with specified NAME
// FLAG is flagname: in IsDead, the FLAG will be Dead
#define GETSET_FLAG_NAMED_METHOD_DECL(NAME, FLAG)                                                \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		bool typecheck = lua_isboolean(L, 2) != 0;                                               \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck lua_isboolean failed");    \
		if (typecheck)                                                                           \
			getFromLua<bool>(L, 2) ? obj->Set##FLAG() : obj->Clear##FLAG();                      \
		return pushToLua(L, obj->Is##FLAG());                                                    \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will throw lua error.
// In obj->sprite->IsVisible(), the OBJ will be sprite
// FLAG is flagname: in IsVisible, the FLAG will be Visible
#define GETSET_FLAG_NAMED_CHECKED_METHOD_DECL(NAME, CHECK, OBJ, FLAG)                          \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
			luaL_error(L, "Check not passed");                                                   \
		bool typecheck = lua_isboolean(L, 2) != 0;                                               \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck lua_isboolean failed");    \
		if (typecheck)                                                                           \
			getFromLua<bool>(L, 2) ? obj->OBJ->Set##FLAG() : obj->OBJ->Clear##FLAG();            \
		return pushToLua(L, obj->OBJ->Is##FLAG());                                               \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will return nil.
// In obj->sprite->IsVisible(), the OBJ will be sprite
// FLAG is flagname: in IsVisible, the FLAG will be Visible
#define GETSET_FLAG_NAMED_CHECKED_NIL_METHOD_DECL(NAME, CHECK, OBJ, FLAG)                        \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
		{                                                                                        \
			lua_pushnil(L);                                                                      \
			return 1;                                                                            \
		}                                                                                        \
		bool typecheck = lua_isboolean(L, 2) != 0;                                               \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck lua_isboolean failed");    \
		if (typecheck)                                                                           \
			getFromLua<bool>(L, 2) ? obj->OBJ->Set##FLAG() : obj->OBJ->Clear##FLAG();            \
		return pushToLua(L, obj->OBJ->Is##FLAG());                                               \
	}

// Macros for getting the pointer to getter method with specified NAME
#define GETSET_NAMED_METHOD(ID, NAME)                         \
	&getset_##NAME<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETSET_NAMED_METHOD_ENTRY(ID, NAME)    \
	{ #NAME, GETSET_NAMED_METHOD(ID, NAME) }
//////////////////////////////////////////////////////////////////////////

// Standard methods for metatble
#define STD_METHODS(i)                                                   \
	{ "__gc",       &on_gc<TL::TypeAt<ClassesList, i>::Result> },        \
	{ "__tostring", &on_tostring<TL::TypeAt<ClassesList, i>::Result> },   \
	{ "object_present", &object_present<TL::TypeAt<ClassesList, i>::Result> }

// Sentinel
#define END \
	{ NULL, NULL }

// The block 4 is used to declare all the methods of metatables and the arrays for metatable registrations
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 4
#include "userdata_binding.hpp"


// Registers metatable for type T
template <typename T>
void RegisterTypeMetatable(lua_State* L)
{
	STACK_CHECK_INIT(L);

	lua_newtable(L);                        // st: mt
	lua_pushstring(L, "__index");           // st: mt __index
	lua_pushvalue(L, -2);                   // st: mt __index mt
	lua_settable(L, -3);     /* metatable.__index = metatable */ 
	// st: mt

	luaL_register(L, NULL, ud_meta[UD_TYPE_ID(T)]);
	UD_META_REF(T) = SCRIPT::AddToRegistry();
	// st: 
	
	STACK_CHECK(L);
}

// The block 3 is used to register all the metatables using RegisterTypeMetatable function templates
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 3
#include "userdata_binding.hpp"

