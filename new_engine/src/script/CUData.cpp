#include "StdAfx.h"

#include "CUData.hpp"

#include "CUDataUser.hpp"

#include "script.h"

void CUData::onGC()
{
	if (pUser)
	{
		pUser->setUData(NULL);
		pUser = NULL;
	}
}

void CUData::clearUser()
{
	pUser = NULL;
	SCRIPT::RemoveFromRegistry(regRef);
}