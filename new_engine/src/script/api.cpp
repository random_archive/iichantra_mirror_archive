#include "StdAfx.h"

#include "api.h"
#include "timerevent.h"

#include "../config.h"
#include "../input_mgr.h"
#include "../misc.h"
#include "../resource_mgr.h"
#include "../scene.h"

#include "../game/camera.h"
#include "../game/editor.h"
#include "../game/game.h"
#include "../game/highscores.h"
#include "../game/net.h"
#include "../game/object_mgr.h"
#include "../game/player.h"
#include "../game/proto.h"
#include "../game/ribbon.h"

#include "../game/objects/object_enemy.h"
#include "../game/objects/object_spawner.h"

#include "../gui/gui.h"

#include "../render/texture.h"
#include "../render/font.h"

#include "../sound/snd.h"

#ifdef WIN32
	#include "../dirent/dirent.h"
#else
	#include "dirent.h"
#endif //WIN32

//////////////////////////////////////////////////////////////////////////

// scene.cpp
extern config cfg;
extern ProgrammStates current_state;
extern InputMgr inpmgr;

// misc.cpp
extern char path_home[MAX_PATH];
extern char path_app[MAX_PATH];
extern char path_config[MAX_PATH];
extern char path_textures[MAX_PATH];
extern char path_protos[MAX_PATH];
extern char path_levels[MAX_PATH];
extern char path_scripts[MAX_PATH];
extern char path_sounds[MAX_PATH];
extern char path_fonts[MAX_PATH];
extern char path_log[MAX_PATH];
extern char path_data[MAX_PATH];
extern char path_saves[MAX_PATH];


// camera.cpp
extern CameraFocusObjectPoint CAMERA_FOCUS_ON_OBJ_POS;
extern float CAMERA_X;
extern float CAMERA_Y;

// resource_mgr.cpp
extern ResourceMgr<Proto> * protoMgr;
extern ResourceMgr<Texture> * textureMgr;
extern ResourceMgr<TexFrames> * texFramesMgr;
extern SoundMgr* soundMgr;

//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void scriptApi::RegisterAPI(lua_State* L)
{
	// ������ �������� package.path, ����� ������� require ������ ���� � ����� ��������
	lua_getglobal(L, "package");		// ����: package
	char* new_path = new char[strlen(path_scripts) + 1+6];
	memset(new_path,'\0', strlen(path_scripts) + 1+6);
	sprintf(new_path, "%s\?.lua", path_scripts);
	lua_pushstring(L, new_path);		// ����: package new_path
	lua_setfield(L, -2, "path");		// ����: package
	lua_pop(L, 1);						// ����:
	DELETEARRAY(new_path);

	// ����������� �-�������
	lua_register(L, "LoadDefaultConfig", &scriptApi::LoadDefaultConfig);
	lua_register(L, "LoadConfig", &scriptApi::LoadConfig);
	lua_register(L, "SaveConfig", &scriptApi::SaveConfig);
	lua_register(L, "LoadParticlesConfig", &scriptApi::LoadParticlesConfig);
	lua_register(L, "LoadTexture", &scriptApi::LoadTexture);
	lua_register(L, "ChangeTexFrames", &scriptApi::ChangeTexFrames);
	lua_register(L, "DumpTexFrames", &scriptApi::DumpTexFrames);
	lua_register(L, "LoadFont", &scriptApi::LoadFont);
	lua_register(L, "LoadPrototype", &scriptApi::LoadPrototype);
	lua_register(L, "LoadPrototypeTable", &scriptApi::LoadPrototypeTable);
	lua_register(L, "InitNewGame", &scriptApi::InitNewGame);
	lua_register(L, "DestroyGame", &scriptApi::DestroyGame);
	lua_register(L, "AddTimerEvent", &scriptApi::AddTimerEvent);

	lua_register(L, "RegisterArchive", &scriptApi::RegisterArchive);
	lua_register(L, "UnregisterArchive", &scriptApi::UnregisterArchive);

	lua_register(L, "RegisterFunction", &scriptApi::RegisterFunction);

	lua_register(L, "Log", &scriptApi::Log);
	lua_register(L, "GetCurTime", &scriptApi::GetCurTime);

	lua_register(L, "GetNearestWaypoint", &scriptApi::GetNearestWaypoint);

	lua_register(L, "SetCamLag", &scriptApi::SetCamLag);
	lua_register(L, "SetCamAttachedObj", &scriptApi::SetCamAttachedObj);
	lua_register(L, "CamMoveToPos", &scriptApi::CamMoveToPos);
	lua_register(L, "SetCamFocusOnObjPos", &scriptApi::SetCamFocusOnObjPos);
	lua_register(L, "SetCamObjOffset", &scriptApi::SetCamObjOffset);
	lua_register(L, "SetCamAttachedAxis", &scriptApi::SetCamAttachedAxis);
	lua_register(L, "SetCamUseBounds", &scriptApi::SetCamUseBounds);
	lua_register(L, "SetCamBounds", &scriptApi::SetCamBounds);
	lua_register(L, "GetCamPos", &scriptApi::GetCamPos);
	lua_register(L, "GetCamFocusShift", &scriptApi::GetCamFocusShift);
	lua_register(L, "GetMousePos", &scriptApi::GetMousePos);
	lua_register(L, "SetCamScale", &scriptApi::SetCamScale);
	lua_register(L, "SetCamAngle", &scriptApi::SetCamAngle);

	lua_register(L, "UpdateWindowCaption", &scriptApi::UpdateWindowCaption);

	lua_register(L, "CreateRibbon", &scriptApi::CreateRibbon);
	lua_register(L, "SetRibbonBounds", &scriptApi::SetRibbonBounds);
	lua_register(L, "SetRibbonAttatachToY", &scriptApi::SetRibbonAttatachToY);
	lua_register(L, "SetRibbonZ", &scriptApi::SetRibbonZ);
	lua_register(L, "CreateRibbonObj", &scriptApi::CreateRibbonObj);
	lua_register(L, "SetRibbonObjBounds", &scriptApi::SetRibbonObjBounds);
	lua_register(L, "SetRibbonObjK", &scriptApi::SetRibbonObjK);
	lua_register(L, "SetRibbonObjRepitition", &scriptApi::SetRibbonObjRepitition);

	lua_register(L, "ListDirContents", &scriptApi::ListDirContents);

	lua_register(L, "CreatePlayer", &scriptApi::CreatePlayer);
	lua_register(L, "CreatePlayerZ", &scriptApi::CreatePlayer);

	lua_register(L, "CreateWaypoint", &scriptApi::CreateWaypoint);
	lua_register(L, "CreateSpecialWaypoint", &scriptApi::CreateSpecialWaypoint);
	lua_register(L, "SetNextWaypoint", &scriptApi::SetNextWaypoint);
	lua_register(L, "SetEnemyWaypoint", &scriptApi::SetEnemyWaypoint);
	lua_register(L, "GetWaypoint", &scriptApi::GetWaypoint);
	lua_register(L, "GetNearestEnemy", &scriptApi::GetNearestEnemy);

	lua_register(L, "RevivePlayer", &scriptApi::RevivePlayer);

	lua_register(L, "CreateParticleSystem", scriptApi::CreateParticleSystem);
	lua_register(L, "AddParticleArea", scriptApi::AddParticleArea);
	lua_register(L, "SetWind", scriptApi::SetParticleWind);
	lua_register(L, "GetWind", scriptApi::GetParticleWind);

	lua_register(L, "SwitchLighting", scriptApi::SwitchLighting);

	lua_register(L, "CreateMap", &scriptApi::CreateMap);
	lua_register(L, "CreateColorBox", &scriptApi::CreateColorBox);
	lua_register(L, "CreateSprite", &scriptApi::CreateSprite);
	lua_register(L, "CreateEffect", &scriptApi::CreateEffect);
	lua_register(L, "CreateGroup", &scriptApi::CreateGroup);
	lua_register(L, "CreateSpriteZ", &scriptApi::CreateSprite);
	lua_register(L, "CreateSpriteF", &scriptApi::CreateSprite);
	lua_register(L, "CreateSpriteZF", &scriptApi::CreateSprite);
	lua_register(L, "CreateEnemy", &scriptApi::CreateEnemy);
	lua_register(L, "CreateSpawner", &scriptApi::CreateSpawner);
	lua_register(L, "CreateItem", &scriptApi::CreateItem);
	lua_register(L, "CreateEnvironment", &scriptApi::CreateEnvironment);
	lua_register(L, "CreateRay", &scriptApi::CreateRay);
	lua_register(L, "CreateBullet", &scriptApi::CreateBullet);
	lua_register(L, "SetDefaultEnvironment", &scriptApi::setDefaultEnvironment);

	lua_register(L, "CreateDummySprite", scriptApi::CreateDummySprite);
	lua_register(L, "CreateDummyRay", scriptApi::CreateDummyRay);
	lua_register(L, "CreateDummyRibbon", scriptApi::CreateDummyRibbon);
	lua_register(L, "CreateDummySpawner", scriptApi::CreateDummySpawner);
	lua_register(L, "CreateDummyWaypoint", scriptApi::CreateDummyWaypoint);
	lua_register(L, "CreateDummyEnvironment", scriptApi::CreateDummyEnvironment);
	lua_register(L, "CreateDummyBullet", scriptApi::CreateDummyBullet);
	lua_register(L, "CreateDummyPlayer", scriptApi::CreateDummyPlayer);
	lua_register(L, "CreateDummyEnemy", scriptApi::CreateDummyEnemy);
	lua_register(L, "CreateDummyEffect", scriptApi::CreateDummyEffect);
	lua_register(L, "CreateDummyItem", scriptApi::CreateDummyItem);

	lua_register(L, "SetObjAnim", &scriptApi::SetObjAnim);
	lua_register(L, "SetObjSolidTo", &scriptApi::SetObjSolidTo);
	lua_register(L, "SetObjGhostTo", &scriptApi::SetObjGhostTo);
	lua_register(L, "SetObjSolidToByte", &scriptApi::SetObjSolidToByte);
	lua_register(L, "SetObjGhostToByte", &scriptApi::SetObjGhostToByte);

	lua_register(L, "GetPlayer", &scriptApi::GetPlayer);
	lua_register(L, "GetPlayerCharacter", &scriptApi::GetPlayerCharacter);
	lua_register(L, "GetPlayerNum", &scriptApi::GetPlayerNum);
	lua_register(L, "GetObject", &scriptApi::GetObject);
	lua_register(L, "GetChildren", &scriptApi::GetChildren);
	lua_register(L, "GetParent", &scriptApi::GetParent);
	lua_register(L, "GetMP", &scriptApi::GetMP);
	lua_register(L, "GetObjectUserdata", &scriptApi::GetObjectUserdata);

	lua_register(L, "GetCharHealth", &scriptApi::GetCharHealth);
	lua_register(L, "SetCharHealth", &scriptApi::SetCharHealth);
	
	lua_register(L, "ObjectOnScreen", &scriptApi::ObjectOnScreen);
	
	lua_register(L, "GroupObjects", &scriptApi::GroupObjects);

	lua_register(L, "SetObjDead", &scriptApi::SetObjDead);
	lua_register(L, "SetObjSleep", &scriptApi::SetObjSleep);
	lua_register(L, "SetObjPos", &scriptApi::SetObjPos);
	lua_register(L, "SetObjSpriteRenderMethod", &scriptApi::SetObjSpriteRenderMethod);
	lua_register(L, "SetObjSpriteBlendingMode", &scriptApi::SetObjSpriteBlendingMode);
	lua_register(L, "SetObjPolygon", &scriptApi::SetObjPolygon);
	lua_register(L, "SetObjRectangle", &scriptApi::SetObjRectangle);
	lua_register(L, "SetDynObjAcc", &scriptApi::SetDynObjAcc);
	lua_register(L, "SetDynObjGravity", &scriptApi::SetDynObjGravity);
	lua_register(L, "SetDynObjVel", &scriptApi::SetDynObjVel);
	lua_register(L, "SetDynObjJumpVel", &scriptApi::SetDynObjJumpVel);
	lua_register(L, "SetDynObjShadow", &scriptApi::SetDynObjShadow);
	lua_register(L, "GetDynObjShadow", &scriptApi::GetDynObjShadow);
	lua_register(L, "EnablePlayerControl", &scriptApi::EnablePlayerControl);
	lua_register(L, "GetPlayerControlState", &scriptApi::GetPlayerControlState);
	lua_register(L, "SetDynObjMovement", &scriptApi::SetDynObjMovement);
	lua_register(L, "SetDynObjMoveDirX", &scriptApi::SetDynObjMoveDirX);
	lua_register(L, "SetObjSpriteMirrored", &scriptApi::SetObjSpriteMirrored);
	lua_register(L, "SetObjSpriteAngle", &scriptApi::SetObjSpriteAngle);
	lua_register(L, "SetObjSpriteColor", &scriptApi::SetObjSpriteColor);
	lua_register(L, "SetObjAnim", &scriptApi::SetObjAnim);
	lua_register(L, "SetRayPos", &scriptApi::SetRayPos);
	lua_register(L, "SetRaySearchDistance", &scriptApi::SetRaySearchDistance);
	lua_register(L, "SpawnerLaunch", &scriptApi::SpawnerLaunch);
	lua_register(L, "AddBonusHealth", &scriptApi::AddBonusHealth);
	lua_register(L, "AddBonusAmmo", &scriptApi::AddBonusAmmo);
	lua_register(L, "AddBonusSpeed", &scriptApi::AddBonusSpeed);

	lua_register(L, "ApplyProto", &scriptApi::ApplyProto);	

	lua_register(L, "SetObjProcessor", &scriptApi::SetObjProcessor);
	lua_register(L, "GetObjProcessor", &scriptApi::GetObjProcessor);
	
	lua_register(L, "SetPhysObjBorderColor", &scriptApi::SetPhysObjBorderColor);

	lua_register(L, "DamageObject", &scriptApi::DamageObject);
	lua_register(L, "SetPlayerStats", &scriptApi::SetPlayerStats);
	lua_register(L, "ReplacePrimaryWeapon", &scriptApi::ReplacePrimaryWeapon);
	lua_register(L, "SwitchCharacter", &scriptApi::SwitchPlayer);
	lua_register(L, "SwitchWeapon", &scriptApi::SwitchWeapon);
	lua_register(L, "SetCustomWeapon", &scriptApi::SetCustomWeapon);
	lua_register(L, "SetCustomBullet", &scriptApi::SetCustomBullet);
	lua_register(L, "SetEnvironmentStats", &scriptApi::SetEnvironmentStats);
	lua_register(L, "SetPlayerHealth", &scriptApi::SetPlayerHealth);
	lua_register(L, "SetObjInvincible", &scriptApi::SetObjectInvincible);
	lua_register(L, "SetObjInvisible", &scriptApi::SetObjectInvisible);
	lua_register(L, "SetPlayerRecoveryTime", &scriptApi::SetPlayerRecoveryTime);
	lua_register(L, "SetPlayerAmmo", &scriptApi::SetPlayerAmmo);
	lua_register(L, "SetPlayerAltWeapon", &scriptApi::SetPlayerAltWeapon);
	lua_register(L, "SetPlayerRevivePoint", &scriptApi::SetPlayerRevivePoint);
	lua_register(L, "SetOnChangePlayerProcessor", &scriptApi::SetOnChangePlayerProcessor);
	lua_register(L, "SetOnPlayerDeathProcessor", &scriptApi::SetOnPlayerDeathProcessor);
	lua_register(L, "BlockPlayerChange", &scriptApi::BlockPlayerChange);
	lua_register(L, "UnblockPlayerChange", &scriptApi::UnblockPlayerChange);
	lua_register(L, "CreateActor", &scriptApi::CreateActor);
	lua_register(L, "DestroyActor", &scriptApi::DestroyActor);
	lua_register(L, "GetActorCount", &scriptApi::GetActorCount);
	lua_register(L, "GetDefaultActor", &scriptApi::GetDefaultActor);
	lua_register(L, "SetDefaultActor", &scriptApi::SetDefaultActor);	

	lua_register(L, "NewThread", &scriptApi::NewThread);
	lua_register(L, "Wait", &scriptApi::Wait);
	lua_register(L, "Resume", &scriptApi::Resume);
	lua_register(L, "StopThread", &scriptApi::StopThread);
	lua_register(L, "StopAllThreads", &scriptApi::StopAllThreads);

	lua_register(L, "CreateWidget", &scriptApi::CreateWidget);
	lua_register(L, "DestroyWidget", &scriptApi::DestroyWidget);
	lua_register(L, "WidgetSetMaxTextfieldSize", &scriptApi::WidgetSetMaxTextfieldSize);
	lua_register(L, "WidgetSetParent", &scriptApi::WidgetSetParent);
	lua_register(L, "WidgetSetCaption", &scriptApi::WidgetSetCaption);
	lua_register(L, "WidgetGetCaption", &scriptApi::WidgetGetCaption);
	lua_register(L, "WidgetSetCaptionColor", &scriptApi::WidgetSetCaptionColor);
	lua_register(L, "WidgetSetCaptionFont", &scriptApi::WidgetSetCaptionFont);
	lua_register(L, "WidgetSetCaptionScroll", &scriptApi::WidgetSetCaptionScroll);
	lua_register(L, "GetCaptionSize", &scriptApi::GetCaptionSize);
	lua_register(L, "GetMultilineCaptionSize", &scriptApi::GetMultilineCaptionSize);
	lua_register(L, "WidgetUseTyper", &scriptApi::WidgetUseTyper);
	lua_register(L, "WidgetStartTyper", &scriptApi::WidgetStartTyper);
	lua_register(L, "WidgetStopTyper", &scriptApi::WidgetStopTyper);
	lua_register(L, "WidgetSetOnTyperEndedProc", &scriptApi::WidgetSetOnTyperEndedProc);
	lua_register(L, "WidgetSetVisible", &scriptApi::WidgetSetVisible);
	lua_register(L, "WidgetGetVisible", &scriptApi::WidgetGetVisible);
	lua_register(L, "WidgetSetFixedPosition", &scriptApi::WidgetSetFixedPosition);
	lua_register(L, "WidgetSetFocusable", &scriptApi::WidgetSetFocusable);
	lua_register(L, "WidgetSetActive", &scriptApi::WidgetSetActive);
	lua_register(L, "WidgetSetBorder", &scriptApi::WidgetSetBorder);
	lua_register(L, "WidgetSetBorderColor", &scriptApi::WidgetSetBorderColor);
	lua_register(L, "WidgetSetLMouseClickProc", &scriptApi::WidgetSetLMouseClickProc);
	lua_register(L, "WidgetSetRMouseClickProc", &scriptApi::WidgetSetRMouseClickProc);
	lua_register(L, "WidgetSetMouseEnterProc", &scriptApi::WidgetSetMouseEnterProc);
	lua_register(L, "WidgetSetMouseLeaveProc", &scriptApi::WidgetSetMouseLeaveProc);
	lua_register(L, "WidgetSetKeyDownProc", &scriptApi::WidgetSetKeyDownProc);
	lua_register(L, "WidgetSetKeyInputProc", &scriptApi::WidgetSetKeyInputProc);
	lua_register(L, "WidgetSetKeyPressProc", &scriptApi::WidgetSetKeyPressProc);
	lua_register(L, "WidgetSetFocusProc", &scriptApi::WidgetSetFocusProc);
	lua_register(L, "WidgetSetUnFocusProc", &scriptApi::WidgetSetUnFocusProc);
	lua_register(L, "WidgetSetResizeProc", &scriptApi::WidgetSetResizeProc);	
	lua_register(L, "WidgetSetSize", &scriptApi::WidgetSetSize);
	lua_register(L, "WidgetSetPos", &scriptApi::WidgetSetPos);
	lua_register(L, "WidgetSetZ", &scriptApi::WidgetSetZ);
	lua_register(L, "WidgetGainFocus", &scriptApi::WidgetGainFocus);
	lua_register(L, "WidgetBringToFront", &scriptApi::WidgetBringToFront);
	lua_register(L, "PushWidget", &scriptApi::PushWidget);
	lua_register(L, "GetFocusLock", &scriptApi::GetFocusLock);
	lua_register(L, "WidgetSetSprite", &scriptApi::WidgetSetSprite);
	lua_register(L, "WidgetSetSpriteAngle", &scriptApi::WidgetSetSpriteAngle);
	lua_register(L, "WidgetSetSpriteColor", &scriptApi::WidgetSetSpriteColor);
	lua_register(L, "WidgetSetColorBox", &scriptApi::WidgetSetColorBox);
	lua_register(L, "WidgetSetSpriteRenderMethod", &scriptApi::WidgetSetSpriteRenderMethod);
	lua_register(L, "WidgetSetSpriteBlendingMode", &scriptApi::WidgetSetSpriteBlendingMode);
	lua_register(L, "WidgetSetAnim", &scriptApi::WidgetSetAnim);
	lua_register(L, "GetWidgetName", &scriptApi::WidgetGetName);
	lua_register(L, "WidgetGetSize", &scriptApi::WidgetGetSize);
	lua_register(L, "WidgetGetPos", &scriptApi::WidgetGetPos);
	lua_register(L, "ObjectPushInt", &scriptApi::PushInt);
	lua_register(L, "ObjectPopInt", &scriptApi::PopInt);

	lua_register(L, "GlobalSetKeyDownProc", &scriptApi::GlobalSetKeyDownProc);
	lua_register(L, "GlobalSetKeyReleaseProc", &scriptApi::GlobalSetKeyReleaseProc);
	lua_register(L, "GlobalGetKeyDownProc", &scriptApi::GlobalGetKeyDownProc);
	lua_register(L, "GlobalGetKeyReleaseProc", &scriptApi::GlobalGetKeyReleaseProc);
	lua_register(L, "GlobalSetMouseKeyDownProc", &scriptApi::GlobalSetMouseKeyDownProc);
	lua_register(L, "GlobalSetMouseKeyReleaseProc", &scriptApi::GlobalSetMouseKeyReleaseProc);
	lua_register(L, "GlobalGetMouseKeyDownProc", &scriptApi::GlobalGetMouseKeyDownProc);
	lua_register(L, "GlobalGetMouseKeyReleaseProc", &scriptApi::GlobalGetMouseKeyReleaseProc);

	lua_register(L, "LoadSound", &scriptApi::LoadSound);
	lua_register(L, "PlaySnd", &scriptApi::PlaySnd);
	lua_register(L, "PauseSnd", &scriptApi::PauseSnd);
	lua_register(L, "StopSnd", &scriptApi::StopSnd);
	lua_register(L, "StopAllSnd", &scriptApi::StopAllSnd);
	lua_register(L, "GetMasterSoundVolume", &scriptApi::GetMasterSoundVolume);
	lua_register(L, "SetMasterSoundVolume", &scriptApi::SetMasterSoundVolume);
	lua_register(L, "GetMasterSFXVolume", &scriptApi::GetMasterSFXVolume);
	lua_register(L, "SetMasterSFXVolume", &scriptApi::SetMasterSFXVolume);
	lua_register(L, "GetMasterMusicVolume", &scriptApi::GetMasterMusicVolume);
	lua_register(L, "SetMasterMusicVolume", &scriptApi::SetMasterMusicVolume);
	lua_register(L, "GetSoundVolume", &scriptApi::GetSoundVolume);
	lua_register(L, "SetSoundVolume", &scriptApi::SetSoundVolume);
	lua_register(L, "PlayBackMusic", &scriptApi::PlayBackMusic);
	lua_register(L, "GetBackMusic", &scriptApi::GetBackMusic);
	lua_register(L, "PauseBackMusic", &scriptApi::PauseBackMusic);
	lua_register(L, "ResumeBackMusic", &scriptApi::ResumeBackMusic);
	lua_register(L, "StopBackMusic", &scriptApi::StopBackMusic);
	lua_register(L, "GetBackMusicLevel", &scriptApi::GetBackMusicLevel);
	lua_register(L, "GetBackMusicFFT", &scriptApi::GetBackMusicFFT);
	lua_register(L, "GetBackMusicLength", &scriptApi::GetBackMusicLength);
	lua_register(L, "GetSndLength", &scriptApi::GetSndLength);
	lua_register(L, "GetBackMusicSpeed", &scriptApi::GetBackMusicSpeed);
	lua_register(L, "SetBackMusicSpeed", &scriptApi::SetBackMusicSpeed);
	lua_register(L, "ExitGame", &scriptApi::ExitGame);

	lua_register(L, "SendHighscores", &scriptApi::SendHighscores);
	lua_register(L, "GetHighscores", &scriptApi::GetHighscores);
	lua_register(L, "LockScore", &scriptApi::LockScore);
	lua_register(L, "GetScoreLock", &scriptApi::GetScoreLock);
	lua_register(L, "ProtectedVariable", &scriptApi::ProtectedVariable);

	lua_register(L, "ButtonPressed", &scriptApi::PushButton);
	lua_register(L, "TogglePause", &scriptApi::TogglePause);
	lua_register(L, "GetKeysState", &scriptApi::DumpKeys);
	lua_register(L, "GetKeyName", &scriptApi::PushKeyName);
	lua_register(L, "GetFirstButtonPressed", &scriptApi::GetFirstButtonPressed);
	lua_register(L, "GamePaused", &scriptApi::GamePaused);
	lua_register(L, "IsConfKeyPressed", &scriptApi::IsConfKeyPressed);
	lua_register(L, "IsConfKeyHolded", &scriptApi::IsConfKeyHolded);
	lua_register(L, "IsConfKeyReleased", &scriptApi::IsConfKeyReleased);
	lua_register(L, "IsConfKey", &scriptApi::IsConfKey);
	lua_register(L, "EnableScreenshots", &scriptApi::EnableScreenshots);
#ifdef MAP_EDITOR
	lua_register(L, "EditorSetState", &scriptApi::EditorSetState);
	lua_register(L, "EditorSetLink", &scriptApi::EditorSetLink);
	lua_register(L, "EditorGetObjects", &scriptApi::EditorGetObjects);
	lua_register(L, "EditorDumpMap", &EditorDumpMap);
	lua_register(L, "EditorResize", &EditorResize);
	lua_register(L, "EditorCloneObject", &EditorCopyObject);
	lua_register(L, "EditorToggleBorders", &scriptApi::EditorToggleBorders);
	lua_register(L, "EditorSetGrid", &scriptApi::EditorSetGrid);
	lua_register(L, "RegEditorGetObjectsProc", &scriptApi::RegEditorGetObjectsProc);
#endif // MAP_EDITOR
	lua_register(L, "NetHost", &scriptApi::NetHost);
	lua_register(L, "NetConnect", &scriptApi::NetConnect);
	lua_register(L, "ChatMessage", &scriptApi::ChatMessage);

	lua_register(L, "need", &scriptApi::NeedArchiveFile);

	// �������������� dofile ��� ������ � ��������
	lua_getglobal(L, "dofile");
	lua_setglobal(L, "__dofile");
	lua_register(L, "doarchivefile", &scriptApi::DoArchiveFile);
	lua_getglobal(L, "doarchivefile");
	lua_setglobal(L, "dofile");

	lua_getglobal(L, "loadfile");
	lua_setglobal(L, "__loadfile");
	lua_register(L, "loadarchivefile", &scriptApi::LoadArchiveFile);
	lua_getglobal(L, "loadarchivefile");
	lua_setglobal(L, "loadfile");

	// ������������ ����������� �������� ��� ������.
	lua_newtable(L);			// ������� ����� �������. ����: �������
	{
		char ch[MAX_VKEY_NAME_LEN] = "";	// ����� ��� ��������


		for (UINT i = 1; i < inpmgr.keys_count; i++)		// ���� �� ���� ��������� �����
		{
			GetVKeyByNum(i, ch);
			lua_pushinteger(L,i);		// �������� � ���� ��� �������. ����: �������, i
			lua_setfield(L, -2, ch);	// ������� � ������� ��������� � ������ � ���������. ����: �������
		}
	}
	lua_setglobal(L, "keys");		// ������� ������� � ���������� ����������. ����:

	lua_newtable(L);			// ������� ����� �������. ����: �������
	{
		for (size_t i = 0; i < cakLastKey; i++)		// ���� �� ���� ��������� ������� � �������
		{
			lua_pushinteger(L, (int)i);
			lua_setfield(L, -2, GetConfigKeyName((ConfigActionKeys)i));
		}
	}
	lua_setglobal(L, "config_keys");		// ������� ������� � ���������� ����������. ����:

	// ��������� ���������� ���������
	lua_newtable(L);		// ����: �������
	{
		lua_pushinteger(L, protoNullBeh);		lua_setfield(L, -2, "NullBehaviour");
		lua_pushinteger(L, protoPlayer);		lua_setfield(L, -2, "PlayerBehaviour");
		lua_pushinteger(L, protoEnemy);			lua_setfield(L, -2, "EnemyBehaviour");
		lua_pushinteger(L, protoPowerup);		lua_setfield(L, -2, "PowerUpBehaviour");
		lua_pushinteger(L, protoSprite);		lua_setfield(L, -2, "SpriteBehaviour");

		// ��������� ��� SetCamFocusOnObjPos
		lua_pushinteger(L, CamFocusLeftBottomCorner);	lua_setfield(L, -2, "CamFocusLeftBottomCorner");
		lua_pushinteger(L, CamFocusLeftCenter);			lua_setfield(L, -2, "CamFocusLeftCenter");
		lua_pushinteger(L, CamFocusLeftTopCorner);		lua_setfield(L, -2, "CamFocusLeftTopCorner");
		lua_pushinteger(L, CamFocusRightBottomCorner);	lua_setfield(L, -2, "CamFocusRightBottomCorner");
		lua_pushinteger(L, CamFocusRightCenter);		lua_setfield(L, -2, "CamFocusRightCenter");
		lua_pushinteger(L, CamFocusRightTopCorner);		lua_setfield(L, -2, "CamFocusRightTopCorner");
		lua_pushinteger(L, CamFocusBottomCenter);		lua_setfield(L, -2, "CamFocusBottomCenter");
		lua_pushinteger(L, CamFocusCenter);				lua_setfield(L, -2, "CamFocusCenter");
		lua_pushinteger(L, CamFocusTopCenter);			lua_setfield(L, -2, "CamFocusTopCenter");

		// ��������� ��� CreateMap
		lua_pushinteger(L, otSprite);		lua_setfield(L, -2, "ObjSprite");
		lua_pushinteger(L, otPlayer);		lua_setfield(L, -2, "ObjPlayer");
		lua_pushinteger(L, otEnemy);		lua_setfield(L, -2, "ObjEnemy");
		lua_pushinteger(L, otBox);			lua_setfield(L, -2, "ObjBox");
		lua_pushinteger(L, otGroup);		lua_setfield(L, -2, "ObjGroup");
		lua_pushinteger(L, otItem);			lua_setfield(L, -2, "ObjItem");
		lua_pushinteger(L, otSecret);		lua_setfield(L, -2, "ObjSecret");
		lua_pushinteger(L, otTile);			lua_setfield(L, -2, "ObjTile");
		lua_pushinteger(L, otSpawner);		lua_setfield(L, -2, "ObjSpawner");
		lua_pushinteger(L, otRibbon);		lua_setfield(L, -2, "ObjRibbon");
		lua_pushinteger(L, otPolygon);		lua_setfield(L, -2, "ObjPolygon");
		lua_pushinteger(L, otNone);			lua_setfield(L, -2, "ObjNone");

		// ������� ��������
		lua_pushinteger(L, afcNone);					lua_setfield(L, -2, "AnimComNone");
		lua_pushinteger(L, afcBreakpoint);				lua_setfield(L, -2, "AnimComBreakpoint");
		lua_pushinteger(L, afcSetDamageMult);			lua_setfield(L, -2, "AnimComSetDamageMult");
		lua_pushinteger(L, afcSetTrajectory);			lua_setfield(L, -2, "AnimComSetTrajectory");
		lua_pushinteger(L, afcSetSolid);				lua_setfield(L, -2, "AnimComSetSolid");
		lua_pushinteger(L, afcSetSolidTo);				lua_setfield(L, -2, "AnimComSetSolidTo");
		lua_pushinteger(L, afcSetWaypoint);				lua_setfield(L, -2, "AnimComSetWaypoint");
		lua_pushinteger(L, afcPop);						lua_setfield(L, -2, "AnimComPop");
		lua_pushinteger(L, afcReloadTime);				lua_setfield(L, -2, "AnimComReloadTime");
		lua_pushinteger(L, afcSetShadow);				lua_setfield(L, -2, "AnimComSetShadow");
		lua_pushinteger(L, afcMirror);					lua_setfield(L, -2, "AnimComMirror");
		lua_pushinteger(L, afcReplaceWithRandomTile);	lua_setfield(L, -2, "AnimComReplaceWithRandomTile");
		lua_pushinteger(L, afcRandomAngledSpeed);		lua_setfield(L, -2, "AnimComRandomAngledSpeed");
		lua_pushinteger(L, afcSetShielding);			lua_setfield(L, -2, "AnimComSetShielding");
		lua_pushinteger(L, afcEnemyClean);				lua_setfield(L, -2, "AnimComEnemyClean");
		lua_pushinteger(L, afcStartDying);				lua_setfield(L, -2, "AnimComStartDying");
		lua_pushinteger(L, afcSetBulletCollidable);		lua_setfield(L, -2, "AnimComSetBulletCollidable");
		lua_pushinteger(L, afcJumpIfCloseToCamera);		lua_setfield(L, -2, "AnimComJumpIfCloseToCamera");
		lua_pushinteger(L, afcJumpIfCloseToCameraLeft);	lua_setfield(L, -2, "AnimComJumpIfCloseToCameraLeft");
		lua_pushinteger(L, afcJumpIfCloseToCameraRight);lua_setfield(L, -2, "AnimComJumpIfCloseToCameraRight");
		lua_pushinteger(L, afcJumpIfCloseToCameraUp);	lua_setfield(L, -2, "AnimComJumpIfCloseToCameraUp");
		lua_pushinteger(L, afcJumpIfCloseToCameraDown);	lua_setfield(L, -2, "AnimComJumpIfCloseToCameraDown");
		lua_pushinteger(L, afcJumpIfObjectExists);		lua_setfield(L, -2, "AnimComJumpIfObjectExists");
		lua_pushinteger(L, afcJumpIfXGreater);			lua_setfield(L, -2, "AnimComJumpIfXGreater");
		lua_pushinteger(L, afcJumpIfYGreater);			lua_setfield(L, -2, "AnimComJumpIfYGreater");
		lua_pushinteger(L, afcJumpIfWaypointClose);		lua_setfield(L, -2, "AnimComJumpIfWaypointClose");

		lua_pushinteger(L, afcSetNearestWaypoint);		lua_setfield(L, -2, "AnimComSetNearestWaypoint");
		lua_pushinteger(L, afcLocalJumpIfIntEquals);	lua_setfield(L, -2, "AnimComLocalJumpIfIntEquals");
		lua_pushinteger(L, afcLocalJumpIfNextIntEquals);lua_setfield(L, -2, "AnimComLocalJumpIfNextIntEquals");
		lua_pushinteger(L, afcJumpIfXLess);				lua_setfield(L, -2, "AnimComJumpIfXLess");
		lua_pushinteger(L, afcJumpIfYLess);				lua_setfield(L, -2, "AnimComJumpIfYLess");
		lua_pushinteger(L, afcJumpCheckFOV);			lua_setfield(L, -2, "AnimComJumpCheckFOV");
		lua_pushinteger(L, afcCreateEnemy);				lua_setfield(L, -2, "AnimComCreateEnemy");
		lua_pushinteger(L, afcSummonObject);			lua_setfield(L, -2, "AnimComSummonObject");
		lua_pushinteger(L, afcStopMorphing);			lua_setfield(L, -2, "AnimComStopMorphing");
		lua_pushinteger(L, afcJumpIfStackIsNotEmpty);	lua_setfield(L, -2, "AnimComJumpIfStackIsNotEmpty");
		lua_pushinteger(L, afcRandomOverlayColor);		lua_setfield(L, -2, "AnimComRandomOverlayColor");
		lua_pushinteger(L, afcControlledOverlayColor);	lua_setfield(L, -2, "AnimComControlledOverlayColor");
		lua_pushinteger(L, afcSetColor);				lua_setfield(L, -2, "AnimComSetColor");
		lua_pushinteger(L, afcRealX);					lua_setfield(L, -2, "AnimComRealX");
		lua_pushinteger(L, afcRealY);					lua_setfield(L, -2, "AnimComRealY");
		lua_pushinteger(L, afcRealW);					lua_setfield(L, -2, "AnimComRealW");
		lua_pushinteger(L, afcRealH);					lua_setfield(L, -2, "AnimComRealH");
		lua_pushinteger(L, afcInitW);					lua_setfield(L, -2, "AnimComInitW");
		lua_pushinteger(L, afcInitH);					lua_setfield(L, -2, "AnimComInitH");
		lua_pushinteger(L, afcInitWH);					lua_setfield(L, -2, "AnimComInitWH");
		lua_pushinteger(L, afcLoop);					lua_setfield(L, -2, "AnimComLoop");
		lua_pushinteger(L, afcDrop);					lua_setfield(L, -2, "AnimComDrop");
		lua_pushinteger(L, afcShootX);					lua_setfield(L, -2, "AnimComShootX");
		lua_pushinteger(L, afcShootY);					lua_setfield(L, -2, "AnimComShootY");
		lua_pushinteger(L, afcShootDir);				lua_setfield(L, -2, "AnimComShootDir");
		lua_pushinteger(L, afcShootBeh);				lua_setfield(L, -2, "AnimComShootBeh");
		lua_pushinteger(L, afcShoot);					lua_setfield(L, -2, "AnimComShoot");
		lua_pushinteger(L, afcCallAnim);				lua_setfield(L, -2, "AnimComCallAnim");
		lua_pushinteger(L, afcSetAnim);					lua_setfield(L, -2, "AnimComSetAnim");
		lua_pushinteger(L, afcSetAnimIfGunDirection);	lua_setfield(L, -2, "AnimComSetAnimIfGunDirection");
		lua_pushinteger(L, afcSetAnimOnTargetPos);		lua_setfield(L, -2, "AnimComSetAnimOnTargetPos");
		lua_pushinteger(L, afcSetAnimIfWeaponNotReady);	lua_setfield(L, -2, "AnimComSetAnimIfWeaponNotReady");
		lua_pushinteger(L, afcPushString);				lua_setfield(L, -2, "AnimComPushStr");
		lua_pushinteger(L, afcPushInt);					lua_setfield(L, -2, "AnimComPushInt");
		lua_pushinteger(L, afcPushRandomInt);			lua_setfield(L, -2, "AnimComPushRandomInt");
		lua_pushinteger(L, afcJump);					lua_setfield(L, -2, "AnimComJump");
		lua_pushinteger(L, afcJumpIfIntEquals);			lua_setfield(L, -2, "AnimComJumpIfIntEquals");
		lua_pushinteger(L, afcJumpIfYSpeedGreater);		lua_setfield(L, -2, "AnimComJumpIfYSpeedGreater");
		lua_pushinteger(L, afcJumpIfXSpeedGreater);		lua_setfield(L, -2, "AnimComJumpIfXSpeedGreater");
		lua_pushinteger(L, afcJumpIfOnPlane);			lua_setfield(L, -2, "AnimComJumpIfOnPlane");
		lua_pushinteger(L, afcJumpRandom);				lua_setfield(L, -2, "AnimComJumpRandom");
		lua_pushinteger(L, afcJumpIfPlayerId);			lua_setfield(L, -2, "AnimComJumpIfPlayerId");
		lua_pushinteger(L, afcRecover);					lua_setfield(L, -2, "AnimComRecover");
		lua_pushinteger(L, afcRemoveRecoveryState);		lua_setfield(L, -2, "AnimComRemoveRecoveryState");
		lua_pushinteger(L, afcCreateRecoveryState);		lua_setfield(L, -2, "AnimComCreateRecoveryState");
		lua_pushinteger(L, afcCreateEffect);			lua_setfield(L, -2, "AnimComCreateEffect");
		lua_pushinteger(L, afcCreateObject);			lua_setfield(L, -2, "AnimComCreateObject");
		lua_pushinteger(L, afcDestroyObject);			lua_setfield(L, -2, "AnimComDestroyObject");
		lua_pushinteger(L, afcCreateParticles);			lua_setfield(L, -2, "AnimComCreateParticles");
		lua_pushinteger(L, afcCreateItem);				lua_setfield(L, -2, "AnimComCreateItem");
		lua_pushinteger(L, afcJumpIfWeaponReady);		lua_setfield(L, -2, "AnimComJumpIfWeaponReady");
		lua_pushinteger(L, afcSetWeaponAngle);			lua_setfield(L, -2, "AnimComSetWeaponAngle");
		lua_pushinteger(L, afcMountPointSet);			lua_setfield(L, -2, "AnimComMPSet");
		lua_pushinteger(L, afcSetHealth);				lua_setfield(L, -2, "AnimComSetHealth");
		lua_pushinteger(L, afcSetGravity);				lua_setfield(L, -2, "AnimComSetGravity");
		lua_pushinteger(L, afcSetLifetime);				lua_setfield(L, -2, "AnimComSetLifetime");
		lua_pushinteger(L, afcWait);				lua_setfield(L, -2, "AnimComWait");
		lua_pushinteger(L, afcWaitForTarget);			lua_setfield(L, -2, "AnimComWaitForTarget");
		lua_pushinteger(L, afcClearTarget);				lua_setfield(L, -2, "AnimComClearTarget");
		lua_pushinteger(L, afcWaitForEnemy);			lua_setfield(L, -2, "AnimComWaitForEnemy");
		lua_pushinteger(L, afcFlyToWaypoint);			lua_setfield(L, -2, "AnimComFlyToWaypoint");
		lua_pushinteger(L, afcTeleportToWaypoint);		lua_setfield(L, -2, "AnimComTeleportToWaypoint");
		lua_pushinteger(L, afcMoveToTargetX);			lua_setfield(L, -2, "AnimComMoveToTargetX");
		lua_pushinteger(L, afcMoveToTargetY);			lua_setfield(L, -2, "AnimComMoveToTargetY");
		lua_pushinteger(L, afcMoveToTarget);			lua_setfield(L, -2, "AnimComMoveToTarget");
		lua_pushinteger(L, afcAdjustVelToTarget);		lua_setfield(L, -2, "AnimComAdjustVelToTarget");
		lua_pushinteger(L, afcJumpIfTargetX);			lua_setfield(L, -2, "AnimComJumpIfTargetX");
		lua_pushinteger(L, afcJumpIfTargetY);			lua_setfield(L, -2, "AnimComJumpIfTargetY");
		lua_pushinteger(L, afcJumpIfTargetClose);		lua_setfield(L, -2, "AnimComJumpIfTargetClose");
		lua_pushinteger(L, afcJumpIfTargetCloseByX);	lua_setfield(L, -2, "AnimComJumpIfTargetCloseByX");
		lua_pushinteger(L, afcJumpIfTargetCloseByY);	lua_setfield(L, -2, "AnimComJumpIfTargetCloseByY");
		lua_pushinteger(L, afcCreateEnemyRay);			lua_setfield(L, -2, "AnimComCreateEnemyRay");
		lua_pushinteger(L, afcCreateEnemyBullet);		lua_setfield(L, -2, "AnimComCreateEnemyBullet");
		lua_pushinteger(L, afcCustomBullet);			lua_setfield(L, -2, "AnimComCustomWeapon");
		lua_pushinteger(L, afcAdjustAim);				lua_setfield(L, -2, "AnimComAdjustAim");
		lua_pushinteger(L, afcAdjustHomingAcc);			lua_setfield(L, -2, "AnimComAdjustHomingAcc");
		lua_pushinteger(L, afcAimedShot);				lua_setfield(L, -2, "AnimComAimedShot");
		lua_pushinteger(L, afcAngledShot);				lua_setfield(L, -2, "AnimComAngledShot");
		lua_pushinteger(L, afcFaceTarget);				lua_setfield(L, -2, "AnimComFaceTarget");
		lua_pushinteger(L, afcPlaySound);				lua_setfield(L, -2, "AnimComPlaySound");
		lua_pushinteger(L, afcEnvSound);				lua_setfield(L, -2, "AnimComEnvSound");
		lua_pushinteger(L, afcMaterialSound);				lua_setfield(L, -2, "AnimComMaterialSound");
		lua_pushinteger(L, afcEnvSprite);				lua_setfield(L, -2, "AnimComEnvSprite");
		lua_pushinteger(L, afcMaterialSprite);				lua_setfield(L, -2, "AnimComMaterialSprite");
		lua_pushinteger(L, afcGiveHealth);				lua_setfield(L, -2, "AnimComGiveHealth");
		lua_pushinteger(L, afcGiveWeapon);				lua_setfield(L, -2, "AnimComGiveWeapon");
		lua_pushinteger(L, afcGiveAmmo);				lua_setfield(L, -2, "AnimComGiveAmmo");
		lua_pushinteger(L, afcDamage);					lua_setfield(L, -2, "AnimComDamage");
		lua_pushinteger(L, afcDealDamage);				lua_setfield(L, -2, "AnimComDealDamage");
		lua_pushinteger(L, afcReduceHealth);			lua_setfield(L, -2, "AnimComReduceHealth");
		lua_pushinteger(L, afcSetZ);					lua_setfield(L, -2, "AnimComSetZ");
		lua_pushinteger(L, afcSetInvincible);			lua_setfield(L, -2, "AnimComSetInvincible");
		lua_pushinteger(L, afcSetInvisible);			lua_setfield(L, -2, "AnimComSetInvisible");
		lua_pushinteger(L, afcMapVarFieldAdd);			lua_setfield(L, -2, "AnimComMapVarAdd");
		lua_pushinteger(L, afcSetAccY);					lua_setfield(L, -2, "AnimComSetAccY");
		lua_pushinteger(L, afcAdjustAccY);				lua_setfield(L, -2, "AnimComAdjustAccY");
		lua_pushinteger(L, afcSetVelY);					lua_setfield(L, -2, "AnimComSetVelY");
		lua_pushinteger(L, afcSetAccX);					lua_setfield(L, -2, "AnimComSetAccX");
		lua_pushinteger(L, afcSetRelativeVelX);			lua_setfield(L, -2, "AnimComSetRelativeVelX");
		lua_pushinteger(L, afcSetRelativeAccX);			lua_setfield(L, -2, "AnimComSetRelativeAccX");
		lua_pushinteger(L, afcSetRelativePos);			lua_setfield(L, -2, "AnimComSetRelativePos");
		lua_pushinteger(L, afcSetOwnVelX);					lua_setfield(L, -2, "AnimComSetOwnVelX");
		lua_pushinteger(L, afcSetOwnVelY);					lua_setfield(L, -2, "AnimComSetOwnVelY");
		lua_pushinteger(L, afcSetVelX);					lua_setfield(L, -2, "AnimComSetVelX");
		lua_pushinteger(L, afcAdjustX);					lua_setfield(L, -2, "AnimComAdjustX");
		lua_pushinteger(L, afcAdjustY);					lua_setfield(L, -2, "AnimComAdjustY");
		lua_pushinteger(L, afcStop);					lua_setfield(L, -2, "AnimComStop");
		lua_pushinteger(L, afcSetMaxVelX);				lua_setfield(L, -2, "AnimComSetMaxVelX");
		lua_pushinteger(L, afcSetMaxVelY);				lua_setfield(L, -2, "AnimComSetMaxVelY");
		lua_pushinteger(L, afcSetTouchable);			lua_setfield(L, -2, "AnimComSetTouchable");
		lua_pushinteger(L, afcJumpIfSquashCondition);	lua_setfield(L, -2, "AnimComJumpIfSquashCondition");
		lua_pushinteger(L, afcBounceObject);			lua_setfield(L, -2, "AnimComBounceObject");
		lua_pushinteger(L, afcPushObject);				lua_setfield(L, -2, "AnimComPushObject");
		lua_pushinteger(L, afcCallFunction);			lua_setfield(L, -2, "AnimComCallFunction");
		lua_pushinteger(L, afcSetProcessor);			lua_setfield(L, -2, "AnimComSetProcessor");
		lua_pushinteger(L, afcCallFunctionWithStackParameter);	lua_setfield(L, -2, "AnimComCallFunctionWithStackParameter");

		// ��������� ��� AnimComShootBeh
		lua_pushinteger(L, csbNoShooting);		lua_setfield(L, -2, "csbNoShooting");
		lua_pushinteger(L, csbFreeShooting);	lua_setfield(L, -2, "csbFreeShooting");
		lua_pushinteger(L, csbOnAnimCommand);	lua_setfield(L, -2, "csbOnAnimCommand");

		// ��������� ��� AnimComShootDir
		lua_pushinteger(L, cgdNone);			lua_setfield(L, -2, "cgdNone");
		lua_pushinteger(L, cgdUp);				lua_setfield(L, -2, "cgdUp");
		lua_pushinteger(L, cgdDown);			lua_setfield(L, -2, "cgdDown");

		// ��������� ��� CreateWidget
		lua_pushinteger(L, wt_Widget);			lua_setfield(L, -2, "wt_Widget");
		lua_pushinteger(L, wt_Button);			lua_setfield(L, -2, "wt_Button");
		lua_pushinteger(L, wt_Picture);			lua_setfield(L, -2, "wt_Picture");
		lua_pushinteger(L, wt_Label);			lua_setfield(L, -2, "wt_Label");
		lua_pushinteger(L, wt_Textfield);		lua_setfield(L, -2, "wt_Textfield");
		lua_pushinteger(L, wt_Panel);			lua_setfield(L, -2, "wt_Panel");

		// ������ ����������� �������
		lua_pushinteger(L, tdtAlways);			lua_setfield(L, -2, "tdtAlways");
		lua_pushinteger(L, tdtFromBottom);		lua_setfield(L, -2, "tdtFromBottom");
		lua_pushinteger(L, tdtFromEverywhere);	lua_setfield(L, -2, "tdtFromEverywhere");
		lua_pushinteger(L, tdtFromTop);			lua_setfield(L, -2, "tdtFromTop");
		lua_pushinteger(L, tdtFromSides);		lua_setfield(L, -2, "tdtFromSides");
		lua_pushinteger(L, tdtTopAndSides);		lua_setfield(L, -2, "tdtTopAndSides");

		// ��������� ��� GuiSetNavMode
		lua_pushinteger(L, gnm_None);			lua_setfield(L, -2, "gnm_None");
		lua_pushinteger(L, gnm_Normal);			lua_setfield(L, -2, "gnm_Normal");

		//������ �����
		lua_pushinteger(L, logLevelNone);		lua_setfield(L, -2, "logLevelNone");
		lua_pushinteger(L, logLevelError);		lua_setfield(L, -2, "logLevelError");
		lua_pushinteger(L, logLevelWarning);	lua_setfield(L, -2, "logLevelWarning");
		lua_pushinteger(L, logLevelInfo);		lua_setfield(L, -2, "logLevelInfo");
		lua_pushinteger(L, logLevelAll);		lua_setfield(L, -2, "logLevelAll");

		//��������� �������� �� ������
		lua_pushinteger(L, cobNone);		lua_setfield(L, -2, "offscreenDoNothing");
		lua_pushinteger(L, cobNone);		lua_setfield(L, -2, "offscreenNone");
		lua_pushinteger(L, cobSleep);		lua_setfield(L, -2, "offscreenSleep");
		lua_pushinteger(L, cobDie);			lua_setfield(L, -2, "offscreenDestroy");
		lua_pushinteger(L, cobAnim);		lua_setfield(L, -2, "offscreenSwitchAnim");

		//����������� ����������� ������� � ��������� �������
		lua_pushinteger(L, ofNormal);		lua_setfield(L, -2, "facingNormal");
		lua_pushinteger(L, ofMoonwalking);	lua_setfield(L, -2, "facingMoonwalking");
		lua_pushinteger(L, ofFixed);		lua_setfield(L, -2, "facingFixed");

		//���������� ������ � ���������.
		lua_pushinteger(L, pttLine);		lua_setfield(L, -2, "pttLine");
		lua_pushinteger(L, pttSine);		lua_setfield(L, -2, "pttSine");
		lua_pushinteger(L, pttCosine);		lua_setfield(L, -2, "pttCosine");
		lua_pushinteger(L, pttRipple);		lua_setfield(L, -2, "pttRipple");
		lua_pushinteger(L, pttRandom);		lua_setfield(L, -2, "pttRandom");
		lua_pushinteger(L, pttPseudoDepth);	lua_setfield(L, -2, "pttPseudoDepth");
		lua_pushinteger(L, pttTwist);		lua_setfield(L, -2, "pttTwist");
		lua_pushinteger(L, pttOrbit);		lua_setfield(L, -2, "pttOrbit");
		lua_pushinteger(L, pttGlobalSine);	lua_setfield(L, -2, "pttGlobalSine");
		lua_pushinteger(L, pttToCenter);	lua_setfield(L, -2, "pttToCenter");

		//�����������.
		lua_pushinteger(L, dirNone);		lua_setfield(L, -2, "dirNone");
		lua_pushinteger(L, dirLeft);		lua_setfield(L, -2, "dirLeft");
		lua_pushinteger(L, dirRight);		lua_setfield(L, -2, "dirRight");
		lua_pushinteger(L, dirDown);		lua_setfield(L, -2, "dirDown");
		lua_pushinteger(L, dirUp);			lua_setfield(L, -2, "dirUp");
		lua_pushinteger(L, dirAny);			lua_setfield(L, -2, "dirAny");
		lua_pushinteger(L, dirHorizontal);	lua_setfield(L, -2, "dirHorizontal");
		lua_pushinteger(L, dirVertical);	lua_setfield(L, -2, "dirVertical");

		// ���������� ������� ������ � �������
		lua_pushinteger(L, KEY_SETS_NUMBER);lua_setfield(L, -2, "configKeySetsNumber");

		// ������ ���������� ��������
		lua_pushinteger(L, rsmStandart);	lua_setfield(L, -2, "rsmStandart");
		lua_pushinteger(L, rsmStretch);		lua_setfield(L, -2, "rsmStretch");
		lua_pushinteger(L, rsmCrop);		lua_setfield(L, -2, "rsmCrop");
		lua_pushinteger(L, rsmRepeatX);		lua_setfield(L, -2, "rsmRepeatX");
		lua_pushinteger(L, rsmRepeatY);		lua_setfield(L, -2, "rsmRepeatY");
		lua_pushinteger(L, rsmRepeatXY);	lua_setfield(L, -2, "rsmRepeatXY");

		lua_pushinteger(L, physPlayer);		lua_setfield(L, -2, "physPlayer" );
		lua_pushinteger(L, physEffect);		lua_setfield(L, -2, "physEffect" );
		lua_pushinteger(L, physBullet);		lua_setfield(L, -2, "physBullet" );
		lua_pushinteger(L, physSprite);		lua_setfield(L, -2, "physSprite" );
		lua_pushinteger(L, physItem);		lua_setfield(L, -2, "physItem" );
		lua_pushinteger(L, physEnemy);		lua_setfield(L, -2, "physEnemy" );
		lua_pushinteger(L, physEnvironment);	lua_setfield(L, -2, "physEnvironment" );
		lua_pushinteger(L, physParticles);	lua_setfield(L, -2, "physParticles" );
		lua_pushinteger(L, physEverything);	lua_setfield(L, -2, "physEverything" );

		// ������ ���������
		lua_pushinteger(L, bmFirst);              lua_setfield(L, -2, "bmFirst");
		lua_pushinteger(L, bmNone);               lua_setfield(L, -2, "bmNone");
		lua_pushinteger(L, bmSrcA_OneMinusSrcA);  lua_setfield(L, -2, "bmSrcA_OneMinusSrcA");
		lua_pushinteger(L, bmBright);             lua_setfield(L, -2, "bmBright");
		lua_pushinteger(L, bmSrcA_One);           lua_setfield(L, -2, "bmSrcA_One");
		lua_pushinteger(L, bmPostEffect);         lua_setfield(L, -2, "bmPostEffect");
		lua_pushinteger(L, bmLast);               lua_setfield(L, -2, "bmLast");

		lua_pushinteger(L, wdLeft);		lua_setfield(L, -2, "wdLeft");
		lua_pushinteger(L, wdRight);		lua_setfield(L, -2, "wdRight");
		lua_pushinteger(L, wdUpLeft);		lua_setfield(L, -2, "wdUpLeft");
		lua_pushinteger(L, wdUpRight);		lua_setfield(L, -2, "wdUpRight");
		lua_pushinteger(L, wdDownLeft);		lua_setfield(L, -2, "wdDownLeft");
		lua_pushinteger(L, wdDownRight);	lua_setfield(L, -2, "wdDownRight");

		lua_pushinteger(L, intensity);		lua_setfield(L, -2, "pIntensity");
		lua_pushinteger(L, relative_speed);	lua_setfield(L, -2, "pRelativeSpeed");
		lua_pushinteger(L, use_physic);		lua_setfield(L, -2, "pUsePhysic");
		lua_pushinteger(L, include_one_sided);	lua_setfield(L, -2, "pIncludeOneSided");
		lua_pushinteger(L, include_polygons);	lua_setfield(L, -2, "pIncludePolygons");
		lua_pushinteger(L, max_simple_particles);	lua_setfield(L, -2, "pMaxSimpleParticles");
		lua_pushinteger(L, max_phys_particles);	lua_setfield(L, -2, "pMaxPhysParticles");
		lua_pushinteger(L, max_weather_particles); lua_setfield(L, -2, "pMaxWeatherParticles");

		lua_pushinteger(L, no_animation);	lua_setfield(L, -2, "paNone");
		lua_pushinteger(L, lifetime_animation);	lua_setfield(L, -2, "paLifetime");
		lua_pushinteger(L, bounce_animation);	lua_setfield(L, -2, "paBounce");
		lua_pushinteger(L, loop_animation);	lua_setfield(L, -2, "paLoop");
		lua_pushinteger(L, random_frame_animation);	lua_setfield(L, -2, "paRandomFrame");

		lua_pushinteger(L, objPlayer);          lua_setfield(L, -2, "objPlayer");
		lua_pushinteger(L, objSprite);          lua_setfield(L, -2, "objSprite");
		lua_pushinteger(L, objBullet);          lua_setfield(L, -2, "objBullet");
		lua_pushinteger(L, objEffect);          lua_setfield(L, -2, "objEffect");
		lua_pushinteger(L, objEnemy);           lua_setfield(L, -2, "objEnemy");
		lua_pushinteger(L, objItem);            lua_setfield(L, -2, "objItem");
		lua_pushinteger(L, objEnvironment);     lua_setfield(L, -2, "objEnvironment");
		lua_pushinteger(L, objRay);             lua_setfield(L, -2, "objRay");
		lua_pushinteger(L, objParticleSystem);  lua_setfield(L, -2, "objParticleSystem");
		lua_pushinteger(L, objWaypoint);        lua_setfield(L, -2, "objWaypoint");
		lua_pushinteger(L, objSpawner);         lua_setfield(L, -2, "objSpawner");
		lua_pushinteger(L, objTile);            lua_setfield(L, -2, "objTile");
		lua_pushinteger(L, objRibbon);          lua_setfield(L, -2, "objRibbon");

		lua_pushstring(L, VERSION);             lua_setfield(L, -2, "__VERSION");
		lua_pushstring(L, GAMENAME);            lua_setfield(L, -2, "__GAME_NAME");
		lua_pushstring(L, ENGINE);              lua_setfield(L, -2, "__ENGINE");
		lua_pushstring(L, path_log);            lua_setfield(L, -2, "path_log");
		lua_pushstring(L, path_config);         lua_setfield(L, -2, "path_config");
		lua_pushstring(L, path_textures);       lua_setfield(L, -2, "path_textures");
		lua_pushstring(L, path_fonts);          lua_setfield(L, -2, "path_fonts");
		lua_pushstring(L, path_protos);         lua_setfield(L, -2, "path_protos");
		lua_pushstring(L, path_scripts);		lua_setfield(L, -2, "path_scripts");
		lua_pushstring(L, path_levels);			lua_setfield(L, -2, "path_levels");
		lua_pushstring(L, path_sounds);			lua_setfield(L, -2, "path_sounds");
		lua_pushstring(L, path_data);			lua_setfield(L, -2, "path_data");
		lua_pushstring(L, path_app);			lua_setfield(L, -2, "path_app");
		lua_pushstring(L, path_home);			lua_setfield(L, -2, "path_home");
		lua_pushstring(L, path_saves);			lua_setfield(L, -2, "path_saves");
		lua_pushstring(L, __DATE__);			lua_setfield(L, -2, "__BUILD_DATE");
		lua_pushstring(L, __TIME__);			lua_setfield(L, -2, "__BUILD_TIME");
#if defined(PACKAGE)
		lua_pushstring(L, PACKAGE);				lua_setfield(L, -2, "__BUILD_TYPE");
#elif defined(__linux__)
		lua_pushstring(L, "LINUX");				lua_setfield(L, -2, "__BUILD_TYPE");
#elif defined(__FreeBSD__)
		lua_pushstring(L, "FREEBSD");			lua_setfield(L, -2, "__BUILD_TYPE");
#elif defined(__APPLE__)
		lua_pushstring(L, "OSX");				lua_setfield(L, -2, "__BUILD_TYPE");
#elif defined(WIN32)
		lua_pushstring(L, "WIN32");				lua_setfield(L, -2, "__BUILD_TYPE");
#else
		lua_pushstring(L, "UNKNOWN");			lua_setfield(L, -2, "__BUILD_TYPE");
#endif
	}
	lua_setglobal(L, "constants");		// ������� ������� � ���������� ����������. ����:
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::LoadDefaultConfig(lua_State* L)
{
	UNUSED_ARG(L);
	InitParticlesParam();
	::LoadDefaultConfig();
	return 0;
}

int scriptApi::LoadConfig(lua_State* L)
{
	UNUSED_ARG(L);
	InitParticlesParam();
	::LoadConfig();
	return 0;
}

int scriptApi::SaveConfig(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1)||lua_isnone(L, 1)||lua_isnil(L, 1), 1, "Filename expected");
	if ( lua_isstring( L, 1 ) )
	{
		::SaveConfig( lua_tostring( L , 1 ) );	
	}
	else
	{
		::SaveConfig( NULL );
	}
	return 0;
}

int scriptApi::LoadParticlesConfig(lua_State* L)
{
	luaL_argcheck(L, lua_istable(L, 1), 1, "Config table expected");
	for ( size_t i = particles_param_first; i < particles_param_last; i++ )
	{
		lua_rawgeti( L, 1, i );
		if ( lua_isnumber( L, -1 ) )
		{
			SetParticlesParam( static_cast<ParticlesParam>(i), static_cast<float>(lua_tonumber(L, -1)) );
		}
		lua_pop( L, 1 );
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////

/// ��������� ��������. ����� ��������� ������� �������� ������ ��������.
/// @luaparam string name - ��� ��������
/// @luaparam table [proto] - ������� �������� ������ ��������
/// @luaparam string [name_hint] - �������������� ��� ��� �������. ���� �� ���������� ��� ���� ������ � ����� ������� ��� ����������, ������ �������� ��� ���.
/// @return string name - ���, ��� ������� ������ ��������
int scriptApi::LoadTexture (lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Texture name expected");
	luaL_argcheck(L, lua_istable(L, 2) || lua_isnone(L, 2), 2, "Texture frame table expected");
	luaL_argcheck(L, lua_isstring(L, 3) || lua_isnoneornil(L, 3), 3, "Name hint expected");
	luaL_argcheck(L, lua_gettop(L) <= 3, 4, "Too many arguments");

	if (!textureMgr)
	{
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Texture manager is not created");
		return 0;
	}
	
	const char* tex_name = lua_tostring(L, 1);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading texture %s", tex_name);

	Texture* tex = NULL;
	if (lua_isnone(L, 2))
	{
		tex = textureMgr->GetByName(tex_name);
	}
	else
	{
		const char* name_hint = NULL;
		if (!lua_isnone(L, 3))
		{
			name_hint = lua_tostring(L, 3);
			lua_pop(L, 1);
		}

		textureMgr->doLoad = false;
		tex = textureMgr->GetByNameWithHint(tex_name, name_hint ? name_hint : tex_name);
		textureMgr->doLoad = true;

		if (tex)
		{
			TexFrames* frames = new TexFrames(tex->name, NULL, 0);
			assert(lua_istable(L, -1));

			texFramesMgr->LoadResource(frames, tex->name);
			
			if (tex->LoadWithTexFrames(frames))
			{
				if (frames)

				{
					if (!frames->LoadFramesFromTable(L))
						frames->MakeDummyFrame((float)tex->width, (float)tex->height);
				}
			}
			else
			{
				textureMgr->Unload(tex->name);
				tex = NULL;
			}
		}
	}

	if ( tex == NULL)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error loading texture");
		return 0;
	}
	else
		lua_pushstring(L, tex->name.c_str());
	return 1;	
}

/// �������� ������� �������� ������ ����������� ��������.
/// @luaparam string name - ��� ��������
/// @luaparam table proto - ������� �������� ������ ��������
int scriptApi::ChangeTexFrames(lua_State* L)
{

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Texture name expected");
	luaL_argcheck(L, lua_istable(L, 2), 2, "Texture frame table expected");

	const char* name = lua_tostring(L, 1);
	Texture* tex = NULL;
	{
		bool alr = textureMgr->autoLoadResource;
		bool alar = textureMgr->autoLoadArchiveResource;
		textureMgr->autoLoadResource = false;
		textureMgr->autoLoadArchiveResource = false;
		tex = textureMgr->GetByName(name);
		textureMgr->autoLoadArchiveResource = alar;
		textureMgr->autoLoadResource = alr;
	}
	if (tex && tex->frames)
	{
		if (!tex->frames->LoadFramesFromTable(L))
			tex->frames->MakeDummyFrame((float)tex->width, (float)tex->height);
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "ChangeTexFrames: Cannot find texture %s", name);
	}
	return 0;
}

int scriptApi::DumpTexFrames(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Texture name expected");

	const char* name = lua_tostring(L, 1);
	Texture* tex = textureMgr->GetByName(name);
	if (tex && tex->frames)
		return tex->frames->DumpTable(L);
	else
		Log(DEFAULT_LOG_NAME, logLevelWarning, "DumpTexFrames: Cannot find texture %s with frames", name);

	return 0;
}

//////////////////////////////////////////////////////////////////////////

// �������� ������.
// ��������� ��������� �������� true ��� false
int scriptApi::LoadFont (lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be font name");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be font name for game");

	const char* font_name = lua_tostring(L, 1);
	const char* out_name = lua_tostring(L, 2);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading font %s", font_name);

	bool res = LoadTextureFont(font_name, out_name);

	if (!res)
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error loading font");

	lua_pushboolean(L, res);
	return 1;
}


// ������� ��������� �������� ������� �� �����
int scriptApi::LoadPrototype(lua_State* L)
{
	if (!protoMgr)
	{
		Log(DEFAULT_LOG_NAME, logLevelInfo, "protoMgr is not created");
		return 0;
	}

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");

	// ����: proto_name
	const char* proto_name = lua_tostring(L, 1);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading proto %s", proto_name);

	if ( protoMgr->GetByName(proto_name) == NULL)
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error loading proto");

	return 0;
}

int scriptApi::LoadSound(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");

	const char* file_name = lua_tostring(L, 1);
	char* snd_file = new char[strlen(path_sounds) + strlen(file_name) + 1];
	memset(snd_file, '\0', strlen(path_sounds) + strlen(file_name) + 1);
	sprintf(snd_file, "%s%s", path_sounds, file_name);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading sound %s", file_name);

	if (!soundMgr || !::soundMgr->GetByName(snd_file))
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error loading sound");

	DELETEARRAY(snd_file);

	return 0;
}

/// ��������� �������� �� ���������� �������.
/// @luaparam table proto - ������� �������� ���������
/// @luaparam string [name_hint] - �������������� ��� ��� ���������. ���� �� ���������� ��� ���� �������� � ����� ������� ��� ����������, ������ �������� ��� ���.
/// @return string name - ���, ��� ������� �������� ��������
int scriptApi::LoadPrototypeTable(lua_State* L)
{
	luaL_argcheck(L, lua_istable(L, 1), 1, "Prototype table expected");
	luaL_argcheck(L, lua_isstring(L, 2) || lua_isnoneornil(L, 2), 2, "Prototype name expected");
	luaL_argcheck(L, lua_gettop(L) <= 2, 3, "Too many arguments");

	const char* name_hint = NULL;
	if (!lua_isnone(L, 2))
	{
		name_hint = lua_tostring(L, 2);
		lua_pop(L, 1);
	}

	Proto* proto = new Proto("", NULL, 0);
	proto->LoadTable(L);
	std::string name = protoMgr->LoadResource(proto, name_hint ? name_hint : "");

	lua_pushstring(L, name.c_str());

	return 1;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::RegisterFunction(lua_State* L)
{
	luaL_argcheck(L, lua_isfunction(L, 1), 1, "Function expected");

	LuaRegRef fref = LUA_NOREF;
	SCRIPT::RegProc(L, &fref, 1);
	SCRIPT::SetInGameRegRef(fref);
	lua_pushinteger(L, fref);
	return 1;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int scriptApi::InitNewGame(lua_State* L)
{
	UNUSED_ARG(L);
	game::InitGame();
	return 0;
}

int scriptApi::DestroyGame(lua_State* L)
{
	UNUSED_ARG(L);
	// TODO: ������. ����� ������� �� ��������� ������� - ����� ���� ������.
	// ���� ������� ���-�� ����� need_destroying. 
	game::FreeGame(true);
	return 0;
}

int scriptApi::ExitGame(lua_State* L)
{
	UNUSED_ARG(L);
	current_state = PROGRAMM_EXITING;
	return 0;
}

////////////////////////////////////

int scriptApi::AddTimerEvent(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be time interval");
	luaL_argcheck(L, lua_isfunction(L, 2), 2, "Must be timer event handler");

	//SCRIPT::StackDumpToLog(L);

	UINT dt = (UINT)lua_tointeger(L, 1);
	UINT period = 0;
	UINT maxCalls = 0;

	int num = lua_gettop ( L );	// ����� ���������� ��������� � ����� (���������� ���������� ����������)
	if (num == 4)
	{
		luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be period");
		luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be repetition count");

		period = (UINT)lua_tointeger(L, 3);
		maxCalls = (UINT)lua_tointeger(L, 4);
	}


	lua_settop(L, 2);	// ����� ������� ������� �������� �� ������� �����
	//SCRIPT::StackDumpToLog(L);

	int action = SCRIPT::AddToRegistry();

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating timer event");

	int result = 0;
	if (num == 2)
		result = ::AddTimerEvent(dt, action);
	else
		result = ::AddTimerEvent(dt, action, period, maxCalls);

	if (!result)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating timer event");
	}

	return 0;
}


///////////////////////////////////////

// ������ � ���. �������� ��� print
int scriptApi::Log(lua_State* L)
{
	int num = lua_gettop ( L );	// ����� ���������� ��������� � ����� (���������� ���������� ����������)

	string buf("");
	char char_buf[20];

	for (int i = -num; i < 0; i++)
	{
		int t = lua_type(L, i);
		switch (t)
		{
		case LUA_TSTRING: /* strings */
			{
				buf += lua_tostring(L, i);
			}
			break;

		case LUA_TBOOLEAN: /* booleans */
			{
				buf += lua_toboolean(L, i) ? "true" : "false";
			}
			break;

		case LUA_TNUMBER: /* numbers */
			{
				// TODO: sprintf_s � snprintf ������� ���������� ����������.
				// sprintf_s �����������, ��� � ����� ������� ����� 0.
#ifdef WIN32
				sprintf_s(char_buf, 20, "%f", lua_tonumber(L, i));
#else
				snprintf(char_buf, 20, "%f", lua_tonumber(L, i));
#endif // WIN32
				buf += string(char_buf);
			}
			break;

		case LUA_TNIL:
			buf += "nil";
			break;

		default:
			{
				// TODO: sprintf_s � snprintf ������� ���������� ����������.
				// sprintf_s �����������, ��� � ����� ������� ����� 0.
#ifdef WIN32
				sprintf_s(char_buf, 20, "0x%p", lua_topointer(L, i));
#else
				snprintf(char_buf, 20, "0x%p", lua_topointer(L, i));
#endif // WIN32
				buf += lua_typename(L, t) + string(": ") + string(char_buf);
			}

		}

	}

	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelScript, buf.c_str());

	lua_pop(L, num);	// ����:
	return 0;
}

//////////////////////////////////////////////////////////////////////////


// ����� ����������� ���������� ������
int scriptApi::SetCamLag(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "CAMERA_LAG expected!");

	float k = (float)lua_tonumber(L, 1);

	CameraSetLag(k);

	return 0;
}

// ��������� ������ �� �������. ������ �������� �� ��� id

int scriptApi::SetCamAttachedObj(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Object id expected");

	UINT id = static_cast<UINT>(lua_tointeger(L, 1));

	GameObject* obj = NULL;
	if (id != 0)
	{
		obj = GetGameObject(id);
	}

	CameraAttachToObject(obj);

	return 0;
}

// ������ ����� ��������� ������ �� �������� ����, ����� �� ��������
int scriptApi::SetCamAttachedAxis(lua_State* L)
{
	luaL_argcheck(L, lua_isboolean(L, 1), 1, "Boolean expected");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Boolean expected");

	CameraAttachToAxis(lua_toboolean(L, 1) != 0, lua_toboolean(L, 2) != 0);

	return 0;
}

// ���������� ������ �� �����, �������� ������������
int scriptApi::CamMoveToPos(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Number expected");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Number expected");
	luaL_argcheck(L, lua_isboolean(L, 3)||lua_isnil(L, 3)||lua_isnone(L, 3), 3, "Bool expected");

	bool use_bounds = true;
	if ( lua_isboolean(L, 3) && lua_toboolean(L, 3) != 0 )
		use_bounds = false;

	CameraMoveToPos(static_cast<float>(lua_tonumber(L, 1)), static_cast<float>(lua_tonumber(L, 2)), use_bounds);

	return 0;
}


// ������ �������� ������ ������������ �������
int scriptApi::SetCamObjOffset(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Number expected");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Number expected");


	SetCameraAttachedObjectOffset((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2));

	return 0;
}


// ������ ���� �������, �� ������ ������������ ������
int scriptApi::SetCamFocusOnObjPos(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Number expected");

	CAMERA_FOCUS_ON_OBJ_POS = (CameraFocusObjectPoint)lua_tointeger(L, 1);

	return 0;
}

int scriptApi::SetCamUseBounds(lua_State* L)
{
	luaL_argcheck(L, lua_isboolean(L, 1), 1, "Boolean expected");
	::CameraUseBounds(lua_toboolean(L,1) != 0);
	return 0;
}

int scriptApi::SetCamBounds(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Number expected");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Number expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Number expected");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Number expected");

	::CameraSetBounds((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2),
		(float)lua_tonumber(L, 3), (float)lua_tonumber(L, 4));
	return 0;
}

int scriptApi::GetCamPos(lua_State* L)
{
	lua_pushnumber(L, CAMERA_X);
	lua_pushnumber(L, CAMERA_Y);

	return 2;
}

int scriptApi::GetCamFocusShift(lua_State* L)
{
	float x = 0, y = 0;
	GetCameraAttachedFocusShift(x,y);

	lua_pushnumber(L, x);
	lua_pushnumber(L, y);
	return 2;
}

int scriptApi::SetCamScale(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "scale x");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "scale y");

	CameraSetScale((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2));

	return 0;
}

int scriptApi::SetCamAngle(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "angle in degrees");

	CameraSetAngle(static_cast<float>(lua_tonumber(L, 1)));

	return 0;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int scriptApi::CreateRibbon(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be speed factor X");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be speed factor Y");
	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);
	float fact = (float)lua_tonumber(L, 4);
	float fact_y = (float)lua_tonumber(L, 5);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating Ribbon: %s", proto_name);
	Ribbon* r = NULL;
	r = ::CreateRibbon(proto_name, Vector2(x, y), fact, fact_y);
	if (r)
	{

		lua_pushnumber(L, r->id);	// ����: r->id
	}
	else
	{
		lua_pushnil(L);				// ����: nil
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating ribbon");
	}

	return 1;
}

int scriptApi::SetRibbonZ(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Ribbon number");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate Z");

	Ribbon* r = GetRibbon( static_cast<UINT>(lua_tointeger(L,1)) );
	if (r && r->sprite)
	{
		r->sprite->z = (float)lua_tonumber(L, 2);
	}
	return 0;
}

int scriptApi::SetRibbonAttatachToY(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Ribbon number");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "bool");

	Ribbon* r = GetRibbon( static_cast<UINT>(lua_tointeger(L,1)) );
	if (r)
	{
		r->y_attached = lua_toboolean(L, 2) != 0;
	}
	return 0;
}

int scriptApi::SetRibbonBounds(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Ribbon number");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be bounds coordinate X1");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be bounds coordinate Y1");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be bounds coordinate X2");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be bounds coordinate Y2");


	Ribbon* r = GetRibbon( static_cast<UINT>(lua_tointeger(L,1)) );
	if (r)
	{
		r->SetBounds((float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3),
			(float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5));
	}
	return 0;
}




//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int scriptApi::PlaySnd(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");
	luaL_argcheck(L, lua_isnumber(L, 3)||lua_isnil(L, 3)||lua_isnone(L, 3), 3, "Must be x");
	luaL_argcheck(L, lua_isnumber(L, 4)||lua_isnil(L, 4)||lua_isnone(L, 4), 4, "Must be y");
	luaL_argcheck(L, lua_isboolean(L, 5)||lua_isnil(L, 5)||lua_isnone(L, 5), 5, "Must be relativity flag");
	luaL_argcheck(L, lua_isnumber(L, 6)||lua_isnil(L, 6)||lua_isnone(L, 6), 6, "Must be sound volume level");
	const char* sound_name = lua_tostring(L, 1);
	bool restart = lua_toboolean(L, 2) != 0;
	float volume = lua_isnumber(L, 6) ? static_cast<float>(lua_tonumber(L, 6)) : 1.0f;
	if (soundMgr)
	{
		if ( lua_isnumber(L, 3) && lua_isnumber(L, 4)  )
		{
			if ( lua_isboolean(L,5) && lua_toboolean(L,5) )
				soundMgr->PlaySnd(string(sound_name), restart, Vector2( CAMERA_X+(float)lua_tonumber(L, 3), CAMERA_Y+(float)lua_tonumber(L, 4) ), volume);
			else
				soundMgr->PlaySnd(string(sound_name), restart, Vector2( (float)lua_tonumber(L, 3), (float)lua_tonumber(L, 4) ), volume );
		}
		else
			soundMgr->PlaySnd(string(sound_name), restart);
	}
	return 0;
}

int scriptApi::PauseSnd(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");
	const char* sound_name = lua_tostring(L, 1);
	if (soundMgr)
		soundMgr->PauseSnd(string(sound_name));
	return 0;
}

int scriptApi::StopSnd(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");
	const char* sound_name = lua_tostring(L, 1);
	if (soundMgr)
		soundMgr->StopSnd(string(sound_name));
	return 0;
}

int scriptApi::StopAllSnd(lua_State* L)
{
	UNUSED_ARG(L);
	if (soundMgr)
		soundMgr->StopAll();
	return 0;
}

int scriptApi::GetMasterSoundVolume(lua_State* L)
{
	if (soundMgr)
		lua_pushnumber(L, soundMgr->GetVolume());
	else

		lua_pushnumber(L, 0);
	return 1;
}

int scriptApi::SetMasterSoundVolume(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be sound volume level");
	if (soundMgr)
		soundMgr->SetVolume((float)lua_tonumber(L, 1));
	return 0;
}

int scriptApi::GetMasterSFXVolume(lua_State* L)
{
	if (soundMgr)
		lua_pushnumber(L, soundMgr->GetSoundVolume());
	else
		lua_pushnumber(L, 0);
	return 1;
}

int scriptApi::SetMasterSFXVolume(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be sound volume level");
	if (soundMgr)
		soundMgr->SetSoundVolume((float)lua_tonumber(L, 1));
	return 0;
}

int scriptApi::GetMasterMusicVolume(lua_State* L)
{
	if (soundMgr)
		lua_pushnumber(L, soundMgr->GetMusicVolume());
	else
		lua_pushnumber(L, 0);
	return 1;
}

int scriptApi::SetMasterMusicVolume(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be sound volume level");
	if (soundMgr)
		soundMgr->SetMusicVolume((float)lua_tonumber(L, 1));
	return 0;
}

int scriptApi::GetSoundVolume(lua_State* L)
{

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");
	if (soundMgr)
		lua_pushnumber(L, soundMgr->GetSndVolume(string(lua_tostring(L, 1))) );
	else
		lua_pushnumber(L, 0);
	return 1;
}

int scriptApi::SetSoundVolume(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be sound volume level");
	if (soundMgr)
		soundMgr->SetSndVolume(string(lua_tostring(L, 1)), (float)lua_tonumber(L, 2));
	return 0;
}

int scriptApi::PlayBackMusic(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be sound name");
	luaL_argcheck(L, lua_isnumber(L, 2)||lua_isnil(L, 2)||lua_isnone(L, 2), 2, "Must be sound volume level");
	float volume = 1.0f;
	if ( lua_isnumber(L, 2) )
	{
		volume = (float)lua_tonumber(L, 2);
	}
	if (soundMgr)
		soundMgr->PlayBackMusic(string(lua_tostring(L, 1)),volume);
	return 0;

}

int scriptApi::GetBackMusic(lua_State* L)
{
	char* mus = NULL;
	if (soundMgr)
		mus = soundMgr->GetBackMusic();
	if ( mus != NULL )
	{
		lua_pushstring( L, mus );
		DELETESINGLE( mus );
	}
	else
		lua_pushnil( L );
	return 1;

}

int scriptApi::PauseBackMusic(lua_State* L)
{
	UNUSED_ARG(L);
	if (soundMgr)
		soundMgr->PauseBackMusic();
	return 0;
}

int scriptApi::ResumeBackMusic(lua_State* L)
{
	UNUSED_ARG(L);
	if (soundMgr)
		soundMgr->ResumeBackMusic();
	return 0;
}

int scriptApi::StopBackMusic(lua_State* L)
{
	UNUSED_ARG(L);
	if (soundMgr)
		soundMgr->StopBackMusic();
	return 0;
}

int scriptApi::GetBackMusicLevel(lua_State* L)
{
	uint32_t level = 0;
	if (soundMgr)
		level = soundMgr->GetBackMusicLevel();
	lua_pushinteger( L, level & 0xffff );
	lua_pushinteger( L, (level >> 16) & 0xffff );
	return 2;
}

int scriptApi::GetBackMusicFFT(lua_State* L)
{
	if (!soundMgr)
		return 0;
	float* fft = soundMgr->GetBackMusicFFT();
	if ( fft == NULL )
		return 0;
	lua_newtable(L);
	for ( int i = 0; i < 255; i++ )
	{
		lua_pushnumber( L, fft[i] );
		lua_rawseti( L, -2, i+1 );
	}
	DELETEARRAY(fft);
	return 1;
}

int scriptApi::GetSndLength(lua_State* L)
{
	if (!soundMgr)
		return 0;
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Sound name");
	lua_pushnumber( L, soundMgr->GetSndLength( lua_tostring(L, 1) ) );
	return 1;
}

int scriptApi::GetBackMusicLength(lua_State* L)
{
	if (!soundMgr)
		return 0;
	lua_pushnumber( L, soundMgr->GetBackMusicLength() );
	return 1;
}

int scriptApi::GetBackMusicSpeed(lua_State* L)
{
	if (!soundMgr)
		return 0;
	lua_pushnumber( L, soundMgr->GetMusicSpeed() );
	return 1;
}

int scriptApi::SetBackMusicSpeed(lua_State* L)
{
	if (!soundMgr)
		return 0;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Tics per row (from 0 to 255)");
	float speed = static_cast<float>(lua_tointeger(L, 1));
	soundMgr->SetMusicSpeed( speed );
	return 0;
}


void scriptApi::MapVarAdd( lua_State* L, char const * var_name, int amount )
{
	lua_getglobal( L, "mapvar" );
	bool nt = false;
	if ( !lua_istable(L, -1) )
	{
		lua_newtable(L);
		nt = true;
	}
	{
		int oldValue = 0;
		if ( !nt )
		{
			lua_getfield(L, -1, var_name);
			oldValue = static_cast<int>(lua_tointeger(L, -1));
		}
		lua_pushinteger(L, oldValue + amount);
		if (nt) lua_setfield(L, -2, var_name);
		else
		{
			lua_setfield(L, -3, var_name);
			lua_pop(L, 1);
		}
	}
	lua_setglobal( L, "mapvar" );
	lua_getglobal( L, "varchange" );
	if ( lua_isfunction(L, -1) )
	{
		lua_pushstring( L, var_name );
		lua_pushinteger( L, amount );
		SCRIPT::ExecChunk(2);;
	}

}
/////////////////////////////////////////////////////////////

int scriptApi::IsConfKeyPressed(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be key number in config (from config_keys)");
	lua_pushboolean(L, inpmgr.IsPressed( ((ConfigActionKeys)lua_tointeger(L, 1)) ) );
	return 1;
}

int scriptApi::IsConfKeyHolded(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be key number in config (from config_keys)");
	lua_pushboolean(L, inpmgr.IsHolded( ((ConfigActionKeys)lua_tointeger(L, 1)) ) );
	return 1;
}

int scriptApi::IsConfKeyReleased(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be key number in config (from config_keys)");
	lua_pushboolean(L, inpmgr.IsReleased( ((ConfigActionKeys)lua_tointeger(L, 1)) ) );
	return 1;
}

int scriptApi::IsConfKey(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be key");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be key number in config (from config_keys)");
	bool is_key = false;
	UINT key = (UINT)lua_tointeger(L, 1);
	ConfigActionKeys cak = (ConfigActionKeys)lua_tointeger(L, 2);
	for (size_t i = 0; i < KEY_SETS_NUMBER; i++)
	{
		is_key = is_key || (cfg.cfg_keys[i][cak] == key);
	}
	lua_pushboolean(L, is_key);
	return 1;
}

int scriptApi::EnableScreenshots(lua_State* L)
{
	luaL_argcheck(L, lua_isboolean(L, 1), 1, "Must be boolean state");
	scene::EnableScreenshots( lua_toboolean(L, 1) != 0 );
	return 0;
}

int scriptApi::PushButton(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be key");
	UINT keynum = (UINT)lua_tointeger(L, 1);
	lua_pushboolean(L, (int)inpmgr.IsHolded( keynum ));
	return 1;
}

int scriptApi::TogglePause(lua_State* L)
{
	luaL_argcheck(L, lua_isboolean(L,1)||lua_isnone(L,1)||lua_isnil(L,1), 1, "Must be state");
	if ( lua_isboolean(L,1) )
			game::TogglePause( lua_toboolean(L,1) != 0 );
	else
			game::TogglePause();
	return 0;
}

int scriptApi::DumpKeys(lua_State* L)
{
	for ( UINT i = 0; i < inpmgr.keys_count; i++ )
		lua_pushboolean(L, inpmgr.IsHolded( i ));

	return inpmgr.keys_count;
}

int scriptApi::PushKeyName(lua_State* L)
{
	if ( !lua_isnumber(L, 1) )
	{
		lua_pushstring(L, "(unknown key)");
		return 1;
	}
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be key");
	int keynum = static_cast<int>(lua_tointeger(L, 1));
	char ch[MAX_VKEY_NAME_LEN] = "";
	if ( GetVKeyByNum(keynum, ch) )
	{
		lua_pushstring(L, ch);
	}
	else
	{
		lua_pushstring(L, "(unknown key)");
	}
	return 1;
}

int scriptApi::GetFirstButtonPressed(lua_State* L)
{
	for ( UINT i = 0; i < inpmgr.keys_count; i++ )
	{
		if (inpmgr.IsHolded(i))
		{
			lua_pushinteger(L, i);
			return 1;
		}
	}
	lua_pushnil(L);
	return 1;
}

int scriptApi::GamePaused(lua_State* L)
{
	lua_pushboolean( L, game::GetPause() );
	return 1;
}

//////////////////////////////////////////////////////////////////////////

extern UINT internal_time;
int scriptApi::GetCurTime(lua_State* L)
{
	lua_pushinteger(L, internal_time);
	return 1;
}

extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_SCALE_X;
extern float CAMERA_SCALE_Y;
extern float CAMERA_SCALE_X;
extern float CAMERA_SCALE_Y;

int scriptApi::GetMousePos(lua_State* L)
{
	luaL_argcheck(L, lua_isboolean(L, 1)||lua_isnone(L, 1)||lua_isnil(L, 1), 1, "Screen relativity bool");
	if ( lua_isboolean(L, 1) && lua_toboolean(L, 1) != 0 )
	{
		lua_pushnumber(L, inpmgr.mouseX);
		lua_pushnumber(L, inpmgr.mouseY);
	}
	else
	{
		if ( _CameraUsesTransformations() )
			_CameraCalcTransformedBounds();
		lua_pushnumber(L, floor(CAMERA_DRAW_LEFT + inpmgr.mouseX / CAMERA_SCALE_X));
		lua_pushnumber(L, floor(CAMERA_DRAW_TOP + inpmgr.mouseY / CAMERA_SCALE_Y));
		if ( _CameraUsesTransformations() )
			RestoreCameraEdges();
	}

	return 2;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::EditorGetObjects(lua_State* L)
{
#ifdef MAP_EDITOR
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be x1");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be y1");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be x2");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be y2");
	
	float x1 = (float)lua_tonumber(L, 1);
	float y1 = (float)lua_tonumber(L, 2);
	float x2 = (float)lua_tonumber(L, 3);
	float y2 = (float)lua_tonumber(L, 4);


	if (x1 == x2) x2 = x1 + 0.1f;
	if (y1 == y2) y2 = y1 + 0.1f;
	
	EditorGetObjects(CAABB(x1, y1, x2, y2));
#else
	UNUSED_ARG( L );
#endif // MAP_EDITOR
	return 0;
}


int scriptApi::RegEditorGetObjectsProc(lua_State* L)
{
#ifdef MAP_EDITOR
	luaL_argcheck(L, lua_isfunction(L, 1), 1, "Must be proc");
	EditorRegAreaSelectProc(L);
#else
	UNUSED_ARG( L );
#endif // MAP_EDITOR
	return 0;
}

#ifdef MAP_EDITOR
extern bool editor_ShowBorders;
extern editor::EditorStates editor_state;
#endif //MAP_EDITOR

int scriptApi::EditorToggleBorders(lua_State* L)
{
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		luaL_argcheck(L, lua_isboolean(L, 1), 1, "Must be bool");
		editor_ShowBorders = lua_toboolean(L, 1) != 0;
	}
	else
#endif //MAP_EDITOR
	{
		UNUSED_ARG(L);
	}
	return 0;
}

#ifdef MAP_EDITOR
extern Vector2 grid;
extern RGBAf grid_color;
#endif //MAP_EDITOR

int scriptApi::EditorSetGrid(lua_State* L)
{
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		luaL_argcheck(L, lua_isnumber(L, 1), 1, "grid x expected");
		luaL_argcheck(L, lua_isnumber(L, 2), 2, "grid y expected");
		luaL_argcheck(L, lua_istable(L, 3), 3, "grid color expected");
		grid.x = static_cast<float>(lua_tointeger( L, 1 ));
		grid.y = static_cast<float>(lua_tointeger( L, 2 ));
		SCRIPT::GetColorFromTable(L, 3, grid_color);
	}
	else
#endif //MAP_EDITOR
	{
		UNUSED_ARG(L);
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::ListDirContents(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be path");
	luaL_argcheck(L, lua_isboolean(L, 2)|lua_isnone(L, 2)|lua_isnil(L, 2), 2, "...");

	const char* path = lua_tostring(L, 1);
	char* root_path = new char[ strlen(path_app) + 1 + strlen(path) + 1 + 1 ];
	bool absolute = false;
	if ( lua_isboolean(L, 2) )
		absolute = lua_toboolean(L, 2) != 0;
	if (absolute) sprintf(root_path, "%s/", path);
	else sprintf(root_path, "%s/%s/", path_app, path);

	DIR *dp;
	struct dirent *ep;

	
	list<string> dirs;
	list<string> files;

	dp = opendir(root_path);
	if (dp == NULL)
	{
		lua_pushnil(L);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in ListDirContents. Unable to open folder.");
		return 1;
	}
	
	while ( (ep = readdir(dp)) != NULL )
	{
		if (ep->d_name[0] == '.')
			continue;

		if (ep->d_type == DT_DIR)
		{
			dirs.push_back(string(ep->d_name));
		}
		else
		{
			files.push_back(string(ep->d_name));
		}
	}
	closedir(dp);
	DELETEARRAY(root_path);


	UINT dirs_count = (UINT)dirs.size();
	lua_newtable(L);
	list<string>::iterator it;
	int i = 1;

	for (it = dirs.begin(); it != dirs.end(); it++, i++)
	{
		lua_pushstring(L, (*it).c_str());
		lua_rawseti(L, -2, i);
	}
	for (it = files.begin(); it != files.end(); it++, i++)
	{
		lua_pushstring(L, (*it).c_str());
		lua_rawseti(L, -2, i);
	}

	lua_pushinteger(L, dirs_count);
	return 2;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::ProtectedVariable(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "variable number");
	luaL_argcheck(L, lua_isnumber(L, 2)||lua_isnone(L, 2)||lua_isnil(L, 3), 2, "new value");
	int num = static_cast<int>(lua_tointeger(L, 1));
	bool modify = lua_isnumber(L, 2) != 0;
	int value = modify ? static_cast<int>(lua_tointeger(L, 2)) : 0;
	lua_pushboolean( L, Highscores::ProtectedVariable( num, value, modify ) );
	lua_pushinteger( L, value );
	return 2;
}

int scriptApi::SendHighscores(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "nickname");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "points");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "seconds");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "game mode");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "stat");
	luaL_argcheck(L, lua_isstring(L, 6), 6, "char name");

	const char* nick = lua_tostring(L, 1);
	const int score = static_cast<const int>(lua_tointeger(L, 2));
	const int time = static_cast<const int>(lua_tointeger(L, 3));
	const int mode = static_cast<const int>(lua_tointeger(L, 4));
	const int stat = static_cast<const int>(lua_tointeger(L, 5));
	const char* char_name = lua_tostring(L, 6);
	lua_pushboolean(L, hsSendScore(nick, score, time, stat, mode, char_name));
	lua_pushstring(L, Highscores::GetAnswer() );
	return 2;
}

int scriptApi::GetHighscores(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "mode");

	int mode = static_cast<int>( lua_tointeger(L, 1) );

	lua_pushstring(L, Highscores::GetScoreTable( mode ) );

	return 1;
}

int scriptApi::LockScore(lua_State* L)
{
	UNUSED_ARG(L);

	Highscores::LockScore();
	
	return 0;
}

int scriptApi::GetScoreLock(lua_State* L)
{
	UNUSED_ARG(L);

	lua_pushboolean( L, Highscores::GetScoreLock() );
	
	return 1;
}

int scriptApi::RegisterArchive(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "archive");

	RegisterResourceArchive( lua_tostring(L, 1) );

	return 0;
}

int scriptApi::UnregisterArchive(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "archive");

	UnregisterResourceArchive( lua_tostring(L, 1) );

	return 0;
}

int scriptApi::DoArchiveFile(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "filename");
	return max( SCRIPT::DoFile( lua_tostring(L, 1 ) ), 0);
}

int scriptApi::LoadArchiveFile(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "filename");
	if ( !SCRIPT::LuaLoadFile( lua_tostring(L, 1 ) ) )
		return 1;
	else
		return 2;
}

int scriptApi::NetConnect(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "host");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "port");
	Net::Connect( lua_tostring(L, 1), static_cast<uint16_t>(lua_tointeger(L, 2)) );
	
	return 0;
}

int scriptApi::NetHost(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "port");
	Net::StartServer( static_cast<uint16_t>(lua_tointeger(L, 1)) );
	
	return 0;
}

int scriptApi::ChatMessage(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "message");
	char* message = StrDupl( lua_tostring(L, 1) );
	Net::ChatMessage( message );
	DELETEARRAY( message );

	return 0;
}

int scriptApi::NeedArchiveFile(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "filename");

	const char* name = lua_tostring(L, 1);
	lua_pop(L, 1);

	lua_getglobal(L, "__needed" );
	if ( lua_isnil( L, -1 ) ) 
	{
		lua_createtable(L, 0, 1 );
		lua_setglobal(L, "__needed" );
		lua_getglobal(L, "__needed" );
	}
	lua_pushstring( L, name );
	lua_rawget( L, -2 );
	if ( lua_isnil( L, -1 ) )
	{
		lua_pop(L, 1);
		int result = SCRIPT::DoFile( name );
		if ( result >= 0 )
		{
			lua_getglobal( L, "__needed" );
			lua_pushstring( L, name );
			lua_pushinteger( L, 1 );
			lua_rawset( L, -3 );
			lua_pop(L, 1);
		}
		return max( result, 0);
	}
	else
	{
		return 0;
	}
}

int scriptApi::UpdateWindowCaption(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "string");

	scene::UpdateWindowCaption( lua_tostring(L, 1) );
	return 0;
}
