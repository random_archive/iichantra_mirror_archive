#ifndef __LUATHREAD_H_
#define __LUATHREAD_H_

#include "script.h"

class LuaThread
{
public:
	LuaThread(void)
	{
		lThread = NULL;
		refKey = LUA_NOREF;
		pausable = false;
	}

	lua_State* lThread;				// Coroutine

	LuaRegRef refKey;

	bool pausable;				// ����� ���� ���� (Wait), ����� �� ���������, ���� game_state == GAME_PAUSED
};

void ProcessThread(LuaRegRef r);
LuaRegRef NewThread(lua_State* L);

void RemoveAllThreads();

#endif // __LUATHREAD_H_