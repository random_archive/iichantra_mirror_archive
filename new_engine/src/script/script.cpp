#include "StdAfx.h"

#include "../misc.h"

#include "script.h"
#include "api.h"
#include "luathread.h"
#include <vector>
#include "../resource_mgr.h"

#include "udata.hpp"


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

lua_State* lua = NULL;

typedef std::map<LuaRegRef, int> RegRefCountsMap;
RegRefCountsMap regRefCounts;
typedef std::vector<LuaRegRef> RegRefInGame;
RegRefInGame regRefInGame;

extern char path_app[MAX_PATH];
extern char path_scripts[MAX_PATH];
extern ResourceMgr<Script>* scriptMgr;

//////////////////////////////////////////////////////////////////////////
// �������������� ���������� �������
bool SCRIPT::InitScript(int argc, char *argv[])
{
	if (lua)
	{
		FreeScript();
	}

	lua = luaL_newstate();			// ������� ����������� ������ Lua
	if ( lua == NULL )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Error creating Lua" );
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "Error creating Lua" );
		return true;
	}
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelInfo, "Lua created");

	luaL_openlibs ( lua );                              // open standart libraries

	lua_atpanic(lua, SCRIPT::Panic);

	RegisterAllTypeMetatables(lua);

	scriptApi::RegisterAPI(lua);			// �������������� ���������, ����������� C-�������.
	
	lua_pushinteger( lua, argc );
	lua_setglobal( lua, "_ARGC" );
	lua_newtable( lua );
	for ( int i = 0; i < argc; i++ )
	{
		lua_pushstring( lua, argv[i] );
		lua_rawseti( lua, -2, i+1 );
	}
	lua_setglobal( lua, "_ARGV" );

	return false;
}

// ����������� ���������� �������
void SCRIPT::FreeScript()
{
	if (lua)
	{
		RemoveAllThreads();

		// ���� �� ������ �������� - �����-�� ������, ��������, �������� ��� ��, ��� ������ ���� �����������.
		// �� ��������, �� ���������.
		assert(regRefCounts.size() == 0);

		lua_close(lua);
		lua = NULL;
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelInfo, "Lua destroyed" );
	}
}

// ����������� ��� ����������� ������� Lua
int SCRIPT::Panic(lua_State* L)
{
	const char* msg = lua_tostring(L, -1);
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
		"Lua Panic: %s", msg);
	Log(DEFAULT_LOG_NAME, logLevelError,
		"Lua Panic. Exiting.");
	//luaL_where(lua, 1);
	//msg = lua_tostring(L, -1);
	//Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
	//	"Traceback: %s", msg);
	return 0;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#ifdef LUA_ERROR_SCRIPT
void SCRIPT::ScriptError()
{
	lua_getglobal( lua, LUA_ERROR_SCRIPT );
	if ( lua_isfunction( lua, -1 ) )
	{
		lua_pcall( lua, 0, 0, 0 );
	}
}
#endif

// ��������� ���� � ��������� ���� �� ������� �����
bool SCRIPT::LoadFile( const char* filename )
{
	if (!filename)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"Error loading file. Filename is NULL" );
		return true;
	}

	if (!lua)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
			"Error loading file '%s'. Lua is not created", filename);
		return true;
	}

	//SetCurrentDirectory(path_scripts);
	ChangeDir(path_scripts);

	if(luaL_loadfile(lua, filename))
	{
		const char* err = lua_tostring(lua, -1);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "%s", err);
#ifdef LUA_ERROR_SCRIPT
		ScriptError();
#endif
		return true;
	}

	//SetCurrentDirectory(path_app);
	ChangeDir(path_app);
	// ����: func
	return false;
}

bool SCRIPT::LuaLoadFile( const char* filename )
{
	if (!filename)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"Error loading file. Filename is NULL" );
		return true;
	}

	if (!lua)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
			"Error loading file '%s'. Lua is not created", filename);
		return true;
	}

	Script* file = scriptMgr->GetByName( filename, DEFAULT_SCRIPTS_PATH );
	if ( !file ) return true;
	
	return file->LuaLoad();
}

// ��������� ������ ������� � ��������� ���� �� ������� �����
bool SCRIPT::LoadString( const char* str )
{
	if (!str)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error loading command. str is NULL" );
		return true;
	}

	if (!lua)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
			"Error loading comand: %s. Lua is not created.", str);
		return true;
	}

	if(luaL_loadstring(lua, str))
	{
		const char* err = lua_tostring(lua, -1);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "%s", err);
#ifdef LUA_ERROR_SCRIPT
		ScriptError();
#endif
		return true;
	}

	return false;
}

// ��������� ������ ������� � ��������� ���� �� ������� �����
bool SCRIPT::LoadFileBuffer( const char* name, const char* str )
{
	if (!str)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error loading command. str is NULL" );
		return true;
	}

	if (!lua)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError,
			"Error loading comand: %s. Lua is not created.", str);
		return true;
	}

	if(luaL_loadbuffer(lua, str, strlen(str), name))
	{
		const char* err = lua_tostring(lua, -1);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "%s", err);
#ifdef LUA_ERROR_SCRIPT
		ScriptError();
#endif
		return true;
	}

	return false;
}

// ��������� ������� (����) � ����� �����
int SCRIPT::ExecChunk()
{
	return SCRIPT::ExecChunk(0);
}

int SCRIPT::ExecChunk(UINT args)
{
	int level = lua_gettop(lua) - args - 1;	//���� ����� - ��������� - ���� �������
	if (lua_pcall(lua, args, LUA_MULTRET, 0))
	{
		const char* err = lua_tostring(lua, -1);
			Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "In ExecChunk(UINT args): %s", err);
#ifdef LUA_ERROR_SCRIPT
		ScriptError();
#endif
		return -1;
	}
	return lua_gettop(lua) - level;	//����� ���� ����� - ������ = ���������� �����������
}

//////////////////////////////////////////////////////////////////////////
// ������� � ������ ��, ��� ����� �� ������� �����, � ���������� ������ �� ����
// ��� ���� � ������� ����� ��� ���������
LuaRegRef SCRIPT::AddToRegistry()
{
	// ����: ... data
	return luaL_ref(lua, LUA_REGISTRYINDEX);	// ����:
}

// ������� �� ������� ���������� �� ������
void SCRIPT::RemoveFromRegistry(LuaRegRef& r)
{
	lua_unref(lua, r);
	r = LUA_NOREF;
}

// �������� �� �������� ����� �������� �� �������
void SCRIPT::GetFromRegistry(lua_State* L, LuaRegRef r)
{
	//lua_rawgeti(L, LUA_REGISTRYINDEX, r);
	lua_rawgeti(lua, LUA_REGISTRYINDEX, r);
	lua_xmove(lua, L, 1);
}

//////////////////////////////////////////////////////////////////////////

// �������� � procref ������ ������� ��� �� �������, ����������� � ����� ��� ������� n
void SCRIPT::RegProc(lua_State* L, LuaRegRef* procref, int n)
{
	luaL_argcheck(L, lua_isfunction(L, n) || lua_isnil(L, n), n, "Function or nil expected");

	ReleaseProc(procref);

	if(!lua_isnil(L, n))
	{
		// TODO: ������� ����������� ������� �� ����� ���� ����� � �������� �� ������
		// ������ �� ������ ���������� ���� ������� �������� �� �����, ��� ����� ���� � �� �������
		lua_xmove(L, lua, 1);
		*procref = SCRIPT::AddToRegistry();
		ReserveProc(*procref);
	}
}

// ����������� ������� ���������� ������������� ������ �� ������
void SCRIPT::ReserveProc(LuaRegRef procref)
{
	if (procref == LUA_NOREF)
		return;
	RegRefCountsMap::iterator it = regRefCounts.find(procref);
	if (it == regRefCounts.end())
	{
		regRefCounts[procref] = 1;
	}
	else
	{
		assert(it->second >= 0);
		++(it->second);
	}
}

// ��������������� �������. ������ ������������ ��� ����� ������������� � 
// ����������� ��������� ������ �� ������.
LuaRegRef SCRIPT::ReserveAndReturnProc(LuaRegRef procref)
{
	ReserveProc(procref);
	return procref;
}

// ��������� ������� ���������� ������������� ������ �� ������.

// ���� ������� ��������� 0, ������ �� ������� ���������.
void SCRIPT::ReleaseProc(LuaRegRef* procref)
{
	STACK_CHECK_INIT(lua);
	if (*procref == LUA_NOREF)
		return;
	RegRefCountsMap::iterator it = regRefCounts.find(*procref);
	if (it == regRefCounts.end())
		return;
	
	assert(it->second > 0);
	if (it->second <= 0)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "SCRIPT::ReleaseProc: procref %d references count is %d", *procref, it->second);
		it->second = 1;
	}
	
	--(it->second);
	if (it->second == 0)
	{
		SCRIPT::RemoveFromRegistry(*procref);
		*procref = LUA_NOREF;
		regRefCounts.erase(it);
	}
	STACK_CHECK(lua);
}

void SCRIPT::SetInGameRegRef(LuaRegRef procref)
{
	regRefInGame.push_back(procref);
}

void SCRIPT::ReleaseAllInGameRegRef()
{
	for (RegRefInGame::iterator it = regRefInGame.begin(); it != regRefInGame.end(); ++it)
	{
		ReleaseProc(&*it);
	}
	regRefInGame.clear();
}


//////////////////////////////////////////////////////////////////////////

// ������� ���� � ��������� ���� �������
int SCRIPT::ExecFile( const char* filename )
{
	if (lua)
	{
		lua_pop(lua, lua_gettop(lua));
		return DoFile( filename );
	}
	return -1;
}

// ������� ���� � ��������� ������ �������
int SCRIPT::ExecString( const char* str )
{
	return !LoadString(str) ? ExecChunk() : -1;
}

// ��������� ���� ������� � ������� ��� � ������
int SCRIPT::LoadFileToReg(const char* filename)
{
	return !LoadFile(filename) ? AddToRegistry() : 1;
}

// ��������� ������ ������� � ������� � � ������
int SCRIPT::LoadStringToReg(const char* str)
{
	return !LoadString(str) ? AddToRegistry() : 1;
}

// ����� ������� �� ������� � ���������� ��
// � ������ ������ ���������� true
int SCRIPT::ExecChunkFromReg(LuaRegRef r, UINT args)
{
	GetFromRegistry(lua, r);

	int i = lua_gettop(lua) - args;
	if (i <= 0)
		i = 1;

	lua_insert(lua, i);

	return ExecChunk(args);
}

int SCRIPT::ExecChunkFromReg(LuaRegRef r)
{
	return SCRIPT::ExecChunkFromReg(r, 0);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



void SCRIPT::GetFunctionFieldByName(lua_State* L, const char* name, int* value )
{
	lua_getfield(L, -1, name);
	if (lua_isfunction(L, -1))
	{
		SCRIPT::RegProc(L, static_cast<LuaRegRef*>(value), -1);
	}
	else
	{
		*value = LUA_NOREF;
		lua_pop(L, 1);
	}
}

// �������� ��������� ������ ������ �������� ������� �� ������� � ����� �� ������� n.
void SCRIPT::GetColorFromTable(lua_State* L, int n, RGBAf& val)
{
	if (lua_istable(L, n))
	{
		// ����: ..., color, ...
		lua_rawgeti(L, n, 1);	val.r = (float)lua_tonumber(L, -1); lua_pop(L, 1);
		lua_rawgeti(L, n, 2);	val.g = (float)lua_tonumber(L, -1); lua_pop(L, 1);
		lua_rawgeti(L, n, 3);	val.b = (float)lua_tonumber(L, -1); lua_pop(L, 1);
		lua_rawgeti(L, n, 4);	val.a = (float)lua_tonumber(L, -1); lua_pop(L, 1);
	}
}

void SCRIPT::GetIntVectorFieldByName(lua_State* L, const char* name, vector<int>& val, bool add)
{
	// ����: ..., table
	lua_getfield(L, -1, name);	// ����: ..., table, name
	//val = new vector<int>;
	if (lua_istable(L, -1))
	{
		size_t size = lua_objlen( L, -1 );
		if ( !add && val.size() > 0 )
			val.clear();
		for ( size_t i = 1; i <= size; i++ )
		{
			lua_rawgeti( L, -1, static_cast<int>(i) );
			if ( lua_isnumber(L, -1) )
				val.push_back( static_cast<const int>(lua_tointeger(L, -1)) );
			lua_pop( L, 1 );
		}
	}
	lua_pop(L, 1);		// ����: ..., table
}

__INLINE bool clockwise( Vector2 p1, Vector2 p2, Vector2 p3 )
{
	return (p2.x - p1.x)*(p3.y - p1.y)-(p2.y - p1.y)*(p3.x - p1.x) >= 0.0f;
}

CPolygon* SCRIPT::GetPolygonField( lua_State* L, int depth )
{
	if ( !lua_istable(L, depth) ) return NULL;
	int vertices = static_cast<int>(lua_objlen(L, depth));
	CPolygon* poly = new CPolygon( vertices );
	Vector2 p1, p2, p3;
	for ( int i = 1; i <= vertices; i++ )
	{
		p1 = p2;
		p2 = p3;
		p3.x = p3.y = 0.0f;
		lua_rawgeti( L, depth, i );
		if ( lua_istable( L, -1 ) )
		{
			lua_rawgeti( L, -1, 1 );
			p3.x = static_cast<float>(lua_tonumber( L, -1 ));
			lua_pop( L, 1 );
			lua_rawgeti( L, -1, 2 );
			p3.y = static_cast<float>(lua_tonumber( L, -1 ));
			lua_pop( L, 1 );
		}
		if ( i >= 3 && !clockwise( p1, p2, p3 ) ) 
		{
			DELETESINGLE( poly );	//We cannot use non-convex polygons or non-clockwise vertices
			return NULL;
		}
		(*poly)[i-1] = Vector2(p3.x, p3.y);
		lua_pop( L, 1 );
	}
	if ( poly->GetVertexCount() > 2 )	//We cannot use a line or point - it would lead to AABB having zero width or height
	{
		int vertices = static_cast<int>(poly->GetVertexCount());
		if ( !clockwise( (*poly)[vertices-1], (*poly)[         1], (*poly)[2] ) || 
			 !clockwise( (*poly)[vertices-2], (*poly)[vertices-1], (*poly)[1] ) )
		{
			DELETESINGLE( poly );	//We cannot use non-convex polygons or non-clockwise vertices
			return NULL;
		}
		poly->CalcBox();
		CBox box = poly->GetBox();
		float center_x = (box.Max.x + box.Min.x)/2.0f;
		float center_y = (box.Max.y + box.Min.y)/2.0f;
		for ( int i = 0; i < (int)poly->GetVertexCount(); i++ )
		{
			(*poly)[i].x -= center_x;
			(*poly)[i].y -= center_y;
		}
	}
	else return NULL;
	poly->CalcBox();
	
	return poly;
}

//////////////////////////////////////////////////////////////////////////

// ������� � ��� ���� ����������� �����. ������������ ��� ������� �������.
void SCRIPT::StackDumpToLog(lua_State *L)
{
	int i;
	int top = lua_gettop(L);
	string s("");
	char buf[20];
#ifdef WIN32
	sprintf_s(buf, 20, "0x%p: ", (void*)L);
#else
	snprintf(buf, 20, "0x%p: ", (void*)L);
#endif // WIN32
	s += buf;

	for (i = 1; i <= top; i++)
	{  /* repeat for each level */
		int t = lua_type(L, i);
		switch (t)
		{
		case LUA_TSTRING: /* strings */
			s += string("'") + lua_tostring(L, i) + "'";
			break;

		case LUA_TBOOLEAN: /* booleans */
			s += lua_toboolean(L, i) ? "true" : "false";
			break;

		case LUA_TNUMBER: /* numbers */
			// TODO: sprintf_s � snprintf ������� ���������� ����������.
			// sprintf_s �����������, ��� � ����� ������� ����� 0.
#ifdef WIN32
			sprintf_s(buf, 20, "%f", lua_tonumber(L, i));
#else
			snprintf(buf, 20, "%f", lua_tonumber(L, i));
#endif // WIN32
			s += string(buf);
			break;

		case LUA_TTHREAD:
			//char buf[20];
			// TODO: sprintf_s � snprintf ������� ���������� ����������.
			// sprintf_s �����������, ��� � ����� ������� ����� 0.
#ifdef WIN32
			sprintf_s(buf, 20, "0x%p", (void*)lua_tothread(L, i));
#else
			snprintf(buf, 20, "0x%p", (void*)lua_tothread(L, i));
#endif // WIN32

			s += "thread " + string(buf);
			break;

		default: /* other values */
			s += lua_typename(L, t);
			break;

		}
		s += ", "; /* put a separator */
	}
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelScript, s.c_str());
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// �������� � ���� �������, �������������� ������
void SCRIPT::PushVector(lua_State* L, Vector2& v)
{
	// ����: obj
	lua_createtable(L, 0, 2);	// ����: obj vector

	lua_pushnumber(L, v.x); lua_setfield(L, -2, "x");
	lua_pushnumber(L, v.y); lua_setfield(L, -2, "y");
}

// �������� � ���� �������, �������������� AABB
void SCRIPT::PushAABB(lua_State* L, CAABB& a)
{
	// ����: obj
	lua_createtable(L, 0, 3);	// ����: obj aabb

	PushVector(L, a.p);		lua_setfield(L, -2, "p");
	lua_pushnumber(L, a.H); lua_setfield(L, -2, "H");
	lua_pushnumber(L, a.W); lua_setfield(L, -2, "W");
}

void SCRIPT::PushPolygon(lua_State* L, CPolygon& p)
{
	UINT size = p.GetVertexCount();
	lua_createtable(L, size, 0);

	for ( UINT i = 1; i <= size; i++ )
	{
		lua_createtable(L, 2, 0);
		lua_pushnumber(L, p[i-1].x); lua_rawseti(L, -2, 1);
		lua_pushnumber(L, p[i-1].y); lua_rawseti(L, -2, 2);
		lua_rawseti(L, -2, i);
	}
}

int SCRIPT::DoFile( const char* name )
{
	Script* scr = scriptMgr->GetByName( name, DEFAULT_SCRIPTS_PATH );
	if ( scr ) 
	{
		SCRIPT::DbgSetFile( name );
		return scr->Execute();
	}
	else return -1;
}

void SCRIPT::DbgSetFile( const char* name )
{
#ifdef DEBUG_LUA_CONSTANTS
	lua_pushstring( lua, name );
	lua_setglobal( lua, "__dbg_cur" );
#endif //DEBUG_LUA_CONSTANTS
}

bool Script::Load()
{
	if( buffer != NULL )
	{
		if ( _buffer ) DELETEARRAY( buffer );
		_buffer = new char[ buffer_size + 1 ];
		memcpy( _buffer, buffer, buffer_size );
		_buffer[ buffer_size ] = 0;
		_file = NULL;
		return true;
	}
	return false;
}

bool Script::LuaLoad()
{
	if ( _buffer != NULL )
	{
		return SCRIPT::LoadFileBuffer( name.c_str(), _buffer );
	}
	if ( _file != NULL )
	{
		return SCRIPT::LoadFile( _file );
	}
	return true;
}

int Script::Execute()
{
	if( _buffer != NULL )
	{
		if ( !SCRIPT::LoadFileBuffer( name.c_str(), _buffer ) )
			return SCRIPT::ExecChunk();
		else return -1;
	}
	if( _file != NULL )
	{
		if ( !SCRIPT::LoadFile( _file ) )
		{
			return SCRIPT::ExecChunk();
		}
		else return -1;
	}
	return -1;
}

void SCRIPT::OnChatMessage( char* nick, char* message )
{
	//TODO: script from registry
	lua_getglobal( lua, "OnChatMessage" );
	if ( lua_isfunction( lua, -1 ) )
	{
		lua_pushstring( lua, nick );
		lua_pushstring( lua, message );
		SCRIPT::ExecChunk( 2 );
	}
	lua_pop( lua, 1 );
}

void SCRIPT::LogLuaCallStack( lua_State* L, const char* fname, LogLevel level)
{
	lua_Debug entry;
	int depth = 0;
	 
	while (lua_getstack(L, depth, &entry))
	{
		int status = lua_getinfo(L, "Sln", &entry);
	        assert(status);
		UNUSED_ARG( status );
	 
	        Log(fname, level, "%s(%d): %s", entry.short_src, entry.currentline, entry.name ? entry.name : "?");
		depth++;
	}
}
