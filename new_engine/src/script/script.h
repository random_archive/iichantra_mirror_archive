#ifndef __SCRIPT_H_
#define __SCRIPT_H_

#include <lua.hpp>

#include "lua_pusher.hpp"

#include "../resource.h"
#include "../game/phys/phys_misc.h"
#include "../game/phys/2de_RGBA.h"
#include "../game/phys/2de_Geometry.h"

namespace SCRIPT
{
#ifdef LUA_ERROR_SCRIPT
	void ScriptError();
#endif

	void LogLuaCallStack(lua_State* L, const char* fname, LogLevel level);

	bool InitScript(int argc, char *argv[]);
	void FreeScript();
	int Panic(lua_State* L);
	
	bool LuaLoadFile(const char* filename);	//LuaLoadFile tries to load script from archive first
	bool LoadFile(const char* filename);	//LoadFile doesn't
	bool LoadString(const char* str);
	bool LoadFileBuffer(const char* name, const char* str);
	int ExecChunk();
	int ExecChunk(UINT args);

	LuaRegRef AddToRegistry();
	void RemoveFromRegistry(LuaRegRef& r);
	void GetFromRegistry(lua_State* L, LuaRegRef r);

	void RegProc(lua_State* L, LuaRegRef* procref, int n);
	void ReserveProc(LuaRegRef procref);
	LuaRegRef ReserveAndReturnProc(LuaRegRef procref);
	void ReleaseProc(LuaRegRef* procref);
	void SetInGameRegRef(LuaRegRef procref);
	void ReleaseAllInGameRegRef();

	int ExecFile(const char* filename);
	int ExecString(const char* str);
	int ExecChunkFromReg(LuaRegRef r);
	int ExecChunkFromReg(LuaRegRef r, UINT args);
	int LoadFileToReg(const char* filename);
	int LoadStringToReg(const char* str);


	template<typename T>
	void GetFieldByName(lua_State* L, const char* name, T& val)
	{
		lua_getfield(L, -1, name);			// ST: table, [name]
		if (CHECK_INPUT_PARAM(L, -1, val))
		{
			getFromLua(L, -1, val);
		}
		lua_pop(L, 1);                      // ST: table
	}

	void GetColorFromTable(lua_State* L, int n, RGBAf& val);
	void GetIntVectorFieldByName(lua_State* L, const char* name, vector<int>& val, bool add);
	void GetFunctionFieldByName(lua_State* L, const char* name, int* val);
	
	CPolygon* GetPolygonField(lua_State* L, int depth);

	void StackDumpToLog(lua_State *L);

	void PushVector(lua_State* L, Vector2& v);
	void PushAABB(lua_State* L, CAABB& a);
	void PushPolygon(lua_State* L, CPolygon& p);

	void DbgSetFile( const char* name );
	void OnChatMessage( char* nick, char* message );

	int DoFile( const char* name );
}

class Script: public Resource
{
public:

	Script(string name, unsigned char* buffer, size_t buffer_size) : Resource(name, buffer, buffer_size)
	{
		_buffer = NULL;
		_file = NULL;
	}

	~Script()
	{
		if( _buffer ) DELETEARRAY( _buffer );
		if( _file ) DELETEARRAY( _file );
	}

	virtual bool Load();
	bool LuaLoad();
	int Execute();
private:
	char* _buffer;
	char* _file;
};

//#define SCRIPTLOG(x) LogToFile( DEFAULT_SCRIPT_LOG_NAME, string(x)+"\n" );

#ifdef _DEBUG
#define STACK_CHECK_INIT(L) int stackCountBefore = lua_gettop(L); int stackCountAfter = -1;
#define STACK_CHECK(L)  { stackCountAfter = lua_gettop(L); assert(stackCountBefore == stackCountAfter); }
#else
#define STACK_CHECK_INIT(L)		{(void)L;}
#define STACK_CHECK(L)			{(void)L;}
#endif // _DEBUG

#endif // __SCRIPT_H_
