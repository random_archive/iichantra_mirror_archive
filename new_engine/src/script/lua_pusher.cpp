#include "StdAfx.h"

#include "lua_pusher.hpp"

#include <lua.hpp>

#include "../game/phys/2de_Math.h"
#include "../game/phys/phys_misc.h"
#include "../game/objects/object.h"
#include "../game/objects/object_enemy.h"

// int
template <>
int pushToLua(lua_State* L, int const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
int getFromLua(lua_State* L, int idx)
{
	return static_cast<int>(lua_tointeger(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, int& val)
{
	val = static_cast<int>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<int>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// unsigned int
template <>
int pushToLua(lua_State* L, unsigned int const & val)
{
	lua_pushnumber(L, val);
	return 1;
}

template<>
unsigned int getFromLua(lua_State* L, int idx)
{
	return static_cast<unsigned int>(lua_tonumber(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, unsigned int& val)
{
	val = static_cast<unsigned int>(lua_tonumber(L, idx));
}

template<>
bool CHINP_TESTER<unsigned int>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// long
template <>
int pushToLua(lua_State* L, long const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
long getFromLua(lua_State* L, int idx)
{
	return static_cast<long>(lua_tointeger(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, long& val)
{
	val = static_cast<long>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<long>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// unsigned long
template <>
int pushToLua(lua_State* L, unsigned long const & val)
{
	lua_pushnumber(L, val);
	return 1;
}

template<>
unsigned long getFromLua(lua_State* L, int idx)
{
	return static_cast<unsigned long>(lua_tonumber(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, unsigned long& val)
{
	val = static_cast<unsigned long>(lua_tonumber(L, idx));
}

template<>
bool CHINP_TESTER<unsigned long>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// short
template <>
int pushToLua(lua_State* L, short const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
short getFromLua(lua_State* L, int idx)
{
	return static_cast<short>(lua_tointeger(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, short& val)
{
	val = static_cast<short>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<short>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// unsigned short
template <>
int pushToLua(lua_State* L, unsigned short const & val)
{
	lua_pushnumber(L, val);
	return 1;
}

template<>
unsigned short getFromLua(lua_State* L, int idx)
{
	return static_cast<unsigned short>(lua_tonumber(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, unsigned short& val)
{
	val = static_cast<unsigned short>(lua_tonumber(L, idx));
}

template<>
bool CHINP_TESTER<unsigned short>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// float
template <>
int pushToLua(lua_State* L, float const & val)
{
	lua_pushnumber(L, val);
	return 1;
}

template<>
float getFromLua(lua_State* L, int idx)
{
	return static_cast<float>(lua_tonumber(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, float& val)
{
	val = static_cast<float>(lua_tonumber(L, idx));
}

template<>
bool CHINP_TESTER<float>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// double
template <>
int pushToLua(lua_State* L, double const & val)
{
	lua_pushnumber(L, val);
	return 1;
}

template<>
double getFromLua(lua_State* L, int idx)
{
	return static_cast<double>(lua_tonumber(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, double& val)
{
	val = static_cast<double>(lua_tonumber(L, idx));
}

// char
template <>
int pushToLua(lua_State* L, char const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

// unsigned char
template <>
int pushToLua(lua_State* L, unsigned char const & val)
{
	lua_pushnumber(L, val);
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, unsigned char& val)
{
	val = static_cast<unsigned char>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<unsigned char>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// bool
template <>
int pushToLua(lua_State* L, bool const & val)
{
	lua_pushboolean(L, val);
	return 1;
}

template<>
bool getFromLua(lua_State* L, int idx)
{
	return lua_toboolean(L, idx) != 0;
}

template<>
void getFromLua(lua_State* L, int idx, bool& val)
{
	val = (lua_toboolean(L, idx) != 0);
}

template<>
bool CHINP_TESTER<bool>(lua_State* L, int idx)
{
	return lua_isboolean(L, idx) != 0;
}

// const char*
template <>
int pushToLua(lua_State* L, const char* const & val)
{
	lua_pushstring(L, val);
	return 1;
}

template<>
const char* getFromLua(lua_State* L, int idx)
{
	return lua_tostring(L, idx);
}

template<>
void getFromLua(lua_State* L, int idx, const char * & val)
{
	val = lua_tostring(L, idx);
}

template<>
bool CHINP_TESTER<const char *>(lua_State* L, int idx)
{
	return lua_isstring(L, idx) != 0;
}

// char*
template <>
int pushToLua(lua_State* L, char* const & val)
{
	lua_pushstring(L, val);
	return 1;
}

template<>
char* getFromLua(lua_State* L, int idx)
{
	return StrDupl(lua_tostring(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, char * & val)
{
	// If it failed here, there is, probably, memory leak.
	// Please, be very careful to pass non-NULL pointers here.
	assert(val == NULL);
	val = StrDupl(lua_tostring(L, idx));
}

template<>
bool CHINP_TESTER<char *>(lua_State* L, int idx)
{
	return lua_isstring(L, idx) != 0;
}

// std::string
template <>
int pushToLua(lua_State* L, std::string const & val)
{
	lua_pushstring(L, val.c_str());
	return 1;
}

template<>
std::string getFromLua(lua_State* L, int idx)
{
	size_t size = 0;
	const char* s = lua_tolstring(L, idx, &size);
	return std::string(s, size);
}

template<>
void getFromLua(lua_State* L, int idx, std::string& val)
{
	size_t size = 0;
	const char* s = lua_tolstring(L, idx, &size);
	val.assign(s, size);
}

template<>
bool CHINP_TESTER<std::string>(lua_State* L, int idx)
{
	return lua_isstring(L, idx) != 0;
}

// RGBAf
template <>
int pushToLua(lua_State* L, RGBAf const & val)
{
	lua_createtable(L, 4, 0);	// st: RGBA
	pushToLua(L, val.r);  lua_rawseti(L, -2, 1);
	pushToLua(L, val.g);  lua_rawseti(L, -2, 2);
	pushToLua(L, val.b);  lua_rawseti(L, -2, 3);
	pushToLua(L, val.a);  lua_rawseti(L, -2, 4);
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, RGBAf& val)
{
	SCRIPT::GetColorFromTable(L, idx, val);
}

template<>
bool CHINP_TESTER<RGBAf>(lua_State* L, int idx)
{
	return lua_istable(L, idx) != 0;
	/// @todo: may be check the content of the table in debug version
}

// Vector2
template <>
int pushToLua(lua_State* L, Vector2 const & val)
{
	lua_createtable(L, 0, 2);	// st: Vector2
	pushToLua(L, val.x); lua_setfield(L, -2, "x");
	pushToLua(L, val.y); lua_setfield(L, -2, "y");
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, Vector2& val)
{
	if (lua_istable(L, idx))
	{
		// st: Vector2, ...
		lua_getfield(L, idx, "x"); getFromLua(L, -1, val.x); lua_pop(L, 1);
		lua_getfield(L, idx, "y"); getFromLua(L, -1, val.y); lua_pop(L, 1);
	}
}

template<>
bool CHINP_TESTER<Vector2>(lua_State* L, int idx)
{
	return lua_istable(L, idx) != 0;
	/// @todo: may be check the content of the table in debug version
}

// CAABB
template <>
int pushToLua(lua_State* L, CAABB const & val)
{
	lua_createtable(L, 0, 3);	// st: CAABB
	pushToLua(L, val.p); lua_setfield(L, -2, "p");
	pushToLua(L, val.H); lua_setfield(L, -2, "H");
	pushToLua(L, val.W); lua_setfield(L, -2, "W");
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, CAABB& val)
{
	if (lua_istable(L, idx))
	{
		// st: CAABB, ...
		lua_getfield(L, idx, "p"); getFromLua(L, -1, val.p); lua_pop(L, 1);
		lua_getfield(L, idx, "H"); getFromLua(L, -1, val.H); lua_pop(L, 1);
		lua_getfield(L, idx, "W"); getFromLua(L, -1, val.W); lua_pop(L, 1);
	}
}

template<>
bool CHINP_TESTER<CAABB>(lua_State* L, int idx)
{
	return lua_istable(L, idx) != 0;
	/// @todo: may be check the content of the table in debug version
}

// ObjectType
template <>
int pushToLua(lua_State* L, ObjectType const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

// TrajectoryType
template <>
int pushToLua(lua_State* L, TrajectoryType const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, TrajectoryType& val)
{
	val = static_cast<TrajectoryType>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<TrajectoryType>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// ObjectFacingType
template <>
int pushToLua(lua_State* L, ObjectFacingType const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, ObjectFacingType& val)
{
	val = static_cast<ObjectFacingType>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<ObjectFacingType>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// CharacterOffscreenBeh
template <>
int pushToLua(lua_State* L, CharacterOffscreenBeh const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, CharacterOffscreenBeh& val)
{
	val = static_cast<CharacterOffscreenBeh>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<CharacterOffscreenBeh>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// LogLevel
template <>
int pushToLua(lua_State* L, LogLevel const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
LogLevel getFromLua(lua_State* L, int idx)
{
	return static_cast<LogLevel>(lua_tointeger(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, LogLevel& val)
{
	val = static_cast<LogLevel>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<LogLevel>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// AnimationFrameCommand
template <>
int pushToLua(lua_State* L, AnimationFrameCommand const & val)
{
	lua_pushinteger(L, val);
	return 1;
}

template<>
AnimationFrameCommand getFromLua(lua_State* L, int idx)
{
	return static_cast<AnimationFrameCommand>(lua_tointeger(L, idx));
}

template<>
void getFromLua(lua_State* L, int idx, AnimationFrameCommand& val)
{
	val = static_cast<AnimationFrameCommand>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<AnimationFrameCommand>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}

// GameObject*
template<>
int pushToLua(lua_State* L, GameObject* const & val)
{
	if ( val != NULL )
		val->pushUData(L);
	else
		lua_pushnil(L);

	return 1;
}

template<>
bool CHINP_TESTER<GameObject*>(lua_State* L, int idx)
{
	return lua_isuserdata(L, idx) != 0;
}

// ObjPhysic*
template<>
int pushToLua(lua_State* L, ObjPhysic* const & val)
{
	if ( val != NULL )
		val->pushUData(L);
	else
		lua_pushnil(L);

	return 1;
}

template<>
bool CHINP_TESTER<ObjPhysic*>(lua_State* L, int idx)
{
	return lua_isuserdata(L, idx) != 0;
}

//BlendingMode
template<>
int pushToLua(lua_State* L, BlendingMode const & val)
{
	lua_pushinteger(L, val);

	return 1;
}

template<>
void getFromLua(lua_State* L, int idx, BlendingMode& val)
{
	val = static_cast<BlendingMode>(lua_tointeger(L, idx));
}

template<>
bool CHINP_TESTER<BlendingMode>(lua_State* L, int idx)
{
	return lua_isnumber(L, idx) != 0;
}