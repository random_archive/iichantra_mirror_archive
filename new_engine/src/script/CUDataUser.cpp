#include "StdAfx.h"

#include "CUDataUser.hpp"

#include "CUData.hpp"

#include "udata.hpp"

#include "script.h"

#include "../misc.h"

CUDataUser::~CUDataUser()
{
	if (pUdata)
		pUdata->clearUser();
}

//LuaRegRef CUDataUser::getUData()
//{
//	//if (!pUdata)
//	//{
//	//	pUdata = this->createUData();
//	//}
//
//	if (!pUdata)
//		return LuaRegRef();
//	else
//		return pUdata->getRegRef();
//}


int CUDataUser::pushUData(lua_State* L) 
{
	if (!pUdata)
	{
		pUdata = this->createUData();
	}

	if (!pUdata)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "CUDataUser::pushUData: userdata creation failed");
		return 0;
	}
	else
	{
		SCRIPT::GetFromRegistry(L, pUdata->getRegRef());
		return 1;
	}
}