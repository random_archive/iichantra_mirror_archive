--Deleting any value will set it to default

CONFIG_PARTICLES =
{
	[constants.pIntensity] = 1.0;
	[constants.pRelativeSpeed] = 0.2;
	[constants.pUsePhysic] = 1;
	[constants.pIncludeOneSided] = 1;
	[constants.pIncludePolygons] = 0;
	[constants.pMaxSimpleParticles] = 4096;
	[constants.pMaxPhysParticles] = 1024;
	[constants.pMaxWeatherParticles] = 2048;
}

CONFIG_EXTRA =
{
	UseFlares = 1;
	ClassicPhysics = 0;
	DamageFlash = 1;
}

LoadParticlesConfig( CONFIG_PARTICLES )
