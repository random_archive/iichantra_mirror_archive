CONFIG = 
{	window_width = 1024;
	window_height = 768;
	scr_width = 1024;
	scr_height = 768;
	near_z = -1.100000;
	far_z = 1.100000;
	bpp = 32;
	fullscreen = 0;
	vert_sync = 1;
	debug = 0;
	show_fps = 1;

	log_level = constants.logLevelWarning;
	
	gametick = 10;

	
	backcolor_r = 0.280000;
	backcolor_g = 0.280000;
	backcolor_b = 0.280000;
	
	volume = 1.000000;
	volume_music = 0.500000;
	volume_sound = 1.000000;
	
	-- controls

	key_conf = {
		{
			left = keys["left"];
			right = keys["right"];
			down = keys["down"];
			up = keys["up"];
			jump = keys["x"];
			sit = keys["z"];
			fire = keys["c"];
			change_weapon = keys["lshift"];
			change_player = keys["a"];
			gui_nav_accept = keys["enter"];
			gui_nav_decline = keys["x"];
			gui_nav_prev = keys["up"];
			gui_nav_next = keys["down"];
			gui_nav_menu = keys["esc"];
			gui_nav_screenshot = keys["f11"];
			player_use = keys["d"];
			weapon_slot1 = 0;
			weapon_slot2 = 0;
			weapon_slot3 = 0;
			weapon_slot4 = 0;
		},
		{
			left = keys["joystick_hat_left"];
			right = keys["joystick_hat_right"];
			down = keys["joystick_hat_down"];
			up = keys["joystick_hat_up"];
			jump = keys["joystick_3"];
			sit = keys["joystick_4"];
			fire = keys["joystick_1"];
			change_weapon = keys["joystick_2"];
			change_player = keys["joystick_6"];
			gui_nav_accept = keys["joystick_1"];
			gui_nav_decline = keys["joystick_3"];
			gui_nav_prev = keys["joystick_hat_up"];
			gui_nav_next = keys["joystick_hat_down"];
			gui_nav_menu = keys["joystick_10"];
			gui_nav_screenshot = keys["f11"];
			player_use = 0;
			weapon_slot1 = 0;
			weapon_slot2 = 0;
			weapon_slot3 = 0;
			weapon_slot4 = 0;
		},
		{
			left = 0;
			right = 0;
			down = 0;
			up = 0;
			jump = 0;
			sit = 0;
			fire = 0;
			change_weapon = 0;
			change_player = 0;
			gui_nav_accept = keys["c"];
			gui_nav_decline = 0;
			gui_nav_prev = 0;
			gui_nav_next = 0;
			gui_nav_menu = 0;
			gui_nav_screenshot = 0;
			player_use = 0;
			weapon_slot1 = 0;
			weapon_slot2 = 0;
			weapon_slot3 = 0;
			weapon_slot4 = 0;
		},
		{
			left = 0;
			right = 0;
			down = 0;
			up = 0;
			jump = 0;
			sit = 0;
			fire = 0;
			change_weapon = 0;
			change_player = 0;
			gui_nav_accept = 0;
			gui_nav_decline = 0;
			gui_nav_prev = 0;
			gui_nav_next = 0;
			gui_nav_menu = 0;
			gui_nav_screenshot = 0;
			player_use = 0;
			weapon_slot1 = 0;
			weapon_slot2 = 0;
			weapon_slot3 = 0;
			weapon_slot4 = 0;
		}
	};
	joystick_sensivity = 1000;
	
	-- gui settings

	gui_nav_mode = 1;
	gui_nav_cycled = 1;
	
	-- game

	language = "english";
	shadows = 1;
	weather = 1;
}
LoadConfig();
