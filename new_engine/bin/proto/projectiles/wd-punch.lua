--phys_ghostlike = 1;

ghost_to = constants.physBullet + constants.physSprite + constants.physEffect;

bullet_damage = 30;
push_force = 2;
bullet_vel = 0;

hurts_same_type = 1;
multiple_targets = 1;
faction_hates = {3, -1, -2}

animations = 
{
	{ 
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 127 },
			{ com = constants.AnimComInitW; param = 300 },
			{ com = constants.AnimComInitH; param = 80 },
			{ com = constants.AnimComAdjustAccY; param = 5000 },
			{ dur = 300 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "die";
		frames =
		{
			{ dur = 300 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
