texture = "pear15-grenade";

z = -0.002;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 23 },
			{ com = constants.AnimComInitH; param = 23 },
			{ com = constants.AnimComRealX; param = -5 },
			{ com = constants.AnimComRealY; param = -5 },
			{ param = function( obj )
					obj:max_x_vel( 1000 )
					obj:max_y_vel( 1000 )
					local angle = -math.pi/2 + (2 * (math.random()-0.5)) * math.pi/16
					obj:vel( { x = 8 * math.cos(angle), y = 8 * math.sin(angle) } )
					obj:gravity( { x = 0, y = 0.4 } )
			end },
			{ dur = 1},
			{ dur = 80; num = 0; com = constants.AnimComDrop; param = 1 }

		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-grenade-shard" },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
