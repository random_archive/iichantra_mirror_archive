name = "btard-grenade";

physic = 1;


-- �������� ����
bullet_damage = 0;
bullet_vel = 20;

-- �������� ������� ����
texture = "grenade";

bounce = 0.75;

--0-4

local TIME = 750
local ARC = 100

local function grenade_processor( obj )
	local t = mapvar.tmp[obj]
	if t and t.desired_pos then
		local ab = obj:aabb()
		ab.p.x = ab.p.x + (t.desired_pos[1] - ab.p.x) * 0.25
		ab.p.y = ab.p.y + (t.desired_pos[2] - ab.p.y) * 0.25
		obj:aabb(ab)
	end
end

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ dur = 50, param = function( obj )
				local t = mapvar.tmp[obj]
				t.frame = t.frame + 1
				if t.frame > 4 then
					t.frame = 0
				end
				t.progress = (Loader.time - t.first_time) / TIME
				if t.progress > 1 then
					SetObjAnim( obj, "die", false )
					return
				end
				t.desired_pos = { t.start.x + ( t.target.x - t.start.x ) * t.progress, 
				                  t.start.y + ( t.target.y - t.start.y ) * t.progress - ARC * math.sin( t.progress * math.pi ) }
				
				return 0, nil, t.frame
			end },
			{ com = constants.AnimComLoop }
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 12000 },
			{ com = constants.AnimComSetGravity }, 
			{ com = constants.AnimComRealX; param = 10 },
			{ com = constants.AnimComRealY; param = 10 },
			{ com = constants.AnimComRealW; param = 11 },
			{ com = constants.AnimComRealH; param = 11 },
			{ com = constants.AnimComSetMaxVelX; param = 0 },
			{ com = constants.AnimComSetMaxVelY; param = 0 },
			{ com = constants.AnimComStop },
			{ hur = 1, dur = 1 },
			{ param = function( obj )
				local t = mapvar.tmp[obj]
				t.frame = -1
				t.first_time = Loader.time
				SetObjProcessor( obj, grenade_processor )
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "straight" }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "straight" }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion" },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
