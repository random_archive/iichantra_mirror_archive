physic = 1;
phys_ghostlike = 1;
push_force = 4.0;
damage_type = Game.damage_types.substances;

phys_max_y_vel = 2;

bullet_damage = 30;
bullet_vel = 0;
multiple_targets = 1;

texture = "penguin_omsk";

ghost_to = constants.physEverything - constants.physPlayer;

z = 0;

bounce = 0;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComSetAccY; param = -800 },
			{ com = constants.AnimComRealW; param = 20 },
			{ com = constants.AnimComRealH; param = 11 },
			{ dur = 70; num = 22 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks"; },
			{ com = constants.AnimComRealW; param = 26 },
			{ com = constants.AnimComRealH; param = 17 },
			{ dur = 100; num = 23 },
			{ com = constants.AnimComRealW; param = 42 },
			{ com = constants.AnimComRealH; param = 28 },
			{ dur = 100; num = 24 },
			{ com = constants.AnimComRealW; param = 59 },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 25 },
			{ com = constants.AnimComRealW; param = 55 },
			{ com = constants.AnimComRealH; param = 29 },
			{ dur = 100; num = 26 },
			{ com = constants.AnimComRealW; param = 53 },
			{ com = constants.AnimComRealH; param = 28 },
			{ dur = 100; num = 27 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}