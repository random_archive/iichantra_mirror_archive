--forbidden
name = "bomb";

physic = 1;

reload_time = 500;
bullets_per_shot = 0;

-- �������� ����
bullet_damage = 15;
bullet_vel = 0;

-- �������� ������� ����
texture = "heli";

z = -0.002;

--bounce = 0.75;

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ dur = 100; num = 2 },
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0; num = 2 },
			{ com = constants.AnimComPushInt; param = 400; num = 2 },
			{ com = constants.AnimComSetGravity; num = 2 }, 
			{ com = constants.AnimComRealX; param = 0; num = 2 },
			{ com = constants.AnimComRealY; param = 0; num = 2 },
			{ com = constants.AnimComRealW; param = 9; num = 2 },
			{ com = constants.AnimComRealH; param = 19; num = 2 },
			{ com = constants.AnimComSetMaxVelX; param = 5000; num = 2 },
			{ com = constants.AnimComSetMaxVelY; param = 8000; num = 2 },
			{ com = constants.AnimComSetAnim; txt = "fly"; num = 2 }
		}
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "straight"; num = 2 }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{

			{ com = constants.AnimComSetAnim; txt = "straight"; num = 2 }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "die"}
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop; num = 2 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-bomb"; num = 2 },
			{ com = constants.AnimComDestroyObject; num = 2 }
		}
	}
}