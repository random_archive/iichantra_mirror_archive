can_hit_multiple_targets = 1;


-- �������� ����
bullet_damage = 10;
bullet_vel = 1.5;

-- �������� ������� ����
texture = "flamer-orange";

z = -0.002;

color = { 1, 1, 1, .75 }
blendingMode = constants.bmSrcA_One

ghost_to = constants.physSprite + constants.physBullet;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "weapons/flame.ogg"; },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComInitH; param = 32 },
			{ dur = 1 },
			{ com = constants.AnimComSetLifetime; param = function( obj ) 
					local pos = obj:aabb().p
					local flare = GetObjectUserdata(CreateSprite( "circle", pos.x, pos.y ))
					--mapvar.tmp[flare] = obj
					SetObjSpriteRenderMethod( flare, constants.rsmStretch )
					SetObjRectangle( flare, 64, 64 )
					SetObjPos( flare, pos.x, pos.y )
					SetObjSpriteColor( flare, { 1, 247/255, 35/255, 0.05 } )
					SetObjProcessor( flare, function( this )
						if obj and obj:object_present() then
							local p = obj:aabb().p
							SetObjPos( this, p.x, p.y )
						else
							SetObjDead( this )
						end
					end )
					return mapvar.tmp[obj].lifetime / 10 
			end },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 4 },
			{ dur = 50; num = 5 },
			{ com = constants.AnimComJump; param = function(obj) return math.random(10, 12) end },
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop },
			{ dur = 30; num = 6 },
			{ dur = 30; num = 7 },
			{ com = constants.AnimComDestroyObject },
		},
	},
	{
		name = "miss";
		frames =
		{
			{ dur = 100; num = 6 },
			{ dur = 100; num = 7 },
			{ com = constants.AnimComDestroyObject },
		},
	}
}
