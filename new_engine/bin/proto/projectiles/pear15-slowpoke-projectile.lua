--forbidden
damage_type = 6;
push_force = 3;

hurts_same_type = 1;

faction_hates = {3}


-- �������� ����
bullet_damage = 25;
bullet_vel = 1.75;

-- �������� ������� ����
texture = "slowpoke-projectile";

z = -0.001;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComInitWH; param = 16 },
			{ com = constants.AnimComSetLifetime; param = 300 },
			{ dur = 1 },
			{ com = constants.AnimComPlaySound; txt = "slowpoke-shoot.ogg" },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "fly";
		frames =
		{
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ com = constants.AnimComLoop }
		},
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ dur = 0 }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{
			{ dur = 0 }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pslime"; param = 0 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pslime"; param = 0 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
