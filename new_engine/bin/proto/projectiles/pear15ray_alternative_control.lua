multiple_targets = -1;
damage_type = Game.damage_types.super_laser;

time_to_live = 1;

bullet_damage = 0;

z = 0.6;

local t = 100

texture = "superlaser";

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ dur = 1; com = constants.AnimComSetInvisible; param = 1 },
			{ dur = 5*t; },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ dur = 0 }
		}
	}
}
