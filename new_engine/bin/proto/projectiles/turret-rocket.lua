texture = "rocket";

bullet_damage = 0;
bullet_vel = 0;

push_force = 1;

z = -0.001;

facing = constants.facingFixed;

local flare_processor = function(obj)
	local t = mapvar.tmp[obj]
	if t.parent and t.parent:object_present() then
		local p = t.parent:aabb().p
		SetObjPos( obj, p.x, p.y )
	else
		SetObjDead( obj )
	end
end

animations = 
{
	{
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 17 },
			{ com = constants.AnimComRealH; param = 7 },
			{ com = constants.AnimComSetLifetime; param = 500 },
			{ param = function( obj )
				obj:max_x_vel( 2 )
				obj:max_y_vel( 2 )
				obj:own_vel( { x = 0, y = 0 } )
				obj:acc( { x = 0, y = 0 } )
				obj:sprite_mirrored( false )
				local flare = GetObjectUserdata( CreateSprite( "circle", obj:aabb().p.x, obj:aabb().p.y ) )
				mapvar.tmp[flare] = { parent = obj, size = { 64, 64 } }
				SetObjSpriteColor( flare, { 209/255, 146/255, 20/255, 0.25 } )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjSpriteBlendingMode( flare, constants.bmSrcA_One )
				SetObjProcessor( flare, flare_processor )
				SetObjRectangle( flare, 64, 64 )
				flare:sprite_z( -0.0005 )
				CreateParticleSystem( "prockettrail", obj, 0, 0, 10 )
			end },
			{ dur = 1 },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "fly";
		frames =
		{
			{ param = function( obj )
				local angst = obj:vel()
				angst = math.atan2( angst.y, angst.x )
				SetObjSpriteAngle( obj, angst )
			end },
			{ dur = 30; num = 0 },
			{ dur = 30; num = 1 },
			{ dur = 30; num = 2 },
			{ param = function( obj )
				obj:own_vel( { x= 0, y = 0 } )
				obj:acc( { x= 0, y = 0 } )
				local target = mapvar.tmp[ obj ]
				local vel = obj:vel()
				local pos = obj:aabb().p
				if target and target:object_present() then
					target = target:aabb().p
					local angst = math.atan2( target.y - pos.y, target.x - pos.x )
					vel.x = vel.x + 1 * math.cos( angst )
					vel.y = vel.y + 1 * math.sin( angst )
					obj:vel( vel )
				end
			end },
			{ com = constants.AnimComLoop },
		},
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_small"; },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_small"; },
			{ com = constants.AnimComDestroyObject }
		}
	}
}