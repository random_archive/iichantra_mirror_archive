physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.4;

mass = -1;

faction_id = 1;
faction_hates = { -1, -2 };

-- �������� �������

texture = "cmaniac";

local MAX_HEALTH = 2000

z = -0.1;

local phase
local health_lock = false
local boss_bar_bkg
local boss_bar
local cleanup

local function boss_processor( obj )
	
	--mapvar.tmp.boss_active = true
	boss_bar_bkg = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137, CONFIG.scr_height - 3 * 21, 1, 1 )
	WidgetSetSprite( boss_bar_bkg, "pear15boss_bar", "background" )
	WidgetSetSpriteColor( boss_bar_bkg, { 0.8, 0.4, 0.4, 0.5 } )
	boss_bar = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137 + 24, CONFIG.scr_height - 3 * 21 + 5, 243, 11 )
	WidgetSetSprite( boss_bar, "pear15boss_bar", "bar" )
	WidgetSetSpriteRenderMethod( boss_bar, constants.rsmCrop )
	WidgetSetSpriteColor( boss_bar, { 0.8, 0.4, 0.4, 0.5 } )
	cleanup = function() DestroyWidget(boss_bar) DestroyWidget(boss_bar_bkg) end
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	mapvar.tmp.boss_object = obj

	local last_action = Loader.time or 0
	local cooldown = 5000
	local first_change = true
	while not Loader.level do
		Wait(1)
	end
	local waypoints = Loader.level.bosspoints
	local objs = Loader.level.bossobjects
	phase = 1
	local left = false
	local cooldown_lock = false
	local attack
	local bombing_runs = 0
	local bombing_top = false
	local extras = {}
	local boss_inactive = true
	if not waypoints then return end
	while true do
		if mapvar.tmp.boss_active then
			if boss_inactive then
				last_action = Loader.time
				cooldown = 10000
				boss_inactive = false
			end
			if not cooldlown_lock then
				cooldown = cooldown - ((Loader.time or 0) - last_action )
				if not first_change then
					last_action = Loader.time
				else
					first_change = false
					last_action = 0
				end
			end
			if cooldown <= 0 then
				if phase == 1 then
					attack = math.random( 1, 2 )
					if attack == 1 then
						local go_top = math.random(10) > 5
						mapvar.tmp.flicker = true
						if go_top then
							for k,v in pairs( objs.middle ) do
								SetObjAnim( v, "flicker", true )
							end
						else
							for k,v in pairs( objs.bottom ) do
								SetObjAnim( v, "flicker", true )
							end
						end
						local target_x = iff( left, waypoints.right, waypoints.left )
						mapvar.tmp.boss_destination = { x = target_x, y = 98 }
						mapvar.tmp.boss_moving = true
						left = not left
						cooldown = 999999999999
						cooldown_lock = true
						local pos = obj:aabb().p
						local go_left = left
						local jumps = 0
						Resume( NewMapThread( function()
							--Wait( 500 )
							while mapvar.tmp.flicker do
								Wait( 1 ) 
							end
							SetObjAnim( obj, "move", false )
							local last_grenade = Loader.time
							while (go_left and pos.x > target_x) or (not go_left and pos.x < target_x) do
								pos = obj:aabb().p
								if go_left then
									SetDynObjAcc( obj, -4, 0 )
									if go_top then
										if pos.x < waypoints.right_jump_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -8 )
											jumps = jumps + 1
										end
										if pos.x < waypoints.left_jump_end and jumps < 2 then
											SetDynObjVel( obj, obj:vel().x, -4 )
											jumps = jumps + 1
										end
									else
										if pos.x < waypoints.left_jumpup_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -10 )
											jumps = jumps + 1
										end
									end
								else
									SetDynObjAcc( obj, 4, 0 )
									if go_top then
										if pos.x > waypoints.left_jump_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -8 )
											jumps = jumps + 1
										end
										if pos.x > waypoints.right_jump_end and jumps < 2 then
											SetDynObjVel( obj, obj:vel().x, -4 )
											jumps = jumps + 1
										end
									else
										if pos.x > waypoints.right_jumpup_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -10 )
											jumps = jumps + 1
										end
									end
								end
								if Loader.time - last_grenade > 250 then
									last_grenade = Loader.time
									local g = GetObjectUserdata( CreateBullet( "btard-grenade", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
									mapvar.tmp[g] = { target = { x = pos.x, y = pos.y }, start = { x = pos.x, y = pos.y  } }
								end
								Wait( 1 )
							end
							mapvar.tmp.boss_moving = false
							SetObjAnim( obj, "idle2", false )
							SetObjSpriteMirrored( obj, not obj:sprite_mirrored() )
							cooldown = 2000
							cooldown_lock = false
						end ))
					elseif attack == 2 then
						cooldown = 3000
						SetObjAnim( obj, "fireballs1", false )
					end
				elseif phase == 2 then
					attack = math.random( 1, 2 )
					if bombing_runs > 0 then
						attack = 3
					end
					if attack == 1 or attack == 3 then
						if attack == 1 then
							bombing_runs = 2
						else
							bombing_runs = bombing_runs - 1
						end
						local go_top = math.random(10) > 5
						if attack == 3 then
							bombing_top = not bombing_top
							go_top = bombing_top
						else
							bombing_top = go_top
						end
						mapvar.tmp.flicker_phase2 = true
						if go_top then
							for k,v in pairs( objs.middle ) do
								SetObjAnim( v, "flicker", true )
							end
						else
							for k,v in pairs( objs.bottom ) do
								SetObjAnim( v, "flicker", true )
							end
						end
						local target_x = iff( left, waypoints.right, waypoints.left )
						mapvar.tmp.boss_destination = { x = target_x, y = 98 }
						mapvar.tmp.boss_moving = true
						left = not left
						cooldown = 999999999999
						cooldown_lock = true
						local pos = obj:aabb().p
						local go_left = left
						local jumps = 0
						Resume( NewMapThread( function()
							while mapvar.tmp.flicker_phase2 do
								Wait( 1 ) 
							end
							SetObjAnim( obj, "move", false )
							local last_grenade = Loader.time
							while (go_left and pos.x > target_x) or (not go_left and pos.x < target_x) do
								pos = obj:aabb().p
								if go_left then
									SetDynObjAcc( obj, -4, 0 )
									if go_top then
										if pos.x < waypoints.right_jump_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -8 )
											jumps = jumps + 1
										end
										if pos.x < waypoints.left_jump_end and jumps < 2 then
											SetDynObjVel( obj, obj:vel().x, -4 )
											jumps = jumps + 1
										end
									else
										if pos.x < waypoints.left_jumpup_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -10 )
											jumps = jumps + 1
										end
									end
								else
									SetDynObjAcc( obj, 4, 0 )
									if go_top then
										if pos.x > waypoints.left_jump_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -8 )
											jumps = jumps + 1
										end
										if pos.x > waypoints.right_jump_end and jumps < 2 then
											SetDynObjVel( obj, obj:vel().x, -4 )
											jumps = jumps + 1
										end
									else
										if pos.x > waypoints.right_jumpup_start and jumps < 1 then
											SetDynObjVel( obj, obj:vel().x, -10 )
											jumps = jumps + 1
										end
									end
								end
								if Loader.time - last_grenade > 175 then
									last_grenade = Loader.time
									local g = GetObjectUserdata( CreateBullet( "btard-grenade", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
									mapvar.tmp[g] = { target = { x = pos.x, y = pos.y }, start = { x = pos.x, y = pos.y  } }
									local g = GetObjectUserdata( CreateBullet( "btard-grenade", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
									mapvar.tmp[g] = { target = { x = pos.x + 50, y = pos.y }, start = { x = pos.x, y = pos.y  } }
									local g = GetObjectUserdata( CreateBullet( "btard-grenade", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
									mapvar.tmp[g] = { target = { x = pos.x - 50, y = pos.y }, start = { x = pos.x, y = pos.y  } }
								end
								Wait( 1 )
							end
							mapvar.tmp.boss_moving = false
							SetObjAnim( obj, "idle2", false )
							SetObjSpriteMirrored( obj, not obj:sprite_mirrored() )
							if bombing_runs == 0 then
								cooldown = 3000
							else
								cooldown = 300
							end
							cooldown_lock = false
						end ))
					elseif attack == 2 then
						cooldown = 3000
						SetObjAnim( obj, "fireballs2", false )
					end
				elseif phase == 3 then
					attack = 1
					local pos = obj:aabb().p
					if math.abs( pos.x - waypoints.center[1] ) > 10 or math.abs( pos.y - waypoints.center[2] ) > 50 then
						attack = 2
					end
					if attack == 1 then
						local left_attack = math.random( 1, 10 ) > 5
						cooldown = 99999
						cooldown_lock = true
						Resume( NewMapThread( function()
							local flare
							if left_attack then
								flare = CreateSpriteCenter( "red_flare", waypoints.screen_left[1], waypoints.screen_left[2] )
								SetObjAnim( objs.left_screen, "warning", false )
							else
								flare = CreateSpriteCenter( "red_flare", waypoints.screen_right[1], waypoints.screen_right[2] )
								SetObjAnim( objs.right_screen, "warning", false )
							end
							SetObjAnim( flare, "flicker", false )
							Wait( 750 )
							SetObjDead( flare )
							cooldown = 3000
							cooldown_lock = false
							if left_attack then
								for i = -379, -224, 15 do
									CreateRay( "btard-lazor", obj:id(), -90, i, -129, 40, 1 )
								end
							else
								for i = 10, 188, 15 do
									CreateRay( "btard-lazor", obj:id(),	-90, i, -129, 40, 1 )
								end
							end
						end ))
					elseif attack == 2 then
						cooldown = 999999
						cooldown_lock = true
						local jump = false
						Resume( NewMapThread( function()
							SetObjAnim( obj, "move", false )
							while math.abs( pos.x - waypoints.center[1] ) > 10 or math.abs( pos.y - waypoints.center[2] ) > 10 do
								pos = obj:aabb().p
								if pos.x > waypoints.center[1] + 10 then
									SetDynObjAcc( obj, -4, 0 )
								elseif pos.x < waypoints.center[1] - 10 then
									SetDynObjAcc( obj, 4, 0 )
								elseif not jump then
									jump = true
									SetDynObjAcc( obj, 0, 0 )
									SetDynObjVel( obj, 0, -15 )
								end
								Wait(1)
							end
							health_lock = false
							Wait( 100 )
							SetObjAnim( obj, "idle2", false )
							table.insert( extras, CreateEnemy( "fireball1", waypoints.left, 135 ) )
							table.insert( extras, CreateEnemy( "fireball1", waypoints.right, 135 ) )
							--table.insert( extras, CreateEnemy( "fireball1", -142, 87 ) )
							--table.insert( extras, CreateEnemy( "fireball1", -58, 87 ) )
							for k,v in pairs( extras ) do
								SetObjAnim( v, "permament", false )
							end
							table.insert( extras, CreateEnemy( "flame_moving", -357, 344 ) )
							cooldown = 1000
							cooldown_lock = false
						end ) )
					end
				elseif phase == 4 then
					for k,v in pairs( extras ) do
						SetObjDead( v )
					end
					Game.cleanupFunctions[ cleanup ] = false
					mapvar.tmp.boss_object = nil
					cleanup()
					return
				end
			end
		end
		Wait(1)
	end
end

local function failsafe( boss_obj )
	local last_pos = { x = 0, y = 0 }
	local pos = { x = 0, y = 0 }
	local last_change = 0
	while true do
		if mapvar.tmp.boss_moving then
			pos = boss_obj:aabb().p
			if math.abs(pos.x - last_pos.x) < 5 and math.abs(pos.y - last_pos.y) < 5 then
				if Loader.time - last_change > 1000 then
					SetObjPos( boss_obj, mapvar.tmp.boss_destination.x, mapvar.tmp.boss_destination.y )
				end
			else
				last_change = Loader.time
				last_pos = pos
			end
		else
			last_change = Loader.time
		end
		Wait(1)
	end
end

function boss_hurt( obj )
	local damage_type = ObjectPopInt( obj )
	local damage = ObjectPopInt( obj )
	if health_lock or damage_type == Game.damage_types.explosion then
		return
	end
	local health = obj:health( obj:health() - damage )
	if health > 0 then
		Game.FlashObject( obj )
	end
	WidgetSetSize( boss_bar, math.max( 1, 243 * health / MAX_HEALTH ), 11 )
	if health <= 1000 and phase == 1 then
		phase = 2
		WidgetSetSpriteColor( boss_bar, { 0.8, 0.2, 0.2, 0.75 } )
	elseif health <= 400 and phase == 2 then
		phase = 3
		health_lock = true
		WidgetSetSpriteColor( boss_bar, { 0.8, 0.1, 0.1, 0.75 } )
	elseif health <= 0 then
		phase = 4
	end
end

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 74 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetHealth; param = 2000 },
			--{ com = constants.AnimComSetHealth; param = 20 },
			{ dur = 1 },
			{ param = function(obj) Resume( NewMapThread( boss_processor ), obj ) Resume( NewMapThread( failsafe ), obj ) end },
			{ com = constants.AnimComSetAnim; txt = "idle2"}
		}
	},
	{
		name = "idle2";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 74 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = -3 },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "talk";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 74 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 30 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 31; },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 32; },
			{ dur = 100; num = 33 },
			{ dur = 100; num = 32; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 31; },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 34; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 31; },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 32; },
			{ dur = 100; num = 33 },
			{ dur = 100; num = 32; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 31; },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 34; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 31; },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 32; },
			{ dur = 100; num = 33 },
			{ dur = 100; num = 32; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 31; },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 30; },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle2" }
		}
	},
	{
		name = "pre-idle";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; param = 4; txt = "cm-energy-shooter"},
			{ com = constants.AnimComWaitForTarget; txt = "idle"; param = 64000 }
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComPushInt; param = 2+8+64 },
			{ com = constants.AnimComFlyToWaypoint; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 74 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = -3 },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = -3 },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = -3 },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2; },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "shoot"; param = 196 },
			{ com = constants.AnimComSetAnim; txt = "move_script" },
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ param = boss_hurt },
			{ com = constants.AnimComPlaySound; txt = "ouch2.ogg" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small" },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "move_script";
		frames = 
		{
			{ com = constants.AnimComCallFunction; txt = "cm_move" }
		}
	},
	{ 
		-- ��������
		name = "move";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 50 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComMaterialSound; param = 1},
			{ dur = 100; num = 6 },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 12 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComLoop },
		}
	},
	{
		name = "stop";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 2+8+64 },
			{ com = constants.AnimComFlyToWaypoint; param = 0 },
			{ num = 4; dur = 300 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "fireballs1";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 39 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComFaceTarget },
			{ dur = 50; num = 15 },
			{ dur = 50; num = 16 },
			{ dur = 50; num = 17 },
			{ dur = 50; num = 19 },
			{ dur = 100; num = 20 },
			{ param = function( obj )
				local pos = obj:aabb().p
				CreateEnemy( "fireball1", iff( obj:sprite_mirrored(), pos.x-30, pos.x+30), pos.y-10 )
			end },
			{ com = constants.AnimComSetAnim; txt = "idle2" }
		}
	},
	{
		name = "fireballs2";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 39 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComFaceTarget },
			{ dur = 50; num = 15 },
			{ dur = 50; num = 16 },
			{ dur = 50; num = 17 },
			{ dur = 50; num = 19 },
			{ dur = 100; num = 20 },
			{ param = function( obj )
				local pos = obj:aabb().p
				CreateEnemy( "fireball2", iff( obj:sprite_mirrored(), pos.x-30, pos.x+30), pos.y-10 )
			end },
			{ com = constants.AnimComSetAnim; txt = "idle2" }
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComPlaySound; txt = "slowpoke-death.ogg" },
			{ param = function( obj )
				mapvar.tmp.cutscene = true
				mapvar.tmp.boss_moving = false
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealX; param = 64 },
			{ com = constants.AnimComRealY; param = -8 },
			{ com = constants.AnimComRealW; param = 10 },
			{ com = constants.AnimComRealH; param = 77 },
			{ dur = 100; num = 22 },
			{ dur = 100; num = 23; com = constants.AnimComRealH; param = 78 },
			{ com = constants.AnimComRealX; param = 72 },
			{ dur = 100; num = 24; com = constants.AnimComRealH; param = 72 },
			{ com = constants.AnimComRealY; param = -4 },
			{ com = constants.AnimComRealX; param = 82 },
			{ dur = 100; num = 25; com = constants.AnimComRealH; param = 63 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealX; param = 62 },
			{ dur = 100; num = 26; com = constants.AnimComRealH; param = 51 },
			{ com = constants.AnimComRealY; param = -1 },
			{ dur = 100; num = 27; com = constants.AnimComRealH; param = 51 },
			{ com = constants.AnimComRealY; param = -2 },
			{ com = constants.AnimComRealX; param = 57 },
			{ dur = 100; num = 28; com = constants.AnimComRealH; param = 43 },
			{ com = constants.AnimComRealY; param = -3 },
			{ com = constants.AnimComRealX; param = 29 },
			{ dur = 100; num = 29; com = constants.AnimComRealH; param = 35 },
			{ param = function( obj )
				if Loader.level.boss_done then
					Loader.level.boss_done()
				end
			end }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
}



