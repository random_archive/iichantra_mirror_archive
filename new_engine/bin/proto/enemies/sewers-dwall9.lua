parent = "enemies/sewers-dwall1";

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
ghost_to = 255;

isometric_depth_x = 0;
isometric_depth_y = 0;

z = -0.3;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 1; num = 29 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 29 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 29 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ com = constants.AnimComDestroyObject }
		}
	}
}
