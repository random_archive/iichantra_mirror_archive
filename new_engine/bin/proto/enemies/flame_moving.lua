texture = "flames";

z = -0.7;
physic = 1;
phys_bullet_collidable = 0;
ghost_to = constants.physEverything - constants.physPlayer - constants.physSprite;

phys_max_x_vel = 2;

animations =
{
	{
		name = "init",
		frames =
		{
			{ dur = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ param = function( obj )
				local pos = obj:aabb().p
				CreateParticleSystem( "pfiresparks", obj, 0, 0, 10 )
				CreateParticleSystem( "pfiresmoke", obj, 0, 0, 0 )
			end },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{ com = constants.AnimComRealW; param = 60 },
			{ com = constants.AnimComRealH; param = 86 },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				DamageObject( toucher, 10 )
				local o1 = obj:aabb()
				local o2 = toucher:aabb()
				local diff = { x = o2.p.x - o1.p.x, y = o2.p.y - o1.p.y }
				local l = math.sqrt( diff.x * diff.x + diff.y * diff.y )
				diff.x = 32 * diff.x / l
				diff.y = 8 * diff.y / l
				toucher:vel( diff )
			end },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover }
		}
	},

}
