--forbidden
bounce = 0;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 200;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

mass = -1; --���������� ������� �����.

FunctionName = "CreateEnemy";

drops_shadow = 1;

-- �������� �������

texture = "manpac_big";

z = -0.001;

local speed = 300*((difficulty-1)/3+1);
local health = 200;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComRealW; param = 128 },
			{ com = constants.AnimComRealH; param = 128 },
			{ dur = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; param = 4; txt = "manpac_talker"},
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 9 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 11 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{
		name = "return";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 64 },
			{ com = constants.AnimComJumpRandom; param = 7 },
			{ com = constants.AnimComPushInt; param = 64 },
			{ com = constants.AnimComJumpRandom; param = 9 },
			{ com = constants.AnimComPushInt; param = 64 },
			{ com = constants.AnimComJumpRandom; param = 11 },
			{ com = constants.AnimComTeleportToWaypoint; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ com = constants.AnimComTeleportToWaypoint; param = 2 },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ com = constants.AnimComTeleportToWaypoint; param = 3 },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ com = constants.AnimComTeleportToWaypoint; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- ��������
		name = "refresh";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; param = 4; txt = "manpac_talker"},
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 9 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 11 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "manpac_orbit2"; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "return" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ dur = 100; num = 4; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ dur = 100; num = 5; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ dur = 100; num = 6; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComCallFunction; txt = "giant_manpac_hurt" },
			--{ com = constants.AnimComSetAnim; txt = "move" }
			{ com = constants.AnimComRecover; }
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 0 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPlaySound; txt = "pacman.ogg" },
			{ com = constants.AnimComSetAnim; txt = "shoot"; param = 150 },
			{ com = constants.AnimComSetAnim; txt = "change_position"; param = 150 },
			{ com = constants.AnimComSetAnim; txt = "sweep" }
		}
	},
	{
		name = "shoot";
		frames =
		{
			{ com = constants.AnimComFaceTarget; },
			{ com = constants.AnimComAdjustAim; },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; param = 1; txt = "manpac_projectile" },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; param = 1; txt = "manpac_projectile" },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; param = 1; txt = "manpac_projectile" },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		name = "change_position";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 40 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ com = constants.AnimComJump; param = 3 }
		}
	},
	{
		name = "sweep";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "sweep_left"; param = 127 },
			{ com = constants.AnimComSetAnim; txt = "sweep_right" }
		}
	},
	{ 
		name = "sweep_left";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 35 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 60 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 256 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComJumpIfWaypointClose; param = 13 },
			{ com = constants.AnimComJump; param = 0 },
			{ com = constants.AnimComTeleportToWaypoint; param = 1 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 35 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 60 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 256 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComJumpIfWaypointClose; param = 28 },
			{ com = constants.AnimComJump; param = 14 },
			{ com = constants.AnimComTeleportToWaypoint; param = 3 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 35 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 60 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 256 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComJumpIfWaypointClose; param = 43 },
			{ com = constants.AnimComJump; param = 29 },
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 1; com = constants.AnimComFlyToWaypoint; param = 20 },		
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 2; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 3; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 4; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 5; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComJump; param = 44 }
		}
	},
	{ 
		name = "sweep_right";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 35 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 60 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 256 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComJumpIfWaypointClose; param = 13 },
			{ com = constants.AnimComJump; param = 4 },
			{ com = constants.AnimComTeleportToWaypoint; param = 3 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 35 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 60 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 256 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComJumpIfWaypointClose; param = 28 },
			{ com = constants.AnimComJump; param = 14 },
			{ com = constants.AnimComTeleportToWaypoint; param = 1 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 35 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 60 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 256 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComJumpIfWaypointClose; param = 43 },
			{ com = constants.AnimComJump; param = 29 },
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 1; com = constants.AnimComFlyToWaypoint; param = 20 },		
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 2; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 3; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 4; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComPushInt; param = 2000 },
			{ com = constants.AnimComPushInt; param = 34 },
			{ dur = 100; num = 5; com = constants.AnimComFlyToWaypoint; param = 20 },
			{ com = constants.AnimComJump; param = 44 }
		}
	},
	{
		name = "respawn";
		frames =
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			--{ com = constants.AnimComStartDying; param = -1 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetVelY; param = -6000 },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ com = constants.AnimComPushInt; param = 640 },
			{ com = constants.AnimComPushInt; param = 480 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 6 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComTeleportToWaypoint; param = 5 },
			{ com = constants.AnimComSetVelY; param = 1 },
			{ dur = 10000; num = 0; },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetAnim; txt = "refresh" }
		}
	},
	{ 
		name = "retreat";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = health }
		}
	},
	{
		name = "final_death";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 0; num = 0; com = constants.AnimComFlyToWaypoint; param = 0 },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ com = constants.AnimComStartDying; param = 1 },
			{ com = constants.AnimComPlaySound; txt = "pacman-die_big.ogg" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 800 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComMapVarAdd; param = 1500; txt = "score" },
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "kills" },
			{ num = 7; dur = 25 },
			{ num = 8; dur = 25 },
			{ dur = 0 },
			{ num = 9; dur = 1 },
			{ com = constants.AnimComJumpIfOnPlane; param = 19 },
			{ com = constants.AnimComJump; param = 15 },
			{ dur = 0 },
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComPushInt; param = -29 },
			{ com = constants.AnimComPushInt; param = 31 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComPushInt; param = 18 },
			{ com = constants.AnimComPushInt; param = 31 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComPushInt; param = -100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ num = 10; dur = 100; },
			{ num = 11; dur = 100; },
			{ num = 12; dur = 100; },
			{ num = 13; dur = 500; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJump; param = 38 },
			{ com = constants.AnimComDestroyObject }

		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComCallFunction; txt = "giant_manpac_death" },
			{ dur = 1 },
			{ com = constants.AnimComSetHealth; param = health },
			--{ com = constants.AnimComSetAnim; txt = "respawn" },
			{}
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComPushInt; param = -29 },
			{ com = constants.AnimComPushInt; param = 31 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComPushInt; param = 18 },
			{ com = constants.AnimComPushInt; param = 31 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComPushInt; param = -100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ num = 10; dur = 100; },
			{ num = 11; dur = 100; },
			{ num = 12; dur = 100; },
			{ num = 13; dur = 500; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJump; param = 18 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{ 
		-- ��������
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
}



