health = 100;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 2;
offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.4;

faction_id = 1;
faction_hates = { -1, -2 };

texture = "slime";

z = -0.1;

color = {148/255, 208/255, 121/255, 1}
overlay = {0, 1};
ocolor = {{1, 1, 1, 1}, {1, 1, 1, 0}}

local slime_smile = 40

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ param = function( obj )
				local parent = GetParent( obj )
				if parent and mapvar.tmp[parent] and mapvar.tmp[parent].type == "green_slime" then
					mapvar.tmp[obj] = { type = "green_slime", size = mapvar.tmp[parent].size * 0.75, facing = obj:aabb().p.x < parent:aabb().p.x }
				else
					mapvar.tmp[obj] = { type = "green_slime", size = math.random( slime_smile, 80 ) }
				end
				if mapvar.tmp[obj].size < slime_smile then
					SetObjSpriteColor( obj, {1,1,1,1}, 1 )
				end
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },		
			
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			
			{ com = constants.AnimComPushInt; param = function(obj) return -mapvar.tmp[obj].size/2+16 end},
			{ com = constants.AnimComPushInt; param = function(obj) return mapvar.tmp[obj].size/2-16 end},
			{ com = constants.AnimComMPSet; param = 1 },
			
		
			{ com = constants.AnimComPushInt; param = 1},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComCreateEffect; txt = "slime_trail", param = 4 },
			
			{ param = function( obj )
				local children = GetChildren( obj )
				if not children then return end
				for k, v in pairs( children ) do
					mapvar.tmp[v] = 
					{
						start_color = { 148/255, 208/255, 121/255, 1 },
						end_color = { 148/255, 208/255, 121/255, 0 }
					}
				end
			end },
			
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = function(obj) return mapvar.tmp[obj].size * 39/25 end },
			{ com = constants.AnimComRealH; param = function(obj) return mapvar.tmp[obj].size end },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = function(obj)
				if mapvar.tmp[obj].size > slime_smile then
					return 1
				end
				return 0
			end },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 50 * 39/25 },
			{ com = constants.AnimComRealH; param = 50 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{ 
		-- Создание
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ com = constants.AnimComLoop }	
		}
	},
	{
		name = "chase";
		frames =
		{
			{ constants.AnimComFaceTarget }, --Does not work for some reason.
			{ param = function( obj )
				if mapvar.tmp[obj].facing ~= nil then
					SetObjSpriteMirrored( obj, mapvar.tmp[obj].facing )
					return
				end
				if obj:aabb().p.x > obj:target():aabb().p.x then
					SetObjSpriteMirrored( obj, true );
				end
			end },
			{ constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "slime.ogg" },
			{ param = function( obj )
				local damagetype = ObjectPopInt( obj )
				if damagetype == Game.damage_types.super_laser or damagetype == Game.damage_types.laser then
					mapvar.tmp[obj].lasered = true
				else
					mapvar.tmp[obj].lasered = false
				end
			end },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime2" },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Создание
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 400 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetTouchable; param = function(obj)
				if mapvar.tmp[obj].size > slime_smile then
					return 1
				end
				return 0
			end },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- Создание
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime2" },
			{ com = constants.AnimComRealW; param = function(obj) return mapvar.tmp[obj].size * 67/25 end },
			{ com = constants.AnimComRealH; param = function(obj) return mapvar.tmp[obj].size * 35/25 end },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ num = 4; dur = 100 },
			{ num = 5; dur = 100 },
			{ num = 6; dur = 100 },
			{ param = function( obj )
				Info.RevealEnemy( "GREEN_SLIME" )
				local pos = obj:aabb().p
				local size = mapvar.tmp[obj].size
				if size > slime_smile then
					Game.AddCombo( 1 )
					Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "green_slime", { 39, 25 } ) 
					Game.CreateScoreItems( 3, pos.x, pos.y )
					if not mapvar.tmp[obj].lasered then
						CreateEnemy( "green_slime", pos.x - size / 2, pos.y + size/2 - size*0.375 - 2, nil, nil, obj )
						CreateEnemy( "green_slime", pos.x + size / 2, pos.y + size/2 - size*0.375 - 2, nil, nil, obj )
					end
				else
					Game.CreateScoreItems( 1, pos.x, pos.y )
				end
			end },
			{ num = 7; dur = 100 },
			{ num = 8; dur = 100 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "land";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComRecover; txt = "move" }
		}
	}
}



