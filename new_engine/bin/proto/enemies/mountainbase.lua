texture = "mountainbase";
facing = constants.facingFixed

z = -0.8;

physic = 1;
phys_solid = 1;
phys_bullet_collidable = 1;

mass = -1;

phys_max_x_vel = 0;
phys_max_y_vel = 0;

gravity_x = 0;
gravity_y = 0;

faction_id = 3;
faction_hates = { -1, -2 }
ghost_to = constants.physEverything - constants.physPlayer - constants.physEnemy;

local base
local gun1
local gun2
local light
local top
local labels

local done

local btards = {}

local GUN_DELAY = 5000
local BTARD_DELAY = 10000

local last_gun_attack
local last_btards

local function processor( this )
	if not last_gun_attack then
		last_gun_attack = 0
	end
	if not last_btards then
		last_btards = Loader.time
	end
	if last_gun_attack + GUN_DELAY < Loader.time then
		last_gun_attack = Loader.time
		Resume( NewMapThread( function()
			for i=1, 3 do
				if gun1 then
					SetObjAnim( gun1, "gun_fire", true )
				end
				Wait( 250 )
				if gun2 then
					SetObjAnim( gun2, "gun_fire", true )
				end
				Wait( 250 )
			end
		end ))
	end
	if last_btards + BTARD_DELAY/2 < Loader.time then
		last_btards = Loader.time
		Resume( NewMapThread( function()
			local pos = this:aabb()
			local btrd
			pos.p.y = pos.p.y - pos.H - 128
			pos.p.x = pos.p.x + pos.W
			for i=1,5 do
				if done then
					return
				end
				btrd = GetObjectUserdata(CreateEnemy( "btard-com", pos.p.x, pos.p.y) )
				SetObjProcessor( btrd, function(this)
					if this:health() <= 0 then
						btards[ this ] = false
					end
				end )
				btards[ btrd ] = true
				Wait( 500 )
			end
			Wait( 200 )
			if done then
				return
			end
			btrd = GetObjectUserdata( CreateEnemy( "btard-com-grenadier", pos.p.x, pos.p.y ) )
			SetObjProcessor( btrd, function(this)
					if this:health() <= 0 then
						btards[ this ] = false
					end
				end )
			btards[ btrd ] = true
		end ))
	end
end

animations, labels = WithLabels
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 118-15 },
			{ com = constants.AnimComRealH; param = 318 }, --113
			{ com = constants.AnimComRealX; param = 18 },
			{ dur = 1; num = 0 },
			{ param = function( this )
				local pos = this:aabb()
				base = this
				gun1 = GetObjectUserdata( CreateEnemy( "mountainbase", pos.p.x, pos.p.y ) )
				mapvar.tmp[gun1] = { type = "gun" }
				SetObjPos( gun1, pos.p.x - pos.W - 51 - 18, pos.p.y - pos.H + 55  )
				SetObjAnim( gun1, "gun", false )
				gun2 = GetObjectUserdata( CreateEnemy( "mountainbase", pos.p.x, pos.p.y ) )
				SetObjPos( gun2, pos.p.x - pos.W - 35 - 18, pos.p.y - pos.H + 69  )
				SetObjAnim( gun2, "gun", false )
				mapvar.tmp[gun2] = { type = "gun" }
				light = GetObjectUserdata( CreateEnemy( "mountainbase", pos.p.x, pos.p.y ) )
				SetObjPos( light, pos.p.x - pos.W + 7 - 18-30, pos.p.y - pos.H + 148+16 )
				SetObjAnim( light, "light", false )
				mapvar.tmp[light] = { type = "light" }
				local balcony = CreateSprite( "phys-empty", pos.p.x - pos.W, pos.p.y - pos.H )
				GroupObjects( balcony, CreateSprite( "phys-empty", pos.p.x + pos.W, pos.p.y - pos.H + 15 ) )
				GetObjectUserdata( balcony ):solid_to( constants.physEnemy )
				top = CreateEnemy( "mountainbase", pos.p.x, pos.p.y )
				SetObjAnim( top, "top", false )
				SetObjPos( top, pos.p.x + 118/2 - 101/2 + 8, pos.p.y + 318/2 - 123/2 - 6 )
				SetObjInvisible( top, true )
			end },
			{ com = constants.AnimComSetAnim; txt = "wait" }
		}
	},
	{
		name = "wait";
		frames =
		{
			{ label = "WAIT" },
			{ dur = 100 },
			{ com = constants.AnimComJump; param = function( this )
				if ObjectOnScreen( this, -40 ) then
					return labels.GO
				else
					return labels.WAIT
				end
			end },
			{ label = "GO" },
			{ param = function( this )
				local focus = GetObjectUserdata( CreateSprite( "phys-empty", this:aabb().p.x, this:aabb().p.y - 156 ) )
				focus:solid_to( 0 )
				mapvar.tmp.boss_started = true
				mapvar.tmp.boss_object = focus
				if Loader.level.boss_start then
					Loader.level.boss_start()
				end
				SetObjProcessor( this, processor )
			end },
			{ dur = 1 }
		}
	},
	{
		name = "gun";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ param = function( obj )
				obj:solid_to( constants.physBullet )
				obj:solid( false )
				obj:health( 200 )
			end },
			{ com = constants.AnimComInitW; param = 42 },
			{ com = constants.AnimComInitH; param = 9 },
			{ dur = 100; num = 3 }
		}
	},
	{
		name = "gun_fire";
		frames =
		{
			{ dur = 100; num = 4 },
			{ param = function( this )
				local enemy = GetNearestEnemy( this )
				local dx = 100
				if enemy then
					dx = enemy:aabb().p.x - this:aabb().p.x
				end
				local bull = GetObjectUserdata( CreateBullet( "fireshot_heavy", this:aabb().p.x, this:aabb().p.y, this:id(), false, 180, 0 ) )
				mapvar.tmp[bull] = dx
			end },
			{ dur = 100; num = 5 },
			{ dur = 100; num = 3 }
		},
	},
	{
		name = "gun_dead";
		frames =
		{
			{ param = function( this )
				if this == gun1 then
					gun1 = nil
				else
					gun2 = nil
				end
				this:solid_to( 0 )
			end; num = 6 },
			{ dur = 100; num = 6 }
		},
	},
	{
		name = "light";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ param = function( obj )
				obj:solid_to( constants.physBullet )
				obj:solid( false )
				obj:health( 300 )
			end },
			{ com = constants.AnimComInitW; param = 18 },
			{ com = constants.AnimComInitH; param = 40 },
			{ label = "LIGHT_LOOP" },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 9 },
			{ com = constants.AnimComJump; param = "LIGHT_LOOP" },
			{}
		}
	},
	{
		name = "top";
		frames =
		{
			{ param = function( obj )
				obj:solid_to( 0 )
				obj:solid( false )
				obj:sprite_z( 0.5 )
			end },
			{ com = constants.AnimComRealW; param = 101 },
			{ com = constants.AnimComRealH; param = 123 },
			{ dur = 100; num = 2 }
		}
	},
	{
		name = "light_dead";
		frames =
		{
			{ param = function( this )
				mapvar.tmp.boss_object = nil
				SetObjAnim( base, "base_destroyed", false )
				if gun1 and gun2 then
					Game.ProgressAchievement( "HARD_WAY", 1 )
				end
				if gun1 then
					SetObjAnim( gun1, "gun_dead", false )
				end
				if gun2 then
					SetObjAnim( gun2, "gun_dead", false )
				end
				gun1 = nil
				gun2 = nil
				done = true
				for k, v in pairs( btards ) do
					if v then
						DamageObject( k, 100 )
					end
				end
				SetObjProcessor( base, function() end )
				if Loader.level.boss_done then
					Loader.level.boss_done()
				end
			end },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "base_destroyed";
		frames =
		{
			{ param = function( this )
				this:solid_to( 0 )
				this:solid( false )
				local pos = this:aabb().p
				--SetObjPos( top, pos.x + 118/2 - 101/2 + 8, pos.y + 318/2 - 123/2 - 6 )
				CreateEnemy( "big_explosion", pos.x + 1182/2 - 101/2 + 8 + 20, pos.y + 318/2 - 123 /2 - 6 + 60 )
				SetObjInvisible( top, false  )
			end },
			{ dur = 100, num = 1 }
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComJump; param = function( this )
				if mapvar.tmp.boss_object and mapvar.tmp.boss_started then
					return labels.CONTINUE
				else
					ObjectPopInt( this )
					if base == this then
						return labels.GO_WAIT
					else
						return labels.RECOVER
					end
				end
			end },
			{ label = "CONTINUE" },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComJump; num = 10; param = function( this )
				Game.FlashObject( this )
				if not (this == gun1 or this == gun2) then
					CreateParticleSystem( "pspark2", this:aabb().p.x, this:aabb().p.y )
				end
				if this:health() > 0 then
					return labels.RECOVER
				elseif this == gun1 or this == gun2 then
					this:health( 1000 )
					CreateEnemy( "big_explosion", this:aabb().p.x, this:aabb().p.y )
					return labels.GUN_DEAD
				else
					this:health( 1000 )
					CreateEnemy( "big_explosion", this:aabb().p.x, this:aabb().p.y )
					return labels.LIGHT_DEAD
				end
			end },
			{ label = "RECOVER" },
			{ com = constants.AnimComRecover },
			{ label = "GUN_DEAD" },
			{ com = constants.AnimComSetAnim; txt = "gun_dead"; num = 10 },
			{ label = "LIGHT_DEAD" },
			{ com = constants.AnimComSetAnim; txt = "light_dead"; num = 10 },
			{ label = "GO_WAIT" },
			{ com = constants.AnimComRemoveRecoveryState },
			{ com = constants.AnimComSetAnim; txt = "wait" },
			{}
		}
	}
}
