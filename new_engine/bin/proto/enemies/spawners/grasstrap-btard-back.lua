texture = "grasstrap";

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_ghostlike = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.0001;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 128 },
			{ com = constants.AnimComRealH; param = 20 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 250 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCameraLeft; param = 6 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pdust"; param = 2 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 50; num = 1 },
			{ com = constants.AnimComRealY; param = 37 },
			{ dur = 50; num = 2 },
			{ com = constants.AnimComRealY; param = 30 },
			{ dur = 50; num = 3 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -32 },
			{ com = constants.AnimComCreateEnemy; txt = "btard" },
			{ com = constants.AnimComSetAnim; txt = "empty" },
		}
	},
	{
		name = "empty";
		frames =
		{
			{ com = constants.AnimComSetZ; param = -500 },
			{ com = constants.AnimComRealY; param = 6 },
			{ dur = 50; num = 4 }
		}
	}
}



