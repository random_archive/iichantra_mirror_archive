texture = "forest-details3";

offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_ghostlike = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.015;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 22 },
			{ com = constants.AnimComRealH; param = 104 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 1 }	
		}
	},
	{ 
		name = "btard";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 16 },
			{ num = 2; dur = 100 },
			{ com = constants.AnimComRealX; param = 20 },
			{ num = 3; dur = 100 },
			{ com = constants.AnimComCallFunction; txt = "boss_enemy_created" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -32 },
			{ com = constants.AnimComPushInt; param = 62 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-boss" },
			{ num = 3; dur = 100 },
			{ com = constants.AnimComRealX; param = 16 },
			{ num = 2; dur = 100 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "init" },
		}
	},
	{ 
		name = "superbtard";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 16 },
			{ num = 2; dur = 100 },
			{ com = constants.AnimComRealX; param = 20 },
			{ num = 3; dur = 100 },
			{ com = constants.AnimComCallFunction; txt = "boss_enemy_created" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -32 },
			{ com = constants.AnimComPushInt; param = 62 },
			{ com = constants.AnimComCreateEnemy; txt = "superbtard-boss" },
			{ num = 3; dur = 100 },
			{ com = constants.AnimComRealX; param = 16 },
			{ num = 2; dur = 100 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "init" },
		}
	}
}



