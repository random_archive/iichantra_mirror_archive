parent = "enemies/sewers-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 4 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 5 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 6 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 7 }                                      
		}
	}
}
