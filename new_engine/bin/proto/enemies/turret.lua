offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

physic = 1;
phys_solid = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 0;
phys_max_y_vel = 0;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
phys_ghostlike = 0;

texture = "turret";

z = -0.4;

mass = -1;

gravity_x = 0;
gravity_y = 0;

faction_id = 1;
faction_hates = { -1, -2 };

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 38 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealY; param = 5 },
			{ com = constants.AnimComSetHealth; param = 100*difficulty },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealY; param = 5 },
			{ com = constants.AnimComRealW; param = 38 },
			{ com = constants.AnimComRealH; param = 1 },
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 3000; txt = "hidden" },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.force_alt; txt = "forcegun" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComRecover}
		}
	},
	{ 
		name = "forcegun";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPushInt; param = 9000; },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Создание
		name = "hidden";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealY; param = 5 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ dur = 100; num = 0; com = constants.AnimComJumpIfTargetClose; param = 7 },
			{ com = constants.AnimComLoop },
			{ dur = constants.AnimComBreakpoint },
			{ com = constants.AnimComPushInt; param = 90 },
			{ com = constants.AnimComJump; param = 11 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "appear" }
		}
	},
	{
		name = "appear";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRealX; param = 4 },
			{ com = constants.AnimComRealY; param = 6 },
			{ com = constants.AnimComRealH; param = 2 },
			{ dur = 50; num = 1 },
			{ com = constants.AnimComRealY; param = 10 },
			{ com = constants.AnimComRealH; param = 4 },
			{ dur = 50; num = 2 },
			{ com = constants.AnimComRealH; param = 13 },
			{ dur = 50; num = 3 },
			{ com = constants.AnimComRealH; param = 23 },
			{ dur = 50; num = 4 },
			{ com = constants.AnimComRealH; param = 27 },
			{ dur = 50; num = 5 },
			{ com = constants.AnimComSetAnim; txt = "firstshot" }			
		}
	},
	{
		name = "firstshot";
		frames =
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = 9 },
			{ com = constants.AnimComRealH; param = 28 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 180 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 0; num = 9; com = constants.AnimComAngledShot; txt = "fireshot" },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 180+30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 0; num = 9; com = constants.AnimComAngledShot; txt = "fireshot" },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 180-30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 9; com = constants.AnimComAngledShot; txt = "fireshot" },
			{ com = constants.AnimComSetAnim; txt = "active" }
		}
	},
	{ 
		-- Создание
		name = "active";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRealX; param = 4 },
			{ com = constants.AnimComRealY; param = 10 },
			{ com = constants.AnimComRealH; param = 28 },
			{ dur = 50; num = 6 },
			{ dur = 300; num = 7 },
			{ dur = 300; num = 8 },
			{ dur = 300; num = 7 },
			{ dur = 300; num = 8 },
			{ dur = 300; num = 7 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = 9 },
			{ com = constants.AnimComRealH; param = 28 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 180 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 0; num = 9; com = constants.AnimComAngledShot; txt = "fireshot" },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 180+30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 0; num = 9; com = constants.AnimComAngledShot; txt = "fireshot" },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 180-30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 9; com = constants.AnimComAngledShot; txt = "fireshot" },
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealY; param = 10 },
			{ com = constants.AnimComRealH; param = 28 },
			{ com = constants.AnimComPushInt; param = 400 },
			{ dur = 100; num = 0; com = constants.AnimComJumpIfTargetClose; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "hide" },
		}
	},
	{
		name = "hide";
		frames =
		{
			{ com = constants.AnimComRealX; param = 4 },
			{ com = constants.AnimComRealY; param = 10 },
			{ com = constants.AnimComRealH; param = 27 },
			{ dur = 50; num = 5 },
			{ com = constants.AnimComRealH; param = 23 },
			{ dur = 50; num = 4 },
			{ com = constants.AnimComRealH; param = 13 },
			{ dur = 50; num = 3 },
			{ com = constants.AnimComRealH; param = 4 },
			{ dur = 50; num = 2 },
			{ com = constants.AnimComRealY; param = 6 },
			{ com = constants.AnimComRealH; param = 2 },
			{ dur = 50; num = 1 },
			{ com = constants.AnimComRealH; param = 12 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "hidden" }
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 2 },
			{ com = constants.AnimComRecover },
			{ },
			{ com = constants.AnimComDamage; param = 50*difficulty },
			{ com = constants.AnimComRecover },
		}
	},
	{ 
		-- Создание
		name = "die";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 38 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComRealX; param = 2 },
			{ com = constants.AnimComRealY; param = 5 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
			{ param = function( obj )
				Game.turret_destroyed( obj )
				obj:solid_to( 0 )
				obj:sprite_z( -0.4 )
				Info.RevealEnemy( "TURRET" )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "turret", { 39, 22 } ) 
				Game.CreateScoreItems( 4, pos.x, pos.y )
			end },
			{ dur = 5000; num = 10 },
			{ com = constants.AnimComJump; param = 8 }
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 44 },
			{ com = constants.AnimComRealH; param = 43 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 300; num = 7 },
			{ dur = 300; num = 8 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{
		name = "target_dead";
		frames = 
		{
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
}



