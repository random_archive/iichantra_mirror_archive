texture = "flyingplatform";
facing = constants.facingFixed

z = -0.01;

physic = 1;
phys_solid = 1;
phys_one_sided = 1;
phys_bullet_collidable = 1;

mass = -1;

ghost_to = 16;

phys_max_x_vel = 5;
phys_max_y_vel = 5;

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 14 },
			{ com = constants.AnimComRealH; param = 23 },
			{ com = constants.AnimComRealW; param = 65 },
			{ com = constants.AnimComInitW; param = 48 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComInitW; param = 64 },
			{ com = constants.AnimComRealX; param = 17 },
			{ com = constants.AnimComRealY; param = 13 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{
		name = "loop";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 8+32+128 },
			{ dur = 0, num = 0; com = constants.AnimComFlyToWaypoint; param = 2000 },
			{ dur = 50, num = 0 },
			{ dur = 50, num = 1 },
			{ dur = 50, num = 2 },
			{ com = constants.AnimComJump; param = 2 }
		}
	}
}
