--forbidden
z = 0.8;
phys_ghostlike = 1;
texture = "2011topturret";
physic = 1;
phys_bullet_collidable = 1;
offscreen_behavior = constants.offscreenNone

facing = constants.facingFixed

mass = -1;

faction_id = 1;
faction_hates = { -1, -2 };

animations = 
{
	{
		name = "init";
		frames =
		{
			{ com = constants.AnimComSetHealth; param = 100 },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 25 },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 0 },
			{ param = function( this )
				local delay = ObjectPopInt( this )
				if not mapvar.tmp.turret_groups then
					mapvar.tmp.turret_groups = {}
				end
				local group = -1
				for k, v in ipairs( mapvar.tmp.turret_groups ) do
					if v.delay == delay then
						group = k
						break
					end
				end
				if group == -1 then
					table.insert( mapvar.tmp.turret_groups, { delay = delay, uds = { this }, till_next = 0 } )
				else
					table.insert( mapvar.tmp.turret_groups[group].uds, this )
				end
				if not mapvar.tmp.top_turrets_thread then
					local next_update = 0
					local current_group = 0
					mapvar.tmp.top_turrets_thread = NewMapThread( function()
						Wait(1)
						table.sort( mapvar.tmp.turret_groups, function( a, b ) return a.delay < b.delay end )
						for i=1, #mapvar.tmp.turret_groups-1 do
							mapvar.tmp.turret_groups[i].till_next = mapvar.tmp.turret_groups[i+1].delay - mapvar.tmp.turret_groups[i].delay
						end
						mapvar.tmp.turret_groups[#mapvar.tmp.turret_groups].till_next = -1
						next_update = Loader.time + mapvar.tmp.turret_groups[1].delay+1
						while current_group == 0 or mapvar.tmp.turret_groups[current_group].till_next ~= -1 do
							if Loader.time >= next_update then
								current_group = current_group + 1
								for k, v in ipairs( mapvar.tmp.turret_groups[current_group].uds ) do
									SetObjAnim( v, "fire", false )
								end
								next_update = Loader.time + mapvar.tmp.turret_groups[current_group].till_next
							end
							Wait( 1 )
						end
					end )
					Resume( mapvar.tmp.top_turrets_thread )
				end
			end }
		}
	},
	{
		name = "inactive";
		frames =
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComJump; param = function()
				if difficulty > 1.5 then
					return 16
				else
					return 8
				end
			end
			},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "fire" }
		}
	},
	{ 
		name = "fire";
		frames = 
		{
			{ dur = 50; num = 2 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 4 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 50; num = 5 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 50; num = 6 },
			{ dur = 50; num = 7 },
			{ dur = 50; num = 8 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "topturret-projectile"; param = 0 },
			{ dur = 50; num = 9 },
			{ dur = 50; num = 10 },
			{ com = constants.AnimComSetAnim; txt = "inactive" }
		}
	},
	{
		name = "die";
		frames =
		{
			{ param = function(obj)
				Game.turret_destroyed( obj )
				local pos = obj:aabb().p
				Game.AddCombo(1)
				Info.RevealEnemy( "TURRET" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "topturret", { 39, 15 } ) 
				Game.CreateScoreItems( 6, pos.x, pos.y )
			end },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_small" },
			{ dur = 10000, num = 11 },
			{ com = constants.AnimComJump; param = 5 }
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 25 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 4 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 50; num = 5 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 50; num = 6 },
			{ dur = 50; num = 7 },
			{ dur = 50; num = 8 },
			{ dur = 50; num = 9 },
			{ dur = 50; num = 10 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.force_alt; txt = "forcegun" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComRecover}
		}
	},
	{ 
		name = "forcegun";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPushInt; param = 9000; },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRecover }
		}
	},
}
