--forbidden
physic = 1;
ghost_to = 255;

phys_max_x_vel = 2;
phys_max_y_vel = 2;

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitWH; param = 1 }
		}
	}
}