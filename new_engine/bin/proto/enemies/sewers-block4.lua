parent = "enemies/sewers-block1"

animations =
{
	{
		name = "closed";
		frames =
		{
			{ dur = 1; num = 3 }
		}
	},
	{
		name = "open";
		frames =
		{
			{ param = function( obj ) obj:solid_to(0) obj:solid(false) end },
			{ dur = 1; num = 0 }                                      
		}
	},
}
