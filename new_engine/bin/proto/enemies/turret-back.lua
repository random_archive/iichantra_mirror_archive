physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
mp_count = 1;
offscreen_behavior = constants.offscreenSleep
drops_shadow = 0;

faction_id = 1;
faction_hates = { -1, -2 };

texture = "turret-back";

z = -0.15;
mass = -1;
phys_ghostlike = 1;

local ROCKET_SPEED = 1

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitWH, param = 43 },
			
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetHealth; param = 300 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 45 },
			{ com = constants.AnimComRealH; param = 45 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 1000; num = 0 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 100; num = 0; }, 
			{ com = constants.AnimComWaitForTarget; param = 30000; txt = "activate" },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "activate",
		frames =
		{
			{ com = constants.AnimComSetAnim; param = function( obj )
				local t = obj:target():aabb().p
				local p = obj:aabb().p
				local d = { x = t.x - p.x, y = t.y - p.y }
				local l = math.sqrt( math.pow(d.x, 2) + math.pow(d.y, 2) )
				d.x = d.x / l
				d.y = d.y / l
				obj:sprite_mirrored( d.x < 0 )
				if d.y < 0 then
					if d.y < -0.75 then
						return 0, "up"
					elseif d.y < -0.5 then
						return 0, "upforward"
					else
						return 0, "forward"
					end
				else
					if d.y > 0.75 then
						return 0, "down"
					elseif d.y > 0.5 then
						return 0, "downforward"
					else
						return 0, "forward"
					end
				end
			end },
			{ com = constants.AnimComLoop },
		}
	},
	{
		name = "forward",
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 0, dur = 1000 },
			{ param = function( obj )
				if not ObjectOnScreen( obj ) then
					SetObjAnim( obj, "activate", false )
				end
			end },
			{ num = 1; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local vel = { x = iff( obj:sprite_mirrored(), -ROCKET_SPEED, ROCKET_SPEED ), y = 0 }
				local rocket = GetObjectUserdata( CreateBullet( "turret-rocket", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
				rocket:vel( vel )
				rocket:own_vel( { x = 0; y = 0 } )
				rocket:max_x_vel( 2 )
				rocket:max_y_vel( 2 )
				mapvar.tmp[rocket] = obj:target()
			end },
			{ num = 0, dur = 1000 },
			{ com = constants.AnimComSetAnim; txt = "activate" }
		}
	},
	{
		name = "upforward",
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 1 },
			{ num = 3, dur = 1000 },
			{ param = function( obj )
				if not ObjectOnScreen( obj ) then
					SetObjAnim( obj, "activate", false )
				end
			end },
			{ num = 5; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local vel = { x = iff( obj:sprite_mirrored(), -ROCKET_SPEED/math.sqrt(2), ROCKET_SPEED/math.sqrt(2) ), y = -ROCKET_SPEED/math.sqrt(2) }
				local rocket = GetObjectUserdata( CreateBullet( "turret-rocket", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), iff( obj:sprite_mirrored(), 45, -45), 0 ) )
				rocket:vel( vel )
				rocket:own_vel( { x = 0; y = 0 } )
				rocket:max_x_vel( 2 )
				rocket:max_y_vel( 2 )
				mapvar.tmp[rocket] = obj:target()
			end },
			{ com = constants.AnimComRealY; param = 1 },
			{ num = 3, dur = 1000 },
			{ com = constants.AnimComSetAnim; txt = "activate" }
		}
	},
	{
		name = "up",
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 2 },
			{ num = 6, dur = 1000 },
			{ param = function( obj )
				if not ObjectOnScreen( obj ) then
					SetObjAnim( obj, "activate", false )
				end
			end },
			{ com = constants.AnimComRealY; param = 7 },
			{ num = 7; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local vel = { x = 0, y = -ROCKET_SPEED }
				local rocket = GetObjectUserdata( CreateBullet( "turret-rocket", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), iff( obj:sprite_mirrored(), 90, -90), 0 ) )
				rocket:vel( vel )
				rocket:own_vel( { x = 0; y = 0 } )
				rocket:max_x_vel( 2 )
				rocket:max_y_vel( 2 )
				mapvar.tmp[rocket] = obj:target()
			end },
			{ com = constants.AnimComRealY; param = 2 },
			{ num = 6, dur = 1000 },
			{ com = constants.AnimComSetAnim; txt = "activate" }
		}
	},
	{
		name = "downforward",
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 10, dur = 1000 },
			{ param = function( obj )
				if not ObjectOnScreen( obj ) then
					SetObjAnim( obj, "activate", false )
				end
			end },
			{ num = 4; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local vel = { x = iff( obj:sprite_mirrored(), -ROCKET_SPEED/math.sqrt(2), ROCKET_SPEED/math.sqrt(2) ), y = ROCKET_SPEED/math.sqrt(2) }
				local rocket = GetObjectUserdata( CreateBullet( "turret-rocket", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), iff( obj:sprite_mirrored(), -45, 45), 0 ) )
				rocket:vel( vel )
				rocket:own_vel( { x = 0; y = 0 } )
				rocket:max_x_vel( 2 )
				rocket:max_y_vel( 2 )
				mapvar.tmp[rocket] = obj:target()
			end },
			{ num = 10, dur = 1000 },
			{ com = constants.AnimComSetAnim; txt = "activate" }
		}
	},
	{
		name = "down",
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 8, dur = 1000 },
			{ param = function( obj )
				if not ObjectOnScreen( obj ) then
					SetObjAnim( obj, "activate", false )
				end
			end },
			{ num = 9; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local vel = { x = 0, y = ROCKET_SPEED }
				local rocket = GetObjectUserdata( CreateBullet( "turret-rocket", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), iff( obj:sprite_mirrored(), -90, 90), 0 ) )
				rocket:vel( vel )
				rocket:own_vel( { x = 0; y = 0 } )
				rocket:max_x_vel( 2 )
				rocket:max_y_vel( 2 )
				mapvar.tmp[rocket] = obj:target()
			end },
			{ num = 8, dur = 1000 },
			{ com = constants.AnimComSetAnim; txt = "activate" }
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.force_alt; txt = "forcegun" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComRecover}
		}
	},
	{ 
		name = "forcegun";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPushInt; param = 9000; },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "die",
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ param = function( obj )
				Game.turret_destroyed( obj )
				obj:solid_to( 0 )
				obj:sprite_z( -0.4 )
				Info.RevealEnemy( "TURRET" )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "turret-back", { 39, 22 } ) 
				Game.CreateScoreItems( 4, pos.x, pos.y )
			end },
			{ dur = 5000, num = 2 },
			{ com = constants.AnimComJump; param = 5 }
		},
	}
}



