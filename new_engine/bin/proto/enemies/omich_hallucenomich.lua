parent = "enemies/omich_flyer"

color = { 1, .8, .8, .8 }
trajectory_param1 = 6;
ghost_to = 255
drops_shadow = 0;

animations = 
{
	{ 
		-- �� ���� ��
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = 40 },
			{ com = constants.AnimComRealW; param = 37 },
			{ com = constants.AnimComRealH; param = 66 },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetRelativeAccX; param = 300 },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{
		--������� �����
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	}
}



