phys_max_x_vel = 5;
phys_max_y_vel = 10;
mp_count = 1;

gravity_x = 0;
gravity_y = 0.8;

ghost_to = constants.physEverything;

texture = "house-debris"

z = -0.002;

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ dur = 1 },
			{ param = function( obj ) mapvar.tmp[obj] = math.random( 5, 14 ) end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "big";
		frames = 
		{
			{ param = function( obj ) mapvar.tmp[obj] = math.random( 0, 4 ) obj:max_y_vel( 15 ) end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "fly";
		frames =
		{
			{},
			{ dur = 200, param = function( obj ) return 0, nil, mapvar.tmp[obj] end },
			{ com = constants.AnimComJump; param = function( obj ) if ObjectOnScreen( obj, 320 ) then return 0 else return 2 end end },
			{ com = constants.AnimComDestroyObject }
		}
	}
}



