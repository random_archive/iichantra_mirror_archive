--$(DESCRIPTION).���� �������
--$(DESCRIPTION).
--$(DESCRIPTION).�������� �������, ������������ �� ���� ����� ������. ������ �� ����� ����� ��������� �� ����� �����, ������ ��� ���� ��� ����� ���������.

reload_time = 200;
bullets_per_shot = 0;
damage_type = 2;
bullet_damage = 0;

-- �������� ������� ����
texture = "btard_half";

offscreen_distance = 640
offscreen_behavior = constants.offscreenNone


z = -0.03;

physic = 1;
--phys_ghostlike = 1;
phys_bullet_collidable = 0;
gravity_x = 0;
gravity_y = 0.4;
phys_max_vel_x = 10;
phys_max_vel_y = 10;

bounce = 0.2

overlay = {0};
ocolor = {{1, 0.8, 1, 1}}

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 34 },
			{ com = constants.AnimComRealH; param = 35 },
			{ com = constants.AnimComPushInt; param = 4000 },
			{ com = constants.AnimComPushInt; param = 8000 },
			{ com = constants.AnimComPushInt; param = -135 },
			{ com = constants.AnimComPushInt; param = -45 },
			{ com = constants.AnimComRandomAngledSpeed; param = 1 },
			{ dur = 1 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2"; param = 2 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "fly"}
		}
	},
	{
		name = "fly";
		frames = 
		{
			{ num = 0; dur = 50 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 33 },
			{ num = 1; dur = 50 },
			{ com = constants.AnimComRealW; param = 33 },
			{ com = constants.AnimComRealH; param = 30 },
			{ num = 2; dur = 50 },
			{ num = 3; dur = 50 },
			{ com = constants.AnimComRealW; param = 33 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 4; dur = 50 },
			{ num = 5; dur = 50 },
			{ com = constants.AnimComRealW; param = 33 },
			{ com = constants.AnimComRealH; param = 30 },
			{ num = 6; dur = 50 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 25 },
			{ num = 7; dur = 100 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 21 },
			{ num = 8; dur = 3000 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
