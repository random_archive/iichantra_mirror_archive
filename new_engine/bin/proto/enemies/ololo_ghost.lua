--forbidden
bounce = 0;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 200;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

mass = -1; --���������� ������� �����.

FunctionName = "CreateEnemy";

drops_shadow = 1;

-- �������� �������

texture = "ololo_smile";

z = -0.001;

health = 200;


animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComRealY; param = 64 },
			{ com = constants.AnimComRealX; param = 64 },
			{ dur = 1 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 9 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 11 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 1 },
			{ com = constants.AnimComCreateParticles; param = 3; txt = "pghostsmoke" },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- ��������
		name = "refresh";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 9 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 11 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComPushInt; param = 12 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit1"; param = 4 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 6 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 7 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "ghost_orbit2"; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },

			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2" },
			{ com = constants.AnimComCallFunction; txt = "ololo_ghost_hurt" },
			{ com = constants.AnimComRecover; }
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 0 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetAnim; txt = "shoot"; param = 128 },
			{ com = constants.AnimComSetAnim; txt = "change_position" },
		}
	},
	{
		name = "shoot";
		frames =
		{
			{ com = constants.AnimComFaceTarget; },
			{ com = constants.AnimComAdjustAim; },
			{ dur = 200; num = 0; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; param = 1; txt = "ghost_projectile" },
			{ dur = 200; num = 0; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; param = 1; txt = "ghost_projectile" },
			{ dur = 200; num = 0; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; param = 1; txt = "ghost_projectile" },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		name = "change_position";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 4000 },
			{ com = constants.AnimComPushInt; param = 66 },
			{ dur = 100; num = 0; com = constants.AnimComFlyToWaypoint; param = 24 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComJump; param = 3 }
		}
	},
	{
		name = "respawn";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "refresh" }
		}
	},
	{ 
		name = "retreat";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = health }
		}
	},
	{
		name = "final_death";
		frames = 
		{
			{ com = constants.AnimComDestroyObject }

		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComCallFunction; txt = "ololo_ghost_death" },
			{ dur = 1 },
			{ com = constants.AnimComSetHealth; param = health },
			{}
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComDestroyObject }
		}
	},
	{ 
		-- ��������
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
}



