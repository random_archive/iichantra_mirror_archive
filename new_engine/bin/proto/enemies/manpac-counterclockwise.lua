parent = "enemies/manpac-clockwise"

animations = 
{
	{
		name = "stuck";
		frames =
		{
			{ param = function( obj )
				obj:vel( { x = 0, y = 0 } )
				local acc = {}
				if mapvar.tmp[obj] == 6 then
					acc.x = 0
					acc.y = -0.4
					mapvar.tmp[obj] = 8
				elseif mapvar.tmp[obj] == 4 then
					acc.x = 0
					acc.y = 0.4
					mapvar.tmp[obj] = 2
				elseif mapvar.tmp[obj] == 2 then
					acc.x = 0.4
					acc.y = 0
					obj:sprite_mirrored( false )
					mapvar.tmp[obj] = 6
				else
					acc.x = -0.4
					acc.y = 0
					obj:sprite_mirrored( true )
					mapvar.tmp[obj] = 4
				end
				obj:acc( acc )
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "stuck_vertical";
		frames =
		{
			{ param = function( obj )
				obj:vel( { x = 0, y = 0 } )
				local acc = {}
				if mapvar.tmp[obj] == 6 then
					acc.x = 0
					acc.y = -0.4
					mapvar.tmp[obj] = 8
				elseif mapvar.tmp[obj] == 4 then
					acc.x = 0
					acc.y = 0.4
					mapvar.tmp[obj] = 2
				elseif mapvar.tmp[obj] == 2 then
					acc.x = 0.4
					acc.y = 0
					obj:sprite_mirrored( false )
					mapvar.tmp[obj] = 6
				else
					acc.x = -0.4
					acc.y = 0
					obj:sprite_mirrored( true )
					mapvar.tmp[obj] = 4
				end
				obj:acc( acc )
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	}
}



