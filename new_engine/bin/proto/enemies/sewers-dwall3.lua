parent = "enemies/sewers-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 8 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 9 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 10 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 11 }                                      
		}
	}
}
