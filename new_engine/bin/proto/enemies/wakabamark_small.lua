--$(DESCRIPTION).���� �������
--$(DESCRIPTION).
--$(DESCRIPTION).�������� �������, ������������ �� ���� ����� ������. ������ �� ����� ����� ��������� �� ����� �����, ������ ��� ���� ��� ����� ���������.
-- �������� ������� ����
texture = "wakabamark";

offscreen_distance = 640
offscreen_behavior = constants.offscreenNone


z = -0.03;

physic = 1;
--phys_ghostlike = 1;
phys_bullet_collidable = 0;
gravity_x = 0;
gravity_y = 0.3;
phys_max_vel_x = 10;
phys_max_vel_y = 10;

bounce = 0.5

overlay = {0};
ocolor = {{1, 0.8, 1, 1}}

faction_id = 0
faction_hates = { -1 }

local speed = 500
local next_frame = function(obj)
	mapvar.tmp[obj] = (mapvar.tmp[obj] or -1) + 1
	if mapvar.tmp[obj] >= 20 then
		mapvar.tmp[obj] = 0
	end
	return 0, nil, mapvar.tmp[obj]
end

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 15 },
			{ com = constants.AnimComRealH; param = 35 },
			{ com = constants.AnimComPushInt; param = 4000 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = -130 },
			{ com = constants.AnimComPushInt; param = -80 },
			{ com = constants.AnimComRandomAngledSpeed; param = 1 },
			{ param = function( obj )
				Resume( NewMapThread( function()
					Wait( 5000 )
					local vis = true
					if not obj:object_present() then return end
					for i=1,50 do
						if not obj:object_present() then return end
						vis = not vis
						SetObjInvisible( obj, vis )
						Wait( 100 )
					end
					if not obj:object_present() then return end
					SetObjDead( obj )
				end ))
			end },
			{ dur = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "fly"}
		}
	},
	{
		name = "fly";
		frames = 
		{
			{ dur = 50; num = 0; param = next_frame },
			{ com = constants.AnimComWaitForTarget; param = 200; txt = "follow" },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "follow";
		frames =
		{
			{ com = constants.AnimComSetMaxVelX; param = 5000 },
			{ com = constants.AnimComSetMaxVelY; param = 5000 },
			{ param = function( obj )
				obj:sprite_z( 0.8 )
				obj:gravity( { x = 0, y = 0 } )
				obj:solid_to( constants.physPlayer )
			end },
			{},
			{ dur = 50; num = 0; param = next_frame },
			{ com = constants.AnimComMoveToTarget; param = speed },
			{ com = constants.AnimComJump; param = 3 }
		}
	},
	{
		name = "target_dead";
		frames = 
		{
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetAnim; txt = "init" }
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{

			{ com = constants.AnimComPlaySound; txt = "item-score.ogg" },
			{ param = function( obj )
				Info.RevealItem( "WAKABAMARK" )
				local pos = obj:aabb().p
				Game.GainScore( GetObjectUserdata( ObjectPopInt(obj) ), 100, pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject }

		}
	}
}
