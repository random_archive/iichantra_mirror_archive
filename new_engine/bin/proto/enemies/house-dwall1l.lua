texture = "house-burned-01";
facing = constants.facingFixed

z = -0.15;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 1;
phys_ghostlike = 0;
ghost_to = constants.physSprite;

mass = -1;

phys_one_sided = 0;

phys_max_x_vel = 0;
phys_max_y_vel = 0;

gravity_x = 0;
gravity_y = 0;

faction_id = 3;

isometric_depth_x = 0.001;
isometric_depth_y = 0.001;

local group_size = 40

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealX; param = 15 },
			{ com = constants.AnimComRealY; param = 0 },
			{ param = function( obj, num )
				local num = 1
				local pos = obj:aabb().p
				if mapvar.tmp.destr_walls then
					local found_group = false
					for k, v in pairs( mapvar.tmp.destr_walls ) do
						if (pos.x + group_size >= v[1] and pos.x - group_size <= v[3]) and 
						   (pos.y + group_size >= v[2] and pos.y - group_size <= v[4]) then
							found_group = true
							mapvar.tmp.destr_walls[k] = { math.min( v[1], pos.x - group_size ), math.min( v[2], pos.y - group_size ), 
							                              math.max( v[3], pos.x + group_size ), math.max( v[4], pos.y + group_size ) }
							num = k
						end
					end
					if not found_group then
						num = #mapvar.tmp.destr_walls + 1
						mapvar.tmp.destr_walls[num] = { pos.x - group_size, pos.y - group_size, pos.x + group_size, pos.y + group_size }
					end
				else
					mapvar.tmp.destr_walls = { { pos.x - group_size, pos.y - group_size, pos.x + group_size, pos.y + group_size } }
				end
				mapvar.tmp[obj] = num
				if not mapvar.tmp.destruction then
					mapvar.tmp.destruction = {}
				end
				if not mapvar.tmp.destruction[num] then
					mapvar.tmp.destruction[num] = {}
				end
				table.insert( mapvar.tmp.destruction[num], obj )
			end },
			{ com = constants.AnimComSetHealth; param = 150 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 38 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 38 }
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 38 }
		}
	},
	{
		name = "pre25%";
		frames =
		{
			{ param = function( obj ) 
				obj:solid_to(0) obj:solid(false) 
				obj:sprite_z( 0.9 )
				--obj:sprite_isometric_depth( { x = 0, y = 0 } )
			end; num = 374 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pwood" },
--			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComSetAnim; txt = "25%" },
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 410 }                                      
		}
	},
	{
		name = "beam";
		frames =
		{
			{ param = function ( obj )
				obj:health( 1 )
				local num = mapvar.tmp[ obj ]
				for k, v in pairs( mapvar.tmp.destruction[num] ) do
					v:health( 1 )
					SetObjAnim( v, "pre25%", false )
				end
			end }
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.super_laser; txt = "beam" },
			{ com = constants.AnimComPop },   
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPlaySound; txt = "wood.ogg" },
			{ com = constants.AnimComCreateParticles; txt = "pwood" },
			{ param = function ( obj )
				local health = obj:health()
				local pos = obj:aabb().p
				CreateParticleSystem( "pwood_small", pos.x - 20, pos.y )
				local num = mapvar.tmp[ obj ]
				if health < 0 then
					return
				elseif health < 100 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "pre25%", false )
					end
				elseif health < 125 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "50%", false )
					end
				elseif health < 150 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "75%", false )
					end
				else
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "idle", false )
					end
				end
			end }
		}
	},
}
