parent = "enemies/sewers-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 20 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 21 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 22 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 35 }                                      
		}
	}
}
