physic = 1;

phys_max_y_vel = 5
--gravity_y = -0.4

-- �������� ������� ����
texture = "warhead2";

z = -0.002;

mass = -1;

animations = 
{
	{
		name = "init";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 294 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAccY; param = -1000 },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "fly";
		frames =
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ param = function( obj )
				if not mapvar.tmp[obj] then
					mapvar.tmp[obj] = obj:aabb().p.y
				else
					if math.abs( mapvar.tmp[obj] - obj:aabb().p.y ) < 5 then
						SetObjDead( obj )
					end
					mapvar.tmp[obj] = obj:aabb().p.y
				end
			end },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( this )
				local touchee = GetObjectUserdata( ObjectPopInt( this ) )
				DamageObject( touchee, 40 )
				local dx = 5
				if touchee:aabb().p.x < this:aabb().p.x then
					local vel = touchee:vel()
					vel.x = vel.x + dx
					touchee:vel(vel)
				end
			end },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "stuck_vertical";
		frames =
		{
			{ com = constants.AnimComDestroyObject }
		}
	}
}
