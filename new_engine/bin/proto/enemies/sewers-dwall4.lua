parent = "enemies/sewers-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 12 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 13 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 14 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 15 }                                      
		}
	}
}
