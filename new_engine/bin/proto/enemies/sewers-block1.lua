texture = "sewers-block";
facing = constants.facingFixed

z = -0.0015;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 1;
phys_ghostlike = 0;
ghost_to = constants.physSprite + constants.physBullet;

mass = -1;

phys_one_sided = 0;

phys_max_x_vel = 0;
phys_max_y_vel = 0;

gravity_x = 0;
gravity_y = 0;

faction_id = 3;

isometric_depth_x = 0.001;
isometric_depth_y = 0;

local group_size = 64

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComRealW; param = 16 },
			{ com = constants.AnimComRealX; param = 8 },
			{ com = constants.AnimComRealY; param = 0 },
			{ param = function( obj )
				local pos = obj:aabb().p
				if mapvar.tmp.blocks then
					table.insert( mapvar.tmp.blocks, { obj, pos.x, pos.y } )
				else
					mapvar.tmp.blocks = { { obj, pos.x, pos.y } }
				end
			end },
			{ com = constants.AnimComSetAnim; txt = "closed" },
		}
	},
	{
		name = "closed";
		frames =
		{
			{ dur = 1; num = 0 }
		}
	},
	{
		name = "open";
		frames =
		{
--[[
			{ param = function( obj ) obj:solid_to(0) obj:solid(false) end }, ]]
			{ com = constants.AnimComDestroyObject }
--			{ dur = 1; num = 3 }                                      
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComSetAnim; txt = "closed" }
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComDestroyObject; param = 2 }
		}
	}
}
