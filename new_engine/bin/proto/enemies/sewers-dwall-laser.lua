parent = "enemies/sewers-dwall1"

overlay = { 0, 1 }
ocolor = { {1,1,1,0}, {1,1,1,1} }

local group_size = 60

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealX; param = 16 },
			{ com = constants.AnimComRealY; param = 16 },
			{ param = function( obj, num )
				local num = 1
				local pos = obj:aabb().p
				if mapvar.tmp.destr_walls_laser then
					local found_group = false
					for k, v in pairs( mapvar.tmp.destr_walls_laser ) do
						if (pos.x + group_size >= v[1] and pos.x - group_size <= v[3]) and 
                           			   (pos.y + group_size >= v[2] and pos.y - group_size <= v[4]) then
							found_group = true
							mapvar.tmp.destr_walls_laser[k] = { math.min( v[1], pos.x - group_size ), math.min( v[2], pos.y - group_size ), 
							                              math.max( v[3], pos.x + group_size ), math.max( v[4], pos.y + group_size ) }
							num = k
						end
					end
					if not found_group then
						num = #mapvar.tmp.destr_walls_laser + 1
						mapvar.tmp.destr_walls_laser[num] = { pos.x - group_size, pos.y - group_size, pos.x + group_size, pos.y + group_size }
					end
				else
					mapvar.tmp.destr_walls_laser = { { pos.x - group_size, pos.y - group_size, pos.x + group_size, pos.y + group_size } }
				end
				mapvar.tmp[obj] = num
				if not mapvar.tmp.destruction_laser then
					mapvar.tmp.destruction_laser = {}
				end
				if not mapvar.tmp.destruction_laser[num] then
					mapvar.tmp.destruction_laser[num] = {}
				end
				table.insert( mapvar.tmp.destruction_laser[num], obj )
			end },
			{ com = constants.AnimComSetHealth; param = 400 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
	{
		name = "beam";
		frames =
		{
			{ param = function ( obj )
				obj:health( 1 )
				local num = mapvar.tmp[ obj ]
				for k, v in pairs( mapvar.tmp.destruction_laser[num] ) do
					v:health( 1 )
					SetObjAnim( v, "pre25%", false )
				end
			end }
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.super_laser; txt = "beam" },
			{ com = constants.AnimComPop },   
			{ com = constants.AnimComPop },
			{ param = function( obj )
				if (not Game.info.reinforced_walls) and ObjectOnScreen( obj, -100 ) then
					local thread
					local function blink()
						thread = NewThread( function()
							local tiem = GetCurTime()
							local t = 0
							local tab = mapvar.tmp.destruction_laser[mapvar.tmp[ obj ] or 0] or {}
							while GetCurTime() - tiem < 900 * math.pi / 2 or t > 0.01 do
								for k, v in ipairs( tab ) do
									SetObjSpriteColor( v, { 1, 1, 1, t }, 0 )
								end
								Wait( 1 )
								t = 1 - (0.5 + 0.5 * math.cos( (GetCurTime() - tiem) / 100 ))
							end
							for k, v in ipairs( tab ) do
								SetObjSpriteColor( v, { 1, 1, 1, 0 }, 0 )
							end
							thread = nil
						end )
						Resume( thread )
					end
					local function blink_stop()
						if thread then
							StopThread( thread )
							thread = nil
						end
						local tab = mapvar.tmp.destruction_laser[mapvar.tmp[ obj ] or 0] or {}
						for k, v in ipairs( tab ) do
							SetObjSpriteColor( v, { 1, 1, 1, 0 }, 0 )
						end
					end
					Conversation.create( {
						function( var ) 
							if Game.GetPlayersNum() == 2 then
								return "BOTH-PLAYERS"
							elseif mapvar.actors[1].info.character == "pear15unyl" then
								return "UNYL-SINGLE"
							end
						end,
						blink,
						{ sprite = "portrait-soh" },
						"�������, ��� ����������� ����� ��������. ����� ��� ����� �� �������.",
						blink_stop,
						nil,
						blink,
						{ type = "LABEL", "UNYL-SINGLE" },
						{ sprite = "portrait-unyl" },
						"�� ������, ��� � ����� ���������� ������ ���� �������.",
						blink_stop,
						nil,
						blink,
						{ type = "LABEL", "BOTH-PLAYERS" },
						{ sprite = "portrait-unyl" },
						"�� ������, ��� �� ������ ���������� ���� ������ ���� �������.",
						{ sprite = nil },
						{ sprite = "portrait-soh", right = true },
						"� ����� ������ ����� �������� ����� ��������.",
						blink_stop,
						nil
					} ):start(true)
					Game.info.reinforced_walls = true
				end
			end },
			{ com = constants.AnimComRecover }
		}
	}
}
