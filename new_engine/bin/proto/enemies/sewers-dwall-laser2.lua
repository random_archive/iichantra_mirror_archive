parent = "enemies/sewers-dwall-laser"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 24 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 25 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 26 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 27 }                                      
		}
	}
}
