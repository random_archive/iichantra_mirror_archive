parent = "enemies/dark_green_slime"

color = {61/255, 114/255, 163/255, 1}
overlay = {0};
ocolor = {{ 1, 1, 1, 1}}

local next_frame = function( obj )
	if not mapvar.tmp[obj] then
		mapvar.tmp[obj] = { c = 0, d = 1 }
	end
	local it = mapvar.tmp[obj]
	it.c = it.c + it.d
	if it.c == 3 or it.c == 0 then
		it.d = -it.d
	end
	return 0, nil, it.c
end

--[[
--This is an example of adding an enemy info from the prototype. It will, however, not be listed in enemies info until first encountered and loaded.
local info_handle = Info.AddEnemy({
	name = "�������, �����",
	sprite = nil,
	description = "����� �������������� ������ �����������, ������ ����� �������� �� ��, ��� �����-������ �� ������� � JRPG. ��������� ������./n/n/cffff99�������/n/n/cffffff�� ���� ����� ����������� ��� � ������ ������, ��-�� ���� ������������ ���������� � �������. ������ ������� �� ������� �, �������, ������ ��� ����� ����� ������, �������� � ������ ����-������ �� ������./n/n/cffff99����������/n/n/cffffff���� �������� ����������. ����� ����������� �� ������������� �������."
})
]]

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			
			{ com = constants.AnimComPushInt; param = -26},
			{ com = constants.AnimComPushInt; param = 20},
			{ com = constants.AnimComMPSet; param = 1 },
			
			{ com = constants.AnimComPushInt; param = 1},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComCreateEffect; txt = "slime_trail", param = 4 },
			
			{ param = function( obj )
				local children = GetChildren( obj )
				if not children then return end
				for k, v in pairs( children ) do
					mapvar.tmp[v] = 
					{
						start_color = { 61/255, 114/255, 163/255, 1 },
						end_color = { 61/255, 114/255, 163/255, 0 }
					}
				end
			end },
			
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 50 * 39/25 },
			{ com = constants.AnimComRealH; param = 50 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 50 * 39/25 },
			{ com = constants.AnimComRealH; param = 50 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; param = next_frame },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{
		name = "edge";
		frames =
		{
			{ com = constants.AnimComJump; param = function( obj )
				local plane = obj:suspected_plane()
				local o1 = obj:aabb()
				if plane then
					local o2 = plane:aabb()
					if not mapvar.tmp[obj].time then mapvar.tmp[obj].time = Loader.time end
					if mapvar.tmp[obj].time + 150 < Loader.time then
					--if o2.W > o1.W + 20 then
						mapvar.tmp[obj].time = Loader.time
						return 0
					end
				end
				return 1
			end },
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetRelativeAccX; param = 450 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 450 },
			{ dur = 150; num = 0, param = next_frame },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComLoop }
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "slime.ogg" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime3", param = 0 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime3", param = 0 },
			{ com = constants.AnimComRealW; param = 40 * 67/25 },
			{ com = constants.AnimComRealH; param = 40 * 35/25 },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ num = 4; dur = 100 },
			{ num = 5; dur = 100 },
			{ num = 6; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				--Info.RevealEnemy( info_handle )
				Info.RevealEnemy( "BLUE_SLIME" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "blue_slime", { 39, 25 } ) 
				Game.CreateScoreItems( 2, pos.x, pos.y )
			end },
			{ num = 7; dur = 100 },
			{ num = 8; dur = 100 },
			{ com = constants.AnimComDestroyObject }
		}
	},
}



