--forbidden
bounce = 0;

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 200;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

FunctionName = "CreateEnemy";

-- �������� �������

drops_shadow = 1;
ghost_to = 252;

z = -0.001;

local speed = 300*((difficulty-1)/3+1);
local health = 60;
if difficulty > 1 then
	health = 80;
elseif difficulty < 1 then
	health = 40;
end

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "pacman_big.ogg" },
			{ dur = 350; num = 0; },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComDestroyObject }
		}
	}
	
}



