--forbidden
physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
mp_count = 1;
offscreen_behavior = constants.offscreenNone

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.8;

faction_id = 4;
faction_hates = { -1, -2 };

-- �������� �������

texture = "pytard";

z = -0.1;

mass = -1

local function aabb_in_area( p, area )
	if  not (p.p.x - p.W > area[3] or p.p.x + p.W < area[1])
	and not (p.p.y - p.H > area[4] or p.p.y + p.H < area[2]) then
		return true
	else
		return false
	end
end

local pytard_hurt = function( obj )
	local damage = ObjectPopInt( obj )
end

local pytard_teleport = function( obj )
	local target = obj:target()
	local self = obj:aabb()
	if not target or not target:object_present() then
		SetObjAnim( obj, "idle", false )
	end
	if not target:suspected_plane() then
		return
	end
	target = target:aabb().p
	if (target.y < self.p.y - self.H) or ((target.y > self.p.y + self.H) and obj:suspected_plane()) then
		local telepoints = Loader.level.telepoints or { { target.x, target.y } }
		local num = 1
		for i = 2, #telepoints do
			if math.abs( telepoints[i][1] - target.x ) + math.abs( telepoints[i][2] - target.y ) < 
			   math.abs( telepoints[num][1] - target.x ) + math.abs( telepoints[num][2] - target.y ) then
				num = i
			end
		end
		CreateParticleSystem( "ppytard_teleport_out", self.p.x, self.p.y, 64 )
		SetObjPos( obj, telepoints[num][1], telepoints[num][2] )
		CreateParticleSystem( "ppytard_teleport_in", obj, 0, 0, 0 )
		return SetObjAnim( obj, "idle", false )
	end
end

local pytard_processor = function( obj )
	mapvar.tmp.boss_object = obj
	local target = obj:target()
	local self = obj:aabb().p
	if not target or not target:object_present() then
		return
	end
	target = target:aabb().p
	local dist = math.sqrt( math.pow( self.x - target.x, 2 ) + math.pow( self.y - target.y, 2 ) )
	if dist <= 120 then
		return SetObjAnim( obj, "attack", false )
	end
end

local labels
animations, labels = WithLabels
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 108 },
			{ com = constants.AnimComRealH; param = 82 },
			{ com = constants.AnimComSetAnim; txt = "wait" }	
		}
	},
	{
		name = "wait";
		frames =
		{
			{ com = constants.AnimComRealX; param = 72 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 82 },
			{ label = "LOOP" },
			{ dur = 150; num = 0 },
			{ dur = 150; num = 1; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 150; num = 2; com = constants.AnimComRealX; param = -3 + 72 },
			{ dur = 150; num = 3; com = constants.AnimComRealX; param = -1 + 72 },
			{ com = constants.AnimComJump; param = function( obj )
				return iff( mapvar.tmp.boss_start, labels.START, labels.LOOP )
			end },
			{ label = "START" },
			--{ com = constants.AnimComSetProcessor; param = RegisterFunction( pytard_processor ) },
			{ dur = 1; num = 3; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ com = constants.AnimComJump; param = "START" }
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			--72
			{ com = constants.AnimComRealX; param = 72 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 82 },
			{ dur = 150; num = 0 },
			{ dur = 150; num = 1; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 150; num = 2; com = constants.AnimComRealX; param = -3 + 72 },
			{ dur = 150; num = 3; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 150; num = 0; com = constants.AnimComRealX; param = -0 + 72 },
			{ dur = 150; num = 1; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 150; num = 2; com = constants.AnimComRealX; param = -3 + 72 },
			{ dur = 150; num = 3; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 150; num = 0; com = constants.AnimComRealX; param = -0 + 72 },
			{ dur = 150; num = 1; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 150; num = 2; com = constants.AnimComRealX; param = -3 + 72 },
			{ dur = 150; num = 3; com = constants.AnimComRealX; param = -1 + 72 },
			{ dur = 1; num = 3; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ com = constants.AnimComLoop; }	
		}
	},

	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ param = pytard_hurt },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood-wound"; param = 2 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "move";
		frames = 
		{
			{ param = pytard_teleport },
			{ param = pytard_processor },
			{ com = constants.AnimComRealX; param = 84 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 37 },
			{ com = constants.AnimComRealH; param = 78 },
			{ com = constants.AnimComMaterialSound; param = 1},
			{ dur = 100; num = 5; com = constants.AnimComMoveToTargetX; param = 200 },
			{ param = pytard_processor },
			{ com = constants.AnimComRealX; param = 73 },
			{ dur = 100; num = 6; com = constants.AnimComMoveToTargetX; param = 200 },
			{ param = pytard_processor },
			{ com = constants.AnimComRealX; param = 57 },
			{ com = constants.AnimComRealH; param = 79 },
			{ dur = 100; num = 7; com = constants.AnimComMoveToTargetX; param = 200 },
			{ param = pytard_processor },
			{ com = constants.AnimComRealH; param = 77 },
			{ com = constants.AnimComRealX; param = 57 },
			{ com = constants.AnimComMaterialSound; },
			{ dur = 100; num = 8; com = constants.AnimComMoveToTargetX; param = 200 },
			{ param = pytard_processor },
			{ com = constants.AnimComRealX; param = 56 },
			{ dur = 100; num = 9; com = constants.AnimComMoveToTargetX; param = 200 },
			{ param = pytard_processor },
			{ com = constants.AnimComRealX; param = 72 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 100; num = 10; com = constants.AnimComMoveToTargetX; param = 200 },
			{ com = constants.AnimComLoop },
		}
	},
	{
		name = "attack";
		frames =
		{
			{ com = constants.AnimComRealX; param = 83 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 37 },
			{ com = constants.AnimComRealH; param = 81 },
			{ com = constants.AnimComFaceTarget },
			{ dur = 500; num = 11 },
			{ com = constants.AnimComRealX; param = 84 },
			{ com = constants.AnimComRealH; param = 79 },
			{ dur = 400; num = 12 },
			{ com = constants.AnimComRealX; param = 38 },
			{ com = constants.AnimComRealH; param = 77 },
			{ dur = 200; num = 13 },
			{ com = constants.AnimComRealH; param = 76 },
			{ com = constants.AnimComRealX; param = 36 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			--[[{ dur = 100; num = 14; com = constants.AnimComCreateEnemyBullet; txt = "pear15pytard-slash" },]]
			{ param = function( obj )
				local pos = obj:aabb().p
--[[
				if obj:sprite_mirrored() then
					CreateColorBox( pos.x - 120, pos.y - 30, pos.x, pos.y + 30, 1.1, { 1, 0, 0, 0.5 } )
				else
					CreateColorBox( pos.x, pos.y - 30, pos.x + 120, pos.y + 30, 1.1, { 1, 0, 0, 0.5 } )
				end
--]]
				local area = iff( obj:sprite_mirrored(), { pos.x - 120, pos.y - 30, pos.x, pos.y + 30 }, { pos.x, pos.y - 30, pos.x + 120, pos.y + 30 } )
				local ch = GetPlayerCharacter( 1 )
				local p
				if ch then
					p = ch:aabb()
					if aabb_in_area( p, area ) then
						DamageObject( ch, 60 )
					end
				end
				ch = GetPlayerCharacter( 2 )
				if ch then
					p = ch:aabb()
					if aabb_in_area( p, area ) then
						DamageObject( ch, 60 )
					end
				end
				for k, v in pairs(mapvar.tmp.portraits or {}) do
					p = k:aabb()
					if v and aabb_in_area( p, area ) then
						SetObjAnim( k, "fall", false )
						mapvar.tmp.portraits[k] = false
					end
				end
				for k, v in pairs(mapvar.tmp.enemies or {}) do
					if k:object_present() then
						p = k:aabb()
						if v and aabb_in_area( p, area ) then
							DamageObject( k, 1000 )
						end
					end
				end
				if mapvar.tmp.rolled_out then
					ch = mapvar.tmp.tank
					if ch and ch:object_present() then
						p = ch:aabb()
						p.W = p.W * 2
						if aabb_in_area( p, area ) then
							DamageObject( mapvar.tmp.tank, 80, Game.damage_types.ghost )
						end
					end
				end
			end },
			{ com = constants.AnimComRealX; param = 5 },
			{ dur = 100; num = 15 },
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 100; num = 16 },
			{ com = constants.AnimComRealH; param = 81 },
			{ com = constants.AnimComRealX; param = 13 },
			{ dur = 100; num = 17 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
}



