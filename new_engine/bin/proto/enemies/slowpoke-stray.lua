offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
touch_detection = constants.tdtFromTop;

drops_shadow = 1;

faction_id = 1;
faction_hates = { -1, -2 };

mass = 9000;
gravity_x = 0;
gravity_y = 0.8;

-- �������� �������

texture = "slowpoke";

z = -0.02;

local speed = 200;
local labels;

animations, labels = WithLabels
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetHealth; param = 200; },
			{ com = constants.AnimComSetTouchable; param = 1; },
			{ com = constants.AnimComSetAnim; txt = "idle"; }
		}
	},
	{
		name = "final_count";
		frames = 
		{

			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 8 }
		}
	},
	{
		name = "idle";
		frames = 
		{
			{ dur = 100; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase"; },
			{ com = constants.AnimComLoop; }
		}
	},
	{
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop; },
			{ com = constants.AnimComReduceHealth; },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComCreateParticles; param = 2; txt = "pblood_gravity_small"; },
			{ com = constants.AnimComRecover }
		}
	},
	{	name = "chase";
		frames =
		{
			{ constants.AnimComFaceTarget }, --Does not work for some reason.
			{ param = function( obj )
				if obj:aabb().p.x > obj:target():aabb().p.x then
					SetObjSpriteMirrored( obj, true );
				end
			end },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = speed },
			{ com = constants.AnimComSetRelativeVelX; param = speed },
			{ dur = 200; num = 0 },
			{ dur = 200; num = 1 },
			{ dur = 200; num = 2 },
			{ dur = 200; num = 3 },
			{ dur = 200; num = 4 },
			{ dur = 200; num = 5 },
			{ dur = 200; num = 6 },
			{ dur = 200; num = 7 },
			{ dur = 200; num = 0 },
			{ dur = 200; num = 1 },
			{ dur = 200; num = 2 },
			{ dur = 200; num = 3 },
			{ dur = 200; num = 4 },
			{ dur = 200; num = 5 },
			{ dur = 200; num = 6 },
			{ dur = 200; num = 7 },
			{ com = constants.AnimComSetAnim; txt = "shoot" }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "sleep"; }
		}
	},
	{
		name = "shoot";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ param = function( obj )
				mapvar.tmp[obj] = obj:sprite_mirrored()
			end },
			{ com = constants.AnimComFaceTarget },
			{ dur = 100; num = 8; com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealH; param = 51; },
			{ dur = 100; num = 9; com = constants.AnimComFaceTarget; },
			{ dur = 100; num = 10; com = constants.AnimComRealH; param = 72; },
			{ dur = 100; num = 11; com = constants.AnimComRealH; param = 71; },
			{ dur = 100; num = 12; com = constants.AnimComRealH; param = 70; },
			{ com = constants.AnimComRealH; param = 70; },
			{ com = constants.AnimComPushInt; param = 60 },
			{ com = constants.AnimComAdjustAim; param = 5 },
			{ com = constants.AnimComFaceTarget },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ com = constants.AnimComRealX; param = -3 },
			{ dur = 100; num = 13; com = constants.AnimComAimedShot; txt = "pear15-slowpoke-projectile"; },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 14; com = constants.AnimComRealH; param = 62; },
			{ dur = 100; num = 15; com = constants.AnimComRealH; param = 51; },
			{ com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComJump; param = function( obj )
				if obj:sprite_mirrored() ~= mapvar.tmp[obj] then
					return labels.MIRROR
				else
					return labels.RESUME
				end
			end },
			{ label = "MIRROR" },
			{ com = constants.AnimComMirror },
			{ label = "RESUME" },
			{ com = constants.AnimComSetAnim; txt = "move"; }
		}
	},
	{
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "reinit"; }
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComBounceObject; param = 2000; },
			{ com = constants.AnimComStartDying; },
			{ param = function()
				Game.ProgressAchievement( "PLUMBER", 1 )
			end },
			{ com = constants.AnimComSetAnim; txt = "die"; }
		}
	},
	{
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComSetGravity }, 
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetTouchable; param = 0; },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pmeat" },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComPlaySound; txt = "slowpoke-death.ogg" },
			{ param = function(obj)
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "SLOWPOKE_STRAY" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "slowpoke-stray", { 42, 32 } )
				Game.CreateScoreItems( 4, pos.x, pos.y )
			end },
			{ com = constants.AnimComRealX; param = 11; },
			{ dur = 100; num = 17; com = constants.AnimComRealH; param = 45; },
			{ com = constants.AnimComRealX; param = 15; },
			{ dur = 100; num = 18; com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; param = 20; },
			{ dur = 100; num = 19; com = constants.AnimComRealH; param = 58; },
			{ com = constants.AnimComRealX; param = 28; },
			{ dur = 100; num = 20; com = constants.AnimComRealH; param = 48; },
			{ dur = 100; num = 21; com = constants.AnimComRealH; param = 30; },
			{ dur = 100; num = 22; com = constants.AnimComRealH; param = 22; },
			{ com = constants.AnimComSetZ; param = -450; label = "KEEP-CORPSE"},
			{ dur = 5000; num = 23; com = constants.AnimComRealH; param = 10 },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 23; com = constants.AnimComJumpIfCloseToCamera; param = "KEEP-CORPSE" },
			{ num = 23 }
		}
	},
	{
		name = "land";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "reinit"; }
		}
	},
	{
		name = "reinit";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetAnim; txt = "idle"; }
		}
	},
	{
		name = "sleep";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 10; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComRealW; param = 86; },
			{ dur = 100; num = 48; com = constants.AnimComRealH; param = 41; },
			{ com = constants.AnimComRealX; param = 15; },
			{ dur = 100; num = 49; com = constants.AnimComRealH; param = 37; },
			{ dur = 100; num = 50; com = constants.AnimComRealH; param = 34; },
			{ dur = 300; num = 51; com = constants.AnimComRealH; param = 29; },
			{ dur = 300; num = 52; },
			{ dur = 300; num = 53; label = "SLEEP-REPEAT" },
			{ dur = 900; num = 54; },
			{ dur = 300; num = 53; },
			{ dur = 300; num = 52; },
			{ dur = 900; num = 51; },
			{ com = constants.AnimComJump; param = "SLEEP-REPEAT"; }
		}
	},
}
