physic = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 0.5;
phys_max_y_vel = 50;
mp_count = 1;

gravity_x = 0;
gravity_y = 0.8;
mass = -1;

texture = "slowlek";

z = -0.001;

faction_id = 1;
faction_hates = { -1, -2 };

drops_shadow = 1;

animations, labels = WithLabels
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 54 },
			{ com = constants.AnimComRealH; param = 52 },
			{ com = constants.AnimComSetHealth; param = 600*difficulty },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 1 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local target = GetNearestEnemy( obj )
				if target then
					target = target:aabb().p
					if pos.x > target.x then
						SetObjSpriteMirrored( obj, true )
					end
				end
			end },
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; txt = "move"; param = 10000 },
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ param = function(this) Game.FlashObject( this, {1,1,1,1} ) end },
			{ param = function( this )
				local damage_type = ObjectPopInt( this )
				local vel = this:vel()
				local pos = this:aabb().p
				local dir = this:last_hit_from()
				local l
				if damage_type == Game.damage_types.force_primary then
					dir.x = pos.x - dir.x
					dir.y = pos.y - dir.y
					l = math.sqrt( math.pow( dir.x, 2 ) + math.pow( dir.y, 2 ) )
					dir.x = dir.x / l
					dir.y = dir.y / l
					vel.x = vel.x + dir.x * 15
					vel.y = vel.y + dir.y * 15
					vel.y = vel.y - 5
					this:vel( vel )
				elseif damage_type == Game.damage_types.force_alt then
					dir.x = pos.x - dir.x
					dir.y = pos.y - dir.y
					l = math.sqrt( math.pow( dir.x, 2 ) + math.pow( dir.y, 2 ) )
					dir.x = dir.x / l
					dir.y = dir.y / l
					vel.x = vel.x + dir.x * -15
					vel.y = vel.y + dir.y * -15
					vel.y = vel.y - 5
					this:vel( vel )
				end
			end },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks" },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Создание
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 800  },
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 17 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "shoot" }
		}
	},
	{
		name = "shoot";
		frames =
		{
			--{ com = constants.AnimComStop },
			{},
			{ com = constants.AnimComPushInt; param = 45 },
			{ com = constants.AnimComAdjustAim; param = 5 },
			{ com = constants.AnimComJump; param = function(this)
				local target = this:target()

				if target and target:object_present() then
					if this:sprite_mirrored() then
						if target:aabb().p.x > this:aabb().p.x then
							return labels.SKIP_FIRE
						end
					else
						if target:aabb().p.x < this:aabb().p.x then
							return labels.SKIP_FIRE
						end
					end 
				end
				return labels.FIRE
			end },
			{ label = "FIRE" },
			{ com = constants.AnimComPushInt; param = 20; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ dur = 20; num = 0; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ com = constants.AnimComPushInt; param = 20; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ dur = 20; num = 0; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ com = constants.AnimComPushInt; param = 20; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ dur = 20; num = 0; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ com = constants.AnimComPushInt; param = 20; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ dur = 20; num = 0; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ com = constants.AnimComPushInt; param = 20; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ dur = 20; num = 0; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ com = constants.AnimComPushInt; param = 20; },
			{ com = constants.AnimComPushInt; param = -10; },
			{ dur = 20; num = 0; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ label = "SKIP_FIRE" },
			{ com = constants.AnimComSetAnim; txt = "move" }			
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComRecover },
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealW; param = 67 },
			{ com = constants.AnimComRealH; param = 42 },
			{ param = function( obj )
				mapvar.tmp.slowleks_destroyed = ( mapvar.tmp.slowleks_destroyed or 0 ) + 1
				obj:sprite_z( -0.4 )
				Info.RevealEnemy( "SLOWLEK" )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "slowlek", { 35, 30 } ) 
				Game.CreateScoreItems( 10, pos.x, pos.y )
			end },
			{ label = "KEEP_CORPSE" },
			{ dur = 5000; num = 4 },
			{ com = constants.AnimComPushInt; param = 400 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = "KEEP_CORPSE" },
			{ num = 4 }
		}
	},
	{ 
		name = "stuck";
		frames = 
		{
			{ com = constants.AnimComMirror },	
			{ com = constants.AnimComSetRelativeAccX; param = 800  },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 54 },
			{ com = constants.AnimComRealH; param = 52 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
}



