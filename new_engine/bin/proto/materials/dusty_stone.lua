friction = 1

sprites =
{
	"dust-run",
	"dust-land",
	"dust-stop"
}

sounds =
{
	"foot-left.ogg",
	"foot-right.ogg",
	"stop.ogg"
}
