--forbidden
texture = "boss_wdlhand";

z = -0.39;
phys_ghostlike = 1;
physic = 1;
phys_bullet_collidable = 0;
phys_max_x_vel = 9000;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

effect = 1;
ghost_to = constants.physBullet

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 300 },
			{ com = constants.AnimComRealH; param = 200 },
			{ dur = 1; num = 0 },
			{ com = constants.AnimComSetAnim; txt = "uploop" }
		}
	},
	{
		name = "up";
		frames =
		{
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 4; },
			{ com = constants.AnimComSetAnim; txt = "uploop" }
		}
	},
	{
		name = "down";
		frames =
		{
			{ dur = 50; num = 4; },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComPushInt; param = 150 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "wd-punch"; param = 3 },
			{ dur = 50; num = 3; },
			{ param = function( obj )
				mapvar.tmp[obj]:solid_to( 0 )
			end },
			{ dur = 50; num = 2; },
			{ dur = 50; num = 1; },
			{ dur = 50; num = 0; },
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-explosion.ogg" },
			{ dur = 400; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ param = function( obj )
				mapvar.tmp[obj]:solid_to( constants.physEverything )
			end },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 4; },
			{ com = constants.AnimComSetAnim; txt = "uploop" }
		}
	},
	{
		name = "uploop";
		frames =
		{
			{ dur = 50; num = 4; },
			{ com = constants.AnimComLoop }
		}
	}
}
