--forbidden
texture = "boss_wdface";

z = -0.94;
phys_ghostlike = 1;
physic = 1;
phys_bullet_collidable = 0;
phys_max_x_vel = 9000;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

effect = 1;
ghost_to = constants.physBullet

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 100 },
			{ com = constants.AnimComRealH; param = 100 },
			{ dur = 1; num = 0 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{
		name = "loop";
		frames =
		{
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 4; },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "loop2";
		frames =
		{
			{ dur = 100; num = 5; },
			{ dur = 100; num = 6; },
			{ dur = 100; num = 7; },
			{ dur = 100; num = 8; },
			{ dur = 100; num = 9; },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "death";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 220+35 },
			{ com = constants.AnimComPushInt; param = 70 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2"; param = 0 },
			{ com = constants.AnimComPushInt; param = 220-35 },
			{ com = constants.AnimComPushInt; param = 70 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2"; param = 0 },
			{ com = constants.AnimComPushInt; param = 220 },
			{ com = constants.AnimComPushInt; param = 70+40 },
			{ com = constants.AnimComCreateParticles; txt = "pblood2"; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComSetColor },
			{ dur = 100; num = 0; },
			{ com = constants.AnimComPushInt; param = 155 },
			{ com = constants.AnimComPushInt; param = 155 },
			{ com = constants.AnimComPushInt; param = 155 },
			{ com = constants.AnimComSetColor },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComSetColor },
			{ dur = 100; num = 0; },
		}
	}
}
