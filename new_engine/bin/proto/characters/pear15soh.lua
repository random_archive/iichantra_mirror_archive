--forbidden
name = "sohchan";

additional_jumps = 1;

main_weapon = "pear15soh_primary";
alt_weapon = "pear15soh_alternative";

health = 120;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2.25; 
phys_max_y_vel = 20; --2/5
phys_jump_vel = 7.25; --7.25/8
phys_walk_acc = 1.75;
gravity_x = 0;
gravity_y = 0.25; --5/6

drops_shadow = 1;

mp_count = 2;

faction_id = -1;

isometric_depth_x = 0.001;

-- �������� �������

--LoadTexture("soh-chan1024.png")
texture = "soh-chan1024";
z = -0.0015;

aiming_speed = 0.2;

--DISCLAIMER: I cannot be hold accountable if you go insane while trying to read and comprehend this file

local labels
animations, labels = WithLabels
{
	{
	name = "idle";
	frames =
		{
			{ param = function( obj )
				if not mapvar then mapvar = {} end
				if not mapvar.tmp then mapvar.tmp = {} end
				if not mapvar.tmp.chartime then mapvar.tmp.chartime = {} end
				mapvar.tmp.chartime[obj] = 0
			end },
			{ dur = 0 },
			{ com = constants.AnimComJump, param = function( obj )
				if mapvar.tmp.fail then
					return labels.FAIL
				end
				if mapvar.tmp.talk and mapvar.tmp.talk[obj] then
					if mapvar.tmp.talk[obj] == "transmission" then
						return labels.TRANSMISSION
					end
					mapvar.tmp.talk[obj] = false
					return labels.TALK
				end
				return 2
			end },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -50, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 0, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 1, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 2, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 3, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComJump, param = function( obj )
				if mapvar.tmp.talk and mapvar.tmp.talk[obj] then
					mapvar.tmp.talk[obj] = false
					return labels.TALK
				end
				if mapvar.tmp.cutscene then
					return 1
				end
				if mapvar.tmp.chartime[obj] > 50 then
					mapvar.tmp.chartime[obj] = 0
					if obj:sprite_mirrored() == true then
						return labels.SLEEPING_LEFT
					else
						return labels.SLEEPING_RIGHT
					end
				else
					mapvar.tmp.chartime[obj] = mapvar.tmp.chartime[obj] + 1
					return 1
				end
			end },
			--{ dur = 0, num = 0, com = constants.AnimComLoop, param = 0, txt = nil },
			{ label = "SLEEPING_RIGHT" },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 18, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 400, num = 108, com = nil, param = 0, txt = nil },
			{ dur = 400, num = 109, com = nil, param = 0, txt = nil },
			{ dur = 400, num = 110, com = nil, param = 0, txt = nil },
			{ dur = 400, num = 109, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComJump, param = "SLEEPING_RIGHT" },
			{ label = "SLEEPING_LEFT" },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 18, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 400, num = 111, com = nil, param = 0, txt = nil },
			{ dur = 400, num = 112, com = nil, param = 0, txt = nil },
			{ dur = 400, num = 113, com = nil, param = 0, txt = nil },
			{ dur = 400, num = 112, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComJump, param = "SLEEPING_LEFT" },
			{ label = "TALK" },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -50, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 114, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 115, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 116, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 117, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 114, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 115, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 116, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 117, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 114, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 115, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 116, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 117, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComLoop },
			{ label = "TRANSMISSION" },
			{ dur = 100, num = 118 },
			{ dur = 100, num = 119 },
			{ dur = 100, num = 120 },
			{ label = "TRANSMISSION_LOOP" },
			{ dur = 100, num = 121 },
			{ dur = 100, num = 122 },
			{ dur = 100, num = 123 },
			{ com = constants.AnimComJump; param = function(this)
				if mapvar.tmp.talk[this] then
					return labels.TRANSMISSION_LOOP
				else
					return labels.TRANSMISSION_END
				end
			end },
			{ label = "TRANSMISSION_END" },
			{ dur = 100, num = 120 },
			{ dur = 100, num = 119 },
			{ dur = 100, num = 118 },
			{ com = constants.AnimComLoop },
			{ label = "FAIL" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 76, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 25 },
			{ dur = 100, num = 30, com = constants.AnimComRealY, param = -2, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -32 },
			{ dur = 100, num = 31, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -7, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 15 },
			{ dur = 100, num = 32, com = constants.AnimComRealH, param = 81, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -4, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 33, com = constants.AnimComRealH, param = 78, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 34, com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 35, com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 36, com = constants.AnimComRealH, param = 60, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 37, com = constants.AnimComRealH, param = 52, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 38, com = constants.AnimComRealH, param = 50, txt = nil },
		}
	},
	{
	name = "walk";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 41 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 12, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 42 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 13, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 41, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 14, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 42, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = -4 },

			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 15, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 43, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 16, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 42, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 17, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 18, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 37, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 41 },
			{ com = constants.AnimComPushInt, param = -3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 19, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootX, param = 36, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 9, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 70, num = 10, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 36, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 70, num = 11, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJump, param = 11, txt = nil },
		}
	},
	{
	name = "jump";
	frames =
		{
			{ dur = 0, num = 45, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 45, com = constants.AnimComRealH, param = 81, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 46, com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 47, com = constants.AnimComRealH, param = 65, txt = nil },
			{ dur = 0, num = 47, com = constants.AnimComJump; param = 19 }
		}
	},
	{
	name = "fly";
	frames =
		{
			{ dur = 0, num = 45, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComPushInt, param = 34, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComPushInt, param = -13, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 45, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -12 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 47, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 47, com = constants.AnimComRealX, param = 6, txt = nil },
			{ dur = 0, num = 47, com = constants.AnimComPushInt, param = 37, txt = nil },
			{ dur = 0, num = 47, com = constants.AnimComPushInt, param = -11, txt = nil },
			{ dur = 0, num = 47, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -10 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 48, com = constants.AnimComRealH, param = 81, txt = nil },
			{ dur = 0, num = 48, com = constants.AnimComPushInt, param = 36, txt = nil },
			{ dur = 0, num = 48, com = constants.AnimComPushInt, param = -6, txt = nil },
			{ dur = 0, num = 48, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 49, com = constants.AnimComRealH, param = 81, txt = nil },
		}
	},
	{
	name = "sit";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 42, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -7 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 24, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "sitaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 24, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComPushInt, param = -40 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 88, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "sitaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 22, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 20 },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 91, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "land";
	frames =
		{
			{ param = function( obj )
				Game.CharacterLanded( obj )
			end },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 57, txt = nil },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComPushInt, param = 4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 43, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 44, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = 1, txt = "gunaimup" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = -1, txt = "gunaimdown" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "pain";
	frames =
		{
			{ com = constants.AnimComJump; param = function(obj) 
				if mapvar.tmp.cutscene then
					ObjectPopInt( obj )
					ObjectPopInt( obj )
					return labels.RECOVER
				end
				Game.ResetCombo() 
				Game.characterHurt( obj, ObjectPopInt( obj ) ) 
				return labels.PAIN
			end },
			{ label = "PAIN" },
			{ dur = 0, num = 0, com = constants.AnimComPlaySound, param = 0, txt = "ouch.ogg" },
			{ dur = 0, num = 0, com = constants.AnimComReduceHealth, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComCreateParticles, param = 2, txt = "pblood_gravity_small" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 30, txt = nil },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 33 },
			{ dur = 100, num = 28, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 14 },
			{ com = constants.AnimComPushInt, param = -19 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 47 },
			{ dur = 100, num = 29, com = constants.AnimComRealH, param = 70, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 30, txt = nil },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 26 },
			{ dur = 100, num = 28, com = constants.AnimComRealH, param = 73, txt = nil },
			{ label = "RECOVER" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "die";
	frames =
		{
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 76, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComJump; param = function( obj )
				if not mapvar.tmp[obj] then
					mapvar.tmp[obj] = {}
				end
				if obj:on_plane() ~= 0 then
					mapvar.tmp[obj].death_frames = 0
					return labels.GROUND_DEATH
				else
					mapvar.tmp[obj].death_frames = 11
					return labels.AIR_DEATH
				end
			end },
			{ label = "AIR_DEATH" },
			{ dur = 100, num = 124 },
			{ com = constants.AnimComJump; param = function( obj )
				if obj:on_plane() ~= 0 then
					return labels.LAND_DEATH
				else
					return labels.AIR_DEATH
				end
			end },
			{ label = "GROUND_DEATH" },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 25 },
			{ dur = 100, num = 30, com = constants.AnimComRealY, param = -2, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -32 },
			{ dur = 100, num = 31, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -7, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 15 },
			{ dur = 100, num = 32, com = constants.AnimComRealH, param = 81, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -4, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 33, com = constants.AnimComRealH, param = 78, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 34, com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 35, com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 36, com = constants.AnimComRealH, param = 60, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 37, com = constants.AnimComRealH, param = 52, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 38, com = constants.AnimComRealH, param = 50, txt = nil },
			{ dur = 100, num = 39, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 40, com = nil, param = 0, txt = nil },
			{ label = "LAND_DEATH" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 50, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 41, com = constants.AnimComRealH, param = 37, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 11 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ label = "DEATH_LOOP" },
			{ dur = 100, num = 42, com = constants.AnimComRealH, param = 26, txt = nil },
			{ com = constants.AnimComJump; param = function(obj) 
				if mapvar.tmp.cutscene then
					obj:health(100)
					return labels.DEATH_RECOVERY
				end
				mapvar.tmp[obj].death_frames = mapvar.tmp[obj].death_frames - 1
				return iff( mapvar.tmp[obj].death_frames < 0, labels.DEATH_END, labels.DEATH_LOOP )
			end },
			{ label = "DEATH_END", num = 42 },
			{ com = constants.AnimComDestroyObject },
			{ label = "DEATH_RECOVERY"; num = 42 },
			{ com = constants.AnimComStopMorphing; num = 42 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
			{}
		}
	},
	{
	name = "aim";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -14 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 21, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "shoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "aim" },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 71, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -14 },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 39 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 22, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 500, num = 21 },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "aim" },
		}
	},
	{
	name = "sitshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 59, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "sit" },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -7 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 25, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComRealX, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 26, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "sit" },
		}
	},
	--[[{
	name = "jumpshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 34, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 0, num = 47, com = constants.AnimComRealH, param = 65, txt = nil },
			{ dur = 1, num = 47, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 1, num = 47, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "fly" },
		}
	},]]
	{
	name = "gunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComPushInt, param = -41 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 97, com = constants.AnimComRealH, param = 86, txt = nil },
		}
	},
	{
	name = "gunliftaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 20, txt = nil },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComPushInt, param = -24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 24 },
			{ dur = 100, num = 50, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimup" },
		}
	},
	{
	name = "gunloweraimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 20, txt = nil },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComPushInt, param = -24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 24 },
			{ dur = 100, num = 50, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "gunaimupshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 83, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -43, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComPushInt, param = -41 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ com = constants.AnimComShoot },
			{ dur = 100, num = 98, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 82, txt = nil },
			{ com = constants.AnimComPushInt, param = -7 },
			{ com = constants.AnimComPushInt, param = -41 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 99, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "gunaimup" },
		}
	},
	{
	name = "situpshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 8, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 59, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 22, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -33, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComPushInt, param = -37 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 89, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 10, txt = nil },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComPushInt, param = -40 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 90, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "sitaimup" },
		}
	},
	{
	name = "sitdownshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 59, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 23, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComPushInt, param = 21 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 92, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "sitaimdown" },
		}
	},
	{
	name = "walkgunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = "RAU_LOOP", txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },
			{ label = "RAU_LOOP" },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 50 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 7 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 47 },
			{ dur = 70, num = 60, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 39 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 8 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 61, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 34 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 9 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 62, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 37 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 10 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 63, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 40 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 9 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 49 },
			{ dur = 70, num = 64, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 46 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 8 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 50 },
			{ dur = 70, num = 65, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 41 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 9 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 66, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 32 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 10 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 67, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 34 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 10 - 35 },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 70, num = 68, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 34 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 8 - 35 },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 + 5 },
			{ com = constants.AnimComPushInt, param = 8 - 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 37 + (-30), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 70, num = 69, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComJump, param = "RAU_LOOP" },
		}
	},
	{
	name = "gunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 106, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "gunaimdownforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 81, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "gunliftaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -23 },
			{ dur = 100, num = 80, com = constants.AnimComRealH, param = 71, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimdown" },
		}
	},
	{
	name = "gunloweraimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -23 },
			{ dur = 100, num = 80, com = constants.AnimComRealH, param = 71, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "gunaimdownshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 6, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ com = constants.AnimComShoot },
			{ dur = 100, num = 107, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "gunaimdown" },
		}
	},
	{
	name = "gunaimdownforwardshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 23, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComPushInt, param = 11 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 82, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimdownforward" },
		}
	},
	{
	name = "walkgunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = "RAD_LOOP", txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },
			{ label = "RAD_LOOP" },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 55 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 62 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -48 },
			{ dur = 70, num = 70, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 48 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0 - 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 63 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 71, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 44 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 64 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 72, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 48 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 65 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -47 },
			{ dur = 70, num = 73, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 54 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 64 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 74, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 55 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 63 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -40 },
			{ dur = 70, num = 75, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 51 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 64 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 76, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 43 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 65 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 77, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 48 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 65 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 78, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 52 + (-45), txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 2 + 27 },
			{ com = constants.AnimComPushInt, param = 63 - 38 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 79, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComJump, param = "RAD_LOOP" },
		}
	},
	{
	name = "jumpgunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = -59 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 100, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunaimupshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = -57 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 0, num = 101, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 1, num = 101, com = constants.AnimComReloadTime, param = 3, txt = nil },
			{ com = constants.AnimComPushInt, param = 6 },
			{ com = constants.AnimComPushInt, param = -56 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 1, num = 102, com = constants.AnimComReloadTime, param = 3, txt = nil },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = -59 },
			{ com = constants.AnimComMPSet, param = 0 }, 
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 1, num = 100, com = constants.AnimComReloadTime, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimup" },
		}
	},
	{
	name = "jumpgunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 8, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 103, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunaimdownshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 8, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -9, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = -9 },
			{ com = constants.AnimComPushInt, param = 20 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 0, num = 104, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 1, num = 104, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 1, num = 103, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimdown" },
		}
	},
	{
	name = "jumpgunaimupforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 10, txt = nil },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComPushInt, param = -54 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 85, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunliftaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = -33 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 20 },
			{ dur = 100, num = 84, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimup" },
		}
	},
	{
	name = "jumpgunaimupforwardshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 21, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 },
			{ com = constants.AnimComPushInt, param = -52 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 49 },
			{ num = 86, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 100, num = 86, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 100, num = 85, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimupforward" },
		}
	},
	{
	name = "jumpgunaimdownforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 56, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunliftaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComPushInt, param = -12 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -15 },
			{ dur = 100, num = 55, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimdown" },
		}
	},
	{
	name = "jumpgunaimdownforwardshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 24, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = 7 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 0, num = 57, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 100, num = 57, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 100, num = 56, com = constants.AnimComReloadTime, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimdownforward" },
		}
	},
	{
	name = "stop";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 75, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 48, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = 1, txt = "gunaimup" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = -1, txt = "gunaimdown" },
			{ com = constants.AnimComRealW, param = 40 },
			{ com = constants.AnimComRealH, param = 75 },
			{ com = constants.AnimComRealX, param = 0 },
			{ com = constants.AnimComRealY, param = -4 },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = 9 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -14 },
			{ dur = 100, num = 7, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "morph_out";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPlaySound, param = 0, txt = "morph.ogg" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 56, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetBulletCollidable, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "lie";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 200, num = 96, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "roll";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 6, txt = nil },
			{ dur = 600, num = 95, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 95, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "morph_in";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 56, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetBulletCollidable, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 1, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComStopMorphing, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
}
