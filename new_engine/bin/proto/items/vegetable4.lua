name = "vegetable4";

trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 0.5;
trajectory_param2 = 0.05;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 0;
phys_max_y_vel = 50;

FunctionName = "CreateItem";

-- �������� �������

--LoadTexture("vegetable3.png");
texture = "vegetable4";
z = -0.001;

image_width = 128;
image_height = 256;
frame_width = 32;
frame_height = 32;
frames_count = 23;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 21 },
			{ com = constants.AnimComRealH; param = 28 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			--{ dur = 100; num = 20; com = constants.AnimComCreateParticles; txt = "pteleport" },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ dur = 50; num = 0; },
			{ dur = 50; num = 1; },
			{ dur = 50; num = 2; },
			{ dur = 50; num = 3; },
			{ dur = 50; num = 4; },
			{ dur = 50; num = 5; },
			{ dur = 50; num = 6; },
			{ dur = 50; num = 7; },
			{ dur = 50; num = 8; },
			{ dur = 50; num = 9; },
			{ dur = 50; num = 10;},
			{ dur = 50; num = 11;},
			{ dur = 50; num = 12;},
			{ dur = 50; num = 13;},
			{ dur = 50; num = 14;},
			{ dur = 50; num = 15;},
			{ dur = 50; num = 16;},
			{ dur = 50; num = 17;},
			{ dur = 50; num = 18;},
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComGiveHealth; param = 40 },
			{ com = constants.AnimComJumpIfPlayerId; param = 3 },
			{ com = constants.AnimComDestroyObject },
			{ dur = 100 },
			{ com = constants.AnimComPlaySound; txt = "health-pickup.ogg" },
			{ param = function( obj )
				Info.RevealItem( "VEGETABLES" )
				local pos = obj:aabb().p
				Game.GainHealth( GetObjectUserdata( ObjectPopInt( obj ) ), 40, pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 21 },
			{ com = constants.AnimComRealH; param = 28 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ dur = 1; com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	}
}