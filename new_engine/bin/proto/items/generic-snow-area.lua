--forbidden
name = "generic-snow-area";

physic = 1;
phys_solid = 0;
phys_ghostlike = 1;
phys_bullet_collidable = 0;
phys_max_x_vel = 0;
phys_max_y_vel = 30;

FunctionName = "CreateSprite";

-- �������� �������
image_width = 1024;
image_height = 1024;
frame_width = 256;
frame_height = 128;
frames_count = 28;


animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ num = 28; dur = 50; com = constants.AnimComCreateParticles; txt = "psnow"; param = 4 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "part";
		frames =
		{
			{ num = 28; dur = 50; com = constants.AnimComCreateParticles; txt = "psnow-part"; param = 4 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "off";
		frames =
		{
			{ dur = 100 },
			{ com = constants.AnimComLoop }
		}
	}
}



