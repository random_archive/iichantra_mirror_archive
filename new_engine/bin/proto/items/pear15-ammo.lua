name = "vegetable1";

trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 0.5;
trajectory_param2 = 0.05;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 0;
phys_max_y_vel = 80;

FunctionName = "CreateItem";

-- �������� �������

texture = "ammo";
z = -0.001;

image_width = 256;
image_height = 128;
frame_width = 32;
frame_height = 32;
frames_count = 19;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 10 },
			{ com = constants.AnimComRealY; param = 9 },
			{ com = constants.AnimComRealW; param = 14 },
			{ com = constants.AnimComRealH; param = 20 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
	
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComLoop }	

		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComGiveAmmo; param = 50 },
			{ com = constants.AnimComJumpIfPlayerId; param = 3 },
			{ com = constants.AnimComDestroyObject },
			{ dur = 100 },
			{ com = constants.AnimComPlaySound; txt = "ammo-pickup.ogg" },
			{ param = function( obj )
				Info.RevealItem( "AMMO" )
				local pos = obj:aabb().p
				Game.ShowAmmoGain( 50, pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject }
		}
	}
	
}



