physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_ghostlike = 1;
phys_max_x_vel = 0;
phys_max_y_vel = 30;

-- �������� �������

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComInitW; param = 1 },
			{ com = constants.AnimComInitH; param = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ num = 28; dur = 100 }
		}
	},
	{ 
		-- �����
		-- Oh, the irony!
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ param = function( obj )
				local fallen = GetObjectUserdata( ObjectPopInt( obj ) )
				if difficulty < 1 and Game.playerMisplaced then
					Game.playerMisplaced( fallen )
					return
				end
				if Game.void then Game.void( fallen ) end
				fallen:health( 0 )
				mapvar.tmp.void = (mapvar.tmp.void or 0) + 1
			end },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
	
}



