--$(DESCRIPTION).���� �������
--$(DESCRIPTION).
--$(DESCRIPTION).�������� �������, ������������ �� ���� ����� ������. ������ �� ����� ����� ��������� �� ����� �����, ������ ��� ���� ��� ����� ���������.

reload_time = 200;
bullets_per_shot = 0;
damage_type = 2;
bullet_damage = 0;

-- �������� ������� ����
texture = "wakabamark";

offscreen_distance = 640
offscreen_behavior = constants.offscreenNone


z = -0.03;

physic = 1;
--phys_ghostlike = 1;
phys_bullet_collidable = 0;
gravity_x = 0;
gravity_y = 0.3;
phys_max_vel_x = 10;
phys_max_vel_y = 10;

bounce = 0.5

overlay = {0};
ocolor = {{1, 0.8, 1, 1}}

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 15 },
			{ com = constants.AnimComRealH; param = 35 },
			{ com = constants.AnimComPushInt; param = 4000 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = -130 },
			{ com = constants.AnimComPushInt; param = -80 },
			{ com = constants.AnimComRandomAngledSpeed; param = 1 },
			{ dur = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "fly"}
		}
	},
	{
		name = "fly";
		frames = 
		{
			{ dur = 50; num = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 4 },
			{ dur = 50; num = 5 },
			{ dur = 50; num = 6 },
			{ dur = 50; num = 7 },
			{ dur = 50; num = 8 },
			{ dur = 50; num = 9 },
			{ dur = 50; num = 10 },
			{ dur = 50; num = 11 },
			{ dur = 50; num = 12 },
			{ dur = 50; num = 13 },
			{ dur = 50; num = 14 },
			{ dur = 50; num = 15 },
			{ dur = 50; num = 16 },
			{ dur = 50; num = 17 },
			{ dur = 50; num = 18 },
			{ dur = 50; num = 19 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{

			{ com = constants.AnimComPlaySound; txt = "item-score.ogg" },
			{ param = function( obj )
				--Player score + 100
				local pos = obj:aabb().p
				Game.GainScore( GetObjectUserdata( ObjectPopInt(obj) ), 100, pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject }

		}
	}
}
