--material = 2; --water

walk_vel_multiplier = 1;
jump_vel_multiplier = 1;

gravity_bonus_x = 0;
gravity_bonus_y = 0;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_ghostlike = 1;
phys_bullet_collidable = 0;

script_on_enter = function( ud, x, y, material, this )
end

script_on_stay = function( ud, x, y, this )
	if ud:type() == 1 then
	if not mapvar then mapvar = {} end
	if not mapvar.tmp then mapvar.tmp = {} end
	if not mapvar.tmp.spikes then mapvar.tmp.spikes = {} end
	if not mapvar.tmp.spikes.n then mapvar.tmp.spikes.n = 0 end
	local n = mapvar.tmp.spikes.n
	for i = 1,n do
		if mapvar.tmp.spikes.x[i] >= this:aabb().p.x - this:aabb().W
		and mapvar.tmp.spikes.x[i] <= this:aabb().p.x + this:aabb().W
		and mapvar.tmp.spikes.y[i] >= this:aabb().p.y - this:aabb().H
		and mapvar.tmp.spikes.y[i] <= this:aabb().p.y + this:aabb().H then
			DamageObject( ud, 20 )
			SetObjAnim(mapvar.tmp.spikes.obj[i]:id(),"spike",false)
		end
	end
	end
end

sprites =
{
}

sounds =
{
}
