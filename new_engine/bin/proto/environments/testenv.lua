name = "testenv";
material = 1;

walk_vel_multiplier = 0.5;
jump_vel_multiplier = 1;
bounce_bonus = 0.5;

gravity_bonus_x = 0.5;
gravity_bonus_y = -0.5;

script_on_enter = "testenv";
script_on_stay = "testenvstay";
script_on_leave = "testenv";

sprites =
{
	"dust-run2",
	"dust-land",
	"dust-stop"
}

sounds =
{
	"ammo-pickup",
	"boss-hit",
	"flame"
}