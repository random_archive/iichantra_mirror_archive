flash = "flash-straight-sfg5000";

reload_time = 200;
bullets_per_shot = 5;
clip_reload_time = 0;

-- �������� ����
bullet_damage = 25;
bullet_vel = 4;
damage_type = Game.damage_types.unyl_alt;
--bullet_vel = 1;

facing = constants.facingFixed;

-- �������� ������� ����
texture = "bullets-sfg5000";

push_force = 1.0;

z = -0.002;

-- ������ ���������, ����� ���� �����
local sound_shoot = "weapons/blaster_shot.ogg"

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = sound_shoot },
			{ com = constants.AnimComSetLifetime; param = 3000 },
			{ com = constants.AnimComRealX; param = 38 },
			{ com = constants.AnimComRealY; param = 7 },
			{ com = constants.AnimComInitWH; param = 14 },
			{ param = function( obj )
				local pos = obj:aabb().p
				mapvar.tmp[obj] = GetNearestEnemy( obj:shooter() )
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { .8, .2, .6, .4 } )
				SetObjRectangle( flare, 64, 32 )
				SetObjSpriteAngle( flare, obj:sprite_angle() )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjPos( flare, pos.x, pos.y )
				SetObjProcessor( flare, function( object )
					if obj and obj:object_present() and obj:sprite_cur_anim() ~= "die" then
						local p = obj:aabb().p
						SetObjPos( object, p.x, p.y )
					else
						mapvar.tmp[object:id()] = ( mapvar.tmp[object:id()] or 64 ) * 0.8;
						if ( mapvar.tmp[object:id()] < 2 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, mapvar.tmp[object:id()], mapvar.tmp[object:id()] / 2 )
					end 
				end )
				obj:max_x_vel( bullet_vel )
				obj:max_y_vel( bullet_vel )
			end },
			{ com = constants.AnimComSetAnim; txt = "chase" },
		}
	},
	{
		name = "chase";
		frames = 
		{
			{ param = function( obj )
				local target = mapvar.tmp[obj]
				if not ( target and target:object_present() and target:health() > 0 and ObjectOnScreen( target ) ) then
					return
				end
				target = target:aabb().p
				local pos = obj:aabb().p
				local vel = obj:own_vel()
				local angle1 = math.atan2( target.y - pos.y, target.x - pos.x )
				vel = { x = vel.x + 2 * math.cos(angle1), y = vel.y + 2 * math.sin(angle1) }
				obj:own_vel( vel )
				if obj:sprite_mirrored()==false then
					SetObjSpriteAngle( obj, math.atan2( vel.y, vel.x ) )
				else
					SetObjSpriteAngle( obj, math.pi + math.atan2( vel.y, vel.x ) )
				end
			end },
			{ dur = 100 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealX; param = 25 },
			{ com = constants.AnimComRealY; param = 6 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
