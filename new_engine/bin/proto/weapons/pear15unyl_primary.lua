flash = "flash-straight-sfg5000";

reload_time = 200;
bullets_per_shot = 0;
clip_reload_time = 0;

-- �������� ����
bullet_damage = 17;
bullet_vel = 5;
damage_type = Game.damage_types.unyl_primary;
--bullet_vel = 1;

facing = constants.facingFixed;

-- �������� ������� ����
texture = "bullets-sfg5000";

push_force = 1.0;

z = -0.002;

-- ������ ���������, ����� ���� �����
local sound_shoot = "weapons/blaster_shot.ogg"

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = sound_shoot },
			{ com = constants.AnimComSetLifetime; param = 3000 },
			{ com = constants.AnimComRealX; param = 38 },
			{ com = constants.AnimComRealY; param = 7 },
			{ com = constants.AnimComInitWH; param = 14 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { .8, .2, .6, .4 } )
				SetObjRectangle( flare, 64, 32 )
				SetObjSpriteAngle( flare, obj:sprite_angle() )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjPos( flare, pos.x, pos.y )
				SetObjProcessor( flare, function( object )
					if obj and obj:object_present() and obj:sprite_cur_anim() ~= "die" then
						local p = obj:aabb().p
						SetObjPos( object, p.x, p.y )
					else
						mapvar.tmp[object:id()] = ( mapvar.tmp[object:id()] or 64 ) * 0.8;
						if ( mapvar.tmp[object:id()] < 2 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, mapvar.tmp[object:id()], mapvar.tmp[object:id()] / 2 )
					end 
				end )
			end },
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealX; param = 25 },
			{ com = constants.AnimComRealY; param = 6 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
