reload_time = 700;
bullets_per_shot = 20;
clip_reload_time = 150;
multiple_targets = -1;

time_to_live = 0;

bullet_damage = 0;
bullet_vel = 0;
ghost_to = 255;

--texture = "superlaser";
flash = "flash-straight";
charge_effect = "flash-charge";

z = 0.6;
local t = 100;

next_shift_y = 4; --wtf is that I don't even.

local sound_shoot = "weapons/laser_alt.ogg"

local function get_smaller( obj )
	mapvar.tmp[obj] = mapvar.tmp[obj] * 0.8
	return mapvar.tmp[obj]
end

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			--{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComInitH; param = 32 },
			{ com = constants.AnimComRealX; param = -24 },
			{ dur = 1 },
			{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ param = function(obj)
				local pos = obj:aabb().p
				local pos2 = obj:aabb().p
				local angle = obj:sprite_angle()
				local n = { x = -math.sin(angle), y = math.cos(angle) }
				local l = math.sqrt( math.pow( n.x, 2 ) + math.pow( n.y, 2 ) )
				n.x = n.x / l
				n.y = n.y / l
				if obj:sprite_mirrored() then
					angle = math.pi - angle
				else
					angle = -angle
				end
				local shooter = obj:shooter()
				CreateRay( "pear15ray_alternative_control", shooter:id(), angle, shooter:aabb().p.x, pos.y )
				--pos.x = pos.x + 32*math.cos(angle)
				--pos.y = pos.y - 32*math.sin(angle)
				pos2.x = pos2.x + 8*math.cos(angle)
				pos2.y = pos2.y - 8*math.sin(angle)
				angle = (angle * 180) / math.pi
				local charge = math.min( 1, obj:charge()/1000 )
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { .4, .4, .8, 1 } )
				SetObjRectangle( flare, 10+70*charge, 10+70*charge )
				SetObjPos( flare, pos.x, pos.y )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjInvisible( flare, true )
				SetObjProcessor( flare, function( object )
						if not mapvar.tmp[object:id()] then mapvar.tmp[object:id()] = Loader.time end
						local r = 10+70*charge/500 + 10 * (Loader.time - mapvar.tmp[object:id()])/1000
						--SetObjSpriteColor( object, { .4, .4, .8, 1-(Loader.time - mapvar.tmp[object:id()])/500 } )
						if r > 20+70*charge then
							r = 20+70*charge
						end
						if Loader.time - mapvar.tmp[object:id()] > 500 then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, r, r )
				end )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				SetObjRectangle( obj, 32, 16 + 32 * charge )
				mapvar.tmp[obj] = 16 + 32 * charge
				local bul = CreateRay( "pear15ray_alternative_ray", shooter:id(), angle, pos.x, pos.y )
				mapvar.tmp[bul] = 16 + 32 * charge
				bul = GetObjectUserdata( bul )
				bul:player_num(obj:player_num())
				for i = 0, (16 * charge) - 5, 5 do
					bul = GetObjectUserdata( CreateRay( "pear15ray_alternative_invisible_ray", shooter:id(), angle, pos2.x + (16-i)*n.x, pos2.y + (16-i)*n.y ) )
					bul:player_num(obj:player_num())
					bul = GetObjectUserdata( CreateRay( "pear15ray_alternative_invisible_ray", shooter:id(), angle, pos2.x - (16-i)*n.x, pos2.y - (16-i)*n.y ) )
					bul:player_num(obj:player_num())
				end
			end },
			{ dur = t; num = 0; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 1; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 0; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 1; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 0; com = constants.AnimComInitH; param = get_smaller },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "miss";
		frames =
		{
			{}
		}
	},
	{
		name = "die";
		frames = 
		{
			{}
		}
	}
}
