flash = "flash-straight";

reload_time = 430;
bullets_per_shot = 0;
damage_type = 2;

-- �������� ����
bullet_damage = 0;
bullet_vel = 0;
ghost_to = 255;

-- �������� ������� ����

recoil = 0.1;

z = -0.002;

-- ������ ���������, ����� ���� �����
local sound_shoot = "weapons/blaster_shot.ogg"

fire_script = function( char, x, y )
	if not mapvar.tmp[char] then
		mapvar.tmp[char] = {}
	end
	if not mapvar.tmp[char].spray_charge then
		mapvar.tmp[char].spray_charge = 7
		mapvar.tmp[char].spray_time = Loader.time
	end
	local charge = mapvar.tmp[char].spray_charge
	charge = math.min( 7, charge + 7 * ( Loader.time - mapvar.tmp[char].spray_time ) / 5000 )
	mapvar.tmp[char].spray_time = Loader.time
	charge = math.max( 0, charge - 1 )
	mapvar.tmp[char].spray_charge = charge
	if mapvar.tmp[char].spray_charge > 0 then
		return { reload_time = 165 }
	else
		return { reload_time = 430 }
	end
end

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ dur = 1; num = 0; com = constants.AnimComInitH; param = 1 },
			{ com = constants.AnimComDestroyObject; param = function( obj )
				local shooter = GetParent( obj )
				local angle = shooter:weapon_angle() * (180/math.pi)
				local mirror = shooter:sprite_mirrored()
				local x, y = GetMP( shooter, 0 )
				local p = shooter:aabb().p
				if mirror then
					x = p.x - x
					y = p.y + y
				else
					x = p.x + x
					y = p.y + y
				end
				
				local bul
				for i=1,4 do
					local bul = GetObjectUserdata( CreateBullet( "pear15spray_bullet", x, y, shooter:id(), mirror, angle + math.random( -5, 5 ), 0 ) )
					bul:player_num(obj:player_num())
					SetObjSpriteMirrored( bul, mirror )
				end

				return 0
			end }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComDestroyObject }
		}
	}
}
