--forbidden
name = "grenade";

physic = 1;

reload_time = 300;
--bullets_per_shot = 10;
flash = "flash-straight";
charge_effect = "flash-charge";

push_force = 1.0;

-- �������� ����
bullet_damage = 0;
bullet_vel = 12;

-- �������� ������� ����
texture = "grenade";

z = -0.002;

bounce = 0.3;

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetAccY; param = 0 },
			{ dur = 30; num = 0 },
			{ dur = 30; num = 1 },
			{ dur = 30; num = 2 },
			{ dur = 30; num = 3 },
			{ dur = 30; num = 4 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 16 },
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealX; param = 8 },
			{ com = constants.AnimComRealY; param = 8 },
			{ com = constants.AnimComSetProcessor; param = RegisterFunction( function( obj )
					local id = obj:id()
					if not mapvar.tmp[id] then
						mapvar.tmp[id] = { lifetime = 1000, last_update = Loader.time, dead = false }
						obj:acc( {x=0, y=0} )
						obj:gravity( {x=0, y=0.8} )
						obj:max_x_vel( 1000 )
						obj:max_y_vel( 1000 )
						local vel = obj:own_vel()
						local angle
						if vel.x == 0 then
							if vel.y > 0 then
								angle = 1.5707963267949
							else
								angle = -1.5707963267949
							end
						else
							angle = math.atan2( vel.y, vel.x )
						end
						local charge = obj:charge()
						if charge > 1000 then
							charge = 1000
						end
						if obj:sprite_mirrored() then
							angle = angle + 0.39269908169872 * (charge/1000)
						else
							angle = angle - 0.39269908169872 * (charge/1000)
						end
						vel.x = math.cos(angle) * ( 2 + 10 * (charge/1000) )
						vel.y = math.sin(angle) * ( 2 + 10 * (charge/1000) )
						obj:vel( vel )
					end
					obj:own_vel( {x=0, y=0} )
					mapvar.tmp[id].lifetime = mapvar.tmp[id].lifetime - (Loader.time - mapvar.tmp[id].last_update)
					mapvar.tmp[id].last_update = Loader.time
					if ( not mapvar.tmp[id].dead ) and mapvar.tmp[id].lifetime < 0 then
						SetObjAnim( obj, "die", false )
					end
			end ) },
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-launch.ogg"; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "miss";
		frames =
		{
			--{ com = constants.AnimComPlaySound; txt = "weapons/grenade-bounce.ogg" },
			{param = function() 
				local vel = obj:vel()
				vel = math.sqrt( math.pow( vel.x, 2 ) + math.pow( vel.y, 2 ) )
				PlaySnd( "weapons/grenade-bounce.ogg", false, x, y, false, vel / max_vel )
			end},
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-grenade" },
			{ com = constants.AnimComDestroyObject }
		}
	}
}

local vel = obj:vel()
vel = math.sqrt( math.pow( vel.x, 2 ) + math.pow( vel.y, 2 ) )
