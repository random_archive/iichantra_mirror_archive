texture = "circle";

z = 0.6;

color = { 199/255, 46/255, 26/255, 0.4 }

animations =
{
	{
		name = "idle",
		frames =
		{
			{ com = constants.AnimComInitWH; param = 64 },
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				SetObjSpriteBlendingMode( obj, constants.bmSrcA_One )
			end },
			{ dur = 100 }
		}
	},
	{
		name = "flicker",
		frames =
		{
			{ param = function( obj )
				if mapvar.tmp[obj] then
					return
				end
				mapvar.tmp[obj] = true
				Resume( NewMapThread( function()
					local last_time = Loader.time or 0
					local t = 0
					local dt
					while t < 300 * math.pi do
						dt = (Loader.time or 0) - last_time
						last_time = (Loader.time or 0)
						t = t + dt
						if t > 200 * math.pi then
							mapvar.tmp.flicker = false
						elseif t > 100 * math.pi then
							mapvar.tmp.flicker_phase2 = false
						end
						SetObjSpriteColor( obj, { color[1], color[2], color[3], 0.4 * math.cos( t / 50 ) } )
						Wait(1)
					end
					SetObjSpriteColor( obj, { color[1], color[2], color[3], 0.4 } )
					mapvar.tmp[obj] = false
				end ))
			end },
			{ hur = 100, dur = 100 }
		}
	},
}
