texture = "editor_misc";

z = 0.8;

animations =
{
	{
		name = "arrow",
		frames =
		{
			{ com = constants.AnimComRealW; param = 32; num = 1 },
			{ com = constants.AnimComRealH; param = 32; num = 1 }
		}
	},
	{
		name = "cross",
		frames =
		{
			{ com = constants.AnimComRealW; param = 32; num = 5 },
			{ com = constants.AnimComRealH; param = 32; num = 5 }
		}
	}
}
