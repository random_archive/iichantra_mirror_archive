texture = "waterfall2";
FunctionName = "CreateSprite";

z = -0.89999;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ dur = 100, num = 2 },
			{ dur = 100, num = 0 },
			{ dur = 100, num = 1 },
			{ com = constants.AnimComLoop }
		},
	}
}


