texture = "waterfall1";
FunctionName = "CreateSprite";

z = -0.9;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComRealH; param = 48 },
			{ com = constants.AnimComRealW; param = 10 },
			{ dur = 100, num = 4 },
			{ dur = 100, num = 5 },
			{ dur = 100, num = 3 },
			{ com = constants.AnimComJump; param = 0 }
		},
	}
}


