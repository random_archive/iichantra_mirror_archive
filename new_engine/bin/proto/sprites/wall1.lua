name = "wall1";
texture = "wall1";
FunctionName = "CreateSprite";
--47

z = -0.03;

--physic = 1;
--phys_solid = 1;
--phys_one_sided = 0;
--phys_bullet_collidable = 1;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 256 },
			{ com = constants.AnimComRealW; param = 66 },
			{ com = constants.AnimComPushInt; param = 0 },
		}
	},
	{
		name = "blasted";
		frames = 
		{	
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -15 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = 60 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComPushInt; param = 80 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },			
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -30 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ num = 1 }
		}
	}
}