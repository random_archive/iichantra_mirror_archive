name = "lab-floor2-random";
texture = "lab-floor2";
FunctionName = "CreateSprite";

z = -0.8;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 36 },
			{ com = constants.AnimComRealW; param = 58 },
			{ com = constants.AnimComPushInt; param = 230 },
			{ com = constants.AnimComJumpRandom; param = 10 },
			{ com = constants.AnimComPushInt; param = 230 },
			{ com = constants.AnimComJumpRandom; param = 11 },
			{ com = constants.AnimComPushInt; param = 230 },
			{ com = constants.AnimComJumpRandom; param = 12 },
			{ com = constants.AnimComPushInt; param = 230 },
			{ com = constants.AnimComJumpRandom; param = 13 },
			{ com = constants.AnimComJump; param = 14 },
			{ com = constants.AnimComSetAnim; txt = "4"},
			{ com = constants.AnimComSetAnim; txt = "1"},
			{ com = constants.AnimComSetAnim; txt = "2"},
			{ com = constants.AnimComSetAnim; txt = "3"},
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "4";
		frames =
		{
			{ dur = 100; num = 4 }
		}
	},
	{
		name = "1";
		frames =
		{
			{ dur = 100; num = 1 }
		}
	},
	{
		name = "2";
		frames =
		{
			{ dur = 100; num = 2 }
		}
	},
	{
		name = "3";
		frames =
		{
			{ dur = 100; num = 3 }
		}
	}
}
