--forbidden
name = "btard-corpse";
texture = "btard";
FunctionName = "CreateSprite";

z = 0.25;
image_width = 1024;
image_height = 1024;
frame_width = 256;
frame_height = 128;
frames_count = 28;

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 125 },
			{ com = constants.AnimComRealY; param = 128 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 0 },
			{ dur = 1000; num = 27 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
