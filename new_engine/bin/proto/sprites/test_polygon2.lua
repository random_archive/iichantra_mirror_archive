texture = "test_polygon";

z = 0;

polygons = {
	{{-64, 0}, {0, -64}, {64, 0},  {64, 32}, {-32, 64}}
}

physic = 1;
phys_solid = 1;
phys_bullet_collidable = 1;
phys_one_sided = 1;


animations =
{
	{
		name = "idle";
		frames =
		{
			{ num = 0; poly = 1 }
		}
	}
}
