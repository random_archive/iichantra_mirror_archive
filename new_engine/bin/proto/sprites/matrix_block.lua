--forbidden
name = "matrix_block";
texture = "matrix_block";
FunctionName = "CreateSprite";

image_width = 128;
image_height = 64;
frame_width = 32;
frame_height = 32;
--21
frames_count = 5;

z = -0.1;

physic = 1;
phys_solid = 1;
phys_bullet_collidable = 1;

animations = 
{
	{ 
		name = "idle";
		frames_count = 5;
		start_frame = 0;
		anim_speed = 100;
		real_x = 0;
		real_y = 0;
		real_mirror_x = 0;
		real_mirror_y = 0;
		real_width = 32;
		real_height = 21;
	}
}
