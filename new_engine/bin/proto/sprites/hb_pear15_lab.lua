texture = "hb_pear15_lab";

z = -0.8999;

animations =
{
	{
		name = "floor";
		frames =
		{
			{ com = constants.AnimComRealW; param = 128 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 0 }
		}
	},
	{
		name = "pit_left";
		frames =
		{
			{ com = constants.AnimComRealW; param = 7*32 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 1 }
		}
	},
	{
		name = "pit_right";
		frames =
		{
			{ com = constants.AnimComRealW; param = 6*32 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 2 }
		}
	},
}
