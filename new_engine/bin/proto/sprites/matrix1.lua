--forbidden
name = "matrix1";
texture = "matrix1";
FunctionName = "CreateSprite";

z = -0.25;
image_width = 256;
image_height = 512;
frame_width = 16;
frame_height = 144;
frames_count = 6;

animations = 
{
	{ 
		name = "idle";
		frames_count = 6;
		start_frame = 0;
		anim_speed = 200;
		real_x = 0;
		real_y = 0;
		real_mirror_x = 0;
		real_mirror_y = 0;
		real_width = 0;
		real_height = 0;
	}
}
