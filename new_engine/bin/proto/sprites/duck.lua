--Пригнись.

texture = "duck";

z = -0.1;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 24 },
			{ com = constants.AnimComRealH; param = 21; dur = 200 },
			{ com = constants.AnimComRealH; param = 22; dur = 150 },
			{ com = constants.AnimComRealH; param = 24; dur = 200 },
			{ com = constants.AnimComRealH; param = 22; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	}
}
