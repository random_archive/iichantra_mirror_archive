texture = "2011lab4";

z = -0.1;

material_name = "jumppad";

physic = 1;
phys_solid = 1;
phys_one_sided = 1;

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 33 },
			{ com = constants.AnimComInitH; param = 23 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = 13 },
			{ num = 21 }
		}
	},
}



