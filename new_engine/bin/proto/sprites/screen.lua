texture = "screen"
z = -0.5

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 50 },
			{ dur = 1, num = 5 }
		}
	},
	{
		name = "tile1";
		frames =
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 50 },
			{ num = 1; dur = 500 },
			{ num = 5; dur = 500 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile2";
		frames =
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 50 },
			{ num = 2; dur = 500 },
			{ num = 5; dur = 500 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile3";
		frames =
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 50 },
			{ num = 3; dur = 500 },
			{ num = 5; dur = 500 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile4";
		frames =
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 50 },
			{ num = 4; dur = 500 },
			{ num = 5; dur = 500 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "warning";
		frames =
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 50 },	
			{ num = 0; dur = 200 },
			{ num = 5; dur = 200 },
			{ num = 0; dur = 200 },
			{ num = 5; dur = 200 },
			{ num = 0; dur = 200 },
			{ num = 5; dur = 200 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	}
}
