texture = "hw_tiles";
z = -0.9;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 78 },
			{ com = constants.AnimComRealW; param = 32 },
			{ num = 41; dur = 75; },
			{ num = 42; dur = 75; },
			{ num = 43; dur = 75; },
			{ num = 44; dur = 75; },
			{ com = constants.AnimComJump; param = 3 }
		}
	}
}