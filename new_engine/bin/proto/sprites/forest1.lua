name = "forest1";
texture = "forest1";
FunctionName = "CreateSprite";

z = -0.8;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 47 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComJumpRandom; param = 6 },
			{ com = constants.AnimComPushInt; param = 5 },
			{ com = constants.AnimComJumpRandom; param = 7 },
			{ com = constants.AnimComSetAnim; txt = "2"},
			{ com = constants.AnimComSetAnim; txt = "1"},
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "1";
		frames =
		{
			{ dur = 100; num = 1 }
		}
	},
	{
		name = "2";
		frames =
		{
			{ dur = 100; num = 2 }
		}
	}
}
