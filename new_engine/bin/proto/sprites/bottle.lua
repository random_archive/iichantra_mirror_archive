texture = "penguin_omsk";

z = -0.6;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 9; num = 19 },
			{ com = constants.AnimComRealH; param = 19; num = 19 },
			{ dur = 100; num = 19 }
		}
	}
}
