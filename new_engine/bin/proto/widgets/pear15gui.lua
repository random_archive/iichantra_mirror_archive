texture = "pear15gui";

z = 0.9;

animations =
{
	{
		name = "bars_back";
		frames =
		{
			{ num = 0 }
		}
	},
	{
		name = "bar1";
		frames =
		{
			{ num = 1 }
		}
	},
	{
		name = "bar2";
		frames =
		{
			{ num = 2 }
		}
	},
	{
		name = "combo";
		frames =
		{
			{ num = 3 },
		}
	},
	{
		name = "x";
		frames =
		{
			{ num = 4 },
		}
	},
	{
		name = "1";
		frames =
		{
			{ num = 5 },
		}
	},
	{
		name = "2";
		frames =
		{
			{ num = 6 },
		}
	},
	{
		name = "3";
		frames =
		{
			{ num = 7 },
		}
	},
	{
		name = "4";
		frames =
		{
			{ num = 8 },
		}
	},
	{
		name = "5";
		frames =
		{
			{ num = 9 },
		}
	},
	{
		name = "6";
		frames =
		{
			{ num = 10 },
		}
	},
	{
		name = "7";
		frames =
		{
			{ num = 11 },
		}
	},
	{
		name = "8";
		frames =
		{
			{ num = 12 },
		}
	},
	{
		name = "9";
		frames =
		{
			{ num = 13 },
		}
	},
	{
		name = "0";
		frames =
		{
			{ num = 14 },
		}
	},
	{
		name = "+";
		frames =
		{
			{ num = 15 },
		}
	},
	{
		name = "weapon1";
		frames =
		{
			{ num = 16 }
		}
	},
	{
		name = "weapon2";
		frames =
		{
			{ num = 17 }
		}
	},
	{
		name = "weapon3";
		frames =
		{
			{ num = 18 }
		}
	},
	{
		name = "weapon4";
		frames =
		{
			{ num = 19 }
		}
	},
	{
		name = "weapon_icon_none";
		frames =
		{
			{ num = 29 }
		}
	},
	{
		name = "weapon_icon_soh";
		frames =
		{
			{ num = 20 }
		}
	},
	{
		name = "weapon_icon_2";
		frames =
		{
			{ num = 22 }
		}
	},
	{
		name = "weapon_icon_3";
		frames =
		{
			{ num = 21 }
		}
	},
	{
		name = "weapon_icon_4";
		frames =
		{
			{ num = 23 }
		}
	},
	{
		name = "weapon_icon_unyl";
		frames =
		{
			{ num = 24 }
		}
	},
	{
		name = "heart";
		frames =
		{
			{ num = 25 }
		}
	},
	{
		name = "weapon_charge_background";
		frames =
		{
			{ num = 26 }
		}
	},
	{
		name = "weapon_charge_bar";
		frames =
		{
			{ num = 27 }
		}
	},
	{
		name = "info";
		frames =
		{
			{ num = 28 }
		}
	}
}
