--forbidden
name = "pspark";
texture = "pspark";
--FunctionName = "CreateSprite";

z = 0.5;

start_color = { 1.0, 1.0, 1.0, 1.0 };
var_color = { 0.2, 0.0, 0.0, 0.0 };
end_color = { 1, 0.8, 0.8, 1.0 };

max_particles = 50;
particle_life_min = 20;
particle_life_max = 50;

start_size = 2.0;
size_variability = 3.0;
end_size = 1.0;

particle_life = 3;
particle_life_var = 2;

system_life = 5;
emission = 20;
particle_min_speed = 12;
particle_max_speed = 16;
particle_min_angle = 0;
particle_max_angle = 360;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 0;
particle_max_trace = 16;
trajectory_type = constants.pttLine;
trajectory_param1 = 90;
trajectory_param2 = 8;
affected_by_wind = 1;
gravity_x = 0;
gravity_y = 0.8;

physic = -1;