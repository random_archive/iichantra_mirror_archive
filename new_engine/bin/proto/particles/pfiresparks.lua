texture = "pspark";
--FunctionName = "CreateSprite";

z = 0.05;


start_color = { 1.0, 1.0, 1.0, 1.0 };
var_color = { 0.2, 0.0, 0.0, 0.0 };
end_color = { 1, 0.8, 0.8, 0.0 };

max_particles = 40;

start_size = 4.0;
size_variability = 3.0;
end_size = 3.5;

particle_life = 20;
particle_life_var = 2;

system_life = -1;
emission = 1.5;

particle_min_speed = 8;
particle_max_speed = 15;
particle_min_angle = -180;
particle_max_angle = 0;
particle_min_param = 0;
particle_max_param = 2*3.1415;
particle_min_trace = 0;
particle_max_trace = 0;
trajectory_type = constants.pttSine;
trajectory_param1 = 10;
trajectory_param2 = 0.5;
affected_by_wind = 0;
gravity_x = 0;
gravity_y = 0;