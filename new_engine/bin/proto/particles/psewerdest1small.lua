texture = "psewerdest";

z = -0.1;

start_color = { 1, 1, 1, 1.0 };
var_color = { 0, 0, 0, 0.0 };
end_color = { 1, 1, 1, 1 };

max_particles = 100;
particle_life_min = 5;
particle_life_max = 20;

start_size = 16.0;
size_variability = 4.0;
end_size = 16.0;

particle_life = 15;
particle_life_var = 2;

system_life = 5;
emission = 1;
particle_min_speed = 8;
particle_max_speed = 12;
particle_min_angle = 0;
particle_max_angle = 360;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 0;
particle_max_trace = 0;
trajectory_type = constants.pttLine;
trajectory_param1 = 90;
trajectory_param2 = 8;
affected_by_wind = 1;
gravity_x = 0;
gravity_y = 0.7;

physic = 1;
bounce = 0.5;

particle_anim = constants.paRandomFrame;
particle_anim_param = 0 * 256 + 19;
max_bounce = 3;
