--forbidden
texture = "pspark";

z = 0.9;

start_color = { 1.0, 1.0, 1.0, 1.0 };
var_color = { 0.2, 0.0, 0.0, 0.0 };
end_color = { 1.0, 1.0, 0.3, 1.0 };

max_particles = 128;
particle_life_min = 10;
particle_life_max = 30;

start_size = 4.0;
size_variability = 3.0;
end_size = 2.0;

particle_life = 15;
particle_life_var = 2;

system_life = -1;

emission = 30;

particle_min_speed = 1;
particle_max_speed = 3;
particle_min_angle = -90;
particle_max_angle = -90;
particle_min_param = 0;
particle_max_param = 3;
trajectory_type = constants.pttSine;
trajectory_param1 = 10;
trajectory_param2 = 0.5;
affected_by_wind = 0;