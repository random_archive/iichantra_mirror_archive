require( "pear15-achievements" )
require( "pear15-info" )
require( "pear15-conversation" )
require( "highscores" )

Game = { ["_game_name"] = "pear15", ["_version"] = "1.2" }
UpdateWindowCaption( string.format( "P.E.A.R. v.%s", Game._version ) );
Game.info = {}
Game.damage_types =
{
	normal = 0,
	substances = 30,
	ghost = 31,
	fake_ghost = 32,
	fire = 33,
	deadly_fire = 34,
	player_weapon_start = 35,
	soh_primary = 36,
	soh_alt = 37,
	unyl_primary = 38,
	unyl_alt = 39,
	laser = 40,
	super_laser = 41,
	spray_primary = 42,
	spray_alt = 43,
	force_primary = 44,
	force_alt = 45,
	player_weapon_end = 46,
	explosion = 47,
}
Game.variables =
{
	score_start = 0,
	time = 10
}

Game.GUI = require("pear15gui").create()

_NetConnect = NetConnect
_NetHost = NetHost

local cam_off_x = 0
local cam_off_y = 0

local _SetCamObjOffset = SetCamObjOffset
SetCamObjOffset = function( x, y )
	cam_off_x = -x
	cam_off_y = -y
	_SetCamObjOffset( x, y )
end

NetConnect = function( host, port )
	NETGAME = true
	_NetConnect( host, port )
end

NetHost = function( port )
	NETGAME = true
	_NetHost( port )
end

function Game.Heartbeat()
	if Game.mapvar.game_timer_enabled and GetPlayerControlState() then 
		Game.mapvar.game_timer = Game.mapvar.game_timer + Loader.dt
	end
	if Game.last_combo_gain and Loader.time - Game.last_combo_gain > 2000 then
		Game.ResetCombo()
	end
end

function StabilizeSync()
end

function Game.CharacterSystem( old_id, new_id )
end

-- Called on level (re)start 
function Game.InitGameSession()
	SetOnPlayerDeathProcessor(Game.onPlayerDeath)
	SetOnChangePlayerProcessor(Game.CharacterSystem)
	Game.mapvar.game_timer_enabled = true
end

local cam_dont_follow = {}

function varchange( varname, change )
	-- if varname == "score" then
		-- if not mapvar.next_life then mapvar.next_life = 5000 end
		-- if mapvar.score >= mapvar.next_life then
			-- mapvar.lives = mapvar.lives + 1
			-- mapvar.next_life = mapvar.next_life * 2
		-- end
		-- mapvar.score = math.min( mapvar.score, 10000-(mapvar.score_part_two or 10000)+change, ((mapvar.score_part_three or 0)/(-2))+change )
		-- mapvar.score_part_two = 10000 - mapvar.score
		-- mapvar.score_part_three = -2*mapvar.score
	-- end
end

function BonusCombo( first, second, what1, what2 )
	return ( (what1 == first) and (what2 == second) ) or ( (what1 == second) and (what2 == first) )
end

function RemoveDoubleBonus( player_name, bonus_type, char1_id, char2_id )
	Wait(10000)
	if bonus_type == "bonus_speed" then
		-- Тут все равно обоим чарам меняется скорость, так что не нужны заморочки с определением активного чара
		--local char1 = GetPlayer(1)
		--local char2 = GetPlayer(2)
		--if (char1 and char1.id == char1_id) then AddBonusSpeed(char1.id, -3) end
		--if (char2 and char2.id == char2_id) then AddBonusSpeed(char2.id, -3) end
	elseif bonus_type == "bonus_invul" then
		--local num = mapvar.char[player_name]
		--if num == 1 or num == 2 then 
		--	SetObjInvincible(GetPlayer(num).id, false)
		--end
	end
	Game.mapvar.weapon[GetDefaultActor()] = nil
end

function WeaponSystem( id, weapon )
	if Loader and Loader.level and Loader.level.WeaponBonus then
		Loader.level.WeaponBonus( id, weapon )
	end
	
	-- определение active_char
	local active_num = GetDefaultActor();
	local pl = GetPlayer();
	
	if not mapvar.bonuses then mapvar.bonuses = {} end
	

	if not mapvar.bonuses[active_num] then mapvar.bonuses[active_num] = {} end

	local bt = mapvar.bonuses[active_num]
	if #bt == 2 then
		table.remove(bt, 1)
	end
	if not mapvar.weapon then mapvar.weapon = {} end
	table.insert( bt, weapon )
	if active_num and #bt == 2 then
		--circle
		if BonusCombo("circle", "triangle", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "flamer"
			Game.mapvar.weapon[0] = "flamer"
			SetPlayerAltWeapon( nil, "flamer" )
			SwitchWeapon( nil, false )
			return "flamer"
		end
		if BonusCombo("circle", "square", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "grenade"
			Game.mapvar.weapon[0] = "grenade"
			SetPlayerAltWeapon( nil, "grenade" )
			SwitchWeapon( nil, false )
			return "grenade"
		end
		if BonusCombo("circle", "diamond", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "overcharged"
			Game.mapvar.weapon[0] = "overcharged"
			SetPlayerAltWeapon( nil, "overcharged" )
			SwitchWeapon( nil, false )
			return "overcharged"
		end
		if BonusCombo("circle", "line", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "bouncy"
			Game.mapvar.weapon[0] = "bouncy"
			SetPlayerAltWeapon( nil, "bouncy" )
			SwitchWeapon( nil, false )
			return "bouncy"
		end

		--square (not enix)
		if BonusCombo("square", "triangle", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "rocketlauncher"
			Game.mapvar.weapon[0] = "rocketlauncher"
			SetPlayerAltWeapon( nil, "rocketlauncher" )
			SwitchWeapon( nil, false )
			return "rocketlauncher"
		end
		if BonusCombo("square", "diamond", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "fragmentation"
			Game.mapvar.weapon[0] = "fragmentation"
			SetPlayerAltWeapon( nil, "fragmentation" )
			SwitchWeapon( nil, false )
			return "fragmentation"
		end
		if BonusCombo("square", "line", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "twinshot"
			Game.mapvar.weapon[0] = "twinshot"
			SetPlayerAltWeapon( nil, "twinshot" )
			SwitchWeapon( nil, false )
			return "twinshot"
		end

		--triangle
		if BonusCombo("triangle", "line", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "wave"
			Game.mapvar.weapon[0] = "wave"
			SetPlayerAltWeapon( nil, "wave_weapon" )
			SwitchWeapon( nil, false )
			return "wave_weapon"
		end
		if BonusCombo("triangle", "diamond", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "spread"
			Game.mapvar.weapon[0] = "spread"
			SetPlayerAltWeapon( nil, "spread" )
			SwitchWeapon( nil, false )
			return "spread"
		end

		--line
		if BonusCombo("line", "diamond", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "mikuru_beam"
			Game.mapvar.weapon[0] = "mikuru_beam"
			SetPlayerAltWeapon( nil, "mikuru_beam" )
			SwitchWeapon( nil, false )
			return "mikuru_beam"
		end

		--check out mah doubles
		if BonusCombo("circle", "circle", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "bonus_sync"
			Game.mapvar.weapon[0] = "bonus_sync"
			Game.mapvar.sync = 0
			Game.mapvar.bonuses[active_num] = {}
			Resume(NewThread(RemoveDoubleBonus), active_num , "bonus_sync")
		end
		if BonusCombo("square", "square", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "bonus_ammo"
			Game.mapvar.weapon[0] = "bonus_ammo"
			Game.mapvar.bonuses[mapvar.active_char ] = {}
			Game.mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), active_num, "bonus_ammo")
		end
		if BonusCombo("triangle", "triangle", bt[1], bt[2]) then
			Game.mapvar.weapon[mactive_num] = "bonus_speed"
			Game.mapvar.weapon[0] = "bonus_speed"
			AddBonusSpeed(char1.id, 3)
			AddBonusSpeed(char2.id, 3)
			Game.mapvar.bonuses[active_num] = {}
			Game.mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus),active_num , "bonus_speed", char1.id, char2.id)
		end
		if BonusCombo("diamond", "diamond", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "bonus_invul"
			Game.mapvar.weapon[0] = "bonus_invul"
			SetObjInvincible(id, true)
			Game.mapvar.bonuses[active_num] = {}
			Game.mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), active_num, "bonus_invul")
		end
		if BonusCombo("line", "line", bt[1], bt[2]) then
			Game.mapvar.weapon[active_num] = "bonus_health"
			Game.mapvar.weapon[0] = "bonus_health"
			Game.mapvar.bonuses[active_num] = {}
			Game.mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), active_num, "bonus_health")
		end

	end
	return nil
end

--SetOnChangePlayerProcessor(Game.CharacterSystem);

function Game.InitCustomWeapon()

end

function UpdateCustomWeapon( id )
end

function custom_weapon( id, shooter, param, dir, x, y )
	--DOUBLE NOPE
end

function Game.InitWeapons( pl )
	--ABSOLUTELY NOPE
end

Game.last_weapon_change = 0

function Game.change_weapon( player, weapon, forced )
	if not forced and (not Game.mapvar.actors[player] or not mapvar.weapons[weapon]) then
		return false
	end
	local pl = GetPlayerCharacter( player )
	if not pl then return false end
	if pl:weapon_charge_time() > 0 then
		return false
	end
	if forced or ( pl:weapon_ready() and (pl:alt_weapon_ready() or pl:ammo() == 0 ) ) then
		GUI:changeWeapon( player, weapon )
		Game.mapvar.actors[player].info.weapon = weapon
		SetDefaultActor( player )
		if weapon == 1 then
			if Game.actors[ player ].info.character == "pear15soh" then
				ReplacePrimaryWeapon( pl, "pear15soh_primary" )
				SetPlayerAltWeapon( 1, "pear15soh_alternative" )
			else
				ReplacePrimaryWeapon( pl, "pear15unyl_primary" )
				SetPlayerAltWeapon( 1, "pear15unyl_alternative" )
			end
		elseif weapon == 2 then
			ReplacePrimaryWeapon( pl, "pear15ray_primary" )
			SetPlayerAltWeapon( 1, "pear15ray_alternative" )
		elseif weapon == 3 then
			ReplacePrimaryWeapon( pl, "pear15spray_primary" )
			SetPlayerAltWeapon( 1, "pear15spray_alternative" )
		elseif weapon == 4 then
			ReplacePrimaryWeapon( pl, "pear15force_primary" )
			SetPlayerAltWeapon( 1, "pear15force_alternative" )
		end
		return true
	else
		return false
	end
end

local last_weapon_change = {0,0}

function Game.game_key_pressed( key )
	if not Loader.level then
		return false
	end
	if GamePaused() then
		return
	end
	for i = 1, 2 do
		for j = 1, 4 do
			if key == CONFIG.key_conf[i]["weapon_slot"..j] then
				Game.change_weapon( i, j )
			end
		end
	end
	if Game.actors[2] and key == CONFIG.key_conf[2].change_weapon and GetCurTime() - last_weapon_change[2] > 100 then
		last_weapon_change[2] = GetCurTime()
		local weapon = Game.mapvar.actors[2].info.weapon + 1 
		local first_weapon = Game.mapvar.actors[2].info.weapon
		while (not Game.change_weapon( 2, weapon )) and (not (weapon == first_weapon)) do
			weapon = weapon + 1
			if weapon >= 5 then
				weapon = 1
			end
		end
		SwitchWeapon( GetPlayerCharacter(2), false )
	elseif IsConfKey( key, config_keys.change_weapon ) and GetCurTime() - last_weapon_change[1] > 100 then
		last_weapon_change[1] = GetCurTime()
		local weapon = Game.mapvar.actors[1].info.weapon + 1
		local first_weapon = Game.mapvar.actors[1].info.weapon
		while (not Game.change_weapon( 1, weapon )) and (not (weapon == first_weapon)) do
			weapon = weapon + 1
			if weapon >= 5 then
				weapon = 1
			end
		end
		SwitchWeapon( GetPlayerCharacter(1), false )
	end
	if Game.info and Game.info.can_join and key == CONFIG.key_conf[2].gui_nav_accept and Game.info.players < 2 and not (Loader.state == 'game over') then
		Players.NewActor()
		if not Game.actors[2] then
			return
		end
		if Game.actors[1].info.character == "pear15soh" then
			Game.actors[2].info.character = "pear15unyl"
		else
			Game.actors[2].info.character = "pear15soh"
		end
		if mapvar and mapvar.actors then
			mapvar.actors[2] = deep_copy( Game.actors[2] )
		end
		GUI:updatePlayers()
		GUI:changeWeapon(2, 1)
		Info.RevealWeapon("soh")
		Info.RevealWeapon("unyl")
	elseif NETGAME and key == keys["t"] and not Game.mapvar.in_chat then
		Game.mapvar.in_chat = true
		Game.mapvar.subchat = CreateWidget(constants.wt_Label, "console_text", nil, 100, CONFIG.scr_height/2 - 20, 25500, CONFIG.scr_height/2 )
		WidgetSetCaptionColor( Game.mapvar.subchat, { 1, 1, 0.5, 1}, false )
		WidgetSetCaptionFont( Game.mapvar.subchat, "dialogue" )
		WidgetSetCaption( Game.mapvar.subchat, "SAY" )
		WidgetSetZ(Game.mapvar.subchat, 0.99999)
		Game.mapvar.chat = CreateWidget(constants.wt_Textfield, "console_text", nil, 150, CONFIG.scr_height/2 - 20, 25500, CONFIG.scr_height/2 )
		WidgetSetCaptionColor( Game.mapvar.chat, { 1, 1, 1, 1}, true )
		WidgetSetCaptionFont( Game.mapvar.chat, "dialogue" )
		WidgetSetZ(Game.mapvar.chat, 0.99999)

		WidgetGainFocus( 0 )
		WidgetGainFocus( Game.mapvar.chat )
		WidgetGainFocus( Game.mapvar.chat )
	elseif NETGAME and key == keys["enter"] and Game.mapvar.in_chat then
		ChatMessage( WidgetGetCaption( Game.mapvar.chat ) )
		DestroyWidget( Game.mapvar.chat )
		DestroyWidget( Game.mapvar.subchat )
		Game.mapvar.in_chat = false
	else
		return false
	end
	return false
end

function Game.game_key_released( key )
	if Editor then
		return false
	end
--[[	if isConfigKeyPressed(key, "change_weapon") then
		repeat 
			Game.mapvar.player_info[1].active_weapon = Game.mapvar.player_info[1].active_weapon + 1
		until Game.mapvar.player_info[1].weapons[Game.mapvar.player_info[1].active_weapon] or Game.mapvar.player_info[1].active_weapon > 4
		if Game.mapvar.player_info[1].active_weapon > 4 then
			Game.mapvar.player_info[1].active_weapon = 1
			SwitchWeapon( nil, false )
		else
			SetPlayerAltWeapon( nil, Game.mapvar.player_info[1].weapons[Game.mapvar.player_info[1].active_weapon].name )
			Game.mapvar.weapon = deep_copy( Game.mapvar.player_info[1].weapons[Game.mapvar.player_info[1].active_weapon].param )
			SwitchWeapon( nil, true )
		end
	else
		return false
	end
--]]	return false
end

function UpdateWeaponSlot( pl, num, weapon )
end

if not Editor then
	_GlobalSetKeyDownProc = GlobalSetKeyDownProc
	GlobalSetKeyDownProc = function( func )
		_GlobalSetKeyDownProc( function( key )
			if not Game.game_key_pressed( key ) then
				if func then
					return func( key )
				else
					return false
				end
			else
				return true
			end
		end )
	end
	_GlobalSetKeyReleaseProc = GlobalSetKeyReleaseProc
	GlobalSetKeyReleaseProc = function( func )
		_GlobalSetKeyReleaseProc( function( key )
			if not Game.game_key_released( key ) then
				if func then
					return func( key )
				else
					return false
				end
			else
				return true
			end
		end )
	end
end

function Game.GetNextCharacter()
	local spawn = { 0, 0 }
	if Loader.level and Loader.level.spawnpoints then
		spawn = random( Loader.level.spawnpoints )
	end
	return { "super-sohchan", spawn }
end

function Game.CreatePlayerTable()
	SetOnPlayerDeathProcessor(Game.onPlayerDeath)
	return {
		score = 0,
		lives = 3,
		weapon = 1,
		debt = 0,
		character = vars.character or Game.info.first_player_character or "pear15soh"
	}
end

local voided = {}

function Game.playerMisplaced( ud )
	local pl2 = GetPlayerCharacter( 2 )
	DamageObject( ud, 20 )
	if ud:health() >= 0 then
		if not pl2 or pl2:health() <= 0 then
			local x = mapvar.tmp.last_land_x or Loader.level.spawnpoints[1][1]
			local y = mapvar.tmp.last_land_y or Loader.level.spawnpoints[1][1]
			SetObjPos( ud, x, y )
		else
			local num = 2
			if ud == pl2 then
				num = 1
			end
			local pos = GetPlayerCharacter( num ):aabb().p
			SetObjPos( ud, pos.x, pos.y )
		end
	end
end

function Game.onPlayerDeath(old_plr_info, num, char_num)
	if Loader.level.onPlayerDeath then
		if Loader.level.onPlayerDeath(old_plr_info, num, char_num) then
			return
		end
	end
	if Game.mapvar.actors[num].info then
		Game.mapvar.actors[num].info.lives = Game.mapvar.actors[num].info.lives - 1
		local gameover = true
		if Game.mapvar.actors[num].info.lives > 0 then
			gameover = false
		elseif Game.mapvar.actors[2] and Game.mapvar.actors[3-num].info.lives > 1 then
			Game.mapvar.actors[num].info.debt = Game.mapvar.actors[num].info.debt + 1
			Game.mapvar.actors[num].info.lives = Game.mapvar.actors[num].info.lives + 1
			Game.mapvar.actors[3-num].info.lives = Game.mapvar.actors[3-num].info.lives - 1
			GUI:updateLives( 1, Game.mapvar.actors[1].info.lives )
			GUI:updateLives( 2, Game.mapvar.actors[2].info.lives )
			gameover = false
		end
		if gameover then
			mapvar.tmp.fail = true
			EnablePlayerControl( false )
			if Loader.state ~= 'game over' then
				Game.ShowCaptain()
			end
			Loader.state = 'game over'
		else
			local ch1 = GetPlayerCharacter(1)
			local ch2 = GetPlayerCharacter(2)
			SetDefaultActor(num)
			if num == 1 and ch2 and ch2:object_present() and ch2:health() >= 0 and not voided[2] then
				SetPlayerRevivePoint( ch2:aabb().p.x, ch2:aabb().p.y - 10 )
			elseif num == 2 and ch1 and ch1:object_present() and ch1:health() >= 0 and not voided[1] then
				SetPlayerRevivePoint( ch1:aabb().p.x, ch1:aabb().p.y - 10 )
			else
				local x = mapvar.tmp.last_land_x or Loader.level.spawnpoints[1][1]
				local y = mapvar.tmp.last_land_y or Loader.level.spawnpoints[1][1]
				SetPlayerRevivePoint( x, y )
				if not Game.mapvar.tmp.void then
					CamMoveToPos( x, y, true )
				elseif not Game.mapvar.actors[2] then
					SetCamAttachedObj( 0 )
				end
			end
			local plr_id = RevivePlayer(1, nil);
			--SetCamAttachedObj(plr_id)
			Game.mapvar.actors[num].char = GetObject( plr_id )
			Game.change_weapon( num, Game.mapvar.actors[num].info.weapon, true )
			Game.UpdateCharacter( GetObjectUserdata( plr_id ), Game.mapvar.actors[num].info.cahracter or "pear15soh" )
			SetPlayerAmmo(num,old_plr_info.ammo)
			inv = num
			voided[num] = false
			cam_dont_follow[num] = false
		end
	end
	Game.mapvar.tmp.void = (Game.mapvar.tmp.void or 0) - 1
	if Game.mapvar.tmp.void <= 0 then
		Game.mapvar.tmp.void = nil
	end
end

function Game.UpdateCharacter( ud, char )
	if (not ud) or (not CONFIG_EXTRA) or (CONFIG_EXTRA.ClassicPhysics == 0) then
		return
	end
	if char == "pear15soh" then
		ud:max_y_vel(50)
		ud:jump_vel(8)
		ud:gravity{ x = 0, y = 0.3 }
	elseif char == "pear15unyl" then
		ud:max_y_vel(50)
		ud:jump_vel(8.5)
		ud:gravity{ x = 0, y = 0.3 }
	else
		ud:max_y_vel(50)
		ud:jump_vel(8)
	end
end

local captain

function Game.ShowCaptain()
	if captain then
		DestroyWidget( captain )
		captain = nil
	end
	GUI:hide()
	captain = CreateWidget( constants.wt_Picture, "", nil, 10, 10, 100, 100 )
	WidgetSetSprite( captain, "portrait-captain", "left" )
	local captain_advice = Loader.level.hints or
	{
		"��������� �� ������, ���� ��� �� ��������!",
		"�������������� �� ��������� ���������, ����� �������� �����������!",
		"���� � ��� ������������� �����, �� �� ������������!",
		"���� � ��� ���� �������� - ��������� �����!"
	}
	local ad_vice = captain_advice[ math.random( 1, #captain_advice ) ]
	local captain_hint = CreateWidget( constants.wt_Label, "", captain, 140, 40, CONFIG.scr_width/2, 1 )
	WidgetSetCaptionFont( captain_hint, "dialogue", 1.5 )
	WidgetSetCaptionColor( captain_hint, { 0, 0, 0, 1 }, false )
	WidgetSetCaption( captain_hint, ad_vice, true )
	local sx, sy = GetMultilineCaptionSize( "dialogue", ad_vice, CONFIG.scr_width/2, 1.5 )
	local captain_bubble = CreateWidget( constants.wt_Panel, "", captain, 120, 20, sx + 40, sy + 40 )
	WidgetSetSprite( captain_bubble, "window2" )
	WidgetSetFocusable( captain_bubble, false )
end


function Game.HideCaptain()
	if captain then
		DestroyWidget( captain )
		captain = nil
	end
end

-- Initializes new 'stat' table for this game
-- Called on new game start
function Game.Prepare()
--	-- stat = { lives = 3, score = 0, game_timer = 0 }
	-- stat = { game_timer = 0 }
	Game.HideCaptain()
	Game.info = { 
		game_timer = 0, 
		players = 0, 
		first_player_character = vars.character or "pear15soh",
		weapons = { true, false, false, false },
		second_player_needed = vars.second_player_needed
	}
	
	Game.actors = {{}}
end

-- Initializes new empty 'Game.mapvar' table
function Game.PrepareMapvar(var)
	if var then
		Game.mapvar = var
		for k,v in pairs(var) do var[k] = nil end
	else
		Game.mapvar = {}
		-- setmetatable(Game.mapvar, dbg_set_printer)
		mapvar = Game.mapvar	-- for compatablitiy
	end

	Game.mapvar.tmp = {}
	Game.mapvar.actors = deep_copy( Game.actors )
	for k, v in ipairs( Game.actors ) do
		if v.info then
			ProtectedVariable( Game.variables.score_start + k, v.info.score )
		end
	end
end

-- Restores the content of 'mapvar' table from last 'stat'
function Game.RestoreMapvar()
	Game.mapvar.tmp = {}
	deep_merge(Game.mapvar, Game.info)
	
	for num, value in pairs(Game.actors) do
		value.char = nil -- Ïåðåäåëàòü
	end
	
	Game.mapvar.actors = deep_copy( Game.actors )
	for k, v in ipairs( Game.actors ) do
		if v.info then
			ProtectedVariable( Game.variables.score_start + k, v.info.score )
		end
	end
end

-- Saves the current state of mapvar table
function Game.AbsorbMapvar()
	Game.actors = deep_copy( Game.mapvar.actors or {{}} )
	
	for k,v in pairs(Game.info) do
		if type(Game.mapvar[k]) == 'table' then
			Game.info[k] = deep_copy(Game.mapvar[k])
		else
			Game.info[k] = Game.mapvar[k]
		end
	end
end

function Game.AddPlayerVars(num)
	Game.mapvar.actors[num] = deep_copy(Game.actors[num])
end

function Game.AddCombo( num )
	if GUI.comboIncrease then
		GUI:comboIncrease()
	end
	if GUI.visible == false then return end
	Game.ProgressAchievement( "FIRST_BLOOD", 1 )
	Game.last_combo_gain = Loader.time
	Game.mapvar.tmp.combo = (Game.mapvar.tmp.combo or 0) + num
	if Game.mapvar.tmp.combo >= 28 then
		Game.ProgressAchievement( "X28", 1 )
	end
	if Game.mapvar.tmp.combo >= 5 then
		Game.mapvar.tmp.combo_multiplyer = 2
		if Game.mapvar.tmp.combo >= 15 then
			for i=1,Game.mapvar.tmp.combo-5,10 do
				Game.mapvar.tmp.combo_multiplyer = Game.mapvar.tmp.combo_multiplyer + 1
			end
		end
	else
		Game.mapvar.tmp.combo_multiplyer = 1
	end
end

function Game.ResetCombo( num )
	Game.mapvar.tmp.combo = 0
	Game.mapvar.tmp.combo_multiplyer = 1
end

function Game.GetComboMultiplyer()
	return Game.mapvar.tmp.combo_multiplyer or 1
end

function Game.GetCombo()
	return Game.mapvar.tmp.combo or 0
end

function Game.GainScore( character_ud, score, x, y )
	local id = character_ud:id()
	local scx = x or character_ud:aabb().p.x
	local scy = y or character_ud:aabb().p.y
	for k, v in pairs( Game.mapvar.actors ) do
		if v.char.id == id then
			local check, old_value = ProtectedVariable( Game.variables.score_start + k )
			if not check then
				Log( "Score modified externally" )
			end
			_, Game.mapvar.actors[k].info.score = ProtectedVariable( Game.variables.score_start + k, old_value + score )
			while Game.mapvar.actors[k].info.score >= ( Game.mapvar.actors[k].info.next_life or 10000 ) do
				Game.GainLife( character_ud, scx, scy )
				PlaySnd( "life.ogg", true )
				if not Game.mapvar.actors[k].info.next_life then
					Game.mapvar.actors[k].info.next_life = 25000
				else
					Game.mapvar.actors[k].info.next_life = Game.mapvar.actors[k].info.next_life * 2
				end
			end
			GUI:updateScore( k, Game.mapvar.actors[k].info.score )
			break
		end
	end
	Game.ShowScoreGain( score, scx, scy )
end

function Game.ShowScoreGain( score, x, y, character )
	if not mapvar.tmp.score_widgets then
		mapvar.tmp.score_widgets = {}
	end
	local sx, sy = GetCaptionSize( "default", tostring(score) )
	local wx, wy = x-sx/2, y-sy/2
	local w = CreateWidget( constants.wt_Label, "score", nil, wx, wy, 1, 1 )
	local c = 1
	WidgetSetCaption( w, tostring(score) )
	WidgetSetCaptionColor( w, {1,1,1,1}, false, {0,0,0,1} )
	WidgetSetFixedPosition( w, false )
	WidgetSetZ( w, 0.9 )
	local cleanup = function() DestroyWidget(w) end
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	Resume( NewMapThread( function()
		for i = 1, 50 do
			wy = wy - 3
			c = c - 1/50
			WidgetSetPos( w, wx, wy )
			WidgetSetCaptionColor( w, { 1, 1, 1, c }, false, { 0, 0, 0, c } )
			Wait( 1 )
		end
		DestroyWidget( w )
		Game.cleanupFunctions[ cleanup ] = false
	end ))
end
--
function Game.GainHealth( character_ud, health, x, y )
	if character_ud:health() == character_ud:health_max() then
		Game.ProgressAchievement( "FULL_HEALTH", 1 )
	end
	local id = character_ud:id()
	local scx = x or character_ud:aabb().p.x
	local scy = y or character_ud:aabb().p.y
	for k, v in pairs( Game.mapvar.actors ) do
		if v.char.id == id then
			--???
			--Game.mapvar.actors[k].info.health = Game.mapvar.actors[k].info.health + health
			--GUI:updateHealth( k, Game.mapvar.actors[k].info.ammo )
			break
		end
	end
	Game.ShowHealthGain( health, scx, scy )
end
--
function Game.ShowHealthGain( health, x, y )
	local sx, sy = GetCaptionSize( "default", "+"..tostring(health) )
	local wx, wy = x-sx/2, y-sy/2
	local w = CreateWidget( constants.wt_Label, "health", nil, wx, wy, 1, 1 )
	local c = 1
	WidgetSetCaption( w, "+"..tostring(health) )
	WidgetSetCaptionColor( w, {0,1,0,0}, false, {0,0,0,1} )
	WidgetSetFixedPosition( w, false )
	WidgetSetZ( w, 0.9 )
	local cleanup = function() DestroyWidget(w) end
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	Resume( NewMapThread( function()
		for i = 1, 50 do
			wy = wy - 3
			c = c - 1/50
			WidgetSetPos( w, wx, wy )
			WidgetSetCaptionColor( w, { 0, 1, 0, c }, false, { 0, 0, 0, c } )
			Wait( 1 )
		end
		DestroyWidget( w )
		Game.cleanupFunctions[ cleanup ] = false
	end ))
end
--[[
function Game.GainAmmo( character_ud, ammo, x, y )
	local id = character_ud:id()
	local scx = x or character_ud:aabb().p.x
	local scy = y or character_ud:aabb().p.y
	for k, v in pairs( Game.mapvar.actors ) do
		if v.char.id == id then
			--???
			--Game.mapvar.actors[k].info.ammo = Game.mapvar.actors[k].info.ammo + ammo
			--GUI:updateAmmo( k, Game.mapvar.actors[k].info.ammo )
			break
		end
	end
	Game.ShowAmmoGain( ammo, scx, scy )
end
--]]
function Game.ShowAmmoGain( ammo, x, y )
	local sx, sy = GetCaptionSize( "default", "+"..tostring(ammo) )
	local wx, wy = x-sx/2, y-sy/2
	local w = CreateWidget( constants.wt_Label, "ammo", nil, wx, wy, 1, 1 )
	local c = 1
	WidgetSetCaption( w, "+"..tostring(ammo) )
	WidgetSetCaptionColor( w, {0.3,0.3,1,0}, false, {0,0,0,1} )
	WidgetSetFixedPosition( w, false )
	WidgetSetZ( w, 0.9 )
	local cleanup = function() DestroyWidget(w) end
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	Resume( NewMapThread( function()
		for i = 1, 50 do
			wy = wy - 3
			c = c - 1/50
			WidgetSetPos( w, wx, wy )
			WidgetSetCaptionColor( w, { 0.3, 0.3, 1, c }, false, { 0, 0, 0, c } )
			Wait( 1 )
		end
		DestroyWidget( w )
		Game.cleanupFunctions[ cleanup ] = false
	end ))
end

function Game.ShowLifeGain( x, y )
	local sx, sy = GetCaptionSize( "default", "1UP" )
	local wx, wy = x-sx/2, y-sy/2
	local w = CreateWidget( constants.wt_Label, "life", nil, wx, wy, 1, 1 )
	WidgetSetCaption( w, "1UP" )
	WidgetSetCaptionColor( w, {1,1,1,0}, false, {0,0,0,1} )
	WidgetSetFixedPosition( w, false )
	WidgetSetZ( w, 0.9 )
	local cleanup = function() DestroyWidget(w) end
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	Resume( NewMapThread( function()
		for i = 1, 100 do
			wy = wy - 1
			WidgetSetPos( w, wx, wy )
			WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
			Wait( 1 )
		end
		DestroyWidget( w )
		Game.cleanupFunctions[ cleanup ] = false
	end ))
end

function Game.CreateScoreItems( score, x, y )
	local total_score = score * Game.GetComboMultiplyer()
	while total_score > 0 do
		if total_score >= 10 then
			total_score = total_score - 10
			CreateEnemy( "wakabamark_large", x, y )
		elseif total_score >= 4 then
			total_score = total_score - 4
			CreateEnemy( "wakabamark_medium", x, y )
		elseif total_score >= 1 then
			total_score = total_score - 1
			CreateEnemy( "wakabamark_small", x, y )
		end
	end
end

function Game.characterHurt( obj, damage_type )
	local cleanup = function()
		SetCamScale( 1, 1 )
		SetCamAngle( 0 )
	end
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	if damage_type == Game.damage_types.substances then
		Game.cleanupFunctions[ cleanup ] = true
		Resume( NewMapThread( function()
			Game.ProgressAchievement( "WELCOME_TO_OMSK", iff( mapvar.drugged, Loader.time + 5000 - (mapvar.drugged_time or Loader.time), 5000 ) )
			mapvar.drugged_time = Loader.time + 5000
			if mapvar.drugged then
				return

			end
			mapvar.drugged = true
			local zx, zy = 1, 1
			local rot = 0
			while Loader.time < mapvar.drugged_time do
				zx = 1 + 0.1 * math.cos( Loader.time / 1000 )
				zy = 1 + 0.1 * math.cos( Loader.time / 1200 )
				rot = 0 + 2 * math.cos( Loader.time / 1310 )
				SetCamScale( zx, zy )
				SetCamAngle( rot )
				Wait(1)
			end
			while math.abs(1-zx) > 0.01 or math.abs(1-zy) > 0.01 or math.abs(rot) > 0.1 do
				zx = zx + (1 - zx) * 0.5
				zy = zy + (1 - zy) * 0.5
				rot = rot / 2
				SetCamScale( zx, zy )
				SetCamAngle( rot )
				Wait(1)
			end
			Game.cleanupFunctions[ cleanup ] = nil
			SetCamScale( 1, 1 )
			SetCamAngle( 0 )
			mapvar.drugged = false
		end ))
	end
	local id = obj:id()
	for k, v in pairs( Game.mapvar.actors ) do
		if id == v.char.id then
			GUI:healthDecrease( k )
			break
		end
	end
end

function Game.LevelCleanup()
	if not Game.cleanupFunctions then
		return
	end
	for k,v in pairs( Game.cleanupFunctions ) do
		if v then k() end
	end
	Game.info.players = 0
	Game.cleanupFuncitons = nil
	GUI:hideCombo()
end

function Game.GetNextCharacter()
	if not Loader.level or not Loader.level.spawnpoints then
		return nil
	end
	if Game.actors[1] and Game.actors[1].char then
		Game.info.players = Game.info.players + 1
	else
		Game.info.players = 1
	end
	if Game.info.players == 1 then
		return { vars.character or Game.info.first_player_character or "pear15soh", Loader.level.spawnpoints[1] or {0,0} }
	else
		local pos = GetObjectUserdata( Game.mapvar.actors[1].char.id ):aabb().p
		if Game.actors[1].info.character == "pear15soh" then
			return { "pear15unyl", {pos.x, pos.y} }
		else
			return { "pear15soh", {pos.x, pos.y} }
		end
	end
end

function Game.AttachCamToPlayers( bounds )
	local camera_bounds = bounds
	local manual_offset = 0
		cam_dont_follow = {}
	if bounds then
		SetCamUseBounds(true)
	else
		SetCamUseBounds(false)
	end
	if mapvar.camera_thread then
		StopThread( mapvar.camera_thread )
	end
	mapvar.camera_thread = NewMapThread( function()
		local cx, cy = GetCamPos() 
		local old_area = -1
		local camera_target = 0
		while true do
			local ccx, ccy = GetCamPos()
			local o1, o2 = GetPlayerCharacter(1), GetPlayerCharacter(2)
			local c = ((o1 and 1) or 0) + ((o2 and 1) or 0)
			local p1 = (o1 and o1:health() > 0 and (not cam_dont_follow[1]) and o1:aabb().p) or { x = 0, y = 0 }
			local p2 = (o2 and o2:health() > 0 and (not cam_dont_follow[2]) and o2:aabb().p) or { x = 0, y = 0 }
			manual_offset = (((o1 and o1:camera_offset()) or 0) + ((o2 and o2:camera_offset()) or 0)) / 2
			if c == 1 and (cam_dont_follow[1] or not o1 or o1:health() <= 0) then
				ccx, ccy = GetCamPos()
			elseif c == 2 and (cam_dont_follow[1] or not o2 or o2:health() <= 0) and (cam_dont_follow[1] or not o2 or o2:health() <= 0) then
				ccx, ccy = GetCamPos()
			elseif c > 0 then
				ccx, ccy = (p1.x + p2.x) / c, (p1.y + p2.y) / c
			end
			
			for k, v in ipairs( camera_bounds or {} ) do
				if ccx > v[1][1] and ccx < v[1][3] and ccy > v[1][2] and ccy < v[1][4] then
					SetCamBounds( v[2][1], v[2][3], v[2][2], v[2][4] )
					if old_area ~= k then
						SetCamUseBounds( true )
						old_area = k
						camera_target = 0
					end
					break
				end
			end
			local player1 = GetPlayerCharacter(1)
			local player2 = GetPlayerCharacter(2)
			local tmp = {}
			if mapvar and mapvar.tmp then
				tmp = mapvar.tmp
			end
			if player1 and player1:health() > 0 and player1:on_plane() ~= 0 then
				local plane = player1:suspected_plane()
				if plane then
					local aabb = player1:aabb()
					local aabb_plane = plane:aabb();
					if aabb.p.x + aabb.W < aabb_plane.p.x + aabb_plane.W and aabb.p.x - aabb.W > aabb_plane.p.x - aabb_plane.W then
						mapvar.tmp.last_land_x = aabb.p.x
						mapvar.tmp.last_land_y = aabb.p.y - 10
					end
				end
			end
			local boss_object = tmp.boss_object or nil
			local poi
			local n
			local aabb
			if tmp.poi and tmp.poi:object_present() then
				poi = mapvar.tmp.poi:aabb().p
				if mapvar.tmp.poi_y then
					poi.y = poi.y + mapvar.tmp.poi_y
				end
				n = 1
				ctx = ctx + poi.x
				cty = cty + poi.x
				if player1 and player1:object_present() then
					aabb = player1:aabb()
					ctx = ctx + aabb.p.x + cam_off_x
					cty = cty + aabb.p.y + aabb.H + cam_off_y + manual_offset
					n = n + 1
				end
				if player2 and player2:object_present() then
					aabb = player2:aabb()
					ctx = ctx + aabb.p.x + cam_off_x
					cty = cty + aabb.p.y + aabb.H + cam_off_y + manual_offset
					n = n + 1
				end
				ctx = math.min( poi.x + CONFIG.scr_width / 2, math.max( poi.x - CONFIG.scr_width / 2, ctx / n ) )
				cty = math.min( poi.y + CONFIG.scr_height / 2, math.max( poi.y - CONFIG.scr_height / 2, cty / n ) )
				local cx, cy = GetCamPos()
				cx = cx + ( ctx - cx ) * 0.1
				cy = cy + ( cty - cy ) * 0.1
				CamMoveToPos( cx, cy )
			elseif boss_object and boss_object:object_present() then
				if camera_target ~= 0 then
					SetCamAttachedObj( 0 )
					camera_target = 0
				end
				local points = {}
				local o
				if player1 and player1:object_present() then
					o = player1:aabb()
					table.insert( points, { o.p.x + cam_off_x, o.p.y + o.H + cam_off_y + manual_offset } )
				end
				if player2 and player2:object_present() then
					o = player2:aabb()
					table.insert( points, { o.p.x + cam_off_x, o.p.y + o.H + cam_off_y + manual_offset } )
				end
				if boss_object:object_present() then
					o = boss_object:aabb()
					table.insert( points, { o.p.x, o.p.y + o.H } )
				end
				if #points > 0 then
					ctx = 0
					cty = 0
					for k, v in pairs( points ) do
						ctx = ctx + v[1]
						cty = cty + v[2]
					end
					ctx = ctx / #points
					cty = cty / #points
					--Check if both players are in frame, otherwise forget about boss
					local ok = true
					for i=1,#points-1 do
						if math.abs( points[i][1] - ctx ) > CONFIG.scr_width / 2 or 
						   math.abs( points[i][2] - cty ) > CONFIG.scr_height / 2 then
							ok = false
							break
						end
					end
					if not ok then
						if #points == 3 then
							if math.abs( points[1][1] - points[2][1] ) > CONFIG.scr_width or
							   math.abs( points[1][2] - points[2][2] ) > CONFIG.scr_height then
								SetObjPos( player2, player1:aabb().p.x, player1:aabb().p.y - 10 )
								ctx = points[1][1]
								cty = points[1][2] 
							else
								ctx = (points[1][1] + points[2][1]) / 2
								cty = (points[1][2] + points[2][2]) / 2
							end
						else
							ctx = points[1][1]
							cty = points[1][2]
						end
					end
				end
				local cx, cy = GetCamPos()
				cx = cx + ( ctx - cx ) * 0.1
				cy = cy + ( cty - cy ) * 0.1
				CamMoveToPos( cx, cy )
			else
				local pl1_valid = not (not player1 or not player1:object_present() or player1:health() <= 0)
				local pl2_valid = not (not player2 or not player2:object_present() or player2:health() <= 0)
				if (not player2 or not player2:object_present()) and (not player1 or not player1:object_present() ) then
					--Do nothing
				elseif not pl2_valid then
					if cam_dont_follow[1] or not pl1_valid then
						SetCamAttachedObj( 0 )
						camera_target = 0
					elseif player1 and player1:object_present() and camera_target ~= player1:id() then
						SetCamAttachedObj( player1:id() )
						camera_target = player1:id() 
					end
				elseif not pl1_valid then
					if cam_dont_follow[2] or not pl2_valid then
						SetCamAttachedObj( 0 )
						camera_target = 0
					elseif player2 and player2:object_present() and camera_target ~= player2:id()  then
						SetCamAttachedObj( player2:id() )
						camera_target = player2:id() 
					end
				else
					if camera_target ~= 0 then
						SetCamAttachedObj( 0 )
						camera_target = 0
					end
					local cx, cy = GetCamPos()
					local o1 = (player1 and player1:aabb()) or { p = {x = cx, y = cy}, W = 0, H = 0 }
					local o2 = (player2 and player2:aabb()) or { p = {x = cx, y = cy}, W = 0, H = 0 }
					ctx = ( o1.p.x + o2.p.x ) / 2 + cam_off_x
					cty = ( o1.p.y + o1.H + o2.p.y + o2.H )/ 2 + cam_off_y + manual_offset
					cx = cx + ( ctx - cx ) * 0.1
					cy = cy + ( cty - cy ) * 0.1
					if (( o2.p.x + o2.W < cx - CONFIG.scr_width/2 or o2.p.x - o2.W > cx + CONFIG.scr_width / 2 ) or
					   ( o2.p.y + o2.H < cy - CONFIG.scr_height/2 or o2.p.y - o2.H > cy + CONFIG.scr_height / 2 )) then	
						if player2:health() <= 0 then
							SetObjInvisible( player2, true )
						end
						if player1:health() > 0 then
							SetObjPos( player2, player1:aabb().p.x, player1:aabb().p.y - 10 )
							if mapvar.tmp[player2] and mapvar.tmp[player2].effect then
								SetObjDead( mapvar.tmp[player2].effect )
								mapvar.tmp[player2].effect = nil
							end
							player2:vel( player1:vel() )
							player2:own_vel{ x = 0, y = 0 } 
							ctx = o1.p.x
							cty = o1.p.y + o1.H
						else
							ctx = o2.p.x
							cty = o2.p.y + o2.H						
						end
					end
					CamMoveToPos( cx, cy )
				end
			end
			Wait(1)
		end
	end )
	Resume( mapvar.camera_thread )
end

function Game.GainLife( object, x, y )
	local scx = x
	local scy = y
	for k, v in ipairs( Game.mapvar.actors ) do
		if GetPlayerCharacter(k) == object then
			if Game.mapvar.actors[2] and v.info.debt > 0 then
				v.info.debt = v.info.debt - 1
				Game.mapvar.actors[3-k].info.lives = Game.mapvar.actors[3-k].info.lives + 1
				GUI:updateLives( 1, Game.mapvar.actors[1].info.lives )
				GUI:updateLives( 2, Game.mapvar.actors[2].info.lives )
			else
				v.info.lives = v.info.lives + 1
				GUI:updateLives( k, v.info.lives )
			end
			break
		end
	end
	Game.ShowLifeGain( scx, scy - 30 )
end

function Game.CharacterLanded( character )
	Game.ProgressAchievement( "PLUMBER", 0, true )
	if mapvar.tmp.void then
		return
	end
	if Game.mapvar.actors[1].char and character:id() == Game.mapvar.actors[1].char.id then
		local pos = character:aabb().p
		mapvar.tmp.last_land_x = pos.x
		mapvar.tmp.last_land_y = pos.y - 10
	end
end

function Game.StartWeaponCharge( character, effect )
	local id = character:id()
	for k, v in pairs( Game.mapvar.actors ) do
		if v.char.id == id then
			GUI:startWeaponCharge( k, 1000, effect )
			break
		end
	end
end

function Game.ProgressAchievement( ach, num, reset )
	Achievements.Progress( ach, Game.profile, num, reset )
	Saver:saveProfile()
end

function Game.BitProgressAchievement( ach, num )
	Achievements.BitProgress( ach, Game.profile, num )
	Saver:saveProfile()
end

function Game.GetPlayersNum()
	return Game.info.players
end

function Game.GetPlayerCharacter( num )
	if num and (num < 0 or num > Game.info.players) then
		return nil
	end
	return GetPlayerCharacter( num )
end

function Game.GetActorByCharacter( char )
    local id = char
    if type( char ) == 'userdata' then 
        id = char:id() 
    end
    for k, v in pairs( Game.mapvar.actors ) do
        if char:id() == v.char.id then
            return k
        end
    end
    return nil
end

function Game.EnemyDead( char, damage_type, enemy_type, size )
	local player
	if type(char) == 'userdata' then
		player = Game.GetActorByCharacter( char )
	elseif type(char) == 'number' then
		player = char
	end
	if not player then
		return
	end
	if not mapvar.tmp.kills then
		mapvar.tmp.kills = {}
	end
	if not mapvar.tmp.kills[player] then
		mapvar.tmp.kills[player] = {}
	end
	if not mapvar.tmp.kills[player][enemy_type] then
		mapvar.tmp.kills[player][enemy_type] = { count = 0, size = size }
	end
	mapvar.tmp.kills[player][enemy_type].count = mapvar.tmp.kills[player][enemy_type].count + 1
	if damage_type > Game.damage_types.player_weapon_start and damage_type < Game.damage_types.player_weapon_end then
		Game.BitProgressAchievement( "WEAPON_TESTER", damage_type - Game.damage_types.player_weapon_start )
	end
end

-------------------------------------------------------------------------------------------------------
--Loader hooks

function Game.startGame()
	Game.ProgressAchievement( "LORD_OF_TIME", 0, true )
	BlockPlayerChange()
end

function Game.restartGame()
	Game.HideCaptain()
	GUI:hideCombo()
	BlockPlayerChange()
	if Game.info.players == 2 or Game.actors[2] then
		Game.info.second_player_needed = true
	end
	Game.info.players = 0
	Game.info.can_join = true
end

function Game.UpdatePlayers()
	if Game.info.second_player_needed then
		Players.NewActor()
		GUI:updatePlayers()
		GUI:changeWeapon(2, 1)
		if Game.actors[2].info and Game.actors[2].info.ammo then
			GetPlayerCharacter(2):ammo(Game.actors[2].info.ammo)
		end
	end
	if Game.actors[1].info and Game.actors[1].info.ammo then
		GetPlayerCharacter(1):ammo(Game.actors[1].info.ammo)
	end
	if Game.actors[2] and Game.actors[2].info and Game.actors[2].info.ammo then
		GetPlayerCharacter(2):ammo(Game.actors[2].info.ammo)
	end
	BlockPlayerChange()
	Game.UpdateCharacter( GetPlayerCharacter(1), Game.actors[1].info.character or "pear15soh" )
	if Game.actors[2] then
		Game.UpdateCharacter( GetPlayerCharacter(2), Game.actors[2].info.character or "pear15soh" )
	end
	GUI:changeWeapon(1, 1)
	Game.info.can_join = true
	if Game.mapvar.actors[1] then
		Game.change_weapon( 1, Game.mapvar.actors[1].info.weapon or 1, true )
	end
	if Game.mapvar.actors[2] then
		if Game.actors[1].info.character == "pear15soh" then
			Game.actors[2].info.character = "pear15unyl"
		else
			Game.actors[2].info.character = "pear15soh"
		end
		Game.mapvar.actors[2].info.character = Game.actors[2].info.character
		Game.change_weapon( 2, Game.mapvar.actors[2].info.weapon or 1, true )
	end
end

function Game.ChangeLevel()
	if mapvar.tmp.slowleks_destroyed then
		Game.ProgressAchievement( "LORD_OF_TIME", mapvar.tmp.slowleks_destroyed )
	end
	local obj = GetPlayerCharacter(1)
	if obj then
		Game.mapvar.actors[1].info.ammo = obj:ammo()
	else
		Game.mapvar.actors[1].info.ammo = nil
	end
	if difficulty < 1 then
		Game.mapvar.actors[1].info.lives = math.max(Game.mapvar.actors[1].info.lives, 3)
	end
	if Game.actors[2] then
		obj = GetPlayerCharacter(2)
		if obj then
			Game.mapvar.actors[2].info.ammo = obj:ammo()
		else
			Game.mapvar.actors[2].info.ammo = nil
		end
		if difficulty < 1 then
			Game.mapvar.actors[2].info.lives = math.max(Game.mapvar.actors[2].info.lives, 3)
		end
	end
end

function Game.exitGame()
	for i=1,2 do
		ProtectedVariable( Game.variables.score_start + i, 0 )
	end
	Game.ProgressAchievement( "LORD_OF_TIME", 0, true )
	Game.HideCaptain()
	Game.info.players = 0
	Game.info.can_join = false
end

-------------------------------------------------------------------------------------------------------
function Game.ShowLevelResults( next_level, skip_menu )
	Game.reset_fade()
	StopMapThreads()
	SetCamScale( 1, 1 )
	SetCamAngle( 0 )
	SetCamAttachedObj(0)
	SwitchLighting(false)
	Game.ResetCombo()
	GUI:hideCombo()
	GUI:hide()
	Menu.lock()
	for k, v in pairs(Game.actors) do
		SetDefaultActor( k )
		GetPlayerCharacter( k ):is_invincible( true )
		EnablePlayerControl( false )
	end
 	FadeOut({0, 0, 0}, 0)
	local background = CreateWidget( constants.wt_Widget, "", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
	WidgetSetZ( background, 1.04 )
	WidgetSetSpriteBlendingMode( background, constants.bmSrcA_OneMinusSrcA )
	SetCamUseBounds( false )
	CamMoveToPos( -9999+CONFIG.scr_width/2, -9999+CONFIG.scr_height/2 )
	local player_done = { false, false }
	local final_widgets = {}
	local final_enemas = {}
	local player_thread = function(player)
		local sx, sy = GetCaptionSize( "default", dictionary_string("PLAYER").." "..player )
		local title = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2 + (player-1) * CONFIG.scr_width/2, 10, 1, 1 )
		local enema
		local widgets = {}
		local enemas = {}
		local x = CONFIG.scr_width / 4 + (player-1) * CONFIG.scr_width/2
		local y = 10 + 4 * sy
		WidgetSetCaptionColor( title, { 1, 1, .7, 1 }, false ) 
		WidgetSetCaption( title, dictionary_string("PLAYER").." "..player )
		if player > Game.GetPlayersNum() then
			sx, sy = GetCaptionSize( "default", "NOT PRESENT" )
			nokills = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2 + (player-1) * CONFIG.scr_width/2, 10 + 4 * sy, 1, 1 )
			WidgetSetCaptionColor( nokills, { 1, 1, .7, 1 }, false ) 
			WidgetSetCaption( nokills, dictionary_string("NOT PRESENT") )
			player_done[player] = true
			return
		end
		if mapvar.tmp.kills and mapvar.tmp.kills[player] then
			local keys = {}
			for k, v in pairs( mapvar.tmp.kills[player] ) do
				table.insert( keys, k )
			end
			table.sort( keys )
			for i=1, #keys do
				local k = keys[i]
				local v = mapvar.tmp.kills[player][keys[i]]
				if y + 2 * v.size[2] <= CONFIG.scr_height then
					Wait( 500 )
				else
					Wait( 3000 )
					for k2, v2 in pairs( enemas ) do
						SetObjDead(v2)
					end
					enemas = {}
					for k2, v2 in pairs( widgets ) do
						DestroyWidget(v2)
					end
					widgets = {}
					y = 10 + 4 * sy
				end
				enema = GetObjectUserdata(CreateEnemy( k, x-v.size[1], y+v.size[2], "final_count" ))
				enema:sprite_z( 1.1 )
				SetObjPos( enema, x-v.size[1]-9999, y+v.size[2]-9999 )
				local w = CreateWidget( constants.wt_Label, "", background, 3*CONFIG.scr_width / 8 + (player-1) * CONFIG.scr_width/2, y + v.size[2], 1, 1 )
				WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
				WidgetSetCaption( w, "x"..v.count )
				table.insert( widgets, w )
				table.insert( enemas, enema )
				y = y + 2 * v.size[2] + 20
			end
		else
			sx, sy = GetCaptionSize( "default", dictionary_string("NO KILLS") )
			nokills = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2 + (player-1) * CONFIG.scr_width/2, 10 + 4 * sy, 1, 1 )
			WidgetSetCaptionColor( nokills, { 1, 1, 1, 1 }, false ) 
			WidgetSetCaption( nokills, dictionary_string("NO KILLS") )
			table.insert( widgets, nokills )
		end
		Wait( 1000 )
		for k, v in pairs( widgets ) do
			table.insert( final_widgets, v )
		end
		for k, v in pairs( enemas ) do
			table.insert( final_enemas, v )
		end
		player_done[player] = true
	end
	Resume( NewThread( player_thread ), 1 )
	Resume( NewThread( player_thread ), 2 )
	Resume( NewThread( function()
		while not ( player_done[1] and player_done[2] ) do
			Wait( 100 )
		end
		Wait( 3000 )
		for k2, v2 in pairs( final_enemas ) do
			SetObjDead(v2)
		end
		for k2, v2 in pairs( final_widgets ) do
			DestroyWidget(v2)
		end
		sx, sy = GetCaptionSize( "default", dictionary_string("FINAL SCORE:") )
		local score11 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2, 10 + 4 * sy, 1, 1 )
		WidgetSetCaptionColor( score11, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( score11, dictionary_string("FINAL SCORE:") )
		sx, sy = GetCaptionSize( "default", tostring( Game.mapvar.actors[1].info.score ) )
		local score12 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2, 10 + 6 * sy, 1, 1 )
		WidgetSetCaptionColor( score12, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( score12, tostring( Game.mapvar.actors[1].info.score ) )
		if Game.GetPlayersNum() > 1 then
			sx, sy = GetCaptionSize( "default", dictionary_string("FINAL SCORE:") )
			local score21 = CreateWidget( constants.wt_Label, "", background, 3 * CONFIG.scr_width / 4 - sx/2, 10 + 4 * sy, 1, 1 )
			WidgetSetCaptionColor( score21, { 1, 1, 1, 1 }, false ) 
			WidgetSetCaption( score21, dictionary_string("FINAL SCORE:") )
			sx, sy = GetCaptionSize( "default", tostring( Game.mapvar.actors[2].info.score ) )
			local score22 = CreateWidget( constants.wt_Label, "", background, 3 * CONFIG.scr_width / 4 - sx/2, 10 + 6 * sy, 1, 1 )
			WidgetSetCaptionColor( score22, { 1, 1, 1, 1 }, false ) 
			WidgetSetCaption( score22, tostring( Game.mapvar.actors[2].info.score ) )
		end
		sx, sy = GetCaptionSize( "default", dictionary_string("SECRETS FOUND:") )
		local secrets1 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 2 - sx/2, CONFIG.scr_height/2 - 2 * sy, 1, 1 )
		WidgetSetCaptionColor( secrets1, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( secrets1, dictionary_string("SECRETS FOUND:") )
		local secrets_found = Game.mapvar.tmp.secrets or 0
		local secrets_total = Loader.level.secrets or 0
		if Loader.level.name == "Sewers" and secrets_found == 0 then
			Game.ProgressAchievement( "SEWER_NOSECRETS", 1 )
		end
		if Loader.level.name == "Sewers" and secrets_found == secrets_total then
			Game.ProgressAchievement( "SEWER_SECRETS", 1 )
		end
		if Loader.level.name == "Mountains" and secrets_found == 0 then
			Game.ProgressAchievement( "MOUNTAINS_NOSECRETS", 1 )
		end
		if Loader.level.name == "Mountains" and secrets_found == secrets_total then
			Game.ProgressAchievement( "MOUNTAINS_SECRETS", 1 )
		end
		secrets_found = tostring(secrets_found).." "..dictionary_string( "OUT OF" ).." "..tostring(secrets_total)
		sx, sy = GetCaptionSize( "default", secrets_found )
		local secrets2 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 2 - sx/2, CONFIG.scr_height/2 + 2 * sy, 1, 1 )
		WidgetSetCaptionColor( secrets2, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( secrets2, secrets_found )
		Wait( 3000 )
		DestroyWidget( background )
		FadeIn(0)
		SwitchLighting(true)
		if ( next_level ) then
			if not skip_menu then
				Menu.showEndLevelMenu( next_level )
			else
				Loader.ChangeLevel( next_level )
			end
		end
	end ))
end

------------------------------------------------------------------------------------------------------------------------------------
function Game.ShowTip( num, reveal, post_event )
	if reveal then
		Game.profile.info_pages["tutorial"..num] = Info.UNLOCKED
		Saver:saveProfile()
	end
	if Game.info.no_tutorials then
		if post_event then
			return post_event()
		end
		return
	end
	local window
	Menu.lock()
	SwitchLighting(false)
	local x, y
	if num == 1 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 246 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236, 245, 236 )
		WidgetSetSprite( picture, "tips1", true, 2 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+25-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("������ ����� � ������ ���������� ���������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+108-5, 226, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("����� � ���� �������� ������."), true )
		local tip3 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+188-5, 226, 26 )
		WidgetSetCaptionFont( tip3, "dialogue" )
		WidgetSetCaptionColor( tip3, {1,1,1,1}, false )
		WidgetSetCaption( tip3, dictionary_string("������ � ���������� �������� �� ������������ ��������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 2 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 246 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236, 245, 236 )
		WidgetSetSprite( picture, "tips1", true, 0 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+21-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("������� ���������� �� ����, ����� ��������� ��� ���������� �����������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+106-5, 226, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("����������� ������ ����������� � ����������, ����� ������ �� �����."), true )
		local tip3 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+188-5, 226, 26 )
		WidgetSetCaptionFont( tip3, "dialogue" )
		WidgetSetCaptionColor( tip3, {1,1,1,1}, false )
		WidgetSetCaption( tip3, dictionary_string("��������� ���������� ������ ������� �� ����, ��� ����� �� ������� ������ ��������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 3 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 246 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236, 245, 236 )
		WidgetSetSprite( picture, "tips1", true, 1 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-92, -236+27-5, 226+92, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("� ������� ������ ���� ��� ������ ����. ��������������, � ������� �� ���������, ���������� �������, ����� ��� ����� ������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-92, -236+150-5, 226+92, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("������������ ������ �������� �������� ����� � ����� ������ ���� ������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 4 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 246 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236, 245, 236 )
		WidgetSetSprite( picture, "tips1", true, 3 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+41-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("��������� ��������� �������� ��������� �������������� ������ � �������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+106-5, 226, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("��������� �������� �� ������������� ���������� ����� - ��������� �� ��� ����� ����������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 5 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 326 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-156, -236+15, 2*156, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("�� �������� ����� ������ - �������� ��������� PLGB."), true )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236+35, 245, 236 )
		WidgetSetSprite( picture, "tips2", true, 1 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+61-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("�������� ��������������� ���� ������� �� ����, ��� ����� �� ������� ������ ��������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+136-5, 226, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("����������� ������ ������������ ������, ����� ������� ������� ������."), true )
		local tip3 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+208-5, 226, 26 )
		WidgetSetCaptionFont( tip3, "dialogue" )
		WidgetSetCaptionColor( tip3, {1,1,1,1}, false )
		WidgetSetCaption( tip3, dictionary_string("��������� �������� �� ������������� ���������� ����� - ��������� �� ��� ����� ���������� ������ �������������� ����� ����� ������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 6 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 246 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236, 245, 236 )
		WidgetSetSprite( picture, "tips4", true, 0 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+25-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("��������� � ���� ������ ���������� �����������. ��� ������� ����������� �������� ������ ��������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+108-5, 226, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("����������� ������, ����� �������� ��������, � �����, ����� �� ����� �� �����."), true )
		local tip3 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+188-5, 226, 26 )
		WidgetSetCaptionFont( tip3, "dialogue" )
		WidgetSetCaptionColor( tip3, {1,1,1,1}, false )
		WidgetSetCaption( tip3, dictionary_string("����� � �������� ����������� ����� ������ � ������!"), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 7 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 326 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-156, -236+15, 2*156, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("�� �������� ����� ������ - ����������� HPLG-37."), true )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236+35, 245, 236 )
		WidgetSetSprite( picture, "tips2", true, 2 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+61-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("�� ������ ������ ���� ������� ������� �������� ��������, � ����� ������ ������. ����� �������� ����� ���������� ������ �����������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-50, -236+176-5, 226+50, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("�������� ����������� � ���������� ��������� ��������� ��������� ������� ���������������� ���������."), true )
		local tip3 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-140, -236+248-5, 226+140, 26 )
		WidgetSetCaptionFont( tip3, "dialogue" )
		WidgetSetCaptionColor( tip3, {1,1,1,1}, false )
		WidgetSetCaption( tip3, dictionary_string("� ��������������� �������� �������� ������� � ������� ������. ��� ����� ���� ������������ ��� ���������� ������� �������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	elseif num == 8 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 245, -236, 2*245, 326 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-156, -236+15, 2*156, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("�� �������� ����� ������ - ���������� GPG."), true )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 245, -236+35, 245, 236 )
		WidgetSetSprite( picture, "tips3", true, 2 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2, -236+61-5, 226, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("�� ������ ������ ���� ������� ��������, �������� �������� � ����� ����� ����������. ����� �������� ����� ���������� ������ �����������."), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-70, -236+176-5, 226+70, 56 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("����������� ���������� ��������� ������� ������� � ������� �����. ������� ����� ������� �������� ��������� ��� ����������� �� ���� ��-�� ������������� ������� ������. �������������� ������� ������� � �������� �������, ������ �������� ����������� ����������� ��������."), true )
		y = -236
		x = CONFIG.scr_width/2 - 245
	end
	Resume( NewThread( function()
		while y ~= CONFIG.scr_height/2 - 118 do
			if math.abs( CONFIG.scr_height/2 - 118 - y ) < 5 then
				y = CONFIG.scr_height/2 - 118
			else
				y = y + (CONFIG.scr_height/2 - 118 - y) * 0.5
			end
			WidgetSetPos( window, x, y )
			Wait(1)
		end
		Wait(300)
		local done = false
		local __key = GlobalGetKeyReleaseProc()
		GlobalSetKeyReleaseProc( function( key )
			done = true
			GlobalSetKeyReleaseProc( __key )
			return true
		end )
		while not done do
			Wait( 1 )
		end
		DestroyWidget( window )
		Menu.unlock()
		SwitchLighting(true)
		if post_event then
			return post_event()
		end
	end ))
end

function Game.ShowMainTip( num, reveal, post_event )
	if Game.info.no_tutorials then
		return
	end
	local window
	Menu.lock()
	local x, y
	if num == 1 then
		window = CreateWidget( constants.wt_Panel, "TIP", nil, CONFIG.scr_width/2 - 295, -236, 2*295, 396 )
		WidgetSetSprite( window, "window1" )
		WidgetSetFocusable( window, false )
		local picture = CreateWidget( constants.wt_Picture, "TIP", window, CONFIG.scr_width/2 - 255, -236+100, 2*255, 236 )
		WidgetSetSprite( picture, "tips2", true, 0 )
		WidgetSetZ( picture, 1.1 )
		local tip1 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-276, -236+25-5, 2*276, 26 )
		WidgetSetCaptionFont( tip1, "dialogue" )
		WidgetSetCaptionColor( tip1, {1,1,1,1}, false )
		WidgetSetCaption( tip1, dictionary_string("���� ����� ���������"), true )
		local tip2 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-276, -236+248-5, 2*276, 26 )
		WidgetSetCaptionFont( tip2, "dialogue" )
		WidgetSetCaptionColor( tip2, {1,1,1,1}, false )
		WidgetSetCaption( tip2, dictionary_string("��������! ���������� � ���� ������ ���������� �� ����������. ������������� ������ �������� � ������� � ���������� �������� ���������� ����������."), true )
		local tip3 = CreateWidget( constants.wt_Label, "TIP", window, CONFIG.scr_width/2-276, -236+328-5, 2*276, 26 )
		WidgetSetCaptionFont( tip3, "dialogue" )
		WidgetSetCaptionColor( tip3, {1,1,1,1}, false )
		WidgetSetCaption( tip3, dictionary_string("������� ����������� ������ �� ���������������� ���������, ��� ���������� �� ���� ����������� ����. ��� �������� �����, �.�."), true )
		y = -236
		x = CONFIG.scr_width/2 - 295
	end
	Resume( NewThread( function()
		while y ~= CONFIG.scr_height/2 - 200 do
			if math.abs( CONFIG.scr_height/2 - 200 - y ) < 5 then
				y = CONFIG.scr_height/2 - 200
			else
				y = y + (CONFIG.scr_height/2 - 200 - y) * 0.5
			end
			WidgetSetPos( window, x, y )
			Wait(1)
		end
		local done = false
		local __key = GlobalGetKeyReleaseProc()
		GlobalSetKeyReleaseProc( function( key )
			done = true
			GlobalSetKeyReleaseProc( __key )
			return true
		end )
		while not done do
			Wait( 1 )
		end
		DestroyWidget( window )
		Menu.unlock()
		if post_event then
			return post_event()
		end
	end ))
end
------------------------------------------------------------------------------------------------------------------------------------

function Game.SkipCharSelMenu( map )
	if not Game.info then
		Game.info = {}
	end
	if vars.menu_thread then
		StopThread( vars.menu_thread )
		if vars.menu_thread_cleanup then
			vars.menu_thread_cleanup()
		end
		vars.menu_thread = nil
	end
	difficulty = 1
	Loader.startGame( map )
	if Game.custom_map then
		Game.ProgressAchievement( "LEVEL_TESTER", 1 )
	end
end

function Game.ShowCharSelMenu( back_menu, map, back_event )
	if not Game.info then
		Game.info = {}
	end
	if vars.menu_thread then
		StopThread( vars.menu_thread )
		if vars.menu_thread_cleanup then
			vars.menu_thread_cleanup()
		end
		vars.menu_thread = nil
	end

	back_menu:hide()
	Menu.setMainMenuVisible( false )
	local fade_widget = CreateWidget( constants.wt_Widget, "The Fade", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
	--WidgetSetColorBox( fade_widget, {0,0,0,1} )
	--WidgetSetSprite( fade_widget, "pear15charsel-back", true, 0 )
	--WidgetSetSpriteRenderMethod( fade_widget, constants.rsmStretch )
	--WidgetSetSpriteColor( fade_widget, {0, 0, 0, 1} )
	WidgetSetZ( fade_widget, 0.9 )
	local pictures_widget = CreateWidget( constants.wt_Widget, "", nil, 0, 0, 1, 1 )

	local sx, sy = GetCaptionSize( "default", dictionary_string("�������� ���������") )
	local title = CreateWidget( constants.wt_Label, "", fade_widget, CONFIG.scr_width/2 - sx,  20, 1, 1 )
	WidgetSetCaptionFont( title, "default", 2 )
	WidgetSetCaptionColor( title, {1,1,.6,1}, false )
	WidgetSetCaption( title, dictionary_string("�������� ���������") )
	local dwindow1 = CreateWidget( constants.wt_Panel, "", title, 0, 3*CONFIG.scr_height/4, CONFIG.scr_width/2, CONFIG.scr_height/4 )
	WidgetSetSprite( dwindow1, "window1" )
	WidgetSetZ( dwindow1, 1.05 )
	WidgetSetFocusable( dwindow1, false )
	local dwindow2 = CreateWidget( constants.wt_Panel, "", title, CONFIG.scr_width/2, 3*CONFIG.scr_height/4, CONFIG.scr_width/2, CONFIG.scr_height/4 )
	WidgetSetSprite( dwindow2, "window1" )
	WidgetSetZ( dwindow2, 1.05 )
	WidgetSetFocusable( dwindow2, false )
	--[[local bwindow1 = CreateWidget( constants.wt_Panel, "", dwindow1, -64, -64, CONFIG.scr_width/8+64, CONFIG.scr_height+64 )
	WidgetSetSprite( bwindow1, "window2" )
	WidgetSetZ( bwindow1, -0.5 )
	WidgetSetFocusable( bwindow1, false )
	WidgetSetSpriteColor( bwindow1, {47/255, 37/255, 51/255, 1} ) 
	local bwindow2 = CreateWidget( constants.wt_Panel, "", dwindow2, 7*CONFIG.scr_width/8, -64, CONFIG.scr_width/8+64, CONFIG.scr_height+64 )
	WidgetSetSprite( bwindow2, "window2" )
	WidgetSetZ( bwindow2, -0.5 )
	WidgetSetFocusable( bwindow2, false )
	WidgetSetSpriteColor( bwindow2, {47/255, 37/255, 51/255, 1} ) 
	WidgetSetVisible( dwindow2, false )]]

	local description1 = CreateWidget( constants.wt_Label, "", dwindow1, 0, 0, CONFIG.scr_width - 20, CONFIG.scr_height/4 - 20 )
	WidgetSetPos( description1, 10, 10, true )
	WidgetSetCaptionFont( description1, "dialogue" )
	WidgetSetCaptionColor( description1, {1,1,1,1}, false )
	local description2 = CreateWidget( constants.wt_Label, "", dwindow2, 0, 0, CONFIG.scr_width - 20, CONFIG.scr_height/4 - 20 )
	WidgetSetPos( description2, CONFIG.scr_width/2+10, 10, true )
	WidgetSetCaptionFont( description2, "dialogue" )
	WidgetSetCaptionColor( description2, {1,1,1,1}, false )
	WidgetSetVisible( description2, false )
	local picture11 = CreateWidget( constants.wt_Picture, "", pictures_widget, -193, 3*CONFIG.scr_height/4 - 245, 193, 245 )
	WidgetSetSprite( picture11, "pear15charselect2", true, 0 )
	WidgetSetZ( picture11, 1.1 )
	WidgetSetVisible( picture11, false )
	local picture12 = CreateWidget( constants.wt_Picture, "", pictures_widget, -193, 3*CONFIG.scr_height/4 - 245, 193, 245 )
	WidgetSetSprite( picture12, "pear15charselect2", true, 1 )

	WidgetSetZ( picture12, 1.1 )
	WidgetSetVisible( picture12, false )
	local picture21 = CreateWidget( constants.wt_Picture, "", pictures_widget, CONFIG.scr_width, 3*CONFIG.scr_height/4 - 245, 193, 245 )
	WidgetSetSprite( picture21, "pear15charselect2", true, 0 )
	WidgetSetSpriteBlendingMode( picture21, constants.bmSrcA_OneMinusSrcA )
	WidgetSetZ( picture21, 1.1 )
	WidgetSetVisible( picture21, false )
	local picture22 = CreateWidget( constants.wt_Picture, "", pictures_widget, CONFIG.scr_width, 3*CONFIG.scr_height/4 - 245, 193, 245 )
	WidgetSetSprite( picture22, "pear15charselect2", true, 1 )
	WidgetSetZ( picture22, 1.1 )
	WidgetSetVisible( picture22, false )
	WidgetSetSpriteBlendingMode( picture22, constants.bmSrcA_OneMinusSrcA )

	local caption =  string.format( dictionary_string("����� 2: ������� %s"), GetKeyName( CONFIG.key_conf[2].gui_nav_accept ) )
	local sx2, sy2 = GetCaptionSize( "dialogue", caption )
	caption = string.format( dictionary_string("����� 2: ������� /cffff88%s"), GetKeyName( CONFIG.key_conf[2].gui_nav_accept ) )
	local player2 = CreateWidget( constants.wt_Label, "", title, CONFIG.scr_width/2 - sx2/2, 30+2*sy, sx2, sy2 )
	WidgetSetCaptionFont( player2, "dialogue" )
	WidgetSetCaptionColor( player2, {1,1,1,1}, false )
	WidgetSetCaption( player2, caption, true )

	Resume( NewThread( function()
		local y = CONFIG.scr_height / 2 - 32
		local x = 3*CONFIG.scr_width / 8 - 32
		local done = 0
		local can_join = true
		
		Resume( NewThread( function()
			local vis = true
			while can_join do
				vis = not vis
				WidgetSetVisible( player2, vis )
				Wait( 1000 )
			end
		end ))

		local char_appear = function( widget, x1, y1, x2, y2 )
			local x, y = x1, y1
			local done_x = false
			local done_y = false
			while not (done_x and done_y) do
				if math.abs( x2 - x ) > 3 then
					x = x + (x2 - x) * 0.2
				else
					x = x2
					done_x = true
				end
				if math.abs( y2 - y ) > 3 then
					y = y + (y2 - y) * 0.2
				else
					y = y2
					done_y = true
				end
				WidgetSetPos( widget, x, y )
				Wait( 1 )
			end
			done = done + 1
		end

		local char1 = CreateWidget( constants.wt_Picture, "", title, -100, -100, 64, 64 )
		WidgetSetSprite( char1, "pear15charselect", true, 0 )
		WidgetSetZ( char1, 1.05 )
		local char2 = CreateWidget( constants.wt_Picture, "", title, -100, -100, 64, 64 )
		WidgetSetSprite( char2, "pear15charselect", true, 1 )
		WidgetSetZ( char2, 1.05 )
		local pl1 = CreateWidget( constants.wt_Picture, "", title, -100, -100, 64, 64 )
		WidgetSetSprite( pl1, "pear15charselect", true, 2 )
		WidgetSetZ( pl1, 1.1 )
		local pl2 = CreateWidget( constants.wt_Picture, "", title, -100, -100, 64, 64 )
		WidgetSetSprite( pl2, "pear15charselect", true, 3 )
		WidgetSetZ( pl2, 1.1 )
		WidgetSetVisible( pl2, false )

		local t = 0
		local time = GetCurTime()
		local tt = 0
		local glow_thread = NewThread( function()
			while true do
				t = t + GetCurTime() - time
				time = GetCurTime()
				tt = 0.8 + 0.2 * math.cos( t / 100 )
				WidgetSetSpriteColor( pl1, { tt, tt, tt, 1 } )
				WidgetSetSpriteColor( pl2, { tt, tt, tt, 1 } )
				Wait( 1 )
			end
		end )
		Resume( glow_thread )

		Resume( NewThread( char_appear ), char1, CONFIG.scr_width, y, x, y )
		Resume( NewThread( char_appear ), pl1, CONFIG.scr_width, y, x, y )
		Wait( 300 )
		Resume( NewThread( char_appear ), char2, CONFIG.scr_width, y, x + CONFIG.scr_width/4, y )
		Resume( NewThread( char_appear ), pl2, CONFIG.scr_width, y, x + CONFIG.scr_width/4, y )
		while done < 4 do
			Wait( 1 )
		end
	
		RemoveKeyProcessors()
		local param = {
			target_x2 = x + CONFIG.scr_width/4,
			target_x1 = x,
			x1 = x,
			x2 = x + CONFIG.scr_width/4,
			px11 = -193,
			px12 = -193,
			px21 = CONFIG.scr_width,
			px22 = CONFIG.scr_width,
			target_px11 = 0,
			target_px12 = -193,
			target_px21 = CONFIG.scr_width,
			target_px22 = CONFIG.scr_width-193,
			y = y,
			py = 3*CONFIG.scr_height/4 - 245
		}

		local movement_thread = NewThread( function()

			local move_to = function( widget, x, tx, y )
				if math.abs( param[tx] - param[x] ) > 5 then
					param[x] = param[x] + ( param[tx] - param[x] ) * 0.5
				else
					param[x] = param[tx]
				end
				WidgetSetPos( widget, param[x], param[y] )
			end
			while true do
				move_to( pl1, "x1", "target_x1", "y" )
				move_to( pl2, "x2", "target_x2", "y" )
				move_to( picture11, "px11", "target_px11", "py" )
				move_to( picture12, "px12", "target_px12", "py" )
				move_to( picture21, "px21", "target_px21", "py" )
				move_to( picture22, "px22", "target_px22", "py" )
				Wait( 1 )
			end
		end )
		Resume( movement_thread )

		local dstr1 = dictionary_string( "������ ��������./n����� ��������� ������." )
		local dstr1w, dstr1h = 180, 30
		local dstr2 = dictionary_string( "���� ��������./n���� ������./n������-����." )
		local dstr2w, dstr2h = 140, 45
		local frame1 = 0
		local frame2 = 1
		vars.character = "pear15soh"
		local other_character = "pear15unyl"

		WidgetSetPos( description1, CONFIG.scr_width / 4 - dstr1w/2, 7 * CONFIG.scr_height / 8 - dstr1h / 2 )
		WidgetSetPos( description2, 3 * CONFIG.scr_width / 4 - dstr2w/2, 7 * CONFIG.scr_height / 8 - dstr2h / 2 )

		vars.second_player_needed = false

		GlobalSetKeyReleaseProc( function( key )
			if IsConfKey(key, config_keys.left) or IsConfKey(key, config_keys.right) or
			   IsConfKey(key, config_keys.gui_nav_prev) or IsConfKey(key, config_keys.gui_nav_next) then
				param.target_x1, param.target_x2 = param.target_x2, param.target_x1
				param.target_px11, param.target_px12 = param.target_px12, param.target_px11
				param.target_px21, param.target_px22 = param.target_px22, param.target_px21
				dstr1, dstr2 = dstr2, dstr1
				dstr1w, dstr1h, dstr2w, dstr2h = dstr2w, dstr2h, dstr1w, dstr1h
				frame1, frame2 = frame2, frame1
				vars.character, other_character = other_character, vars.character
				WidgetSetCaption( description1, dstr1, true )
				WidgetSetPos( description1, CONFIG.scr_width / 4 - dstr1w/2, 7 * CONFIG.scr_height / 8 - dstr1h / 2 )
				WidgetSetCaption( description2, dstr2, true )
				WidgetSetPos( description2, 3 * CONFIG.scr_width / 4 - dstr2w/2, 7 * CONFIG.scr_height / 8 - dstr2h / 2 )
			elseif (key == CONFIG.key_conf[2].gui_nav_accept or key == CONFIG.key_conf[2].fire) and can_join then
				can_join = false
				Game.info.players = 2
				vars.second_player_needed = true
				WidgetSetVisible( player2, false )
				WidgetSetVisible( description2, true )
				WidgetSetVisible( pl2, true )
				WidgetSetVisible( picture21, true )
				WidgetSetVisible( picture22, true )
				WidgetSetVisible( dwindow2, true )
			elseif isConfigKeyPressed( key, "gui_nav_decline" ) or isConfigKeyPressed( key, "gui_nav_menu" ) then
				StopThread( glow_thread )
				StopThread( movement_thread )
				DestroyWidget( fade_widget )
				DestroyWidget( pictures_widget )
				RestoreKeyProcessors()
				if back_event then 
					back_event()
				else
					back_menu:show()
				end
			elseif ((key ~= CONFIG.key_conf[2].gui_nav_accept or key ~= CONFIG.key_conf[2].fire) or (not can_join)) and 
			(isConfigKeyPressed( key, "gui_nav_accept" ) or isConfigKeyPressed( key, "fire" )) then
				StopThread( glow_thread )
				StopThread( movement_thread )
				can_join = false
				RestoreKeyProcessors()
				DestroyWidget( title )
				Game.ShowDifficultyMenu( function()
					DestroyWidget( fade_widget )
					DestroyWidget( pictures_widget )
					back_event()
				end,
				function() 
					DestroyWidget( fade_widget )
					DestroyWidget( pictures_widget )
					Loader.startGame( map )
					if Game.custom_map then
						Game.ProgressAchievement( "LEVEL_TESTER", 1 )
					end
				end )
			end
		end )
		WidgetSetCaption( description1, dstr1, true )
		WidgetSetCaption( description2, dstr2, true )
		WidgetSetVisible( picture11, true )
		WidgetSetVisible( picture12, true )
	end ))	
end

local const = {}
const.__index = constants
function Game.getROConstants()
	local const_ro = {}
	setmetatable( const_ro, const )
	return const_ro
end

function Game._ShowSolution( back_event )
	RemoveKeyProcessors()
	
	local window1
	local captain
	local solution
	local up, down
	local okay
	local lines
	local scroll = 0
	local sx, sy

	local solution_text = Loader.level.solution or dictionary_string("��� ������ ������� ��� ���� �������. ������ ������ �������� ���� ��������� ���.")
	local okay_text = dictionary_string("OK")

	captain = CreateWidget( constants.wt_Picture, "", nil, 10, CONFIG.scr_height/4, 100, 100 )
	WidgetSetSprite( captain, "portrait-captain", "left" )

	local w1x, w1y = 125, CONFIG.scr_height/4
	local w1w, w1h = CONFIG.scr_width - w1x - 40, CONFIG.scr_height/2
	window1 = CreateWidget( constants.wt_Panel, "", captain, w1x, w1y, w1w, w1h )
	WidgetSetSprite( window1, "window1" )
	WidgetSetFocusable( window1, false )

	_, _, lines = GetMultilineCaptionSize( "dialogue", solution_text, w1w-100, 1 )
	solution = CreateWidget( constants.wt_Label, "", window1, w1x + 20, w1y + 20, w1w - 100, w1h - 60 )
	WidgetSetCaptionFont( solution, "dialogue" )
	WidgetSetCaptionColor( solution, {1,1,1,1}, false, {0,0,0,1} )
	WidgetSetCaption( solution, solution_text, true )
	WidgetSetCaptionScroll( solution, 0 )

	local function scroll_up()
		if scroll > 0 then
			scroll = scroll - 1
			WidgetSetCaptionScroll( solution, scroll )
		end
	end
	
	local function scroll_down()
		if scroll < lines-8 then
			scroll = scroll + 1
			WidgetSetCaptionScroll( solution, scroll )
		end
	end

	up = CreateWidget( constants.wt_Button, "", window1, w1x + w1w - 52, w1y + 20, 32, 32 )
	WidgetSetSprite( up, "editor_misc", true, 7 )
	WidgetSetSpriteRenderMethod( up, constants.rsmStretch )
	WidgetSetLMouseClickProc( up, function()
		scroll_up()
	end )
	WidgetSetZ( up, 1.05 )

	down = CreateWidget( constants.wt_Button, "", window1, w1x + w1w - 52, w1y + w1h - 30 - 20 - 32, 32, 32 )
	WidgetSetSprite( down, "editor_misc", true, 6 )
	WidgetSetSpriteRenderMethod( down, constants.rsmStretch )
	WidgetSetLMouseClickProc( down, function()
		scroll_down()
	end )
	WidgetSetZ( down, 1.05 )

	sx, sy = GetCaptionSize( "default", okay_text )
	okay = CreateWidget( constants.wt_Button, "", window1, w1x + (w1w-sx)/2, w1y+w1h-30, sx, sy )
	WidgetSetCaptionFont( okay, "default" )
	WidgetSetCaptionColor( okay, {1,1,1,1}, false, {0,0,0,1} )
	WidgetSetCaptionColor( okay, {.6,.6,1,1}, true, {0,0,0,1} )
	WidgetSetCaption( okay, okay_text )
	WidgetSetLMouseClickProc( okay, function()
		RestoreKeyProcessors()
		DestroyWidget( captain )
		if back_event then
			back_event()
		end
	end )

	GlobalSetKeyDownProc( function( key )
		if isConfigKeyPressed( key, "gui_nav_next" ) then
			scroll_down()
			WidgetGainFocus( okay )
			return true
		elseif isConfigKeyPressed( key, "gui_nav_prev" ) then
			scroll_up()
			WidgetGainFocus( okay )
			return true
		end
	end )
	WidgetGainFocus( okay )
end

function Game.ShowSolution( back_event )
	RemoveKeyProcessors()

	local window1
	local warning1
	local warning2

	local sx, sy

	local warncapt1 = dictionary_string("��������!")
	local warncapt2 = dictionary_string("���� �� ������ ��������� � �� ������ �������������� ���������� �� ����, ���� ���� ��� ��� �������� ���������� �����, �� ��������� 10 ������ � �� ������� ��������� �������./n/n� ��������� ������ � ��� ��� ���� ����������� �������� � ���� ��������� �������, ����� ����� ������.")

	local w1px, w1py = 20, 20
	local w1w, w1h = 0, 0
	w1w = CONFIG.scr_width / 2
	sx, sy = GetMultilineCaptionSize( "default", warncapt1, w1w-2*w1px, 2 )
	w1h = w1py + sy + 20
	sx, sy = GetMultilineCaptionSize( "dialogue", warncapt2, w1w-2*w1px, 1 )
	w1h = w1h + sy + w1py
	local w1x, w1y = (CONFIG.scr_width-w1w)/2, (CONFIG.scr_height-w1h)/2

	window1 = CreateWidget( constants.wt_Panel, "", nil, w1x, w1y, w1w, w1h )
	WidgetSetSprite( window1, "window1" )
	WidgetSetFocusable( window1, false )
	
	sx, sy = GetCaptionSize( "default", warncapt1 )
	sx, sy = 2 * sx, 2 * sy
	warning1 = CreateWidget( constants.wt_Label, "", window1, w1x + (w1w-sx)/2, w1y + w1py, 1, 1 )
	WidgetSetCaptionFont( warning1, "default", 2 )
	WidgetSetCaptionColor( warning1, {1, .6, .6, 1}, false, {0,0,0,1} )
	WidgetSetCaption( warning1, warncapt1 )

	local y = w1y + w1py + sy + 20
	warning2 = CreateWidget( constants.wt_Label, "", window1, w1x + w1px, y, w1w - 2*w1px, 1 )
	WidgetSetCaptionFont( warning2, "dialogue" )
	WidgetSetCaptionColor( warning2, {1,1,1,1}, false, {0,0,0,1} )
	WidgetSetCaption( warning2, warncapt2, true )

	local thread = NewThread( function()
		Wait( 10000 )
		DestroyWidget( window1 )
		RestoreKeyProcessors()
		Game._ShowSolution( back_event )
	end )

	GlobalSetKeyDownProc( function( key )
--		if not ( isConfigKeyPressed( key, "gui_nav_accept" ) or isConfigKeyPressed( key, "fire" ) ) then
			StopThread( thread )
			DestroyWidget( window1 )
			RestoreKeyProcessors()
			if back_event then
				back_event()
			end
--		end
	end )

	Resume( thread )
end

------------------------------------------------------------------------------------------------------------------------------------

function Game.isGoodProfileName(name)
	return iff(string.match(name, "^[%a%d%-'_ %.�-��-���]+$"), true, false)
end

function Game.CreateProfile( name )
	if not Game.isGoodProfileName(name) then return false end
	
	Game.profile = {
		name = name,
		achievement_progress = { },
		info_pages = { }
	}
	Saver:saveProfile()
	SaveConfig()
	return true
end

function Game.GetConfigName()
	return iff( Game.profile.default, "default.lua", string.format( "profile_%s.lua", remove_cyrillic(Game.profile.name) ) )
end

Game.profile = {
	name = "Anonya",
	default = true,
	achievement_progress = { },
	info_pages = { },
}

local _SaveConfig = SaveConfig
SaveConfig = function( default, filename )
	if default and type( default ) == 'boolean' then
		_SaveConfig( filename )
	else
		_SaveConfig( Game.GetConfigName() )
	end
end

local _dofile = dofile
dofile = function( file )
	if string.match( file, "^"..protect_string(build_relative_path(constants.path_config)) ) then

		return __dofile( file )
	else
		return _dofile( file )
	end
end

LoadProfileConfig = function(...)
	local r,g,b = CONFIG.backcolor_r, CONFIG.backcolor_g, CONFIG.backcolor_b
	local file = build_relative_path(constants.path_config..Game.GetConfigName())
	local test = io.open(file, "r")
	if test then
		test:close()
		local sbox = { LoadConfig = LoadConfig, constants = Game.getROConstants(), keys = keys }
		local lfile = __loadfile( file )
		if type( lfile ) == 'function' then
			setfenv( lfile, sbox )
			local success, result = pcall( lfile )
			CONFIG = deep_copy( sbox.CONFIG )
		end
	else
		LoadDefaultConfig()
	end
	CONFIG.backcolor_r = r;
	CONFIG.backcolor_g = g;
	CONFIG.backcolor_b = b;
	LoadConfig( unpack(arg) )
	SetMasterSoundVolume( CONFIG.volume )
	SetMasterSFXVolume( CONFIG.volume_sound )
	SetMasterMusicVolume( CONFIG.volume_music )
end

local default_extra =
[[--Deleting any value will set it to default

CONFIG_PARTICLES =
{
	[constants.pIntensity] = 1.0;
	[constants.pRelativeSpeed] = 0.2;
	[constants.pUsePhysic] = 1;
	[constants.pIncludeOneSided] = 1;
	[constants.pIncludePolygons] = 0;
	[constants.pMaxSimpleParticles] = 4096;
	[constants.pMaxPhysParticles] = 1024;
	[constants.pMaxWeatherParticles] = 2048;
}

CONFIG_EXTRA =
{
	UseFlares = 1;
	ClassicPhysics = 0;
	DamageFlash = 1;
}

LoadParticlesConfig( CONFIG_PARTICLES )
]]

function Game.LoadExtraConfig()
	local file = build_relative_path( constants.path_config .. "extra.lua" )
	local test = io.open( file, "r" )
	local lsuccess = false
	if test then
		test:close()
		local sbox = { LoadParticlesConfig = LoadParticlesConfig, constants = Game.getROConstants() }
		local lfile = __loadfile( file )
		if type( lfile ) == 'function' then
			setfenv( lfile, sbox )
			local success, result = pcall( lfile )
			CONFIG_PARTICLES = deep_copy( sbox.CONFIG_PARTICLES )
			CONFIG_EXTRA = deep_copy( sbox.CONFIG_EXTRA )
			lsuccess = true
		end
	end
	if not lsuccess then
		loadstring(default_extra)()
		test = io.open( file, "w" )
		test:write( default_extra )
		test:close()
	end
end

-------------------------------------------------------------------------------------------------------------------------------------

function Game.AddCleanup( cleanup )
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
end

function Game.RemoveCleanup( cleanup )
	if not Game.cleanupFunctions then
		return
	end
	Game.cleanupFunctions[ cleanup ] = false
end

function Game.fade_out()
	local x, y = GetCamPos()
	if not mapvar.tmp.fade_widget then
		SwitchLighting( false )
		mapvar.tmp.fade_widget = CreateWidget( constants.wt_Widget, "", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
		Game.AddCleanup( function() Game.reset_fade() end )
		WidgetSetColorBox( mapvar.tmp.fade_widget, { 0, 0, 0, 1 } )
		return
	else
		mapvar.tmp.fade_complete = false
		WidgetSetVisible( mapvar.tmp.fade_widget, true )
		Resume( NewMapThread( function()
			local time = Loader.time
			local dt = 0
			local a = 0
			local lights = true
			while a < 1 do
				dt = Loader.time - time
				time = Loader.time
				if a > 0.3 and lights then
					lights = false
					SwitchLighting( false )
				end
				a = math.min( 1, a + 0.1 * dt / 100 )
				if mapvar.tmp.fade_widget then WidgetSetColorBox( mapvar.tmp.fade_widget, { 0, 0, 0, a } ) end
				if mapvar.tmp.fade_widget_text then
					for k, v in pairs( mapvar.tmp.fade_widget_text ) do
						WidgetSetCaptionColor( v, { 1, 1, 1, a }, false )
					end
				end
				Wait( 1 )
			end
			mapvar.tmp.fade_complete = true
		end))
	end
end

function Game.fade_in( instant, event )
	mapvar.tmp.fade_complete = false
	if instant then
		WidgetSetVisible( mapvar.tmp.fade_widget, false )
		mapvar.tmp.fade_complete = true
	else
		Resume( NewMapThread( function()
				local time = Loader.time
				local dt = 0
				local a = 1
				local lights = false
				while a > 0 do
					dt = Loader.time - time
					time = Loader.time
					if a < 0.3 and not lights then
						lights = true
						SwitchLighting( true )
					end
					a = math.max( 0, a - 0.1 * dt / 100 )
					if mapvar.tmp.fade_widget then WidgetSetColorBox( mapvar.tmp.fade_widget, { 0, 0, 0, a } ) end
					if mapvar.tmp.fade_widget_text then
						for k, v in pairs( mapvar.tmp.fade_widget_text ) do
							WidgetSetCaptionColor( v, { 1, 1, 1, a }, false )
						end
					end
					Wait( 1 )
				end
				mapvar.tmp.fade_complete = true
				if event then
					FireEvent( event )
				end
				WidgetSetVisible( mapvar.tmp.fade_widget, false )
			end))
	end
end

function Game.reset_fade()
	mapvar.tmp.fade_complete = true
	SwitchLighting( true )
	if mapvar.tmp.fade_widget_text then
		for k, v in pairs( mapvar.tmp.fade_widget_text ) do
			DestroyWidget( v )
		end
		mapvar.tmp.fade_widget_text = nil
	end
	if mapvar.tmp.fade_widget then
		DestroyWidget( mapvar.tmp.fade_widget ) 
		mapvar.tmp.fade_widget = nil
	end
end

function Game.GetCharacters()
	local char1 = GetPlayerCharacter( 1 )
	local char2 = GetPlayerCharacter( 2 )
	local function talk_function( ud )
		return function()
			if not mapvar.tmp.talk then
				mapvar.tmp.talk = { [ud] = true }
			else
				mapvar.tmp.talk[ud] = true
			end

		end
	end
	if char2 and mapvar.actors[1].info.character == "pear15unyl" then
		return char2, char1, talk_function(char2), talk_function(char1)
	end
	return char1, char2, talk_function(char1), talk_function(char2 or char1)
end

function Game.MoveWithAccUntilX( ud, acc, x, event )
	ud:acc( acc )
	SetObjProcessor( ud, function( this )
		local pos = this:aabb().p
		if (acc.x < 0 and pos.x <= x) or (acc.x > 0 and pos.x >= x) then
			this:acc( { x = 0, y = 0 } )
			SetObjProcessor( ud, function() end )
			if event then
				if type( event ) == 'function' then
					event()
				else
					FireEvent( event )
				end
			end
		end
	end )
end

function Game.SpecialMoveWithAccUntilX( ud, acc, x, event )
	ud:acc( acc )
	SetObjProcessor( ud, function( this )
		local pos = this:aabb().p
		if (acc.x < 0 and pos.x <= x) or (acc.x > 0 and pos.x >= x) then
			this:acc( { x = 0, y = 0 } )
			if event then
				event()
			end
		end
	end )
end

function Game.void( char )
	local num = 1
	if char == GetPlayerCharacter(2) then
		num  = 2
	end
	cam_dont_follow[num] = true
	voided[num] = true
	if not Game.mapvar.actors[2] then
		SetCamAttachedObj( 0 )
	end
end

function Game.weapon_weakness( tab )
	return function( obj )
		local dt = ObjectPopInt( obj )
		for k, v in pairs( tab ) do
			if k == dt then
				local dmg = ObjectPopInt( obj )
				ObjectPushInt( obj, math.floor( v * dmg ) )
				break;
			end
		end
	end
end

function Game.touch_push( this, char )
	if char:is_invincible() or char:health() <= 0 then
		return
	end
	local o1 = char:aabb()
	local o2 = this:aabb()
	local diff = { x = o2.p.x - o1.p.x, y = o2.p.y - o1.p.y }
	local l = math.sqrt( diff.x * diff.x + diff.y * diff.y )
	diff.x = 8 * diff.x / l
	diff.y = 8 * diff.y / l
	if o1.p.y <= o2.p.y + o2.H then
		diff.y = math.min( -4, diff.y )
	end
	char:vel( diff )
end

function Game.turret_destroyed( obj )
	local lpd = obj:last_player_damage()
	if lpd == 1 or lpd == 2 then
		if ObjectOnScreen( obj, 3 * 640 ) then
			mapvar.tmp.turret_destroyed = true
		end
	end
end

function Game.ShowDifficultyMenu( cancel_event, event )
	RemoveKeyProcessors()

	local titlc = dictionary_string("�������� ������� ���������")
	local diff1 = dictionary_string("������")
	local diff2 = dictionary_string("�������")
	local diff3 = dictionary_string("��ƨ���")
	local desc1 = dictionary_string("������� ������/n����������� ������ ������ ����� ��������/n�������� �� ��� ��������")
	local desc2 = dictionary_string("���������� ���������� ������/n�������� �������������� ����������� �� �������/n�������� ��� ��������")
	local desc3 = dictionary_string("������������ ���������� ������/n����� ����� ����� �������������� �����������/n�������� ��� ��������")

	local sx, sy = 0, 0
	local w1px, w1py = 20, 20
	local w1w, w1h = 0, w1py
	sx, sy = GetCaptionSize( "default", diff1 )
	w1w = math.max( w1w, sx )
	w1h = w1h + sy + 10
	sx, sy = GetCaptionSize( "default", diff2 )
	w1w = math.max( w1w, sx )
	w1h = w1h + sy + 10
	sx, sy = GetCaptionSize( "default", diff3 )
	w1w = math.max( w1w, sx )
	w1h = w1h + sy + w1py
	w1w = w1w + 2*w1px
	local w1x, w1y = (CONFIG.scr_width - w1w)/2, (CONFIG.scr_height - w1h)/2

	local w2x, w2y = 0, math.max( w1y + w1h + 20, 3 * CONFIG.scr_height / 4 )
	local w2w, w2h = CONFIG.scr_width, CONFIG.scr_height - w2y

	local window1 = CreateWidget( constants.wt_Panel, "", nil, w1x, w1y, w1w, w1h )
	WidgetSetSprite( window1, "window1" )
	WidgetSetFocusable( window1, false )

	local window2 = CreateWidget( constants.wt_Panel, "", window1, w2x, w2y, w2w, w2h )
	WidgetSetSprite( window2, "window1" )
	WidgetSetFocusable( window2, false )

	local description = CreateWidget( constants.wt_Label, "", window2, w2x, w2y, 1, 1 )
	WidgetSetCaptionFont( description, "dialogue" )
	WidgetSetCaptionColor( description, {1, 1, 1, 1}, false )

	sx, sy = GetCaptionSize( "default", titlc )
	sx, sy = 2 * sx, 2 * sy
	local tx, ty = (CONFIG.scr_width-sx)/2, CONFIG.scr_height/8 - sy/2
	local title = CreateWidget( constants.wt_Label, "", window1, tx, ty, 1, 1 )
	WidgetSetCaptionFont( title, "default", 2 )
	WidgetSetCaptionColor( title, {1,1,.6,1}, false )
	WidgetSetCaption( title, titlc )

	local function create_diff( x, y, w, h, caption, desc, diff )
		local wd = CreateWidget( constants.wt_Button, "", window1, x, y, w, h )
		WidgetSetCaptionFont( wd, "default" )
		WidgetSetCaption( wd, caption )
		WidgetSetCaptionColor( wd, {1,1,1,1}, false, {0,0,0,1} )
		WidgetSetCaptionColor( wd, {.6,.6,1,1}, true, {0,0,0,1} )
		WidgetSetLMouseClickProc( wd, function()
			difficulty = diff
			DestroyWidget( window1 )
			RestoreKeyProcessors()
			if event then
				event()
			end
		end )
		WidgetSetFocusProc( wd, function()
			local strs = explode( desc, "/n" )
			local sx, sy = 0, 0
			local w, h = 0, 0
			for k, v in ipairs( strs ) do
				sx, sy = GetCaptionSize( "dialogue", v )
				w = math.max( sx, w )
			end
			_, h = GetMultilineCaptionSize( "dialogue", desc, CONFIG.scr_width, 1 ) 
			WidgetSetSize( description, 1000, h )
			WidgetSetPos( description, w2x + (w2w-w)/2, w2y + (w2h-h)/2 )
			WidgetSetCaption( description, desc, true )
		end )
		if diff == 1 then
			WidgetGainFocus( wd )
		end
	end

	sx, sy = GetCaptionSize( "default", diff1 )
	create_diff( w1x + w1px, w1y + w1py            , w1w-20, sy, diff1, desc1, 0.5 )
	create_diff( w1x + w1px, w1y + w1py + 10 +   sy, w1w-20, sy, diff2, desc2,   1 )
	create_diff( w1x + w1px, w1y + w1py + 20 + 2*sy, w1w-20, sy, diff3, desc3,   2 )

	GlobalSetKeyDownProc( function( key )
		if isConfigKeyPressed( key, "gui_nav_cancel" ) or isConfigKeyPressed( key, "gui_nav_menu" ) then
			DestroyWidget( window1 )
			RestoreKeyProcessors()
			if cancel_event then
				cancel_event()
			end
		end
	end )
end

--------------------------------------------------------------------------------------------------------------------------

function Game.LoadLastProfile()
	local lp = io.open( constants.path_config..".last_profile", "r" )
	if lp then
		local name = lp:read()
		Saver:loadProfile( name )
		LoadProfileConfig()
		Menu.resetAllKeyConfigButtonName()
		lp:close()
	else
		LoadDefaultConfig()
		Menu.resetAllKeyConfigButtonName()
	end
end

function Game.SaveLastProfile()
	if Game.profile.default then
		return
	end
	local name = Game.profile.name
	local file = io.open( constants.path_config..".last_profile", "w" )
	file:write( name )
	file:close()
end

function Game.GiveTutorialBonus()
	Info.RevealEnemy( "CAKE" )
	--function Achievements.Message( id, title, sprite, frame, text )
	Achievements.Message( "CAEK", "�� �������� ����!", "pear15-cake", 0, "� ����� ���." )
	Game.profile.achievement_progress["TUTORIAL_REMINDER"] = 1
	Saver:saveProfile()
end

--------------------------------------------------------------------------------------------------------------------------

local blinking_objects = {}

function Game.FlashObject( ud, color, tme, even_dead )
		local color = color or {0.3,0.3,0.3,1}
		local tme = tme or 100
		if not even_dead and ud:health() <= 0 then
			return
		end
		if (not CONFIG_EXTRA) or (not (CONFIG_EXTRA.DamageFlash == 1)) then
			return
		end
		if not blinking_objects[ud] then
			local t = {}
			t.old_color = ud:sprite_color()
			t.old_mode = ud:sprite_blendingMode()
			t.thread = NewMapThread( function()
				Wait(tme)
				if ud:object_present() then
					ud:sprite_color(t.old_color)
					ud:sprite_blendingMode(t.old_mode)
				end
				blinking_objects[ud] = nil
			end )
			ud:sprite_color( color )
			ud:sprite_blendingMode( constants.bmBright )
			blinking_objects[ud] = t
			Resume( t.thread )
		else
			local t = blinking_objects[ud]
			StopThread( t.thread )
			t.thread = NewMapThread( function()
				Wait(tme)
				if ud:object_present() then
					ud:sprite_color(t.old_color)
					ud:sprite_blendingMode(t.old_mode)
				end
				blinking_objects[ud] = nil
			end )
			ud:sprite_color( color )
			ud:sprite_blendingMode( constants.bmBright )
			Resume( t.thread )
		end
end

--------------------------------------------------------------------------------------------------------------------------

local random_names = 
{
	["RU"] =
	{
		"�����", "���-���", "�������", "����-���", "����������-���", "����-���", "����-���", "�������-���",
		"�����-���", "����-���", "������", "�����", "��������", "����i�", "�����", "��������", "��������",
		"���������-���", "QRBG121-���", "�������", "����������", "����-���", "����-���", "�����-���", "�����",
		"���� ������", "��������", "�����"
	},
	["EN"] =
	{
		"Anonymous"
	}
}

function Game.RandomName()
	local tab = random_names[language] or lang_random_names or { "Anonymous" }
	return tab[math.random(1, #tab)]
end

--------------------------------------------------------------------------------------------------------------------------

Console.addSafeConsoleCommand( "restart", "Restarts current level.", function()
	Loader.restartGame()
	Console.toggle()
end )

Console.addSafeConsoleCommand( "allweapons", "(cheat) Gives access to all weapons.", function()
	LockScore()
	mapvar.weapons = { true, true, true, true }
end )

Console.addSafeConsoleCommand( "giveweapon", "(cheat) Gives access to a weapon by number.", function(num)
	LockScore()
	local num = string.match( num, "%d+" )
	if num then num = tonumber(num) end
	if num then
		mapvar.weapons[num] = true
	end
end )

Console.addSafeConsoleCommand( "giveammo", "(cheat) Gives specified amount of ammo (or 100) to a specified player (or everyone).", function(num)
	LockScore()
	local amm, play = string.match( num, "(%d+)%s+(%d+)" )
	if not amm then
		amm = string.match( num, "%d+" )
	end
	if amm then amm = tonumber( amm ) end
	if play then play = tonumber( play ) end
	local o
	if not play then
		for k, v in ipairs( mapvar.actors ) do
			o = GetPlayerCharacter( k )
			if o then o:ammo( o:ammo() + (amm or 100 ) ) end
		end
	else
		o = GetPlayerCharacter( play )
		if o then o:ammo( o:ammo() + (amm or 100 ) ) end
	end
end )

Console.addSafeConsoleCommand( "givehealth", "(cheat) Gives specified amount of health (or 100) to a specified player (or everyone).", function(num)
	LockScore()
	local amm, play = string.match( num, "(%d+)%s+(%d+)" )
	if not amm then
		amm = string.match( num, "%d+" )
	end
	if amm then amm = tonumber( amm ) end
	if play then play = tonumber( play ) end
	local o
	if not play then
		for k, v in ipairs( mapvar.actors ) do
			o = GetPlayerCharacter( k )
			if o then o:health( math.max( o:health_max(), o:health() + (amm or 100 ) ) ) end
		end
	else
		o = GetPlayerCharacter( play )
		if o then o:health( math.max( o:health_max(), o:health() + (amm or 100 ) ) ) end
	end
end )

Console.addSafeConsoleCommand( "givelives", "(cheat) Gives specified amount of lives (or 1) to a specified player (or everyone).", function(num)
	LockScore()
	local amm, play = string.match( num, "(%d+)%s+(%d+)" )
	if not amm then
		amm = string.match( num, "%d+" )
	end
	if amm then amm = tonumber( amm ) end
	if play then play = tonumber( play ) end
	local o
	if not play then
		for k, v in ipairs( mapvar.actors ) do
			mapvar.actors[k].info.lives = mapvar.actors[k].info.lives + (amm or 1)
		end
	else
		mapvar.actors[play].info.lives = mapvar.actors[play].info.lives + (amm or 1)
	end
end )

local godmode = false
Console.addSafeConsoleCommand( "god", "(cheat) Turns player characters [almost] invincible.", function(num)
	LockScore()
	godmode = not godmode
	local o
	if godmode then
		for k, v in ipairs( mapvar.actors ) do
			o = GetPlayerCharacter( k )
			o:is_invincible( true )
			o:recovery_time( 0 )
		end
	else
		for k, v in ipairs( mapvar.actors ) do
			o = GetPlayerCharacter( k )
			o:is_invincible( false )
		end
	end
end )

Console.addSafeConsoleCommand( "gotolevel", "(cheat) Ends current level and allows you to progress to a specified one.", function(num)
	LockScore()
	local levels = 
	{
		["0"] = "pear15intro",
		["1"] = "pear15sewers",
		["2"] = "pear15house_opening",
		["3"] = "pear15house_boss",
		["4"] = "pear15fm",
		["5"] = "pear15fm_boss",
		["6"] = "pear15mountains",
		["7a"] = "pear15lab",
		["8a"] = "pear15that_guy",
		["7b"] = "pear15tower",
		["8b"] = "pear15winged_doom",
		["7"] = "pear15ending"
	}
	local num = string.match( num, "%S+" )
	if not levels[num] then
		return "Unknown level."
	end
	Game.ShowLevelResults( levels[num] )
	Console.toggle()
end )

Console.addSafeConsoleCommand( "teleport", "(cheat) Teleports all players to mouse cursor.", function(num)
	LockScore()
	local x, y = GetMousePos()
	for k, v in ipairs( mapvar.actors ) do
		o = GetPlayerCharacter( k )
		if o then 
			o:vel{ x = 0, y = 0 }
			SetObjPos( o, x, y ) 
		end
	end
end )

Console.addSafeConsoleCommand( "knoweverything", "(cheat) Reveal all info about items and enemies.", function(num)
	LockScore()
	for k, v in pairs( Info.enemies ) do
		Info.RevealEnemy( k )
	end
	for k, v in pairs( Info.items ) do
		Info.RevealItem( k )
	end
	for k, v in pairs( Info.weapons ) do
		Info.RevealWeapon( k )
	end
end )

Console.addSafeConsoleCommand( "bind", "bind [player] [command name] [value]", function(param)
	local num, action, key = string.match( param, "(%S+)%s+(%S+)%s+(%S+)" )
	if not num then
		return "Bad syntax."
	end
	num = tonumber(num)
	if (not num) or (num > 4) or (num < 0) then
		return "Bad player number."
	end
	if (not keys[key]) then
		return string.format("Unknown key '%s'.", key)
	end
	CONFIG.key_conf[action] = keys[key]
	LoadConfig()
	SaveConfig()
end )

