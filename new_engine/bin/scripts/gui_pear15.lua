local GUIc = {}
GUIc.__index = GUIc

function GUIc.create()
	g = {}
	setmetatable(g, GUIc)
	g.thread = NewThread(GUIc.update)
	g.visible = false
	return g
end

function GUIc:show()

end

function GUIc:hide()

end

function GUIc:destroy()

end

function GUIc:init()
	
end

function GUIc:update()

end

function GUIc:showGameOver()

end

return GUIc