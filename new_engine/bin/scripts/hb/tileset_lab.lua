local t = {}

function t.BuildTrackPart( track, part, context )
	local ud
	local x = context.x
	local y = 0
	local TheVegetable = context.TheVegetable
	local cx = x
	local w
	local anim
	if part == "=" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,2 do
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )			
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			CreateSpriteCenter( "green_flare", cx + 110, y + 335 )
			CreateSpriteCenter( "red_flare", cx + 110, y + 80 )
			cx = cx + w
		end

		return 2 * w

	elseif part == "1" or part == "!" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,2 do
			if i == 2 then
				ud = GetObjectUserdata( CreateItem( "vegetable_hb1", cx, y+350+16 ) )
				SetObjPos( ud, cx+w/2, y+350+16 )
				TheVegetable( ud, iff( part == "!", -96, -32 ) )
			end
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			CreateSpriteCenter( "green_flare", cx + 110, y + 335 )
			CreateSpriteCenter( "red_flare", cx + 110, y + 80 )
			cx = cx + w
		end

		return 2 * w
		
	elseif part == "2"  or part == "@" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,2 do
			if i == 2 then
				ud = GetObjectUserdata( CreateItem( "vegetable_hb2", cx, y+350+32 ) )
				SetObjPos( ud, cx+w/2, y+350+32 )
				TheVegetable( ud, iff( part == "@", -96, -32 ) )
			end
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			CreateSpriteCenter( "green_flare", cx + 110, y + 335 )
			CreateSpriteCenter( "red_flare", cx + 110, y + 80 )
			cx = cx + w
		end

		return 2 * w


	elseif part == "3" or part == "#" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,2 do
			if i == 2 then
				ud = GetObjectUserdata( CreateItem( "vegetable_hb3", cx, y+350+48 ) )
				SetObjPos( ud, cx+w/2, y+350+48 )
				TheVegetable( ud, iff( part == "#", -96, -32 ) )
			end
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			CreateSpriteCenter( "green_flare", cx + 110, y + 335 )
			CreateSpriteCenter( "red_flare", cx + 110, y + 80 )
			cx = cx + w
		end

		return 2 * w


	elseif part == "[" then
		
		table.insert( track.tiles, {x, "["} )

		w = t.parts[2][1]
		anim = t.parts[2][2]

		ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
		SetObjAnim( ud, anim, false )
		SetObjPos( ud, cx+w/2, 240 )
		CreateSpriteCenter( "green_flare", cx + 110, y + 335 )
		CreateSpriteCenter( "red_flare", cx + 110, y + 80 )
		cx = cx + w

		return w

	elseif part == " " then

		table.insert( track.tiles, {x, " "} )

		w = 64

		return 3 * w

	elseif part == "]" then

		table.insert( track.tiles, {x, "]"} )

		w = t.parts[4][1]
		anim = t.parts[4][2]

		ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
		SetObjAnim( ud, anim, false )
		SetObjPos( ud, cx+w/2, 240 )
		CreateSpriteCenter( "green_flare", cx + 110-64, y + 335 )
		CreateSpriteCenter( "red_flare", cx + 110-64, y + 80 )
		CreateSpriteCenter( "green_flare", cx + 110+64, y + 335 )
		CreateSpriteCenter( "red_flare", cx + 110+64, y + 80 )
		cx = cx + w

		return w
	
	elseif part == "~" then
		if not track.finish_line then
			track.finish_line = x
		end
		table.insert( track.tiles, {x, "~"} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,3 do
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			CreateSpriteCenter( "green_flare", cx + 110, y + 335 )
			CreateSpriteCenter( "red_flare", cx + 110, y + 80 )
			cx = cx + w
		end

		return 3 * w

	end
	return 0
end

function t.GetFloorZ( x, y, context )
	local tile, start = context.GetTileType( x )
	if tile == "=" or tile == "~" then
		return 0
	elseif tile == " " then
		return 1024
	elseif tile == "[" then
		return iff( x - start - 138 > (y - t.top), 1024, 0 )
	elseif tile == "]" then
		return iff( x - start - 11 > (y - t.top), 0, 1024 )
	end
end

t.top = 352
t.bottom = 408

t.parts =
{
	{ 128, "floor" },
	{ 7*32, "pit_left" },
	{},
	{ 6*32, "pit_right" }
}

t.sprite = "hb_pear15_lab";

t.InitTrack = function( context )
	CONFIG.backcolor_r = 38/255
	CONFIG.backcolor_g = 41/255
	CONFIG.backcolor_b = 50/255
	LoadConfig()
end

return t
