_stack = {}

function push_pause( state )
	if not pause_stack then
		pause_stack = {}
	end
	table.insert( pause_stack, GamePaused() )
	TogglePause( state )
end

function pop_pause()
	if not pause_stack then
		pause_stack = {}
	end
	TogglePause( (pause_stack[#pause_stack] or false) )
	table.remove( pause_stack, #pause_stack )
end

function dictionary_string(string)
	if not dictionary or not dictionary[string] then
		return string
	end
	return dictionary[string]
end

function iff( condition, a, b )
	if condition then
		return a
	else
		return b
	end
end

function diff_iff( condition, object )
	if condition then
		return object
	else
		return { constants.ObjNone }
	end
end

function child( parent )
	local a = {}
	local a_mt = { __index = a }
	
	function a.bareCreate()
		local ni = {}
		setmetatable( ni, a_mt )
		return ni
	end
	
	function a.superCreate( ... )
		local b = parent.create( unpack(arg) )
		setmetatable( b, a_mt ) 
		return b
	end
	
	function a:super( func, ... )
		local l_arg = arg
		table.remove(l_arg, 1)
		return parent[func]( parent, l_arg )
	end
	
	if parent then
		setmetatable( a, { __index = parent } )
		a.super = parent
	end
	
	return a
end

function shallow_copy( source )
	if not source then return {} end
	local ret = {}
	for key, value in pairs(source) do
		ret[key] = value
	end
	return ret
end

function deep_copy( source )
	if not source then return {} end
	assert(type(source) == 'table', debug.traceback())
	local ret = {}
	for key, value in pairs(source) do
		if type(value) == 'table' then
			ret[key] = deep_copy(value)
		else
			ret[key] = value
		end
	end
	-- mt = getmetatable(source)
	-- setmetatable(ret, mt)
	return ret
end

-- Merges 'src' table into 'dst' table deeply.
function deep_merge(dst, src)
	assert(dst)
	assert(type(dst) == 'table')
	for k,v in pairs(src) do
		if type(v) == 'table' then
			if not dst[k] then
				dst[k] = {}
			end
			deep_merge(dst[k], v)
		else
			dst[k] = v
		end
	end
end

function is_char( char )
	if not char then return false end
	if #char == 1 then
		return true
	end
	return false
end

function random( tab )
	if tab then
		if #tab == 0 then
			ret = {}
			for key, value in pairs(tab) do
				table.insert(ret, value)
			end
		else
			ret = tab
		end
		return ret[math.random(1, #ret)]
	end
	return nil
end

function protected_string(str)
	return string.gsub(str, "[%.%!%*%?%-%[%]%(%)%^%$%%]", "%%%1")
end

local function nextid()
	WidgetContainer.last_id = WidgetContainer.last_id + 1
	return WidgetContainer.last_id - 1
end

--------------------------------------------------------------------------

local waiting_events = {}
local map_waiting_events = {}

function WaitForEvent( event_id )
	local thread = coroutine.running()
	if not thread then
		Log( string.format("Trying to stop main thread and wait for event %s!", tostring( event_id )) )
		return
	end
	local map_thread = false
	for k, v in ipairs( mapvar.threads or {} ) do
		if v == thread then
			map_thread = true
			break
		end
	end
	if map_thread then
		map_waiting_events[thread] = event_id
	else
		waiting_events[thread] = event_id
	end
	coroutine.yield()
end

function FireEvent( event_id )
	for k, v in pairs( waiting_events ) do
		if v == event_id then
			waiting_events[k] = nil
			Resume( k )
		end
	end
	for k, v in pairs( map_waiting_events ) do
		if v == event_id then
			map_waiting_events[k] = nil
			Resume( k )
		end
	end
end

function NewMapThread( thread )
	if not mapvar then
		return NewThread( thread, true )
	else
		if not mapvar.threads then mapvar.threads = {} end
		local ret = NewThread( thread, true )
		table.insert( mapvar.threads, ret )
		return ret
	end
end

function NewMapThreadWithCleanup( thread, cleanup )
	if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	return NewMapThread( function( ... ) 
		thread( unpack( arg ) )
		Game.cleanupFunction[ cleanup ] = false
	end )
end

function StopMapThreads()
	if not mapvar or not mapvar.threads then return end
	for key, value in pairs( mapvar.threads ) do
		StopThread( value )
	end
	mapvar.threads = {}
	map_waiting_threads = {}
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- WidgetContainer

WidgetContainer = {}
WidgetContainer.__index = WidgetContainer
WidgetContainer.w2c = {}
WidgetContainer.w2id = {}
WidgetContainer.last_id = 1

function WidgetContainer.create(id, visibility, on_show)
	local a = {}
	setmetatable(a,WidgetContainer)
	a.elements = {}
	a.pointers = {}
	a.onfocus = {}
	a.onunfocus = {}
	a.params = {}
	a.onleft = {}
	a.onright = {}
	a.onkey = {}
	a.visible = {}
	a.id = id
	a.visibility = visibility or false
	a.on_show = on_show
	return a
end

function WidgetContainer:addLabel(WidgetTable)
	local wt = WidgetTable
	wt.text = wt.text or "NULL"
	if dictionary and dictionary[wt.text] then wt.text = dictionary[wt.text] end
	wt.color = wt.color or {1, 1, 1, 1}
	wt.x = wt.x or 0
	wt.y = wt.y or 0
	wt.id = wt.id or "WIDGET" ..nextid()
	wt.font = wt.font or "default"
	wt.visible = wt.visible or true
	if wt.center and wt.text then
		local dx, dy = GetCaptionSize(wt.font, wt.text)
		--Log(wt.text.." "..dx.." "..dy)
		wt.x = wt.center[1]-math.floor(dx/2)
		wt.y = wt.center[2]-math.floor(dy/2)
		--Log(wt.text.." "..wt.x.." "..wt.y)
	end
	local widget = CreateWidget(constants.wt_Label, wt.id, nil, wt.x, wt.y, GetCaptionSize(wt.font, wt.text))
	WidgetSetCaption(widget, wt.text)
	WidgetSetCaptionColor(widget, wt.color, false)
	WidgetSetCaptionFont(widget, wt.font)
	WidgetSetVisible(widget, self.visibility and wt.visible)

	if self.elements[wt.id] then DestroyWidget(self.elements[wt.id]) end
	self.elements[wt.id] = widget
	self.visible[wt.id] = wt.visible
	WidgetContainer.w2c[wt.id] = self
	WidgetContainer.w2id[widget] = wt.id

	return wt.id
end

function WidgetContainer:addPicture(WidgetTable)
	local wt = WidgetTable
	wt.sprite = wt.sprite or "wakaba-widget"
	wt.anim = wt.anim or "idle"
	wt.visible = iff(wt.invisible, false, true)
	wt.x = wt.x or 0
	wt.y = wt.y or 0
	wt.w = wt.w or 28
	wt.h = wt.h or 28
	wt.id = wt.id or "WIDGET" ..nextid()
	local widget = CreateWidget(constants.wt_Picture, wt.id, nil, wt.x, wt.y, wt.w, wt.h)
	WidgetSetSprite(widget, wt.sprite)
	WidgetSetAnim(widget, wt.anim)
	WidgetSetLMouseClickProc(widget, WidgetContainer.onLeft)
	WidgetSetRMouseClickProc(widget, WidgetContainer.onRight)
	WidgetSetFocusProc(widget, WidgetContainer.onFocus)
	WidgetSetUnFocusProc(widget, WidgetContainer.onUnfocus)
	WidgetSetKeyPressProc(widget, WidgetContainer.onKey)
	WidgetSetVisible(widget, (self.visibility and wt.visible))

	if self.elements[wt.id] then DestroyWidget(self.elements[wt.id]) end
	self.elements[wt.id] = widget
	self.onfocus[wt.id] = wt.on_focus
	self.onunfocus[wt.id] = wt.on_unfocus
	self.onleft[wt.id] = wt.on_left
	self.onright[wt.id] = wt.on_right
	self.onkey[wt.id] = wt.on_key
	self.params[wt.id] = wt.params
	self.visible[wt.id] = wt.visible
	WidgetContainer.w2c[wt.id] = self
	WidgetContainer.w2id[widget] = wt.id

	return wt.id
end

function WidgetContainer:addButton(WidgetTable)
	local wt = WidgetTable
	wt.font = wt.font or "default"
	wt.color = wt.color or {1, 1, 1, 1}
	wt.color_focused = wt.color_focused or {1, 1, 1, 1}
	wt.anim = wt.anim or "idle"
	wt.visible = wt.visible or true
	wt.x = wt.x or 0
	wt.y = wt.y or 0
	if wt.text and dictionary and dictionary[wt.text] then wt.text = dictionary[wt.text] end
	local sz1, sz2 = GetCaptionSize(wt.font, (wt.text) or ".")
	wt.w = wt.w or sz1
	wt.h = wt.h or sz2
	wt.id = wt.id or "WIDGET" ..nextid()
	local dx, dy = 0,0
	if wt.center and wt.text then
		dx = wt.x - (wt.center[1]-math.floor(sz1/2))
		dy = wt.y - (wt.center[2]-math.floor(sz2/2))
		wt.x = wt.x - dx
		wt.y = wt.y - dy
	end
	local widget = CreateWidget(constants.wt_Button, wt.id, nil, wt.x, wt.y, wt.w, wt.h)
	if wt.text then
		--Log(wt.text..":"..(dictionary[wt.text] or "nil"))
		WidgetSetCaption(widget, wt.text)
		WidgetSetCaptionColor(widget, wt.color, false)
		WidgetSetCaptionColor(widget, wt.color_focused, true)
		WidgetSetCaptionFont(widget, wt.font)
	end
	WidgetSetVisible(widget, (self.visibility and wt.visible))
	if wt.sprite then
		WidgetSetSprite(widget, wt.sprite)
		WidgetSetAnim(widget, wt.anim)
	end
	WidgetSetLMouseClickProc(widget, WidgetContainer.onLeft)
	WidgetSetRMouseClickProc(widget, WidgetContainer.onRight)
	WidgetSetFocusProc(widget, WidgetContainer.onFocus)
	WidgetSetUnFocusProc(widget, WidgetContainer.onUnfocus)
	if wt.non_focusable then
		WidgetSetFocusable(widget, false)
	end

	if wt.pointer_proto then
		pointer = CreateWidget(constants.wt_Picture, wt.id.."_pointer", nil, wt.pointer_x-dx, wt.pointer_y-dy, 1, 1)
		WidgetSetSprite(pointer, wt.pointer_proto)
		WidgetSetAnim(pointer, "idle")
		WidgetSetVisible(pointer, false)
	end

	if self.elements[wt.id] then DestroyWidget(self.elements[wt.id]) end
	if self.pointers[wt.id] then 
		--Log(wt.id)
		--Log(self.pointers[wt.id])
		DestroyWidget(self.pointers[wt.id]) 
		self.pointes[wt.id] = nil 
	end
	self.elements[wt.id] = widget
	self.onfocus[wt.id] = wt.on_focus
	self.onunfocus[wt.id] = wt.on_unfocus
	self.onleft[wt.id] = wt.on_left
	self.onright[wt.id] = wt.on_right
	self.params[wt.id] = wt.params
	self.pointers[wt.id] = pointer
	self.visible[wt.id] = wt.visible
	WidgetContainer.w2c[wt.id] = self
	WidgetContainer.w2id[widget] = wt.id

	--Log(serialize("wt", wt))

	return wt.id
end

function WidgetContainer:show(state)
	if self.on_show then
		self.on_show(state)
	end
	self.visibility = state
	for key, value in pairs(self.elements) do
		WidgetSetVisible(self.elements[key], (self.visible[key] and self.visibility))
		if not self.visibility and self.pointers[key] then WidgetSetVisible(self.pointers[key], false) end
	end
end

function WidgetContainer:destroy()
	for key, value in pairs(self.elements) do
		DestroyWidget(self.elements[key])
		if self.pointers[key] then 
			DestroyWidget(self.pointers[key]) 
		end
		self.elements[key] = nil
		self.pointers[key] = nil
	end
end

function WidgetContainer:destroyWidget(id)
	if self.elements[id] then
		DestroyWidget(self.widgets[id])
		self.elements[id] = nil
		if self.pointers[id] then
			DestroyWidget(self.pointers[id])
		end
	else
		Log(string.format("WARNING: WidgetContainer %s: trying to destroy widget %s which does not exists.", (self.id or "BAD CONTAINER"), (id or "BAD WIDGET")))
	end
end

function WidgetContainer:showWidget(id, state)
	if self.elements[id] then
		self.visible[id] = iff(state, true, false)
		WidgetSetVisible(self.elements[id], (self.visible[id] and self.visibility))
		if not self.visible[id] and self.pointers[id] then WidgetSetVisible(self.pointers[id], false) end
	else
		Log(string.format("WARNING: WidgetContainer %s: trying to change visibility of widget %s which does not exists.", (self.id or "BAD CONTAINER"), (id or "BAD WIDGET")))
	end

end

function WidgetContainer.onFocus(sender)
	local id = WidgetContainer.w2id[sender]
	local cont = WidgetContainer.w2c[id]
	if not cont or not id then
		Log("ERROR: A call to WidgetContainer function from widget not in the container!")
		return
	end
	
	if ( cont.pointers[id] ) then
		WidgetSetVisible( cont.pointers[id], true )
	end
	if ( cont.onfocus[id] ) then
		cont.onfocus[id](sender, cont.params[id])
	end
end

function WidgetContainer.onUnfocus(sender)
	local id = WidgetContainer.w2id[sender]
	local cont = WidgetContainer.w2c[id]

	if not cont or not id then
		Log("ERROR: A call to WidgetContainer function from widget not in the container!")
		return
	end
	
	if ( cont.pointers[id] ) then
		WidgetSetVisible( cont.pointers[id], false )
	end
	if ( cont.onunfocus[id] ) then
		cont.onunfocus[id](sender, cont.params[id])
	end
end

function WidgetContainer.onLeft(sender)
	local id = WidgetContainer.w2id[sender]
	local cont = WidgetContainer.w2c[id]

	if not cont or not id then
		Log("ERROR: A call to WidgetContainer function from widget not in the container!")
		return
	end
	
	if ( cont.onleft[id] ) then
		cont.onleft[id](sender, cont.params[id])
	end
end

function WidgetContainer.onRight(sender)
	local id = WidgetContainer.w2id[sender]
	local cont = WidgetContainer.w2c[id]

	if not cont or not id then
		Log("ERROR: A call to WidgetContainer function from widget not in the container!")
		return
	end
	
	if ( cont.onright[id] ) then
		cont.onright[id](sender, cont.params[id])
	end
end

function WidgetContainer.onKey(sender, key)
	local id = WidgetContainer.w2id[sender]
	local cont = WidgetContainer.w2c[id]

	if not cont or not id then
		Log("ERROR: A call to WidgetContainer function from widget not in the container!")
		return
	end
	
	if ( cont.onkey[id] ) then
		cont.onkey[id](sender, key)
	end
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Window
-- ������ ���� � ������. ���������� 9 ��������. ��� ��������� �������� ���������� ������ ������� � ����������� �������.
Window = {}
Window.__index = Window
Window.widgets = {}

-- ������� � ����������� ������� ����. ���������� ������� ���� ��� (���� �� ������ �����-�� ��� ��������� ���� ���� �������).
function Window:prepareWidgets()
	local ww = self.widgets
	
	if not self.widget then
		self.widget = CreateWidget( constants.wt_Panel, "window", nil, self.x, self.y, self.w, self.h )
		WidgetSetSprite( self.widget, self.sprite )
		WidgetSetVisible( self.widget, true )
		WidgetSetFocusable( self.widget, false )
		if z then
			WidgetSetZ( self.widget, z )
		end
	end
end

-- ������ �������� ���� ���������� ��������� � �������
function Window:show()
	self:setVisible(true)
	WidgetSetPos(self.widget, self.x, self.y)
	WidgetSetSize(self.widget, self.w, self.h)
end

function Window:resize( w, h )
	if (w > self.w and not self.locked_size[1][2]) or (w < self.w and not self.locked_size[1][1]) then
		self.w = w
	end
	if (h > self.h and not self.locked_size[2][2]) or (h < self.h and not self.locked_size[2][1]) then
		self.h = h
	end
	self:show()
end

function Window:move( x, y )
	--if ( x ~= self.x ) then print( x ) end
	self.x = x
	self.y = y
	self:show()
end

function Window:recreate( x, y, w, h )
	if (w > self.w and not self.locked_size[1][2]) or (w < self.w and not self.locked_size[1][1]) then
		self.w = w
		self.x = x
	end
	if (h > self.h and not self.locked_size[2][2]) or (h < self.h and not self.locked_size[2][1]) then
		self.h = h
		self.y = y
	end
	self:show()
end

function Window:setVisible(visibility)
	self.visible = visibilty
	WidgetSetVisible(self.widget, visibility)
end

function Window:lock()
	self.locked = true
	WidgetSetActive( self.widget, false )
end

function Window:unlock()
	self.locked = false
	WidgetSetActive( self.widget, true )
end

function Window:is_locked()
	return self.locked
end

function Window:destroy()
	DestroyWidget( self.widget )
end

function Window.create(x, y, w, h, sprite, z)
	local a = {}
	setmetatable(a, Window)
	a.locked_size = { {false, false}, {false, false} }
	a.x = x
	a.y = y
	a.w = w
	a.h = h
	a.z = z
	a.dsize = 16
	a.sprite = sprite
	a.visible = true
	a.widget = nil
	a:prepareWidgets()
	a:show()
	return a
end

-----------------------------------------------------------------

function LetterBox( state )
	if state then
		if not letterbox1 then
			letterbox1 = CreateWidget(constants.wt_Widget, "letterbox1", nil, 0, 0, CONFIG.scr_width, CONFIG.scr_height/8)
			WidgetSetZ(letterbox1, 0.5)
			WidgetSetColorBox( letterbox1, {0,0,0,1} )
		end
		if not letterbox2 then 
			letterbox2 = CreateWidget(constants.wt_Widget, "letterbox2", nil, 0, CONFIG.scr_height*7/8, CONFIG.scr_width, CONFIG.scr_height/8)
			WidgetSetZ(letterbox2, 0.5)
			WidgetSetColorBox( letterbox2, {0,0,0,2} )
		end
	else
		if ( letterbox1 ) then DestroyWidget(letterbox1); letterbox1 = nil end
		if ( letterbox2 ) then DestroyWidget(letterbox2); letterbox2 = nil end
	end
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Fading

local fade_color = {0, 0, 0, 0}
local fade_box = nil
local fade_z = 0.95
local fading_in = false

local function FadeOutThread( color, time )
	--Log("FadeOutThread called")
	if not color then color = { 0, 0, 0 } end
	if time == 0 then time = 1 end
	local fade_start = GetCurTime()
	local fade_alpha = 0
	local progress
	fade_color = color
	










	if not fade_box then
		fade_box = CreateWidget(constants.wt_Widget, "fade_box", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2)
		WidgetSetZ(fade_box, fade_z)
		WidgetSetVisible(fade_box, true)
	end	
	
	while fade_alpha ~= 1 do
		fade_alpha = (GetCurTime() - fade_start) / time
		if fade_alpha > 1 then fade_alpha = 1 end
		WidgetSetColorBox(fade_box, {color[1], color[2], color[3], fade_alpha} )
		Wait(1)
	end
	
	--Log("FadeOutThread ending, fading_in = ", fading_in)
end

local function FadeInThread( time )
	local fade_start = GetCurTime()
	--Log("FadeInThread called, time = ", time, ", cur_time = ", fade_start, ", fading_in = ", fading_in, ", fade_box = ", fade_box)
	if not fade_box then return end
	local fade_alpha = 1
	while (fade_start + time > GetCurTime()) and fading_in do
		fade_alpha = 1 - (GetCurTime()-fade_start)/time
		WidgetSetColorBox(fade_box, {fade_color[1], fade_color[2], fade_color[3], fade_alpha} )
		Wait(1)
	end
	if fading_in then 
		DestroyWidget(fade_box)
		fade_box = nil
	end	
	--Log("FadeInThread ending, fading_in = ", fading_in)
	fading_in = false
end

function FadeOut( color, time )
	--Log("FadeOut called, fading_in = ", fading_in)
	fading_in = false
	if time > 0 then		
		local fade_thread = NewThread( FadeOutThread )
		Resume( fade_thread, color, time )
	else
		if not fade_box then 
			fade_box = CreateWidget(constants.wt_Widget, "fade_box", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2)
		end
		WidgetSetZ(fade_box, fade_z)
		WidgetSetVisible(fade_box, true)
		WidgetSetColorBox(fade_box, {color[1], color[2], color[3], 1} )
	end
end

function FadeIn( time )
	--Log("FadeIn called, fading_in = ", fading_in)
	if fading_in then return end
	fading_in = true
	local fade_thread = NewThread( FadeInThread )
	Resume( fade_thread, time )
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- ��� �������, ������� ���-�� ������ �������

-- ������� ������, ����������� � ��������� ������ � ������� �����
local function moveThread(time, linear, action, end_action)
	assert(action and (type(action) == "function"))
	assert(end_action and (type(end_action) == "function"))
	
	local time_start = GetCurTime()
	local cur_time = time_start
Wait(1)
	while time_start + time > cur_time do
		local t = (cur_time - time_start)/time

		if not linear then t = t*t*(3-2*t) end
		
		action(t)
		
		Wait(1)
		cur_time = GetCurTime()
	end 
	
	end_action()
	
	--Log("moveThread ended")
end

-- ������ ������� ������
function MoveWidgetTo(widget, new_pos, time, linear, onEnd)
	assert(widget)
	assert(new_pos and (type(new_pos) == "table"))
	assert(time)
	
	local old_x, old_y = WidgetGetPos(widget)
	if not old_x or not old_y then return end
	local time_start = GetCurTime()
	local cur_time = time_start
	
	local x = 0
	local y = 0
	
	
	
	local function on_timer(t)
		x = old_x + (new_pos[1] - old_x) * t
		y = old_y +(new_pos[2] - old_y) * t
		WidgetSetPos(widget, x, y)
	end
	local function last_move()
		WidgetSetPos(widget, new_pos[1], new_pos[2])
		if onEnd and (type(onEnd) == "function") then onEnd() end
	end
	
	local move_thread = NewThread(moveThread)
	--Log("move_thread = ", move_thread)
	Resume(move_thread, widget, new_pos, time, linear, onEnd)
end

-- ������� ������ � ������� � ������ ������� � � ����
-- shift_x, shift_y - ����� ������������ �������
function AttachCamToObjSlowly(id, shift_x, shift_y, time, linear, onEnd)
	assert(id)
	assert(shift_x)
	assert(shift_y)
	assert(time)
	
	local obj = GetObject(id)
	if not obj then return end
	
	local obj_x, obj_y = obj.aabb.p.x, obj.aabb.p.y
	local cam_x, cam_y = GetCamPos()
	
	SetCamAttachedObj(id);
	
	local foc_x, foc_y = GetCamFocusShift()
	local old_x = -(cam_x - obj_x) + foc_x
	local old_y = -(cam_y - obj_y) + foc_y
	
	SetCamObjOffset(old_x,old_y)
	
	local function on_timer(t)
		local x = old_x + (shift_x - old_x) * t
		local y = old_y + (shift_y - old_y) * t
		SetCamObjOffset(x,y)
	end
	local function last_move()
		SetCamObjOffset(shift_x,shift_y)
		if onEnd and (type(onEnd) == "function") then onEnd() end
	end
	
	local move_thread = NewThread(moveThread)
	--Log("move_thread = ", move_thread)
	Resume(move_thread, time, linear, on_timer, last_move)
end

-- ������� ������ � ������� � ������ ������� � � ����
-- shift_x, shift_y - ����� ������������ �������
function ChangeGlobalVolumeSlowly(from, to, time, linear, onEnd)
	assert(from)
	assert(to)
	assert(time)
	
	local old = from
	CONFIG.volume = from
	LoadConfig()
	
	local function on_timer(t)
		local v = old + (to - old) * t
		
		CONFIG.volume = v
		LoadConfig()
	end
	local function last_move()
		CONFIG.volume = to
		LoadConfig()
		SaveConfig()
		if onEnd and (type(onEnd) == "function") then onEnd() end
	end
	
	local move_thread = NewThread(moveThread)
	--Log("move_thread = ", move_thread)
	Resume(move_thread, time, linear, on_timer, last_move)
end

function FadeOutGlobalVolume(time, linear, onEnd)
	ChangeGlobalVolumeSlowly(0, CONFIG.volume, time, linear, onEnd)
end
----------------------------------------------------------------------
----------------------------------------------------------------------

function isConfigKeyPressed(key, cfgKeyName)
	for i = 1, constants.configKeySetsNumber do
		if CONFIG.key_conf[i][cfgKeyName] == key then return true end
	end
	return false
end
--------------------------------------------------------------------------

function showStageName( number, event )
	push_pause(true)
	GUI:hide()
	local st1, st2 = dictionary_string("STAGE").." "..tostring(number), dictionary_string(Loader.level.name or "ANONYMOUS")
	local x1, y1 = GetCaptionSize("default", st1)
	local x2, y2 = GetCaptionSize("default", " ")
	local x3, y3 = GetCaptionSize("default", st2)
	local label = CreateWidget(constants.wt_Label, "Label1", nil, CONFIG.scr_width/2-math.max(x1, x3)/2, CONFIG.scr_height/2-y1-y2/2, 200, 10)
	WidgetSetBorder(label, false)
	WidgetSetCaptionColor(label, {1,1,1,1}, false)
	WidgetSetCaption(label, st1.."/n/n"..st2, true)
	WidgetUseTyper(label, true, 20, false)
	WidgetStartTyper(label);
	mapvar.tmp.label = label
	mapvar.tmp.ssn_event = event
	Resume( NewThread( function()
		Wait( 2000 )
		DestroyWidget( mapvar.tmp.label )
		pop_pause()
		GUI:show()
		mapvar.tmp.ssn_event()
	end ))
end

--------------------------------------------------------------------------

--SetCamObjOffset(0, 60);
_SetCamObjOffset = SetCamObjOffset
_cam_offset = { 0, 0 }
_cam_shake = { 0, 0 }
function SetCamObjOffset( x, y )
	_cam_offset = { x, y }
	_SetCamObjOffset( x, y )
end

function ShakeCamera( x, y, duration, intensity )
	local x = (x or 10)
	local y = (y or 5)
	local duration = (duration or 500)
	local intensity = (intensity or 1)
	Resume( NewMapThread( function()
		local start_time = Loader.time
		local cx, cy
		local t = 0
		local oox, ooy = 0, 0
		local param
		while t < duration do
			param = (t/10) / intensity
			_cam_shake[1] = _cam_shake[1] + x*math.sin(param)
			_cam_shake[2] = _cam_shake[2] + y*math.sin(param/1.3)
			_SetCamObjOffset( _cam_offset[1] + _cam_shake[1], _cam_offset[2] + _cam_shake[2] )
			Wait(1)
			_cam_shake[1] = _cam_shake[1] - x*math.sin(param)


			_cam_shake[2] = _cam_shake[2] - y*math.sin(param/1.3)
			t = Loader.time - start_time
		end
		--if _cam_shake[1] == 0 and _cam_shake[2] == 0 then
			_SetCamObjOffset( unpack(_cam_offset) )
		--end		
	end ))
end

--Dropping all possible parameters from a calling object

function _ShakeCamera()
	ShakeCamera()
end

--------------------------------------------------------------------------


function ShowDir( menu, path, root_dir, filter, preprocess, event, cancel_event, preview )
	local dir, dirs = ListDirContents( path )
	dirs = dirs or 0
	dir = dir or {}
	local files = {}
	for i = #dir, dirs+1, -1 do
		table.insert( files, table.remove( dir, i ) )
	end
	table.sort( files )
	table.sort( dir )
	for i = 1, #files do
		table.insert( dir, files[i] )
	end
	
	menu:clear()
	if not preprocess then preprocess = function(a) return a end end
	if not filter then filter = function(a) return true end end
	if not preview then preview = function(a) end end


	if path ~= root_dir then
		menu:add(
				MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				"..",
				0, 
				function()
					path = string.gsub(path, "/[^/]+$", "")
					ShowDir( menu, path, filter, root_dir, preprocess, event, preview )
				end,
				nil,
				{name="wakaba-widget"},
				nil,
				function() preview() end
				),
			2
		)
	end
	menu:add(
				MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={1,.2,.2,1}}, 
				"CANCEL",
				0, 
				function()
					menu:hide()
					cancel_event()
				end,
				nil,
				{name="wakaba-widget"},
				nil,
				function() preview() end
				),
			2
		)

	for i=1,dirs do
		menu:add(
				MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				preprocess(dir[i]),
				0, 
				function() 
					path = path.."/"..dir[i]
					ShowDir( menu, path, filter, root_dir, preprocess, event, preview )	
				end,
				dir[i],
				{name="wakaba-widget"},
				nil,
				function() preview() end
				),
			2
		)
	end

	local first_item
	for i=dirs+1, #dir do
		if filter(path.."/"..dir[i]) then
			local item = menu:add(
					MWFunctionButton.create(
					{name="dialogue", color={.6,.6,.6,1}, active_color={.2,.2,.6,1}}, 
					preprocess(dir[i]),
					0, 
					function()
						if not event( path.."/"..dir[i] ) then menu:hide() end
					end,
					dir[i],
					{name="wakaba-widget"},
					nil,
					function() preview( path.."/"..dir[i] ) end
					),
				2
			)
			
			if not first_item then first_item = item end
		end
	end
	
	if first_item then menu:gainFocus(first_item) end
	
	menu:move( SHOW_DIR_X or CONFIG.scr_width/2, SHOW_DIR_Y or CONFIG.scr_height/2, 2 )
	menu:show()
end

--------------------------------------------------------------------------

function MakeWindowAppear( window, pos1, pos2 )
	local win = window
	if win:is_locked() then return end
	win.appearing = true
	win.appeared_from = pos1
	win:lock()
	Resume( NewThread( function()
		while window.appearing and (math.abs( pos2[1] - pos1[1] ) > 1 or math.abs( pos2[2] - pos1[2] ) > 1) do
			pos1[1] = pos1[1] + ( pos2[1] - pos1[1] ) * 0.2
			pos1[2] = pos1[2] + ( pos2[2] - pos1[2] ) * 0.2
			win:move( pos1[1], pos1[2] )
			Wait( 1 )
		end
		win:move(pos2[1], pos2[2])
		win.appearing = false
		win:unlock()
	end ))
end
function MakeWindowDisappear( window, pos, dont_clear )
	local win = window
	if win:is_locked() then return end
	win.appearing = false
	local p = shallow_copy( window.anchor )
	pos = pos or win.appeared_from or {0, 0}
	win:lock()
	Resume( NewThread( function()
		while math.abs( pos[1] - p[1] ) > 100 or math.abs( pos[2] - p[2] ) > 100 do
			p[1] = p[1] + ( pos[1] - p[1] ) * 0.2
			p[2] = p[2] + ( pos[2] - p[2] ) * 0.2
			win:move( p[1], p[2] )
			Wait( 1 )
		end
		win:hide()
		win:unlock()
		if not dont_clear then win:clear() end
	end ))
end

--------------------------------------------------------------------------

if not RestoreKeyProcessors then
	function RestoreKeyProcessors()
	end
end

function RemoveKeyProcessors()
	local on_press = GlobalGetKeyDownProc()
	local on_release = GlobalGetKeyReleaseProc()
	local mouse_press = GlobalGetMouseKeyDownProc()
	local mouse_release = GlobalGetMouseKeyReleaseProc()
	local restore_proc = RestoreKeyProcessors
	GlobalSetKeyDownProc( nil )
	GlobalSetKeyReleaseProc( nil )
	GlobalSetMouseKeyDownProc( nil )
	GlobalSetMouseKeyReleaseProc( nil )
	RestoreKeyProcessors = function()
		GlobalSetKeyDownProc( on_press )
		GlobalSetKeyReleaseProc( on_release )
		GlobalSetMouseKeyDownProc( mouse_press )
		GlobalSetMouseKeyReleaseProc( mouse_release )
		RestoreKeyProcessors = restore_proc
	end
end

--------------------------------------------------------------------------

local skip_key_active = false

function SetSkipKey( skip_event, skip_delay, specific_keys )
	RemoveKeyProcessors()
	skip_key_active = true
	local time = GetCurTime()
	local delayed_time = time + (skip_delay or 0)
	GlobalSetKeyReleaseProc( function( key )
		local skip = false
		if specific_keys then
			for k,v in pairs( specific_keys ) do
				if key == v then
					skip = true
					break
				end
			end
		else
			skip = true
		end
		if GetCurTime() < delayed_time then
			skip = false
		end
		if skip then
			if not skip_event(key) then
				RestoreKeyProcessors()
				skip_key_active = false
			else
				delayed_time = GetCurTime() + skip_delay
			end
		end	
	end )
	GlobalSetMouseKeyReleaseProc( function( key )
		local skip = false
		if specific_keys then
			for k,v in pairs( specific_keys ) do
				if "mouse"..key == v then
					skip = true
					break
				end
			end
		else
			skip = true
		end
		if GetCurTime() < delayed_time then
			skip = false
		end
		if skip then
			if not skip_event(key) then
				RestoreKeyProcessors()
				skip_key_active = false
			else
				delayed_time = GetCurTime() + skip_delay
			end
		end	
	end )
end

function RemoveSkipKey()
	if skip_key_active then
		RestoreKeyProcessors()
		skip_key_active = false
	end
end

--------------------------------------------------------------------------

local back_function = nil

-- Will restore all changes made with PushBackEvent without issuing the event handler.
-- Useful in cases, when code overrides back click system
function PopBackEvent()
end

function PushBackEvent( func )
	local old_pop = PopBackEvent
	local old_back = back_function
	
	local new_back
	new_back = function(sender) 
		--Log("b ", new_back)
		if not func(sender) then
			PopBackEvent()
		end
	end
	
	PopBackEvent = function()
		--Log("p ", PopBackEvent, " op ", old_pop, " ob ", old_back)
		back_function = old_back
		PopBackEvent = old_pop
		RestoreKeyProcessors()
	end
	
	--Log("op ", old_pop, " ob ", old_back, " p ", PopBackEvent, " b ", new_back)
	
	back_function = new_back
	RemoveKeyProcessors()
	GlobalSetKeyReleaseProc( function( key )
		if IsConfKey(key, config_keys.gui_nav_menu) or IsConfKey(key, config_keys.gui_nav_decline) then
			back_function()
			return true
		end
	end )
	return new_back
end

function WidgetSetBackClickProc( widget, func )
	WidgetSetLMouseClickProc( widget, PushBackEvent( func ) )
end

--------------------------------------------------------------------------
local ordered_arrays = {}

function specific_order( order )
	local lorder = {}
	for k, v in ipairs( order ) do
		lorder[v] = k
	end
	return function( a, b )
		return (lorder[a] or 0) < (lorder[b] or 0)
	end
end

function update_array_order( array, greater )
	local order = greater or function( a, b ) return array[a].priority > array[b].priority end
	local ord = function( a, b ) return order( a, b ) end
	ordered_arrays[ array ] = {}
	for k, v in pairs( array ) do
		table.insert( ordered_arrays[ array ], k )
	end
	table.sort( ordered_arrays[array], ord )
end

function sorted_pairs( array )
	local num = 0
	if not ordered_arrays[array] then
		return nil
	end
	return function()
		num = num + 1
		return ordered_arrays[array][num], array[ordered_arrays[array][num]]
	end	
end

--------------------------------------------------------------------------

function CreateSpriteCenter( sprite, x, y )
	local sp = GetObjectUserdata( CreateSprite( sprite, x, y ) )
	SetObjPos( sp, x, y )
	return sp
end

--------------------------------------------------------------------------

function WithLabels( tab )
	local ret_labels = {};
	--First, find all labels
	local labels = { anim = {} }
	for anim_num, anim in pairs( tab ) do
		labels.anim[ anim_num ] = {}
		for num, frame in pairs( anim.frames ) do
			if frame.label then
				if labels.anim[ anim_num ][ frame.label ] then
					assert( false, string.format("Error: duplicate label '%s'", frame.label) )
				end
				labels.anim[ anim_num ][ frame.label ] = num - 1
				ret_labels[ frame.label ] = num - 1
			end
		end
	end
	--Next, replace all strings in param fields
	for anim_num, anim in pairs( tab ) do
		for num, frame in pairs( anim.frames ) do
			if frame.param and type( frame.param ) == 'string' then
				if not labels.anim[ anim_num ][ frame.param ] then
					assert( false, string.format("Error: label '%s' does not exist!", frame.param) )
				end
				tab[ anim_num ].frames[ num ].param = labels.anim[ anim_num ][ frame.param ]
			end
		end
	end
	return tab, ret_labels;
end

--------------------------------------------------------------------------

function WaitGameTime( ms )
	local time = Loader.time + ms
	while Loader.time < time do
		Wait(1)
	end
end

--------------------------------------------------------------------------

function bit(p) 
	return 2 ^ (p - 1) -- 1-based indexing 
end 

-- Typical call: if hasbit(x, bit(3)) then ... 
function hasbit(x, p) 
	return x % (p + p) >= p 
end 

function setbit(x, p) 
	return hasbit(x, p) and x or x + p 
end 

function clearbit(x, p) 
	return hasbit(x, p) and x - p or x 
end 

--------------------------------------------------------------------------

function explode(what, how)
	local pos, ret = 0,{}
	local iter = function()
		return string.find( what, how, pos, true )
	end
	for newpos, newposend in iter do
		table.insert( ret, string.sub(what, pos, newpos-1 ) )
		pos = newposend + 1
	end
	table.insert(ret, string.sub(what, pos))
	return ret
end

--------------------------------------------------------------------------

function OpenFile( tab )
	local tab = tab or {}
	tab.limits = tab.limits or { 99999, 99999 }
	tab.limits[1] = tab.limits[1] or 99999 
	tab.limits[2] = tab.limits[2] or 99999
	RemoveKeyProcessors()

	--"constants"
	local FONT_X, FONT_Y = GetCaptionSize( tab.font or "dialogue", "I" )
	local SIZE_X, SIZE_Y = CONFIG.scr_width-60, CONFIG.scr_height-60
	local SLOT_X, SLOT_Y = tab.slot_width or 150, FONT_Y + 10
	local LIMIT_X, LIMIT_Y = math.min(tab.limits[1], math.floor(SIZE_X / SLOT_X)), math.min(tab.limits[2], math.floor(SIZE_Y / SLOT_Y))
	SIZE_X, SIZE_Y = SLOT_X * LIMIT_X, SLOT_Y * iff(tab.cancel_event, LIMIT_Y+1, LIMIT_Y )
	local PAGE_SIZE = ( SIZE_X / SLOT_X ) * ( SIZE_Y / (FONT_Y + 10) )
	if tab.cancel_event then
		PAGE_SIZE = ( SIZE_X / SLOT_X ) * ( SIZE_Y / (FONT_Y + 10) - 1 )
	end
	SIZE_X, SIZE_Y = SIZE_X + 20, SIZE_Y + 20

	--variables
	local window = CreateWidget( constants.wt_Panel, "", nil, (CONFIG.scr_width-SIZE_X)/2, (CONFIG.scr_height-SIZE_Y)/2, SIZE_X, SIZE_Y )
	WidgetSetFocusable( window, false )
	local path = tab.path or "/"
	local absolute = tab.absolute or false
	local dl, fl, dnum, fnum = {}, {}, 0, 0
	local quick_delete = nil
	local page = 0
	local slots = {}
	local active_slot = 0
	local column = -1
	local cancel_button
	local cancel_x, cancel_y
	local virtual_files = tab.virtual_files or {}

	--function "declarations"
	local ShutDownEveryting
	local ShortenName
	local GetLists
	local PopulateSlots
	local CreateSlot
	local WidgetUp
	local WidgetBack
	local WidgetForward
	local WidgetDir
	local WidgetFile
	local AdjustSize

	--functions
	AdjustSize = function(elements)
		if tab.lock_size then
			return
		end
		local x = math.floor( elements / LIMIT_Y )
		if elements % LIMIT_Y ~= 0 then
			x = x+1
		end
		local y = iff( x > 1, LIMIT_Y, elements )
		if tab.cancel_event then
			y = y + 1
		end
		WidgetSetSize( window, x * SLOT_X + 20, y * SLOT_Y + 20 )
		WidgetSetPos( window, (CONFIG.scr_width-x*SLOT_X)/2 - 10, (CONFIG.scr_height-y*SLOT_Y)/2 - 10 )
		if cancel_button then
			WidgetSetPos( cancel_button, (CONFIG.scr_width-cancel_x)/2, (CONFIG.scr_height+y*SLOT_Y)/2 - SLOT_Y + 5 )
		end
	end

	ShutDownEverything = function()
		RestoreKeyProcessors()
		DestroyWidget( window )
	end

	ShortenName = function( name, width )
		local font = tab.font or "dialogue"
		local n = name
		local width = GetCaptionSize( font, n )
		while width > SLOT_X-10 do
			n = string.gsub( n, ".%.?%.?%.?$", "..." )
			width = GetCaptionSize( font, n )
		end
		return n
	end

	GetLists = function( path )
		local files = {}
		local directories = {}
		local content
		local filter = tab.filter or function() return true end
		local preprocess = tab.preprocess or function(a) return a end

		local dir, dirs = ListDirContents( path, absolute )

		dirs = dirs or 0
		dir = dir or {}
		for i = #dir, dirs+1, -1 do
			if dir[i] and filter(dir[i], path) then
				content = table.remove( dir, i )
				table.insert( files, { content, ShortenName( preprocess(content, path) ), SLOT_X-20 } )
			end
		end
		for i = #dir, 1, -1 do
			if dir[i] and filter(dir[i], path) then
				content = table.remove( dir, i )
				table.insert( directories, { content, ShortenName( preprocess(content, path), SLOT_X-20 ) } )
			end
		end
		for k, v in ipairs( virtual_files ) do
			if v[1] == path then
				table.insert( files, { v[3], ShortenName( preprocess(v[2], path), SLOT_X-20 ), v[4], v[5] } )
			end
		end
		table.sort( directories, function( a, b ) return a[2] < b[2] end )
		table.sort( files, function( a, b ) return a[2] < b[2] end )

		return directories, files, #directories, #files
	end

	CreateSlot = function( x, y, text, event, colors, num )
		local w = CreateWidget( constants.wt_Button, "", quick_delete, x, y, SLOT_X, FONT_Y )
		WidgetSetPos( w, x, y, true )
		WidgetSetCaptionFont( w, tab.font or "dialogue" )
		WidgetSetCaptionColor( w, colors[1], false, {0,0,0,1} )
		WidgetSetCaptionColor( w, colors[2], true, {0,0,0,1} )
		WidgetSetCaption( w, text )
		WidgetSetLMouseClickProc( w, event )
		WidgetSetFocusProc( w, function()
			active_slot = num
		end )
		slots[num] = w
	end

	WidgetUp = function( x, y, slot )
		local event = function()
			path = string.gsub( path, "/[^/]+/", "/" )
			dl, fl, dnum, fnum = GetLists(path)
			page = 0
			PopulateSlots(0)		
		end
		CreateSlot( x, y, "..", event, { {1, 1, 1, 1}, {.2, .2, 1, 1} }, slot )
	end

	WidgetBack = function( x, y, slot )
		local event = function()
			page = page - 1
			PopulateSlots(page)		
		end
		CreateSlot( x, y, "(PAGE UP)", event, { {1, 1, 1, 1}, {.2, .2, 1, 1} }, slot )
	end

	WidgetForward = function( x, y, slot )
		local event = function()
			page = page + 1
			PopulateSlots(page)		
		end
		CreateSlot( x, y, "(PAGE DOWN)", event, { {1, 1, 1, 1}, {.2, .2, 1, 1} }, slot )
	end

	WidgetDir = function( x, y, slot, show_name, real_name )
		local event = function()
			path = path .. real_name .. "/"
			dl, fl, dnum, fnum = GetLists(path)
			page = 0
			PopulateSlots(0)		
		end
		CreateSlot( x, y, show_name, event, { {1, 1, 1, 1}, {.2, .2, 1, 1} }, slot )
	end

	WidgetFile = function( x, y, slot, show_name, real_name, virtual_value, custom_values )
		local event = function()
			if (tab.event or function(a, b) print(b..a) end)(real_name, path, virtual_value, custom_values) then
				ShutDownEverything()
			end
		end
		CreateSlot( x, y, show_name, event, { {.6, .6, .6, 1}, {.2, .2, .6, 1} }, slot )
	end

	PopulateSlots = function( page )
		if quick_delete then
			DestroyWidget( quick_delete )
		end
		quick_delete = CreateWidget( constants.wt_Widget, "", window, 0, 0, 1, 1 )
		WidgetSetPos( quick_delete, 0, 0, true )
		slots = {}
		column = -1
		
		local x, y = 15, 15

		local first_element = (page * PAGE_SIZE) + 1
		local current_slot = 1
		local widget_created = false
		local element_num = first_element
		local widgets_created = 0
		while current_slot <= PAGE_SIZE do

			widget_created = false

			if current_slot == 1 then
				if page == 0 and path ~= (tab.root or "/") then
					WidgetUp( x, y, current_slot )
					widget_created = true
				elseif page > 0 then
					WidgetBack( x, y, current_slot )
					widget_created = true
		
				end
			elseif current_slot == PAGE_SIZE then
				if dnum + fnum > PAGE_SIZE * ( page + 1 ) then
					WidgetForward( x, y, current_slot )
					widget_created = true
				end
			end

			if not widget_created then
				if element_num > dnum + fnum then
					current_slot = current_slot + 1
					break
				elseif element_num > dnum then
					WidgetFile( x, y, current_slot, fl[ element_num - dnum ][2], fl[ element_num - dnum ][1], fl[ element_num - dnum ][3], fl[ element_num - dnum ][4] )
					widgets_created = widgets_created + 1
				else
					WidgetDir( x, y, current_slot, dl[ element_num ][2], dl[ element_num ][1] )
					widgets_created = widgets_created + 1
				end
				element_num = element_num + 1
			else
				widgets_created = widgets_created + 1
			end

			y = y + FONT_Y + 10
			if y > SIZE_Y - FONT_Y - 10 - iff( tab.cancel_event, FONT_Y+10, 0 ) then
				if column == -1 then
					column = current_slot
				end
				x = x + SLOT_X
				y = 15
			end
			current_slot = current_slot + 1
		end
		AdjustSize( current_slot-1 )
		return widgets_created > 0
	end

	--main
	WidgetSetSprite( window, tab.sprite or "window1" )
	dl, fl, dnum, fnum = GetLists(path)
	if not PopulateSlots(0) then
		ShutDownEverything()
		if tab.cancel_event then
			tab.cancel_event()
			return 
		else
			return
		end
	end
	if tab.cancel_event then
		local sx, sy = GetCaptionSize( tab.font or "dialogue", dictionary_string( "BACK" ) )
		local w = CreateWidget( constants.wt_Button, "", window, (CONFIG.scr_width-sx)/2, (CONFIG.scr_height+SIZE_Y)/2 - FONT_Y - 15, sx, sy )
		WidgetSetCaptionFont( w, tab.font or "dialogue" )
		WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false, {0,0,0,1} )
		WidgetSetCaptionColor( w, { 1, .2, .2, 1 }, true, {0,0,0,1} )
		WidgetSetCaption( w, dictionary_string( "BACK" ) )
		WidgetSetLMouseClickProc( w, function()
			ShutDownEverything()
			tab.cancel_event()
		end )
		cancel_x, cancel_y = sx, sy
		cancel_button = w
	end
	GlobalSetKeyDownProc( function( key )
		if isConfigKeyPressed( key, "gui_nav_next" ) or isConfigKeyPressed( key, "down" ) then
			active_slot = active_slot + 1
			if not slots[active_slot] then
				active_slot = 1
			end
			WidgetGainFocus( slots[active_slot] )
			return true
		elseif isConfigKeyPressed( key, "gui_nav_next" ) or isConfigKeyPressed( key, "down" ) then
			active_slot = active_slot + 1
			if not slots[active_slot] then
				active_slot = 1
			end
			WidgetGainFocus( slots[active_slot] )
			return true
		elseif isConfigKeyPressed( key, "right" ) and column > 0 then
			active_slot = active_slot + column
			if active_slot > #slots then
				active_slot = active_slot % column + 1				
			end
			WidgetGainFocus( slots[active_slot] )
		elseif isConfigKeyPressed( key, "left" ) and column > 0 then
			active_slot = active_slot - column
			if not slots[active_slot] then
				active_slot = #slots - active_slot % column
			end
			WidgetGainFocus( slots[active_slot] )
		elseif key == keys.pgdown and (page + 1) * PAGE_SIZE < dnum + fnum then
			page = page + 1
			PopulateSlots( page )
		elseif key == keys.pgup and page > 0 then
			page = page - 1
			PopulateSlots( page )
		end
	end )
	GlobalSetKeyReleaseProc( function( key )
		if (isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )) and tab.cancel_event then
			ShutDownEverything()
			if tab.cancel_event then
				tab.cancel_event()
			end
		end
	end )
end

--------------------------------------------------------------------------

function CleanMultilineString( str )
	local ret = str
	ret = string.gsub( ret, "/c......", "" )
	ret = string.gsub( ret, "/n", "" )
	return ret
end

local cp1251=
{
	[128]='\208\130',[129]='\208\131',[130]='\226\128\154',[131]='\209\147',[132]='\226\128\158',[133]='\226\128\166',
	[134]='\226\128\160',[135]='\226\128\161',[136]='\226\130\172',[137]='\226\128\176',[138]='\208\137',[139]='\226\128\185',
	[140]='\208\138',[141]='\208\140',[142]='\208\139',[143]='\208\143',[144]='\209\146',[145]='\226\128\152',
	[146]='\226\128\153',[147]='\226\128\156',[148]='\226\128\157',[149]='\226\128\162',[150]='\226\128\147',[151]='\226\128\148',
	[152]='\194\152',[153]='\226\132\162',[154]='\209\153',[155]='\226\128\186',[156]='\209\154',[157]='\209\156',
	[158]='\209\155',[159]='\209\159',[160]='\194\160',[161]='\209\142',[162]='\209\158',[163]='\208\136',
	[164]='\194\164',[165]='\210\144',[166]='\194\166',[167]='\194\167',[168]='\208\129',[169]='\194\169',
	[170]='\208\132',[171]='\194\171',[172]='\194\172',[173]='\194\173',[174]='\194\174',[175]='\208\135',
	[176]='\194\176',[177]='\194\177',[178]='\208\134',[179]='\209\150',[180]='\210\145',[181]='\194\181',
	[182]='\194\182',[183]='\194\183',[184]='\209\145',[185]='\226\132\150',[186]='\209\148',[187]='\194\187',
	[188]='\209\152',[189]='\208\133',[190]='\209\149',[191]='\209\151'
}

local utf8=
{
	[128]={[147]='\150',[148]='\151',[152]='\145',[153]='\146',[154]='\130',[156]='\147',[157]='\148',[158]='\132',[160]='\134',[161]='\135',[162]='\149',[166]='\133',[176]='\137',[185]='\139',[186]='\155'},
	[130]={[172]='\136'},
	[132]={[150]='\185',[162]='\153'},
	[194]={[152]='\152',[160]='\160',[164]='\164',[166]='\166',[167]='\167',[169]='\169',[171]='\171',[172]='\172',[173]='\173',[174]='\174',[176]='\176',[177]='\177',[181]='\181',[182]='\182',[183]='\183',[187]='\187'},
	[208]={[129]='\168',[130]='\128',[131]='\129',[132]='\170',[133]='\189',[134]='\178',[135]='\175',[136]='\163',[137]='\138',[138]='\140',[139]='\142',[140]='\141',[143]='\143',[144]='\192',[145]='\193',[146]='\194',[147]='\195',[148]='\196',
	[149]='\197',[150]='\198',[151]='\199',[152]='\200',[153]='\201',[154]='\202',[155]='\203',[156]='\204',[157]='\205',[158]='\206',[159]='\207',[160]='\208',[161]='\209',[162]='\210',[163]='\211',[164]='\212',[165]='\213',[166]='\214',
	[167]='\215',[168]='\216',[169]='\217',[170]='\218',[171]='\219',[172]='\220',[173]='\221',[174]='\222',[175]='\223',[176]='\224',[177]='\225',[178]='\226',[179]='\227',[180]='\228',[181]='\229',[182]='\230',[183]='\231',[184]='\232',
	[185]='\233',[186]='\234',[187]='\235',[188]='\236',[189]='\237',[190]='\238',[191]='\239'},
	[209]={[128]='\240',[129]='\241',[130]='\242',[131]='\243',[132]='\244',[133]='\245',[134]='\246',[135]='\247',[136]='\248',[137]='\249',[138]='\250',[139]='\251',[140]='\252',[141]='\253',[142]='\254',[143]='\255',[144]='\161',[145]='\184',
	[146]='\144',[147]='\131',[148]='\186',[149]='\190',[150]='\179',[151]='\191',[152]='\188',[153]='\154',[154]='\156',[155]='\158',[156]='\157',[158]='\162',[159]='\159'},[210]={[144]='\165',[145]='\180'}
}

local nmdc = {
  [36] = '$',
  [124] = '|'
}

function cp1251_utf8( text )
	local r, b = ''
	for i = 1, text and text:len() or 0 do
		b = text:byte(i)
		if b < 128 then
			r = r..string.char(b)
		else
			if b > 239 then
				r = r..'\209'..string.char(b-112)
			elseif b > 191 then
				r = r..'\208'..string.char(b-48)
			elseif cp1251[b] then
				r = r..cp1251[b]
			else
				r = r..'_'
			end
		end
	end
	return r
end

function utf8_cp1251( text )
	local a, j, r, b = 0, 0, ''
	for i = 1, text and text:len() or 0 do
		b = text:byte(i)
		if b < 128 then
			if nmdc[b] then
				r = r..nmdc[b]
			else
				r = r..string.char(b)
			end
		elseif a == 2 then
			a, j = a - 1, b
		elseif a == 1 then
			a, r = a - 1, r..utf8[j][b]
		elseif b == 226 then
			a = 2
		elseif b == 194 or b == 208 or b == 209 or b == 210 then
			j, a = b, 1
		else
			r = r..'_'
		end
	end
	return r
end

local hex = "^[0123456789abcdef]+$"

function hex_dec( str )
	local s = string.lower( str )
	if not string.match( s, hex ) then
		return nil
	end
	local ret = 0
	local len = #s
	for i=1, len do
		ret = ret + (string.find( hex, string.sub( s, len-i+1, len-i+1 ) ) - 3) * 16^(i-1)
	end
	return ret
end

--------------------------------------------------------------------------

local strans = {
	["�"] = "AA", ["�"] = "BB", ["�"] = "WW", ["�"] = "GG", ["�"] = "DD", ["�"] = "YE",
	["�"] = "YO", ["�"] = "ZH", ["�"] = "ZZ", ["�"] = "II", ["�"] = "YY", ["�"] = "KK",
	["�"] = "LL", ["�"] = "MM", ["�"] = "NN", ["�"] = "OO", ["�"] = "PP", ["�"] = "RR", 
	["�"] = "SS", ["�"] = "TT", ["�"] = "UU", ["�"] = "FF", ["�"] = "KH", ["�"] = "CC", 
	["�"] = "CH", ["�"] = "SH", ["�"] = "CH", ["�"] = "_B", ["�"] = "YI", ["�"] = "_0",
	["�"] = "EE", ["�"] = "YU", ["�"] = "YA",
	["�"] = "aa", ["�"] = "bb", ["�"] = "ww", ["�"] = "gg", ["�"] = "dd", ["�"] = "ye",
	["�"] = "yo", ["�"] = "zh", ["�"] = "zz", ["�"] = "ii", ["�"] = "yy", ["�"] = "kk",
	["�"] = "ll", ["�"] = "mm", ["�"] = "nn", ["�"] = "oo", ["�"] = "pp", ["�"] = "rr", 
	["�"] = "ss", ["�"] = "tt", ["�"] = "uu", ["�"] = "ff", ["�"] = "kh", ["�"] = "cc", 
	["�"] = "ch", ["�"] = "sh", ["�"] = "ch", ["�"] = "_b", ["�"] = "yi", ["�"] = "_1",
	["�"] = "ee", ["�"] = "yu", ["�"] = "ya",
}

function remove_cyrillic( str )
	return string.gsub( str, "[�-��-���]", strans )
end

--------------------------------------------------------------------------

local depth = 0
local depth_str = {}
for _ in string.gmatch( constants.path_app, "[/\\][^/\\]+" ) do
	depth = depth + 1; 
end
for i=1, depth do
	table.insert( depth_str, "../" )
end
depth_str = table.concat( depth_str )
function protect_string( str )
	return string.gsub( str, "[%%%-%+%?%*%.%^%$]", "%%%1" )
end
function build_relative_path( absolute_path )
	local path = "^"..protect_string( constants.path_app )
	if string.match( absolute_path, path ) then
		return string.gsub( absolute_path, path, "" )
	end
	return depth_str .. absolute_path
end

--------------------------------------------------------------------------

routines = {}

return routines
