Achievements = {}
Achievements.info =
{
	["X28"] =
	{
		name = "��������� �����",
		description = "�� �������� ���������� ����� 28.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 8,
	},
	["PLUMBER"] =
	{
		name = "���� ��������������",
		description_locked = "�������, ��� � �������.",
		description = "�� �������� �� ��� ���������, �� ���� �� ���������� �����.",
		progress = 3,
		sprite = "pear15-achievements",
		frame = 1
	},
	["FIRST_BLOOD"] =
	{
		name = "� ���� ���� ���� ����������?",
		description_locked = "�������! ����. ����� ������� ��� ���������� ������ ������ ������� �����.",
		description = "�������! ����. �� ����� ������ ������� �����.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 0
	},
	["SEWER_SECRETS"] =
	{
		name = "������������",
		description_locked = "/cff8888������: �����������/cffffff. ������� ���������������� ������ ���������������.",
		description = "�� ����� ��� ������� �� ������ \"�����������\".",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 3
	},
	["SEWER_NOSECRETS"] =
	{
		name = "������ ��������",
		description_locked = "/cff8888������: �����������/cffffff. ������ ������ ����������� � ��������� ����������� ��������.",
		description = "�� ��������� ������� \"�����������\", �� ����� �� ������ �������.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 4
	},
	["SLIMES"] =
	{
		name = "������ ����",
		description_locked = "/cff8888������: �����������/cffffff. ��� ������ ������� ��� � ���-��.",
		description = "�� ����� ���� ������� �� ������� �� ������� �������� ������.",
		progress = 5,
		sprite = "pear15-achievements",
		frame = 5
	},
	["KEEPING_COOL"] =
	{
		name = "������� �����",
		description_locked = "/cff8888������: �������, �����!/cffffff. ��� ������ ������ - �� �������� ������.",
		description = '�� ������ "�������, �����!" �� �������� ��������� ����������� �� ��������.',
		progress = 1,
		sprite = "pear15-achievements",
		frame = 6
	},
	["MOUNTAINS_SECRETS"] =
	{
		name = "���������",
		description_locked = "/cff8888������: ����/cffffff. ������� ��������������� ������������.",
		description = "�� ����� ��� ������� �� ������ \"����\".",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 9
	},
	["MOUNTAINS_NOSECRETS"] =
	{
		name = "������",
		description_locked = "/cff8888������: ����/cffffff. ���� ���������� ������?",
		description = "�� ��������� ������� \"����\", �� ����� �� ������ �������.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 10
	},
	["WELCOME_TO_OMSK"] =
	{
		name = "����� ���������� � ����",
		description_locked = "/cff8888������: ������ ���/cffffff. ��� ��� �����.",
		description = "�� ������� ������ � ��������� ����������� ������.",
		progress = 60000,
		sprite = "pear15-achievements",
		frame = 15
	},
	["I_BOT"] =
	{
		name = "�, ���",
		description_locked = "� �� ���� ������ ���, ��� �������� ��� ���� ��� ����.",
		description = "�� ������ ���� �� ������ ��������� \"�����-���\" (�������).",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 13
	},
	["ENDING_DOOM"] =
	{
		name = "� ������!",
		description = "�� �������� �������� �� ������ ��������� ����.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 22
	},
	["ENDING_THATGUY"] =
	{
		name = "����� ������ ���",
		description = "�� �������� ������� �� ������������ OMX.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 23
	},
	["BAD_ENDING"] =
	{
		name = "BAD END",
		description = "�� �� ������ ������� ����� � �������� ������.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 16
	},
	["ALL_ENDINGS"] =
	{
		name = "����� ���� �������!",
		description_locked = "����, �������, ��� ����� ������� ��������.",
		description = "�� ���������� ��� ��������� ��������.",
		progress = 2^3-1,
		sprite = "pear15-achievements",
		frame = 21
	},
	["READ_EVERYTHING"] =
	{
		name = "����� ��� �����",
		description_locked = "���� �� ������� ��� ��������, �� �� ��� �� ���������� ����.",
		description = "�� ����������� ��� �������� ���� \"����������\" ��� ������� ���� ���.",
		progress = 100,
		sprite = "pear15-achievements",
		frame = 24
	},
	["WEAPON_TESTER"] =
	{
		name = "���������������",
		description_locked = "��������� �������, ��� �� � ���� ����� ����� ����������� ���� �� ���.",
		description = "�� ����� �� ������ ����� ������ ������� ������� ������ � ����.",
		progress = (2^10)-1,
		sprite = "pear15-achievements",
		frame = 18
	},
	["LEVEL_TESTER"] =
	{
		name = "˸����-����������",
		description = "�� ��������� ���������������� �����.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 17
	},
	["FULL_HEALTH"] =
	{
		name = "�������� ���",
		description = "�� �������� ��������� ��������� ���������.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 19
	},
	["DEJA_VU"] =
	{
		name = "���� ��",
		description = "�� ������ ��������� ���� ������, ������� �����������.",
		progress = 2^2-1,
		sprite = "pear15-achievements",
		frame = 25
	},
	["MULTIPLAYER"] =
	{
		name = "���������� ��������",
		description_locked = "������ ��� � ���� ����� ���� ������?",
		description = "�� ������ ���� �����.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 26
	},
	["I_KNOW_THE_WAY"] =
	{
		name = "� ���� ������!",
		description_locked = "/cff8888������: ����/cffffff. � �� ������� �� ���������������!",
		description = '�� ������ ������� "����", �� ������ � ������ ����� ��� ���.',
		progress = 1,
		sprite = "pear15-achievements",
		frame = 11,
	},
	["LEAVE_TURRETS_ALONE"] =
	{
		name = "��� �� ��������",
		description_locked = "/cff8888������: ������������ �������/cffffff. ��������� ����� �� �������� ��� ������. �������� ����������.",
		description = '�� ������ ������� "������������ �������", �� ��������� �� ����� �������������� ������.',
		progress = 1,
		sprite = "pear15-achievements",
		frame = 14
	},
	["LICENSED_TO_PLAY"] =
	{
		name = "����� � �����",
		description_locked = "������ ������� ����������� ������ ������������� �����.",
		description = "�� ��������� ��������� �������.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 2
	},
	["LORD_OF_TIME"] =
	{
		name = "���������� �������",
		description_locked = "���� ������� ����� ������ ���� ���������� �� ��� ���������!",
		description = "�� ���������� 5 ��������� �� ���� ����.",
		progress = 5,
		sprite = "pear15-achievements",
		frame = 20
	},
	["STUBBORN"] =
	{
		name = "������ �� ��������",
		description_locked = "/cff8888������: ������ ��������/cffffff. ����� � ��� ���-�� �� ���������� - ����� �������� ����� � �����. ������ ��� �� �� ����������.",
		description = "�� ���������� ���� ������� �������.",
		progress = 1,
		sprite = "pear15-achievements",
		frame = 7
	},
	["HARD_WAY"] =
	{
		name = "������� ������",
		description_locked = "/cff8888������: ����/cffffff. � ����� ����������� ������ ���� ������� ���� �� ������ �����. ����������� �������� ��������.",
		description = '�� �������� �������� ������ �� ����� � ����������� �� ������ "����", �� ������ � ������.',
		progress = 1,
		sprite = "pear15-achievements",
		frame = 12
	}
}
update_array_order( Achievements.info, specific_order
	{"FIRST_BLOOD", "LICENSED_TO_PLAY",
	"SEWER_SECRETS", "SEWER_NOSECRETS", "SLIMES", 
	"KEEPING_COOL", "STUBBORN",
	"MOUNTAINS_SECRETS", "MOUNTAINS_NOSECRETS", "I_KNOW_THE_WAY", "HARD_WAY",
	"LEAVE_TURRETS_ALONE", "WELCOME_TO_OMSK",
	"LEVEL_TESTER", "WEAPON_TESTER",
	"X28", "PLUMBER", "READ_EVERYTHING", "FULL_HEALTH", "LORD_OF_TIME",
	"I_BOT", "ENDING_DOOM", "ENDING_THATGUY", "BAD_ENDING", "ALL_ENDINGS", "DEJA_VU", "MULTIPLAYER" }
)

Achievements.UNLOCKED = -1
Achievements.RESET = 0

function Achievements.Progress( ach, profile, num, reset )
	if profile.achievement_progress[ach] == Achievements.UNLOCKED then
		return
	end
	if ( reset ) then
		profile.achievement_progress[ach] = Achievements.RESET
	else
		profile.achievement_progress[ach] = (profile.achievement_progress[ach] or 0) + num
		if profile.achievement_progress[ach] >= (Achievements.info[ach].progress or profile.achievement_progress[ach]+1) then
			profile.achievement_progress[ach] = Achievements.UNLOCKED
			profile.info_pages[ach] = Info.NEW
			Achievements.Unlock( ach )
		end
	end
	Saver.saveProfile()
end

function Achievements.BitProgress( ach, profile, num )
	if profile.achievement_progress[ach] == Achievements.UNLOCKED then
		return
	end
	if not profile.achievement_progress[ach] then
		profile.achievement_progress[ach] = 0
	end
	profile.achievement_progress[ach] = setbit( profile.achievement_progress[ach], bit(num) )
	if profile.achievement_progress[ach] == (Achievements.info[ach].progress or -1) then
		profile.achievement_progress[ach] = Achievements.UNLOCKED
		profile.info_pages[ach] = Info.NEW
		Achievements.Unlock( ach )
	end
end

Achievements.stack = {}

function Achievements.Message( id, title, sprite, frame, text )
	local window = CreateWidget( constants.wt_Panel, "Achievement", nil, CONFIG.scr_width-200, CONFIG.scr_height, 300, 54 )
	WidgetSetZ( window, 1.09 )
	WidgetSetSprite( window, "window1" )
	WidgetSetFocusable( window, false )
	local picture = CreateWidget( constants.wt_Picture, "Achievement", window, CONFIG.scr_width-200+11, CONFIG.scr_height+11, 32, 32 )
	WidgetSetFocusable( picture, false )
	WidgetSetSprite( picture, (sprite or "editor_misc"), true, (frame or 0) )
	WidgetSetZ( picture, 1.095 )
	WidgetSetSpriteRenderMethod( picture, constants.rsmStretch )
	local sx, sy = GetCaptionSize( "dialogue", dictionary_string(title or "???") )
	local label1 = CreateWidget( constants.wt_Label, "Achievement", window, CONFIG.scr_width-200+79+84-sx/2, CONFIG.scr_height+5+sy/2, 1, 1 )
	WidgetSetZ( label1, 1.09 )
	WidgetSetCaptionFont( label1, "dialogue" )
	WidgetSetCaptionColor( label1, { 1, 1, 0.3, 1 }, false )
	WidgetSetCaption( label1, dictionary_string(title or "???") )
	WidgetSetFocusable( label1, false )
	sx, sy = GetCaptionSize( "dialogue", dictionary_string(text or "???") )
	local label2 = CreateWidget( constants.wt_Label, "Achievement", window, CONFIG.scr_width-200+79+84-sx/2, CONFIG.scr_height+25+sy/2, 1, 1 )
	WidgetSetZ( label2, 1.09 )
	WidgetSetCaptionFont( label2, "dialogue" )
	WidgetSetCaptionColor( label2, { 1, 1, 1, 1 }, false )
	WidgetSetCaption( label2, dictionary_string(text or "???") )
	WidgetSetFocusable( label2, false )
	local ach = id
	table.insert( Achievements.stack, ach )
--	Resume( NewMapThreadWithCleanup( function()
	Resume( NewThread( function()
		local y = CONFIG.scr_height
		local target_y = CONFIG.scr_height
		for i=1,#Achievements.stack do
			target_y = target_y - 54
			if Achievements.stack[i] == ach then
				break
			end
		end
		local lifetime = 3000
		local last_time = GetCurTime()
		local achievement_life = true
		local alpha
		while achievement_life do
			if math.abs( y - target_y ) <= 16 then
				y = target_y
			else
				y = y + (target_y - y)/math.abs(target_y - y) * 16
			end
			WidgetSetPos( window, CONFIG.scr_width-300, y )
			lifetime = lifetime - ( GetCurTime() - last_time )
			last_time = GetCurTime()
			--print( lifetime )
			if lifetime <= 0 then
				achievement_life = false
			elseif lifetime < 1000 then
				alpha = lifetime / 1000
				WidgetSetSpriteColor( window, { 1, 1, 1, alpha } )
				WidgetSetSpriteColor( picture, { 1, 1, 1, alpha } )
				WidgetSetCaptionColor( label1, { 1, 1, 0.3, alpha }, false )
				WidgetSetCaptionColor( label2, { 1, 1, 1, alpha }, false )
			end
			Wait(1)
			target_y = CONFIG.scr_height
			for i=1,#Achievements.stack do
				target_y = target_y - 54
				if Achievements.stack[i] == ach then
					break
				end
			end
		end
		for k, v in pairs( Achievements.stack ) do
			if v == ach then
				table.remove( Achievements.stack, k )
				break
			end
		end
		DestroyWidget( window )
	--[[end,
	function()
		DestroyWidget( window )]]
	end ))
end

function Achievements.Unlock( ach )
	local ach = Achievements.info[ach]
	Achievements.Message( ach, "������� ����������", ach.sprite, ach.frame, ach.name )
end

function h()
	Resume( NewThread( function()
		Achievements.Unlock( "FIRST_BLOOD" )
		Achievements.Unlock( "X28" )
		Wait( 800 )
		Achievements.Unlock( "PLUMBER" )
	end ))
end

