Editor.proto_editor = {}

function Editor.initProtoEditor()
	--Editor.proto_editor = {}
	Editor.proto_editor.BACKGROUND = 1
	Editor.proto_editor.LAST_FRAME = 2
	Editor.proto_editor.NEXT_FRAME = 3
	Editor.proto_editor.PHYS_OBJ = 4
	Editor.proto_editor.INFO = 5
	Editor.proto_editor.zoom = 4
	Editor.proto_editor.COMMAND_NAME = {}
	for k, v in pairs( constants ) do
		if string.match( k, "^AnimCom" ) then
			Editor.proto_editor.COMMAND_NAME[v] = k
		end
	end
	Editor.proto_editor.COMMAND_NAME[0] = "none"
	Editor.proto_editor.COMMAND_NAME[-1] = "DELETE ME"
end

_PE = Editor.proto_editor

function Editor.proto_editor.removeWidgets()
	if not Editor.proto_editor.widgets then
		return
	end
	for k, v in pairs( Editor.proto_editor.widgets ) do
		DestroyWidget( v )
	end
end

function Editor.editProto( ptype, file )
	local sandbox = { math = math, difficulty = 1, string = string, table = table, constants = constants }
	local lfile = loadfile( file )
	if not lfile then return end
	setfenv( lfile, sandbox )
	pcall( lfile )
	lfile = loadfile( "textures/"..sandbox.texture..".lua" )
	setfenv( lfile, sandbox )
	pcall( lfile )
	Editor.proto_editor.sandbox = sandbox
	Editor.proto_editor.removeWidgets()
	local widget = CreateWidget( constants.wt_Widget, "proto editor back", nil, 0, 0, CONFIG.scr_width, CONFIG.scr_height )
	WidgetSetColorBox( widget, {0,0,0,1} )
	WidgetSetZ( widget, 0.84 )
	_PE.file = file
	_PE.widgets = {}
	_PE.widgets[_PE.BACKGROUND] = background
	widget = CreateWidget( constants.wt_Picture, "current_frame", nil, 0, 0, 1, 1 )
	_PE.widgets[_PE.NEXT_FRAME] = widget
	widget = CreateWidget( constants.wt_Picture, "previous_frame", nil, 0, 0, 1, 1 )
	_PE.widgets[_PE.LAST_FRAME] = widget
	widget = CreateWidget( constants.wt_Widget, "physic_border", nil, 0, 0, 1, 1 )
	WidgetSetBorder( widget, true )
	WidgetSetBorderColor( widget, {1, .5, 0, 1}, false )
	WidgetSetBorderColor( widget, {1, .5, 0, 1}, true )
	_PE.widgets[_PE.PHYS_OBJ] = widget
	widget = CreateWidget( constants.wt_Label, "info", nil, 0, 0, CONFIG.scr_width/2, CONFIG.scr_height )
	WidgetSetCaptionColor( widget, {1,1,1,1}, false )
	WidgetSetCaptionFont( widget, "dialogue" )
	_PE.widgets[_PE.INFO] = widget
	_PE.anim = 1
	_PE.frame = 0
	RemoveKeyProcessors()
	_PE.setKeyProcessors()
	_PE.w = 1
	_PE.h = 1
	_PE.x = 0
	_PE.y = 0
	_PE.num = 0
	_PE.new = {}
	_PE.mpsprite1 = CreateColorBox( 0, 0, 16, 16, 1, { 0, 1, 0, 0.25 } )
	_PE.mpsprite2 = CreateColorBox( 0, 0, 8, 8, 1, { 0, .5, 0, 0.25 } )
	EditorSetLink( _PE.mpsprite1, _PE.mpsprite2 )
	for i=1, #_PE.sandbox.animations do
		_PE.new[i] = {}
	end
	_PE.stack = {}
	_PE.mp = {}
	_PE.mode = "NONE"
	EditorSetGrid( 64*4, 64*4, { .5, .5, .5, .5 } )
	_PE.nextFrame()
	_PE.showFrame()
end

function Editor.proto_editor.showFrame()
	local cx, cy = CONFIG.scr_width/2, CONFIG.scr_height/2 - CONFIG.scr_height/3
	local cmx, cmy = GetCamPos()
	cmx, cmy = cmx - cx, cmy - cy
	local px, py
	if _PE.last then
		px = cx - (_PE.last.w/2 + _PE.last.x) * _PE.zoom
		py = cy + CONFIG.scr_height/2 - (_PE.last.h + _PE.last.y) * _PE.zoom
		WidgetSetPos( _PE.widgets[_PE.LAST_FRAME], px, py )
		WidgetSetSize( _PE.widgets[_PE.LAST_FRAME], _PE.sandbox.main[ _PE.last.num+1 ].w*_PE.zoom, _PE.sandbox.main[ _PE.last.num+1 ].h*_PE.zoom )
		WidgetSetSprite( _PE.widgets[_PE.LAST_FRAME], _PE.sandbox.texture, true, _PE.last.num )
		WidgetSetSpriteColor( _PE.widgets[_PE.LAST_FRAME], { 1, 1, 1, 0.25 } )
		WidgetSetZ( _PE.widgets[_PE.LAST_FRAME], 0.846 )
		WidgetSetVisible( _PE.widgets[_PE.LAST_FRAME], true )
	else
		WidgetSetVisible( _PE.widgets[_PE.LAST_FRAME], false )
	end
	local frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame]
	px = cx - (_PE.w/2 + _PE.x) * _PE.zoom
	py = cy + CONFIG.scr_height/2 - (_PE.h + _PE.y) * _PE.zoom
	WidgetSetPos( _PE.widgets[_PE.NEXT_FRAME], px, py )
	WidgetSetSize( _PE.widgets[_PE.NEXT_FRAME], _PE.sandbox.main[ _PE.num+1 ].w*_PE.zoom, _PE.sandbox.main[ _PE.num+1 ].h*_PE.zoom )
	WidgetSetSprite( _PE.widgets[_PE.NEXT_FRAME], _PE.sandbox.texture, true, _PE.num )
	WidgetSetZ( _PE.widgets[_PE.NEXT_FRAME], 0.847 )
	WidgetSetPos( _PE.widgets[_PE.PHYS_OBJ], px+_PE.x*_PE.zoom, py+_PE.y*_PE.zoom )
	WidgetSetSize( _PE.widgets[_PE.PHYS_OBJ], _PE.w*_PE.zoom, _PE.h*_PE.zoom )
	if _PE.mp[0] then
		local x = cmx + px +_PE.x*_PE.zoom + (_PE.w*_PE.zoom)/2 + _PE.mp[0][1] * _PE.zoom
		local y =  cmy + py + _PE.y*_PE.zoom + (_PE.h*_PE.zoom)/2 + _PE.mp[0][2] * _PE.zoom - CONFIG.scr_height/3
		SetObjPos( _PE.mpsprite1, x, y, 1 )
		local angle = ((_PE.weapon_angle or 0)/180) * math.pi
		angle = -angle
		x = x + 10 * _PE.zoom * math.cos( angle )
		y = y + 10 * _PE.zoom * math.sin( angle )
		SetObjPos( _PE.mpsprite2, x, y, 1 )
	end
	_PE.updateInfo()
end

_PE.HELP = "m = move sprite/np = change physic object/nw = set weapon origin point/na = zoom out/nz = zoom in/ns = save/n[,] = change frame"

function Editor.proto_editor.updateInfo()
	local frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame]
	local command = _PE.COMMAND_NAME[frame.com or 0]
	WidgetSetCaption( _PE.widgets[_PE.INFO], string.format( 
	"Animation: %s/nFrame: %i/nDuration: %i/nCommand: %s/n/nZoom: %ix/nMode: %s/n/n%s", 
		_PE.sandbox.animations[_PE.anim].name or "error",
		_PE.frame,
		frame.dur or 0,
		command,
		_PE.zoom,
		_PE.mode,
		_PE.HELP
	), true)
	WidgetSetPos( _PE.widgets[_PE.INFO], 0, 25 )
end

function Editor.proto_editor.nextFrame()
	local frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame]
	if frame and frame.dur and frame.dur > 0 then
		_PE.last = {}
		_PE.last.w = _PE.w
		_PE.last.h = _PE.h
		_PE.last.x = _PE.x
		_PE.last.y = _PE.y
		_PE.last.num = _PE.num
	end
	repeat
		_PE.frame = _PE.frame + 1
		if _PE.frame > #_PE.sandbox.animations[ _PE.anim ].frames then
			_PE.anim = _PE.anim + 1
			_PE.w = 1
			_PE.h = 1
			_PE.x = 0
			_PE.y = 0
			_PE.mp = {}
			_PE.frame = 1
			_PE.last = nil
		end
		if _PE.anim > #_PE.sandbox.animations then
			_PE.anim = 1
		end
		frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame]
--		print( frame.dur or 0, frame.num or 0, _PE.COMMAND_NAME[frame.com or 0], frame.param or 0 )
		_PE.num = frame.num or 0
		if frame.com then
			if frame.com == constants.AnimComRealH then
				_PE.h = frame.param or 0
			elseif frame.com == constants.AnimComInitH then
				_PE.h = frame.param or 0
			elseif frame.com == constants.AnimComRealW then
				_PE.w = frame.param or 0
			elseif frame.com == constants.AnimComInitW then
				_PE.w = frame.param or 0
			elseif frame.com == constants.AnimComRealX then
				_PE.x = frame.param or 0
			elseif frame.com == constants.AnimComRealY then
				_PE.y = frame.param or 0
			elseif frame.com == constants.AnimComPushInt then
				table.insert( _PE.stack, frame.param or 0 )
			elseif frame.com == constants.AnimComPop then
				table.remove( _PE.stack, #_PE.stack )
			elseif frame.com == constants.AnimComSetWeaponAngle then
				_PE.weapon_angle = frame.param or 0
			elseif frame.com == constants.AnimComMPSet then
				_PE.mp[ frame.param or 0 ] = { _PE.stack[#_PE.stack-1] or 0, _PE.stack[#_PE.stack] or 0 }
				table.remove( _PE.stack, #_PE.stack )
				table.remove( _PE.stack, #_PE.stack )
			end
		end
	until frame.dur and frame.dur > 0
	_PE.GetNew()
	--print( "============================")
end

function Editor.proto_editor.GetNew()
	if not _PE.new[_PE.anim][_PE.frame] then
		return
	end
	if _PE.new[_PE.anim][_PE.frame].w then _PE.w = _PE.new[_PE.anim][_PE.frame].w end
	if _PE.new[_PE.anim][_PE.frame].h then _PE.h = _PE.new[_PE.anim][_PE.frame].h end
	if _PE.new[_PE.anim][_PE.frame].x then _PE.x = _PE.new[_PE.anim][_PE.frame].x end
	if _PE.new[_PE.anim][_PE.frame].y then _PE.y = _PE.new[_PE.anim][_PE.frame].y end
	if _PE.new[_PE.anim][_PE.frame].weapon_angle then _PE.weapon_angle = _PE.new[_PE.anim][_PE.frame].weapon_angle end
	if _PE.new[_PE.anim][_PE.frame].mp then _PE.mp = deep_copy(_PE.new[_PE.anim][_PE.frame].mp) end
end

function Editor.proto_editor.prevFrame()
	local frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame]
	if frame and frame.dur and frame.dur > 0 then
		_PE.last = {}
		_PE.last.w = _PE.w
		_PE.last.h = _PE.h
		_PE.last.x = _PE.x
		_PE.last.y = _PE.y
		_PE.last.num = _PE.num
	end
	repeat
		_PE.frame = _PE.frame - 1
		if _PE.frame == 0 then
			_PE.anim = _PE.anim - 1
			if _PE.anim == 0 then
				_PE.anim = #_PE.sandbox.animations
			end
			_PE.frame = #_PE.sandbox.animations[ _PE.anim ].frames
			_PE.last = nil
		end
		frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame]
--		print( frame.dur or 0, frame.num or 0, _PE.COMMAND_NAME[frame.com or 0], frame.param or 0 )
		_PE.num = frame.num or 0
		if frame.com then
			if frame.com == constants.AnimComRealH then
				_PE.h = frame.param or 0
			elseif frame.com == constants.AnimComInitH then
				_PE.h = frame.param or 0
			elseif frame.com == constants.AnimComRealW then
				_PE.w = frame.param or 0
			elseif frame.com == constants.AnimComInitW then
				_PE.w = frame.param or 0
			elseif frame.com == constants.AnimComRealX then
				_PE.x = frame.param or 0
			elseif frame.com == constants.AnimComRealY then
				_PE.y = frame.param or 0
			elseif frame.com == constants.AnimComPushInt then
				table.insert( _PE.stack, frame.param or 0 )
			elseif frame.com == constants.AnimComPop then
				table.remove( _PE.stack, #_PE.stack )
			elseif frame.com == constants.AnimComSetWeaponAngle then
				_PE.weapon_angle = frame.param or 0
			elseif frame.com == constants.AnimComMPSet then
				_PE.mp[ frame.param or 0 ] = { _PE.stack[#_PE.stack-1] or 0, _PE.stack[#_PE.stack] or 0 }
				table.remove( _PE.stack, #_PE.stack )
				table.remove( _PE.stack, #_PE.stack )
			end
		end
	until frame.dur and frame.dur > 0
	local i = 0
	local set_mp = false
	_PE.stack = {}
	repeat
		i = i + 1
		if i == _PE.frame then
			break
		end
		frame = _PE.sandbox.animations[_PE.anim].frames[_PE.frame-i]
		if (not frame.dur or frame.dur == 0) and frame.com then
			if frame.com == constants.AnimComRealH then
				_PE.h = frame.param or 0
			elseif frame.com == constants.AnimComInitH then
				_PE.h = frame.param or 0
			elseif frame.com == constants.AnimComRealW then
				_PE.w = frame.param or 0
			elseif frame.com == constants.AnimComInitW then
				_PE.w = frame.param or 0
			elseif frame.com == constants.AnimComRealX then
				_PE.x = frame.param or 0
			elseif frame.com == constants.AnimComRealY then
				_PE.y = frame.param or 0
			elseif frame.com == constants.AnimComPushInt then
				table.insert( _PE.stack, 1, frame.param or 0 )
			elseif frame.com == constants.AnimComPop then
				table.remove( _PE.stack, 1 )
			elseif frame.com == constants.AnimComSetWeaponAngle then
				_PE.weapon_angle = frame.param or 0
			elseif frame.com == constants.AnimComMPSet then
				set_mp = true
			end
		end
	until frame.dur and frame.dur > 0
	if set_mp then
		_PE.mp[ frame.param or 0 ] = { _PE.stack[#_PE.stack-1] or 0, _PE.stack[#_PE.stack] or 0 }
		table.remove( _PE.stack, #_PE.stack )
		table.remove( _PE.stack, #_PE.stack )
	end
	_PE.GetNew()
	--print( "============================")
end

function Editor.proto_editor.preSave()
	for k, v in pairs( _PE.sandbox.animations ) do
		local extra_frames = 0
		for i = 1, #v.frames do
			if _PE.new[k][i-extra_frames] then
				if _PE.new[k][i-extra_frames].w then
					table.insert( v.frames, i, { com = constants.AnimComRealW, param = _PE.new[k][i-extra_frames].w } )
					_PE.new[k][i-extra_frames].w = nil
					extra_frames = extra_frames + 1
					i = i + 1
				end
				if _PE.new[k][i-extra_frames].h then
					table.insert( v.frames, i, { com = constants.AnimComRealH, param = _PE.new[k][i-extra_frames].h } )
					_PE.new[k][i-extra_frames].h = nil
					extra_frames = extra_frames + 1
					i = i + 1
				end
				if _PE.new[k][i-extra_frames].x then
					table.insert( v.frames, i, { com = constants.AnimComRealX, param = _PE.new[k][i-extra_frames].x } )
					_PE.new[k][i-extra_frames].x = nil
					extra_frames = extra_frames + 1
					i = i + 1
				end
				if _PE.new[k][i-extra_frames].y then
					table.insert( v.frames, i, { com = constants.AnimComRealY, param = _PE.new[k][i-extra_frames].y } )
					_PE.new[k][i-extra_frames].y = nil
					extra_frames = extra_frames + 1
					i = i + 1
				end
				if _PE.new[k][i-extra_frames].mp then
					table.insert( v.frames, i, { com = constants.AnimComPushInt, param = _PE.new[k][i-extra_frames].mp[0][1] } )
					table.insert( v.frames, i+1, { com = constants.AnimComPushInt, param = _PE.new[k][i-extra_frames].mp[0][2] } )
					table.insert( v.frames, i+2, { com = constants.AnimComMPSet, param = 0 } )
					_PE.new[k][i-extra_frames].mp = nil
					extra_frames = extra_frames + 3
					i = i + 3
				end
				if _PE.new[k][i-extra_frames].weapon_angle then
					table.insert( v.frames, i, { com = constants.AnimComWeaponAngle, param = _PE.new[k][i-extra_frames].weapon_angle } )
					_PE.new[k][i-extra_frames].weapon_angle = nil
					extra_frames = extra_frames + 1
					i = i + 1
				end
			end
		end
	end
	local death_row = {}
	local function delete( pos )
		table.insert( death_row, pos )
	end
	for k, v in pairs( _PE.sandbox.animations ) do
		local last_w, last_h, last_x, last_y, last_mp, last_angle, v1 = nil, nil, nil, nil, nil, nil
		for i = 1, #v.frames do
			v1 = v.frames[i]
			if not v1 then break end
			if v1.com and v1.com == constants.AnimComRealW then
				if last_w then 
					delete( last_w )
				end
				last_w = i
			end
			if v1.com and v1.com == constants.AnimComRealH then
				if last_h then 
					delete( last_h )
				end
				last_h = i
			end
			if v1.com and v1.com == constants.AnimComRealX then
				if last_x then 
					delete( last_x )
				end
				last_x = i
			end
			if v1.com and v1.com == constants.AnimComRealY then
				if last_y then 
					delete( last_y )
				end
				last_w = i
			end
			if v1.com and v1.com == constants.AnimComSetWeaponAngle then
				if last_angle then
					delete( last_angle )
				end
				last_angle = i
			end
			if v1.com and v1.com == constants.AnimComMPSet then
				if last_mp then 
					if last_mp > 2 and v.frames[last_mp-2].com and v.frames[last_mp-2].com == constants.AnimComPushInt then
						delete( last_mp - 2 )
					end
					if last_mp > 1 and v.frames[last_mp-1].com and v.frames[last_mp-1].com == constants.AnimComPushInt then
						delete( last_mp - 1 )
					end
					delete( last_mp )
				end
				last_mp = i
			end
			if v1.dur and v1.dur > 0 then
				last_w, last_h, last_x, last_y, last_mp, last_angle = nil, nil, nil, nil, nil, nil
			end
		end
		for j=#death_row,1,-1 do
				table.remove( v.frames, death_row[j] )
		end
		death_row = {}
	end
end

function Editor.proto_editor.setKeyProcessors()
	GlobalSetKeyDownProc( function( key )
		if key == keys.rbracket then 
			_PE.nextFrame()
			_PE.showFrame()
		elseif key == keys.lbracket then
			_PE.prevFrame()
			_PE.showFrame()
		elseif key == keys.m then
			_PE.mode = "MOVE SPRITE"
			_PE.updateInfo()
		elseif key == keys.p then
			_PE.mode = "RESIZE PHYSIC"
			_PE.updateInfo()
		elseif key == keys.w then
			_PE.mode = "SET WEAPON POINT"
			_PE.updateInfo()
		elseif key == keys.z then
			_PE.zoom = _PE.zoom * 2
			EditorSetGrid( 64*_PE.zoom, 64*_PE.zoom, { .5, .5, .5, .5 } )
			_PE.showFrame()
		elseif key == keys.a then
			_PE.zoom = _PE.zoom / 2
			EditorSetGrid( 64*_PE.zoom, 64*_PE.zoom, { .5, .5, .5, .5 } )
			_PE.showFrame()
		elseif key == keys.s then
			_PE.preSave()
			local out = io.open( string.gsub( _PE.file, "%.lua$", "_editor.lua" ), "w" )
			out:write( "animations =\n{\n" )
			for k, v in pairs( _PE.sandbox.animations ) do
				out:write(string.format('\t{\n\tname = "%s";\n\tframes =\n\t\t{\n', v.name or "" ))
				for i = 1, #v.frames do
						local k1, v1 = i, v.frames[i]
						local txt = "nil"
						local com = "nil"
						if v1.com and v1.com > 0 then
							com = "constants.".._PE.COMMAND_NAME[v1.com]
						end
						if v1.txt then txt = '"'..v1.txt..'"' end
						out:write( string.format( '\t\t\t{ dur = %i, num = %i, com = %s, param = %i, txt = %s },\n', v1.dur or 0, v1.num or 0, com, v1.param or 0, txt ))
				end
				out:write("\t\t}\n\t},\n")
			end
			out:write("}\n")
			out:close()
		end
	end )
	GlobalSetMouseKeyDownProc( function(key)
		if _PE.mode == "MOVE SPRITE" then
			_PE.mouse_x, _PE.mouse_y = GetMousePos()
			_PE.old_x, _PE.old_y = _PE.x, _PE.y
			_PE.drag = true
			Resume( NewThread( function()
				while _PE.drag do
					local mx, my = GetMousePos()
					_PE.x = _PE.old_x - math.floor((mx - _PE.mouse_x)/_PE.zoom)
					_PE.y = _PE.old_y - math.floor((my - _PE.mouse_y)/_PE.zoom)
					_PE.showFrame()
					Wait(1)
				end
				if not _PE.new[_PE.anim][_PE.frame] then _PE.new[_PE.anim][_PE.frame] = {} end
				local new = _PE.new[_PE.anim][_PE.frame]
				new.x = _PE.x
				new.y = _PE.y
			end ))
		elseif _PE.mode == "RESIZE PHYSIC" then
			_PE.mouse_x, _PE.mouse_y = GetMousePos()
			_PE.old_w, _PE.old_h = _PE.w, _PE.h
			_PE.old_x, _PE.old_y = _PE.x, _PE.y
			_PE.drag = true
			Resume( NewThread( function()
				while _PE.drag do
					local mx, my = GetMousePos()
					local cx, cy = math.floor((mx - _PE.mouse_x)/_PE.zoom), math.floor((my - _PE.mouse_y)/_PE.zoom)
					if cx < _PE.old_w then
						_PE.w = _PE.old_w - cx
						_PE.x = _PE.old_x + cx/2
					end
					if cy < _PE.old_h then
						_PE.h = _PE.old_h - cy
						_PE.y = _PE.old_y + cy
					end
					_PE.showFrame()
					Wait(1)
				end
				if not _PE.new[_PE.anim][_PE.frame] then _PE.new[_PE.anim][_PE.frame] = {} end
				local new = _PE.new[_PE.anim][_PE.frame]
				new.x = _PE.x
				new.y = _PE.y
				new.w = _PE.w
				new.h = _PE.h
			end ))
		elseif _PE.mode == "SET WEAPON POINT" then
			_PE.mouse_x, _PE.mouse_y = GetMousePos()
			local cmx, cmy = GetCamPos()
			if not _PE.mp[0] then _PE.mp[0] = {} end
			_PE.mp[0][1] = math.floor ( (_PE.mouse_x - cmx )/_PE.zoom )
			_PE.mp[0][2] = math.floor ( (_PE.mouse_y - (cmy + CONFIG.scr_height/2 - CONFIG.scr_height/3 - _PE.h/2 * _PE.zoom) )/_PE.zoom )
			_PE.drag = true
			Resume( NewThread( function()
				while _PE.drag do
					local mx, my = GetMousePos()
					_PE.weapon_angle = math.atan2( my - _PE.mouse_y, mx - _PE.mouse_x )
					_PE.weapon_angle = 180 * _PE.weapon_angle / math.pi
					_PE.weapon_angle = -_PE.weapon_angle
					_PE.showFrame()
					Wait(1)
				end
				if not _PE.new[_PE.anim][_PE.frame] then _PE.new[_PE.anim][_PE.frame] = {} end
				local new = _PE.new[_PE.anim][_PE.frame]
				if not new.mp then new.mp = { [0] = {} } end
				new.mp[0][1] = _PE.mp[0][1]
				new.mp[0][2] = _PE.mp[0][2]
				new.weapon_angle = _PE.weapon_angle
			end ))
		end
	end	)
	GlobalSetMouseKeyReleaseProc( function()
		_PE.drag = false
	end )
end
