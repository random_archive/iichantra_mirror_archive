function Editor.InitSnap()
	Editor.sensitive_spots = {}
	Editor.sensitive_keys = {}
	Editor.anchor = nil
	Editor.grid = { x = 64, y = 64, show = true, color = {0.5, 0.5, 0.5, 0.5}, snap = true, dist = 10 }
	EditorSetGrid( 0, 0, {0,0,0,1} )
end

function Editor.CheckSensitiveSpot( x, y )
	local ret = false
	for k, v in pairs( Editor.sensitive_spots ) do
		if x > v.area[1] and x < v.area[3] and y > v.area[2] and y < v.area[4] then
			v.event()
			ret = true
		end
	end
	for k, v in pairs( Editor.sensitive_spots ) do
		if v.alt_event then
			v.alt_event()
		end
	end
	return ret
end

function Editor.CheckSensitiveKeys( key )
	local ret = false
	for k, v in pairs( Editor.sensitive_keys ) do
		if key == v.key then
			v.event()
			ret = true
		end
	end
	for k, v in pairs( Editor.sensitive_keys ) do
		if v.alt_event then
			v.alt_event()
		end
	end
	return ret
end

function Editor.hull( objs )
	local ret = {}
	if not objs or #objs == 0 then
		return nil
	end
	local o = objs[1]
	local x1, y1 = o.aabb.p.x - o.aabb.W, o.aabb.p.y - o.aabb.H
	local x2, y2 = o.aabb.p.x + o.aabb.W, o.aabb.p.y + o.aabb.H
	for i=2, #objs do
		o = objs[i]
		x1 = math.min( x1, o.aabb.p.x - o.aabb.W )
		y1 = math.min( y1, o.aabb.p.y - o.aabb.H )
		x2 = math.max( x2, o.aabb.p.x + o.aabb.W )
		y2 = math.max( y2, o.aabb.p.y + o.aabb.H )
	end
	return { x1, y1, x2, y2 }
end

function Editor.points_hull( objs )
	local ret = {}
	if not objs or #objs == 0 then
		return nil
	end
	local o = objs[1]
	local x1, y1, x2, y2 = o[1], o[2], o[1], o[2]
	for i=2, #objs do
		o = objs[i]
		x1 = math.min( x1, o[1] )
		y1 = math.min( y1, o[2] )
		x2 = math.max( x2, o[1] )
		y2 = math.max( y2, o[2] )
	end
	return { x1, y1, x2, y2 }
end

function _square( x, y, size )
	return { x-size, y-size, x+size, y+size }
end

function Editor._dir_clone( objs, dx, dy, metaspot, metakey )
	return function()
		local action = { type = "CREATION", obj = {}, text = "directional cloning" }
		Editor.selectionSet()
		local obj
		for k,v in pairs(metaspot) do
			if v.sprite then
				SetObjDead( v.sprite )
			end
			for k1, v1 in pairs( Editor.sensitive_spots ) do
				if v1 == v then
					table.remove( Editor.sensitive_spots, k1 )
					break
				end
			end
		end
		for k,v in pairs(metakey) do
			for k1, v1 in pairs( Editor.sensitive_keys ) do
				if v1 == v then
					table.remove( Editor.sensitive_keys, k1 )
					break
				end
			end
		end
		for k,v in pairs( objs ) do
			obj = Editor.cloneObject( GetObject(v.id), math.floor(v.aabb.p.x+dx+0.5), math.floor(v.aabb.p.y+dy+0.5) )
			obj = GetObject(obj)
			Editor.selectionSet( obj, true )
			table.insert( action.obj, obj )
		end
		Editor.AddHistoryEvent( action )
	end
end

function Editor._dir_clone_alt( metaspot, metakey )
	return function()
		for k,v in pairs(metaspot) do
			if v.sprite then
				SetObjDead( v.sprite )
			end
			for k1, v1 in pairs( Editor.sensitive_spots ) do
				if v1 == v then
					table.remove( Editor.sensitive_spots, k1 )
					break
				end
			end
		end
		for k,v in pairs(metakey) do
			for k1, v1 in pairs( Editor.sensitive_keys ) do
				if v1 == v then
					table.remove( Editor.sensitive_keys, k1 )
					break
				end
			end
		end
	end
end

function Editor.DirectionalCloning( objs )
	if not objs or #objs == 0 then
		return
	end
	local metaspot = {}
	local metakey = {}
	local hull = Editor.hull( objs )
	local spr
	spr = CreateSprite( "editor_misc", hull[1]-42, hull[2]+(hull[2]+hull[4])/2 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, hull[1]-42, (hull[2]+hull[4])/2 )
	SetObjSpriteAngle( spr, math.pi )
	table.insert( metaspot, {
		area = _square( hull[1]-42, (hull[2]+hull[4])/2, 16 ),
		sprite = spr,
		event = Editor._dir_clone( deep_copy( objs ), -(hull[3]-hull[1]), 0, metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[3]+42, hull[2]+(hull[2]+hull[4])/2 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, hull[3]+42, (hull[2]+hull[4])/2 )
	table.insert( metaspot, {
		area = _square( hull[3]+42, (hull[2]+hull[4])/2, 16 ),
		sprite = spr,
		event = Editor._dir_clone( deep_copy( objs ), (hull[3]-hull[1]), 0, metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]+(hull[1]+hull[3])/2, hull[2]-42 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, (hull[1]+hull[3])/2, hull[2]-42 )
	SetObjSpriteAngle( spr, -math.pi/2 )
	table.insert( metaspot, {
		area = _square( (hull[1]+hull[3])/2, hull[2]-42, 16 ),
		sprite = spr,
		event = Editor._dir_clone( deep_copy( objs ), 0, -(hull[4]-hull[2]), metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]+(hull[1]+hull[3])/2, hull[2]-42 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, (hull[1]+hull[3])/2, hull[4]+42 )
	SetObjSpriteAngle( spr, math.pi/2 )
	table.insert( metaspot, {
		area = _square( (hull[1]+hull[3])/2, hull[4]+42, 16 ),
		sprite = spr,
		event = Editor._dir_clone( deep_copy( objs ), 0, (hull[4]-hull[2]), metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.left, 
		event = Editor._dir_clone( deep_copy( objs ), -(hull[3]-hull[1]), 0, metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.right, 
		event = Editor._dir_clone( deep_copy( objs ), (hull[3]-hull[1]), 0, metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.up, 
		event = Editor._dir_clone( deep_copy( objs ), 0, -(hull[4]-hull[2]), metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.down, 
		event = Editor._dir_clone( deep_copy( objs ), 0, (hull[4]-hull[2]), metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	for k, v in pairs( metaspot ) do
		table.insert( Editor.sensitive_spots, v )
	end
	for k, v in pairs( metakey ) do
		table.insert( Editor.sensitive_keys, v )
	end
end

function Editor._set_anchor( anchor, metaspot, metakey )
	return function()
		Editor.selectionSet()
		local obj
		for k,v in pairs(metaspot) do
			if v.sprite then
				SetObjDead( v.sprite )
			end
			for k1, v1 in pairs( Editor.sensitive_spots ) do
				if v1 == v then
					table.remove( Editor.sensitive_spots, k1 )
					break
				end
			end
		end
		for k,v in pairs(metakey) do
			for k1, v1 in pairs( Editor.sensitive_keys ) do
				if v1 == v then
					table.remove( Editor.sensitive_keys, k1 )
					break
				end
			end
		end
		Editor.anchor = anchor
		Editor.FloatyText( "anchor set" )
	end
end

function Editor._set_anchor_alt( metaspot, metakey )
	return function()
		for k,v in pairs(metaspot) do
			if v.sprite then
				SetObjDead( v.sprite )
			end
			for k1, v1 in pairs( Editor.sensitive_spots ) do
				if v1 == v then
					table.remove( Editor.sensitive_spots, k1 )
					break
				end
			end
		end
		for k,v in pairs(metakey) do
			for k1, v1 in pairs( Editor.sensitive_keys ) do
				if v1 == v then
					table.remove( Editor.sensitive_keys, k1 )
					break
				end
			end
		end
	end
end

function Editor.SetAnchor( objs )
	if not objs or #objs == 0 then
		return
	end
	local metaspot = {}
	local metakey = {}
	local hull = Editor.hull( objs )
	local spr
	spr = CreateSprite( "editor_misc", hull[1]-42, hull[2]+(hull[2]+hull[4])/2 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, hull[1]-42, (hull[2]+hull[4])/2 )
	table.insert( metaspot, {
		area = _square( hull[1]-42, (hull[2]+hull[4])/2, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "right", hull[1] }, metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]-42, hull[2]+(hull[2]+hull[4])/2 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjSpriteAngle( spr, math.pi )
	SetObjPos( spr, hull[1]-42-37, (hull[2]+hull[4])/2 )
	table.insert( metaspot, {
		area = _square( hull[1]-42-37, (hull[2]+hull[4])/2, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "left", hull[1] }, metaspot, metakey ),
		alt_event = Editor._dir_clone_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[3]+42, hull[2]+(hull[2]+hull[4])/2 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjSpriteAngle( spr, math.pi )
	SetObjPos( spr, hull[3]+42, (hull[2]+hull[4])/2 )
	table.insert( metaspot, {
		area = _square( hull[3]+42, (hull[2]+hull[4])/2, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "left", hull[3] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[3]+42, hull[2]+(hull[2]+hull[4])/2 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, hull[3]+42+37, (hull[2]+hull[4])/2 )
	table.insert( metaspot, {
		area = _square( hull[3]+42+37, (hull[2]+hull[4])/2, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "right", hull[3] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]+(hull[1]+hull[3])/2, hull[2]-42 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, (hull[1]+hull[3])/2, hull[2]-42 )
	SetObjSpriteAngle( spr, math.pi/2 )
	table.insert( metaspot, {
		area = _square( (hull[1]+hull[3])/2, hull[2]-42, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "down", hull[2] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]+(hull[1]+hull[3])/2, hull[2]-42 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, (hull[1]+hull[3])/2, hull[2]-42-37 )
	SetObjSpriteAngle( spr, -math.pi/2 )
	table.insert( metaspot, {
		area = _square( (hull[1]+hull[3])/2, hull[2]-42-37, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "up", hull[2] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]+(hull[1]+hull[3])/2, hull[2]-42 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, (hull[1]+hull[3])/2, hull[4]+42 )
	SetObjSpriteAngle( spr, -math.pi/2 )
	table.insert( metaspot, {
		area = _square( (hull[1]+hull[3])/2, hull[4]+42, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "up", hull[4] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	spr = CreateSprite( "editor_misc", hull[1]+(hull[1]+hull[3])/2, hull[2]-42 )
	Editor.setObjParam( spr, "DONT_SELECT", true )
	Editor.setObjParam( spr, "NOT_REAL", true )
	SetObjAnim( spr, "arrow", false )
	SetObjPos( spr, (hull[1]+hull[3])/2, hull[4]+42+37 )
	SetObjSpriteAngle( spr, math.pi/2 )
	table.insert( metaspot, {
		area = _square( (hull[1]+hull[3])/2, hull[4]+42+37, 16 ),
		sprite = spr,
		event = Editor._set_anchor( { "down", hull[4] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.left, 
		event = Editor._set_anchor( { "right", hull[1] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.right, 
		event = Editor._set_anchor( { "left", hull[3] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.up, 
		event = Editor._set_anchor( { "down", hull[2] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	table.insert( metakey, { 
		key = keys.down, 
		event = Editor._set_anchor( { "up", hull[4] }, metaspot, metakey ),
		alt_event = Editor._set_anchor_alt( metaspot, metakey )
	})
	for k, v in pairs( metaspot ) do
		table.insert( Editor.sensitive_spots, v )
	end
	for k, v in pairs( metakey ) do
		table.insert( Editor.sensitive_keys, v )
	end
end

function Editor.updateGrid()
	if Editor.grid.show then
		EditorSetGrid( Editor.grid.x, Editor.grid.y, Editor.grid.color )
	else
		EditorSetGrid( 0, 0, Editor.grid.color )
	end
end

function Editor.grid_menu( p )
	if p["x"] then Editor.grid.x = tonumber(p["x"]) end
	if p["y"] then Editor.grid.y = tonumber(p["y"]) end
	if p["snap distance"] then Editor.grid.dist = tonumber(p["snap distance"]) end
	if p["color"] then Editor.grid.color = str2col( p["color"] ) end
	if p["show grid"] then Editor.grid.show = p["show grid"] == "true" end
	if p["snap to grid"] then Editor.grid.snap = p["snap to grid"] == "true" end
	Editor.updateGrid()
end

function Editor.snap( hull, x, y )
	if not Editor.grid.snap then return x, y end
	local sx, sy
	if not hull then
		sx1 = x % Editor.grid.x
		sy1 = y % Editor.grid.y
		sx2 = sx1
		sy2 = sy1
	else
		sx1 = (hull[1] + x) % Editor.grid.x
		sy1 = (hull[2] + y) % Editor.grid.y
		sx2 = (hull[3] + x) % Editor.grid.x
		sy2 = (hull[4] + y) % Editor.grid.y
	end
	if sx1 < Editor.grid.dist then
		x = x - sx1
	elseif sx2 >= Editor.grid.x - Editor.grid.dist then
		x = x + Editor.grid.x - sx2
	end
	if sy1 < Editor.grid.dist then
		y = y - sy1
	elseif sy2 >= Editor.grid.y - Editor.grid.dist then
		y = y + Editor.grid.y - sy2
	end
	return x, y
end

function Editor.toggleSnap()
	Editor.grid.snap = not Editor.grid.snap
	if Editor.grid.snap then
		Editor.FloatyText( "Grid snapping: on" )
	else
		Editor.FloatyText( "Grid snapping: off" )
	end
end

function Editor.toggleGrid()
	Editor.grid.show = not Editor.grid.show
	Editor.updateGrid()
end

function Editor.DropToAnchor( objs )
	if not objs or #objs == 0 then
		return
	end
	if not Editor.anchor then
		Editor.FloatyText( "anchor not set!" )
		return
	end
	local action = { type = "MOVEMENT", from = {}, to = {}, text = "Snap to anchor" }
	for k, v in pairs( objs ) do
		table.insert( action.from, { obj = v, pos = { x = v.aabb.p.x, y = v.aabb.p.y } })
	end
	local hull = Editor.hull( objs )
	local dx, dy = 0, 0
	if Editor.anchor[1] == "up" then dy = Editor.anchor[2] - hull[2] end
	if Editor.anchor[1] == "down" then dy = Editor.anchor[2] - hull[4] end
	if Editor.anchor[1] == "left" then dx = Editor.anchor[2] - hull[1] end
	if Editor.anchor[1] == "right" then dx = Editor.anchor[2] - hull[3] end
	for k, v in pairs( objs ) do
		v.aabb.p.x = v.aabb.p.x + dx
		v.aabb.p.y = v.aabb.p.y + dy
		SetObjPos( v.id, v.aabb.p.x, v.aabb.p.y )
		table.insert( action.to, { obj = v, pos = { x = v.aabb.p.x, y = v.aabb.p.y } })
	end
	Editor.AddHistoryEvent( action )
end
