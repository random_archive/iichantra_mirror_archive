function Editor.InitScripts()
	Editor.scripts = { init_script = "", map_script = "", weapon_bonus = "", place_secrets = "", remove_secrets = "", map_trigger = "" }
end

function Editor.DefaultScripts()
	Editor.InitScripts()
	Editor.scripts.init_script = "\tGame.fade_out()\n\tmapvar.tmp.fade_widget_text = {}\n\tif GetPlayer() then\n\t\tlocal sx, sy = GetCaptionSize( \"default\", \"New map\" )\n\t\tlocal text1 = CreateWidget( constants.wt_Label, \"TEXT\", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )\n\t\tmapvar.tmp.fade_widget_text[\"text1\"] = text1\n\t\tWidgetSetCaptionColor( text1, {1,1,1,1}, false )\n\t\tWidgetSetCaption( text1, \"New map\" )\n\t\tWidgetSetZ( text1, 1.05 )\n\t\tSetCamAttachedObj( GetPlayer().id )\n\t\tSetCamAttachedAxis(true, true)\n\t\tSetCamFocusOnObjPos(constants.CamFocusBottomCenter)\n\t\tSetCamObjOffset(0, 60)\n\t\tSetCamLag(0.9)\n\t\tGame.AttachCamToPlayers( {\n\t\t\t{ {-99999,-99999,99999,99999}, {-99999,-99999,99999,99999} },\n\t\t} )\n\t\tlocal env = CreateEnvironment('default_environment', -9001, -9001)\n\t\tSetDefaultEnvironment(env)\n\t\tGame.info.can_join = false\n\t\tmapvar.tmp.cutscene = true\n\t\tEnablePlayerControl( false )\n\t\tMenu.lock()\n\t\t\n\t\tlocal start_intro_thread = false\n\t\tlocal fade_thread = NewThread( function()\n\t\t\tWait( 1 )\n\t\t\tGUI:hide()\n\t\t\tWait( 1000 )\n\t\t\tmapvar.tmp.fade_complete = false\n\t\t\tGame.fade_in()\n\t\t\tstart_intro_thread = true\n\t\tend )\n\t\t\n\t\tResume( fade_thread )\n\t\t\n\t\tlocal intro_thread = NewThread( function()\n\t\t\twhile not start_intro_thread do\n\t\t\t\tWait( 10 )\n\t\t\tend\n\t\t\t--SetSkipKey( function_skip, 100 )\n\t\t\tGame.info.can_join = false\n\t\t\tmapvar.tmp.cutscene = true\n\t\t\tmapvar.tmp.fade_complete = false\n\t\t\tGame.fade_in()\n\t\t\twhile not mapvar.tmp.fade_complete do\n\t\t\t\tWait(1)\n\t\t\tend\n\t\t\t--RemoveSkipKey()\n\t\t\t\n\t\t\tmapvar.tmp.cutscene = false\n\t\t\tmapvar.tmp.fade_complete = true\n\t\t\tEnablePlayerControl( true )\n\t\t\tMenu.unlock()\n\t\t\tGUI:show()\n\t\tend )\n\t\t\n\t\t--SetSkipKey( function_skip, 500 )\n\t\t\n\t\tResume( intro_thread )\n\tend\n"
	Editor.scripts.get_next_character = "\tLoader.level.char_iterator = (Loader.level.char_iterator or 0) + 1\n\treturn { Loader.level.characters[Loader.level.char_iterator], Loader.level.spawnpoints[Loader.level.char_iterator]}\n"
end

function Editor.extractScripts( filename )
	local state = ""
	local tmp, tmpval
	local scripts = {}
	local ivars = {}
	for line in io.lines("scripts/levelscripts/"..filename..".lua") do
		tmp, tmpval = string.match( line, "%-%-%$%(([^%)]+)%)=(.*)" )
		if tmp then
			ivars[string.lower(tmp)] = tmpval
		end
		tmp = string.match( line, "%-%-%$%(([^%)]+)%)%-" )
		if tmp then
			state = ""
		end
		if state ~= "" then
			table.insert( scripts[state], line )
		end
		tmp = string.match( line, "%-%-%$%(([^%)]+)%)%+" )
		if tmp then
			state = string.lower(tmp)
			scripts[ state ] = {}
		end
	end
	for k,v in pairs(scripts) do
		Editor.scripts[k] = table.concat( v, "\n" )
		if Editor.scripts[k] ~= "" then Editor.scripts[k] = Editor.scripts[k] .. "\n" end
	end
	for k,v in pairs(ivars) do
		vars[k] = v
	end
end

function Editor.extractVariables( filename )
	ret = {}
	local var, oper, value
	for line in io.lines( filename ) do
		var, oper, value = string.match( line, "%-%-%$%(([^%)]+)%)(%S)(.*)" )
		if var then
			var = string.lower(var)
			if not ret[var] then
				ret[var] = ""
			end
			if oper == "=" then
				ret[var] = value
			elseif oper == "." then
				ret[var] = ret[var] .. value .. "/n"
			elseif oper == "+" then
				ret[var] = (tonumber( ret[var] ) or 0) + (tonumber(value) or 0)
			elseif oper == "-" then
				ret[var] = (tonumber( ret[var] ) or 0) - (tonumber(value) or 0)
			end
		end
	end
	return ret
end

function Editor.ExportScripts( script )
	if script then
		if #Editor.scripts[script] > 0 then
			local out = io.open( "editor/"..(Editor.current_map or "map").."_"..script..".lua" , "w" )
			if not out then
				out = io.open( (Editor.current_map or "map").."_"..script..".lua" , "w" )
			end
			if not out then
				Log( "Cannot create script export file for script "..script )
				return false
			end
			local osc = string.gsub(Editor.scripts[script], "\n\t", "\n").."\n"
			osc = string.gsub( osc, "^\t", "" )
			out:write( osc )
			out:close()
			return true
		else
			return true
		end
	else
		local result = true
		for k, v in pairs( Editor.scripts ) do
			result = result and Editor.ExportScripts( k )
		end
		return result
	end
end

function Editor._ExportScript( script )
	local result = Editor.ExportScripts( script )
	if result then
		if not script or #Editor.scripts[script] > 0 then
			Editor.popup( "Scipts saved", function() Editor.held[keys.tab] = false Editor.changeState( Editor.STATE.RUNNING ) end )
		else
			Editor.popup( "Nothing to export", function() Editor.held[keys.tab] = false Editor.changeState( Editor.STATE.RUNNING ) end )
		end
	else
		Editor.popup( "Not all scripts are saved (see log)", function() Editor.held[keys.tab] = false Editor.changeState( Editor.STATE.RUNNING ) end )
	end
end

function Editor.ExportImportScript( script, name )
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 5, 5 ) )
	l_window:add( MWCaption.create( {name="default", color={1,1,1,1}}, name, 0 ) )
	l_window:add( MWSpace.create( 5, 5 ) )
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Export",
			0,
			function()
				l_window:clear()
				l_window:hide()
				Editor._ExportScript( script )
				Editor.changeState( Editor.STATE.RUNNING )
			end, 
			nil, 
			{name="wakaba-widget"})
	)
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Import",
			0,
			function()
				l_window:clear()
				l_window:hide()
				Editor._ImportScript( script, nil, function() Editor.ScriptMenu() end )				
			end, 
			nil, 
			{name="wakaba-widget"})
	)
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Cancel",
			0,
			function()
				l_window:clear()
				l_window:hide()
				Editor.ScriptMenu()
			end, 
			nil, 
			{name="wakaba-widget"})
	)
	l_window:add( MWSpace.create( 5, 5 ) )
end

function Editor.ImportScript( script, filename )
	local scr = {}
	for line in io.lines( filename ) do
		table.insert( scr, line )
	end
	scr = "\t"..table.concat( scr, "\n\t" )
	if not string.match( scr, "\n$" ) then
		scr = scr .. "\n"
	end
	Editor.scripts[ script ] = scr
end

function Editor._ImportScript( script, event, cancel_event )
	Editor.path = "editor"
	Editor.show_dir( "", 
		function(a)
			if string.match(a, "%.lua$") then
				return true
			else
				return false
			end
		end, 
		function(a)
			return string.gsub(a, "%.lua$", "")
		end,
		function(filename)
			Editor.ImportScript( script, filename )
			Editor.changeState( Editor.STATE.RUNNING )
			if event then event() end
		end, 
		cancel_event)
end

function Editor.ScriptMenu()
	Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
	local d_window = Window.create( CONFIG.scr_width/2, CONFIG.scr_height/4, CONFIG.scr_width/4, CONFIG.scr_height/2, "window1" )
	local d_widget = CreateWidget( constants.wt_Label, "scipt description", nil, CONFIG.scr_width/2+15, CONFIG.scr_width/4+15, CONFIG.scr_width/4 - 30, CONFIG.scr_height/2 -30 )
	WidgetSetCaptionColor( d_widget, {1,1,1,1}, false )
	WidgetSetCaptionFont( d_widget, "dialogue" )
	local l_window = SimpleMenu.create( {CONFIG.scr_width/4, CONFIG.scr_height/2}, -1, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	
	local function _afw( var )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			var[1],
			0,
			function()
				l_window:clear()
				l_window:hide()
				d_window:destroy()
				DestroyWidget( d_widget )
				Editor.ExportImportScript( var[2], var[1] )
			end, 
			nil, 
			{name="wakaba-widget"},
			nil,
			function()
				WidgetSetCaption( d_widget, var[3], true )
			end ))
	end
	
	_afw { "InitScript", "init_script", "/ceeee66InitScript()/cffffff/n/nThis script is executed one time right after the start (or restart) of the level." }
	_afw { "MapScript", "map_script", "/ceeee66MapScript( /cffffffplayer/ceeee66 )/cffffff/n/nThis script execution is repeated as fast as possible during the gameplay. Note that you need to manually check if the game is paused or not./n/n/c22bb22player/cffffff - player object table, essentialy a result of GetPlayer()." }
	_afw { "WeaponBonus", "weapon_bonus", "/ceeee66WeaponBonus()/cffffff/n/nThis script is executed once player obtains his first weapon bonus on the map." }
	_afw { "PlaceSecrets", "place_secrets", "/ceeee66PlaceSecrets()/cffffff/n/nThis script is executed every time a character gains an ability to see secrets. Place your secret platforms and remove blocks on secret tunnels here." }
	_afw { "RemoveSecrets", "remove_secrets", "/ceeee66RemoveSecrets()/cffffff/n/nThis script is executed every time a character loses an ability to see secrets. You should probably reverse the results of PlaceSecrets() here." }
	_afw { "MapTrigger", "map_trigger", "/ceeee66map_trigger( /cffffffid/ceeee66, /cfffffftrigger/ceeee66 )/cffffff/n/nThis script is executed every time someone touches a generic trigger. Note that you should delete or otherwise disable the trigger in this script, or it would be called again and again until entering object leaves./n/n/c22bb22id/cffffff - id of the generic trigger/n/n/c22bb22trigger/cffffff - additional parameter for adding different scripts to different triggers." }
	_afw { "GetNextCharacter", "get_next_character", "/ceeee66GetNextCharacter()/cffffff/n/nThis script is executed every time a new character is needed (like a new player, entering the game). It should return a table like this: { proto_name, {x, y} }" }
	
	l_window:add( MWSpace.create( 5, 5 ) )

	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Export all scripts",
			0,
			function()
				l_window:clear()
				l_window:hide()
				d_window:destroy()
				DestroyWidget( d_widget )
				Editor._ExportScript()
				Editor.changeState( Editor.STATE.RUNNING )
			end, 
			nil, 
			{name="wakaba-widget"},
			nil,
			function()
				WidgetSetCaption( d_widget, "", true )
			end ))

	l_window:add( MWSpace.create( 5, 5 ) )
	
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Cancel",
			0,
			function()
				l_window:clear()
				l_window:hide()
				d_window:destroy()
				DestroyWidget( d_widget )
				Editor.changeState( Editor.STATE.RUNNING )
			end, 
			nil, 
			{name="wakaba-widget"},
			nil,
			function()
				--WidgetSetCaption( d_widget, var[3], true )
			end ))

	l_window:add( MWSpace.create( 15, 15 ) )
end

