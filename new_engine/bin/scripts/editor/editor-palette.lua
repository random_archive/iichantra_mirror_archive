Editor.palette = {}

Editor.TAB_NAMES =
{
	["BOX"] = "Color box",
	["PHYSIC GROUP"] = "Physic groups",
	["ACTIVE GROUP"] = "Areas",
	["ENEMY"] = "Enemies",
	["PLAYER"] = "Characters",
	["TILE"] = "Tile",
	["SPRITE"] = "Sprites",
	["ITEM"] = "Items",
	["RIBBON"] = "Ribbons",
	["ENVIRONMENT"] = "Environments",
	["FAVORITE"] = "Favorite",
	["RECENT"] = "Recently used",
}

Editor.EDITOR_TYPES =
{
	["BOX"] = Editor.TYPES.OBJ_SPRITE,
	["PHYSIC GROUP"] = Editor.TYPES.OBJ_SPRITE,
	["ACTIVE GROUP"] = Editor.TYPES.OBJ_ITEM,
	["ENEMY"] = Editor.TYPES.OBJ_ENEMY,
	["PLAYER"] = Editor.TYPES.OBJ_PLAYER,
	["TILE"] = Editor.TYPES.OBJ_TILE,
	["SPRITE"] = Editor.TYPES.OBJ_SPRITE,
	["ITEM"] = Editor.TYPES.OBJ_ITEM,
	["RIBBON"] = Editor.TYPES.OBJ_RIBBON,
	["ENVIRONMENT"] = Editor.TYPES.OBJ_ENVIRONMENT,
}


function Editor.ClearPalette( no_new )
	if Editor.palette.window then
		Editor.palette.window:destroy()
		for k, v in pairs(Editor.palette.widgets) do
			if type( v ) == 'table' then
				for k1, v1 in pairs( v ) do
					DestroyWidget( v1 )
					Editor.palette.elements[k1] = nil
				end
			else
				DestroyWidget( v )
			end
		end
		for k, v in pairs(Editor.palette.tab_widgets) do
			DestroyWidget( v )
		end
	end
	Editor.palette.active_pages = {}
	Editor.palette.window = Window.create( 0, 2*CONFIG.scr_height / 3, CONFIG.scr_width, CONFIG.scr_height / 3, "window1" )
	Editor.palette.window:setVisible(false)
	Editor.palette.area = { 0, 2*CONFIG.scr_height / 3, CONFIG.scr_width, CONFIG.scr_height }
	Editor.palette.tab_widgets = {}
	Editor.palette.widgets = {}
	
	local w = CreateWidget( constants.wt_Button, "PREV", nil, Editor.palette.area[1]+5, Editor.palette.area[2]+5, 64, 64 )
	WidgetSetSprite( w, "editor_misc", true )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaption( w, "Previous" )
	WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
	WidgetSetCaptionColor( w, { .6, 1, .6, 1 }, true )
	WidgetSetLMouseClickProc( w, function(sender)
		Editor.palette.PrevPage()
	end )
	WidgetSetZ( w, 1 )
	WidgetSetVisible( w, false )
	Editor.palette.widgets.prev = w
	w = CreateWidget( constants.wt_Button, "NEXT", nil, Editor.palette.area[3]-64-5, Editor.palette.area[4]-64-25, 64, 64 )
	WidgetSetSprite( w, "editor_misc", true )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaption( w, "Next" )
	WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
	WidgetSetCaptionColor( w, { .6, 1, .6, 1 }, true )
	WidgetSetLMouseClickProc( w, function(sender)
		Editor.palette.NextPage()
	end )
	WidgetSetZ( w, 1 )
	WidgetSetVisible( w, false )
	Editor.palette.widgets.next = w
	
	Editor.palette.elements = {}
	Editor.palette.tabs = {}
	Editor.palette.AddTab( "FAVORITE" )
	Editor.palette.AddTab( "RECENT" )
	Editor.palette.active_tab = "RECENT"
	Editor.palette.active_page = 1

	if not no_new then
		local result = Editor.palette.load( "editor/default.palette" )
		if not result then
			Log("[ERROR] Cannot load default palette, creating a new one.")
			Editor.SelectObject( "Color Box", "BOX", "" )
			Editor.SelectObject( "phys-empty", "PHYSIC GROUP", "proto/sprites/phys-empty.lua" )
			Editor.SelectObject( "phys-empty-cl", "PHYSIC GROUP", "proto/sprites/phys-empty-cl.lua" )
			Editor.SelectObject( "phys-empty-cl2", "PHYSIC GROUP", "proto/sprites/phys-empty-cl2.lua" )
			Editor.SelectObject( "sohchan", "PLAYER", "proto/characters/sohchan.lua" )
		end
	end

	Editor.palette.hide()
end

function Editor.palette.OrganizeTabs( active_widget )
	--local widget_x = {}
	--TODO: multiple pages and centering on active
	local x = Editor.palette.area[1] + 5
	local y = Editor.palette.area[2] - 15
	for i=1, #Editor.palette.tab_widgets do
		local sx, sy = GetCaptionSize( "dialogue", WidgetGetCaption( Editor.palette.tab_widgets[i] ) )
		WidgetSetPos( Editor.palette.tab_widgets[i], x, y )
		x = x + sx + 15
	end
end

function Editor.palette.save( filename )
	local out = io.open( filename, "w" )
	if not out then
		Log( "[ERROR] Cannot open file to save palette: "..filename )
		return false
	end
	for k, v in pairs( Editor.palette.elements ) do
		out:write( "TAB:"..k.."\n" )
		for k1, v1 in pairs(v) do
			if k == "BOX" then
				out:write( "\tELEMENT:BOX".."\n" )
			else
				out:write( "\tELEMENT:"..(v1.file or "wtf").."\n" )
			end
			if v1.extra then
				out:write( "\t\tPARAM:"..v1.extra.."\n" )
			end
			if v1.ptype then
				out:write( "\t\tTYPE:"..v1.ptype.."\n" )
			end
		end
	end
	out:close()
	return true
end

function Editor.palette.load( filename )
	Editor.ClearPalette( true )
	local last_element = {}
	local last_tab = "ERROR"
	local tst = io.open( filename, "r" )
	if tst then
		tst:close()
	else
		return false
	end
	for line in io.lines( filename ) do
		local key, param = string.match( line, "([A-Z]+):(.+)" )
		param = string.gsub( param, "%s+$", "" )
		if key == "TAB" then
			if last_element.file then
				Editor.palette.AddButton( last_tab, last_element.name, (last_element.ptype or "SPRITE"), last_element.file, last_element.extra )
			end
			last_element = {}
			if not Editor.palette.elements[param] then
				Editor.palette.AddTab( param )
			end
			last_tab = param
		elseif key == "ELEMENT" then
			if last_element.file then
				Editor.palette.AddButton( last_tab, last_element.name, (last_element.ptype or "SPRITE"), last_element.file, last_element.extra )
			end
			if param == "BOX" then
				Editor.palette.AddButton( last_tab, "Color Box", "BOX", "", nil )
			else
				last_element = { file = param, name = string.match( param, "/([^/]+).lua" ) }
			end
		elseif key == "PARAM" then
			last_element.extra = param
		elseif key == "TYPE" then
			last_element.ptype = param
		end
	end
	if last_element.file then
		Editor.palette.AddButton( last_tab, last_element.name, (last_element.ptype or "SPRITE"), last_element.file, last_element.extra )
	end
	return true
end

function Editor.palette.OpenTab( widget, tab )
	for k, v in pairs( Editor.palette.tab_widgets ) do
		WidgetSetCaptionColor( v, { .6, .6, .6, 1 }, false )
		WidgetSetCaptionColor( v, { .2, 1, .2, 1 }, true )
	end
	if widget then
		WidgetSetCaptionColor( widget, { 1, 1, 1, 1 }, false )
		WidgetSetCaptionColor( widget, { .6, 1, .6, 1 }, true )
	end
	if Editor.palette.active_tab and Editor.palette.widgets[Editor.palette.active_tab] then
		for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
			WidgetSetVisible( v, false )
		end
	end
	WidgetSetVisible( Editor.palette.widgets.prev, true )
	WidgetSetVisible( Editor.palette.widgets.next, true )
	Editor.palette.OrganizeTabs( widget )
	Editor.palette.active_tab = tab
	--Editor.palette.active_page = 1
	Editor.palette.active_page = Editor.palette.active_pages[tab] or 1
	WidgetSetVisible( Editor.palette.widgets.prev, true )
	WidgetSetVisible( Editor.palette.widgets.next, true )
	if Editor.palette.widgets[Editor.palette.active_tab] then
		for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
			if Editor.palette.elements[Editor.palette.active_tab][k].page == Editor.palette.active_page then
				WidgetSetVisible( v, true )
			end
		end
	end
end

function Editor.palette.NextPage()
	local found = false
	for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
		if Editor.palette.elements[Editor.palette.active_tab][k].page == Editor.palette.active_page + 1 then
			found = true
			break
		end
	end
	if not found then return end
	for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
		WidgetSetVisible( v, false )
	end
	Editor.palette.active_page = Editor.palette.active_page + 1
	Editor.palette.active_pages[Editor.palette.active_tab] = Editor.palette.active_page
	for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
		if Editor.palette.elements[Editor.palette.active_tab][k].page == Editor.palette.active_page then
			WidgetSetVisible( v, true )
		end
	end
end

function Editor.palette.PrevPage()
	if Editor.palette.active_page == 1 then return end
	for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
		WidgetSetVisible( v, false )
	end
	Editor.palette.active_page = Editor.palette.active_page - 1
	Editor.palette.active_pages[Editor.palette.active_tab] = Editor.palette.active_page
	for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
		if Editor.palette.elements[Editor.palette.active_tab][k].page == Editor.palette.active_page then
			WidgetSetVisible( v, true )
		end
	end
end

function Editor.palette.AddTab( tabname )
	tabname = tabname or "WTF"
	Editor.palette.widgets[tabname] = {}
	Editor.palette.elements[tabname] = {}
	local sx, sy = GetCaptionSize( "dialogue", (Editor.TAB_NAMES[tabname] or tabname) )
	local w = CreateWidget( constants.wt_Button, "TAB", nil, 0, 0, sx, sy )
	Editor.palette.tabs[tabname] = w
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaptionColor( w, { .6, 1, .6, 1 }, true )
	WidgetSetCaption( w, (Editor.TAB_NAMES[tabname] or tabname) )
	WidgetSetLMouseClickProc( w, function(sender)
		Editor.palette.OpenTab( sender, tabname )
	end )
	table.insert( Editor.palette.tab_widgets, w )
	Editor.palette.OpenTab( w, tabname )
end

function Editor.palette.ReorganizeTab( tab )
	local x = Editor.palette.area[1] + 5 + 64 + 30
	local y = Editor.palette.area[2] + 5
	local page = 1
	for i=1, #Editor.palette.widgets[tab] do
		WidgetSetPos( Editor.palette.widgets[tab][i], x, y )
		Editor.palette.elements[tab][i].page = page
		x = x + 64 + 30
		if y >= Editor.palette.area[4] - 64 - 64 - 20 - 20 and x > Editor.palette.area[3] - 64 - 5 - 64 - 30 then
			x = Editor.palette.area[1] + 5 + 64 + 30
			y = Editor.palette.area[2] + 5
			page = page + 1
		elseif x > Editor.palette.area[3] - 64 - 5 then
			x = Editor.palette.area[1] + 5
			y = y + 64 + 30
		end
	end
end

function Editor.palette.ContextMenu( name, ptype, full_path, extra )
	RemoveKeyProcessors()
	local mx, my = GetMousePos()
	local cx, cy = GetCamPos()
	mx, my = mx - cx + CONFIG.scr_width/2, my - cy + CONFIG.scr_height/2
	for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
		WidgetSetZ( v, 0.89 )
		WidgetSetBorder( v, false )
	end
	for k, v in pairs( Editor.palette.tab_widgets ) do
		WidgetSetVisible( v, false )
	end
	local restore_z = function()
		for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
			WidgetSetZ( v, 1 )
			WidgetSetBorder( v, true )
		end
		for k, v in pairs( Editor.palette.tab_widgets ) do
			WidgetSetVisible( v, true )
		end
	end
	local l_window = SimpleMenu.create( { mx, my }, -1, 1, 30, 10, 1 )
	local function fclose()
		l_window:clear()
		l_window:hide()
		RestoreKeyProcessors()
		restore_z()
	end
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Remove",
			0,
			function()
				fclose()
				for k, v in pairs( Editor.palette.elements[Editor.palette.active_tab] ) do
					if v.file == full_path and v.extra == extra then
						table.remove( Editor.palette.elements[Editor.palette.active_tab], k )
						DestroyWidget( table.remove( Editor.palette.widgets[Editor.palette.active_tab], k ) )
						break
					end
				end
				Editor.palette.ReorganizeTab(Editor.palette.active_tab)
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Add to favorites",
			0,
			function()
				fclose()
				Editor.palette.AddButton( "FAVORITE", name, ptype, full_path, extra )
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	local x, y = l_window.window.x, l_window.window.y
	local w, h  = l_window.window.w, l_window.window.h
	local func = function()
		local mx, my = GetMousePos()
		local cmx, cmy = GetCamPos()
		mx,my = mx - cmx + CONFIG.scr_width/2, my - cmy + CONFIG.scr_height/2
		if mx < x or mx > x + w or my < y or my > y + h then
			fclose()
		end
	end 
	GlobalSetMouseKeyDownProc( func )
	GlobalSetKeyDownProc( func )
end

function Editor.palette.AddButton( tab, name, ptype, full_path, extra )
	if full_path and not full_path == "" then
		local tst = io.open( full_path, "r" )
		if tst then
			tst:close()
		else
			return false
		end
	end
	if not Editor.palette.elements[tab] then
		Editor.palette.AddTab( tab )
	end
	for k, v in pairs( Editor.palette.elements[tab] ) do
		if v.file == full_path and v.extra == extra then
			return
		end
	end
	local info, sb = Editor.emulateObject( Editor.EDITOR_TYPES[tab], full_path )
	local w = CreateWidget( constants.wt_Button, "PALETTE_ELEMENT", nil, 0, 0, 64, 64 )
	if info and info.texture then
		if ptype == "TILE" and extra then info.num = extra end
		WidgetSetSprite( w, info.texture, true, info.num )
		WidgetSetSpriteColor( w, sb.color or {1, 1, 1, 1} )
		WidgetSetZ( w, 1 )
	elseif sb and sb.editor_color then
		WidgetSetColorBox( w, sb.editor_color )
	else
		WidgetSetColorBox( w, Editor.selection_color )
	end
	WidgetSetCaption( w, name or "WHAT" )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
	WidgetSetCaptionColor( w, { .6, 1, .6, 1 }, true )
	WidgetSetBorder( w, true )
	WidgetSetBorderColor( w, {0, 0, 0, 0}, false )
	WidgetSetBorderColor( w, {1, 1, 1, 1}, true )
	WidgetSetLMouseClickProc( w, function(sender)
		local func = Editor.creation_function[ptype] or function() Console.addMessage( "No creation function for object type '"..ptype.."'!" ) end
		local param = { proto = name, path = full_path, extra = extra }
		func( sender, param )
		
		for k, v in pairs( Editor.palette.elements["RECENT"] ) do
			if v.file == full_path then
				table.remove( Editor.palette.elements["RECENT"], k )
				table.remove( Editor.palette.widgets["RECENT"], k )
				break
			end
		end
		local info, sb = Editor.emulateObject( Editor.EDITOR_TYPES[ptype], full_path )
		Editor.palette.AddButton( "RECENT", name, ptype, full_path, extra )
		Editor.palette.hide()
	end )
	WidgetSetRMouseClickProc( w, function(sender) Editor.palette.ContextMenu( name, ptype, full_path, extra ) end )
	WidgetSetVisible( w, false )
	texture = nil
	editor_color = nil
	local next = #Editor.palette.elements[ tab ] + 1
	Editor.palette.elements[tab][next] = { file = full_path, page = 1, extra = extra, ptype = ptype }
	Editor.palette.widgets[tab][next] = w
	Editor.palette.ReorganizeTab( tab )
end

function Editor.SelectObject( name, ptype, full_path, extra )
	if ptype == "TILE" and not extra then
		local info, sb = Editor.emulateObject( Editor.EDITOR_TYPES[ptype], full_path )
		if not info or not info.texture then
			Editor.changeState( Editor.STATE.RUNNING )
			return
		end
		local inp = io.open( "textures/"..info.texture..".lua" )
		if not inp then
			Editor.changeState( Editor.STATE.RUNNING )
			return
		end
		local found = false
		local line = inp:read()
		while not found and line do
			found = string.match( line, "main%s*=" )
			line = inp:read()
		end
		inp:close()
		if not found then
			Editor.changeState( Editor.STATE.RUNNING )
			return
		end
		found = #Editor.sandboxFile( "textures/"..info.texture..".lua" ).main
		for i=0,found-1 do
			Editor.SelectObject( name, ptype, full_path, i )
		end
		return
	end

	Editor.changeState( Editor.STATE.RUNNING )
	texture = nil
	editor_color = nil
	
	Editor.palette.AddButton( ptype, name, ptype, full_path, extra )
	
	Editor.palette.OpenTab( Editor.palette.tabs[ptype], ptype )
	
	Editor.palette.hide()
end

function Editor.palette.hide()
	if Editor.palette.active_tab and Editor.palette.widgets[Editor.palette.active_tab] then
		for k, v in pairs( Editor.palette.widgets[Editor.palette.active_tab] ) do
			WidgetSetVisible( v, false )
		end
	end
	WidgetSetVisible( Editor.palette.widgets.prev, false )
	WidgetSetVisible( Editor.palette.widgets.next, false )
	for k, v in pairs( Editor.palette.tab_widgets ) do
		WidgetSetVisible( v, false )
	end
	Editor.palette.window:setVisible(false)
	WidgetSetVisible( Editor.widgets.main.selection, true )
	WidgetSetVisible( Editor.widgets.main.position, true )
end

function Editor.palette.show()
	for k, v in pairs( Editor.palette.tab_widgets ) do
		WidgetSetVisible( v, true )
	end
	Editor.palette.window:setVisible(true)
	Editor.palette.OpenTab( Editor.palette.tabs[Editor.palette.active_tab], Editor.palette.active_tab )
	WidgetSetVisible( Editor.widgets.main.selection, false )
	WidgetSetVisible( Editor.widgets.main.position, false )
end

Editor.palette.saveMenu = function()
	Editor.menu2:hide() 
	Editor.menu:hide() 
	Editor.menu3:hide()
	Editor.popup_enter( "Enter palette name:", function( str )
			str = str or "palette"
			local file = io.open("editor/"..str..".palette")
			if file then
				Editor.confirm("File already exists. Do you want to overwrite it?", function() 
					Editor.palette.save( "editor/"..file..".palette" )
					Editor.popup( "Palette saved.", 
					function() 
						Editor.changeState( Editor.STATE.RUNNING )
						Editor.held[keys.tab] = false 
					end )
				end, Editor.palette.saveMenu)
			else
				Editor.palette.save( "editor/"..str..".palette" )
				Editor.popup( "Palette saved.", 
					function() 
						Editor.changeState( Editor.STATE.RUNNING )
						Editor.held[keys.tab] = false 
					end )
			end
		end,
		function()
			Editor.changeState( Editor.STATE.RUNNING )
			Editor.held[keys.tab] = false 
		end, "palette")
end

Editor.palette.openMenu = function()
	Editor.menu2:hide() 
	Editor.menu:hide() 
	Editor.menu3:hide()
	Editor.path = "editor"
	RemoveKeyProcessors()
	Editor.show_dir( "editor", function( filename )
			if string.match( filename, "%.palette$" ) then
				return true
			end
			return false
		end,
		function(a) return string.gsub(a, "%.palette$", "") end,
		 function(filename)
			RestoreKeyProcessors()
			Editor.palette.load( filename )
			Editor.menu2:hide()
			Editor.held[keys.tab] = false 
		end, 
		function() 
			RestoreKeyProcessors() 
			Editor.changeState( Editor.STATE.RUNNING )
			Editor.held[keys.tab] = false 
		end )
end

function Editor.completePolygon( one_sided )
	local mark
	for k, v in pairs(Editor.selection) do
		if Editor.getObjParam( v.id, "POLYGON" ) then
			mark = v.id
			break
		end
	end
	if not mark then return end
	local poly = Editor.getObjParam( mark, "POLYGON" )
	local polygon = {}
	local o
	for i=1, #poly do
		o = GetObject(poly[i])
		polygon[i] = { o.aabb.p.x, o.aabb.p.y }
	end
	if #polygon < 3 then
		return Editor.popup( "Polygons cannot have less than 3 vertices.", function() end )
	end
	local hull = Editor.points_hull( polygon )
	local cx, cy = (hull[1] + hull[3])/2, (hull[2]+hull[4])/2
	local obj
	if one_sided then
		obj = CreateSprite( "phys-empty-cl", cx, cy )
	else
		obj = CreateSprite( "phys-empty", cx, cy )
	end
	for k,v in pairs( polygon ) do
		v[1] = v[1]-cx
		v[2] = v[2]-cy
	end
	local result = SetObjPolygon( obj, polygon )
	if not result then
		for i=1, math.floor( #poly/2 ) do
			polygon[i], polygon[ #poly-i+1 ] = polygon[ #poly-i+1 ], polygon[i]
		end
		result = SetObjPolygon( obj, polygon )
	end
	if not result then
		return Editor.popup( "Polygons must be convex.", function() end )
	end
	local event = { text = "polygon creation", type = "POLYGON_CREATE", polygon = GetObject( obj ), markers = {} }
	Editor.selectionSet( nil )
	Editor.selectionSet( GetObject( obj ), true )
	for k, v in pairs( poly ) do
		local o = GetObject( v )
		EditorSetLink( v, nil )
		table.insert( event.markers, o )
		Editor.BanishToVoid( o, true )
	end
	Editor.AddHistoryEvent( event )
end

function Editor.polygonMarker( x, y, polygon )
	local marker = CreateSprite( "editor_misc", x, y, "cross" )
	SetObjPos( marker, x, y )
	if polygon then
		Editor.setObjParam( marker, "POLYGON", polygon )
	else
		Editor.setObjParam( marker, "POLYGON", { marker } )
	end
	Editor.setObjParam( marker, "DONT_SAVE", true )
	Editor.setObjParam( marker, "DELETE_EVENT", function( obj )
		local poly = Editor.getObjParam( obj.id, "POLYGON" )
		local pos = 0
		for i=1,#poly do
			pos = i
			if poly[pos] == obj.id then
				break
			end
		end
		if pos == 1 then
			if #poly > 1 then
				EditorSetLink( poly[#poly], poly[2] )
			end
		elseif pos == #poly then
			EditorSetLink( poly[#poly-1], poly[1] )
		else
			EditorSetLink( poly[pos-1], poly[pos+1] )
		end
		table.remove( poly, pos )
		Editor.setObjParam( obj.id, "OLD_POLY_POS", pos )
		EditorSetLink( obj.id, nil )
	end )
	Editor.setObjParam( marker, "RESTORE_EVENT", function( obj )
		local poly = Editor.getObjParam( obj.id, "POLYGON" )
		local pos = Editor.getObjParam( obj.id, "OLD_POLY_POS" )
		if pos == 1 then
			if #poly > 0 then
				EditorSetLink( poly[#poly], obj.id )
				EditorSetLink( obj.id, poly[1] )
			end
		elseif pos > #poly then
			EditorSetLink( poly[#poly], obj.id )
			EditorSetLink( obj.id, poly[1] )
		else
			EditorSetLink( poly[pos-1], obj.id )
			EditorSetLink( obj.id, poly[pos] )
		end
		table.insert( poly, pos, obj.id )
		Editor.setObjParam( obj.id, "DONT_SAVE", true )
	end )
	SetObjSpriteColor( marker, {1,1,1,0.25} )
	return marker
end

function Editor.createPolygon()
	local event = { type = "CREATION", text = "Vertex creation" }
	local marker = Editor.polygonMarker( Editor.mouse_x, Editor.mouse_y )
	event.obj = { GetObject( marker ) }
	Editor.AddHistoryEvent( event )
	Editor.selectionSet( nil )
	Editor.selectionSet( GetObject(marker), true )
end

function Editor.decomposePolygon()
	local poly, obj
	for k, v in pairs(Editor.selection) do
		if v.polygon then
			if poly then
				return
			else
				obj = v
				poly = v.polygon
			end
		end
	end
	if not poly then return end
	local event = { type = "POLYGON_DECOMPOSE", text = "Alter polygon", polygon = obj, markers = {} }
	local polytab = {}
	local mark, last_mark
	for i=1, #poly do
		last_mark = mark
		mark = Editor.polygonMarker( obj.aabb.p.x + poly[i][1], obj.aabb.p.y + poly[i][2], polytab )
		table.insert( polytab, mark )
		table.insert( event.markers, GetObject( mark ) )
		if last_mark then
			EditorSetLink( last_mark, mark )
		end
	end
	EditorSetLink( mark, polytab[1] )
	Editor.BanishToVoid( obj )
	Editor.AddHistoryEvent( event )
end

function Editor.nextPolygonMarker()
	local mark, new_mark
	for k, v in pairs(Editor.selection) do
		if Editor.getObjParam( v.id, "POLYGON" ) then
			if mark then
				return
			else
				mark = v.id
			end
		end
	end
	if not mark then
		return
	end
	local poly = Editor.getObjParam( mark, "POLYGON" )
	local pos = 0
	for i = 1, #poly do
		pos = i+1
		if poly[i] == mark then
			break
		end
	end
	local event = { type = "CREATION", text = "Vertex creation" }
	new_mark = Editor.polygonMarker( Editor.mouse_x, Editor.mouse_y, poly )
	event.obj = { GetObject( new_mark ) }
	Editor.AddHistoryEvent( event )
	SetObjSpriteColor( new_mark, {1,1,1,0.25} )
	if ( pos <= #poly ) then
		EditorSetLink( new_mark, poly[pos] )
	else
		EditorSetLink( new_mark, poly[1] )
	end
	EditorSetLink( mark, new_mark )
	table.insert( poly, pos, new_mark )
	Editor.selectionSet( nil )
	Editor.selectionSet( GetObject(new_mark), true )
end
