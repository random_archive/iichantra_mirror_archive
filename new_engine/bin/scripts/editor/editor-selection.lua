function Editor.InitSelection()
	Editor.selection = {}
	Editor.saved_selections = {}
end

function Editor.newSelection()
	Editor.saved_selections[0] = deep_copy(Editor.selection)
	Editor.selectionSet( nil )
end

function Editor.restoreSavedSelection( num )
	num = num or 0
	if not Editor.saved_selections[num] then return end
	local old_selection = deep_copy( Editor.selection )
	local new_selection = deep_copy( Editor.saved_selections[num] )
	for k, v in pairs( old_selection ) do
		Editor.selectionSet( v, false )
	end
	for k, v in pairs( new_selection ) do
		Editor.selectionSet( v, true )
	end
	Editor.saved_selections[0] = old_selection
end

function Editor.addSavedSelection( num )
	num = num or 0
	if not Editor.saved_selections[num] then return end
	for k, v in pairs( Editor.saved_selections[num] ) do
		if not Editor.inSelection( v ) then
			Editor.selectionSet( v, true )
		end
	end
end

function Editor.saveSelection( num )
	num = num or 10
	Editor.saved_selections[num] = deep_copy( Editor.selection )
	Editor.FloatyText( "Selection mapped to "..num )
end

function Editor.selectionSet( obj, state )
	if obj and (Editor.getObjParam( obj.id, "DONT_SELECT" ) or Editor.getObjParam( obj.id, "DEAD" )) then
		return
	end
	if not obj then
		for key, value in pairs( Editor.selection ) do
			if not Editor.getObjParam( value.id, "NO_BORDER" ) then
				SetPhysObjBorderColor(value.id, {1,0.5,0,1}, false)
			end
		end
		Editor.selection = {}
		WidgetSetCaption( Editor.widgets.main.selection, "NONE" )
		return
	end
	if state then
		if #Editor.selection == 0 then
			WidgetSetCaption( Editor.widgets.main.selection, (obj.proto or "SOMETHING WITHOUT A NAME") )
		else
			WidgetSetCaption( Editor.widgets.main.selection, (Editor.selection[1].proto or "SOMETHING WITHOUT A NAME").." AND "..(#Editor.selection).." MORE" )
		end
		table.insert( Editor.selection, obj )
		if not Editor.getObjParam( obj.id, "NO_BORDER" ) then
			SetPhysObjBorderColor( obj.id, Editor.selection_color, true )
		end
		Editor.UpdateGroup( obj, true )
	else
		for key, value in pairs( Editor.selection ) do
			if ( value.id == obj.id  ) then
				if not Editor.getObjParam( value, "NO_BORDER" ) then
					SetPhysObjBorderColor( value.id, {1,0.5,0,1}, false)
				end
				Editor.UpdateGroup( obj, false )
				table.remove( Editor.selection, key )
				break
			end
		end
	end
end

