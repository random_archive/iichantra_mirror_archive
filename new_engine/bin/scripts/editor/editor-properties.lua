function Editor.getProperties( otype, id )
	local o = GetObject( id )
	if otype == Editor.TYPES.OBJ_PLAYER or otype == Editor.TYPES.OBJ_ENEMY or otype == Editor.TYPES.OBJ_ITEM then
		return { {"x", "FLOAT", o.aabb.p.x}, {"y", "FLOAT", o.aabb.p.x}, {"stack", "STACK", Editor.getObjParam( id, "STACK" )} }
	end
	if otype == Editor.TYPES.OBJ_TILE then
		return { {"x", "FLOAT", o.aabb.p.x}, {"y", "FLOAT", o.aabb.p.y}, {"z", "FLOAT", o.sprite.z}, {"tile", "INT", o.sprite.frame} }
	end
	if otype == Editor.TYPES.OBJ_RIBBON then
		local s1 = iff( o.ubl, o.bl, "nil" )
		local s2 = iff( o.ubt, o.bt, "nil" )
		local s3 = iff( o.ubr, o.br, "nil" )
		local s4 = iff( o.ubb, o.bb, "nil" )
		return { {"x", "FLOAT", o.aabb.p.x}, {"y", "FLOAT", o.aabb.p.y}, {"z", "FLOAT", o.sprite.z}, {"k_x", "FLOAT", o.k.x}, 
		{"k_y", "FLOAT", o.k.y}, {"min x", "FLOAT", s1}, {"max x", "FLOAT", s3}, {"min y", "FLOAT", s2}, {"max y", "FLOAT", s4}, 
		{"repeat x", "BOOL", tostring(o.repeat_x)}, {"repeat y", "BOOL", tostring(o.repeat_y)}, {"use prototype", "BOOL", tostring(o.from_proto)} }
	end
	if otype == Editor.TYPES.OBJ_SPRITE then
		if not o.proto then
			return { {"x1", "FLOAT", o.aabb.p.x-o.aabb.W}, {"y1", "FLOAT", o.aabb.p.y-o.aabb.H}, 
			{"x2", "FLOAT", o.aabb.p.x+o.aabb.W}, {"y2", "FLOAT", o.aabb.p.y+o.aabb.H}, 
			{"color", "COLOR", "["..table.concat(o.sprite.color, ", ").."]"} }
		else
			return { {"x", "FLOAT", o.aabb.p.x}, {"y", "FLOAT", o.aabb.p.y}, {"stack", "STACK", Editor.getObjParam( id, "STACK" ) } }
		end
	end
	if otype == Editor.TYPES.OBJ_SPAWNER then
		return {
			{"x", "FLOAT", Editor.getObjParam( id, "PROP_X" )},
			{"y", "FLOAT", Editor.getObjParam( id, "PROP_Y" )},
			{"spawn direction", "LIST", Editor.getObjParam( id, "PROP_DIR" ), 
				{ [constants.dirNone]="none",
				  [constants.dirLeft]="from left" ,
				  [constants.dirRight]="from right",
				  [constants.dirUp]="from above",
				  [constants.dirDown]="from below",
				  [constants.dirHorizontal]="from left or right",
				  [constants.dirVertical]="from above or below",
				  [constants.dirAny]="from any direction" }},
			{"max enemy count", "INT", Editor.getObjParam( id, "PROP_COUNT" )},
			{"spawn delay", "INT", Editor.getObjParam( id, "PROP_DELAY" )},
			{"respawn delay", "INT", Editor.getObjParam( id, "RESPAWN_DELAY" )},
			{"area size", "FLOAT", Editor.getObjParam( id, "PROP_SIZE" )},
			{"respawn distance", "FLOAT", Editor.getObjParam( id, "PROP_RESET_DIST" )},
		}
	end
	return { {"x", "FLOAT", o.aabb.p.x}, {"y", "FLOAT", o.aabb.p.x}, {"stack value", "INT", "nil"} }
end

function Editor.setProperties( otype, id, properties )
	local o = GetObject( id )
	local p = properties
	local action
	if otype == Editor.TYPES.OBJ_PLAYER or otype == Editor.TYPES.OBJ_ENEMY or otype == Editor.TYPES.OBJ_ITEM then
		if p["x"] or p["y"] then
			action = { type = "MOVEMENT", from = {{ obj = o, pos = {x = o.aabb.p.x, y = o.aabb.p.y} }}, text = "Properties: position" }
			if p["x"] then
				o.aabb.p.x = tonumber(p["x"])
			end
			if p["y"] then
				o.aabb.p.y = tonumber(p["y"])
			end
			SetObjPos( o.id, o.aabb.p.x, o.aabb.p.y )
			action.to = { { obj = p, pos = {x = o.aabb.p.x, y = o.aabb.p.y} } }
			Editor.AddHistoryEvent( action )
		end
	elseif otype == Editor.TYPES.OBJ_TILE then
		if p["x"] or p["y"] or p["z"] then
			action = { type = "MOVEMENT", from = {{ obj = o, pos = {x = o.aabb.p.x, y = o.aabb.p.y, z = o.sprite.z} }}, text = "Properties: position" }
			if p["x"] then
				o.aabb.p.x = tonumber(p["x"])
			end
			if p["y"] then
				o.aabb.p.y = tonumber(p["y"])
			end
			if p["z"] then
				o.sprite.z = tonumber(p["z"])
			end
			SetObjPos( o.id, o.aabb.p.x, o.aabb.p.y, o.sprite.z )
			action.to = { { obj = p, pos = {x = o.aabb.p.x, y = o.aabb.p.y, z = o.sprite.z} } }
			Editor.AddHistoryEvent( action )
		end
		if p["tile"] then
			action = { type = "FRAME", from = {{ obj = o, frame = o.sprite.frame }}, text = "Properties: tile" }
			o.sprite.frame = tonumber(p["tile"])
			SetObjAnim( o.id, o.sprite.frame, false )
			action.to = {{ obj = o, frame = o.sprite.frame }}
			Editor.AddHistoryEvent( action )
		end
	elseif otype == Editor.TYPES.OBJ_SPRITE then
		if p["x"] or p["y"] then
			action = { type = "MOVEMENT", from = {{ obj = o, pos = {x = o.aabb.p.x, y = o.aabb.p.y} }}, text = "Properties: position" }
			if p["x"] then
				o.aabb.p.x = tonumber(p["x"])
			end
			if p["y"] then
				o.aabb.p.y = tonumber(p["y"])
			end
			SetObjPos( o.id, o.aabb.p.x, o.aabb.p.y )
			action.to = { { obj = o, pos = {x = o.aabb.p.x, y = o.aabb.p.y} } }
			Editor.AddHistoryEvent( action )
		end
		if p["color"] then
			action = { type = "SPRITE_COLOR", from = {{ obj = o, color = o.sprite.color }}, text = "Properties: color" }
			o.sprite.color = str2col( p["color"] )
			SetObjSpriteColor( o.id, o.sprite.color )
			action.to = { { obj = o, color = o.sprite.color } }
			Editor.AddHistoryEvent( action )
		end
	elseif otype == Editor.TYPES.OBJ_RIBBON then
		if p["x"] or p["y"] or p["z"] then
			action = { type = "MOVEMENT", from = {{ obj = o, pos = {x = o.aabb.p.x, y = o.aabb.p.y, z = o.sprite.z} }}, text = "Properties: position" }
			if p["x"] then
				o.aabb.p.x = tonumber(p["x"])
			end
			if p["y"] then
				o.aabb.p.y = tonumber(p["y"])
			end
			if p["z"] then
				o.sprite.z = tonumber(p["z"])
			end
			SetObjPos( o.id, o.aabb.p.x, o.aabb.p.y, o.sprite.z )
			action.to = { { obj = p, pos = {x = o.aabb.p.x, y = o.aabb.p.y, z = o.sprite.z} } }
			Editor.AddHistoryEvent( action )
		end
		if  p["k_x"] or p["k_y"] or p["min x"] or p["min y"] or p["max x"] or p["max y"] or p["repeat x"] or p["repeat y"] or p["use prototype"] then
			action = { obj = o, type = "RIBBON", from = deep_copy(o), text = "Properties: ribbon" }
			if p["k_x"] then o.k.x = tonumber(p["k_x"]) end
			if p["k_y"] then o.k.y = tonumber(p["k_y"]) end
			if p["min x"] then o.bl = tonumber(p["min x"]) o.ubl = not (tonumber(p["min x"]) == "nil") end
			if p["min y"] then o.bt = tonumber(p["min y"]) o.ubt = not (tonumber(p["min y"]) == "nil") end
			if p["max x"] then o.br = tonumber(p["max x"]) o.ubr = not (tonumber(p["max x"]) == "nil") end
			if p["max x"] then o.bb = tonumber(p["max y"]) o.ubb = not (tonumber(p["max y"]) == "nil") end
			if p["repeat x"] then o.repeat_x = (p["repeat x"] == "true") end
			if p["repeat y"] then o.repeat_y = (p["repeat y"] == "true") end
			local s1 = iff( o.ubl, o.bl, nil )
			local s2 = iff( o.ubt, o.bt, nil )
			local s3 = iff( o.ubr, o.br, nil )
			local s4 = iff( o.ubb, o.bb, nil )
			SetRibbonObjK( o.id, o.k.x, o.k.y )
			SetRibbonObjBounds( o.id, s1, s2, s3, s4 )
			SetRibbonObjRepitition( o.id, o.repeat_x, o.repeat_y )
			action.to = deep_copy(o)
			Editor.AddHistoryEvent( action )
		end
	elseif otype == Editor.TYPES.OBJ_SPAWNER then
		if p["x"] or p["y"] then
			if p["x"] then Editor.setObjParam( id, "PROP_X", p["x"] ) end
			if p["y"] then Editor.setObjParam( id, "PROP_Y", p["y"] ) end
			action = { type = "MOVEMENT", from = {}, to = {}, text = "Properties: position" }
			for k,v in pairs(Editor.getObjParam( id, "PERMAGROUP" )) do
				table.insert( action.from, { obj=v, pos = {x=v.aabb.p.x, y=v.aabb.p.y} } )
				if p["x"] then v.aabb.p.x = tonumber(p["x"]) end
				if p["y"] then v.aabb.p.y = tonumber(p["y"]) end
				SetObjPos( v.id, v.aabb.p.x, v.aabb.p.y )
				table.insert( action.to, { obj=v, pos = {x=v.aabb.p.x, y=v.aabb.p.y} } )
			end
			Editor.AddHistoryEvent( action )
		end
		action = { type = "PARAM_CHANGE", obj = o, from = deep_copy(Editor.getAllObjParam( id )), text = "Properties change" }
		if p["spawn direction"] then Editor.setObjParam( id, "PROP_DIR", p["spawn direction"] ) end
		if p["max enemy count"] then Editor.setObjParam( id, "PROP_COUNT", p["max enemy count"] ) end
		if p["spawn delay"] then Editor.setObjParam( id, "PROP_DELAY", p["spawn delay"] ) end
		if p["respawn delay"] then Editor.setObjParam( id, "RESPAWN_DELAY", p["respawn delay"] ) end
		if p["area size"] then Editor.setObjParam( id, "PROP_SIZE", p["area size"] ) end
		if p["respawn distance"] then Editor.setObjParam( id, "PROP_RESET_DIST", p["respawn distance"] ) end
		action.to = deep_copy(Editor.getAllObjParam( id ))
		Editor.AddHistoryEvent( action )
	end
	if p["obstruction"] then
		SetObjSolidToByte( id, p["obstruction"] )
	end
	if p["stack"] then
		local stack = p["stack"]
		--[[
		for i=0,#p["stack"]-1 do
			table.insert( stack, p["stack"][#p["stack"]-i] )
		end--]]
		if #stack == 0 then
			stack = nil
		else
			Editor.setObjParam( id, "SPECIAL_CREATION", true )
		end
		Editor.setObjParam( id, "STACK", stack )
	end
end

function Editor.sandboxFile( file )
	local sandbox = {}
	local lfile = loadfile( file )
	if not lfile then return nil end
	setfenv( lfile, sandbox )
	pcall( lfile )
	return sandbox
end

function Editor.emulateObject( otype, file )
	local sandbox = { math = math, difficulty = 1, string = string, table = table, constants = constants }
	local lfile = loadfile( file )
	if not lfile then return nil end
	setfenv( lfile, sandbox )
	pcall( lfile )
	local ret = { texture = sandbox.texture, num = 0 }
	if sandbox.parent then
		ret, _sandbox = ret, sandbox
		_ret, sandbox = Editor.emulateObject( otype, "proto/"..sandbox.parent..".lua" )
		if not _ret then
			return nil
		end
		if not ret.texture then ret.texture = _ret.texture end
		for k, v in pairs(_sandbox) do
			if k ~= "animations" then
				sandbox[k] = v
			end
		end
		if _sandbox.animations then
			if not sandbox.animations then sandbox.animations = _sandbox.animations 
			else
				for k, v in pairs( _sandbox.animations ) do
					sandbox.animations[k] = v
				end
			end
		end
	end
	local cn = 1
	local nm = "idle"
	local an
	local dur = 0
	if otype == Editor.TYPES.OBJ_ENEMY or otype == Editor.TYPES.OBJ_ITEM then nm = "init" end
	if not sandbox.animations then
		return ret, sandbox
	end
	if otype == Editor.TYPES.OBJ_TILE then
		return ret, sandbox
	end
	for k, v in pairs( sandbox.animations ) do
		if v.name == nm then
			an = v.frames
			break
		end
	end
	if not an then return ret, sandbox end
	while dur == 0 do
		dur = an[cn].dur or 0
		ret.num = an[cn].num or 0
		if an[cn].com and an[cn].com == constants.AnimComSetAnim then
			local txt = an[cn].txt or ""
			an = nil
			for k, v in pairs( sandbox.animations ) do
				if v.name == txt then
					an = v.frames
					break
				end
			end
			cn = 1
			if not an then
				return ret, sandbox
			end
		else
			cn = cn + 1
			if not an[cn] then
				return ret, sandbox
			end
		end
	end
	return ret, sandbox
end

function Editor.ClearParam()
	Editor.obj_param = {}
end

Editor.obj_param_default = {}

function Editor.getAllObjParam( id )
	return Editor.obj_param[id] or {}
end

function Editor.getObjParam( id, param )
	if not Editor.obj_param[id] then return Editor.obj_param_default[param] end
	return Editor.obj_param[id][param] or Editor.obj_param_default[param]
end

function Editor.setObjParam( id, param, value )
	if not Editor.obj_param[id] then
		Editor.obj_param[id] = {}
	end
	Editor.obj_param[id][param] = value
end

function Editor.SpawnerConversion( list, no_event )
	if #list == 0 then
		return
	end
	local action = { type = "SPAWNER_ENEMY_CONVERSION", to_spawner = true, obj = {}, text = "Enemy to spawner" }
	for k, v in pairs( list ) do
		if v.type == Editor.TYPES.OBJ_ENEMY and not Editor.getObjParam( v.id, "CONVERTED" ) then
			local selected = Editor.inSelection( v )
			local sp = CreateSpawner( v.proto, v.aabb.p.x, v.aabb.p.y, 1, 0, 0 )
			SetObjPos( sp, v.aabb.p.x, v.aabb.p.y, v.sprite.z+0.001 )
			local grp = { GetObject(sp), v }
			SetObjSpriteColor( v.id, {1,1,1,.5})
			Editor.setObjParam( v.id, "NO_BORDER", true )
			Editor.setObjParam( v.id, "PERMAGROUP", grp )
			Editor.setObjParam( v.id, "NO_PROPERTIES", true )
			Editor.setObjParam( v.id, "DONT_SAVE", true )
			Editor.setObjParam( v.id, "CONVERTED", true )
			Editor.setObjParam( sp, "PERMAGROUP", grp )
			Editor.setObjParam( sp, "PROP_OBJ", v.proto )
			Editor.setObjParam( sp, "PROP_X", v.aabb.p.x )
			Editor.setObjParam( sp, "PROP_Y", v.aabb.p.y )
			Editor.setObjParam( sp, "PROP_COUNT", 1 )
			Editor.setObjParam( sp, "PROP_DIR", constants.dirAny )
			Editor.setObjParam( sp, "PROP_DELAY", 0 )
			Editor.setObjParam( sp, "RESPAWN_DELAY", 0 )
			Editor.setObjParam( sp, "PROP_SIZE", 0 )
			Editor.setObjParam( sp, "PROP_RESET_DIST", 0 )
			table.insert( action.obj, v )
			if selected then Editor.selectionSet( GetObject(sp), true ) end
			SetPhysObjBorderColor(v.id, {1,0.5,0,0}, false)
		end
	end
	if not no_event then
		Editor.AddHistoryEvent( action )
		Editor.FloatyText( "Spawner conversion" )
	end
end

function Editor.FlipObjects()
	for k, v in pairs(Editor.selection) do
		if v.sprite then
			SetObjSpriteMirrored( v.id, not v.sprite.mirrored )
			v.sprite.mirrored = not v.sprite.mirrored
		end
	end
end

function Editor.EnemyConversion( list, no_event )
	if #list == 0 then
		return
	end
	local action = { type = "SPAWNER_ENEMY_CONVERSION", to_spawner = false, obj = {}, text = "Spawner to enemy" }
	for k, v in pairs( list ) do
		if v.type == Editor.TYPES.OBJ_ENEMY and Editor.getObjParam( v.id, "CONVERTED" ) then
			local grp = Editor.getObjParam( v.id, "PERMAGROUP" )
			local lv
			local selected = Editor.inSelection( v )
			for i = 1, #grp do
				if grp[i].type == Editor.TYPES.OBJ_SPAWNER then
					lv = grp[i]
					break
				end
			end
			table.insert( action.obj, v )
			Editor.setObjParam( lv.id, "PERMAGROUP", nil )
			if selected then Editor.selectionSet( lv, false ) end
			SetObjDead( lv.id )
			SetObjSpriteColor( v.id, v.sprite.color )
			Editor.setObjParam( v.id, "NO_BORDER", false )
			Editor.setObjParam( v.id, "PERMAGROUP", nil )
			Editor.setObjParam( v.id, "NO_PROPERTIES", false )
			Editor.setObjParam( v.id, "DONT_SAVE", false )
			Editor.setObjParam( v.id, "CONVERTED", false )
			if selected then SetPhysObjBorderColor( v.id, Editor.selection_color, true ) end
		end
	end
	if not no_event then
		Editor.AddHistoryEvent( action )
		Editor.FloatyText( "Enemy conversion" )
	end
end

function Editor.prepareObject( obj )
	if obj.type == Editor.TYPES.OBJ_SPAWNER then
		local en = CreateEnemy( obj.enemyType, obj.aabb.p.x, obj.aabb.p.y )
		local ent = GetObject( en )
		local grp = { obj, ent }
		local sp = obj.id
		Editor.setObjParam( sp, "PERMAGROUP", grp )
		Editor.setObjParam( sp, "PROP_OBJ", obj.enemyType )
		Editor.setObjParam( sp, "PROP_X", obj.aabb.p.x )
		Editor.setObjParam( sp, "PROP_Y", obj.aabb.p.y )
		Editor.setObjParam( sp, "PROP_COUNT", obj.maximumEnemies )
		Editor.setObjParam( sp, "PROP_DIR", obj.direction )
		Editor.setObjParam( sp, "PROP_DELAY", obj.enemySpawnDelay )
		Editor.setObjParam( sp, "RESPAWN_DELAY", obj.respawnDelay )
		Editor.setObjParam( sp, "PROP_SIZE", obj.enemySize )
		Editor.setObjParam( sp, "PROP_RESET_DIST", obj.respawn_dist )
		SetObjPos( en, obj.aabb.p.x, obj.aabb.p.y )
		SetObjSpriteColor( en, {1,1,1,.5})
		SetObjPos( obj.id, obj.aabb.p.x, obj.aabb.p.y, ent.sprite.z-0.001 )
		SetPhysObjBorderColor(en, {1,0.5,0,0}, false)
		Editor.setObjParam( en, "NO_BORDER", true )
		Editor.setObjParam( en, "PERMAGROUP", grp )
		Editor.setObjParam( en, "NO_PROPERTIES", true )
		Editor.setObjParam( en, "DONT_SAVE", true )
		Editor.setObjParam( en, "CONVERTED", true )
	end
	if Editor.specials_map[obj.id] and Editor.scripts[ "obj"..Editor.specials_map[obj.id].."_creation_script" ] then
		Editor.setObjParam( obj.id, "CREATION_SCRIPT", Editor.scripts[ "obj"..Editor.specials_map[obj.id].."_creation_script" ] )
		Editor.setObjParam( obj.id, "SPECIAL_CREATION", true )
	end
end

function Editor.ResizePhysic( dir )
	local change = { 0, 0 }
	local shift = 1
	if Editor.held[keys.lshift] then
		shift = 10
	end
	local change = { 0, 0 }
	if dir == 4 then
		change[1] = -shift
	elseif dir == 6 then
		change[1] = shift
	elseif dir == 8 then
		change[2] = -shift
	else
		change[2] = shift
	end
	local oldpos
	for k,v in pairs( Editor.selection ) do
		if v.group then
			oldpos = { v.aabb.p.x - v.aabb.W, v.aabb.p.y - v.aabb.H }
			v.aabb.W = v.aabb.W + change[1]/2
			v.aabb.H = v.aabb.H + change[2]/2
			SetObjRectangle( v.id, 2*v.aabb.W, 2*v.aabb.H )
			v.aabb.p.x = oldpos[1] + v.aabb.W
			v.aabb.p.y = oldpos[2] + v.aabb.H
			SetObjPos( v.id, v.aabb.p.x, v.aabb.p.y )
		end
	end
end
