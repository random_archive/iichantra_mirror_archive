Conversation = {}

Conversation.__index = Conversation

function Conversation.create( conv_table )
	local c = {}
	c.labels = {}
	c.tab = conv_table
	for k,v in pairs(conv_table) do
		if type(v) == 'table' then
			if v.type == "LABEL" then
				c.labels[v[1]] = k
			end
		end
	end
	c.phase = 1
	c.vars = {}
	c.portraits = {}
	setmetatable( c, Conversation )
	return c
end

function Conversation:reset()
	self.phase = 1
	self.vars = {}
end

function Conversation:start(pause)
	Resume( NewThread( function()
		if pause then
			push_pause( true )
		end
		local gui = GUI.visible
		GUI:hide()
		GUI:hideCombo()
		local line = self.tab[self.phase]
		local first_line = true
		self.window = CreateWidget( constants.wt_Panel, "CONVERSATION", nil, 0, 0, CONFIG.scr_width, 120 )
		WidgetSetFocusable( self.window, false )
		WidgetSetSprite( self.window, "window1" )
		WidgetSetSpriteColor( self.window, {0, 0, 0, 0.6 } )
		local text = CreateWidget( constants.wt_Label, "CONVERSATION", self.window, 10, 10, CONFIG.scr_width - 20, 100 )
		WidgetSetCaptionFont( text, "dialogue" )
		WidgetSetCaptionColor( text, { 1, 1, 1, 1 }, false )
		local advance = false
		while line do
			if type(line) == 'function' then
				local result = line( self.vars ) or self.phase+1 
				self.phase = self.labels[result] or result-1
				advance = true
			elseif type(line) == 'string' then
				local typer = true
				local delay = 100
				if first_line then
					delay = 500
				end
				WidgetUseTyper( text, true, 50, false, "ammo-pickup.ogg" )
				SetSkipKey( function(key)
					if isConfigKeyPressed(key, "gui_nav_accept") then
						if typer then
							typer = false
							WidgetUseTyper(text, true, 0, false)
							return true
						else
							advance = true
							return false
						end
					elseif isConfigKeyPressed(key, "gui_nav_decline") then
						typer = false
						WidgetUseTyper(text, true, 0, false)
						advance = true
						return false
					end
					return true
				end, delay )
				WidgetSetOnTyperEndedProc(text, function()
					typer = false
				end )
				WidgetSetCaption( text, dictionary_string(line), true )
				WidgetStartTyper(text)
				first_line = false
			elseif type(line) == 'table' then
				if not line.type or line.type == "PORTRAIT" then
					local side = "left"
					local x = 10
					local y = 10
					if line.right then
						side = "right"
						x = CONFIG.scr_width - 110
					end
					if not line.sprite then
						if self.portraits[side] then
							DestroyWidget( self.portraits[side] )
							self.portraits[side] = nil
						end
					else
						if self.portraits[side] then
							DestroyWidget( self.portraits[side] )
							self.portraits[side] = nil
						end
						self.portraits[side] = CreateWidget( constants.wt_Picture, "PORTRAIT", self.window, x, y, 100, 100 )
						WidgetSetSprite( self.portraits[side], line.sprite, side )
						WidgetSetSpriteRenderMethod( self.portraits[side], constants.rsmStretch )
					end
					local x1, y1, x2, y2 = 10, 10, CONFIG.scr_width - 10, 100
					if self.portraits.left then
						x1 = x1 + 110
					end
					if self.portraits.right then
						x2 = x2 - 110
					end
					WidgetSetPos( text, x1, y1 )
					WidgetSetSize( text, x2-x1, y2-y1 )
					advance = true
				elseif line.type == "LABEL" then
					advance = true
				elseif line.type == "SELECTION" then
					if line[3] and type( line[3] ) == 'function' then
						local WINDOW1_X = 30
						local WINDOW1_Y = 140
						local WINDOW2_X = 310
						local WINDOW2_Y = 140

						local sx, sy = GetCaptionSize( "dialogue", "I" )
						local window1 = CreateWidget( constants.wt_Panel, "", nil, WINDOW1_X, WINDOW1_Y, 250, 20 + ( sy + 10 ) * #line[1] - 10 )
						WidgetSetSprite( window1, "window1" )
						WidgetSetSpriteColor( window1, {0, 0, 0, 0.6 } )
						WidgetSetFocusable( window1, false )
						local window2 = CreateWidget( constants.wt_Panel, "", window1, WINDOW2_X, WINDOW2_Y, 250, 20 + ( sy + 10 ) * #line[2] - 10 )
						WidgetSetSprite( window2, "window1" )
						WidgetSetSpriteColor( window2, {0, 0, 0, 0.6 } )
						WidgetSetFocusable( window2, false )
						local pointer1 = CreateWidget( constants.wt_Picture, "", window1, WINDOW1_X+5, WINDOW1_Y+10, 16, 16 )
						WidgetSetSprite( pointer1, "wakaba-widget" )
						WidgetSetZ( pointer1, 1.1 )
						local pointer2 = CreateWidget( constants.wt_Picture, "", window2, WINDOW2_X+5, WINDOW2_Y+10, 16, 16 )
						WidgetSetSprite( pointer2, "wakaba-widget" )
						WidgetSetZ( pointer2, 1.1 )
						local y = WINDOW1_Y + 10
						wts = { {}, {} }
						for i = 1, #line[1] do
							local wat = CreateWidget( constants.wt_Label, "", window1, WINDOW1_X + 31, y, 1, 1 )
							WidgetSetCaptionColor( wat, { 1, 1, 1, 1 }, false )
							WidgetSetCaption( wat, dictionary_string( line[1][i] ) )
							wts[1][i] = wat
							y = y + sy + 10
						end
						local y = WINDOW2_Y + 10
						for i = 1, #line[2] do
							local wat = CreateWidget( constants.wt_Label, "", window2, WINDOW2_X + 31, y, 1, 1 )
							WidgetSetCaptionColor( wat, { 1, 1, 1, 1 }, false )
							WidgetSetCaption( wat, dictionary_string( line[2][i] ) )
							wts[2][i] = wat
							y = y + sy + 10
						end
						local sline1 = 1
						local sline2 = 1
						WidgetSetCaptionColor( wts[1][1], { 1, 1, 0.7, 1 }, false )
						WidgetSetCaptionColor( wts[2][1], { 1, 1, 0.7, 1 }, false )
						RemoveKeyProcessors()
						local pl1_done = false
						local pl2_done = false
						GlobalSetKeyReleaseProc( function( key )
							if key == CONFIG.key_conf[1].down or key == CONFIG.key_conf[1].gui_nav_next then
								if pl1_done then
									return
								end
								WidgetSetCaptionColor( wts[1][sline1], { 1, 1, 1, 1 }, false )
								sline1 = sline1 + 1
								if sline1 > #line[1] then
									sline1 = 1
								end
								WidgetSetCaptionColor( wts[1][sline1], { 1, 1, 0.7, 1 }, false )
								WidgetSetPos( pointer1, WINDOW1_X + 5, WINDOW1_Y + 10 + (sline1 - 1) * (sy + 10) )
							elseif key == CONFIG.key_conf[1].up or key == CONFIG.key_conf[1].gui_nav_prev then
								if pl1_done then
									return
								end
								WidgetSetCaptionColor( wts[1][sline1], { 1, 1, 1, 1 }, false )
								sline1 = sline1 - 1
								if sline1 < 1 then
									sline1 = #line[1]
								end
								WidgetSetCaptionColor( wts[1][sline1], { 1, 1, 0.7, 1 }, false )
								WidgetSetPos( pointer1, WINDOW1_X + 5, WINDOW1_Y + 10 + (sline1 - 1) * (sy + 10) )
							elseif key == CONFIG.key_conf[2].down or key == CONFIG.key_conf[2].gui_nav_next then
								if pl2_done then
									return
								end
								WidgetSetCaptionColor( wts[2][sline2], { 1, 1, 1, 1 }, false )
								sline2 = sline2 + 1
								if sline2 > #line[2] then
									sline1 = 2
								end
								WidgetSetCaptionColor( wts[2][sline2], { 1, 1, 0.7, 1 }, false )
								WidgetSetPos( pointer2, WINDOW2_X + 5, WINDOW2_Y + 10 + (sline2 - 1) * (sy + 10) )
							elseif key == CONFIG.key_conf[2].up or key == CONFIG.key_conf[2].gui_nav_prev then
								if pl2_done then
									return
								end
								WidgetSetCaptionColor( wts[2][sline2], { 1, 1, 1, 1 }, false )
								sline2 = sline2 - 1
								if sline2 < 1 then
									sline2 = #line[2]
								end
								WidgetSetCaptionColor( wts[2][sline2], { 1, 1, 0.7, 1 }, false )
								WidgetSetPos( pointer2, WINDOW2_X + 5, WINDOW2_Y + 10 + (sline2 - 1) * (sy + 10) )
							elseif key == CONFIG.key_conf[1].fire or key == CONFIG.key_conf[1].gui_nav_accept then
								pl1_done = not pl1_done
								if pl1_done and pl2_done then
									local ret = line[3]( { sline1, sline2 } )
									self.phase = self.labels[ret] or ret
									RestoreKeyProcessors()
									DestroyWidget( window1 )
									advance = true
								elseif pl1_done then
									WidgetSetCaptionColor( wts[1][sline1], { 1, 1, .2, 1 }, false )
									WidgetSetVisible( pointer1, false )
								else
									WidgetSetCaptionColor( wts[1][sline1], { 1, 1, .7, 1 }, false )
									WidgetSetVisible( pointer1, true )
								end
							elseif key == CONFIG.key_conf[2].fire or key == CONFIG.key_conf[2].gui_nav_accept then
								pl2_done = not pl2_done
								if pl1_done and pl2_done then
									local ret = line[3]( { sline1, sline2 } )
									self.phase = self.labels[ret] or ret
									RestoreKeyProcessors()
									DestroyWidget( window1 )
									advance = true
								elseif pl2_done then
									WidgetSetCaptionColor( wts[2][sline2], { 1, 1, .2, 1 }, false )
									WidgetSetVisible( pointer2, false )
								else
									WidgetSetCaptionColor( wts[2][sline2], { 1, 1, .7, 1 }, false )
									WidgetSetVisible( pointer2, true )
								end
							end
						end )
					elseif line[1] and line[2] and type( line[1] ) == 'table' and type( line[2] ) == 'table' then
						local WINDOW_X = 30
						local WINDOW_Y = 140
						local sx, sy = GetCaptionSize( "dialogue", "I" )
						local window = CreateWidget( constants.wt_Panel, "", nil, WINDOW_X, WINDOW_Y, 250, 20 + ( sy + 10 ) * #line[1] - 10 )
						WidgetSetSprite( window, "window1" )
						WidgetSetSpriteColor( window, {0, 0, 0, 0.6 } )
						WidgetSetFocusable( window, false )
						local pointer = CreateWidget( constants.wt_Picture, "", window, WINDOW_X+5, WINDOW_Y+10, 16, 16 )
						WidgetSetSprite( pointer, "wakaba-widget" )
						WidgetSetZ( pointer, 1.1 )
						local y = 150
						wts = {}
						for i = 1, #line[1] do
							local wat = CreateWidget( constants.wt_Label, "", window, WINDOW_X+31, y, 1, 1 )
							WidgetSetCaptionColor( wat, { 1, 1, 1, 1 }, false )
							WidgetSetCaption( wat, dictionary_string( line[1][i] ) )
							wts[i] = wat
							y = y + sy + 10
						end
						local sline = 1
						WidgetSetCaptionColor( wts[1], { 1, 1, 0.7, 1 }, false )
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function( key )
							if key == CONFIG.key_conf[1].down or key == CONFIG.key_conf[1].gui_nav_next then
								WidgetSetCaptionColor( wts[sline], { 1, 1, 1, 1 }, false )
								sline = sline + 1
								if sline > #line[1] then
									sline = 1
								end
								WidgetSetCaptionColor( wts[sline], { 1, 1, 0.7, 1 }, false )
								WidgetSetPos( pointer, WINDOW_X+5, WINDOW_Y + 10 + (sline - 1) * (sy + 10) )
							elseif key == CONFIG.key_conf[1].up or key == CONFIG.key_conf[1].gui_nav_prev then
								WidgetSetCaptionColor( wts[sline], { 1, 1, 1, 1 }, false )
								sline = sline - 1
								if sline < 1 then
									sline = #line[1]
								end
								WidgetSetCaptionColor( wts[sline], { 1, 1, 0.7, 1 }, false )
								WidgetSetPos( pointer, WINDOW_X+5, WINDOW_Y + 10 + (sline - 1) * (sy + 10) )
							elseif key == CONFIG.key_conf[1].fire or key == CONFIG.key_conf[1].gui_nav_accept then
								self.phase = self.labels[line[2][sline]] or line[2][sline]
								RestoreKeyProcessors()
								DestroyWidget( window )
								advance = true
							end
						end )
					else
						advance = true
					end
				end
			end
			while not advance do
				Wait( 1 )
			end
			self.phase = self.phase + 1
			advance = false
			line = self.tab[self.phase]
		end
		DestroyWidget( self.window )
		self.window = nil
		self.portraits = {}
		if gui then
			GUI:show()
		end
		if pause then
			pop_pause()
		end
	end ))
end
