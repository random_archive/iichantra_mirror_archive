local dbg = {}

dbg_set_printer = { ["__newindex"] = function(t, k, v) Log("Set ", k," ", v, " ", debug.traceback()); rawset(t,k,v) end }


local scale = 1
function dbg.keyRelProc(key)
--[[	if key == keys["9"] then
		if scale > 1 then
			scale = scale - 1
		else
			scale = scale / 2
		end
		SetCamScale(scale, scale)
	elseif key == keys["0"] then
		if scale > 1 then
			scale = scale + 1
		else
			scale = scale * 2
		end
		SetCamScale(scale, scale)
	end--]]
end
Loader.addSlaveKeyProcessor(dbg.keyRelProc)

function dbg.s1()
	SetCamScale(1,1)
end

function dbg.s2()
	SetCamScale(2,2)
end

function dbg.scale_omsk()
	Resume( NewMapThread( function()
		while true do
			SetCamScale( 2+0.5*math.sin(Loader.time/1000), 2+0.5*math.cos(Loader.time/2000) )
			SetCamAngle( 10*math.cos(Loader.time/1500) )
			Wait(1)
		end
	end ))
end

function dbg.show_obj_anim(id)
	Log("dbg.show_obj_anim(",id,")")
	if GetObject(id) then dbg.obj_anim_id = id else return "No object" end
	
	if not dbg.obj_anim_text then
		dbg.obj_anim_text = CreateWidget(constants.wt_Label, "dbg_oanim_lbl", nil, 5, 60, 100, 100)
	end
	
	WidgetSetCaptionColor( dbg.obj_anim_text, { 1, 1, 1, 1}, false )
	WidgetSetCaptionFont( dbg.obj_anim_text, "dialogue" )
	WidgetSetZ(dbg.obj_anim_text, 0.99)
	WidgetSetVisible(dbg.obj_anim_text, true)
end

function dbg.hide_obj_anim()
	if dbg.obj_anim_text then 
		DestroyWidget(dbg.obj_anim_text)
		dbg.obj_anim_text = nil
	end
end

function dbg.process()
	if dbg.obj_anim_text then
		local obj = GetObject(dbg.obj_anim_id)
		if obj then
			--Log(obj.sprite.cur_anim)
			WidgetSetCaption(dbg.obj_anim_text, obj.sprite.cur_anim .. "  " .. obj.sprite.currentFrame)
		end
	end

end

function dbg.sab()
	dbg.show_obj_anim(mapvar.tmp.boss)
end
	
function dbg.bonus(bns)
	local char1 = GetPlayer(1)
	local char2 = GetPlayer(2)
	local active = GetPlayer()
	local active_char = iff( active.id == char1.id, 1, 2 )
	local wpn = WeaponSystem(active.id, bns)
	if wpn then 
		SetPlayerAltWeapon(active_char,wpn)
	end
end	

function dbg.sq()
	dbg.bonus("square")
end
function dbg.ci()
	dbg.bonus("circle")
end	
function dbg.tr()
	dbg.bonus("triangle")
end
function dbg.di()
	dbg.bonus("diamond")
end
function dbg.li()
	dbg.bonus("line")
end
function dbg.ammo(count)
	AddBonusAmmo(GetPlayer().id, count)
end	
function dbg.health(count)
	AddBonusHealth(GetPlayer().id, count)
end
return dbg