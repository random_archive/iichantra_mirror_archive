local LEVEL = {}
InitNewGame()
dofile("levels/pear15winged_doom.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

LEVEL.secrets = 0

function LEVEL.cleanUp()
    if mapvar.tmp.boss_bar then
        DestroyWidget( mapvar.tmp.boss_bar )
        mapvar.tmp.boss_bar = nil
    end
end

function LEVEL.boss_done()
	GUI:hide()
	EnablePlayerControl( false )
	Menu:lock()
	Resume( NewMapThread( function()
		local copter = GetObjectUserdata( CreateEnemy("pear15copter", -17, -600) )
		SetObjPos( copter, -17, -600 )
		SetObjSpriteColor( copter, {1,1,1,1}, 0 )
		local pos = copter:aabb().p
		copter:vel{ x = 0, y = 3 }
		local o1, o2 = GetPlayerCharacter(1), GetPlayerCharacter(2)
		local player1ok, player2ok = false, false
		if not o2 then
			player2ok = true
		end
		while not (player1ok and player2ok) do
			if not player1ok then
				player1ok = GetPlayerCharacter(1):health() > 0
			end
			if not player2ok then
				player2ok = GetPlayerCharacter(2):health() > 0
			end
			Wait(1)
		end
		if o1 then
			o1:sprite_mirrored( o1:aabb().p.x > -17 )
		end
		if o2 then
			o2:sprite_mirrored( o2:aabb().p.x > -17 )
		end
		while pos.y < 36 do
			Wait(1)
			pos = copter:aabb().p
		end
		copter:vel{ x = 0, y = 0 }
		Wait(500)
		SetObjAnim( mapvar.tmp.pear15copter_door, "open", false )
		Wait(1000)
		local done1, done2 = false
		Game.MoveWithAccUntilX( o1, iff(o1:aabb().p.x > -17, {x = -1.75, y = 0}, {x = 1.75, y = 0}), -17, function()
			done1 = true
			SetObjInvisible( o1, true )
			o1:drops_shadow(false)
		end )
		if o2 then
			Game.MoveWithAccUntilX( o2, iff(o2:aabb().p.x > -17, {x = -1.75, y = 0}, {x = 1.75, y = 0}), -17, function()
			done2 = true
			SetObjInvisible( o2, true )
			o2:drops_shadow(false)
		end )
		else
			done2 = true
		end
		while not (done1 and done2) do
			Wait(1)
		end
		Wait( 500 )
		copter:acc{ x = 0, y = -2 }
		Wait( 3000 )
		Menu:unlock()
		local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
		ObjectPushInt( obj1, 28 )
		SetObjAnim( obj1, "script", false )
	end ))
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	SwitchLighting(true)
--
	local clouds1 = CreateRibbonObj("clouds1", 0.9, 1, 0, -240, -1.07, false);
	local clouds2 =  CreateRibbonObj("clouds2", 0.9, 1, 0, -240, -1.08, false);
	local clouds3 = CreateRibbonObj("clouds3", 0.9, 1, 0, -140, -1.06, false);
	local moon = CreateRibbonObj("clouds4", 1, 1, -512, -340, -1.09, false);
	SetRibbonObjRepitition(clouds1, true, false)
	SetRibbonObjRepitition(clouds2, true, false)
	SetRibbonObjRepitition(clouds3, true, false)
	SetRibbonObjRepitition(moon, false, false)
--
	clouds1 = GetObjectUserdata(clouds1)
	clouds2 = GetObjectUserdata(clouds2)
	clouds3 = GetObjectUserdata(clouds3)
	local clouds1_x = 0;
	local clouds2_x = 0;
	local clouds3_x = 0;
--
	Resume( NewMapThread( function()
		while true do 
			Wait( 5 )
			local wind = (mapvar.tmp.wind or {5, 0})[1]
			clouds1_x = clouds1_x - 1.7/4;
			clouds2_x = clouds2_x - 1.3/4;
			clouds3_x = clouds3_x - 1.5/4;
			SetObjPos(clouds1, clouds1_x, -240)
			SetObjPos(clouds2, clouds2_x, -240)
			SetObjPos(clouds3, clouds3_x, -140)
			local ver = math.random(1, 50)
			if ver == 1 then
				PlaySnd( "strike_short.ogg", true )
				local dangle = math.random(-0.1,0.1)
				local dx = math.random(-320, 320)
				local dy = math.random(-10, 10)
				local x, y = GetCamPos()
				local lght = CreateSprite("tunnel2", 0, 0)
				SetObjPos( lght, x + dx, y + dy - 50, -1 )
				SetObjRectangle( lght, 640*3, 640*3 )
				SetObjSpriteAngle( lght, dangle + 1 )
			end
		end
	end))
--
	local rain1 = CreateParticleSystem( "prain1", 0, 0, 32 )
	local rain2 = CreateParticleSystem( "prain2", 0, 0, 32 )
	local rain3 = CreateParticleSystem( "prain1", 0, 0, 32 )
	local rain4 = CreateParticleSystem( "prain2", 0, 0, 32 )
	AddParticleArea( rain1, {-700, -500, 500, 256} )
	Resume( NewMapThread( function()
		mapvar.tmp.wind = { 10, 0 }
		while true do
			mapvar.tmp.wind[1] = -math.max( 3, math.min( 30, mapvar.tmp.wind[1] + math.random( -3, 3 ) ) )/5
			SetWind( mapvar.tmp.wind )
			Wait( 100 )
		end
	end ))
--
	local boss = CreateEnemy( 'boss_wd', -5, -250 )
	
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	if GetPlayer() then
		local sx, sy = GetCaptionSize( "default", "������� 8" )
		local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text1"] = text1
		WidgetSetCaptionColor( text1, {1,1,1,1}, false )
		WidgetSetCaption( text1, "������� 8" )
		WidgetSetZ( text1, 1.05 )
		sx, sy = GetCaptionSize( "default", "�������� ���" )
		local text2 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text2"] = text2
		WidgetSetCaptionColor( text2, {1,1,1,1}, false )
		WidgetSetCaption( text2, "�������� ���" )
		WidgetSetZ( text2, 1.05 )
		CamMoveToPos( -100, 89+24-60 )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 120)
		SetCamLag(0.9)
		Game.AttachCamToPlayers( {
			{ {-420,-1000,396,1000},  {-420,-1000,396,1000} },
		} )
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		Game.info.can_join = false
		mapvar.tmp.cutscene = true
		
		SetDefaultActor( 1 )
		EnablePlayerControl( false )
		if mapvar.actors[2] then
			SetDefaultActor( 2 )
			EnablePlayerControl( false )
			SetDefaultActor( 1 )
		end
		Menu.lock()
		Resume( NewThread( function()
			Wait( 1 )
			GUI:hide()
			
			SetObjPos( mapvar.actors[1].char.id, -200,89 )
			if mapvar.actors[2] then
				SetObjPos( mapvar.actors[2].char.id, -200-30,89 )
			end
			
			Wait( 1000 )
			mapvar.tmp.fade_complete = false
			Game.fade_in()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end
			
			Wait( 200 )
			local cx, cy = GetCamPos()
			local actor = GetObjectUserdata(mapvar.actors[1].char.id)
			
			local cx, cy = GetCamPos()
			
			local conversation_end = function()
				mapvar.tmp.cutscene = false
				SetDefaultActor( 1 )
				EnablePlayerControl( true )
				Game.info.can_join = true
				PlayBackMusic("music/iie_boss.it")
				SetCamLag(0.9)
				Menu.unlock()
				SetObjAnim( boss, "start", true )
				GUI:show()
				--[[
				Game.AttachCamToPlayers( {
					{ {-420,-1000,396,1000},  {-420,-1000,396,1000} },
				} )
				--]]
			end
			local char1, char2 = Game.GetCharacters()
			local char1_talk = function()
				mapvar.tmp.talk = mapvar.tmp.talk or {}
				mapvar.tmp.talk[char1] = true
			end
			local char2_talk = function()
				mapvar.tmp.talk = mapvar.tmp.talk or {}
				mapvar.tmp.talk[char2] = true
			end
			Conversation.create( {
				function( var ) 
					if mapvar.actors[2] then
						return "BOTH-PLAYERS"
					elseif mapvar.actors[1].info.character == "pear15unyl" then
						return "UNYL-SINGLE"
					end
				end,
				{ sprite = "portrait-soh" },
				char1_talk,
				"���-���-���, �������� ���... ��! � �� ���� ������, ��� � �������...",
				conversation_end,
				nil,
				nil,
				{ type = "LABEL", "UNYL-SINGLE" },
				{ sprite = "portrait-unyl" },
				char1_talk,
				"������� ������ ����? �, �����, �������� ��� �����...",
				conversation_end,
				nil,
				nil,
				{ type = "LABEL", "BOTH-PLAYERS" },
				{ sprite = "portrait-soh" },
				char1_talk,
				"�����. � ��� ���� ��� �������� ���?",
				{ sprite = "portrait-unyl" },
				char2_talk,
				"�����, ��������? �� ����.",
				{ sprite = "portrait-soh" },
				char1_talk,
				"� ��� � ����� �� ����������, ��� ��� ������� �� ������� �����! �� � ��� ������ ����������� �����.",
				{ sprite = "portrait-unyl" },
				char2_talk,
				"������, ��� ��� �� ����?",
				conversation_end,
				nil,
			} ):start(false)
		end))
	end  
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
	--SetCamBounds(-420,396,-1000,1000)
	--SetCamUseBounds(true)
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	mapvar.actors[1].info.ending = 1
	Loader.ChangeLevel( "pear15ending" )
--$(MAP_TRIGGER)-
end

LEVEL.characters = {}
--LEVEL.spawnpoints = {{-99999,-99999}}
--LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-200,89},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.tips =
{
	"� ������� ����� ���� ��� �������.",
	"����� ������ � �����, ��� ����� ��� � ����.",
	"��� �������� ��� �����, �� ��� ������ ������."
}

LEVEL.solution = '���������� �� ����������� ��� �������� ���� - �� ����� �� ��������./n/n�� ������ ����� ����� ����� ��� ����� ����� ��������� ����� �� �������� ����� ��� ������. ����� �� ������� ������ - ������������ �� ���� �� ��� ������� � ����������� �������� ������. ����� �� �������� ��� ����� - ������� �������, ������������ ���������� �� ��� ��� ����� ������� �������� ����������. ����� �� ��������, �� �������� �� ���, � ����������������� �� ������ ������, ���� ��� �� ����������� � ������./n/n����� ����, ��� �� �������� �������� ��������, ������ ����� ����� ���������� � ��� ��� ��������. ����� �� ����� ������ ������ ����� ���������� � ������ �����, � �� �� �������.'

LEVEL.name = 'Winged Doom'

return LEVEL

