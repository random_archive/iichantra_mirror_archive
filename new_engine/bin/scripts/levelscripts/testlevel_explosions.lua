InitNewGame()

local LEVEL = {}


local x = -48
local dx = 32

GroupObjects( CreateSprite("phys_floor", x, 100), CreateSprite("phys_floor", x+dx*500, 100) )
GroupObjects( CreateSprite("phys_floor", x+10*dx, 50), CreateSprite("phys_floor", x+10*dx, 75) )

CreatePlayer("sohchan", 50, 0);

SetCamAttachedObj(GetPlayer().id);		-- Öåïëÿåì êàìåðó ê îáúåêòó
SetCamAttachedAxis(true, true);		-- Êàìåðà ñìåùàåòñÿ òîëüêî ïî îñè X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- Êàìåðà ñëåäèò çà ñåðåäèíîé íèäíåãî êðàÿ îáúåêòà
-- Ïðè ñìåíå àíèìàöèé y-êîîðäèíàòà íèæíåãî êðàÿ íå ìåíÿåòñÿ, òàêèì îáðàçîì, êàìåðà íå ïëÿøåò ïðè, íàïðèìåð, ïðèñåäàíèè
SetCamObjOffset(0, 60);	-- Çàäàåì ñìåùåíèè êàìåðû îòíîñèòåëüíî îáúåêòà

SetCamLag( 0.9 )

label = CreateWidget(constants.wt_Label, "Label1", nil, 10, 100, 200, 10)
WidgetSetCaption( label, "q - small, w - medium, e - big, r - shake" )
label = CreateWidget(constants.wt_Label, "Label1", nil, 10, 120, 200, 10)
WidgetSetCaption( label, "t - wound, y - death, u - explode" )
label = CreateWidget(constants.wt_Label, "Label1", nil, 10, 140, 200, 10)
WidgetSetCaption( label, "i - btard" )

function onRelease( key )
	local pl = GetPlayer()
	if key == keys.w then
		CreateBullet( "explosion", pl.aabb.p.x, math.random(-50, 150), pl.id, false, 0, 0 )
	elseif key == keys.e then
		CreateBullet( "explosion_big", pl.aabb.p.x, math.random(-50, 150), pl.id, false, 0, 0 )
	elseif key == keys.q then
		CreateBullet( "explosion_small", pl.aabb.p.x, math.random(-50, 150), pl.id, false, 0, 0 )
	elseif key == keys.r then
		ShakeCamera( math.random(0, 100), math.random(0, 50), math.random(250, 750), math.random(0.75, 1.25) )
	elseif key == keys.t then
		CreateParticleSystem( "pblood_gravity_small", pl.aabb.p.x,  pl.aabb.p.y )
	elseif key == keys.y then
		CreateParticleSystem( "pblood_gravity", pl.aabb.p.x,  pl.aabb.p.y )
		CreateParticleSystem( "pblood_gravity2", pl.aabb.p.x,  pl.aabb.p.y )
	elseif key == keys.u then
		CreateParticleSystem( "pmeat", pl.aabb.p.x,  pl.aabb.p.y )
		CreateParticleSystem( "pblood_gravity", pl.aabb.p.x,  pl.aabb.p.y )
		CreateParticleSystem( "pblood_gravity2", pl.aabb.p.x,  pl.aabb.p.y )
	elseif key == keys.i then
		CreateParticleSystem( "pblood_gravity", pl.aabb.p.x,  pl.aabb.p.y )
		CreateParticleSystem( "pblood_gravity2", pl.aabb.p.x,  pl.aabb.p.y )
		CreateEnemy( "btard_corpse", pl.aabb.p.x,  pl.aabb.p.y )
	end
end

GlobalSetKeyReleaseProc(onRelease)







----------------------------------------------------------------------------


function LEVEL.MapScript(player)
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

LEVEL.name = "chain1"

return LEVEL
