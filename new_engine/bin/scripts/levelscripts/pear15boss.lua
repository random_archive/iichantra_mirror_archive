local LEVEL = {}
InitNewGame()
dofile("levels/pear15boss.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

PlayBackMusic("music/iie_w2gh.it")

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
	end
	EnablePlayerControl( false )
	Menu.locked = true
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	showStageName( -1, function()  FadeIn(1000) EnablePlayerControl( true ) Menu.locked = false end )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-trigger', 274, -23 )
	local obj2 = CreateItem( 'generic-trigger', 411, 181 )
	GroupObjects( obj1, obj2 )
	ObjectPushInt( obj1, 1 )
	ObjectPushInt( obj1, 1 )
--$(SPECIAL_CREATION)-
end

function LEVEL.GetNextCharacter()
--$(GET_NEXT_CHARACTER)+
	Loader.level.char_iterator = (Loader.level.char_iterator or 0) + 1
	return { Loader.level.characters[Loader.level.char_iterator], Loader.level.spawnpoints[Loader.level.char_iterator]}
--$(GET_NEXT_CHARACTER)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		SetObjDead( id )
		Menu.showEndLevelMenu("pear15mountains")
	end
--$(MAP_TRIGGER)-
end

LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-73,89},}


function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'pear15boss'

return LEVEL
