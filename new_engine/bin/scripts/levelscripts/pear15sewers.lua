local LEVEL = {}
InitNewGame()
dofile("levels/pear15sewers.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

LEVEL.secrets = 15

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	mapvar.tmp.cutscene = true
	SwitchLighting(true)
	SetCamUseBounds( false )
	CamMoveToPos( -255, 811 )
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	if GetPlayer() then
		if mapvar.actors[2] then
			Info.RevealWeapon("soh")
			Info.RevealWeapon("unyl")
		else
			if mapvar.actors[1].info.character == "pear15unyl" then
				Info.RevealWeapon("unyl")
			else
				Info.RevealWeapon("soh")
			end
		end
		
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		
		local sx, sy = GetCaptionSize( "default", "������� 1" )
		local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text1"] = text1
		WidgetSetCaptionColor( text1, {1,1,1,1}, false )
		WidgetSetCaption( text1, "������� 1" )
		WidgetSetZ( text1, 1.05 )
		sx, sy = GetCaptionSize( "default", "�����������" )
		local text2 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text2"] = text2
		WidgetSetCaptionColor( text2, {1,1,1,1}, false )
		WidgetSetCaption( text2, "�����������" )
		WidgetSetZ( text2, 1.05 )
		CamMoveToPos( -255, 811 )
		EnablePlayerControl( false )
		Menu.lock()
		local special_btard
		local special_btard2
		local start_intro_thread = false
		local fade_thread = NewThread( function()
			Wait( 1 )
			GUI:hide()
			CamMoveToPos( -255, 811 )
			Wait( 1000 )
			mapvar.tmp.fade_complete = false
			Game.fade_in()
			RemoveSkipKey()
			start_intro_thread = true
		end )
	
		Resume( fade_thread )

		local intro_thread = NewThread( function()
			while not start_intro_thread do
				Wait( 10 )
			end
			local cx, cy = GetCamPos()
			special_btard = CreateEnemy( "special_btard", cx - CONFIG.scr_width - 64, 1005 )
			special_btard2 = CreateEnemy( "special_btard", 5586, -2173 )
			SetObjAnim( special_btard, "move_right", false )
			SetObjAnim( special_btard2, "move_left", false )
			while not ObjectOnScreen( special_btard ) do
				Wait(1)
			end
			while ObjectOnScreen( special_btard ) do
				Wait(1)
			end
			SetObjDead( special_btard )
			SetObjPos( mapvar.actors[1].char.id, -251, 124 )
			if mapvar.actors[2] then
				SetObjPos( mapvar.actors[2].char.id, -351, 74 )
			end
			local conversation_end = function()
				mapvar.tmp.cutscene = false
				SetDefaultActor( 1 )
				EnablePlayerControl( true )
				if Game.GetPlayersNum() > 1 then
					SetDefaultActor( 2 )
					EnablePlayerControl( true )
				end
				SetDefaultActor( 1 )
				PlayBackMusic("music/iie_w2gh.it")
				SetCamLag(0.9)
				Menu.unlock()
				GUI:show()
				if not Game.profile.tips then Game.profile.tips = {} end
				if not Game.profile.tips[1] then
					push_pause( true )
					Game.ShowTip( 1, true, function() 
						Game.ShowTip( 2, true, function()
							--We need to go deeper!
							Game.ShowTip( 3, true, function()
								Game.ShowTip( 4, true, function()
									Game.profile.tips[1] = true
									Saver:saveProfile()
									pop_pause()
								end )
							end )
						end )
					end )
				end
				Game.AttachCamToPlayers( {
					{ { -1761,    336, -1181,   942}, {  -2263, -10000, 10000,  2294 } },
					{ {  -789,   -386,  1381,   178}, { -99999,   -162, 99999,  2294 } },
					{ {  1967,   -448,  2765,   160}, { -99999,   -422, 99999,  2294 } },
					{ {  1473,   1348,  9087,  2316}, {   1473, -99999,  9081,  2294 } },
					{ {  8733,    174, 10047,  1046}, { -99999, -99999, 10057,  2294 } },
					{ {  8841,  -3284,  9931,  -692}, {  -3290, -99999,  9845,  -692 } },
					{ {  5113,  -2460,  7619, -1248}, {   5200, -99999, 99999,  2294 } },
					{ {-99999, -99999, 99999, 99999}, { -99999, -99999, 99999,  2294 } },
				} )
			end
			local y = 811
			--
			while y > 750 do
				y = y - 10
				CamMoveToPos( -255, y )
				Wait(1)
			end
			while math.abs(982-y) > 10 do
				y = y + (982-y) * 0.1
				CamMoveToPos( -255, y )
				Wait(1)
			end
			--
			RemoveSkipKey()
			Conversation.create( {
				function( var ) 
					if mapvar.actors[2] then
						return 10
					elseif mapvar.actors[1].info.character == "pear15unyl" then
						return 6
					end
				end,
				{ sprite = "portrait-soh" },
				"��������! �� ������, ������ �� �� ���� �� ����.",
				conversation_end,
				nil,
				{ sprite = "portrait-unyl" },
				"���� �� ���������? ���� �� �������� ��� ��������, ���� � ������������ ������ ����� ��������...",
				conversation_end,
				nil,
				{ sprite = "portrait-unyl" },
				"���� �� ���������? ���� �� �������� ��� ��������, ���� � ������������ ������ ����� ��������...",
				{ sprite = "portrait-soh" },
				"�����, ���� � ���, ��� ����������� �������� - ������ ��, ���� � ����������� ������ �����.",
				conversation_end
			} ):start(false)
		end)


		SetSkipKey( function( key )
			if key == CONFIG.key_conf[2].gui_nav_accept then
				return true
			end
			if not start_intro_thread then
				StopThread( fade_thread )
				GUI:hide()
				CamMoveToPos( -255, 811 )
				Game.reset_fade()
				start_intro_thread = true
				return true
			end
			StopThread( intro_thread )
			if special_btard then
				SetObjDead( special_btard )
			end
			if not special_btard2 then
				special_btard2 = CreateEnemy( "special_btard", 5586, -2173 )
				SetObjAnim( special_btard2, "move_left", false )
			end
			local char1 = GetPlayerCharacter( 1 )
			if char1:aabb().p.x < - 500 then
				SetObjPos( char1, -251, 124 )
				local char2 = GetPlayerCharacter( 2 )
				if char2 then
					SetObjPos( char2, -351, 74 )
				end
			end
			mapvar.tmp.cutscene = false
			SetDefaultActor( 1 )
			EnablePlayerControl( true )
			if Game.GetPlayersNum() > 1 then
				SetDefaultActor( 2 )
				EnablePlayerControl( true )
			end
			SetDefaultActor( 1 )
			PlayBackMusic("music/iie_w2gh.it")
			SetCamLag(0.9)
			Menu.unlock()
			GUI:show()
			if not Game.profile.tips then Game.profile.tips = {} end
			if not Game.profile.tips[1] then
				push_pause( true )
				Game.ShowTip( 1, true, function() 
					Game.ShowTip( 2, true, function()
						--We need to go deeper!
						Game.ShowTip( 3, true, function()
							Game.ShowTip( 4, true, function()
								Game.profile.tips[1] = true
								Saver:saveProfile()
								pop_pause()
							end )
						end )
					end )
				end )
			end
			Resume( NewThread( function()
				local pl2 = GetPlayerCharacter( 2 )
				if pl2 then
					while not ObjectOnScreen( pl2 ) do
						Wait(1)
					end
				end
				Game.AttachCamToPlayers( {
					{ { -1761,    336, -1181,   942}, {  -2263, -10000, 10000, 10000 } },
					{ {  -789,   -386,  1381,   178}, { -99999,   -162, 99999, 99999 } },
					{ {  1967,   -448,  2765,   160}, { -99999,   -422, 99999, 99999 } },
					{ {  1473,   1348,  9087,  2316}, {   1473, -99999,  9081,  2294 } },
					{ {  8733,    174, 10047,  1046}, { -99999, -99999, 10057, 99999 } },
					{ {  8841,  -3284,  9931,  -692}, {  -3290, -99999,  9845,  -692 } },
					{ {  5113,  -2460,  7619, -1248}, {   5200, -99999, 99999, 99999 } },
					{ {-99999, -99999, 99999, 99999}, {-99999,  -99999, 99999, 99999 } },
				} )
			end ))
		end, 500 )

		local area = CreateEnvironment( "default_environment", 8319, -1756 )
		GroupObjects( area, CreateEnvironment( "default_environment", 8660, -1535 ) )
		local ammo
		SetEnvironmentStats( area, { on_enter = function()
			local o1 = GetPlayerCharacter(1)
			local o2 = GetPlayerCharacter(2)
			if ((not o1) or o1:ammo() < 20) and ((not o2) or o2:ammo() < 20) and ((not ammo) or not ammo:object_present()) then
				ammo = GetObjectUserdata( CreateItem( "pear15-ammo", 8049, -1638 ) )
			end
		end } )
		
		Resume( intro_thread )
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateEnvironment( 'sewerslime', -1152, 1004 )
	local obj2 = CreateEnvironment( 'sewerslime', 3200, 1152 )
	GroupObjects( obj1, obj2 )
	local obj3 = CreateEnvironment( 'sewerslime', -9150, -9096 )
	local obj4 = CreateEnvironment( 'sewerslime', -8850, -8903 )
	GroupObjects( obj3, obj4 )
	local obj5 = CreateEnvironment( 'sewerslime', 3863, 1004 )
	local obj6 = CreateEnvironment( 'sewerslime', 5312, 1128 )
	GroupObjects( obj5, obj6 )
	local obj7 = CreateEnvironment( 'sewerslime', 5775, 1001 )
	local obj8 = CreateEnvironment( 'sewerslime', 6354, 1171 )
	GroupObjects( obj7, obj8 )
	local obj9 = CreateEnvironment( 'sewerslime', 6823, 1001 )
	local obj10 = CreateEnvironment( 'sewerslime', 8297, 1122 )
	GroupObjects( obj9, obj10 )
	local obj11 = CreateEnvironment( 'sewerslime', 8831, 1001 )
	local obj12 = CreateEnvironment( 'sewerslime', 9728, 1216 )
	GroupObjects( obj11, obj12 )
	local obj13 = CreateItem( 'generic-death-area', 1537, 2213 )
	local obj14 = CreateItem( 'generic-death-area', 9278, 2344 )
	GroupObjects( obj13, obj14 )
	local obj19 = CreateItem( 'generic-death-area', 8721, -974 )
	local obj20 = CreateItem( 'generic-death-area', 9364, -791 )
	GroupObjects( obj19, obj20 )
	
	local obj21 = CreateItem( 'generic-trigger', 3338, 815 )
	local obj22 = CreateItem( 'generic-trigger', 3441, 883 )
	GroupObjects( obj21, obj22 )
	ObjectPushInt( obj21, 1 )
	local obj23 = CreateItem( 'generic-trigger', 6869, -2327 )
	local obj24 = CreateItem( 'generic-trigger', 6932, -2255 )
	GroupObjects( obj23, obj24 )
	ObjectPushInt( obj23, 1 )
	local obj25 = CreateItem( 'generic-trigger', 3199, 1258 )
	local obj26 = CreateItem( 'generic-trigger', 3392, 1320 )
	GroupObjects( obj25, obj26 )
	ObjectPushInt( obj25, 1 )
	local obj27 = CreateItem( 'generic-trigger', -1834, 575 )
	local obj28 = CreateItem( 'generic-trigger', -1727, 732 )
	GroupObjects( obj27, obj28 )
	ObjectPushInt( obj27, 1 )
	local obj29 = CreateItem( 'generic-trigger', 2047, 202 )
	local obj30 = CreateItem( 'generic-trigger', 2176, 348 )
	GroupObjects( obj29, obj30 )
	ObjectPushInt( obj29, 1 )
	local obj31 = CreateItem( 'generic-trigger', 6092, 2047 )
	local obj32 = CreateItem( 'generic-trigger', 6528, 2176 )
	GroupObjects( obj31, obj32 )
	ObjectPushInt( obj31, 1 )
	local obj33 = CreateItem( 'generic-trigger', 7076, 1485 )
	local obj34 = CreateItem( 'generic-trigger', 7296, 1728 )
	GroupObjects( obj33, obj34 )
	ObjectPushInt( obj33, 1 )
	local obj35 = CreateItem( 'generic-trigger', 7972, 1491 )
	local obj36 = CreateItem( 'generic-trigger', 8345, 1716 )
	GroupObjects( obj35, obj36 )
	ObjectPushInt( obj35, 1 )
	local obj37 = CreateItem( 'generic-trigger', 6066, -20 )
	local obj38 = CreateItem( 'generic-trigger', 6127, 64 )
	GroupObjects( obj37, obj38 )
	ObjectPushInt( obj37, 1 )
	local obj39 = CreateItem( 'generic-trigger', 5798, -370 )
	local obj40 = CreateItem( 'generic-trigger', 5969, -227 )
	GroupObjects( obj39, obj40 )
	ObjectPushInt( obj39, 1 )
	local obj41 = CreateItem( 'generic-trigger', 5889, 477 )
	local obj42 = CreateItem( 'generic-trigger', 5954, 552 )
	GroupObjects( obj41, obj42 )
	ObjectPushInt( obj41, 1 )
	local obj43 = CreateItem( 'generic-trigger', 4762, -64 )
	local obj44 = CreateItem( 'generic-trigger', 4845, 108 )
	GroupObjects( obj43, obj44 )
	ObjectPushInt( obj43, 1 )
	local obj45 = CreateItem( 'generic-trigger', 8708, -1728 )
	local obj46 = CreateItem( 'generic-trigger', 8885, -1571 )
	GroupObjects( obj45, obj46 )
	ObjectPushInt( obj45, 1 )
	local obj47 = CreateItem( 'generic-trigger', 8088, -2578 )
	local obj48 = CreateItem( 'generic-trigger', 8169, -2431 )
	GroupObjects( obj47, obj48 )
	ObjectPushInt( obj47, 1 )
	local obj49 = CreateItem( 'generic-trigger', 1470, 522 )
	local obj50 = CreateItem( 'generic-trigger', 1506, 554 )
	GroupObjects( obj49, obj50 )
	ObjectPushInt( obj49, 1 )

	local obj51 = CreateItem( 'generic-trigger', 5041, -2354 )
	local obj52 = CreateItem( 'generic-trigger', 5170, -2064 )
	GroupObjects( obj51, obj52 )
	ObjectPushInt( obj51, 2 )
	
	local function CreatButten( pos, area )
		local env1 = CreateEnvironment( "default_environment", pos[1]-5, pos[2]-5 )
		local env2 = CreateEnvironment( "default_environment", pos[1]+5, pos[2]+5 )
		GroupObjects( env1, env2 )
		local butten = CreateSprite( "sewers-switch", pos[1], pos[2] )
		SetObjPos( butten, pos[1], pos[2] )
		SetEnvironmentStats( env1, {
			on_enter = function( ud, x, y, material, this )
				if mapvar.tmp[this] then return end
				if ud:type() == 1 then
					if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
					if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
					mapvar.tmp[ud].effect = CreateEffect("use", 0, 0, ud:id(), 1)
				end
			end,
			on_leave = function( ud, x, y, material, new_material, this )
				if ud:type() == constants.objPlayer then
					if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				end
			end,
			on_use = function( ud, x, y, this )
				if mapvar.tmp[this] then return end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if mapvar.actors[2] then
					if ( mapvar.tmp[GetPlayerCharacter(1)] and mapvar.tmp[GetPlayerCharacter(1)].effect ) then SetObjDead( mapvar.tmp[GetPlayerCharacter(1)].effect ) mapvar.tmp[GetPlayerCharacter(1)].effect = nil end
					if ( mapvar.tmp[GetPlayerCharacter(2)] and mapvar.tmp[GetPlayerCharacter(2)].effect ) then SetObjDead( mapvar.tmp[GetPlayerCharacter(2)].effect ) mapvar.tmp[GetPlayerCharacter(2)].effect = nil end
				end
				PlaySnd( "menu-focus.ogg", true )
				SetObjAnim( butten, "open", false )
				for k, v in pairs( mapvar.tmp.blocks ) do
					if v[2] > area[1] and v[2] < area[3] and v[3] > area[2] and v[3] < area[4] then
						SetObjAnim( v[1], "open", false )
					end
				end
				mapvar.tmp[this] = true
			end
		} )
	end

	CreatButten( {2331, 377}, {5741, 585, 5830, 758} )
	CreatButten( {5634, 1651}, {7893, -77, 7959, 134} )
	CreatButten( {5670, -1064}, {8167, -2576, 8233, -2418} )

	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		mapvar.tmp.secrets = ( mapvar.tmp.secrets or 0 ) + 1
	elseif trigger == 2 then
		mapvar.actors[1].info.track = "sewer"
		Game.ShowLevelResults("pear15hoverbike")
	end
	SetObjDead( id )
--$(MAP_TRIGGER)-
end

LEVEL.characters = {}
--LEVEL.spawnpoints = {{-251,124}}
LEVEL.spawnpoints = {{-99999,-99999}}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Sewers'

LEVEL.hints =
{
	"�� ������ ������� ����� ��� ����� - �������!",
	"��������� ����� ����� ���������� � ������������ ���� �����!",
	"������ ������ ��������� ������� ���-�� �� �����!",
	"����� ������ ���� ������� ����� ����� ����� ������!"
}

LEVEL.solution = '�� ������ ����� �������, ���� �� ���������� ������� ���������� � ������� �������. ���������� �� ������� ��������� � ���������� ������, ���� �� ������� ������ ����� � ������. ������� � � ����������� �������� �������./n/n����� ����, ��� �� ������� ����� ��������� ������������� ������� ����������� � ��������� ����� � ������ ��������, ����������� ���� � ������������ � ������� ����� c ����� ������������� "�������". ����������� �� �����./n/n�� ��� ����� ������ �� ������� � ������� ������ ������. ������������� ������ � ����� ������ �� ������� ������������ �� ���������� �� ���� ����./n/n��� ������ ����������� ����������� ���� � ����� �������, ���� �� ������ �������� ���������. ������������� � ��� �� ������� � ������������ ����� �� ������� �������� ������./n/n��� �������� ����� �������� ���������� � ����������� �������� ������ � ����� �� ������, �������� �� ������./n/n���� �� ��-���� ����� � ��������� �������, ��������� � ������� ���� � �������� ������� ���� � ���������, ����� ���������.'

return LEVEL
