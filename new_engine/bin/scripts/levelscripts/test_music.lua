--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/weapontest.lua")

CONFIG.backcolor_r = 0.011719
CONFIG.backcolor_g = 0.011719
CONFIG.backcolor_b = 0.011719
LoadConfig()

StopBackMusic()

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function target_damage( id, ammount )
	last_hit = ammount
	global_damage = (global_damage or 0) + ammount
end

local p_target = {
	texture = "snowman";
	z = 0.0;
	
	phys_max_x_vel = 5;
	phys_max_y_vel = 5;
	
	faction_id = 1;
	physic = 1;
	phys_solid = 0;
	phys_bullet_collidable = 1;
	
	mass = -1;
	
	animations = 
	{
		{
			name = "init";
			frames =
			{
				{ com = constants.AnimComRealW; param = 51 },
				{ com = constants.AnimComRealH; param = 78 },
				{ com = constants.AnimComSetAnim; txt = "idle" }
			}
		},
		{
			name = "idle";
			frames =
			{
				{ num = 0; dur = 1 }
			}
		},
		{
			name = "pain";
			frames =
			{
				{ com = constants.AnimComPop },
				{ com = constants.AnimComCallFunctionWithStackParameter; txt = "target_damage" },
				{ com = constants.AnimComSetAnim; txt = "idle" }
			}
		}
	}
}

function t()
	CreateEnemy( "crate", GetMousePos() )
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
	Loader.level.MapScript = function() end
	Resume( NewMapThread( function()
		--local dir, dirs = ListDirContents( "sounds/music" )
		--local mus = "music/"..dir[math.random(dirs+1, #dir)]
		local mus = "music/iie_win_jingle.it"
		PlayBackMusic( mus )
		local len = GetBackMusicLength()
		Log( mus, ", length: ", len, ", speed ", GetBackMusicSpeed() )
		Wait( len*1000 )
		mus = "music/iie_whbt.it"
		PlayBackMusic( mus )
		Log( mus, ", length: ", GetBackMusicLength(), ", speed ", GetBackMusicSpeed() )
		SetBackMusicSpeed( GetBackMusicSpeed() * 2 )


		return
	end ))
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function LEVEL.GetNextCharacter()
--$(GET_NEXT_CHARACTER)+
	Loader.level.char_iterator = (Loader.level.char_iterator or 0) + 1
	return { Loader.level.characters[Loader.level.char_iterator], Loader.level.spawnpoints[Loader.level.char_iterator]}
--$(GET_NEXT_CHARACTER)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
--$(MAP_TRIGGER)-
end

LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-81,81},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'weapontest'

return LEVEL
