--EDITABLE MAP

local LEVEL = {}
InitNewGame()
--dofile("levels/trialmap.lua")

CONFIG.backcolor_r = 0.090000
CONFIG.backcolor_g = 0.117000
CONFIG.backcolor_b = 0.035000
LoadConfig()

PlayBackMusic("music/01 - Megnetic Holes.mp3")

LEVEL.nextPlayer = 0;
LEVEL.spawnpoints = { {0, 150} }
LEVEL.characters = {"exracer"}

local maxy = 175;
local miny = 225;

local playerh = 0;
local playery = 25;

function bikeup(object)
	SetObjPos(object.id,object.aabb.p.x,math.min(math.max(object.aabb.p.y-1,maxy),miny),object.aabb.p.y/1000-0.5)
end

function bikedown(object)
	SetObjPos(object.id,object.aabb.p.x,math.min(math.max(object.aabb.p.y+1,maxy),miny),object.aabb.p.y/1000-0.5)
end

LEVEL.held = {}

local downproc = GlobalGetKeyDownProc()
local releaseproc = GlobalGetKeyDownProc()

function LEVEL.keyProcessor( key )
	if not LEVEL.held[key] then
		LEVEL.held[key] = true
		LEVEL.keyPress( key )
	end
end

function LEVEL.keyRelease( key )
	LEVEL.held[key] = false
end

GlobalSetKeyReleaseProc(LEVEL.keyRelease)
GlobalSetKeyDownProc(LEVEL.keyProcessor)

function LEVEL.keyPress( key )
	if key == keys["tilde"] and not Console.shown then
		Console.toggle()
	elseif key == keys.p then
		Log( string.format( "coord: %f %f", GetMousePos() ) )
	elseif Loader.state == 'game' and IsConfKey(key, config_keys.gui_nav_menu) and ( not Menu.locked or not Menu.locked[#Menu.locked] ) then
		Menu.mn = not Menu.mn
		if Menu.mn then
			push_pause( true )
		Menu:show(3)
			MakeWindowAppear( Menu, { CONFIG.scr_width/2, -CONFIG.scr_height }, { CONFIG.scr_width/2, CONFIG.scr_height/2 } )
		else
			pop_pause()
			MakeWindowDisappear( Menu.pages[3], { CONFIG.scr_width/2, 2*CONFIG.scr_height }, true )
		end
	end
	local ret = false
	for tkey, value in pairs(Loader.slaveKeyProcessors) do
		--ret = ret or value( key )
		ret = ret or tkey( key )
	end
	if Loader.bind[key] then Loader.bind[key]() end
	return ret
end

local veg1 = {
	name = "vegetable1";
	trajectory_type = constants.pttGlobalSine;
	trajectory_param1 = 0.05;
	trajectory_param2 = 0.05;
	physic = 1;
	phys_solid = 0;
	phys_bullet_collidable = 0;
	phys_ghostlike = 1;
	phys_max_x_vel = 0;
	phys_max_y_vel = 80;
	drops_shadow = 1;
	FunctionName = "CreateItem";
	-- Описание спрайта
	--LoadTexture("vegetable1.png");
	texture = "vegetable1";
	z = -0.5;
	image_width = 256;
	image_height = 128;
	frame_width = 32;
	frame_height = 32;
	frames_count = 16;

	animations = 
	{
		{ 
			-- Создание
			name = "init";
			frames = 
			{
				{ com = constants.AnimComSetProcessor; param = funcref },
				{ com = constants.AnimComRealX; param = 8 },
				{ com = constants.AnimComRealY; param = 44 },
				{ com = constants.AnimComRealW; param = 16 },
				{ com = constants.AnimComRealH; param = 15 },
				{ com = constants.AnimComSetTouchable; param = 1 },
				--{ dur = 100; num = 20; com = constants.AnimComCreateParticles; txt = "pteleport" },
				{ com = constants.AnimComSetAnim; txt = "idle" }	
			}
		},
		{ 
			-- Вращение
			name = "idle";
			frames = 
			{
				{ dur = 50; num = 0 },
				{ dur = 50; num = 1 },
				{ dur = 50; num = 2 },
				{ dur = 50; num = 3 },
				{ dur = 50; num = 4 },
				{ dur = 50; num = 5 },
				{ dur = 50; num = 6 },
				{ dur = 50; num = 7 },
				{ dur = 50; num = 8 },
				{ dur = 50; num = 9 },
				{ dur = 50; num = 10 },
				{ dur = 50; num = 11 },
				{ dur = 50; num = 12 },
				{ dur = 50; num = 13 },
				{ dur = 50; num = 14 },
				{ dur = 50; num = 15 },
				{ dur = 50; num = 16 },
				{ dur = 50; num = 17 },
				{ dur = 50; num = 18 },
				{ com = constants.AnimComLoop }	
			}
		},
		{ 
			-- Бонус
			name = "touch";
			frames = 
			{
				{ com = constants.AnimComGiveHealth; param = 10 },
				{ dur = 100 },
				{ com = constants.AnimComPlaySound; txt = "health-pickup.ogg" },
				{ com = constants.AnimComCallFunction; txt = "StabilizeSync"; param = 1 },
				{ param = function( obj )
					local pos = obj:aabb().p
					Game.AddCombo( 1 )
					Info.RevealEnemy( "VEG1" )
					Game.EnemyDead( 1, 0, "../items/vegetable1", { 10, 10 } ) 
					Game.ShowHealthGain(10, pos.x, pos.y-80 )
					Game.GainScore(GetObjectUserdata(Game.mapvar.actors[1].char.id), 100, pos.x, pos.y-100 )
					obj:solid_to( constants.physSprite + constants.physEnemy )
					obj:sprite_z( -0.4 )
				end },
				{ com = constants.AnimComDestroyObject }
			}
		},
	}
}

local veg2 = {
	name = "vegetable2";
	trajectory_type = constants.pttGlobalSine;
	trajectory_param1 = 0.05;
	trajectory_param2 = 0.05;
	physic = 1;
	phys_solid = 0;
	phys_bullet_collidable = 0;
	phys_ghostlike = 1;
	phys_max_x_vel = 0;
	phys_max_y_vel = 80;
	drops_shadow = 1;
	FunctionName = "CreateItem";
	-- Описание спрайта
	--LoadTexture("vegetable1.png");
	texture = "vegetable2";
	z = -0.5;
	image_width = 256;
	image_height = 128;
	frame_width = 32;
	frame_height = 32;
	frames_count = 16;

	animations = 
	{
		{ 
			-- Создание
			name = "init";
			frames = 
			{
				{ com = constants.AnimComSetProcessor; param = funcref },
				{ com = constants.AnimComRealX; param = 8 },
				{ com = constants.AnimComRealY; param = 44 },
				{ com = constants.AnimComRealW; param = 16 },
				{ com = constants.AnimComRealH; param = 15 },
				{ com = constants.AnimComSetTouchable; param = 1 },
				--{ dur = 100; num = 20; com = constants.AnimComCreateParticles; txt = "pteleport" },
				{ com = constants.AnimComSetAnim; txt = "idle" }	
			}
		},
		{ 
			-- Вращение
			name = "idle";
			frames = 
			{
				{ dur = 50; num = 0 },
				{ dur = 50; num = 1 },
				{ dur = 50; num = 2 },
				{ dur = 50; num = 3 },
				{ dur = 50; num = 4 },
				{ dur = 50; num = 5 },
				{ dur = 50; num = 6 },
				{ dur = 50; num = 7 },
				{ dur = 50; num = 8 },
				{ dur = 50; num = 9 },
				{ dur = 50; num = 10 },
				{ dur = 50; num = 11 },
				{ dur = 50; num = 12 },
				{ dur = 50; num = 13 },
				{ dur = 50; num = 14 },
				{ dur = 50; num = 15 },
				{ com = constants.AnimComLoop }	
			}
		},
		{ 
			-- Бонус
			name = "touch";
			frames = 
			{
				{ com = constants.AnimComGiveHealth; param = 20 },
				{ dur = 100 },
				{ com = constants.AnimComPlaySound; txt = "health-pickup.ogg" },
				{ com = constants.AnimComCallFunction; txt = "StabilizeSync"; param = 1 },
				{ param = function( obj )
					local pos = obj:aabb().p
					Game.AddCombo( 1 )
					Info.RevealEnemy( "VEG2" )
					Game.EnemyDead( 1, 0, "../items/vegetable2", { 10, 10 } ) 
					Game.ShowHealthGain( 20, pos.x, pos.y-80 )
					Game.GainScore(GetObjectUserdata(Game.mapvar.actors[1].char.id), 100, pos.x, pos.y-100 )
					obj:solid_to( constants.physSprite + constants.physEnemy )
					obj:sprite_z( -0.4 )
				end },
				{ com = constants.AnimComDestroyObject }
			}
		},
	}
}

local veg3 = {
	name = "vegetable3";
	trajectory_type = constants.pttGlobalSine;
	trajectory_param1 = 0.05;
	trajectory_param2 = 0.05;
	physic = 1;
	phys_solid = 0;
	phys_bullet_collidable = 0;
	phys_ghostlike = 1;
	phys_max_x_vel = 0;
	phys_max_y_vel = 80;
	drops_shadow = 1;
	FunctionName = "CreateItem";
	-- Описание спрайта
	--LoadTexture("vegetable1.png");
	texture = "vegetable3";
	z = -0.5;
	image_width = 256;
	image_height = 128;
	frame_width = 32;
	frame_height = 32;
	frames_count = 16;

	animations = 
	{
		{ 
			-- Создание
			name = "init";
			frames = 
			{
				{ com = constants.AnimComSetProcessor; param = funcref },
				{ com = constants.AnimComRealX; param = 8 },
				{ com = constants.AnimComRealY; param = 44 },
				{ com = constants.AnimComRealW; param = 16 },
				{ com = constants.AnimComRealH; param = 15 },
				{ com = constants.AnimComSetTouchable; param = 1 },
				--{ dur = 100; num = 20; com = constants.AnimComCreateParticles; txt = "pteleport" },
				{ com = constants.AnimComSetAnim; txt = "idle" }	
			}
		},
		{ 
			-- Вращение
			name = "idle";
			frames = 
			{
				{ dur = 50; num = 0; },
				{ dur = 50; num = 1; },
				{ dur = 50; num = 2; },
				{ dur = 50; num = 3; },
				{ dur = 50; num = 4; },
				{ dur = 50; num = 5; },
				{ dur = 50; num = 6; },
				{ dur = 50; num = 7; },
				{ dur = 50; num = 8; },
				{ dur = 50; num = 9; },
				{ dur = 50; num = 10;},
				{ dur = 50; num = 11;},
				{ dur = 50; num = 12;},
				{ dur = 50; num = 13;},
				{ dur = 50; num = 14;},
				{ dur = 50; num = 15;},
				{ dur = 50; num = 16;},
				{ dur = 50; num = 17;},
				{ dur = 50; num = 18;},
				{ dur = 50; num = 19;},
				{ dur = 50; num = 20;},
				{ dur = 50; num = 21;},
				{ dur = 50; num = 22;},
				{ com = constants.AnimComLoop }	
			}
		},
		{ 
			-- Бонус
			name = "touch";
			frames = 
			{
				{ com = constants.AnimComGiveHealth; param = 30 },
				{ dur = 100 },
				{ com = constants.AnimComPlaySound; txt = "health-pickup.ogg" },
				{ com = constants.AnimComCallFunction; txt = "StabilizeSync"; param = 1 },
				{ param = function( obj )
					local pos = obj:aabb().p
					Game.AddCombo( 1 )
					Info.RevealEnemy( "VEG3" )
					Game.EnemyDead( 1, 0, "../items/vegetable3", { 10, 10 } ) 
					Game.ShowHealthGain( 30, pos.x, pos.y-80 )
					Game.GainScore(GetObjectUserdata(Game.mapvar.actors[1].char.id), 100, pos.x, pos.y-100 )
					obj:solid_to( constants.physSprite + constants.physEnemy )
					obj:sprite_z( -0.4 )
				end },
				{ com = constants.AnimComDestroyObject }
			}
		},
	}
}

local veg4 = {
	name = "vegetable4";
	trajectory_type = constants.pttGlobalSine;
	trajectory_param1 = 0.05;
	trajectory_param2 = 0.05;
	physic = 1;
	phys_solid = 0;
	phys_bullet_collidable = 0;
	phys_ghostlike = 1;
	phys_max_x_vel = 0;
	phys_max_y_vel = 80;
	drops_shadow = 1;
	FunctionName = "CreateItem";
	-- Описание спрайта
	--LoadTexture("vegetable1.png");
	texture = "vegetable4";
	z = -0.5;
	image_width = 256;
	image_height = 128;
	frame_width = 32;
	frame_height = 32;
	frames_count = 16;

	animations = 
	{
		{ 
			-- Создание
			name = "init";
			frames = 
			{
				{ com = constants.AnimComSetProcessor; param = funcref },
				{ com = constants.AnimComRealX; param = 8 },
				{ com = constants.AnimComRealY; param = 44 },
				{ com = constants.AnimComRealW; param = 16 },
				{ com = constants.AnimComRealH; param = 15 },
				{ com = constants.AnimComSetTouchable; param = 1 },
				--{ dur = 100; num = 20; com = constants.AnimComCreateParticles; txt = "pteleport" },
				{ com = constants.AnimComSetAnim; txt = "idle" }	
			}
		},
		{ 
			-- Вращение
			name = "idle";
			frames = 
			{
				{ dur = 50; num = 0; },
				{ dur = 50; num = 1; },
				{ dur = 50; num = 2; },
				{ dur = 50; num = 3; },
				{ dur = 50; num = 4; },
				{ dur = 50; num = 5; },
				{ dur = 50; num = 6; },
				{ dur = 50; num = 7; },
				{ dur = 50; num = 8; },
				{ dur = 50; num = 9; },
				{ dur = 50; num = 10;},
				{ dur = 50; num = 11;},
				{ dur = 50; num = 12;},
				{ dur = 50; num = 13;},
				{ dur = 50; num = 14;},
				{ dur = 50; num = 15;},
				{ dur = 50; num = 16;},
				{ dur = 50; num = 17;},
				{ dur = 50; num = 18;},
				{ com = constants.AnimComLoop }	
			}
		},
		{ 
			-- Бонус
			name = "touch";
			frames = 
			{
				{ com = constants.AnimComGiveHealth; param = 40 },
				{ dur = 100 },
				{ com = constants.AnimComPlaySound; txt = "health-pickup.ogg" },
				{ com = constants.AnimComCallFunction; txt = "StabilizeSync"; param = 1 },
				{ param = function( obj )
					local pos = obj:aabb().p
					Game.AddCombo( 1 )
					Info.RevealEnemy( "VEG4" )
					Game.EnemyDead( 1, 0, "../items/vegetable4", { 10, 10 } ) 
					Game.ShowHealthGain( 40, pos.x, pos.y-80 )
					Game.GainScore(GetObjectUserdata(Game.mapvar.actors[1].char.id), 100, pos.x, pos.y-100 )
					obj:solid_to( constants.physSprite + constants.physEnemy )
					obj:sprite_z( -0.4 )
				end },
				{ com = constants.AnimComDestroyObject }
			}
		}
		
	}
}

function LEVEL.GameEvents()
	local t = {
		name = "border";
		physic = 1;
		phys_solid = 1;
		phys_ghostlike = 1;
		phys_bullet_collidable = 0;
		phys_max_x_vel = 3;
		phys_max_y_vel = 0;
		mass = -1;
		FunctionName = "CreateEnemy";
		-- Описание спрайта
		--LoadTexture("vegetable1.png");
		z = -0.001;
	}
	local proto = LoadPrototypeTable(t);

	local plane = CreateDummyItem()
	SetObjRectangle(plane,88,9);
	SetObjPos(plane,220,225);
	SetDynObjAcc(plane, 1, 0);
	ApplyProto(plane, proto)
	--
	SetCamAttachedObj(GetPlayer().id);
	SetCamObjOffset(-220, 0);
	--local ex = CreateEnemy("exracer",0,125);
	
	local vegetables = {veg1,veg2,veg3,veg4}
	
	local len = 1000;
	for i = 1, 50 do
		len = len + 200;
		
		local obj = CreateDummyItem()
		local proto2 = LoadPrototypeTable(vegetables[math.random(1,4)]);
		ApplyProto(obj, proto2)
		local vegetablex = len + math.random(-75,75);
		local vegetabley = 210+math.random(-25,25);		
		SetObjPos(obj,vegetablex,vegetabley,vegetabley/1000-0.5);
		SetObjAnim(obj, "init", true)
		
		local sh = CreateDummyItem()
		SetObjRectangle(sh,20,10);
		SetObjPos(sh,vegetablex,vegetabley+8);
		proto2 = LoadPrototypeTable(t);
		ApplyProto(sh, proto2)
		
		if len % 320 < 80 then
			proto2 = LoadPrototypeTable(vegetables[1]);
			n = math.random(2,4)
			for j = 1, n do
				--obj = CreateDummyItem()
				--ApplyProto(obj, proto2)
				--SetObjPos(obj,math.floor(len/320)*320+160+math.random(-60,60),-55+math.random(-40,40));
				--SetObjAnim(obj, "init", true)
				--
				local rib = CreateRibbonObj("vegetable1",0.3,0,math.floor(len/320)*320+140+math.random(-60,60)+80*j/(n+1)-40,-105+math.random(-40,60),-0.6,false)
				SetRibbonObjRepitition( rib, false, false )
				SetObjAnim(rib, 19, false)
			end
		end
	end

	--object = CreateDummyItem()
	--SetObjRectangle(object,20,1000);
	--SetObjPos(object,320+220,0);
	--SetDynObjAcc(object, 1, 0);
	--ApplyProto(object, proto)

	--[[
	
	local idCamObject = CreateDummyEnemy()
	Log(idCamObject:aabb().p.x)
	local objcam = GetObjectUserdata(idCamObject);
	Log(objcam)
	Log(objcam:aabb())
	objcam:gravity().x = 5;
	--idCamOpject:physic = 1;
	Log(idCamObject);
	
	SetCamAttachedObj(idCamObject);
	SetCamObjOffset(-225, 0);

	CreateO 
	--]]
	mapvar.tmp.labelbonus = CreateWidget(constants.wt_Label, "label", nil, 320-100, 50, 100*2, 15);
	WidgetSetCaptionColor(mapvar.tmp.labelbonus, {1,1,1,1}, false, {0,0,0,1});
	WidgetSetCaptionFont(mapvar.tmp.labelbonus, "default", 2);
	WidgetSetCaption(mapvar.tmp.labelbonus, "BONUS SCORE" ,false);
	mapvar.tmp.timebonus = 1000;
	local timeparam = 0;
	mapvar.tmp.timer = CreateWidget(constants.wt_Label, "timer", nil, 320-20, 80, 20*2, 15);
	WidgetSetCaptionColor(mapvar.tmp.timer, {1,1,1,1}, false, {0,0,0,1});
	WidgetSetCaptionFont(mapvar.tmp.timer, "default", 2);
	WidgetSetCaption(mapvar.tmp.timer, mapvar.tmp.timebonus ,false);
	Wait(1)
	GUI:show()
	while true do
		timeparam = timeparam + 1;
		if timeparam % 20 == 0 then
			mapvar.tmp.timebonus = mapvar.tmp.timebonus - 10;
			if mapvar.tmp.timebonus < 0 then mapvar.tmp.timebonus = 0 end
			WidgetSetCaption(mapvar.tmp.timer, mapvar.tmp.timebonus ,false);
		end
		local player = GetPlayer();
		
		if LEVEL.held[keys["up"]] and LEVEL.held[keys["up"]] == true then bikeup(player) end
		if LEVEL.held[keys["down"]] and LEVEL.held[keys["down"]] == true then bikedown(player) end
		
		SetObjPos(plane,player.aabb.p.x,player.aabb.p.y+15);
		--SetObjPos(ex,player.aabb.p.x,player.aabb.p.y);
		--SetDynObjVel(ex,player.vel.x,player.vel.y);
		--SetDynObjAcc(ex,player.acc.x,player.acc.y);
		
		--[[
		if player.aabb.p.x > camerax + 150 and player.vel.x > 0 then
			--SetObjPos(player.id, camerax + 150, player.aabb.p.y)
			SetObjVel(player.id, 0, 0);
		end
		--
		local amount = GetActorCount();
		local scalex = 1;
		local scaley = 1;
		local scale = 1;		
		local minx = GetPlayer().aabb.p.x;
		local maxx = GetPlayer().aabb.p.x;
		local miny = GetPlayer().aabb.p.y + GetPlayer().aabb.H - 60;
		local maxy = GetPlayer().aabb.p.y + GetPlayer().aabb.H - 60;
		for i = 1,amount do
			SetDefaultActor(i);
			local player = GetPlayer();
			local x = player.aabb.p.x;
			local y = player.aabb.p.y + player.aabb.H - 60;
			if y > 0 then
				SetObjPos(player.id, x, -100)
				break
			end
		end
		local x = (minx + maxx)/2;
		local y = (miny + maxy)/2;
		SetObjPos(idCamObject,x,y);
		SetCamAttachedObj(idCamObject);
		if maxx - minx > CONFIG.scr_width / 2 then
			scalex = CONFIG.scr_width / (maxx - minx + CONFIG.scr_width / 2 );
		end
		if maxy - miny + 60 > CONFIG.scr_height / 2 then
			scaley = CONFIG.scr_height / (maxy - miny + 60 + CONFIG.scr_height / 2 );
		end
		
		SetCamScale(scale,scale);
		--]]
		Wait(10)
	end
--]]
end

function LEVEL.cleanUp()
	if mapvar.tmp and mapvar.tmp.labelbonus then
		DestroyWidget(mapvar.tmp.labelbonus)
		mapvar.tmp.labelbonus = nil
	end
	if mapvar.tmp and mapvar.tmp.timer then
		DestroyWidget(mapvar.tmp.timer)
		mapvar.tmp.timer = nil
	end
	if mapvar.tmp and mapvar.tmp.timebonus then
		mapvar.tmp.timebonus = nil
	end
end 

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	CONFIG.backcolor_r = 0.000000;
	CONFIG.backcolor_g = 0.000000;
	CONFIG.backcolor_b = 0.000000;

--[[
	SetRibbonObjRepitition( CreateRibbonObj( "2011parallax3", -1.000000, 1.000000, -243, -234, -1.1, false ), true, true )
	local rib = CreateRibbonObj( "2011menu4", 0.15, 0, 0, -160-70, -1, false )
	SetRibbonObjRepitition( rib, true, false )
	rib = CreateRibbonObj( "2011menu3", 0.125, 0, 0, -140-80, -1, false )
	SetRibbonObjRepitition( rib, true, false )
	local rib = CreateRibbonObj( "2011menu2", 0.075, 0, 0, -120-100, -1, false )
	SetRibbonObjRepitition( rib, true, false )
	local rib = CreateRibbonObj( "2011menu1", -0.4, 0, 0, -200-20, -0.1, false )
	SetRibbonObjRepitition( rib, true, false )
--]]
	local rib = CreateRibbonObj("0st_plan2", 0., 0, 0, 230-450, -0.65, false);
	SetRibbonObjRepitition( rib, true, false )
	rib = CreateRibbonObj("1st_plan2", 0.3, 0, 0, 230-450-20, -0.70, false);
	SetRibbonObjRepitition( rib, true, false )
	rib = CreateRibbonObj("2nd_plan2", 0.45, 0, 0, 230-450-10, -0.75, false);
	SetRibbonObjRepitition( rib, true, false )
	rib = CreateRibbonObj("3nd_plan2", 0.5, 0, 0, 230-450, -0.8, false);
	SetRibbonObjRepitition( rib, true, false )
	rib = CreateRibbonObj("4nd_plan2", 0.6, 0, 0, 230-450, -0.85, false);
	SetRibbonObjRepitition( rib, true, false )
	rib = CreateRibbonObj("skyline01", 0.7, 0, 0, 180-450, -0.9, false);
	SetRibbonObjRepitition( rib, true, false )

	--Menu.register_weather = function ( id, weather_id )
	--end
	--Menu.create_weather = function ()
	--	CreateSprite( "weather-snow", 0, 0 )
	--end
	--SetWind( { 2, 0 } )
	--Weather.set_slave_weather_proc(Menu, Menu.register_weather, Menu.create_weather)
	--Weather.check_weather()
	
	--local some = CreateEnemy( "exracer", 0, 175 );
	--SetObjSpriteColor(some, {0, 0, 0, 0}); 
	--CreateEffect( "dust-hoverbike", 0, 0, ex, 1 )
	--sign = CreateSprite("hw_sign", 590, 70);
	--ObjectPushInt((ex or 1), (sign or 0));

	Game.AttachCamToPlayers( {
		{ {-99999,-99999,99999,240},  {-99999,-99999,99999,240} },
	} )
	
	
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
	Resume( NewMapThread( LEVEL.GameEvents ) )
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-trigger', 12000, -1000 )
	local obj2 = CreateItem( 'generic-trigger', 20000, 1000 )
	GroupObjects( obj1, obj2 )
	local object
	ObjectPushInt( obj1, 1 )
	ObjectPushInt( obj1, 1 )
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	GlobalSetKeyDownProc(downproc);
	GlobalSetKeyDownProc(releaseproc);
	SetObjDead( id )
	Game.GainScore(GetObjectUserdata(Game.mapvar.actors[1].char.id), mapvar.tmp.timebonus, 12300, 150-100 )
	if mapvar.tmp and mapvar.tmp.labelbonus then
		DestroyWidget(mapvar.tmp.labelbonus)
		mapvar.tmp.labelbonus = nil
	end
	if mapvar.tmp and mapvar.tmp.timer then
		DestroyWidget(mapvar.tmp.timer)
		mapvar.tmp.timer = nil
	end
	if mapvar.tmp and mapvar.tmp.timebonus then
		mapvar.tmp.timebonus = nil
	end
	Game.ShowLevelResults("trialmap")
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

function LEVEL.GetNextCharacter()
	if not Loader.level or not Loader.level.spawnpoints then
		return nil
	end
	if Game.actors[1] and Game.actors[1].char then
		Game.info.players = Game.info.players + 1
	else
		Game.info.players = 1
	end
	if Game.info.players == 1 then
		--vars.character or Game.info.first_player_character or 
		if true then
			return { "exracer", Loader.level.spawnpoints[1] or {0,0} }
		end
	else
		local pos = GetObjectUserdata( Game.mapvar.actors[1].char.id ):aabb().p
		if Game.actors[1].info.character == "pear15soh" then
			return { "pear15unyl", {pos.x, pos.y} }
		else
			return { "pear15soh", {pos.x, pos.y} }
		end
	end
end

LEVEL.name = 'exracer'

return LEVEL
