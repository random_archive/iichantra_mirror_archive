InitNewGame();			-- ������ ����� ����
difficulty = 1
---[[
LoadTexture("sohchan.png");
LoadTexture("bullets.png");
LoadTexture("flash-straight.png");
LoadTexture("flash-angle.png");
LoadTexture("grenade.png")
LoadTexture("grenade-explosion.png")
LoadPrototype("sohchan.lua");
LoadPrototype("sfg9000.lua");
LoadPrototype("sfg5000.lua");
LoadPrototype("flash-straight.lua");
LoadPrototype("flash-angle-up.lua");
LoadPrototype("flash-angle-down.lua");
LoadPrototype("explosion.lua");
--]]

LoadTexture("window1.png")
LoadPrototype("window1.lua");
LoadTexture("portrait-unyl.png")
LoadPrototype("portrait-unyl.lua");

---[[
LoadTexture("phys_floor.png");
LoadPrototype("phys_floor.lua");
--]]

---[[
LoadTexture("dust-run.png");
LoadPrototype("dust-run.lua");
LoadTexture("dust-land.png");
LoadPrototype("dust-land.lua");
LoadTexture("dust-stop.png");
LoadPrototype("dust-stop.lua");
--]]
---[[
LoadTexture("btard_o.png");
LoadPrototype("btard.lua");
LoadPrototype("btard-corpse.lua");
LoadPrototype("btard-punch.lua");
--]]
--[[
LoadTexture("pblood.png");
LoadPrototype("pblood.lua");
LoadPrototype("pblood-wound.lua");
--]]
---[[
LoadSound("blaster_shot.ogg");
LoadSound("foot-left.ogg");
LoadSound("foot-right.ogg");
LoadSound("land.ogg");
LoadSound("stop.ogg");
LoadSound("music\\iie_lab.it");
--]]

--LoadTexture("unyl_sprite.png");
--LoadPrototype("unylchan.lua");

--[[
LoadTexture("3nd_plan.png");
LoadPrototype("3nd_plan.lua");
--]]

-- ���������� ������
--[[
LoadTexture("clouds1.png")
LoadTexture("clouds2.png")
LoadTexture("clouds3.png")
LoadTexture("clouds4.png")
LoadPrototype("clouds1.lua")
LoadPrototype("clouds2.lua")
LoadPrototype("clouds3.lua")
LoadPrototype("clouds4.lua")


local rib = CreateRibbon("clouds4", 0, 150,1);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds3", 0, 150, 0.25);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds2", 0, 150, 0.6);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds1", 0, 150, 0.5);
SetRibbonAttatachToY(rib, true)
--]]

require("serialize")

CreatePlayer("sohchan", 50, 300);
--CreatePlayer("unylchan", 50, 300);

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 100);	-- ������ �������� ������ ������������ �������


local x = 0
local dx = 32

for i = 0,50 do
	CreateSprite("phys_floor", x + dx*i, 330);
end

local scrmodelbl = CreateWidget(constants.wt_Label, "scrmodelbl", nil, 20, 30, 70, 10)
--WidgetSetCaption(scrmodelbl, "")
WidgetSetCaptionColor(scrmodelbl, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(scrmodelbl, {1.0, 0.2, 0.2, 1.0}, true)

local fullscrbtn;

local function show_screen_mode()
	local str = ""
	str = string.format("window: %dx%d; bpp: %d screen: %dx%d; %s", 
		CONFIG.window_width, CONFIG.window_height, 
		CONFIG.bpp,
		CONFIG.scr_width, CONFIG.scr_height,
		(CONFIG.fullscreen == 1 and "fullscreen" or "windowed"))
	WidgetSetCaption(scrmodelbl, str)
	
	WidgetSetCaption(fullscrbtn, CONFIG.fullscreen == 0 and " make fullscreen" or " make windowed")
end


local function fullscr_toggle(sender)
	CONFIG.fullscreen = (CONFIG.fullscreen == 1 and 0 or 1)
	LoadConfig();
	show_screen_mode()
end

local function set_scr_640(sender)
	CONFIG.scr_width = 640
	CONFIG.scr_height = 480
	LoadConfig();
	show_screen_mode()
end

local function set_scr_800(sender)
	CONFIG.scr_width = 800
	CONFIG.scr_height = 600
	LoadConfig();
	show_screen_mode()
end

local function set_scr_1024(sender)
	CONFIG.scr_width = 1024
	CONFIG.scr_height = 768
	LoadConfig();
	show_screen_mode()
end

local function set_scr_1280(sender)
	CONFIG.scr_width = 1280
	CONFIG.scr_height = 1024
	LoadConfig();
	show_screen_mode()
end

local function set_win_640(sender)
	CONFIG.window_width = 640
	CONFIG.window_height = 480
	LoadConfig();
	show_screen_mode()
end

local function set_win_800(sender)
	CONFIG.window_width = 800
	CONFIG.window_height = 600
	LoadConfig();
	show_screen_mode()
end

local function set_win_1024(sender)
	CONFIG.window_width = 1024
	CONFIG.window_height = 768
	LoadConfig();
	show_screen_mode()
end

local function set_win_1280(sender)
	CONFIG.window_width = 1280
	CONFIG.window_height = 1024
	LoadConfig();
	show_screen_mode()
end

fullscrbtn = CreateWidget(constants.wt_Button, "fullscrbtn", nil, 20, 20, 150, 10)
WidgetSetCaption(fullscrbtn, "+")
WidgetSetCaptionColor(fullscrbtn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(fullscrbtn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(fullscrbtn, fullscr_toggle)


local scr640btn = CreateWidget(constants.wt_Button, "scr640btn", nil, 20, 40, 80, 10)
WidgetSetCaption(scr640btn, "640x480")
WidgetSetCaptionColor(scr640btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(scr640btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(scr640btn, set_scr_640)

local scr800btn = CreateWidget(constants.wt_Button, "scr800btn", nil, 110, 40, 80, 10)
WidgetSetCaption(scr800btn, "800x600")
WidgetSetCaptionColor(scr800btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(scr800btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(scr800btn, set_scr_800)

local scr1024btn = CreateWidget(constants.wt_Button, "scr1024btn", nil, 200, 40, 80, 10)
WidgetSetCaption(scr1024btn, "1024x768")
WidgetSetCaptionColor(scr1024btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(scr1024btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(scr1024btn, set_scr_1024)

local scr1280btn = CreateWidget(constants.wt_Button, "scr1280btn", nil, 290, 40, 80, 10)
WidgetSetCaption(scr1280btn, "1280x1024")
WidgetSetCaptionColor(scr1280btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(scr1280btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(scr1280btn, set_scr_1280)


local win640btn = CreateWidget(constants.wt_Button, "win640btn", nil, 20, 50, 80, 10)
WidgetSetCaption(win640btn, "640x480")
WidgetSetCaptionColor(win640btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(win640btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(win640btn, set_win_640)

local win800btn = CreateWidget(constants.wt_Button, "win800btn", nil, 110, 50, 80, 10)
WidgetSetCaption(win800btn, "800x600")
WidgetSetCaptionColor(win800btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(win800btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(win800btn, set_win_800)

local win1024btn = CreateWidget(constants.wt_Button, "win1024btn", nil, 200, 50, 80, 10)
WidgetSetCaption(win1024btn, "1024x768")
WidgetSetCaptionColor(win1024btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(win1024btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(win1024btn, set_win_1024)

local win1280btn = CreateWidget(constants.wt_Button, "win1280btn", nil, 290, 50, 80, 10)
WidgetSetCaption(win1280btn, "1280x1024")
WidgetSetCaptionColor(win1280btn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(win1280btn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(win1280btn, set_win_1280)

show_screen_mode()