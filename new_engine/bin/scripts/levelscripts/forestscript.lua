InitNewGame();

spread = nil
rocket = nil
rapid = nil

CONFIG.backcolor_r = 67/255;
CONFIG.backcolor_g = 153/255;
CONFIG.backcolor_b = 153/255;
LoadConfig();

dofile("scripts/tilesets/default.lua")
dofile("scripts/tilesets/forest.lua")

LoadTexture("clouds2-1.png")
LoadPrototype("clouds2-1.lua")
LoadTexture("clouds2-2.png")
LoadPrototype("clouds2-2.lua")
LoadTexture("clouds2-3.png")
LoadPrototype("clouds2-3.lua")
LoadPrototype("phys-empty-slope1.lua")

CreateRibbon("forest-back", 140, 190, 1, -1); 
CreateRibbon("clouds2-1", 140, 250, 0.9, -0.9);
CreateRibbon("clouds2-2", 140, 110, 0.8, -0.85);
CreateRibbon("clouds2-3", 140, 70, 0.7, -0.8);
CreateRibbon("forest-ribbon1", 0, 260, 0.4, 0.08); 
CreateRibbon("forest-ribbon2", 0, 260, 0.6, 0.12); 
CreateRibbon("forest-ribbon3", 0, 190, 0.2, 0.04); 

SetCamUseBounds(true)
-- SetCamBounds(left, right, top, bottom)
SetCamBounds(41, 2902, -9000, 461)
--SetCamBounds(41, 1955, -9000, 556)

LoadSound("grenade-launch.ogg");
LoadSound("grenade-bounce.ogg");
LoadSound("item-score.ogg");
LoadSound("item-weapon.ogg");
LoadSound("grenade-explosion.ogg");
LoadSound("blaster_shot.ogg");
LoadSound("foot-left.ogg");
LoadSound("foot-right.ogg");
LoadSound("land.ogg");
LoadSound("stop.ogg");
LoadSound("ammo-pickup.ogg");
LoadSound("health-pickup.ogg");
LoadSound("flame.ogg");


snow_falls = true;
snow_off = false;
bombs = false;
--snow = CreateSprite("generic-snow-area", -100, -900);
--GroupObjects(snow, CreateSprite("phys-empty", 2000, -900));
snow = CreateSprite("phys-empty", -100, -900);

LoadTexture("clouds1.png")
LoadTexture("clouds2.png")
LoadTexture("clouds3.png")
LoadTexture("clouds4.png")
LoadPrototype("clouds1.lua")
LoadPrototype("clouds2.lua")
LoadPrototype("clouds3.lua")
LoadPrototype("clouds4.lua")
LoadTexture("wall.png")
LoadPrototype("wall.lua")
LoadTexture("snow3.png")
LoadTexture("tree2.png")
LoadPrototype("snow3.lua")
LoadPrototype("bomb-waves.lua")
LoadPrototype("explosion-triple.lua")
LoadPrototype("column2.lua")
LoadPrototype("fireshot-heli.lua")
LoadPrototype("heli-chaingun.lua")

LoadPrototype("survival-lift.lua")
LoadPrototype("survival-sky.lua")
LoadPrototype("survival-land.lua")

dofile("levels/fufufu.lua")

timeout = 50;

checkpoint = {}
checkpoint.x = 0
checkpoint.y = 0
--SetPlayerRevivePoint(67, 316);	

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������

bounds = 1

relimited = false
--9861

--LoadSound("music\\iie_survivor.it");
--PlayBackMusic("iie_survivor")
old_snow = "FULL"

function CloseTo(object, x, y, eps)
	return ( math.abs(object.aabb.p.x - x) < eps ) and ( math.abs(object.aabb.p.y - y) < eps )
end

local LEVEL = {}

ccb = {82, 7900, -278, 945}
cb = {82, 7900, -278, 945}
ccs = 5
old_t = 0
health_time = 20
big_health_time = 40
rapid_time = 30
spread_time = 60
rocket_time = 90

function LEVEL.MapScript(player)

end


function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

return LEVEL