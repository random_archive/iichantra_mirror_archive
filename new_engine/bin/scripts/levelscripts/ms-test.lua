--EDITABLE MAP
InitNewGame();
require("layered_bar")

SetCamLag(0.9)

if not mapvar.tmp then
	mapvar.tmp = {}
end

mapvar.char = {}
mapvar.char.SOH = 1


StopBackMusic()

CONFIG.backcolor_r = 2/255;
CONFIG.backcolor_g = 0;
CONFIG.backcolor_b = 0;
LoadConfig();

local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)
dofile( "levels/ms-test.lua" );

SetCamUseBounds(true)
-- SetCamBounds(left, right, top, bottom)
SetCamBounds(-293, 982, -228, 246)

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������

mapvar.tmp.troll_deaths = 0

----------------------------------------------------------------------

local LEVEL = {}

function LEVEL.MapScript(player)
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.cleanUp() 
end 

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
end

function LEVEL.FFTDecorate( fft )
	if not mapvar.tmp.bars then
		mapvar.tmp.bars = {}
		-289, 991
		--for i=1,256 do
		--	mapvar.tmp.bars[i] = CreateWidget( constants.wt_Widget, "wut", nil, i*2, 50, 2, 1 )
		--WidgetSetColorBox( mapvar.tmp.bars[i], {0,0,1,1} )
	end
end

LEVEL.name = "TEST MAP"
LEVEL.spawn_points = { 
				{{-239, -176}, {931, -176}, {-239, 80}, {931, 80}}, 
				{{94, 80}, {144, 80}, {194, 80}}
			   }

return LEVEL
