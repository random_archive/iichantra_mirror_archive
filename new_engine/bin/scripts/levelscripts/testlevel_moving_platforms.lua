InitNewGame();


local LEVEL = {}
require("routines")
--TogglePause()

CONFIG.backcolor_r = 67/255;
CONFIG.backcolor_g = 153/255;
CONFIG.backcolor_b = 153/255;
LoadConfig();

local x = -48
local dx = 32
for i = 0,500 do
	CreateSprite("phys_floor", x + dx*i, 350);
end

CreatePlayer("sohchan", 50, 300);
CreatePlayer("unylchan", 50, 300);

wp = CreateWaypoint( 100, 200, 10, 10 )
wp2 = CreateWaypoint( 200, 200, 10, 10 )
SetNextWaypoint( wp, wp2 )
SetNextWaypoint( wp2, wp )
local platform = CreateEnemy("sprites/phys_floor_moving", 150, 200)
SetEnemyWaypoint( platform, wp )
wp = CreateWaypoint( 250, 100, 10, 10 )
wp2 = CreateWaypoint( 250, 200, 10, 10 )
SetNextWaypoint( wp, wp2 )
SetNextWaypoint( wp2, wp )
platform = CreateEnemy("sprites/phys_floor_moving", 250, 200)
SetEnemyWaypoint( platform, wp )
wp = CreateWaypoint( 300, 200, 10, 10 )
wp2 = CreateWaypoint( 350, 150, 10, 10 )
wp3 = CreateWaypoint( 400, 200, 10, 10 )
SetNextWaypoint( wp, wp2 )
SetNextWaypoint( wp2, wp3 )
SetNextWaypoint( wp2, wp )
platform = CreateEnemy("sprites/phys_floor_moving", 350, 200)
SetEnemyWaypoint( platform, wp )

wp = CreateWaypoint( 700, 200, 10, 10 )
wp2 = CreateWaypoint( 800, 200, 10, 10 )
SetNextWaypoint( wp, wp2 )
SetNextWaypoint( wp2, wp )
platform = CreateEnemy("sprites/phys_floor_moving", 800, 200)
SetEnemyWaypoint( platform, wp )
wp = CreateWaypoint( 800, 100, 10, 10 )
wp2 = CreateWaypoint( 700, 100, 10, 10 )
SetNextWaypoint( wp, wp2 )
SetNextWaypoint( wp2, wp )
platform = CreateEnemy("sprites/phys_floor_moving", 700, 100)
SetEnemyWaypoint( platform, wp )

CreateSprite("release_sign", 500, 275)


local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������



----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------


function LEVEL.MapScript(player)
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

return LEVEL