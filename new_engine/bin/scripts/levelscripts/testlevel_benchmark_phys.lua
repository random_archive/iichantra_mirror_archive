InitNewGame();            -- ������ ����� ����
difficulty = 1


slow_benchmark = CreatePlayer("sohchan", 0, 0)
--CreateEnemy("slowpoke", 0, 0)
--CreateEnemy("slowpoke-spawned", 0, 0)


LEVEL = {}

function TestThread()
    EnablePlayerControl(false)
    for i = 1, 10 do
        SetObjPos(slow_benchmark, 0, 0)
        SetDynObjAcc(slow_benchmark, 3, 0)
        SetDynObjVel(slow_benchmark, 3, 0)
        --Log("slow_benchmark = ", slow_benchmark)
        Wait(1)
        old_y = GetObject( slow_benchmark ).aabb.p.x
        old_time = GetCurTime()
        Wait(5000)
        time = GetCurTime()
        y = GetObject( slow_benchmark ).aabb.p.x
        
        Log( "old time "..old_time..", new time "..time..", dt "..time-old_time..", old pos "..old_y..", new pos "..y..", shift "..y-old_y..", speed "..(y-old_y)/(time-old_time) )
    end
    ExitGame()
end

local fg = true

function LEVEL.MapScript(player)
    --Log("map script")
    if fg then
        fg = false
        Resume( NewThread(TestThread) )
        
    end
end


function LEVEL.SetLoader(l)
    LEVEL.missionStarter = l
    
    --Log("Set loader ", l == nil and "nil" or "l")
end

LEVEL.MapScript()

return LEVEL
