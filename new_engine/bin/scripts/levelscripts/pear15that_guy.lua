local tguy_talk

local LEVEL = {}
InitNewGame()
dofile("levels/pear15that_guy.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

function LEVEL.boss_done()
	local safe_area = { -224, 61, 10, 168 }
	local function players_safe()
		local safe1 = false
		local safe2 = true
		local ud = GetPlayerCharacter( 1 ) 
		local o = ud:aabb()
		if not (o.p.x - o.W > safe_area[3] or o.p.x + o.W < safe_area[1]) 
			and not (o.p.y - o.H > safe_area[4] or o.p.y + o.H < safe_area[2])
			and ud:on_plane() then
			safe1 = true
		end
		ud = GetPlayerCharacter( 2 ) 
		if ud then
			o = ud:aabb()
			if not (o.p.x - o.W > safe_area[3] or o.p.x + o.W < safe_area[1]) 
				and not (o.p.y - o.H > safe_area[4] or o.p.y + o.H < safe_area[2])
				and ud:on_plane() then
				safe2 = true
			else
				safe2 = false
			end
		end
		return safe1, safe2
	end
	GUI:hide()
	Menu:lock()
	Resume( NewMapThread( function()
		local s1, s2 = players_safe()
		local o1, o2 = GetPlayerCharacter(1), GetPlayerCharacter(2)
		local player1ok, player2ok = false, false
		if not o2 then
			player2ok = true
		end
		while not (player1ok and player2ok) do
			if not player1ok then
				player1ok = GetPlayerCharacter(1):health() > 0
			end
			if not player2ok then
				player2ok = GetPlayerCharacter(2):health() > 0
			end
			Wait(1)
		end
		EnablePlayerControl( false )
		local p1, p2 = false, false
		while not (s1 and s2) do
			if not s1 then
				if math.abs( o1:aabb().p.x - (-184) ) > 10 then
					local dir = iff(o1:aabb().p.x > (-184), -1, 1)
					o1:acc{ x = dir * 1.75, y = 0 }
				else
					if not p1 then
						o1:acc{ x = 0, y = 0 }
						o1:vel{ x = 0, y = -12 }
						p1 = true
					end
				end
			end
			if not s2 then
				if math.abs( o2:aabb().p.x - (-27) ) > 10 then
					local dir = iff(o2:aabb().p.x > (-27), -1, 1)
					o2:acc{ x = dir * 1.75, y = 0 }
				else
					if not p2 then
						o2:acc{ x = 0, y = 0 }
						o2:vel{ x = 0, y = -12 }
						p2 = true
					end
				end
			end
			Wait(1)
			s1, s2 = players_safe()
		end
		if o1 then
			o1:mass( -1 )
			o1:sprite_mirrored( false )
		end
		if o2 then
			o2:mass( -1 )
			o2:sprite_mirrored( true )
		end
		Resume( NewMapThread( function()
			while true do
				CreateEnemy( "big_safe_explosion", math.random( -250, 30 ), math.random( 180, 250 ) )
				Wait( math.random( 300, 800 ) )
			end
		end ))
		local copter = GetObjectUserdata( CreateEnemy( "pear15copter", -100, -900 ) )
		copter:sprite_mirrored( false )
		SetObjSpriteColor( copter, { 1, 1, 1, 1 }, 1 )
		local pos = copter:aabb().p
		copter:vel{ x = 0; y = 3 }
		local last_change = Loader.time
		local dt = 0
		while pos.y < 0 do
			Wait(1)
			pos = copter:aabb().p
			--[[
			dt = Loader.time - last_change
			last_change = Loader.time
			pos.y = pos.y + dt / 10
			SetObjPos( copter, pos.x, pos.y )]]
		end
		copter:vel{ x = 0; y = 0 }
		Wait(500)
		SetObjAnim( mapvar.tmp.pear15copter_door, "open", true )
		Wait(1500)
		local done1, done2 = false, false
		Game.MoveWithAccUntilX( o1, { x = 1.75, y = 0 }, -72, function()
			SetObjInvisible( o1, true )
			o1:drops_shadow(false)
			done1 = true
		end )
		if o2 then
			Game.MoveWithAccUntilX( o2, { x = 1.75, y = 0 }, -72, function()
				SetObjInvisible( o2, true )
				o2:drops_shadow(false)
				done2 = true
			end )
		else
			done2 = true
		end
		while not (done1 and done2) do
			Wait(1)
		end
		copter:acc{ x = 0, y = -2 }
		Wait(3000)
		Menu:unlock()
		local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
		ObjectPushInt( obj1, 28 )
		SetObjAnim( obj1, "script", false )
	end ) )
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	EnablePlayerControl( false )
	Menu:lock()
	Game.info.can_join = false
	mapvar.tmp.cutscene = true
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	--	{constants.ObjEnemy, "pear15tguy", 273, 155, -0.100000, solid_to=255, mirrored = true},
	local tguy = GetObjectUserdata( CreateEnemy( "pear15tguy", 273, 155 ) )
	SetObjPos( tguy, 273, 155 )
	tguy:sprite_mirrored( true )
	tguy_talk = function() SetObjAnim( tguy, "talk", false ) end
	Resume( NewMapThread( function()
		local sx, sy = GetCaptionSize( "default", "������� 8" )
		local text1 = CreateWidget( constants.wt_Label, "TEXT", mapvar.tmp.fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text1"] = text1
		WidgetSetCaptionColor( text1, {1,1,1,1}, false )
		WidgetSetCaption( text1, "������� 8" )
		WidgetSetZ( text1, 1.05 )
		sx, sy = GetCaptionSize( "default", "����� ����������" )
		local text2 = CreateWidget( constants.wt_Label, "TEXT", mapvar.tmp.fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text2"] = text2
		WidgetSetCaptionColor( text2, {1,1,1,1}, false )
		WidgetSetCaption( text2, "����� ����������" )
		WidgetSetZ( text2, 1.05 )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		Game.AttachCamToPlayers( {
				{ {-99999, -99999, 99999, 99999}, {-590, -135, 397, 374 } },
		} )
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		LEVEL.bossobjects =
		{
			top =
			{
				CreateSpriteCenter( "red_flare", -359, -83 ),
				CreateSpriteCenter( "red_flare", 162, -85 ),
			},
			middle = 
			{
				CreateSpriteCenter( "red_flare", -251, 111 ),
				CreateSpriteCenter( "red_flare", -217, 112 ),
				CreateSpriteCenter( "red_flare", -111, 113 ),
				CreateSpriteCenter( "red_flare", 0, 113 ),
				CreateSpriteCenter( "red_flare", 36, 112 )
			},
			bottom =
			{
				CreateSpriteCenter( "red_flare", -381, 312 ),
				CreateSpriteCenter( "red_flare", -251, 315 ),
				CreateSpriteCenter( "red_flare", -123, 313 ),
				CreateSpriteCenter( "red_flare", -91, 314 ),
				CreateSpriteCenter( "red_flare", 35, 314 ),
				CreateSpriteCenter( "red_flare", 164, 314 ),
			},
			left_screen = CreateSpriteCenter( "screen", -288, 8 ),
			right_screen = CreateSpriteCenter( "screen", 65, 9 ),
		}
		mapvar.tmp.fade_complete = false
		local char1, char2, char1_talk, char2_talk = Game.GetCharacters()
		Wait(1)
		SetObjPos( char1, -515, 148 )
		if char2 then
			SetObjPos( char2, -435, 148 )
		end
		Wait( 1000 )
		Game.fade_in()
		while not mapvar.tmp.fade_complete do
			Wait(1)
		end
		if char2 then
			char2:acc{ x = 0.75, y = 0 }
		end
		local done = false
		Game.MoveWithAccUntilX( char1, { x = 0.75, y = 0 }, -100, function()
			SetObjAnim( char1, "stop", false )
			done = true
			if char2 then
				char2:acc{ x = 0, y = 0 }
				SetObjAnim( char2, "stop", false )
			end
		end )
		while not done do
			Wait(1)
		end
		Wait( 500 )
		local conversation_end = function()
			mapvar.tmp.cutscene = false
			EnablePlayerControl( true )
			Game.info.can_join = true
			mapvar.tmp.boss_active = true
			PlayBackMusic( "music/iie_boss.it" )
			GUI:show()
			Menu:unlock()
		end
		Conversation.create{
			function( var )
				if char2 then
					return "BOTH-PLAYERS"
				elseif mapvar.actors[1].info.character == "pear15unyl" then
					return "UNYL-SINGLE"
				end
			end,
			tguy_talk,
			{ sprite = "portrait-tguy" },
			"��, ��������, �� ����������? �� ����������� �� ��� � ����.",
			char1_talk,
			{ sprite = "portrait-tguy" },
			{ sprite = "portrait-soh" },
			"���. ������ � ���� ������, ��� �� ������ � ����� �������������� ����������� ����������� ���������� ������ ���������� ����.",
			conversation_end,
			nil,
			{ type = "LABEL", "UNYL-SINGLE" },
			tguy_talk,
			{ sprite = "portrait-tguy" },
			"������ �� ����������� ��������� ����. ��� ���� ���� � ���� � �����.",
			char1_talk,
			{ sprite = "portrait-unyl" },
			"� ���� ���� �����. ������.",
			conversation_end,
			nil,
			{ type = "LABEL", "BOTH-PLAYERS" },
			tguy_talk,
			{ sprite = "portrait-tguy" },
			"� ��� ��� ��������� ����.",
			char1_talk,
			{ sprite = "portrait-soh" },
			"�������, �� ��������� ����� ����, ��� ���������� �������� ����� �����������?",
			{ sprite = "portrait-tguy" },
			tguy_talk,
			"������, ���. �� ����������� ������.",
			char2_talk,
			{ sprite = "portrait-soh" },
			"��� ���� ���� ������� ����. ������ �����, �� ����� ��� �����.",
			{ sprite = "portrait-tguy" },
			"� ���-�� ��� ���� ����! �� ������ ����������.",
			conversation_end,
			nil
		}:start()
	end ))
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	mapvar.actors[1].info.ending = 2
	Loader.ChangeLevel( "pear15ending" )
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{-327,240},}
LEVEL.bosspoints = { 
					left = -485, 
					right = 257, 
					top = 94, 
					bottom = 309, 
					left_jump_start = -418, 
					left_jump_end = -206, 
					right_jump_start = 207, 
					right_jump_end = -7,
					right_jumpup_start = 36,
					left_jumpup_start = -254,
					center = {-113, 98},
					screen_right = {63, 8},
					screen_left = {-291, 8},
				}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'That Guy'

LEVEL.tips =
{
	"��������� �������� �� �������� �����!",
	"������ ����� �������� ������.",
	"����� ������ ������ ����� ������� �������."
}

LEVEL.solution = '������� ������ � ���� ������ ���������� ������./n/n�� ������ ���� ������ ��������� � ������ ������� � ��������� �������� �� ������� �����. ��� ������ ��� �������� ������, ��� ��������, ��� �� ������ ������� �� ���� ������ (������� ��� ������). ������� � ��./n/n����� ����, ��� � ���� ��������� ������ �������� ��������, �� ������ ����� ��������� �������� �������� �� ���, ������� ������� � ������ ����. �� ��������� ������������ �������, ������ �� ���� ����� ���������� ������./n/n�������, ����� ���� ��� ������������ � ����� �������, ����� ���������� ����� - �� ��������� ����� � ���. �������� ������� � ���������.'

return LEVEL
