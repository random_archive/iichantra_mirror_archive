c =
{
	notes = {"A","B","C","D","E"},
	note_types = { A=1, B=2, C=3, D=4, E=5 },
	note_pos = { A=32, B=64, C=96, D=128, E=160  },
	active_zone = { from=460, to=480 },
	test_song =
	{
		{ time = 1000, nt = "A", dur = 1000 },
		{ time = 1500, nt = "B", dur = 0 },
		{ time = 2000, nt = "C", dur = 0 },
		{ time = 2500, nt = "D", dur = 0 },
		{ time = 3000, nt = "E", dur = 0 },
		{ time = 4000, nt = "A", dur = 1000 },
		{ time = 4000, nt = "C", dur = 1000 },
		{ time = 5000, nt = "B", dur = 2000 },
		{ time = 5000, nt = "D", dur = 1000 },
		{ time = 6000, nt = "E", dur = 1000 }
	},
	notekeys =
	{
		"f1",
		"f2",
		"f3",
		"f4",
		"f5"
	},
	keynotes =
	{
		f1=1,
		f2=2,
		f3=3,
		f4=4,
		f5=5
	}
}

Note = {}
Note.__index = Note
Game = {}
Game.__index = Game
Game.keys = {}

function Note.Create( note_type, note_duration, game )
	if not c.note_types[note_type] or not note_duration or not game then return end
	local note = {}
	setmetatable(note, Note)
	note.ntype = c.note_types[note_type]
	note.ns = note_type
	note.duration = note_duration
	note.sprite = CreateSprite( game.note_sprite, c.note_pos[note_type], -16, note_type )
	--from self to self+8-duration*speed
	if ( note_duration > 0 ) then
		note.tail = {}
		for i=-8-note_duration*game.note_speed,-15,16 do
			table.insert(note.tail, CreateSprite( game.note_tail_sprite, c.note_pos[note_type], i, note_type ))
			--Log("creating")
		end
	end
	note.game = game
	note.x = c.note_pos[note_type]
	note.y = -16
	return note
end

function Note:Update( time )
	if not self.sprite then return end
	if self.played and not self.done then
		self.done = true
		SetObjAnim(self.sprite, self.ns.."-complete", true)
	end
	self.y = self.y + self.game.note_speed * time
	SetObjPos( self.sprite, self.x, self.y )
	if ( self.duration > 0 ) then
		local k = 1
		for i=self.y-16,self.y-self.duration*self.game.note_speed,-16 do
			SetObjPos( self.tail[k], self.x, i )
			k = k + 1
		end
	end
	if self.y-self.duration-32 > 480 then
		return true
	end
	return false
end

function Note:Destroy()
	if self.sprite then
		SetObjDead( self.sprite )
		self.y = 900000
		for key,value in pairs(self.tail or {}) do
			SetObjDead( value )
		end
		self.sprite = nil
	end
end

function Note:isActive()
	if not self.sprite then return false end
	if self.y > c.active_zone.to then self.past = true end
	return self.y+self.game.timing > c.active_zone.from and self.y-self.duration-self.game.timing < c.active_zone.to
end

function Note:score(game)
	if self.played then
		game.combo = game.combo + 1
		game.score = game.score + 1 * game.combo
	else
		game.combo = 0
	end
end

function Game.Create()
	local game = {}
	setmetatable(game, Game)
	game.song_container = dofile("scripts/songs/"..unyl_song..".lua")
	game.note_speed = game.song_container.speed
	game.note_sprite = "note"
	game.note_tail_sprite = "note-tail"
	game.notes = {}
	game.score = 0
	game.combo = 0
	game.timing = 32
	game.active_notes = {}
	game.song = game.song_container.notes
	game.time = GetCurTime()
	game.start = game.time
	game.oldenter = false
	return game
end

function Game:UpdateGUI()
	WidgetSetCaption(self.widgets.score, "score: "..math.floor(self.score))
	WidgetSetCaption(self.widgets.combo, "combo: "..self.combo)
	for key, value in pairs(c.notekeys) do
		if ( Game.keys[ keys[value] ] ) then
			WidgetSetAnim(self.widgets[value], value.."-active")
		else
			WidgetSetAnim(self.widgets[value], value)
		end
	end
end

function Game:Init()
	InitNewGame()
	Game.loopback = self
	difficulty = 1
	LoadPrototype("sohchan.lua")
	LoadTexture("soh-chan1024.png")
	LoadPrototype("phys_floor.lua")
	LoadTexture("unylhero.png")
	LoadTexture("unylhero-player.png")
	LoadPrototype("unylhero.lua")
	LoadPrototype("note.lua")
	LoadPrototype("note-tail.lua")
	LoadTexture("unylhero-room.png")
	LoadPrototype("unylhero-room.lua")
	CreatePlayer("sohchan", 920, 240)
	CamMoveToPos(320, 240)
	self.time = GetCurTime()
	self.realtime = self.time
	Game.start = self.start
	self.time = 0
	Log(self.start)
	self.widgets = {}
	self.widgets.songname = CreateWidget(constants.wt_Label, "score", nil, 320, 80, 70, 10)
	WidgetSetCaptionFont(self.widgets.songname, "default")
	WidgetSetCaption(self.widgets.songname, self.song_container.name)
	WidgetSetCaptionColor(self.widgets.songname, {.8, .8, 1.0, 1.0}, false)
	WidgetSetCaptionColor(self.widgets.songname, {1.0, 0.2, 0.2, 1.0}, true)
	self.widgets.score = CreateWidget(constants.wt_Label, "score", nil, 320, 100, 70, 10)
	WidgetSetCaptionFont(self.widgets.score, "default")
	WidgetSetCaption(self.widgets.score, "score: 0")
	WidgetSetCaptionColor(self.widgets.score, {1.0, 1.0, 1.0, 1.0}, false)
	WidgetSetCaptionColor(self.widgets.score, {1.0, 0.2, 0.2, 1.0}, true)
	self.widgets.combo = CreateWidget(constants.wt_Label, "score", nil, 320, 115, 70, 10)
	WidgetSetCaptionFont(self.widgets.combo, "default")
	WidgetSetCaption(self.widgets.combo, "combo: 0")
	WidgetSetCaptionColor(self.widgets.combo, {1.0, 1.0, 1.0, 1.0}, false)
	WidgetSetCaptionColor(self.widgets.combo, {1.0, 0.2, 0.2, 1.0}, true)
	for i=1,5 do
		local key = c.notekeys[i]
		local key_correct = key.."-correct"
		self.widgets[key] = CreateWidget(constants.wt_Picture, "key", nil, c.note_pos[c.notes[i]]-8, 430, 70, 10)
		WidgetSetSprite(self.widgets[key], "note")
		WidgetSetAnim(self.widgets[key], c.notekeys[i])
		WidgetSetVisible(self.widgets[key], true)
		self.widgets[key_correct] = CreateWidget(constants.wt_Picture, "keycorrect", nil, c.note_pos[c.notes[i]]-13, 425, 70, 10)
		WidgetSetSprite(self.widgets[key_correct], "note")
		WidgetSetAnim(self.widgets[key_correct], "invisible")
		WidgetSetVisible(self.widgets[key_correct], true)
	end
	self.unyl = CreateSprite("unylhero", 340, 315, "A")
	CreateSprite( "unylhero-room", 190, 185 )
	GlobalSetKeyDownProc(onpress)
	GlobalSetKeyReleaseProc(onrelease)
	for _,value in pairs(self.song) do
		value.time = value.time + self.time
	end
	LoadSound(self.song_container.file)
end

mus = false

function Game:Update()
	self.active_notes = {}
	local delete_list = {}
	local tiem = GetCurTime()
	local dt = tiem - self.realtime
	self.realtime = tiem
	self.time = self.time + dt
	Game.time = self.time
	--Log(self.time)
	if self.time >= self.song_container.delay and not mus then
		PlayBackMusic(self.song_container.song)
		mus = true
	end
	--moving old notes
	for key,value in pairs(self.notes) do
		--if value:Update(dt) then
		--Log(self.time)
		if value:Update(dt) then
			if not value.played and value.sprite then value:score(self) end
			value:Destroy()
			table.insert( delete_list, key )
		end
		if value:isActive() then
			--Log("" .. value.ntype .. " is active!")
			self.active_notes[value.ntype] = value
		end
	end

	--deleting old notes
	for i=1,#delete_list do
		table.remove( self.notes, delete_list[ #delete_list-i+1 ] )
	end

	--creating new notes
	for _,value in pairs(self.song) do
		if not value.done and self.time >= value.time then
			value.done = true
			--Log( string.format("Creating note: %s, %f", value.nt, value.dur) )
			if value.nt == "MUSIC_STOP" then
				StopBackMusic()
			else
				table.insert( self.notes, Note.Create( value.nt, value.dur, self ) )
			end
		end
	end

	--input
	if Game.keys[ keys.enter ] and not self.oldenter then
		local correct = true
		local note_anim = nil
		for key,value in pairs(c.notekeys) do
			if Game.keys[keys[value]] then
				note_anim = c.notes[c.keynotes[value]]
			end
		end
		if note_anim then
			SetObjAnim(self.unyl, note_anim, true)
		end
		--Checking if correct key is not pressed
		for key, value in pairs(self.active_notes) do
			if value and not Game.keys[ keys[ c.notekeys[ key ]] ] and not value.played and not value.past then
				correct = false
				break
			end
		end
		--Checking if there are excess keys pressed
		if correct then
			for key,value in pairs(c.notekeys) do
				if Game.keys[ keys[value] ] and (not self.active_notes[ c.keynotes[value] ] or self.active_notes[ c.keynotes[value] ].played) then
					correct = false
				end
			end
		end
		if correct  then
			for key,value in pairs(c.notekeys) do
				if Game.keys[ keys[value] ] then
					WidgetSetAnim( self.widgets[value.."-correct"], "note-correct" )
				end
			end
			for key, value in pairs(self.active_notes) do
				value.played = true
				value:score(self)
			end
		else
			self.combo = 0
		end
	end
	if Game.keys[ keys.enter ] and self.oldenter then
		for key, value in pairs(self.active_notes) do
			if value.played and value.duration>0 and Game.keys[ keys[ c.notekeys[ key ]] ] and value.y < 900 then
				self.score = self.score + 0.1
				WidgetSetAnim( self.widgets[c.notekeys[ value.ntype ].."-correct"], "note-correct", true )
			end
		end
	end
	self.oldenter = Game.keys[ keys.enter ]
	self:UpdateGUI()
end

function GameThread()
	local gaem = Game.Create()
	gaem:Init()
	while true do
		gaem:Update()
		Wait(1)
	end
end

--[[
GlobalSetKeyDownProc(onkeydown)
GlobalSetKeyReleaseProc(onkeyup)
--]]
songu = {}
songu.due = {}
songu.song = {}

function onpress(key)
	Game.keys[key] = true

	--EDIT MODE
	--[[
		if key == keys.f1 then
			songu.due[1]=GetCurTime() - Game.start
		elseif key == keys.f2 then
			songu.due[2]=GetCurTime() - Game.start
		elseif key == keys.f3 then
			songu.due[3]=GetCurTime() - Game.start
		elseif key == keys.f4 then
			songu.due[4]=GetCurTime() - Game.start
		elseif key == keys.f5 then
			songu.due[5]=GetCurTime() - Game.start
		end
	--]]
	--/EDIT
end

function onrelease(key)
	Game.keys[key] = false

	--EDIT MODE
	--[[
		if key == keys.f1 then
			table.insert(songu.song, string.format("{time = %i, dur = %i, nt=\"A\" },\n", songu.due[1], GetCurTime() - Game.start - songu.due[1]))
		elseif key == keys.f2 then
			table.insert(songu.song, string.format("{time = %i, dur = %i, nt=\"B\" },\n", songu.due[2], GetCurTime() - Game.start - songu.due[2]))
		elseif key == keys.f3 then
			table.insert(songu.song, string.format("{time = %i, dur = %i, nt=\"C\" },\n", songu.due[3], GetCurTime() - Game.start - songu.due[3]))
		elseif key == keys.f4 then
			table.insert(songu.song, string.format("{time = %i, dur = %i, nt=\"D\" },\n", songu.due[4], GetCurTime() - Game.start - songu.due[4]))
		elseif key == keys.f5 then
			table.insert(songu.song, string.format("{time = %i, dur = %i, nt=\"E\" },\n", songu.due[5], GetCurTime() - Game.start - songu.due[5]))
		elseif key == keys.w then
			songfile = io.open("lolfile.lua", "w")
			for i=1,#songu.song do
				songfile:write(songu.song[i])
			end
			songfile:close()
			ExitGame()
		elseif key == keys.t then
			Log(Game.time)
		end
	--]]
	--/EDIT
end

local gt = NewThread( GameThread )
Resume( gt )
