local LEVEL = {}
InitNewGame()
dofile("levels/pear15house_opening.lua")

CONFIG.backcolor_r = 0
CONFIG.backcolor_g = 0
CONFIG.backcolor_b = 0
LoadConfig()

StopBackMusic()

local char1
local char2

local function teh_end()
	if current_thread then
		StopThread( current_thread )
	end
	RemoveSkipKey()
	Game.reset_fade()
	local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
	ObjectPushInt( obj1, 0 )
	SetObjAnim( obj1, "script", false )
end

local current_thread

local function start( thread )
	current_thread = thread
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	mapvar.tmp.cutscene = true
	SwitchLighting(true)
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	CamMoveToPos( 1116, 101 )
	Menu:lock()
	EnablePlayerControl( false )
	Game.info.can_join = false
	current_thread = 
	Resume( NewMapThread( function()
		Wait(1)
		char1, char2, char1_talk, char2_talk = Game.GetCharacters()
		SetSkipKey( teh_end, 300, { CONFIG.key_conf[1].gui_nav_decline, CONFIG.key_conf[1].gui_nav_menu, CONFIG.key_conf[2].gui_nav_decline, CONFIG.key_conf[2].gui_nav_menu } )
		Wait( 1000 )
		SetObjPos( char1, 1467, 19 )
		if char2 then
			SetObjPos( char2, 1467, 19 )
		end
		local function conversation_end()
			Resume( NewMapThread( function()
				local sci = GetObjectUserdata( CreateEnemy( "special_btard", 237, 149 ) )
				SetObjAnim( sci, "think", false )
				sci:sprite_mirrored( true )
				local tg = GetObjectUserdata( CreateEnemy( "pear15tguy", 200, 149 ) )
				SetObjAnim( tg, "idle2", false )
				local x = 1116
				while math.abs( 467 - x ) > 1 do
					x = x + ( 467 - x ) * 0.1
					CamMoveToPos( x, 101 )
					Wait( 1 )
				end
				CamMoveToPos( 467, 101 )
				Wait( 500 )
				SetObjAnim( tg, "move", false )
				tg:sprite_mirrored( true )
				tg:acc{ x = -1.75, y = 0 }
				SetObjAnim( sci, "walk_with_bottle", false )
				sci:acc{ x = 1.5, y = 0 }
				sci:sprite_mirrored( false )
				local sci_phase = 0
				local time
				SetObjProcessor( sci, function( this )
					local pos = this:aabb().p
					if sci_phase == 0 then
						if pos.x >= 560 then
							mapvar.tmp.bottle = CreateSprite( "bottle", 560, 111 )
							SetObjAnim( this, "walk", false )
						end
						if pos.x >= 621 then
							SetObjAnim( this, "think", false )
							this:acc( { x = 0, y = 0 } )
							time = Loader.time
							sci_phase = 1
						end
					elseif sci_phase == 1 then
						if Loader.time - time >= 1000 then
							SetObjAnim( this, "walk", false )
							sci:sprite_mirrored( true )
							sci:acc{ x = -1.5, y = 0 }
							sci_phase = 2
						end
					elseif sci_phase == 2 then
						if pos.x <= 481 then
							sci_phase = 3
							time = Loader.time
							this:acc({x=0, y=0})
							CreateSprite( "house-door", 434, 13 )
							SetObjAnim( this, "think", false )
						end
					elseif sci_phase == 3 then
						if Loader.time - time >= 500 then
							SetObjDead( this )
							sci_phase = 4
						end
					end
				end )
				while sci_phase < 4 do
					Wait(1)
				end
				Wait( 1000 )
				local x = 467
				while math.abs( 1137 - x ) > 1 do
					x = x + ( 1137 - x ) * 0.1
					CamMoveToPos( x, 101 )
					Wait( 1 )
				end
				CamMoveToPos( 1137, 101 )
				Wait( 500 )
				local done = false
				local conversation_end = function()
					Resume( NewMapThread( function()
						local cx, cy = GetCamPos()
						cx, cy = cx - char1:aabb().p.x, cy - char1:aabb().p.y
						char1:acc( {x=-1.75, y = 0} )
						if char2 then
							char2:acc( {x=-1.60, y = 0} )
						end
						SetObjProcessor( char1, function( this )
							local pos = this:aabb().p
							CamMoveToPos( pos.x + cx, pos.y + cy )
							if pos.x <= 580 then
								char1:acc( { x = 0, y = 0 } )
								SetObjAnim( char1, "stop", false )
								if char2 then
									char2:acc( { x = 0, y = 0 } )
									SetObjAnim( char2, "stop", false )
								end
								done = true
								SetObjProcessor( char1, function() end )
							end
						end )
					end ))
				end
				Conversation.create( {
					function( var ) 
						if mapvar.actors[2] then
							return "BOTH-PLAYERS"
						elseif mapvar.actors[1].info.character == "pear15unyl" then
							return "UNYL-SINGLE"
						end
					end,
					char1_talk,
					{ sprite = "portrait-soh" },
					"����� ����! � �-�� ������, ��� �� ����. ���������, ��� � ���� �������?",
					conversation_end,
					nil,
					{ type = "LABEL", "UNYL-SINGLE" },
					char1_talk,
					{ sprite = "portrait-unyl" },
					"�����? Ÿ ���������� ����� �������.",
					conversation_end,
					nil,
					{ type = "LABEL", "BOTH-PLAYERS" },
					char2_talk,
					{ sprite = "portrait-unyl" },
					"�� ������ ������� ���������� ���� �����.",
					{ sprite = "portrait-soh" },
					"��� ��� ����, �����, ���� �� �����!",
					conversation_end,
					nil
				} ):start()
				while not done do
					Wait(1)
				end
				SetObjDead( mapvar.tmp.bottle )
				Wait( 500 )
				SetObjAnim( mapvar.tmp.candle, "fall", false )
				while not mapvar.tmp.fire do
					Wait(1)
				end
				local x = 522 --133
				while x > 278 do
					CreateSprite( "flames_small", x, 133 + math.random( -5, 5 ) )
					x = x + math.random( -25, -15 )
					Wait(1)
				end
				CreateEnemy( "big_explosion", 292, 131 )
				Wait( 500 )
				local x = -116
				while x < 348 do
					x = x + math.random( 16, 24 )
					SetObjPos( CreateSprite( "flames", x, 122 ), x, 122 + math.random( -5, 10 ) )
				end
				local conversation_end = function()
					Menu:unlock()
					EnablePlayerControl( true )
					current_thread = nil
					teh_end()
				end
				Conversation.create( {
					function( var ) 
						if mapvar.actors[1].info.character == "pear15unyl" then
							return "UNYL-SINGLE"
						end
					end,
					char1_talk,
					{ sprite = "portrait-soh" },
					"���.",
					conversation_end,
					nil,
					{ type = "LABEL", "UNYL-SINGLE" },
					char1_talk,
					{ sprite = "portrait-unyl" },
					"���.",
					conversation_end,
					nil
				} ):start()
			end ))
		end
		char1:acc( {x=-1.75, y = 0} )
		if char2 then
			SetObjPos( char2, char1:aabb().p.x + 80, char1:aabb().p.y )
			char2:acc( {x=-1.60, y = 0} )
		end
		local ready = false
		SetObjProcessor( char1, function( this )
			local pos = this:aabb().p
			if pos.x <= 1327 then
				char1:acc( {x = 0, y = 0} )
				SetObjAnim( char1, "stop", false )
				if char2 then
					char2:acc( {x = 0, y = 0} )
					SetObjAnim( char2, "stop", false )
				end
				ready = true
				SetObjProcessor( char1, function() end )
			end
		end )
		while not ready do
			Wait(1)
		end
		Wait( 500 )
		Conversation.create( {
			function( var ) 
				if mapvar.actors[2] then
					return "BOTH-PLAYERS"
				elseif mapvar.actors[1].info.character == "pear15unyl" then
					return "UNYL-SINGLE"
				end
			end,
			char1_talk,
			{ sprite = "portrait-soh" },
			"��� ����� ������� ������ ��������. �������, � ���� ����� ������?",
			char1_talk,
			"���, � ��� ��� � ��� �����?",
			conversation_end,
			nil,
			{ type = "LABEL", "UNYL-SINGLE" },
			char1_talk,
			{ sprite = "portrait-unyl" },
			"�������� �����. �� ��� �� ��� ����� ���, ��� ��������� ������� ������� ���� �� ������?",
			char1_talk,
			"��, ��� ��� ���?",
			conversation_end,
			nil,
			{ type = "LABEL", "BOTH-PLAYERS" },
			char2_talk,
			{ sprite = "portrait-unyl" },
			"�������� �����. �� ��� �� ��� ����� ���, ��� ��������� ������� ������� ���� �� ������?",
			{ sprite = "portrait-soh" },
			"��-�-�! ������, ������ �� ��������.",
			conversation_end,
			nil
		} ):start()
	end ) )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	Loader.ChangeLevel( "pear15house_sphere" )
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{1467,19},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Cutscene'

return LEVEL
