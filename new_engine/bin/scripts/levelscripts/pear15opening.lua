local LEVEL = {}
InitNewGame()
dofile("levels/pear15opening.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

local char1
local char2

local function skip_all_key(key)
	return (key == CONFIG.key_conf[1].gui_nav_decline or key == CONFIG.key_conf[1].gui_nav_menu or 
	        key == CONFIG.key_conf[2].gui_nav_decline or key == CONFIG.key_conf[2].gui_nav_menu )
end

local function end_this()
	local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
	ObjectPushInt( obj1, 0 )
	SetObjAnim( obj1, "script", false )
end

local widget
local current_phase

local function start( func )
	current_phase = NewThread( func )
	Resume( current_phase )
end

local prof

local function phase7()
	Wait( 500 )
	SetCamBounds( 1918, 1918 + 640, -128, -128 + 480 )
	CamMoveToPos( 1918 + 320, -128 + 240 )
	if char1 then
		SetObjPos( char1, 1874, 283 )
		char1:sprite_mirrored( false )
		char1:acc( { x = 0, y = 0 } )
		SetObjProcessor( char1, function( obj )
			if obj:aabb().p.x >= 2012 then
				SetObjAnim( obj, "stop", false )
				obj:acc( { x = 0, y = 0 } )
				pl1_done = true
				SetObjProcessor( char1, function() end )
			end
		end )
	end
	if char2 then
		SetObjPos( char2, 1727, 245 )
		char2:acc( { x = 0, y = 0 } )
		char2:sprite_mirrored( false )
		SetObjProcessor( char2, function( obj )
			if obj:aabb().p.x >= 2118 then
				SetObjAnim( obj, "stop", false )
				obj:acc( { x = 0, y = 0 } )
				pl1_done = true
				SetObjProcessor( char2, function() end )
			end
		end )
	end
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	if char1 then
		char1:acc( { x = 1.75, y = 0 } )
	end
	if char2 then
		char2:acc( { x = 1.75, y = 0 } )
	end
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	local soh_talk = function()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char1] = true
	end
	local unyl_talk = function()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char2] = true
	end
	local conversation_end = function()
		Resume( NewMapThread( function()
			mapvar.tmp.fade_complete = false
			Game.fade_out()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end
			SetCamBounds( -9000, -9000 + 640, -9000, -9000 + 480 )
			CamMoveToPos( -9000 + 320, -9000 + 240 )
			DestroyWidget( mapvar.tmp.fade_widget )
			end_this()
		end ))
	end
	Conversation.create( {
		function( var ) 
			if mapvar.actors[2] then
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		soh_talk,
		{ sprite = "portrait-soh" },
		"������! �, ����. ��������������� ���. �������.",
		conversation_end,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		soh_talk,
		{ sprite = "portrait-unyl" },
		"������? ��� ���� ��� ��������� ��������.",
		soh_talk,
		"��� �� ��������� � �����������, ��� �������. �������, ����� � �����������.",
		conversation_end,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		unyl_talk,
		{ sprite = "portrait-unyl" },
		"�����!",
		soh_talk,
		{ sprite = "portrait-soh" },
		"��������, �����, �� ��������� � �����������.",
		unyl_talk,
		{ sprite = "portrait-unyl" },
		"����� �� ���, ���������� ���� �� ����������� �� �������! �������� � �����!",
		conversation_end,
		nil,
	} ):start()
end

local function phase6()
	Wait(1)
	--SetSkipKey( phase6_skip, 500 )
	RemoveSkipKey()
	CreateSprite( "artard", 1516, 226 )
	if char1 then
		SetObjPos( char1, 1345, 245 )
		char1:sprite_mirrored( false )
	end
	if char2 then
		SetObjPos( char2, 1484, 245 )
		char2:sprite_mirrored( false )
	end
	SetCamBounds( 1163, 1163 + 640, -128, -128 + 480 )
	CamMoveToPos( 1163 + 320, -128 + 240 )
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	Wait( 500 )
	local bard
	local conversation_end = function()
		if char1 then
			char1:acc( { x = 1.75, y = 0 } )
		end
		if char2 then
			char2:acc( { x = 1.75, y = 0 } )
		end
		if bard and bard:object_present() then
			SetObjDead( bard )
		end
		SetObjAnim( mapvar.tmp.easel, "fall", false )
		Resume( NewMapThread( function()
			Wait( 1000 )
			mapvar.tmp.fade_complete = false
			Game.fade_out()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end		
			RemoveSkipKey()
			start( phase7 )
		end ))
	end
	local soh_talk = function()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char1] = true
	end
	local unyl_talk = function()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char2] = true
	end
	local btard_run = function()
		bard = GetObjectUserdata( CreateEnemy( "special_btard", 1150, 254 ) )
		SetObjAnim( bard, "move_right", false )
		SetObjProcessor( bard, function( this )
			local pos = this:aabb().p
			if pos.x >= 1516 then
				SetObjAnim( mapvar.tmp.easel, "fall", false )
				SetObjProcessor( bard, function() end )
			end
		end )
	end
	Conversation.create( {
		"�����! ��������� �����! �����! �����! �����! �����!",
		function( var ) 
			if mapvar.actors[2] then
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		soh_talk,
		{ sprite = "portrait-soh" },
		"��, � ���� �� ���� ������ ������ ��� �������������. �� ��� � ���� ������ ����� - ��� ��� �����.",
		soh_talk,
		"������, ������ ���������������. ������� ��� � ������� ������� �� ��? ��� �� ������.",
		btard_run,
		soh_talk,
		"���, � ��� ��� ��� �� ��������� � ����� ������? � ���� ��� ��, ���������, ��� ������?",
		conversation_end,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		{ sprite = "portrait-unyl" },
		soh_talk,
		"�� ��� ������� ��� �����. � ������, �������� �������. �� �����. ������ �� ����� ����.",
		soh_talk,
		"������, ��������� ��� ����.",
		btard_run,
		soh_talk,
		"��, ����������! ���� � ��� �� ����.",
		soh_talk,
		"� � ���� ��� ����� ������?",
		conversation_end,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		soh_talk,
		{ sprite = "portrait-soh" },
		"��, � ���� �� ���� ������ ������ ��� �������������. �� ��� � ���� ������ ����� - ��� ��� �����.",
		unyl_talk,
		{ sprite = "portrait-unyl" },
		"�������� �������, ��������.",
		soh_talk,
		{ sprite = "portrait-soh" },
		"��������, ���������. ������. ��� ������ ������ �� ����������. ������ ��� �� �� �������, ��� ��?",
		unyl_talk,
		{ sprite = "portrait-unyl" },
		"���, �����, ���-�� ��� �������.",
		btard_run,
		soh_talk,
		{ sprite = "portrait-soh" },
		"� ��� ��� ��� �� ���� � ������?",
		unyl_talk,
		{ sprite = "portrait-unyl" },
		"������ ����-��. ����� ���������?",
		conversation_end,
		nil
	} ):start()
end

local function phase5_skip(key)
	RemoveSkipKey()
	StopThread( current_phase )
	if widget then
		DestroyWidget( widget )	
		widget = nil
	end
	if skip_all_key(key) then
		return end_this()
	end
	start( phase6 )
end

local function phase5()
	Wait(1)
	SetSkipKey( phase5_skip, 500 )
	local sx, sy = GetCaptionSize( "default", "������, 16:41 �������� �������" )
	local place = CreateWidget( constants.wt_Label, "", nil, CONFIG.scr_width - sx - 10, CONFIG.scr_height - sy - 10, sx, sy )
	WidgetSetCaptionColor( place, { .8, 1, .8, 1 }, false, {0, 0, 0, 1} )
	WidgetSetCaption( place, "������, 16:41 �������� �������" )
	WidgetUseTyper( place, true, 5, false )
	WidgetStartTyper(place)
	widget = place
	local y = -233 + 240
	local time = Loader.time
	local dt
	while y < -233 + 600 - 240 do
		dt = Loader.time - time
		time = Loader.time
		y = y + 1 * dt / 10
		Wait( 1 )
		CamMoveToPos( 471 + 320, y )
	end
	Wait( 1000 )
	DestroyWidget( widget )
	widget = nil
	mapvar.tmp.fade_complete = false
	Game.fade_out()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	RemoveSkipKey()
	start( phase6 )
end

local function phase4_skip(key)
	RemoveSkipKey()
	StopThread( current_phase )
	mapvar.tmp.fade_complete = false
	Game.fade_in( true )
	if skip_all_key(key) then
		return end_this()
	end
	SetCamBounds( 471, 471 + 640, -233, -233 + 600 )
	CamMoveToPos( 471 + 320, -233 + 240 )
	Resume( NewMapThread( function()
		while not mapvar.tmp.fade_complete do
			Wait(1)
		end
		start( phase5 )
	end ))
end

local function phase4()
	Wait( 1 )
	SetSkipKey( phase4_skip, 500 )
	mapvar.tmp.fade_complete = false
	Game.fade_out()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	SetCamBounds( 471, 471 + 640, -233, -233 + 600 )
	CamMoveToPos( 471 + 320, -233 + 240 )
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	RemoveSkipKey()
	start( phase5 )
end

local function phase3()
	Wait(1000)
	local function conversation_end()
		if char2 then
			char2:acc( {x = 0, y = 0 } )
			SetObjProcessor( char2, function() end )
		end
		start( phase4 )
	end
	local function prof_talk1()
		SetObjAnim( prof, "talk", false )
	end
	local function prof_talk2()
		SetObjAnim( prof, "talk_fgsfds", false )
	end
	local function char1_talk()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char1] = true
	end
	local function char2_talk()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char2] = true
	end
	Conversation.create( {
		function( var ) 
			if mapvar.actors[2] then
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_1",
		function( var )
			SetObjAnim( prof, "catch_breath", false )	
		end,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_2",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_3",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_4",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_5",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_6",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_7",
		prof_talk2,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_8",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_9",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_10",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_11",
		{ sprite = "portrait-prof" },
		prof_talk2,
		"PEAR15_OPENING_12",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_13",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_14",
		prof_talk1,
		"PEAR15_OPENING_15",
		prof_talk1,
		"PEAR15_OPENING_16",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_17",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_18",
		conversation_end,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_19",
		function( var )
			SetObjAnim( prof, "catch_breath", false )	
		end,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_20",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_21",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_22",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_23",
		{ sprite = "portrait-prof" },
		prof_talk2,
		"PEAR15_OPENING_24",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_25",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_26",
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_27",
		{ sprite = "portrait-prof" },
		prof_talk2,
		"PEAR15_OPENING_28",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_29",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_30",
		prof_talk1,
		"PEAR15_OPENING_31",
		prof_talk1,
		"PEAR15_OPENING_32",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_33",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_34",
		"PEAR15_OPENING_35",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_36",
		conversation_end,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_37",
		function( var )
			SetObjAnim( prof, "catch_breath", false )	
		end,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_38",
		function( var )
			SetObjAnim( char2, "move", false )
			SetObjPos( char2, 263, 150 )
			char2:acc( { x = -1.75, y = 0 } )
			SetObjProcessor( char2, function( obj )
				if obj:aabb().p.x <= 67 then
					SetObjAnim( obj, "stop", false )
					obj:acc( { x = 0, y = 0 } )
					pl1_done = true
					SetObjProcessor( char2, function() end )
				end
			end )
		end,
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_39",
		prof_talk2,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_40",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_41",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_42",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_43",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_44",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_45",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_46",
		{ sprite = "portrait-prof" },
		prof_talk2,
		"PEAR15_OPENING_47",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_48",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_49",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_50",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_51",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_52",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_53",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_54",
		{ sprite = "portrait-prof" },
		prof_talk1,
		"PEAR15_OPENING_55",
		prof_talk1,
		"PEAR15_OPENING_56",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_57",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_58",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_59",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_60",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_OPENING_61",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_OPENING_62",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_OPENING_63",
		conversation_end,
		nil,
	} ):start()
end

local function phase2_skip(key)
	RemoveSkipKey()
	StopThread( current_phase )
	if skip_all_key(key) then
		return end_this()
	end
	SetObjProcessor( char1, function() end )
	char1:acc({x=0, y=0})
	SetObjPos( char1, 37, 150 )
	char1:sprite_mirrored( true )
	start( phase3 )
end

local function phase2()
	Wait(1)
	SetSkipKey( phase2_skip, 200 )
	Wait( 1000 )
	local pl1_done = false
	SetObjPos( char1, 263, 150 )
	char1:acc( { x = -1.75, y = 0 } )
	SetObjAnim( char1, "move", false )
	SetObjProcessor( char1, function( obj )
		if obj:aabb().p.x <= 37 then
			SetObjAnim( obj, "stop", false )
			obj:acc( { x = 0, y = 0 } )
			pl1_done = true
			SetObjProcessor( char1, function() end )
		end
	end )
	while not pl1_done do
		Wait( 1 )
	end
	RemoveSkipKey()
	start( phase3 )
end

local function phase1_skip(key)
	RemoveSkipKey()
	StopThread( current_phase )
	mapvar.tmp.fade_complete = false
	Game.fade_in( true )
	if skip_all_key(key) then
		return end_this()
	end
	Resume( NewMapThread( function()
		while not mapvar.tmp.fade_complete do
			Wait(1)
		end
		if not prof then
			prof = GetObjectUserdata( CreateEnemy( "prof", -136, 159 ) )
		end
		char1 = GetPlayerCharacter( 1 )
		char2 = GetPlayerCharacter( 2 )
		if Game.actors[2] and vars.character == "pear15unyl" then
			char1, char2 = char2, char1
		end
		SetObjPos( prof, -136, 159 )
		start( phase2 )
	end ))
end

local function phase1()
	Game.fade_out()
	EnablePlayerControl( false )
	SetSkipKey( phase1_skip, 1000 )
	Wait( 1000 )
	char1 = GetPlayerCharacter( 1 )
	char2 = GetPlayerCharacter( 2 )
	if Game.actors[2] and vars.character == "pear15unyl" then
		char1, char2 = char2, char1
	end
	SetObjPos( char1, 303, 50 )
	if char2 then
		SetObjPos( char2, 303, 50 )
	end
	prof = GetObjectUserdata( CreateEnemy( "prof", -136, 159 ) )
	SetObjPos( prof, -136, 159 )
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	RemoveSkipKey()
	start( phase2 )
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if Game.profile.info_pages["CAKE"] and not (Game.profile.info_pages[k] == Info.LOCKED) then
		Game.GainScore( GetPlayerCharacter(1), 1000, 9000, 9000 )
		if GetPlayerCharacter(2) then
			Game.GainScore( GetPlayerCharacter(2), 1000, 9000, 9000 )
		end
	end
	mapvar.tmp.cutscene = true
	Game.info.can_join = false
	SetCamBounds( -369, -369 + 640, -205, -205 + 480 )
	SetCamUseBounds( true )
	CamMoveToPos( -369 + 320, -205 + 240 )
	EnablePlayerControl( false )
	Menu:lock()
	Resume( NewMapThread( function()
		Wait(1)
		start( phase1 )
	end ))
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	Loader.ChangeLevel( "pear15sewers" )
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{303, 50}}
--LEVEL.spawnpoints = {{243, 150}}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Cutscene'

return LEVEL
