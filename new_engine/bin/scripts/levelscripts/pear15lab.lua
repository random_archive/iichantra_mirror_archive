local LEVEL = {}
InitNewGame()
dofile("levels/pear15lab.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

LEVEL.secrets = 9

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
StopBackMusic()
	local function CreateFP( wp )
		local fp = CreateEnemy( "flyingplatform", wp[1][1], wp[1][2] )
		SetObjPos( fp, wp[1][1], wp[1][2] )
		local last = CreateWaypoint( wp[1][1], wp[1][2], 32, 32 )
		local first = last
		local nxt
		for i=2,#wp do
			nxt = CreateWaypoint( wp[i][1], wp[i][2], 32, 32 )
			SetNextWaypoint( last, nxt )
			last = nxt
		end
		SetNextWaypoint( last, first )
		SetEnemyWaypoint( fp, first )
	end
	CreateFP{ { 6451, -4971 }, {6981, -4971} }
	CreateFP{ { 6981, -5054 }, {6451, -5054} }
	CreateFP{ { 6451, -5179 }, {6981, -5179} }
	CreateFP{ { 7016, -6425 }, {7231, -6425} }
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	local sx, sy = GetCaptionSize( "default", "������� 7" )
	local text1 = CreateWidget( constants.wt_Label, "TEXT", mapvar.tmp.fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text1"] = text1
	WidgetSetCaptionColor( text1, {1,1,1,1}, false )
	WidgetSetCaption( text1, "������� 7" )
	WidgetSetZ( text1, 1.05 )
	sx, sy = GetCaptionSize( "default", "������������ �������" )
	local text2 = CreateWidget( constants.wt_Label, "TEXT", mapvar.tmp.fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text2"] = text2
	WidgetSetCaptionColor( text2, {1,1,1,1}, false )
	WidgetSetCaption( text2, "������������ �������" )
	WidgetSetZ( text2, 1.05 )
	CamMoveToPos( -1092,-165 )
	SetCamAttachedAxis(true, true)
	SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
	SetCamObjOffset(0, 60)
	SetCamLag(0.9)
	Game.AttachCamToPlayers( {
					{ {-9999,-9999,9999,9999},  {-1369,-6987, 8404,-60} }
				} )
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	
	EnablePlayerControl( false )
	Menu.lock()
	
	local start_intro_thread = false
	local fade_thread = NewThread( function()
		Wait( 1 )
		GUI:hide()
		Wait( 1000 )
		mapvar.tmp.fade_complete = false
		Game.fade_in()
		start_intro_thread = true
	end )
	
	Resume( fade_thread )
	
	local intro_thread = NewThread( function()
			while not start_intro_thread do
				Wait( 10 )
			end
			--SetSkipKey( function_skip, 100 )
			Game.info.can_join = false
			mapvar.tmp.cutscene = true
			
			SetObjPos( mapvar.actors[1].char.id,-1092,-165 )
			if mapvar.actors[2] then
				SetObjPos( mapvar.actors[2].char.id, -1092-30,-165 )
			end
			--
			mapvar.tmp.fade_complete = false
			Game.fade_in()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end

			Wait( 200 )
			
			local cx, cy = GetCamPos()
			local actor = GetObjectUserdata(mapvar.actors[1].char.id)
			
			RemoveSkipKey()
			local conversation_end = function()
				mapvar.tmp.cutscene = false
				SetDefaultActor( 1 )
				EnablePlayerControl( true )
				Game.info.can_join = true
				SetCamLag(0.9)
				Menu.unlock()
				GUI:show()
				PlayBackMusic("music/iie_escape.it",0.7)
				--
				
				--
			end
			local char1, char2 = Game.GetCharacters()
			local char1_talk = function()
				mapvar.tmp.talk = mapvar.tmp.talk or {}
				mapvar.tmp.talk[char1] = true
			end
			local char2_talk = function()
				mapvar.tmp.talk = mapvar.tmp.talk or {}
				mapvar.tmp.talk[char2] = true
			end
			Conversation.create( {
				function( var ) 
					if mapvar.actors[2] then
						return "BOTH"
					elseif mapvar.actors[1].info.character == "pear15unyl" then
						return "UNYL-SINGLE"
					end
				end,
				{ sprite = "portrait-soh" },
				char1_talk,
				"������ ���� �����������. ��� ����� ����� �����-��. � ������ ������ � ��� ������ ������� ������ �� �������������� ����?",
				conversation_end,
				nil,
				{ type = "LABEL", "UNYL-SINGLE" },
				char1_talk,
				{ sprite = "portrait-unyl" },
				"��... ��� ��������� ����������, ��� � �������.",
				conversation_end,
				nil,
				{ type = "LABEL", "BOTH" },
				{ sprite = "portrait-unyl" },
				char2_talk,
				"��... ��� ��������� ����������, ��� � �������.",
				{ sprite = "portrait-soh" },
				char1_talk,
				"�� ��, ��� ����� ������������ ���-��.",
				conversation_end,
				nil,
			} ):start(false)
		end)
		
	SetSkipKey( function( key )
		if key == CONFIG.key_conf[2].gui_nav_accept then
			return true
		end
		if not start_intro_thread then
			StopThread( fade_thread )
			GUI:hide()
			Game.reset_fade()
			start_intro_thread = true
			SetObjPos( mapvar.actors[1].char.id,-1092,-160 )
			if mapvar.actors[2] then
				SetObjPos( mapvar.actors[2].char.id, -1092-30,-160 )
			end
		end
		StopThread( intro_thread )
		mapvar.tmp.cutscene = false
		EnablePlayerControl( true )
		SetCamLag(0.9)
		Menu.unlock()
		GUI:show()
		Game.info.can_join = true
		PlayBackMusic("music/iie_escape.it",0.7)
	end, 500 )
	
	Resume( intro_thread )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateEnemy( 'turret-wall', -1332, -1248 )
	SetObjSpriteMirrored( obj1, false )
	local obj2 = CreateEnemy( 'turret-wall', -1332, -1477 )
	SetObjSpriteMirrored( obj2, false )
	local obj3 = CreateEnemy( 'topturret', 603, -1194 )
	SetObjSpriteMirrored( obj3, false )
	local obj4 = CreateEnemy( 'topturret', 477, -1194 )
	SetObjSpriteMirrored( obj4, false )
	local obj5 = CreateEnemy( 'topturret', 295, -1194 )
	SetObjSpriteMirrored( obj5, false )
	local obj6 = CreateEnemy( 'topturret', 73, -1194 )
	SetObjSpriteMirrored( obj6, false )
	local obj7 = CreateEnemy( 'topturret', -19, -1194 )
	SetObjSpriteMirrored( obj7, false )
	local obj8 = CreateEnemy( 'topturret', -149, -1194 )
	SetObjSpriteMirrored( obj8, false )
	local obj9 = CreateItem( 'generic-trigger', 1205, -5037 )
	local obj10 = CreateItem( 'generic-trigger', 5846, -4268 )
	GroupObjects( obj9, obj10 )
	local obj11 = CreateItem( 'generic-trigger', 1461, -2415 )
	local obj12 = CreateItem( 'generic-trigger', 1828, -1284 )
	GroupObjects( obj11, obj12 )
	local obj13 = CreateItem( 'generic-trigger', 6372, -1840 )
	local obj14 = CreateItem( 'generic-trigger', 6657, -1268 )
	GroupObjects( obj13, obj14 )
	local obj15 = CreateEnemy( 'topturret', 1361, -307 )
	SetObjSpriteMirrored( obj15, false )
	local obj16 = CreateEnemy( 'topturret', 1744, -1194 )
	SetObjSpriteMirrored( obj16, false )
	local obj17 = CreateEnemy( 'topturret', 1695, -1194 )
	SetObjSpriteMirrored( obj17, false )
	local obj18 = CreateEnemy( 'topturret', 1645, -1195 )
	SetObjSpriteMirrored( obj18, false )
	local obj19 = CreateEnemy( 'topturret', 1600, -1195 )
	SetObjSpriteMirrored( obj19, false )
	local obj20 = CreateEnemy( 'topturret', 1553, -1194 )
	SetObjSpriteMirrored( obj20, false )
	local obj21 = CreateEnemy( 'topturret', 2048, -599 )
	SetObjSpriteMirrored( obj21, false )
	local obj22 = CreateEnemy( 'topturret', 2490, -599 )
	SetObjSpriteMirrored( obj22, false )
	local obj23 = CreateEnemy( 'topturret', 3760, -1789 )
	SetObjSpriteMirrored( obj23, false )
	local obj24 = CreateEnemy( 'topturret', 3824, -1789 )
	SetObjSpriteMirrored( obj24, false )
	local obj25 = CreateEnemy( 'topturret', 3887, -1789 )
	SetObjSpriteMirrored( obj25, false )
	local obj26 = CreateEnemy( 'jumppad_lab_generic', 8017, -726 )
	SetObjSpriteMirrored( obj26, false )
	local obj27 = CreateEnemy( 'turret-wall', 8354, -1119 )
	SetObjSpriteMirrored( obj27, true )
	local obj28 = CreateEnemy( 'turret-wall', 8354, -1069 )
	SetObjSpriteMirrored( obj28, true )
	local obj29 = CreateEnemy( 'jumppad_lab_generic', 4530, -853 )
	SetObjSpriteMirrored( obj29, false )
	local obj30 = CreateEnemy( 'jumppad_lab_generic', 4423, -850 )
	SetObjSpriteMirrored( obj30, false )
	local obj31 = CreateEnemy( 'jumppad_lab_generic', 4583, -809 )
	SetObjSpriteMirrored( obj31, false )
	local obj32 = CreateEnemy( 'jumppad_lab_generic', 4370, -798 )
	SetObjSpriteMirrored( obj32, false )
	local obj33 = CreateEnemy( 'jumppad_lab_generic', 6146, -544 )
	SetObjSpriteMirrored( obj33, false )
	local obj34 = CreateEnemy( 'topturret', 6944, -1195 )
	SetObjSpriteMirrored( obj34, false )
	local obj35 = CreateEnemy( 'topturret', 7007, -1195 )
	SetObjSpriteMirrored( obj35, false )
	local obj36 = CreateEnemy( 'topturret', 6880, -1195 )
	SetObjSpriteMirrored( obj36, false )
	local obj37 = CreateEnemy( 'topturret', 6699, -1193 )
	SetObjSpriteMirrored( obj37, false )
	local obj38 = CreateEnemy( 'topturret', 6572, -1193 )
	SetObjSpriteMirrored( obj38, false )
	local obj39 = CreateEnemy( 'topturret', 6397, -1193 )
	SetObjSpriteMirrored( obj39, false )
	local obj40 = CreateEnemy( 'topturret', 6461, -1193 )
	SetObjSpriteMirrored( obj40, false )
	local obj41 = CreateEnemy( 'topturret', 6334, -1193 )
	SetObjSpriteMirrored( obj41, false )
	local obj42 = CreateEnemy( 'jumppad_lab_generic', 7910, -726 )
	SetObjSpriteMirrored( obj42, false )
	local obj43 = CreateEnemy( 'jumppad_lab_generic', 7614, -1432 )
	SetObjSpriteMirrored( obj43, false )
	local obj44 = CreateEnemy( 'jumppad_lab_generic', 8260, -1541 )
	SetObjSpriteMirrored( obj44, false )
	local obj45 = CreateEnemy( 'jumppad_lab_generic', 7683, -1750 )
	SetObjSpriteMirrored( obj45, false )
	local obj46 = CreateEnemy( 'jumppad_lab_generic', 8262, -1896 )
	SetObjSpriteMirrored( obj46, false )
	local obj47 = CreateEnemy( 'jumppad_lab_generic', 8261, -2152 )
	SetObjSpriteMirrored( obj47, false )
	local obj48 = CreateEnemy( 'jumppad_lab_generic', 8194, -214 )
	SetObjSpriteMirrored( obj48, false )
	local obj49 = CreateEnemy( 'jumppad_lab_generic', 6663, -235 )
	SetObjSpriteMirrored( obj49, false )
	local obj50 = CreateEnemy( 'topturret', 6641, -2733 )
	SetObjSpriteMirrored( obj50, false )
	local obj51 = CreateEnemy( 'topturret', 6770, -2733 )
	SetObjSpriteMirrored( obj51, false )
	local obj52 = CreateEnemy( 'topturret', 6955, -2510 )
	SetObjSpriteMirrored( obj52, false )
	local obj53 = CreateEnemy( 'topturret', 7018, -2510 )
	SetObjSpriteMirrored( obj53, false )
	local obj54 = CreateItem( 'generic-death-area', 830, 328 )
	local obj55 = CreateItem( 'generic-death-area', 6818, 828 )
	GroupObjects( obj54, obj55 )
	local obj56 = CreateEnemy( 'topturret', 64, -3871 )
	SetObjSpriteMirrored( obj56, false )
	local obj57 = CreateEnemy( 'topturret', 95, -3871 )
	SetObjSpriteMirrored( obj57, false )
	local obj58 = CreateEnemy( 'topturret', 127, -3871 )
	SetObjSpriteMirrored( obj58, false )
	local obj59 = CreateItem( 'generic-trigger', 3432, -876 )
	local obj60 = CreateItem( 'generic-trigger', 3648, -736 )
	GroupObjects( obj59, obj60 )
	local obj61 = CreateItem( 'generic-trigger', 227, -2675 )
	local obj62 = CreateItem( 'generic-trigger', 384, -2495 )
	GroupObjects( obj61, obj62 )
	local obj63 = CreateItem( 'generic-trigger', 319, -2016 )
	local obj64 = CreateItem( 'generic-trigger', 429, -1952 )
	GroupObjects( obj63, obj64 )
	local obj65 = CreateItem( 'generic-trigger', 3327, -2080 )
	local obj66 = CreateItem( 'generic-trigger', 3424, -1983 )
	GroupObjects( obj65, obj66 )
	local obj67 = CreateItem( 'generic-trigger', 4095, -1169 )
	local obj68 = CreateItem( 'generic-trigger', 4172, -1087 )
	GroupObjects( obj67, obj68 )
	local obj69 = CreateItem( 'generic-trigger', 5567, -3219 )
	local obj70 = CreateItem( 'generic-trigger', 5716, -3122 )
	GroupObjects( obj69, obj70 )
	local obj71 = CreateItem( 'generic-trigger', 4555, -2581 )
	local obj72 = CreateItem( 'generic-trigger', 4636, -2520 )
	GroupObjects( obj71, obj72 )
	local obj73 = CreateItem( 'generic-trigger', 8223, -3712 )
	local obj74 = CreateItem( 'generic-trigger', 8320, -3606 )
	GroupObjects( obj73, obj74 )
	local obj75 = CreateItem( 'generic-trigger', 7639, -5056 )
	local obj76 = CreateItem( 'generic-trigger', 7744, -4938 )
	GroupObjects( obj75, obj76 )
	local obj77 = CreateItem( 'generic-trigger', 3163, -282 )
	local obj78 = CreateItem( 'generic-trigger', 3282, -81 )
	GroupObjects( obj77, obj78 )
	local obj79 = CreateItem( 'generic-trigger', 320, -2066 )
	local obj80 = CreateItem( 'generic-trigger', 436, -1916 )
	GroupObjects( obj79, obj80 )
	local obj81 = CreateItem( 'generic-trigger', 8238, -291 )
	local obj82 = CreateItem( 'generic-trigger', 8362, -117 )
	GroupObjects( obj81, obj82 )
	local obj83 = CreateItem( 'generic-trigger', 3775, -2071 )
	local obj84 = CreateItem( 'generic-trigger', 3887, -1897 )
	GroupObjects( obj83, obj84 )
	local obj85 = CreateItem( 'generic-trigger', -1245, -3264 )
	local obj86 = CreateItem( 'generic-trigger', -1137, -3086 )
	GroupObjects( obj85, obj86 )
	local obj87 = CreateItem( 'generic-death-area', 889, -1235 )
	local obj88 = CreateItem( 'generic-death-area', 1153, -1217 )
	GroupObjects( obj87, obj88 )
	local obj89 = CreateItem( 'generic-death-area', 703, -3019 )
	local obj90 = CreateItem( 'generic-death-area', 1152, -2996 )
	GroupObjects( obj89, obj90 )
	local obj91 = CreateItem( 'generic-death-area', 5910, -1232 )
	local obj92 = CreateItem( 'generic-death-area', 6177, -1216 )
	GroupObjects( obj91, obj92 )
	local obj93 = CreateItem( 'generic-death-area', 5912, -1829 )
	local obj94 = CreateItem( 'generic-death-area', 6181, -1811 )
	GroupObjects( obj93, obj94 )
	ObjectPushInt( obj1, 1000 )
	ObjectPushInt( obj2, 1000 )
	ObjectPushInt( obj3, 250 )
	ObjectPushInt( obj4, 750 )
	ObjectPushInt( obj5, 500 )
	ObjectPushInt( obj6, 250 )
	ObjectPushInt( obj7, 750 )
	ObjectPushInt( obj8, 500 )
	ObjectPushInt( obj9, 2 )
	ObjectPushInt( obj11, 3 )
	ObjectPushInt( obj13, 4 )
	ObjectPushInt( obj15, 500 )
	ObjectPushInt( obj16, 250 )
	ObjectPushInt( obj17, 500 )
	ObjectPushInt( obj18, 750 )
	ObjectPushInt( obj19, 500 )
	ObjectPushInt( obj20, 250 )
	ObjectPushInt( obj21, 250 )
	ObjectPushInt( obj22, 250 )
	ObjectPushInt( obj23, 1500 )
	ObjectPushInt( obj24, 1250 )
	ObjectPushInt( obj25, 1000 )
	ObjectPushInt( obj26, 3000 )
	ObjectPushInt( obj26, -9000 )
	ObjectPushInt( obj27, 500 )
	ObjectPushInt( obj28, 1000 )
	ObjectPushInt( obj29, -6000 )
	ObjectPushInt( obj29, -7000 )
	ObjectPushInt( obj30, 500 )
	ObjectPushInt( obj30, -9000 )
	ObjectPushInt( obj31, 12000 )
	ObjectPushInt( obj31, -5000 )
	ObjectPushInt( obj32, 6000 )
	ObjectPushInt( obj32, -5000 )
	ObjectPushInt( obj33, -3000 )
	ObjectPushInt( obj33, -9000 )
	ObjectPushInt( obj34, 1000 )
	ObjectPushInt( obj35, 500 )
	ObjectPushInt( obj36, 1250 )
	ObjectPushInt( obj37, 500 )
	ObjectPushInt( obj38, 500 )
	ObjectPushInt( obj39, 1000 )
	ObjectPushInt( obj40, 1250 )
	ObjectPushInt( obj41, 500 )
	ObjectPushInt( obj42, -7000 )
	ObjectPushInt( obj42, -3000 )
	ObjectPushInt( obj43, 25000 )
	ObjectPushInt( obj43, -5000 )
	ObjectPushInt( obj44, -3500 )
	ObjectPushInt( obj44, -6500 )
	ObjectPushInt( obj45, 30000 )
	ObjectPushInt( obj45, -4000 )
	ObjectPushInt( obj46, 0 )
	ObjectPushInt( obj46, -6000 )
	ObjectPushInt( obj47, -10000 )
	ObjectPushInt( obj47, -4000 )
	ObjectPushInt( obj48, 0 )
	ObjectPushInt( obj48, -11000 )
	ObjectPushInt( obj49, 3000 )
	ObjectPushInt( obj49, -7000 )
	ObjectPushInt( obj50, 500 )
	ObjectPushInt( obj51, 500 )
	ObjectPushInt( obj52, 500 )
	ObjectPushInt( obj53, 500 )
	ObjectPushInt( obj56, 1000 )
	ObjectPushInt( obj57, 1000 )
	ObjectPushInt( obj58, 1000 )
	ObjectPushInt( obj59, 1 )
	ObjectPushInt( obj61, 1 )
	ObjectPushInt( obj63, 1 )
	ObjectPushInt( obj65, 1 )
	ObjectPushInt( obj67, 1 )
	ObjectPushInt( obj69, 1 )
	ObjectPushInt( obj71, 1 )
	ObjectPushInt( obj73, 1 )
	ObjectPushInt( obj75, 1 )
	ObjectPushInt( obj77, 5 )
	ObjectPushInt( obj79, 5 )
	ObjectPushInt( obj81, 5 )
	ObjectPushInt( obj83, 5 )
	ObjectPushInt( obj85, 5 )
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		mapvar.tmp.secrets = ( mapvar.tmp.secrets or 0 ) + 1
	elseif trigger == 2 then
		if not mapvar.tmp.turret_destroyed then
			Game.ProgressAchievement( "LEAVE_TURRETS_ALONE", 1 )
		end
		Game.ShowLevelResults("pear15that_guy")
	elseif trigger == 3 then
		local last_time = Loader.time
		local last_explosions = 0
		local x = 1000
		local dt = 0
		Resume( NewMapThread( function()
			while true do
				dt = Loader.time - last_time
				last_time = Loader.time
				x = x + dt * 0.2	
				if x >= 3000 then
					return
				end
				if Loader.time - last_explosions > 350 then
					last_explosions = Loader.time
					for i = 1, math.random(4, 6) do
						CreateEnemy( "big_explosion", x, math.random( -2400, -1300 ) )
					end
				end
				Wait(1)
			end
		end ))
	elseif trigger == 4 then
		local last_time = Loader.time
		local last_explosions = 0
		local x = 6000
		local dt = 0
		Resume( NewMapThread( function()
			while true do
				dt = Loader.time - last_time
				last_time = Loader.time
				x = x + dt * 0.2	
				if x >= 7500 then
					return
				end
				if Loader.time - last_explosions > 350 then
					last_explosions = Loader.time
					for i = 1, math.random(4, 6) do
						CreateEnemy( "big_explosion", x, math.random( -1800, -1200 ) )
					end
				end
				Wait(1)
			end
		end ))
	elseif trigger == 5 then
		--TODO: achievement progression
	end
	SetObjDead( id )
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{-1204,-176},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Lab conditions'

LEVEL.tips =
{
	"�� ������ ������������ �����. ������ ���� ��������.",
	"� ���������, � ���� ���� ��� ����������.",
	"������� ������� ��� ������� ������ ������� �� ������.",
	"������� �� ������ ������ ����."
}

LEVEL.solution = '���� ���� ������� ������� ������� � ����������, � ���������������� � ��� ����� ��� ������������ �����. ��������� �������� �� ������� �� ������ � ����� �� ���, ���� �� �������./n/n�� ������ ����� ������� � ������������ ����� ��� ������ �����������. �� ������ ����� ������������� ������. ���������� ������ �� ����� ����� � ������������ �� ������ ����./n/n�������� ���� ��������, ������� �� �������� � ����������� ���� ������. ������ �� ������� �������� ����, ����� �� ������ ����./n/n�������� ������ ������� � ���������� ����������� � ������������� �������. ����� ������� ��������� ������ ������ � ������������, ���� ��� �� ������ �����./n/n������ ���������� ����� �, �� �����������, �����, ���� �� ������� ���������, �� ������� ����� ��������� ������� � �������� � �������� ����. ����������� ���� � ����� ������� �� ������.'

return LEVEL
