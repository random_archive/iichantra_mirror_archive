--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/pear15tutorial.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

PlayBackMusic( "music/nsmpr_lab.it" )

function LEVEL.onPlayerDeath()
	local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
	ObjectPushInt( obj1, 28 )
	SetObjAnim( obj1, "script", false )
	return true
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	SetPlayerAltWeapon(1,"pear15soh_primary")
	SetPlayerAmmo(1,0)
	GUI:changeWeapon(1,0)
	local _key = Game.game_key_pressed
	Game.AddCleanup( function() Game.game_key_pressed = _key end )
	Game.game_key_pressed = function(key)
		if GamePaused() then
			return
		end
	end
	mapvar.actors[1].info.character = "pear15soh-unarmed"
	SwitchLighting(true)
	if GetPlayer() then
		local fade_widget = CreateWidget( constants.wt_Widget, "THE FADE", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
		WidgetSetColorBox( fade_widget, {0, 0, 0, 1} )
		WidgetSetZ( fade_widget, 1 )
		local sx, sy = GetCaptionSize( "default", "������������� �������" )
		local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
		WidgetSetCaptionColor( text1, {1,1,1,1}, false )
		WidgetSetCaption( text1, "������������� �������" )
		WidgetSetZ( text1, 1.05 )
		CamMoveToPos( 832,38 )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		Game.AttachCamToPlayers( {
			{ {-99999,-99999,99999,99999},  {550,-99999,4500,200} },
		} )
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		Game.info.can_join = false
		mapvar.tmp.cutscene = true
		
		EnablePlayerControl( false )
		Menu.lock()
		Resume( NewThread( function()
			Wait( 1 )
			mapvar.actors[1].info.character = "pear15soh-unarmed"
			GUI:hide()
			Wait( 200 )
			
			char = GetPlayerCharacter()

			for i = 100, 1, -1 do
				WidgetSetColorBox( fade_widget, {0, 0, 0, 1-1/i} )
				WidgetSetCaptionColor( text1, {1,1,1,1-1/i}, false )
				Wait( 1 )
			end
			DestroyWidget( fade_widget )
			
			local cx, cy = GetCamPos()

			local cx, cy = GetCamPos()

			local conversation_end = function()
				mapvar.tmp.cutscene = false
				EnablePlayerControl( true )
				SetCamLag(0.9)
				Menu.unlock()
				GUI:show()
				--[[
				Game.AttachCamToPlayers( {
					{ {-99999,-99999,99999,99999},  {-700,-99999,99999,-900} },
				} )
				--]]
			end
			local char_talk = function()
				mapvar.tmp.talk = mapvar.tmp.talk or {}
				mapvar.tmp.talk[char] = true
			end
			Conversation.create( {
				{ sprite = "portrait-soh" },
				char_talk,
				"�����-�� � ���� ������ ������ ������������. ���� �� �������� ���� ������, �� �� ����������� �� ������ ��� ���������� ������.",
				char_talk,
				"���, �����, ��������� �����������. ��� ��� ������ ������� ������� � ����� ���������?",
				{ sprite = "portrait-captain" },
				"����������� ������� /cffff55"..GetKeyName( CONFIG.key_conf[1].left ).."/cffffff � /cffff55"..GetKeyName( CONFIG.key_conf[1].right ).."/cffffff ��� ������������.",
				conversation_end,
				nil,
			} ):start()
		end))
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj3 = CreateEnvironment( 'default_environment', 1216, -346 )
	local obj4 = CreateEnvironment( 'default_environment', 1345, 92 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].jump ).." ��� ������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].jump ).."/cffffff ��� ������.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 1765, -384 )
	local obj4 = CreateEnvironment( 'default_environment', 1857, -12 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� ��� ��� � ������� "..GetKeyName( CONFIG.key_conf[1].jump ).." ��� �������� ������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� ��� ��� � ������� /cffff55"..GetKeyName( CONFIG.key_conf[1].jump ).."/cffffff ��� �������� ������.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 2070, -448 )
	local obj4 = CreateEnvironment( 'default_environment', 2177, -127 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "�������������� ��������� ������� ������ � ����� � ������" )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "�������������� ��������� ������� ������ � ����� � ������", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 2500, -448 )
	local obj4 = CreateEnvironment( 'default_environment', 2600, 500 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "����������� ������� "..GetKeyName( CONFIG.key_conf[1].up ).." � "..GetKeyName( CONFIG.key_conf[1].down ).." ��� ����, ����� �����������" )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "����������� ������� /cffff55"..GetKeyName( CONFIG.key_conf[1].up ).."/cffffff � /cffff55"..GetKeyName( CONFIG.key_conf[1].down ).."/cffffff ��� ����, ����� �����������", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 3020, -387 )
	local obj4 = CreateEnvironment( 'default_environment', 3082, -249 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "����������� ���������� "..GetKeyName( CONFIG.key_conf[1].sit ).." + "..GetKeyName( CONFIG.key_conf[1].jump ).." ��� ����������� � ��������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - 5*sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].sit ).."/cffffff ��� ����������./n����������� ���������� /cffff55"..GetKeyName( CONFIG.key_conf[1].sit ).."/cffffff + /cffff55"..GetKeyName( CONFIG.key_conf[1].jump ).."/cffffff ��� ����������� � ��������.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 3341, -339 )
	local obj4 = CreateEnvironment( 'default_environment', 3400, -256 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].weapon_slot1 ).." ����� ������� ������ � ������ �����." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - 5*sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].weapon_slot1 ).."/cffffff ����� ������� ������ � ������ �����./n������� /cffff55"..GetKeyName( CONFIG.key_conf[1].fire ).."/cffffff ��� ��������.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )

	local obj3 = CreateEnvironment( 'default_environment', 3667, -545 )
	local obj4 = CreateEnvironment( 'default_environment', 3800, -235 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].weapon_slot1 ).." ��� �������������� � ���������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - 5*sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].player_use ).."/cffffff ��� �������������� � ���������.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )

	local obj3 = CreateEnvironment( 'default_environment', 4095, -347 )
	local obj4 = CreateEnvironment( 'default_environment', 4160, -248 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].weapon_slot1 ).." ����� �������� �������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - 5*sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].player_use ).."/cffffff ����� �������� �������.", true )
				WidgetSetZ( text, 0.5 )
				mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
		on_use = function( ud, x, y, this )
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			GUI:hide()
			EnablePlayerControl(false)
			Resume( NewThread( function()
				GroupObjects( obj3, obj4 )
				local fade_widget = CreateWidget( constants.wt_Widget, "THE FADE", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
				WidgetSetColorBox( fade_widget, {0, 0, 0, 1} )
				WidgetSetZ( fade_widget, 1 )
				for i = 1, 100, 1 do
					WidgetSetColorBox( fade_widget, {0, 0, 0, i/100} )
					Wait( 1 )
				end
				SetCamBounds( -9000, -9000 + 640, -9000, -9000 + 480 )
				CamMoveToPos( -9000 + 320, -9000 + 240 )
				DestroyWidget( fade_widget )
				local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
				ObjectPushInt( obj1, 3 )
				SetObjAnim( obj1, "script", false )
			end))
		end,
	} )

	local obj3 = CreateEnvironment( 'default_environment', 3851, -328 )
	local obj4 = CreateEnvironment( 'default_environment', 3892, -249 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if mapvar.tmp[this] then return end
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
		on_use = function( ud, x, y, this )
			if mapvar.tmp[this] then return end
			--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			mapvar.tmp[this] = true
			EnablePlayerControl( false )
			Menu.lock()
			Resume( NewThread( function()
				Wait( 1 )
				GUI:hide()
				
				char = GetPlayerCharacter()
				
				local cx, cy = GetCamPos()
				
				local conversation_end = function()
					EnablePlayerControl( true )
					Menu.unlock()
					GUI:show()
					mapvar.tmp[this] = false
				end
				local char_talk = function()
					mapvar.tmp.talk = mapvar.tmp.talk or {}
					mapvar.tmp.talk[char] = true
				end
				Conversation.create( {
					{ sprite = "portrait-soh" },
					function( var )
						mapvar.tmp.d1_stage = math.min( (mapvar.tmp.d1_stage or 0) + 1, 3 )
						return tostring(mapvar.tmp.d1_stage)
					end,
					{ type = "LABEL", "1" },
					char_talk,
					"��� ����-���� ��� ����� �� ������.",
					char_talk,
					conversation_end,
					nil,
					{ type = "LABEL", "2" },
					char_talk,
					"��, �������� �������. ����� ����� ������� ����������� ����� �������. �� ���� ������ ������ ���� �����.",
					char_talk,
					conversation_end,
					nil,
					{ type = "LABEL", "3" },
					char_talk,
					"�� ��� �������.",
					char_talk,
					conversation_end,
					nil,
				} ):start()
			end))
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 3954, -335 )
	local obj4 = CreateEnvironment( 'default_environment', 3997, -238 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				mapvar.tmp[ud].effect = CreateEffect("dialogue", 0, 0, ud:id(), 1)
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
		on_use = function( ud, x, y, this )
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			EnablePlayerControl( false )
			Menu.lock()
			Resume( NewThread( function()
				Wait( 1 )
				GUI:hide()
				mapvar.tmp.cutscene = true
				
				char = GetPlayerCharacter()
				
				local cx, cy = GetCamPos()
				
				local cx, cy = GetCamPos()
				
				local conversation_end = function()
					GUI:hide()
					mapvar.tmp.getweapon = true
					mapvar.actors[1].info.character = "pear15soh"
					GroupObjects( obj3, obj4 )
					local fade_widget = CreateWidget( constants.wt_Widget, "THE FADE", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
					WidgetSetColorBox( fade_widget, {0, 0, 0, 1} )
					WidgetSetZ( fade_widget, 1 )
					for i = 1, 100, 1 do
						WidgetSetColorBox( fade_widget, {0, 0, 0, i/100} )
						Wait( 1 )
					end
					SetCamBounds( -9000, -9000 + 640, -9000, -9000 + 480 )
					CamMoveToPos( -9000 + 320, -9000 + 240 )
					DestroyWidget( fade_widget )
					local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
					ObjectPushInt( obj1, 4 )
					SetObjAnim( obj1, "script", false )
				end
				local conversation_end2 = function()
					mapvar.tmp.cutscene = false
					EnablePlayerControl( true )
					Menu.unlock()
					GUI:show()
					mapvar.tmp[ud].effect = CreateEffect("dialogue", 0, 0, ud:id(), 1)
				end
				local char_talk = function()
					mapvar.tmp.talk = mapvar.tmp.talk or {}
					mapvar.tmp.talk[char] = true
				end
				Conversation.create( {
					function( var )
						if mapvar.tmp.no_weapons then
							return "3"
						end
						mapvar.tmp.d2_stage = math.min( (mapvar.tmp.d2_stage or 0) + 1, 2 )
						return tostring(mapvar.tmp.d2_stage)
					end,
					{ type = "LABEL", "1" },
					{ sprite = "portrait-guard" },
					"�������! ����� � ������ ����������� - ����� ����� �� ���� ������. ����, ��, �������, �� �������� ����������� �������� ����...",
					{ sprite = "portrait-soh" },
					char_talk,
					"�������� ����?",
					{ sprite = "portrait-guard" },
					"�� ��. ������ � �������, ��� ����� � ���� �������. ������ �������� �����?",
					{ sprite = "portrait-soh" },
					char_talk,
					"��, ������� �� ��� �����? ��, ��� ���� ��������������.",
					{ sprite = "portrait-guard" },
					"��� ���, �� ��� ����������� ������ �����������. �� � ������������ ������ ��� ��������� ��, �� � ��� ���-�� ���������. �� ����������������� �������-���������� ���, ��� ��� ������ �������� �� ����, ���� ������.",
					"�������, �� ����, �� �� ������ ���� ������ �������, �� ������ ����� ������� ����������.",
					{ sprite = "portrait-soh" },
					char_talk,
					"���, �������.",
					{ sprite = "portrait-guard" },
					"�������, � ��� ������ �������� �� ������. ���� � ���, ��� ���� �� ��� ���� ���, ��� ������ ��������� �������, ��� �� ������. �� ������ ������ ������ ��� ��� - ������ �����������?",
					{ type = "SELECTION", { "��", "���" }, { "SOH-AGREE", "SOH-DISAGREE" }  },
					{ type = "LABEL", "SOH-AGREE" },
					{ sprite = "portrait-soh" },
					char_talk,
					"����...? ��, � �����, ���-�� ������ ����������� � ���� ��������� ��, ������?",
					function( var )
						if mapvar.tmp.weapons_out then
							return "WEAPONS"
						else
							mapvar.tmp.no_weapons = true
							return "NOWEAPONS"
						end
					end,
					{ type = "LABEL", "WEAPONS" },
					{ sprite = "portrait-guard" },
					"���, ��� � ����, ��� �� ���������������. ������, ������ ������.",
					conversation_end,
					nil,
					{ type = "LABEL", "NOWEAPONS" },
					{ sprite = "portrait-guard" },
					"���, ��� � ����, ��� �� ���������������. ������ ������ ����� �� ������. �������� �� ���� �����, ��� �����������.",
					conversation_end2,
					nil,
					{ type = "LABEL", "SOH-DISAGREE" },
					char_talk,
					{ sprite = "portrait-soh" },
					"�� �� ����� ��������! ���� ������� ������ ���� �� �������, �� � � �� �����.",
					{ sprite = "portrait-guard" },
					"�� ������. ��� ���� ����� ��������. ��� ����� �� ������.",
					conversation_end2,
					nil,
					{ type = "LABEL", "2" },
					{ sprite = "portrait-guard" },
					"�� ����������� �� ��� � ����. �� ��� ����������?",
					{ type = "SELECTION", { "��", "���" }, { "SOH-AGREE", "SOH-DISAGREE" }  },
					{ type = "LABEL", "3" },
					{ sprite = "portrait-soh" },
					"� ������!",
					function( var )
						if mapvar.tmp.weapons_out then
							return "WEAPONS2"
						else
							mapvar.tmp.no_weapons = true
							return "NOWEAPONS2"
						end
					end,
					{ type = "LABEL", "WEAPONS2" },
					{ sprite = "portrait-guard" },
					"����. ������, ������ ������...",
					conversation_end,
					nil,
					{ type = "LABEL", "NOWEAPONS2" },
					{ sprite = "portrait-guard" },
					"���-�� � �� ���� � ���� � ����� ������. �� ������ �� ��� �������, ��� ��?",
					conversation_end2,
					nil
				} ):start()
			end))
		end,
	} )
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		mapvar.tmp.secrets = ( mapvar.tmp.secrets or 0 ) + 1
	elseif trigger == 2 then
		Game.ProgressAchievement( "LICENSED_TO_PLAY", 1 )
		GUI:hide()
		Menu.showMainMenu()
	elseif trigger == 3 then
		GUI:hide()
		Menu.showMainMenu()
	elseif trigger == 4 then
		Loader.ChangeLevel( "pear15tutorial2" )
	elseif trigger == 28 then
		Loader.restartGame()
	end
	SetObjDead( id )
--$(MAP_TRIGGER)-
end

function LEVEL.GetNextCharacter()
	if not Loader.level or not Loader.level.spawnpoints then
		return nil
	end
	if Game.actors[1] and Game.actors[1].char then
		Game.info.players = Game.info.players + 1
	else
		Game.info.players = 1
	end
	if Game.info.players == 1 then
		--vars.character or Game.info.first_player_character or 
		if true then
			return { "pear15soh-unarmed", Loader.level.spawnpoints[1] or {0,0} }
		end
	else
		local pos = GetObjectUserdata( Game.mapvar.actors[1].char.id ):aabb().p
		if Game.actors[1].info.character == "pear15soh" then
			return { "pear15unyl", {pos.x, pos.y} }
		else
			return { "pear15soh", {pos.x, pos.y} }
		end
	end
end

LEVEL.characters = {}
LEVEL.spawnpoints = {{832,38},}

function LEVEL.cleanUp()
	ud = GetPlayerCharacter()
	if ( ud and mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'pear15tutorial'

return LEVEL
