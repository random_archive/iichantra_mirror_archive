--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/weapontest.lua")

CONFIG.backcolor_r = 0.011719
CONFIG.backcolor_g = 0.011719
CONFIG.backcolor_b = 0.011719
LoadConfig()

StopBackMusic()
local dir, dirs = ListDirContents( "sounds/music" )
PlayBackMusic( "music/"..dir[math.random(dirs+1, #dir)] )

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function target_damage( id, ammount )
	last_hit = ammount
	global_damage = (global_damage or 0) + ammount
end

local p_target = {
	texture = "snowman";
	z = 0.0;
	
	phys_max_x_vel = 5;
	phys_max_y_vel = 5;
	
	faction_id = 1;
	physic = 1;
	phys_solid = 0;
	phys_bullet_collidable = 1;
	
	mass = -1;
	
	animations = 
	{
		{
			name = "init";
			frames =
			{
				{ com = constants.AnimComRealW; param = 51 },
				{ com = constants.AnimComRealH; param = 78 },
				{ com = constants.AnimComSetAnim; txt = "idle" }
			}
		},
		{
			name = "idle";
			frames =
			{
				{ num = 0; dur = 1 }
			}
		},
		{
			name = "pain";
			frames =
			{
				{ com = constants.AnimComPop },
				{ com = constants.AnimComCallFunctionWithStackParameter; txt = "target_damage" },
				{ com = constants.AnimComSetAnim; txt = "idle" }
			}
		}
	}
}

function t()
	CreateEnemy( "crate", GetMousePos() )
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
	Loader.level.MapScript = function() end
	Resume( NewMapThread( function()
			local widge = CreateWidget( constants.wt_Label, "WHATEVER", nil, CONFIG.scr_width/2, 100, 1, 1 )
			WidgetSetCaptionFont( widge, "default", 2 )
			WidgetSetCaptionColor( widge, {1,1,1,1}, false )
			local ShowText = function( text )
				--Log( text )
				local sx, sy = GetCaptionSize( "default", text ) * 2
				WidgetSetPos( widge, ( CONFIG.scr_width - sx )/2, 100 )
				WidgetSetCaption( widge, text )
				WidgetSetVisible( widge, true )
			end
			local targets = {}
			local countdown = function()
				for i=1,20 do
					Wait(1000)
					if i > 3 then
						ShowText( 20-i )
					end
				end
				for k, v in pairs( targets ) do
					SetObjDead( v )
				end
				targets = {}
			end
			local course_score = function()
				--return math.floor(global_damage/(math.floor((9000-plobj:ammo())/50)+1))
				return global_damage
			end

			ShowText( "Get ready for the first course!" )
			local plobj = GetObjectUserdata( GetPlayer().id )
			p_target = LoadPrototypeTable(p_target);
			Wait( 2000 )
			ShowText( "3..." )
			Wait( 1000 )
			ShowText( "2..." )
			Wait( 1000 )
			ShowText( "1..." )
			Wait( 1000 )
			ShowText( "GO!" )
			global_damage = 0
			plobj:ammo(9000)
			plobj:is_invincible(false)
			local tg = CreateDummyEnemy()
			SetObjPos( tg, 154, 117 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			countdown()
			local score = course_score()
			ShowText( "COURSE SCORE: "..score )
			Log( ">>>WEAPON TEST (COURSE 1): ", score )
			Wait( 2000 )
			
			ShowText( "Get ready for the second course!" )
			Wait( 2000 )
			ShowText( "3..." )
			Wait( 1000 )
			ShowText( "2..." )
			Wait( 1000 )
			ShowText( "1..." )
			Wait( 1000 )
			ShowText( "GO!" )
			global_damage = 0
			plobj:ammo(9000)
			tg = CreateDummyEnemy()
			SetObjPos( tg, 154, 117 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, 254, 117 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, 154, 17 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, 254, 17 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			countdown()
			score = score + course_score()
			ShowText( "COURSE SCORE: "..course_score() )
			Log( ">>>WEAPON TEST (COURSE 2): ", course_score() )
			Wait( 2000 )
			
			ShowText( "Get ready for the third course!" )
			Wait( 2000 )
			ShowText( "3..." )
			Wait( 1000 )
			ShowText( "2..." )
			Wait( 1000 )
			ShowText( "1..." )
			Wait( 1000 )
			ShowText( "GO!" )
			global_damage = 0
			plobj:ammo(9000)
			tg = CreateDummyEnemy()
			SetObjPos( tg, 314, 117 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, 255, -79 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, 164, -145 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			SetObjPos( tg, -111, -145 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, -247, -79 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			tg = CreateDummyEnemy()
			SetObjPos( tg, -302, 117 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			table.insert( targets, tg )
			countdown()
			score = score + course_score()
			ShowText( "COURSE SCORE: "..course_score() )
			Log( ">>>WEAPON TEST (COURSE 3): ", course_score() )
			Wait( 2000 )
			
			ShowText( "Get ready for the fourth course!" )
			Wait( 2000 )
			ShowText( "3..." )
			Wait( 1000 )
			ShowText( "2..." )
			Wait( 1000 )
			ShowText( "1..." )
			Wait( 1000 )
			ShowText( "GO!" )
			global_damage = 0
			plobj:ammo(9000)
			tg = CreateDummyEnemy()
			SetObjPos( tg, 154, 117 )
			ApplyProto( tg, p_target )
			SetObjAnim( tg, "init", false )
			SetObjProcessor( tg, function( obj )
					local ov = obj:vel()
					obj:vel( { ["x"] = ov.x+(math.random( -10, 10 )/10 ), ["y"] = ov.y+(math.random( -10, 10 )/10 ) } )
			end )
			table.insert( targets, tg )
			countdown()
			score = score + course_score()
			ShowText( "COURSE SCORE: "..course_score() )
			Log( ">>>WEAPON TEST (COURSE 4): ", course_score() )
			Wait( 2000 )
			
			Log( ">>>WEAPON TEST (FINAL): ", score )
			ShowText( "FINAL SCORE: "..math.floor(score/4) )
	end ))
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function LEVEL.GetNextCharacter()
--$(GET_NEXT_CHARACTER)+
	Loader.level.char_iterator = (Loader.level.char_iterator or 0) + 1
	return { Loader.level.characters[Loader.level.char_iterator], Loader.level.spawnpoints[Loader.level.char_iterator]}
--$(GET_NEXT_CHARACTER)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
--$(MAP_TRIGGER)-
end

LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-81,81},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'weapontest'

return LEVEL
