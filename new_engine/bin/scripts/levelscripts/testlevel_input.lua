Log("testlevel_input")
InitNewGame();			-- ������ ����� ����
difficulty = 1

Log("testlevel_input loading textures and protos")
---[[
LoadTexture("sohchan.png");
LoadTexture("bullets.png");
LoadTexture("flash-straight.png");
LoadTexture("flash-angle.png");
LoadTexture("grenade.png")
LoadTexture("grenade-explosion.png")
LoadPrototype("sohchan.lua");
LoadPrototype("sfg9000.lua");
LoadPrototype("sfg5000.lua");
LoadPrototype("flash-straight.lua");
LoadPrototype("flash-angle-up.lua");
LoadPrototype("flash-angle-down.lua");
LoadPrototype("explosion.lua");
--]]

LoadTexture("window1.png")
LoadPrototype("window1.lua");
LoadTexture("portrait-unyl.png")
LoadPrototype("portrait-unyl.lua");

---[[
LoadTexture("phys_floor.png");
LoadPrototype("phys_floor.lua");
--]]

---[[
LoadTexture("dust-run.png");
LoadPrototype("dust-run.lua");
LoadTexture("dust-land.png");
LoadPrototype("dust-land.lua");
LoadTexture("dust-stop.png");
LoadPrototype("dust-stop.lua");
--]]
---[[
LoadTexture("btard_o.png");
LoadPrototype("btard.lua");
LoadPrototype("btard-corpse.lua");
LoadPrototype("btard-punch.lua");
--]]
--[[
LoadTexture("pblood.png");
LoadPrototype("pblood.lua");
LoadPrototype("pblood-wound.lua");
--]]
---[[

Log("testlevel_input loading sounds")
LoadSound("blaster_shot.ogg");
LoadSound("foot-left.ogg");
LoadSound("foot-right.ogg");
LoadSound("land.ogg");
LoadSound("stop.ogg");
LoadSound("music\\iie_lab.it");
--]]

--LoadTexture("unyl_sprite.png");
--LoadPrototype("unylchan.lua");

--[[
LoadTexture("3nd_plan.png");
LoadPrototype("3nd_plan.lua");
--]]

-- ���������� ������
--[[
LoadTexture("clouds1.png")
LoadTexture("clouds2.png")
LoadTexture("clouds3.png")
LoadTexture("clouds4.png")
LoadPrototype("clouds1.lua")
LoadPrototype("clouds2.lua")
LoadPrototype("clouds3.lua")
LoadPrototype("clouds4.lua")


local rib = CreateRibbon("clouds4", 0, 150,1);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds3", 0, 150, 0.25);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds2", 0, 150, 0.6);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds1", 0, 150, 0.5);
SetRibbonAttatachToY(rib, true)
--]]

require("serialize")

Log("testlevel_input creating player")

CreatePlayer("sohchan", 50, 300);
--CreatePlayer("unylchan", 50, 300);

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 100);	-- ������ �������� ������ ������������ �������

Log("testlevel_input creating sprites")
local x = 0
local dx = 32

for i = 0,50 do
	CreateSprite("phys_floor", x + dx*i, 330);
end

-- CreateColorBox(x1, y1, x2, y2, z, {r, g, b, a} )
box1 = CreateColorBox(0, 200, 100, 300, 0.5, {1, 0.5, 0.5, 1} )
box2 = CreateColorBox(120, 200, 220, 300, -0.5, {0.5, 1, 0.5, 1} )
-- DESU DESU DESU DESU DESU DESU DESU DESU DESU DESU

Log("testlevel_input creating gui widgets")

local keydown_lbls = {}
local namedown_lbls = {}
local keyrel_lbls = {}
local namerel_lbls = {}

local y = 10
local dy = 10
local max_lines = 5
for i = 1,max_lines do
	local keycodedown_lbl = CreateWidget(constants.wt_Label, "keycodedown_lbl", nil, 100, y, 70, 10)
	WidgetSetCaption(keycodedown_lbl, "")
	WidgetSetCaptionColor(keycodedown_lbl, {1.0, 1.0, 1.0, 1.0}, false)
	WidgetSetCaptionColor(keycodedown_lbl, {1.0, 0.2, 0.2, 1.0}, true)
	table.insert(keydown_lbls, keycodedown_lbl)

	local keynamedown_lbl = CreateWidget(constants.wt_Label, "keynamedown_lbl", nil, 200, y, 70, 10)
	WidgetSetCaption(keynamedown_lbl, "")
	WidgetSetCaptionColor(keynamedown_lbl, {1.0, 1.0, 1.0, 1.0}, false)
	WidgetSetCaptionColor(keynamedown_lbl, {1.0, 0.2, 0.2, 1.0}, true)
	table.insert(namedown_lbls, keynamedown_lbl)

	local keycoderelease_lbl = CreateWidget(constants.wt_Label, "keycoderelease_lbl", nil, 400, y, 70, 10)
	WidgetSetCaption(keycoderelease_lbl, "")
	WidgetSetCaptionColor(keycoderelease_lbl, {1.0, 1.0, 1.0, 1.0}, false)
	WidgetSetCaptionColor(keycoderelease_lbl, {1.0, 0.2, 0.2, 1.0}, true)
	table.insert(keyrel_lbls, keycoderelease_lbl)

	local keynamerelease_lbl = CreateWidget(constants.wt_Label, "keynamerelease_lbl", nil, 500, y, 70, 10)
	WidgetSetCaption(keynamerelease_lbl, "")
	WidgetSetCaptionColor(keynamerelease_lbl, {1.0, 1.0, 1.0, 1.0}, false)
	WidgetSetCaptionColor(keynamerelease_lbl, {1.0, 0.2, 0.2, 1.0}, true)
	table.insert(namerel_lbls, keynamerelease_lbl)
	
	y = y + dy
end

local codesdown = {}
local namesdown = {}
local codesrel = {}
local namesrel = {}

function push(tbl, val)
	table.insert(tbl, 1, val)
	if (#tbl > max_lines) then
		table.remove(tbl)
	end
end

function update_labels()
	local len = #codesdown
	for i = 1,len do
		WidgetSetCaption(keydown_lbls[i], codesdown[i])
		WidgetSetCaption(namedown_lbls[i], namesdown[i])
	end
	
	local len = #codesrel
	for i = 1,len do
		WidgetSetCaption(keyrel_lbls[i], codesrel[i])
		WidgetSetCaption(namerel_lbls[i], namesrel[i])
	end
end

--function onpress(sender, key)
function onpress(key)
	push(codesdown, key)
	push(namesdown, GetKeyName(key))
	update_labels()
end

function onrelease(key)
	push(codesrel, key)
	push(namesrel, GetKeyName(key))
	update_labels()
end

GlobalSetKeyDownProc(onpress)
GlobalSetKeyReleaseProc(onrelease)

Log("testlevel_input done")




