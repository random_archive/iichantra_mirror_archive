InitNewGame();			-- ������ ����� ����
difficulty = 1
---[[
LoadTexture("sohchan.png");
LoadTexture("bullets.png");
LoadTexture("flash-straight.png");
LoadTexture("flash-angle.png");
LoadTexture("grenade.png")
LoadTexture("grenade-explosion.png")
LoadPrototype("sohchan.lua");
LoadPrototype("sfg9000.lua");
LoadPrototype("sfg5000.lua");
LoadPrototype("flash-straight.lua");
LoadPrototype("flash-angle-up.lua");
LoadPrototype("flash-angle-down.lua");
LoadPrototype("explosion.lua");
--]]

LoadTexture("window1.png")
LoadPrototype("window1.lua");
LoadTexture("portrait-unyl.png")
LoadPrototype("portrait-unyl.lua");

---[[
LoadTexture("phys_floor.png");
LoadPrototype("phys_floor.lua");
--]]

---[[
LoadTexture("dust-run.png");
LoadPrototype("dust-run.lua");
LoadTexture("dust-land.png");
LoadPrototype("dust-land.lua");
LoadTexture("dust-stop.png");
LoadPrototype("dust-stop.lua");
--]]
---[[
LoadTexture("btard_o.png");
LoadPrototype("btard.lua");
LoadPrototype("btard-corpse.lua");
LoadPrototype("btard-punch.lua");
--]]
--[[
LoadTexture("pblood.png");
LoadPrototype("pblood.lua");
LoadPrototype("pblood-wound.lua");
--]]
---[[
LoadSound("blaster_shot.ogg");
LoadSound("foot-left.ogg");
LoadSound("foot-right.ogg");
LoadSound("land.ogg");
LoadSound("stop.ogg");
LoadSound("music\\iie_lab.it");
--]]

--LoadTexture("unyl_sprite.png");
--LoadPrototype("unylchan.lua");

--[[
LoadTexture("3nd_plan.png");
LoadPrototype("3nd_plan.lua");
--]]

-- ���������� ������
--[[
LoadTexture("clouds1.png")
LoadTexture("clouds2.png")
LoadTexture("clouds3.png")
LoadTexture("clouds4.png")
LoadPrototype("clouds1.lua")
LoadPrototype("clouds2.lua")
LoadPrototype("clouds3.lua")
LoadPrototype("clouds4.lua")


local rib = CreateRibbon("clouds4", 0, 150,1);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds3", 0, 150, 0.25);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds2", 0, 150, 0.6);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds1", 0, 150, 0.5);
SetRibbonAttatachToY(rib, true)
--]]

require("serialize")

CreatePlayer("sohchan", 50, 300);
--CreatePlayer("unylchan", 50, 300);

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 100);	-- ������ �������� ������ ������������ �������


local x = 0
local dx = 32

for i = 0,50 do
	CreateSprite("phys_floor", x + dx*i, 330);
end

-- CreateColorBox(x1, y1, x2, y2, z, {r, g, b, a} )
box1 = CreateColorBox(0, 200, 100, 300, 0.5, {1, 0.5, 0.5, 1} )
box2 = CreateColorBox(120, 200, 220, 300, -0.5, {0.5, 1, 0.5, 1} )
-- DESU DESU DESU DESU DESU DESU DESU DESU DESU DESU

local function trd(s, n, box)
	Log(s .. "started")
	local b = true
	while true do
		--Log(s .. " wait for" .. n .. " seconds")
		local obj = GetObject(box)
		--Log(serialize("obj", obj))
		SetObjPos(box, obj.aabb.p.x, obj.aabb.p.y + (b and 50 or -50))
		b = not b
		Wait(n)
	end
end

local trd1 = NewThread( trd )
local trd2 = NewThread( trd )

--[[
local blink_lbl = CreateWidget(constants.wt_Label, "blink_lbl", nil, 300, 20, 70, 10)
WidgetSetCaption(blink_lbl, "It blinking!!!")
WidgetSetCaptionColor(blink_lbl, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(blink_lbl, {1.0, 0.2, 0.2, 1.0}, true)
--]]

function restart(sender)
	DestroyWidget(sender)
	if blink_lbl then DestroyWidget(blink_lbl) end
	StopAllThreads()
	trd1 = nil
	trd2 = nil
	dofile("scripts/testlevel2.lua")
end

--[[
local restart_button = CreateWidget(constants.wt_Button, "restart_button", nil, 20, 20, 70, 10)
WidgetSetCaption(restart_button, "RESTART")
WidgetSetCaptionColor(restart_button, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(restart_button, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(restart_button, restart)
--]]

--Resume(trd1, "trd1", 500, box1)
--Resume(trd2, "trd2", 250, box2)

function blink()
	local c = 5
	while c > 0 do
		WidgetSetVisible(blink_lbl, false)
		Wait(100)
		WidgetSetVisible(blink_lbl, true)
		Wait(1000)
		c = c - 1
		WidgetSetCaption(blink_lbl, "It is blinking!!!  " .. c)
	end
	DestroyWidget(blink_lbl)
	blink_lbl = nil
	Log("Exiting blink")
end

local trd_blink = NewThread( blink )
--Resume(trd_blink)
require('dialogue')

function submoveaway()
	Log("And here too!")
	Wait(500)
	SetDynObjAcc(player.id, 0, 0);
end

function moveaway()
	Log("We got here, allright.")
	player = GetPlayer()
        SetDynObjAcc(player.id, -player.max_x_vel, 0);
	current_dialogue.lines["ROOT"]=current_dialogue.lines["repeat"]
	local thr = NewThread(submoveaway)
	Resume(thr)
end

local test = Dialogue.create()
test.lines["ROOT"] = DialogueLine.create(constants.dlText, {"A long time ago", "In a galaxy far, far away"})
test.lines[1] = DialogueLine.create(constants.dlText, {"There was", "some kind of a", "dialogue system"}, {{}, {["align"]=constants.taCenter}, {["align"]=constants.taRight} })
test.lines[2] = DialogueLine.create(constants.dlText, {"And  \\c 1 0 0 1it \\c 0 1 0 1w\\c 0 0.8 0.2 1o\\c 0 0.6 0.4 1r\\c 0 0.4 0.6 1k\\c 0 0.2 0.8 1e\\c 0 0 1 1d!\\c 1 1 1 1 "})
test.lines[2].nextline = 4
test.lines[4] = DialogueLine.create(constants.dlText, {" \\c 1 0 0 1A\\c 0.9 0.1 0 1A\\c 0.8 0.2 0 1A\\c 0.7 0.3 0 1A\\c 0.6 0.4 0 1A\\c 0.5 0.5 0 1A\\c 0.4 0.6 0 1A\\c 0.3 0.7 0 1A\\c 0.2 0.8 0 1A\\c 0.1 0.9 0 1A\\c 0 1 0 1A", 
													   " \\c 0 1 0 1A\\c 0 0.9 0.1 1A\\c 0 0.8 0.2 1A\\c 0 0.7 0.3 1A\\c 0 0.6 0.4 1A\\c 0 0.5 0.5 1A\\c 0 0.4 0.6 1A\\c 0 0.3 0.7 1A\\c 0 0.2 0.8 1A\\c 0 0.1 0.9 1A\\c 0 0 1 1A", 
													   " \\c 0 0 1 1A\\c 0.1 0 0.9 1A\\c 0.2 0 0.8 1A\\c 0.3 0 0.7 1A\\c 0.4 0 0.6 1A\\c 0.5 0 0.5 1A\\c 0.6 0 0.4 1A\\c 0.7 0 0.3 1A\\c 0.8 0 0.2 1A\\c 0.9 0 0.1 1A\\c 1 0 0 1A\\c 1 1 1 1 "
													   }, 
													   {{["align"]=constants.taRight}, {["align"]=constants.taCenter}, {}})
test.lines[5] = DialogueLine.create(constants.dlText, {"This", "is", "a", "really", "long", "message"}, {["portrait_left"]="portrait-unyl", ["portrait_left_x"]=100, ["portrait_left_y"]=100})													   
test.lines[5].nextline = 3
test.lines[3] = DialogueLine.create(constants.dlChoice, {"Yes, it worked", "No, it did not"}, {8, "no", ["text"]=" \\c 1 1 0 1So... did it work?\\c 1 1 1 1 "})
test.lines[8] = DialogueLine.create(constants.dlText, {"Good, good."})
test.lines[9] = DialogueLine.create(constants.dlText, {"We can continue from here.","But we will not."})
test.lines[9].nextline = "no"
test.lines["no"] = DialogueLine.create(constants.dlScript, moveaway)
test.lines["no"].nextline = 68
test.lines["repeat"] = DialogueLine.create(constants.dlText, {"You've been here before, haven't you?"})
test.lines["repeat"].nextline = 66
test.lines[66] = DialogueLine.create(constants.dlText, {"Anyway, there was a dialogue system\nlong time ago"})
test.lines[66].nextline = 2
test.lines[68] = DialogueLine.create(constants.dlText, {"What a pity!"}, {["portrait_left"]="PORTRAIT_CLEAR"})
test.lines[68].nextline = "DIALOGUE_END"

function mapscriptlol()
	while true do
		player = GetPlayer()
		if player.aabb.p.x > 400 then
			if not current_dialogue then 
				Log("PLAYER X = "..player.aabb.p.x)
				EnablePlayerControl(false)
				test:start(10, 10, 600, 75)
			end
		else
			EnablePlayerControl(true)
		end
		Wait(1)
	end
end

local trd_mapscript = NewThread( mapscriptlol )
Resume(trd_mapscript)

