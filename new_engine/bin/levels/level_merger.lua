if #arg < 2 then
	print( string.format( "Usage: %s file1 file2 [file3...]", arg[0] ) )
end

objects = {}
file = nil
line = nil
value = nil
version = 0
result = {}

table.insert( result, "CreateMap({" )

for i=0,#arg do
	file = io.open( arg[i], "r" )
	if not file then
		print( string.format( "Cannot open file '%s'.", arg[i] ) )
		return
	end
	line = file:read()
	while line do
		value = string.match( line, "^}, (%d+)%)%s*$" )
		if value then
			version = math.max( version, tonumber(value) )
		elseif string.match( line, "%s*{constants%.Obj.+}" ) and not objects[line] then
			objects[line] = true
			table.insert( result, line )
		end
		line = file:read()
	end
	file:close()
end

table.insert( result, string.format( "}, %i)", version or 1 ) )

print( table.concat( result, "\n" ) )
