if not levelnum then levelnum = 0 end
nomenu = false
local mapz = 0.8

function DrawMap() 
	plpos = { math.floor((player.aabb.p.x-207)/415), math.floor((player.aabb.p.y-119)/239)-1 }
	screen = GetScreenInfo()
	map = {}
	local box
	for i=-mapsize-1, mapsize+1 do
		for j=-mapsize-1, mapsize+1 do
			if (generated[i][j] or deadend[i][j]) and visited[i][j] then
				box = CreateColorBox(screen.x+(i*15), screen.y+(j*15), screen.x+10+(i*15), screen.y+10+(j*15), mapz, {.8, .8, .2, .8});
				table.insert(map, box)
				if rooms.exits[results[i][j]][1] and generated[i][j-1] then 
					box = CreateColorBox(screen.x+5+(i*15), screen.y-5+(j*15), screen.x+7+(i*15), screen.y+(j*15), mapz, {.8, .8, .2, .8});
					table.insert(map, box)
				end
				if rooms.exits[results[i][j]][2] and generated[i+1][j] then 
					box = CreateColorBox(screen.x+10+(i*15), screen.y+(j*15), screen.x+15+(i*15), screen.y+10+(j*15), mapz, {.8, .8, .2, .8});
					table.insert(map, box)
				end
				if rooms.exits[results[i][j]][3] and generated[i][j+1] then 
					box = CreateColorBox(screen.x+5+(i*15), screen.y+10+(j*15), screen.x+7+(i*15), screen.y+15+(j*15), mapz, {.8, .8, .2, .8});
					table.insert(map, box)
				end
				if rooms.exits[results[i][j]][4] and generated[i-1][j] then 
					box = CreateColorBox(screen.x-5+(i*15), screen.y+(j*15), screen.x+(i*15), screen.y+10+(j*15), mapz, {.8, .8, .2, .8});
					table.insert(map, box)
				end
			end
		end
	end
	for i=1,#checkpoint do
		if visited[checkpoint[i][1]][checkpoint[i][2]] then
			box = CreateColorBox(screen.x+(checkpoint[i][1]*15), screen.y+(checkpoint[i][2]*15), screen.x+10+(checkpoint[i][1]*15), screen.y+10+(checkpoint[i][2]*15), mapz, {.5, .5, .8, .8});
			table.insert(map, box)
		end
	end
	box = CreateColorBox(screen.x+(plpos[1]*15)-1, screen.y+(plpos[2]*15)-1, screen.x+10+(plpos[1]*15)+1, screen.y+10+(plpos[2]*15)+1, mapz, {1, 0, 0, 1});
	table.insert(map, box)
	box = CreateColorBox(screen.x+(plpos[1]*15), screen.y+(plpos[2]*15), screen.x+10+(plpos[1]*15), screen.y+10+(plpos[2]*15), mapz, {1, 1, 1, .8});
	table.insert(map, box)
	Wait(1)
	TogglePause()
end

curcp = 0;

function ExploreMap(player)
	---119
	plpos = { math.floor((player.aabb.p.x-207)/415), math.floor((player.aabb.p.y)/239)-1 }
	for i=1,#checkpoint do
		if plpos[1] == checkpoint[i][1] and plpos[2] == checkpoint[1][2] and curcp ~= i then
			curcp = i
			SetPlayerRevivePoint(coord[checkpoint[i][1]][checkpoint[i][2]][1]+300, coord[checkpoint[i][1]][checkpoint[i][2]][2]+238)
		end
	end
	if not visited[plpos[1]][plpos[2]] then visited[plpos[1]][plpos[2]] = true end
end

function UnDrawMap()
	TogglePause()
	Wait(1)
	for i=1,#map do
		SetObjDead(map[i])
	end
end

function room1(x, y, item, player, enemy)
	wall_down(x, y)
	back1(x,y)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
	local ret = {};
	--if ( math.random(1,3) == 1 ) then way_right(x,y) end
	--ret.shift = { {0, -239}, {415, 0}, {0, 239}, {-415, 0}};
	return ret;
end

function room2(x, y, item, player, enemy)
	wall_down(x, y)
	back1(x,y)
	way_up(x,y)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
	local ret = {};
	return ret;
end

function room3(x, y, item, player, enemy)
	way_down(x, y)
	back1(x,y)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
	local ret = {};
	return ret;
end

function room4(x, y, item, player, enemy)
	wall_down(x,y)
	back1(x,y)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
	wall_right(x,y)
	return ret;
end


function room5(x, y, item, player, enemy)
	back1(x,y)
	way_down(x,y)
	way_up(x,y)
	wall_right(x,y)
	wall_left(x,y)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
end

function room6(x, y, item, player, enemy)
	back1(x,y)
	wall_down(x,y)
	wall_left(x,y)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
	local ret = {};
	wall_left(x,y)
end


function create_content(x, y, item, player, enemy, sprite)
	if item then CreateItem(item, 300+x, 238+y); end
	if player then CreatePlayer(player, 300+x, 238+y); end
	if enemy then CreateEnemy(enemy, 300+x, 238+y); end
	if sprite then CreateSprite(sprite, 300+x, 238+y); end
end

function way_up(x,y)
	CreateSprite("lab-floor2-random", 312+x, 282+y);
	CreateSprite("lab-floor3-random", 344+x, 282+y);
	CreateSprite("lab-floor6", 312+x, 283+y);
	local group = CreateSprite("phys-empty-cl", 312+x, 290+y);
	GroupObjects(group, CreateSprite("phys-empty-cl-end", 382+x, 317+y));
end

function wall_right(x, y)
	CreateSprite("lab-wall2", x+559, y+357);
	CreateSprite("lab-wall4", x+559, y+385);
	CreateSprite("lab-wall2", x+559, y+318);
	CreateSprite("lab-wall2", x+559, y+279);
	CreateSprite("lab-wall2", x+559, y+240);
	CreateSprite("lab-wall2", x+559, y+201);
	CreateSprite("lab-wall5", x+559, y+165);
	local group = CreateSprite("phys-empty", x+576, y+195);
	GroupObjects(group, CreateSprite("phys-empty-end", x+608, y+435));
end

function way_right(x, y)
	CreateSprite("lab-wall2", x+559, y+240);
	CreateSprite("lab-wall2", x+559, y+201);
	CreateSprite("lab-wall2", x+559, y+194);
	local group = CreateSprite("phys-empty", x+576, y+204);
	GroupObjects(group, CreateSprite("phys-empty-end", x+608, y+294));
	CreateSprite("lab-door1", x+559, y+232);
end

function wall_left(x, y)
	wall_right(x-415,y)
end

function wall_down(x, y)
	CreateSprite("lab-floor1-random", 142+x, 388+y);
	CreateSprite("lab-floor7", 141+x, 388+y);
	CreateSprite("lab-floor1-random", 174+x, 388+y);
	CreateSprite("lab-floor1-random", 206+x, 388+y);
	CreateSprite("lab-floor1-random", 238+x, 388+y);
	CreateSprite("lab-floor1-random", 270+x, 388+y);
	CreateSprite("lab-floor1-random", 302+x, 388+y);
	CreateSprite("lab-floor1-random", 334+x, 388+y);
	CreateSprite("lab-floor1-random", 366+x, 388+y);
	CreateSprite("lab-floor1-random", 398+x, 388+y);
	CreateSprite("lab-floor1-random", 430+x, 388+y);
	CreateSprite("lab-floor4-random", 168+x, 423+y);
	CreateSprite("lab-floor4-random", 200+x, 423+y);
	CreateSprite("lab-floor4-random", 232+x, 423+y);
	CreateSprite("lab-floor4-random", 264+x, 423+y);
	CreateSprite("lab-floor4-random", 296+x, 423+y);
	CreateSprite("lab-floor4-random", 328+x, 423+y);
	CreateSprite("lab-floor4-random", 360+x, 423+y);
	CreateSprite("lab-floor4-random", 392+x, 423+y);
	CreateSprite("lab-floor4-random", 424+x, 423+y);
	CreateSprite("lab-floor4-random", 456+x, 423+y);
	CreateSprite("lab-floor1-random", 462+x, 388+y);
	CreateSprite("lab-floor1-random", 494+x, 388+y);
	CreateSprite("lab-floor1-random", 526+x, 388+y);
	CreateSprite("lab-floor4-random", 488+x, 423+y);
	CreateSprite("lab-floor4-random", 520+x, 423+y);
	CreateSprite("lab-floor4-random", 552+x, 423+y);
	local group = CreateSprite("phys-empty", 149+x, 404+y);
	GroupObjects(group, CreateSprite("phys-empty-end", 567+x, 433+y));
end

function way_down(x, y)
	CreateSprite("lab-floor7", 148+x, 388+y);
	CreateSprite("lab-floor1-random", 142+x, 388+y);
	CreateSprite("lab-floor1-random", 174+x, 388+y);
	CreateSprite("lab-floor1-random", 206+x, 388+y);
	CreateSprite("lab-floor1-random", 238+x, 388+y);
	CreateSprite("lab-floor1-random", 270+x, 388+y);
	CreateSprite("lab-floor1-random", 398+x, 388+y);
	CreateSprite("lab-floor1-random", 430+x, 388+y);
	CreateSprite("lab-floor4-random", 168+x, 423+y);
	CreateSprite("lab-floor4-random", 200+x, 423+y);
	CreateSprite("lab-floor4-random", 232+x, 423+y);
	CreateSprite("lab-floor4-random", 264+x, 423+y);
	CreateSprite("lab-floor4-random", 296+x, 423+y);
	CreateSprite("lab-floor4-random", 424+x, 423+y);
	CreateSprite("lab-floor4-random", 456+x, 423+y);
	CreateSprite("lab-floor1-random", 462+x, 388+y);
	CreateSprite("lab-floor1-random", 494+x, 388+y);
	CreateSprite("lab-floor1-random", 526+x, 388+y);
	CreateSprite("lab-floor4-random", 488+x, 423+y);
	CreateSprite("lab-floor4-random", 520+x, 423+y);
	CreateSprite("lab-floor4-random", 552+x, 423+y);
	local group = CreateSprite("phys-empty", 149+x, 404+y);
	GroupObjects(group, CreateSprite("phys-empty-end", 315+x, 433+y));
	group = CreateSprite("phys-empty-cl", 315+x, 404+y);
	GroupObjects(group, CreateSprite("phys-empty-cl-end", 406+x, 433+y));
	group = CreateSprite("phys-empty", 406+x, 404+y);
	GroupObjects(group, CreateSprite("phys-empty-end", 567+x, 433+y));
	CreateSprite("lab-floor3-random", 366+x, 388+y);
	CreateSprite("lab-floor2-random", 302+x, 388+y);
	CreateSprite("lab-floor1-random", 334+x, 388+y);
end

function back1(x,y)
	CreateSprite("lab-back2a", 142+x, 341+y);
	CreateSprite("lab-back1a", 177+x, 341+y);
	CreateSprite("lab-back1a", 270+x, 341+y);
	CreateSprite("lab-back1a", 363+x, 341+y);
	CreateSprite("lab-back1a", 456+x, 341+y);
	CreateSprite("lab-floor1-random", 462+x, 388+y);
	CreateSprite("lab-floor1-random", 494+x, 388+y);
	CreateSprite("lab-floor1-random", 526+x, 388+y);
	CreateSprite("lab-floor4-random", 488+x, 423+y);
	CreateSprite("lab-floor4-random", 520+x, 423+y);
	CreateSprite("lab-floor4-random", 552+x, 423+y);
	CreateSprite("lab-back1a", 467+x, 341+y);
	CreateSprite("lab-back3-random", 142+x, 233+y);
	CreateSprite("lab-back5-random", 142+x, 194+y);
	CreateSprite("lab-back4", 177+x, 194+y);
	CreateSprite("lab-back4", 270+x, 194+y);
	CreateSprite("lab-back4", 363+x, 194+y);
	CreateSprite("lab-back4", 455+x, 194+y);
	CreateSprite("lab-back4", 468+x, 194+y);
	CreateColorBox(160+x, 210+y, 560+x, 419+y, -0.99, wallcolor);
end

wallcolor = {55/255, 59/255, 70/255, 1};
enemies = {nil, "btard-spawner", "btard-spawner", "btard-spawner", "slowpoke-spawner", "slowpoke-spawner", "robot-spawner-right", "btard-com-spawner-right"}
items = {nil, nil, nil, nil, nil, nil, "vegetable1", "vegetable1", "vegetable2"}
rooms = {}
rooms.generators = { room1, room2, room3, room4, room5, room6 }
rooms.exits = { {false, true, false, true}, {true, true, false, true}, {false, true, true, true},  {false, false, false, true},  {true, false, true, false}, {false, true, false, false} }
shift = { {0, -239}, {415, 0}, {0, 239}, {-415, 0}}
generated = {}
results = {}
coord = {}
depths = {}
visited = {}
deadend = {}
maxdepthpoint = {0, 0}
d = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}}

function find_room( up, right, down, left, dead_end_possible )
	local ret = {}
	Log(">>>>find_room")
	if up ~= nil then Log("need up") if not up then Log("being a wall") end end
	if down ~= nil then Log("need down") if not down then Log("being a wall") end end
	if left ~= nil then Log("need left") if not left then Log("being a wall") end end
	if right ~= nil then Log("need right") if not right then Log("being a wall") end end
	if dead_end_poosible then
		for i=1,#rooms.generators do
			if ( (up and rooms.exits[i][1]) or up == nil ) and
			   ( (right and rooms.exits[i][2]) or right == nil ) and
			   ( (down and rooms.exits[i][3]) or down == nil ) and
		           ( (left and rooms.exits[i][4]) or left == nil ) then

				Log("pushed "..i)
				table.insert(ret, i)
			end
		end
	else
		for i=1,#rooms.generators do
			if ( (up and rooms.exits[i][1]) or up == nil ) and
			   ( (right and rooms.exits[i][2]) or right == nil ) and
			   ( (down and rooms.exits[i][3]) or down == nil ) and
		           ( (left and rooms.exits[i][4]) or left == nil ) and 
			   ( (  (rooms.exits[i][1] and up == nil) or (rooms.exits[i][2] and right == nil)
			     or (rooms.exits[i][3] and down == nil) or (rooms.exits[i][4] and left == nil)  ) ) then

				Log("pushed "..i)
				table.insert(ret, i)
			end
		end
	end
	if #ret == 0 then return -1 end
	return ret[math.random(1, #ret)]
end

second_player = false

function GenerateRoom(depth, maxdepth, x, y, dir, ix, iy)
	if depth > maxdepth then
		return nil
	end
	if depth == 1 then
		mapsize = maxdepth
		for i=-maxdepth-1, maxdepth+1 do
			generated[i]={}
			results[i] = {}
			coord[i] = {}
			depths[i] = {}
			visited[i] = {}
			deadend[i] = {}
			for j=-maxdepth-1, maxdepth+2 do
				generated[i][j] = false
				visited[i][j] = false
				results[i][j] = 0
				coord[i][j] = {}
				depths[i][j] = maxdepth+1
				deadend[i][j] = false
			end
		end
		pl = "sohchan"
		en = nil
		it = nil
		ix = 0
		iy = 0
		visited[0][0] = true
		dir = 4
	else
		if second_player then pl = nil else pl = "unylchan" end
		--en = enemies[math.random(1+math.min(#enemies-1, math.floor(depth/5)+levelnum),#enemies)]
		en = enemies[math.random(1+math.min(math.floor(depth*#enemies/maxdepth)-1, levelnum),math.floor(depth*#enemies/maxdepth))]
		it = items[math.random(1,#items)]
	end
	ex = { nil, nil, nil, nil }
	if generated[ix-1][iy] then ex[4]=rooms.exits[results[ix-1][iy]][2] end
	if generated[ix+1][iy] then ex[2]=rooms.exits[results[ix+1][iy]][4] end
	if generated[ix][iy+1] then ex[3]=rooms.exits[results[ix][iy+1]][1] end
	if generated[ix][iy-1] then ex[1]=rooms.exits[results[ix][iy-1]][3] end
	local room = find_room( ex[1], ex[2], ex[3], ex[4] )
	Log("room = "..room)
	if room == -1 then
		Log("No room like that")
		return nil
	end
	results[ix][iy] = room
	generated[ix][iy] = true
	coord[ix][iy] = {x, y}
	depths[ix][iy] = depth
	if depth>depths[maxdepthpoint[1]][maxdepthpoint[2]] or (depth==depths[maxdepthpoint[1]][maxdepthpoint[2]] and math.random(4)==1)
		 then maxdepthpoint = {ix, iy} end
	local result = rooms.generators[room](x, y, it, pl, en)
	for i=1,4 do
		if rooms.exits[room][i] and not generated[ix+d[i][1]][iy+d[i][2]] then
			GenerateRoom(depth+1, maxdepth, x+shift[i][1], y+shift[i][2], i, ix+d[i][1], iy+d[i][2], depth>5)
		end
	end
	if depth==1 then
		for i=-maxdepth, maxdepth do
			for j=-maxdepth, maxdepth do
				if generated[i][j] and results[i][j] > 0 then
					Log("checking room at "..i.." "..j.." for holes")
					Log("which is roomtype "..results[i][j])
					if not generated[i][j-1] then
						wall_down(coord[i][j][1]+shift[1][1], coord[i][j][2]+shift[1][2])
					end
					if rooms.exits[results[i][j]][2] and not generated[i+1][j] then
						wall_right(coord[i][j][1], coord[i][j][2])
					end
					if rooms.exits[results[i][j]][3] and not generated[i][j+1] then
						room2(coord[i][j][1]+shift[3][1], coord[i][j][2]+shift[3][2])
						results[i][j+1] = 2
						deadend[i][j+1] = true
						wall_right(coord[i][j][1], coord[i][j][2]+shift[3][2])
						wall_left(coord[i][j][1], coord[i][j][2]+shift[3][2])
					end
					if rooms.exits[results[i][j]][4] and not generated[i-1][j] then
						wall_right(coord[i][j][1]+shift[4][1], coord[i][j][2]+shift[4][2])
					end
					if rooms.exits[results[i][j]][2] and generated[i+1][j] and math.random(1,3)==1 then
						way_right(coord[i][j][1], coord[i][j][2])
					end
				end
			end
		end
	
		local x = maxdepthpoint[1]
		local y = maxdepthpoint[2]	
		create_content(coord[x][y][1], coord[x][y][2], "banhammer", nil, nil, "lab-back6" )

		local btdepth = depths[x][y]
		while btdepth > math.floor(depths[maxdepthpoint[1]][maxdepthpoint[2]]/2) do
			Log("Backtracking at "..x.." "..y)
			Log("Now at depth "..btdepth)
			if generated[x][y-1] and rooms.exits[results[x][y]][1] and depths[x][y-1] and depths[x][y-1]<btdepth then
				Log("Going up to depth "..depths[x][y-1])
				y = y-1
			elseif generated[x+1][y] and rooms.exits[results[x][y]][2] and depths[x+1][y] and depths[x+1][y]<btdepth then
				Log("Going right to depth "..depths[x+1][y])
				x = x+1
			elseif generated[x][y+1] and rooms.exits[results[x][y]][3] and depths[x][y+1]  and depths[x][y+1]<btdepth then
				Log("Going down to depth "..depths[x][y+1])
				y = y+1
			elseif generated[x-1][y] and rooms.exits[results[x][y]][4] and depths[x-1][y]  and depths[x-1][y]<btdepth then
				Log("Going left to depth "..depths[x-1][y])
				x = x-1
			end
			btdepth = depths[x][y]
		end
		create_content(coord[x][y][1], coord[x][y][2]+50, nil, nil, nil, "ibrj");
		Log("adding checkpoint at "..x.." "..y)
		table.insert(checkpoint, {x, y})
	end
end

GenerateRoom(1, 10, 0, 0)