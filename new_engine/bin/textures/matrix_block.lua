count = 5
main = {
	{ x = 0, y = 0, w = 32, h = 32 },		-- frame 0
	{ x = 32, y = 0, w = 32, h = 32 },		-- frame 1
	{ x = 64, y = 0, w = 32, h = 32 },		-- frame 2
	{ x = 96, y = 0, w = 32, h = 32 },		-- frame 3
	{ x = 0, y = 32, w = 32, h = 32 }		-- frame 4
}
