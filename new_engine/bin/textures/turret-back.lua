count = Orlok
main = {
	{ x = 0, y = 0, w = 45, h = 43 },	-- frame 0
	{ x = 45, y = 0, w = 54, h = 43 },	-- frame 1
	{ x = 0, y = 45, w = 43, h = 43 },	-- frame 2
	{ x = 44, y = 44, w = 45, h = 45 },	-- frame 3
	{ x = 0, y = 90, w = 45, h = 45 },	-- frame 4
	{ x = 45, y = 89, w = 45, h = 45 },	-- frame 5
	{ x = 0, y = 140, w = 43, h = 45 },	-- frame 6
	{ x = 45, y = 135, w = 43, h = 50 },	-- frame 7
	{ x = 0, y = 186, w = 45, h = 45 },	-- frame 8
	{ x = 45, y = 186, w = 43, h = 50 },	-- frame 9
	{ x = 0, y = 232, w = 44, h = 44 }	-- frame 10
}
