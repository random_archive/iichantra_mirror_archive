main = {
	{ x = 0, y = 0, w = 21, h = 28 },		-- frame 0
	{ x = 21, y = 0, w = 21, h = 28 },		-- frame 1
	{ x = 42, y = 0, w = 21, h = 28 },		-- frame 2
	{ x = 63, y = 0, w = 21, h = 28 },		-- frame 3
	{ x = 84, y = 0, w = 21, h = 28 },		-- frame 4
	{ x = 0, y = 28, w = 21, h = 28 },		-- frame 5
	{ x = 21, y = 28, w = 21, h = 28 },		-- frame 6
	{ x = 42, y = 28, w = 21, h = 28 },		-- frame 7
	{ x = 63, y = 28, w = 21, h = 28 },		-- frame 8
	{ x = 84, y = 28, w = 21, h = 28 },		-- frame 9
	{ x = 0, y = 56, w = 21, h = 28 },		-- frame 10
	{ x = 21, y = 56, w = 21, h = 28 },		-- frame 11
	{ x = 42, y = 56, w = 21, h = 28 },		-- frame 12
	{ x = 63, y = 56, w = 21, h = 28 },		-- frame 13
	{ x = 84, y = 56, w = 21, h = 28 },		-- frame 14
	{ x = 0, y = 84, w = 21, h = 28 },		-- frame 15
	{ x = 21, y = 84, w = 21, h = 28 },		-- frame 16
	{ x = 42, y = 84, w = 21, h = 28 },		-- frame 17
	{ x = 63, y = 84, w = 21, h = 28 },		-- frame 18
}
