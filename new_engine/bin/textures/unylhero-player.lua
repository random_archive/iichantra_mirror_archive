count = 20
main = {
	{ x = 0, y = 0, w = 59, h = 72 },	-- frame 0
	{ x = 59, y = 0, w = 59, h = 72 },	-- frame 1
	{ x = 118, y = 0, w = 59, h = 72 },	-- frame 2
	{ x = 177, y = 0, w = 59, h = 72 },	-- frame 3

	{ x = 0, y = 72, w = 59, h = 72 },	-- frame 0
	{ x = 59, y = 72, w = 59, h = 72 },	-- frame 1
	{ x = 118, y = 72, w = 59, h = 72 },	-- frame 2
	{ x = 177, y = 72, w = 59, h = 72 },	-- frame 3

	{ x = 0, y = 144, w = 59, h = 72 },	-- frame 0
	{ x = 59, y = 144, w = 59, h = 72 },	-- frame 1
	{ x = 118, y = 144, w = 59, h = 72 },	-- frame 2
	{ x = 177, y = 144, w = 59, h = 72 },	-- frame 3

	{ x = 0, y = 216, w = 59, h = 72 },	-- frame 0
	{ x = 59, y = 216, w = 59, h = 72 },	-- frame 1
	{ x = 118, y = 216, w = 59, h = 72 },	-- frame 2
	{ x = 177, y = 216, w = 59, h = 72 },	-- frame 3

	{ x = 0, y = 288, w = 59, h = 72 },	-- frame 0
	{ x = 59, y = 288, w = 59, h = 72 },	-- frame 1
	{ x = 118, y = 288, w = 59, h = 72 },	-- frame 2
	{ x = 177, y = 288, w = 59, h = 72 }	-- frame 3
}
