count = 25
main = {
	{ x = 6, y = 4, w = 28, h = 71 },	-- frame 0
	{ x = 36, y = 8, w = 28, h = 67 },	-- frame 1
	{ x = 66, y = 8, w = 28, h = 67 },	-- frame 2
	{ x = 95, y = 8, w = 29, h = 67 },	-- frame 3
	{ x = 128, y = 7, w = 33, h = 68 },	-- frame 4
	{ x = 163, y = 6, w = 38, h = 69 },	-- frame 5
	{ x = 3, y = 78, w = 48, h = 69 },	-- frame 6
	{ x = 56, y = 78, w = 39, h = 69 },	-- frame 7
	{ x = 98, y = 80, w = 32, h = 67 },	-- frame 8
	{ x = 135, y = 79, w = 39, h = 68 },	-- frame 9
	{ x = 181, y = 78, w = 47, h = 69 },	-- frame 10
	{ x = 0, y = 149, w = 35, h = 69 },	-- frame 11
	{ x = 43, y = 152, w = 28, h = 71 },	-- frame 12
	{ x = 72, y = 152, w = 28, h = 71 },	-- frame 13
	{ x = 101, y = 151, w = 28, h = 71 },	-- frame 14
	{ x = 130, y = 151, w = 28, h = 71 },	-- frame 15
	{ x = 161, y = 151, w = 28, h = 71 },	-- frame 16
	{ x = 229, y = 5, w = 28, h = 71 },	-- frame 17
	{ x = 260, y = 5, w = 28, h = 71 },	-- frame 18
	{ x = 293, y = 5, w = 28, h = 71 },	-- frame 19
	{ x = 326, y = 5, w = 35, h = 71 },	-- frame 20
	{ x = 363, y = 6, w = 43, h = 71 },	-- frame 21
	{ x = 231, y = 80, w = 32, h = 71 },	-- frame 22
	{ x = 265, y = 80, w = 32, h = 71 },	-- frame 23
	{ x = 299, y = 80, w = 32, h = 71 }	-- frame 24
}
