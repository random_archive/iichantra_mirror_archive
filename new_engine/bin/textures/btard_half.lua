count = 9
main = {
	{ x = 3, y = 3, w = 34, h = 35 },	-- frame 0
	{ x = 43, y = 3, w = 34, h = 33 },	-- frame 1
	{ x = 84, y = 3, w = 33, h = 30 },	-- frame 2
	{ x = 121, y = 5, w = 33, h = 30 },	-- frame 3
	{ x = 0, y = 45, w = 33, h = 32 },	-- frame 4
	{ x = 38, y = 45, w = 33, h = 32 },	-- frame 5
	{ x = 76, y = 48, w = 33, h = 30 },	-- frame 6
	{ x = 113, y = 52, w = 34, h = 25 },	-- frame 7
	{ x = 153, y = 56, w = 36, h = 21 }	-- frame 8
}

overlay = {
	{
		{ x = 3, y = 88, w = 16, h = 15, ox = 2, oy = 13 },	-- frame 0
		{ x = 22, y = 88, w = 18, h = 14, ox = 1, oy = 12 },	-- frame 1
		{ x = 43, y = 88, w = 17, h = 14, ox = 1, oy = 12 },	-- frame 2
		{ x = 62, y = 88, w = 17, h = 17, ox = 1, oy = 12 },	-- frame 3
		{ x = 80, y = 88, w = 17, h = 17, ox = 0, oy = 14 },	-- frame 4
		{ x = 100, y = 88, w = 16, h = 15, ox = 2, oy = 14 },	-- frame 5
		{ x = 115, y = 88, w = 19, h = 15, ox = 0, oy = 12 },	-- frame 6
		{ x = 116, y = 87, w = 19, h = 17, ox = 2, oy = 11 },	-- frame 7
		{ x = 134, y = 86, w = 17, h = 15, ox = 4, oy = 6 }	-- frame 8
	}
}
