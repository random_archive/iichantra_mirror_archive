count = 3
main = {
	{ x = 0, y = 1, w = 18, h = 18 },	-- frame 9
	{ x = 16, y = 1, w = 18, h = 18 },	-- frame 1
	{ x = 36, y = 1, w = 18, h = 18 }	-- frame 1
}

overlay = {
	{
		{ x = 0, y = 20, w = 18, h = 18, ox = 0, oy = 0 },
		{ x = 16, y = 20, w = 18, h = 18, ox = 0, oy = 0 },
		{ x = 36, y = 20, w = 18, h = 18, ox = 0, oy = 0 }
	}
}
