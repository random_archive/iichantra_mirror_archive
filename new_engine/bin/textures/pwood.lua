count = 6
main = {
	{ x = 0, y = 0, w = 18, h = 18 },	-- frame 0
	{ x = 18, y = 0, w = 16, h = 16 },	-- frame 1
	{ x = 34, y = 0, w = 15, h = 15 },	-- frame 2
	{ x = 49, y = 0, w = 19, h = 19 },	-- frame 3
	{ x = 68, y = 0, w = 14, h = 14 },	-- frame 4
	{ x = 82, y = 0, w = 11, h = 11 }	-- frame 5
}
