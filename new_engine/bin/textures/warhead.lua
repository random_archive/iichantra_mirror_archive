count = 11
main = {
	{ x = 0, y = 0, w = 63, h = 50 },	-- frame 0
	{ x = 0, y = 58, w = 63, h = 41 },	-- frame 1
	{ x = 0, y = 107, w = 63, h = 41 },	-- frame 2
	{ x = 63, y = 0, w = 63, h = 115 },	-- frame 3
	{ x = 126, y = 0, w = 63, h = 110 },	-- frame 4
	{ x = 189, y = 0, w = 63, h = 100 },	-- frame 5
	{ x = 162, y = 121, w = 94, h = 135 },	-- frame 6
	{ x = 0, y = 169, w = 64, h = 18 },	-- frame 7
	{ x = 0, y = 216, w = 64, h = 18 },	-- frame 8
	{ x = 65, y = 169, w = 64, h = 18 },	-- frame 9
	{ x = 65, y = 216, w = 64, h = 19 }	-- frame 10
}
