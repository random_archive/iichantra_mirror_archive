count = 31
main = {
	{ x = 20, y = 15, w = 46, h = 121 },	-- frame 0
	{ x = 65, y = 12, w = 112, h = 125 },	-- frame 1
	{ x = 178, y = 12, w = 42, h = 124 },	-- frame 2
	{ x = 21, y = 137, w = 44, h = 93 },	-- frame 3
	{ x = 65, y = 136, w = 112, h = 94 },	-- frame 4
	{ x = 178, y = 137, w = 43, h = 93 },	-- frame 5
	{ x = 17, y = 231, w = 48, h = 80 },	-- frame 6
	{ x = 17, y = 313, w = 49, h = 80 },	-- frame 7
	{ x = 17, y = 397, w = 48, h = 80 },	-- frame 8
	{ x = 66, y = 231, w = 111, h = 81 },	-- frame 9
	{ x = 66, y = 313, w = 111, h = 81 },	-- frame 10
	{ x = 66, y = 397, w = 111, h = 81 },	-- frame 11
	{ x = 178, y = 231, w = 54, h = 80 },	-- frame 12
	{ x = 178, y = 313, w = 54, h = 80 },	-- frame 13
	{ x = 178, y = 397, w = 54, h = 80 },	-- frame 14
	{ x = 248, y = 19, w = 64, h = 43 },	-- frame 15
	{ x = 317, y = 19, w = 64, h = 43 },	-- frame 16
	{ x = 385, y = 19, w = 64, h = 43 },	-- frame 17
	{ x = 248, y = 66, w = 64, h = 43 },	-- frame 18
	{ x = 317, y = 66, w = 64, h = 43 },	-- frame 19
	{ x = 385, y = 66, w = 64, h = 43 },	-- frame 20
	{ x = 248, y = 113, w = 64, h = 43 },	-- frame 21
	{ x = 317, y = 113, w = 64, h = 43 },	-- frame 22
	{ x = 385, y = 113, w = 63, h = 43 },	-- frame 23
	{ x = 248, y = 160, w = 64, h = 43 },	-- frame 24
	{ x = 317, y = 160, w = 64, h = 43 },	-- frame 25
	{ x = 385, y = 160, w = 62, h = 43 },	-- frame 26
	{ x = 248, y = 208, w = 64, h = 43 },	-- frame 27
	{ x = 317, y = 208, w = 64, h = 43 },	-- frame 28
	{ x = 385, y = 208, w = 60, h = 43 },	-- frame 29
	{ x = 248, y = 255, w = 64, h = 43 }	-- frame 30
}
