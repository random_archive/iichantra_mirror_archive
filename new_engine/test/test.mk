
GTEST_DIR := $(shell pwd)/gtest/

export GTEST_CFLAGS :=-I$(GTEST_DIR)include -pthread -DUSING_GTEST
export GTEST_LDFLAGS :=-L$(GTEST_DIR) -lgtest -pthread

define TESTS
	$(MAKE) -C SapTest -f test.mk $@
endef

test:
	$(TESTS)
	
run_test:
	$(TESTS)

clean_test:
	$(TESTS)
	
.PHONY: test run_test clean_test