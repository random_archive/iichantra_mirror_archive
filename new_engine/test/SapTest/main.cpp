#include <gtest/gtest.h>

#define UNUSED_ARG(x)	(void)x

#include <stdint.h>
#include "game/phys/sap/OPC_ArraySAP.h"
namespace Opcode {

class Obj {
public:
	ASAP_AABB b;
	udword id;
	udword p;
	Obj(udword id) : id(id), p(0) {}
	Obj() : id(0), p(0) {}
};

class SAPTest : public testing::Test 
{
protected:

	Opcode::ArraySAP* asap;

	virtual void SetUp()
	{
		asap = new Opcode::ArraySAP();
		ASSERT_TRUE(asap != NULL);
	}

	virtual void TearDown()
	{
		delete asap;
	}

	static void setupASAP_AABB(ASAP_AABB& a, 
		float xa, float ya, 
		float xb, float yb)
	{
		a.mMin.x = xa;
		a.mMax.x = xb;
		a.mMin.y = ya;
		a.mMax.y = yb;
#ifndef KERNEL_BUG_ASAP_2D
		a.mMin.z = za;
		a.mMax.z = zb;
#endif
	}

	void AddPair(void const* a, void const* b, udword ida, udword idb)
	{
		asap->AddPair(a, b, ida, idb);
	}

	void SETUP_OBJ(Obj& name, float xa, float ya, float xb, float yb, uword group = 1, uword groupMask = 1)
	{
		setupASAP_AABB(name.b, xa, ya, xb, yb);
		name.p = asap->AddObject(&name, name.id, name.b, group, groupMask);
	}

	ASAP_PairManager* getPM() { return &asap->mPairs; }
};


bool checkPair(Opcode::ASAP_Pair const& p, int id0, int id1)
{
	return (p.id0 == id0 && p.id1 == id1) || 
		   (p.id1 == id0 && p.id0 == id1);
}

bool pairInArr(Opcode::ASAP_Pair const* prs, udword pc, int id0, int id1)
{
	for (udword i = 0; i < pc; ++i)
		if (checkPair(prs[i], id0, id1))
			return true;
	return false;
}

TEST_F(SAPTest, CheckAddAndDump)
{
	Obj o1(1); SETUP_OBJ(o1, 0,0, 1,1);
	Obj o2(2); SETUP_OBJ(o2, 2,0, 3,1);
	Obj o3(3); SETUP_OBJ(o3, 0.5f, 0, 2.5f,1);

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(2), pc);
	ASSERT_PRED4(pairInArr, prs, pc, 1,3);
	ASSERT_PRED4(pairInArr, prs, pc, 3,2);
}

TEST_F(SAPTest, CheckGroupsTrivial)
{
	//asap->SetBoxGroupIntersectionMethod(ArraySAP::TRIVIAL);
	Obj o1(1); SETUP_OBJ(o1, 0,0, 1,1, 1,1);
	Obj o2(2); SETUP_OBJ(o2, 0,0, 1,1, 2,2);
	Obj o3(3); SETUP_OBJ(o3, 0,0, 1,1, 4,4);

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(3), pc);
	ASSERT_PRED4(pairInArr, prs, pc, 1,3);
	ASSERT_PRED4(pairInArr, prs, pc, 3,2);
	ASSERT_PRED4(pairInArr, prs, pc, 1,2);
}

TEST_F(SAPTest, CheckGroupsSimmetrical)
{
	asap->SetBoxGroupIntersectionMethod(ArraySAP::SIMMETRICAL);
	Obj o1(1); SETUP_OBJ(o1, 0,0, 1,1, 1,1);
	Obj o2(2); SETUP_OBJ(o2, 0,0, 1,1, 2,2);
	Obj o3(3); SETUP_OBJ(o3, 0,0, 1,1, 4,3);

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(2), pc);
	ASSERT_PRED4(pairInArr, prs, pc, 1,3);
	ASSERT_PRED4(pairInArr, prs, pc, 3,2);
}

TEST_F(SAPTest, CheckGroupsNonSimmetricel)
{
	asap->SetBoxGroupIntersectionMethod(ArraySAP::NONSIMMETRICAL);
	Obj o1(1); SETUP_OBJ(o1, 0,0, 1,1, 2,3);
	Obj o2(2); SETUP_OBJ(o2, 0,0, 1,1, 1,5);
	Obj o3(3); SETUP_OBJ(o3, 0,0, 1,1, 2,5);
	Obj o4(4); SETUP_OBJ(o4, 0,0, 1,1, 4,3);

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(4), pc);
	ASSERT_PRED4(pairInArr, prs, pc, 2,3);
	ASSERT_PRED4(pairInArr, prs, pc, 1,4);
	ASSERT_PRED4(pairInArr, prs, pc, 2,4);
	ASSERT_PRED4(pairInArr, prs, pc, 3,4);
}


TEST_F(SAPTest, DumpTwice)
{
	Obj o0(42); SETUP_OBJ(o0, -200,100, -220,138);
	Obj o1(43); SETUP_OBJ(o1, -403,169, 550,197);
	Obj o2(44); SETUP_OBJ(o2, 549,168, 1503,196);
	Obj o3(45); SETUP_OBJ(o3, -9001.5f,-9001.5f, -9000.5f,-9000.5f);

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	EXPECT_EQ(static_cast<udword>(1), pc);
	EXPECT_PRED4(pairInArr, prs, pc, 43,44);

	pc = asap->DumpPairs(0, 0, 0, &prs);
	EXPECT_EQ(static_cast<udword>(1), pc);
	EXPECT_PRED4(pairInArr, prs, pc, 43,44);
}

TEST_F(SAPTest, Over65536Obj)
{
	const size_t count = 70000;
	Obj* arr = new Obj[count];

	float x = 0, dx = 1, xw = 1.5f;
	for (size_t i = 0; i < count; ++i)
	{
		arr[i].id = i*2;
		SETUP_OBJ(arr[i], x,0, x+xw,1);
		x += dx;
	}

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	EXPECT_EQ(count-1, pc);

	std::set<std::pair<udword, udword> > pairs;
	for (udword i = 0; i < pc; ++i)
	{
		Opcode::ASAP_Pair const & p = prs[i];
		pairs.insert(std::make_pair(p.id0, p.id1));
	}

	for (size_t i = 1; i < count; ++i)
	{
		Obj const & o1 = arr[i-1];
		Obj const & o2 = arr[i];

		EXPECT_TRUE(pairs.find(std::make_pair(o1.id, o2.id)) != pairs.end());
	}

	delete[] arr;
}

TEST_F(SAPTest, BigObjId)
{
	// |  a  |    |  c  |
	//      |  b   |
	Obj a(1);     SETUP_OBJ(a, 0.0f, 0.0f, 1.0f, 1.0f);
	Obj b(2);     SETUP_OBJ(b, 0.5f, 0.0f, 2.5f, 1.0f);
	Obj c(65538); SETUP_OBJ(c, 2.0f, 0.0f, 3.0f, 1.0f);
	

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(2), pc);
	EXPECT_TRUE(pairInArr(prs, pc, a.id, b.id));
	EXPECT_TRUE(pairInArr(prs, pc, b.id, c.id));
	EXPECT_FALSE(pairInArr(prs, pc, a.id, c.id));
}

TEST_F(SAPTest, BigObjIdRemove)
{
	// |  a  |    |  c  |
	//      |  b   |
	Obj a(1);     SETUP_OBJ(a, 0.0f, 0.0f, 1.0f, 1.0f);
	Obj b(2);     SETUP_OBJ(b, 0.5f, 0.0f, 2.5f, 1.0f);
	Obj c(65538); SETUP_OBJ(c, 2.0f, 0.0f, 3.0f, 1.0f);
	

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(2), pc);
	EXPECT_TRUE(pairInArr(prs, pc, a.id, b.id));
	EXPECT_TRUE(pairInArr(prs, pc, b.id, c.id));
	EXPECT_FALSE(pairInArr(prs, pc, a.id, c.id));

	asap->RemoveObject(c.p);
	pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(1), pc);
	EXPECT_TRUE(pairInArr(prs, pc, a.id, b.id));
	EXPECT_FALSE(pairInArr(prs, pc, b.id, c.id));
	EXPECT_FALSE(pairInArr(prs, pc, a.id, c.id));
}

TEST_F(SAPTest, AddPairSAP)
{
	Obj a(1); 
	Obj b(2);
	Obj c(65538);
	Obj d(0x2ffff);
	Obj e(0x1ffff);
	Obj f(0x0ffff);
	
#define ADD(x, y) AddPair(&x, &y, x.id, y.id);
	ADD(a, b);
	ADD(d, c);
	ADD(a, d);
	ADD(a, e);
	ADD(a, f);
#undef ADD

	ASAP_PairManager* pm = getPM();

	const ASAP_Pair* p;
#define P_TRUE(x, y) p = pm->FindPair(x.id, y.id); \
	                 EXPECT_TRUE(p != NULL); \
					 EXPECT_TRUE(p && checkPair(*p, x.id, y.id));
#define P_FALSE(x, y) p = pm->FindPair(x.id, y.id); \
	                 EXPECT_TRUE(p == NULL);

	P_TRUE(a,b);
	P_FALSE(a,c);
	P_TRUE(c,d);
	P_TRUE(a,d);
	P_TRUE(a,e);
	P_TRUE(a,f);
}

TEST_F(SAPTest, MoveOver)
{
	// |  a  |    |  c  |
	Obj a(1);     SETUP_OBJ(a, 0.0f, 0.0f, 1.0f, 1.0f);
	Obj b(2);     SETUP_OBJ(b, 1.5f, 0.0f, 2.5f, 1.0f);


	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(0), pc);

	setupASAP_AABB(a.b, 3.0f, 0.0f, 3.5f, 1.0f);
	asap->UpdateObject(a.p, a.b);

	pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(0), pc);

	EXPECT_FALSE(pairInArr(prs, pc, a.id, b.id));
}



TEST_F(SAPTest, DISABLED_MoveBeforeDump)
{
	// |  a  |    |  c  |
	//      |  b   |
	Obj a(1); SETUP_OBJ(a, 0.0f, 0.0f, 1.0f, 1.0f);
	Obj b(2); SETUP_OBJ(b, 0.5f, 0.0f, 2.5f, 1.0f);
	Obj c(3); SETUP_OBJ(c, 2.0f, 0.0f, 3.0f, 1.0f);
	
	//       |  c  |
	// |  b   |   |  a  |
	setupASAP_AABB(a.b, 2.8f, 0.0f, 3.5f, 1.0f);
	asap->UpdateObject(a.p, a.b);

	Opcode::ASAP_Pair* prs;
	udword pc = asap->DumpPairs(0, 0, 0, &prs);
	ASSERT_EQ(static_cast<udword>(2), pc);
	EXPECT_TRUE (pairInArr(prs, pc, a.id, c.id));
	EXPECT_TRUE (pairInArr(prs, pc, b.id, c.id));
	EXPECT_FALSE(pairInArr(prs, pc, a.id, b.id));
}


};

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
